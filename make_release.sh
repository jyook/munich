#!/bin/bash
# usage: ./make_release <version>

PRC_LIST="pph21 ppz01 ppw01 ppwx01 ppeex02 ppnenex02 ppexne02 ppenex02  ppaa02 ppeexa03 ppnenexa03 ppexnea03 ppenexa03 ppzz02 ppwxw02 ppeeexex04 ppemexmx04 ppeexnenex04 ppeexnmnmx04 ppemxnmnex04 ppemexnmx04 ppeexmxnm04 ppeeexnex04 ppeexexne04" #, pphh22 ppw01nockm ppwx01nockm ppexne02nockm ppenex02nockm
AMP_LIST="ppaa02 ppll02 pplla03 ppllll04 ppv01 pph21 ppzz02 ppww02" # pphh42  pph21loop  ppll02  ppllh03

VERSION=$1
OUTDIR=MATRIX_$VERSION

MATRIX_DIR="MATRIX"

# create folder
rm -rf $OUTDIR
mkdir $OUTDIR

cd $OUTDIR
# create folders; copy makefiles, scripts, source files
cp ../$MATRIX_DIR/COPYING .
cp ../$MATRIX_DIR/matrix .
cp ../$MATRIX_DIR/README .
cp ../$MATRIX_DIR/changelog.txt .
mkdir config
cp ../$MATRIX_DIR/config/MATRIX_configuration_default config/MATRIX_configuration
cp ../$MATRIX_DIR/config/process_inputs.py config/process_inputs.py
cp ../$MATRIX_DIR/Makefile.clean .
#cp ../$MATRIX_DIR/Makefile.clean_res .
cp -r ../$MATRIX_DIR/src .
cp -r ../$MATRIX_DIR/include .
mkdir external
#cp ../$MATRIX_DIR/external/MoRe-v1.0.0.tar external/
cp ../$MATRIX_DIR/external/cln-1.3.4.tar external/
cp ../$MATRIX_DIR/external/ginac-1.6.2.tar external/
cp ../$MATRIX_DIR/external/tdhpl-1.0.tar external/
cp ../$MATRIX_DIR/external/qqvvamp-1.1.tar external/
cp ../$MATRIX_DIR/external/ggvvamp-1.0.tar external/
cp ../$MATRIX_DIR/external/ampzz.tar external/
cp ../$MATRIX_DIR/external/ampww.tar external/

mkdir prc
cp ../$MATRIX_DIR/prc/Makefile prc/
mkdir psg
mkdir amp 
cp ../$MATRIX_DIR/amp/Makefile amp/

mkdir bin
cp ../$MATRIX_DIR/bin/run_process bin
mkdir bin/modules
cp ../$MATRIX_DIR/bin/modules/*.py bin/modules

mkdir run
mkdir run/setup
cp ../$MATRIX_DIR/run/setup/file_parameter.dat run/setup/

mkdir manual
cp ../$MATRIX_DIR/manual/matrix_manual.pdf manual/

mkdir run/input_files

mkdir obj
mkdir obj/ext
mkdir obj/ext/tdhpl
mkdir obj/amp
mkdir obj/src
mkdir obj/prc
mkdir obj/psg

src_folders="amplitude amplitude/OpenLoops amplitude/Recola contribution event munich phasespace routines classes dipolesubtraction observable qTsubtraction summary"
for folder in $src_folders
do
mkdir obj/src/$folder
done

mkdir lib
mkdir lib/ext
mkdir lib/amp
mkdir lib/src
mkdir lib/prc
mkdir lib/psg

for process in $PRC_LIST
do
  echo $process
  cp -r ../$MATRIX_DIR/psg/psg.$process.tar psg/
  cp -r ../$MATRIX_DIR/prc/$process prc/
  mkdir obj/prc/$process
  mkdir lib/prc/$process
  mkdir obj/psg/$process
  mkdir lib/psg/$process
  mkdir run/input_files/$process
  cp ../$MATRIX_DIR/run/input_files/$process/file_parameter.dat run/input_files/$process/
  cp ../$MATRIX_DIR/run/input_files/$process/file_distribution.dat run/input_files/$process/
  cp ../$MATRIX_DIR/run/input_files/$process/file_model.dat run/input_files/$process/

  mkdir run/input_files/$process/default.input.MATRIX
  cp ../$MATRIX_DIR/run/input_files/$process/default.input.MATRIX/parameter*.dat run/input_files/$process/default.input.MATRIX/
  cp ../$MATRIX_DIR/run/input_files/$process/default.input.MATRIX/distribution*.dat run/input_files/$process/default.input.MATRIX/
  cp ../$MATRIX_DIR/run/input_files/$process/default.input.MATRIX/model*.dat run/input_files/$process/default.input.MATRIX/
  for folder in ../$MATRIX_DIR/run/input_files/$process/default.input.MATRIX/*arXiv*/
  do
    if [ ${folder} != "../$MATRIX_DIR/run/input_files/$process/default.input.MATRIX/*arXiv*/" ]; then
      cp -r $folder run/input_files/$process/default.input.MATRIX/;
    fi
  done

  cp ../$MATRIX_DIR/run/run.${process}.tar run/
done

for amp in $AMP_LIST
do
  echo $amp
  cp -r ../$MATRIX_DIR/amp/$amp amp/
  mkdir obj/amp/$amp
  mkdir lib/amp/$amp
done
mkdir obj/amp/ppllll04/qqvvamp
mkdir obj/amp/ppllll04/ggvvamp

cd ../

rm -r MATRIX_${VERSION}.tar.gz
tar -cf MATRIX_${VERSION}.tar $OUTDIR
rm -r $OUTDIR
gzip MATRIX_${VERSION}.tar

#scp -P 222 MATRIX_${VERSION}.tar.gz mwiesemann@login.hepforge.org:/hepforge/projects/matrix/public_html/download/
#scp -P 222 MATRIX/manual/matrix_manual.pdf mwiesemann@login.hepforge.org:/hepforge/projects/matrix/public_html/download/MATRIX_manual_${VERSION}.pdf

# git add MATRIX_${VERSION}.tar.gz
# git commit -m "added new release version MATRIX_${VERSION}.tar.gz"
# git push
