#include "header.hpp"

void observable_set::initialization_CS_splitting_coefficients_QEW(){
  static Logger logger("observable_set::calculate_splitting_coefficients");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  switch_convention_P_qq = 1; //

  double sum_f_charge2 = 0.;
  for (int i_g = 1; i_g < 4; i_g++){
    // charged leptons
    if (M[11 + (i_g - 1) * 2] == 0.){sum_f_charge2 += 1.;}
    // neutrinos
    if (M[12 + (i_g - 1) * 2] == 0.){sum_f_charge2 += 0.;}
    // down-type quarks
    if (M[1 + (i_g - 1) * 2] == 0.){sum_f_charge2 += N_c * (1. / 9.);}
    // up-type quarks
    if (M[2 + (i_g - 1) * 2] == 0.){sum_f_charge2 += N_c * (4. / 9.);}
  }
  /*
  CS_gamma_a = -(2. / 3.) * sum_f_charge2;
  CS_K_a = -(10. / 9.) * sum_f_charge2;
  */
  // Identical content, just different names:
  CS_QEW_gamma_a = -(2. / 3.) * sum_f_charge2;
  CS_QEW_K_a = -(10. / 9.) * sum_f_charge2;

  CS_QEW_gamma_q = (3. / 2.); // C_F -> Q²_f
  CS_QEW_K_q = (7. / 2.) - pi2_6; // C_F -> Q²_f

  // Only K+P terms (CA):

  // P
  // Allow for different conventions i P_qq:
  if (switch_convention_P_qq){CS_QEW_P_qq_delta = (3. / 2.);}
  else {CS_QEW_P_qq_delta = 0.;}

  CS_QEW_P_aa_reg = 0.; // C_A -> 0
  CS_QEW_P_aa = 0.; // C_A -> 0
  CS_QEW_P_aa_plus = 0.; // C_A -> 0
  CS_QEW_intP_aa_plus = 0.; // C_A -> 0
  CS_QEW_P_aa_delta = (-2. / 3.) * sum_f_charge2; // C_A -> 0, T_R -> sum_f (N_c,f * Q²_f)

  // Kbar
  CS_QEW_Kbar_qq_delta = -(5. - pi2); // C_F -> [Q²_q]

  CS_QEW_Kbar_aa = 0.;
  CS_QEW_Kbar_aa_plus = 0.;
  CS_QEW_intKbar_aa_plus = 0.;
  CS_QEW_Kbar_aa_delta = (16. / 9.) * sum_f_charge2;

  // Kt
  CS_QEW_Kt_qq_delta = -pi2 / 3.; // C_F -> [Q²_q]

  CS_QEW_Kt_aa = 0.;
  CS_QEW_Kt_aa_plus = 0.;
  CS_QEW_Kt_aa_delta = 0.;
  CS_QEW_intKt_aa_plus = 0.;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// P
// P_qa and P_aq have opposite N_c factors (compared to e.g. Marek's paper) !!!
double observable_set::CS_QEW_P_qa(double x){return (1. + pow(1. - x, 2)) / x;} // C_F -> [Q²_q]
double observable_set::CS_QEW_P_aq(double x){return N_c * (pow(x, 2) + pow(1. - x, 2));} // T_R -> N_c * [Q²_q]
double observable_set::CS_QEW_P_qq_reg(double x){return -(1. + x);} // C_F -> [Q²_q]
// Allow for different conventions via switch: affects P_qq (apart from P_qq_reg)
double observable_set::CS_QEW_P_qq(double x){
  if (switch_convention_P_qq){return -(1. + x);} // C_F -> [Q²_q]
  else {return 0.;}
}
double observable_set::CS_QEW_P_qq_plus(double x){
  if (switch_convention_P_qq){return 2. / (1. - x);} // C_F -> [Q²_q]
  else {return (1. + pow(x, 2)) / (1. - x);} // C_F -> [Q²_q]
}
//double observable_set::CS_QEW_P_qq_delta(double x){return 0.;}
double observable_set::CS_QEW_intP_qq_plus(double x){
  if (switch_convention_P_qq){return -2 * log(1. - x);} // C_F -> [Q²_q]
  else {return -.5 * pow(x, 2) - x - 2 * log(1. - x);} // C_F -> [Q²_q]
}

// Kbar
double observable_set::CS_QEW_Kbar_qa(double x){return CS_QEW_P_qa(x) * log((1. - x) / x) + x;} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kbar_aq(double x){return CS_QEW_P_aq(x) * log((1. - x) / x) + 2 * N_c * x * (1. - x);} // T_R -> N_c * [Q²_q]

double observable_set::CS_QEW_Kbar_qq(double x){return (-(1. + x) * log((1. - x) / x) + (1. - x));} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kbar_qq_plus(double x){return ((2 / (1. - x)) * log((1. - x) / x));} // C_F -> [Q²_q]
double observable_set::CS_QEW_intKbar_qq_plus(double x){return (-pow(log(1. - x), 2) - 2 * gsl_sf_dilog(1. - x) + pi2_3);} // C_F -> [Q²_q]

// Kt
double observable_set::CS_QEW_Kt_qa(double x){return CS_QEW_P_qa(x) * log(1. - x);} // C_F -> [Q²_f]

double observable_set::CS_QEW_Kt_aq(double x){return CS_QEW_P_aq(x) * log(1. - x);} // T_R -> N_c * [Q²_f]

double observable_set::CS_QEW_Kt_qq(double x){return CS_QEW_P_qq_reg(x) * log(1. - x);} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kt_qq_plus(double x){return (2 / (1. - x)) * log(1. - x);} // C_F -> [Q²_q]
double observable_set::CS_QEW_intKt_qq_plus(double x){return -pow(log(1. - x), 2);} // C_F -> [Q²_q]



void observable_set::initialization_CS_splitting_coefficients_QCD(){
  static Logger logger("observable_set::calculate_splitting_coefficients");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  /*
  CS_gamma_g = (11. / 6.) * C_A - (2. / 3.) * T_R * N_f;
  CS_gamma_g_ferm = - (2. / 3.) * T_R * N_f;

  CS_K_g = (67. / 18. - pi2_6) * C_A - (10. / 9.) * T_R * N_f;
  CS_K_g_ferm = - (10. / 9.) * T_R * N_f;
  */


    // not yet checked ???

  // Identical content, just different names:
  CS_QCD_gamma_g = (11. / 6.) * C_A - (2. / 3.) * T_R * N_f;
  CS_QCD_gamma_g_ferm = - (2. / 3.) * T_R * N_f;
  CS_QCD_gamma_g_bos = (11. / 6.) * C_A;

  CS_QCD_K_g = (67. / 18. - pi2_6) * C_A - (10. / 9.) * T_R * N_f;
  CS_QCD_K_g_ferm = - (10. / 9.) * T_R * N_f;
  CS_QCD_K_g_bos = (67. / 18. - pi2_6) * C_A;

  CS_QCD_gamma_q = (3. / 2.) * C_F;
  CS_QCD_K_q = (7. / 2. - pi2_6) * C_F;




  // Only K+P terms (CA):

  // not yet checked !!!

  // P
  // Allow for different conventions i P_qq:
  if (switch_convention_P_qq){CS_QCD_P_qq_delta = C_F * (3. / 2.);}
  else {CS_QCD_P_qq_delta = 0.;}
  CS_QCD_P_gg_delta = (11. / 6. * C_A - 2. / 3. * N_f * T_R);

  // Kbar
  CS_QCD_Kbar_qq_delta = C_F * (-(5. - pi2));
  CS_QCD_Kbar_gg_delta = -(C_A * ((50. / 9.) - pi2) - T_R * N_f * (16. / 9.));

  // Kt
  CS_QCD_Kt_qq_delta = C_F * (-pi2 / 3.);
  CS_QCD_Kt_gg_delta = C_A * (-pi2 / 3.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


  // not yet checked !!!

// P
double observable_set::CS_QCD_P_qg(double x){return C_F * (1. + pow(1. - x, 2)) / x;}

double observable_set::CS_QCD_P_gq(double x){return T_R * (pow(x, 2) + pow(1. - x, 2));}

double observable_set::CS_QCD_P_qq_reg(double x){return -C_F * (1. + x);}
// Allow for different conventions via switch: affects P_qq (apart from P_qq_reg)
double observable_set::CS_QCD_P_qq(double x){
  if (switch_convention_P_qq){return -C_F * (1. + x);}
  else {return 0.;}
}
double observable_set::CS_QCD_P_qq_plus(double x){
  if (switch_convention_P_qq){return C_F * 2. / (1. - x);}
  else {return C_F * (1. + pow(x, 2)) / (1. - x);}
}
//double observable_set::CS_QCD_P_qq_delta(){return 0.;}
double observable_set::CS_QCD_intP_qq_plus(double x){
  if (switch_convention_P_qq){return -C_F * 2 * log(1. - x);}
  else {return C_F * (-.5 * pow(x, 2) - x - 2 * log(1. - x));}
}
double observable_set::CS_QCD_P_gg(double x){return 2 * C_A * ((1. - x) / x - 1. + x * (1. - x));}
double observable_set::CS_QCD_P_gg_reg(double x){return 2 * C_A * ((1. - x) / x - 1. + x * (1. - x));}
double observable_set::CS_QCD_P_gg_plus(double x){return 2 * C_A / (1. - x);}
//double observable_set::CS_QCD_P_gg_delta(){return (11. / 6. * C_A - 2. / 3. * N_f * T_R);}
double observable_set::CS_QCD_intP_gg_plus(double x){return -2 * C_A * log(1. - x);}

// Kbar
double observable_set::CS_QCD_Kbar_qg(double x){return CS_QCD_P_qg(x) * log((1. - x) / x) + C_F * x;}

double observable_set::CS_QCD_Kbar_gq(double x){return CS_QCD_P_gq(x) * log((1. - x) / x) + 2 * T_R * x * (1. - x);}

double observable_set::CS_QCD_Kbar_qq(double x){return C_F * (-(1. + x) * log((1. - x) / x) + (1. - x));}
double observable_set::CS_QCD_Kbar_qq_plus(double x){return C_F * ((2. / (1. - x)) * log((1. - x) / x));}
//double observable_set::CS_QCD_Kbar_qq_delta(){return CS_QCD_Kbar_qq_delta;}
double observable_set::CS_QCD_intKbar_qq_plus(double x){return C_F * (-pow(log(1. - x), 2) - 2 * gsl_sf_dilog(1. - x) + pi2_3);}

double observable_set::CS_QCD_Kbar_gg(double x){return 2 * C_A * ((1. - x) / x - 1. + x * (1. - x)) * log((1. - x) / x);}
double observable_set::CS_QCD_Kbar_gg_plus(double x){return C_A * ((2 / (1. - x)) * log((1. - x) / x));}
//double observable_set::CS_QCD_Kbar_gg_delta(){return CS_QCD_Kbar_gg_delta;}
double observable_set::CS_QCD_intKbar_gg_plus(double x){return C_A * (-pow(log(1. - x), 2) - 2 * gsl_sf_dilog(1. - x) + pi2_3);}

// Kt
double observable_set::CS_QCD_Kt_qg(double x){return CS_QCD_P_qg(x) * log(1. - x);} // C_F -> [Q²_f]

double observable_set::CS_QCD_Kt_gq(double x){return CS_QCD_P_gq(x) * log(1. - x);} // T_R -> N_c * [Q²_f]

double observable_set::CS_QCD_Kt_qq(double x){return CS_QCD_P_qq_reg(x) * log(1. - x);} // C_F -> [Q²_q]
double observable_set::CS_QCD_Kt_qq_plus(double x){return C_F * (2 / (1. - x)) * log(1. - x);} // C_F -> [Q²_q]
//double observable_set::CS_QCD_Kt_qq_delta(){return CS_QCD_Kt_qq_delta;}
double observable_set::CS_QCD_intKt_qq_plus(double x){return -C_F * pow(log(1. - x), 2);} // C_F -> [Q²_q]

double observable_set::CS_QCD_Kt_gg(double x){return CS_QCD_P_gg_reg(x) * log(1. - x);}
double observable_set::CS_QCD_Kt_gg_plus(double x){return C_A * (2 / (1. - x)) * log(1. - x);}
//double observable_set::CS_QCD_Kt_gg_delta(){return CS_QCD_Kt_gg_delta;}
double observable_set::CS_QCD_intKt_gg_plus(double x){return -C_A * pow(log(1. - x), 2);}


