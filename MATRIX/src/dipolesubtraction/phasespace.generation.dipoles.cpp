#include "header.hpp"

void phasespace_set::ac_psp_RA_dipoles(){
  static Logger logger("phasespace_set::ac_psp_RA_dipoles");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (switch_off_RS_mapping){return;}

  if (csi->dipole[RA_x_a].type_dipole() == 1 && !switch_off_RS_mapping_ij_k){
    if (csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ij_k_massive(RA_x_a);}
    else {ac_psp_RA_group_ij_k(RA_x_a);}
  }
  else if (csi->dipole[RA_x_a].type_dipole() == 2 && !switch_off_RS_mapping_ij_a){
    if (csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ij_a_massive(dipole_sinx_min[RA_x_a], RA_x_a);}
    else {ac_psp_RA_group_ij_a(dipole_sinx_min[RA_x_a], RA_x_a);}
  }
  else if (csi->dipole[RA_x_a].type_dipole() == 3 && !switch_off_RS_mapping_ai_k){
    if (csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ai_k_massive(dipole_sinx_min[RA_x_a], RA_x_a);}
    else {ac_psp_RA_group_ai_k(dipole_sinx_min[RA_x_a], RA_x_a);}
  }
  else if (csi->dipole[RA_x_a].type_dipole() == 5){
    ac_psp_RA_group_ai_b(dipole_sinx_min[RA_x_a], RA_x_a);
  }

  /*
  if      (csi->dipole[RA_x_a].type_dipole() == 1 && csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ij_k_massive(RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 2 && csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ij_a_massive(dipole_sinx_min[RA_x_a], RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 3 && csi->dipole[RA_x_a].massive() == 1){ac_psp_RA_group_ai_k_massive(dipole_sinx_min[RA_x_a], RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 1 && csi->dipole[RA_x_a].massive() == 0){ac_psp_RA_group_ij_k(RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 2 && csi->dipole[RA_x_a].massive() == 0){ac_psp_RA_group_ij_a(dipole_sinx_min[RA_x_a], RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 3 && csi->dipole[RA_x_a].massive() == 0){ac_psp_RA_group_ai_k(dipole_sinx_min[RA_x_a], RA_x_a);}
  else if (csi->dipole[RA_x_a].type_dipole() == 5){ac_psp_RA_group_ai_b(dipole_sinx_min[RA_x_a], RA_x_a);}
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ag_psp_RA_dipoles(){
  static Logger logger("phasespace_set::ag_psp_RA_dipoles");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (switch_off_RS_mapping){return;}

  for (int i_a = 1; i_a < n_dipoles; i_a++){
    if (csi->dipole[i_a].type_dipole() == 1 && !switch_off_RS_mapping_ij_k){
      if (csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ij_k_massive(i_a);}
      else {ag_psp_RA_group_ij_k(i_a);}
    }
    else if (csi->dipole[i_a].type_dipole() == 2 && !switch_off_RS_mapping_ij_a){
      if (csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ij_a_massive(dipole_sinx_min[i_a], i_a);}
      else {ag_psp_RA_group_ij_a(dipole_sinx_min[i_a], i_a);}
    }
    else if (csi->dipole[i_a].type_dipole() == 3 && !switch_off_RS_mapping_ai_k){
      if (csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ai_k_massive(dipole_sinx_min[i_a], i_a);}
      else {ag_psp_RA_group_ai_k(dipole_sinx_min[i_a], i_a);}
    }
    else if (csi->dipole[i_a].type_dipole() == 5 && !switch_off_RS_mapping_ai_b){
      ag_psp_RA_group_ai_b(dipole_sinx_min[i_a], i_a);
    }

    /*
    if      (csi->dipole[i_a].type_dipole() == 1 && csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ij_k_massive(i_a);}
    else if (csi->dipole[i_a].type_dipole() == 2 && csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ij_a_massive(dipole_sinx_min[i_a], i_a);}
    else if (csi->dipole[i_a].type_dipole() == 3 && csi->dipole[i_a].massive() == 1){ag_psp_RA_group_ai_k_massive(dipole_sinx_min[i_a], i_a);}
    else if (csi->dipole[i_a].type_dipole() == 1 && csi->dipole[i_a].massive() == 0){ag_psp_RA_group_ij_k(i_a);}
    else if (csi->dipole[i_a].type_dipole() == 2 && csi->dipole[i_a].massive() == 0){ag_psp_RA_group_ij_a(dipole_sinx_min[i_a], i_a);}
    else if (csi->dipole[i_a].type_dipole() == 3 && csi->dipole[i_a].massive() == 0){ag_psp_RA_group_ai_k(dipole_sinx_min[i_a], i_a);}
    else if (csi->dipole[i_a].type_dipole() == 5){ag_psp_RA_group_ai_b(dipole_sinx_min[i_a], i_a);}
    */

  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
