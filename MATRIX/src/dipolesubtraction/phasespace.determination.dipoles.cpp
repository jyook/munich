#include "header.hpp"

void phasespace_set::determine_dipole_phasespace_RA(){
  Logger logger("phasespace_set::determine_dipole_phasespace_RA");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    logger << LOG_DEBUG_VERBOSE << "xbp_all[" << 0 << "][" << xbi << "] = " << xbp_all[0][xbi] << endl;
  }

  for (int i_a = 1; i_a < csi->dipole.size(); i_a++){
    logger << LOG_DEBUG_VERBOSE << "construct phasespace of dipole no. " << i_a << ": " << csi->dipole[i_a].name() << endl; //"   " << csi->dipole[i_a].xy << endl;

    if (csi->dipole[i_a].type_dipole() == 1){
      if (csi->dipole[i_a].massive() == 0){phasespace_ij_k(i_a);}
      else {phasespace_ij_k_massive(i_a);}
    }
    if (csi->dipole[i_a].type_dipole() == 2){
      if (csi->dipole[i_a].massive() == 0){phasespace_ij_a(i_a);}
      else {phasespace_ij_a_massive(i_a);}
    }
    if (csi->dipole[i_a].type_dipole() == 3){phasespace_ai_k(i_a);}
    if (csi->dipole[i_a].type_dipole() == 5){phasespace_ai_b(i_a);}
    logger << LOG_DEBUG_VERBOSE << "construct phasespace of dipole no. " << i_a << ": " << csi->dipole[i_a].name() << "   " << csi->dipole[i_a].xy << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



#define p_i xbp_all[0][bp_i]
#define p_j xbp_all[0][bp_j]
#define p_k xbp_all[0][bp_k]
#define pt_ij xbp_all[x_a][bpt_ij]
#define pt_k xbp_all[x_a][bpt_k]
#define y_ij_k csi->dipole[x_a].xy

void phasespace_set::phasespace_ij_k(int x_a){
  static Logger logger("phasespace_ij_k");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_i = csi->dipole[x_a].binary_R_emitter_1();
  int bp_j = csi->dipole[x_a].binary_R_emitter_2();
  int bp_k = csi->dipole[x_a].binary_R_spectator();
  int bpt_ij = csi->dipole[x_a].binary_A_emitter();
  int bpt_k = csi->dipole[x_a].binary_A_spectator();
  logger << LOG_DEBUG_VERBOSE << "bp_i   = " << bp_i << endl;
  logger << LOG_DEBUG_VERBOSE << "bp_j   = " << bp_j << endl;
  logger << LOG_DEBUG_VERBOSE << "bp_k   = " << bp_k << endl;
  logger << LOG_DEBUG_VERBOSE << "bpt_ij = " << bpt_ij << endl;
  logger << LOG_DEBUG_VERBOSE << "bpt_k  = " << bpt_k << endl;
  logger << LOG_DEBUG_VERBOSE << "y_ij_k = " << y_ij_k << endl;
  y_ij_k = (p_i * p_j) / (p_i * p_j + p_j * p_k + p_k * p_i);
  pt_ij = p_i + p_j - y_ij_k / (1. - y_ij_k) * p_k;
  pt_k = 1. / (1. - y_ij_k) * p_k;

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_i){}
    else if (xbi == bp_j){}
    else if (xbi == bp_k){}
    else if (xbi < bp_j){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi > bp_j){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi];}
    else {cout << "Should not happen!" << endl;}
  }
  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = xbs_all[0][0];
  xbsqrts_all[x_a][0] = xbsqrts_all[0][0];
  int xbn_all = xbp_all[x_a].size() - 4;
  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef p_i
#undef p_j
#undef p_k
#undef pt_ij
#undef pt_k
#undef y_ij_k


#define p_i xbp_all[0][bp_i]
#define p_j xbp_all[0][bp_j]
#define p_a xbp_all[0][bp_a]
#define pt_ij xbp_all[x_a][bpt_ij]
#define pt_a xbp_all[x_a][bpt_a]
#define x_ij_a csi->dipole[x_a].xy

void phasespace_set::phasespace_ij_a(int x_a){
  static Logger logger("phasespace_ij_a");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_i = csi->dipole[x_a].binary_R_emitter_1();
  int bp_j = csi->dipole[x_a].binary_R_emitter_2();
  int bp_a = csi->dipole[x_a].binary_R_spectator(); // bp_b not needed !!!
  int bpt_ij = csi->dipole[x_a].binary_A_emitter();
  int bpt_a = csi->dipole[x_a].binary_A_spectator(); // always bp_a == bpt_a

  x_ij_a = 1. - (p_i * p_j) / ((p_i + p_j) * p_a);
  pt_ij = p_i + p_j - (1 - x_ij_a) * p_a;
  pt_a = x_ij_a * p_a;

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_i){}
    else if (xbi == bp_j){}
    else if (xbi == bp_a){}
    else if (xbi < bp_j){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi > bp_j){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi];}
    else {cout << "Should not happen!" << endl;}
  }

  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = x_ij_a * xbs_all[0][0];
  xbsqrts_all[x_a][0] = sqrt(xbs_all[x_a][0]);
  int xbn_all = xbp_all[x_a].size() - 4;
  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef p_i
#undef p_j
#undef p_a
#undef pt_ij
#undef pt_a
#undef x_ij_a



#define p_a xbp_all[0][bp_a]
#define p_i xbp_all[0][bp_i]
#define p_k xbp_all[0][bp_k]
#define pt_ai xbp_all[x_a][bpt_ai]
#define pt_k xbp_all[x_a][bpt_k]
#define x_ik_a csi->dipole[x_a].xy

void phasespace_set::phasespace_ai_k(int x_a){
  static Logger logger("phasespace_ai_k");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_a = csi->dipole[x_a].binary_R_emitter_1();
  int bp_i = csi->dipole[x_a].binary_R_emitter_2();
  int bp_k = csi->dipole[x_a].binary_R_spectator();
  int bpt_ai = csi->dipole[x_a].binary_A_emitter();
  int bpt_k = csi->dipole[x_a].binary_A_spectator();

  x_ik_a = 1. - (p_i * p_k) / ((p_i + p_k) * p_a);
  //double u_i = (p_i * p_a) / ((p_i + p_k) * p_a);
  pt_k = p_i + p_k - (1 - x_ik_a) * p_a;
  pt_ai = x_ik_a * p_a;

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_i){}
    else if (xbi == bp_k){}
    else if (xbi == bp_a){}
    else if (xbi < bp_i){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi > bp_i){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi];}
    else {cout << "Should not happen!" << endl;}
  }

  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = x_ik_a * xbs_all[0][0];
  xbsqrts_all[x_a][0] = sqrt(xbs_all[x_a][0]);
  int xbn_all = xbp_all[x_a].size() - 4;
  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef p_i
#undef p_k
#undef p_a
#undef pt_ai
#undef pt_k
#undef x_ik_a



#define bpt_ai bp_a
#define bpt_b bp_b
#define p_a xbp_all[0][bp_a]
#define p_i xbp_all[0][bp_i]
#define p_b xbp_all[0][bp_b]
#define pt_ai xbp_all[x_a][bpt_ai]
#define pt_b xbp_all[x_a][bpt_b]
#define x_i_ab csi->dipole[x_a].xy

void phasespace_set::phasespace_ai_b(int x_a){
  static Logger logger("phasespace_ai_b");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_a = csi->dipole[x_a].binary_R_emitter_1();
  int bp_i = csi->dipole[x_a].binary_R_emitter_2();
  int bp_b = csi->dipole[x_a].binary_R_spectator();
  //  int bpt_ai = csi->dipole[x_a].binary_A_emitter(); // always bpt_ai == bp_a
  //  int bpt_b = csi->dipole[x_a].binary_A_spectator(); // always bpt_b == bp_b
  x_i_ab = 1. - (p_i * (p_a + p_b)) / (p_a * p_b);
  //  double v_i = (p_i * p_a) / (p_a * p_b);
  pt_ai = x_i_ab * p_a;
  fourvector K = p_a + p_b - p_i;
  fourvector Kt = pt_ai + p_b;
  fourvector KKt = K + Kt;
  double KKt2 = KKt.m2();
  double K2 = K.m2();
  //  cout << "phasespace_ai_b   xbp_all.size() = " << xbp_all.size() << endl;
  //  cout << "phasespace_ai_b   xbp_all[0].size() = " << xbp_all[0].size() << endl;
  //  cout << "phasespace_ai_b   xbp_all[" << x_a << "].size() = " << xbp_all[x_a].size() << endl;

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_a){}
    else if (xbi == bp_b){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi == bp_i){}
    else if (xbi < bp_i){xbp_all[x_a][xbi] = xbp_all[0][xbi] - ((2. * (xbp_all[0][xbi] * KKt)) / KKt2) * KKt + (2. * (xbp_all[0][xbi] * K) / K2) * Kt;}
    else if (xbi > bp_i){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi] - ((2. * (xbp_all[0][xbi] * KKt)) / KKt2) * KKt + (2. * (xbp_all[0][xbi] * K) / K2) * Kt;}
    else {cout << "Should not happen!" << endl;}
  }
  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = x_i_ab * xbs_all[0][0];
  xbsqrts_all[x_a][0] = sqrt(xbs_all[x_a][0]);
  int xbn_all = xbp_all[x_a].size() - 4;

  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef p_a
#undef p_i
#undef p_b
#undef pt_ai
#undef pt_b
#undef bpt_ai
#undef bpt_b
#undef x_i_ab



#define p_i xbp_all[0][bp_i]
#define p_j xbp_all[0][bp_j]
#define p_k xbp_all[0][bp_k]
#define pt_ij xbp_all[x_a][bpt_ij]
#define pt_k xbp_all[x_a][bpt_k]
#define m2_i xbs_all[0][bp_i]
#define m2_j xbs_all[0][bp_j]
#define m2_k xbs_all[0][bp_k]
#define m2_ij xbs_all[x_a][bpt_ij]
#define y_ij_k csi->dipole[x_a].xy

void phasespace_set::phasespace_ij_k_massive(int x_a){
  static Logger logger("phasespace_ij_k_massive");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_i = csi->dipole[x_a].binary_R_emitter_1();
  int bp_j = csi->dipole[x_a].binary_R_emitter_2();
  int bp_k = csi->dipole[x_a].binary_R_spectator();
  int bpt_ij = csi->dipole[x_a].binary_A_emitter();
  int bpt_k = csi->dipole[x_a].binary_A_spectator();
  /*
  logger << LOG_DEBUG_VERBOSE << "bp_i   = " << bp_i << endl;
  logger << LOG_DEBUG_VERBOSE << "bp_j   = " << bp_j << endl;
  logger << LOG_DEBUG_VERBOSE << "bp_k   = " << bp_k << endl;
  logger << LOG_DEBUG_VERBOSE << "bpt_ij = " << bpt_ij << endl;
  logger << LOG_DEBUG_VERBOSE << "bpt_k  = " << bpt_k << endl;
  logger << LOG_DEBUG_VERBOSE << "p_i   = " << xbp_all[0][bp_i] << endl;
  logger << LOG_DEBUG_VERBOSE << "p_j   = " << xbp_all[0][bp_j] << endl;
  logger << LOG_DEBUG_VERBOSE << "p_k   = " << xbp_all[0][bp_k] << endl;
  */
  fourvector Q = p_i + p_j + p_k;
  double Q2 = Q.m2();
  double pi_pj = p_i * p_j;
  /*
  logger << LOG_DEBUG_VERBOSE << "Q2    = " << Q2 << endl;
  logger << LOG_DEBUG_VERBOSE << "m2_i = " << m2_i << endl;
  logger << LOG_DEBUG_VERBOSE << "m2_j = " << m2_j << endl;
  logger << LOG_DEBUG_VERBOSE << "m2_ij = " << m2_ij << endl;
  logger << LOG_DEBUG_VERBOSE << "m2_k  = " << m2_k << endl;
  logger << LOG_DEBUG_VERBOSE << "pi_pj = " << pi_pj << endl;
  logger << LOG_DEBUG_VERBOSE << "lambda(Q2, m2_ij, m2_k) = " << lambda(Q2, m2_ij, m2_k) << endl;
  logger << LOG_DEBUG_VERBOSE << "lambda(Q2, m2_i + m2_j + 2 * pi_pj, m2_k) = " << lambda(Q2, m2_i + m2_j + 2 * pi_pj, m2_k) << endl;
  */
  pt_k = sqrt(lambda(Q2, m2_ij, m2_k) / lambda(Q2, m2_i + m2_j + 2 * pi_pj, m2_k)) * (p_k - ((Q * p_k) / Q2) * Q) + ((Q2 + m2_k - m2_ij) / (2 * Q2)) * Q;
  pt_ij = Q - pt_k;
  /*
  logger << LOG_DEBUG_VERBOSE << "Q     = " << Q << endl;
  logger << LOG_DEBUG_VERBOSE << "pt_k  = " << pt_k << endl;
  logger << LOG_DEBUG_VERBOSE << "pt_ij = " << pt_ij << endl;
  */
  // uncommented !!!
  y_ij_k = (p_i * p_j) / (p_i * p_j + p_j * p_k + p_k * p_i);
  //  logger << LOG_DEBUG_VERBOSE << "y_ij_k = " << y_ij_k << endl;
  /*
  pt_ij = p_i + p_j - y_ij_k / (1. - y_ij_k) * p_k;
  pt_k = 1. / (1. - y_ij_k) * p_k;
  */

  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_i){}
    else if (xbi == bp_j){}
    else if (xbi == bp_k){}
    else if (xbi < bp_j){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi > bp_j){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi];}
    else {cout << "Should not happen!" << endl;}
  }
  /*
  for (int xbi = 1; xbi < xbp_all[x_a].size(); xbi = xbi * 2){
    logger << LOG_DEBUG_VERBOSE << "xbp_all[" << x_a << "][" << xbi << "] = " << xbp_all[x_a][xbi] << endl;
  }
  */
  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = xbs_all[0][0];
  xbsqrts_all[x_a][0] = xbsqrts_all[0][0];
  int xbn_all = xbp_all[x_a].size() - 4;
  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef m2_i
#undef m2_j
#undef m2_k
#undef m2_ij
#undef p_i
#undef p_j
#undef p_k
#undef pt_ij
#undef pt_k
#undef y_ij_k



#define p_i xbp_all[0][bp_i]
#define p_j xbp_all[0][bp_j]
#define p_a xbp_all[0][bp_a]
#define pt_ij xbp_all[x_a][bpt_ij]
#define pt_a xbp_all[x_a][bpt_a]
#define m2_i xbs_all[0][bp_i]
#define m2_j xbs_all[0][bp_j]
#define m2_ij xbs_all[x_a][bpt_ij]
#define x_ij_a csi->dipole[x_a].xy

void phasespace_set::phasespace_ij_a_massive(int x_a){
  static Logger logger("phasespace_ij_a_massive");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int bp_i = csi->dipole[x_a].binary_R_emitter_1();
  int bp_j = csi->dipole[x_a].binary_R_emitter_2();
  int bp_a = csi->dipole[x_a].binary_R_spectator(); // bp_b not needed !!!
  int bpt_ij = csi->dipole[x_a].binary_A_emitter();
  int bpt_a = csi->dipole[x_a].binary_A_spectator(); // always bp_a == bpt_a

  double pi_pj = p_i * p_j;
  double pi_pa = p_i * p_a;
  double pj_pa = p_j * p_a;

  double one_minus_x_ij_a = (pi_pj - .5 * (m2_ij - m2_i - m2_j)) / (pi_pa + pj_pa);
  x_ij_a = 1. - one_minus_x_ij_a;
  pt_ij = p_i + p_j - one_minus_x_ij_a * p_a;
  pt_a = x_ij_a * p_a;

  /*
  x_ij_a = 1. - (p_i * p_j) / ((p_i + p_j) * p_a);
  pt_ij = p_i + p_j - (1 - x_ij_a) * p_a;
  pt_a = x_ij_a * p_a;
  */
  for (int xbi = 1; xbi < xbp_all[0].size(); xbi = xbi * 2){
    if      (xbi == bp_i){}
    else if (xbi == bp_j){}
    else if (xbi == bp_a){}
    else if (xbi < bp_j){xbp_all[x_a][xbi] = xbp_all[0][xbi];}
    else if (xbi > bp_j){xbp_all[x_a][xbi / 2] = xbp_all[0][xbi];}
    else {cout << "Should not happen!" << endl;}
  }

  xbp_all[x_a][0] = xbp_all[x_a][1] + xbp_all[x_a][2];
  xbs_all[x_a][0] = x_ij_a * xbs_all[0][0];
  xbsqrts_all[x_a][0] = sqrt(xbs_all[x_a][0]);
  int xbn_all = xbp_all[x_a].size() - 4;
  xbp_all[x_a][xbn_all] = xbp_all[x_a][0];
  xbs_all[x_a][xbn_all] = xbs_all[x_a][0];
  xbsqrts_all[x_a][xbn_all] = xbsqrts_all[x_a][0];
  /*
  for (int i = 0; i < osi_p_parton[x_a].size(); i++){
    int xbi = intpow(2, i - 1);
    osi_p_parton[x_a][i] = xbp_all[x_a][xbi];
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#undef x_ij_a
#undef m2_i
#undef m2_j
#undef m2_ij
#undef p_i
#undef p_j
#undef p_a
#undef pt_ij
#undef pt_a
#undef x_ij_a
