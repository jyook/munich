#include "header.hpp"

void observable_set::determine_integrand_CX_ncollinear_VT2(){
  static Logger logger("observable_set::determine_integrand_CX_ncollinear_VT2");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

#ifdef MORE
  static int initialization = 1;
  static int gg_initiated = 0;
  static int charged = 0;
  if (initialization){
    string initial_state = csi->subprocess.substr(0,3);
    if (initial_state == "uu~") {
      gg_initiated = -1;
    }
    else if (initial_state == "dd~") {
      gg_initiated = -2;
    }
    else if (initial_state == "bb~") {
      gg_initiated = -12;
    }
    else if (initial_state == "gg_") {
      gg_initiated = 1;
    }
    else if (initial_state == "du~") {
      charged = 1;
      gg_initiated = -1;
    }
    else if (initial_state == "ud~") {
      charged = 1;
      gg_initiated = -2;
    }
    else {
      assert(false && "initial state for resummation not known");
    }
    initialization = 0;
  }
#endif

  //  CX_value_integrand_qTcut  must be redefined !!!

  double global_factor = pow(alpha_S, 2) / pi2;

  calculate_A_F();
  // only with resummation ???
  calculate_B2_A_F();
  determine_splitting_tH1F_tH1();
  ///  should be replaced by the following two contributions:
  ///  determine_splitting_tH1F_tH1_without_H1_delta(psi);
  //// -> coll_tH1F_contribution[no_xx/no_zx][...]
  ///  determine_splitting_tH1_only_H1_delta(psi);
  //// -> coll_tH1_only_H1_delta_contribution[no_xx][...]
  ////  new  coll_tH1  is accompanied by  coll_tH1_only_H1_delta  and does not contain respective terms any longer !!!
  determine_splitting_tgaga_tcga_tgamma2();
  determine_splitting_tH2();
  ///  contains H1 contributions that might have to be split...

  double LQ = log(psi->xbs_all[0][0] / pow(QT_Qres, 2));
  if (QT_Qres == 0){LQ = 0.;}

  for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
    for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
      value_LR[i_vr][i_mr] = log(psi->xbs_all[0][0] / pow(value_mu_ren[i_vr][i_mr], 2));
    }
  }

  for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
    for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
      value_LF[i_vf][i_mf] = log(psi->xbs_all[0][0] / pow(value_mu_fact[i_vf][i_mf], 2));
      for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	coll_tH1F_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tH1F[i_l];
	coll_tH1_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tH1[i_l];
	//// to be introduced: additional term and coll_tH1 without H1 terms !!!
	////	coll_tH1_only_H1_delta_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tH1_only_H1_delta[i_l];
	coll_tgaga_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tgaga[i_l];
	coll_tcga_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tcga[i_l];
	coll_tgamma2_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tgamma2[i_l];
	coll_tH2_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tH2[i_l];
	//// modification in coll_tH2 needed ??? Split off H1 terms and replace by loopcc terms ???
      }
      value_tH1F[i_vf][i_mf] = accumulate(coll_tH1F_pdf[i_vf][i_mf].begin(), coll_tH1F_pdf[i_vf][i_mf].end(), 0.);
      value_tH1[i_vf][i_mf] = accumulate(coll_tH1_pdf[i_vf][i_mf].begin(), coll_tH1_pdf[i_vf][i_mf].end(), 0.);
      //// to be introduced: additional term and coll_tH1 without H1 terms !!!
      ////      value_tH1_only_H1_delta[i_vf][i_mf] = accumulate(coll_tH1_only_H1_delta_pdf[i_vf][i_mf].begin(), coll_tH1_only_H1_delta_pdf[i_vf][i_mf].end(), 0.);
      value_sig11[i_vf][i_mf] = calculate_sigma11(value_list_pdf_factor[i_vf][i_mf][0][0], value_tH1F[i_vf][i_mf], LQ);
      value_tgaga[i_vf][i_mf] = accumulate(coll_tgaga_pdf[i_vf][i_mf].begin(), coll_tgaga_pdf[i_vf][i_mf].end(), 0.);
      value_tcga[i_vf][i_mf] = accumulate(coll_tcga_pdf[i_vf][i_mf].begin(), coll_tcga_pdf[i_vf][i_mf].end(), 0.);
      value_tgamma2[i_vf][i_mf] = accumulate(coll_tgamma2_pdf[i_vf][i_mf].begin(), coll_tgamma2_pdf[i_vf][i_mf].end(), 0.);
      value_tH2[i_vf][i_mf] = accumulate(coll_tH2_pdf[i_vf][i_mf].begin(), coll_tH2_pdf[i_vf][i_mf].end(), 0.);
      /*
      // The following term does not exist in H2_gg, but in H2_qqbar !!! -> must be shifted to tH2(qqx) !!!
      value_tH2[i_vf][i_mf] += QT_H1_delta * (value_tH1[i_vf][i_mf] - QT_H1_delta * value_list_pdf_factor[i_vf][i_mf][0][0]);
      */
      for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
	if (i_vr != i_vf){continue;} // CV variation does not allow for different dynamic scales
	for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
	  value_H1full[i_vr][i_vf][i_mr][i_mf] = calculate_H1(value_list_pdf_factor[i_vf][i_mf][0][0], value_tH1[i_vf][i_mf], value_tH1F[i_vf][i_mf], value_LR[i_vr][i_mr], value_LF[i_vf][i_mf], LQ);
	  //// to be introduced: New  calculate_H1  call based on tH1 and tH1_only_H1_delta !!!
	  value_H2full[i_vr][i_vf][i_mr][i_mf] = calculate_H2(value_list_pdf_factor[i_vf][i_mf][0][0], value_tH1[i_vf][i_mf], value_tH1F[i_vf][i_mf], value_H1full[i_vr][i_vf][i_mr][i_mf], value_sig11[i_vf][i_mf], value_tgaga[i_vf][i_mf], value_tcga[i_vf][i_mf], value_tgamma2[i_vf][i_mf], value_tH2[i_vf][i_mf], value_LR[i_vr][i_mr], value_LF[i_vf][i_mf], LQ);
	}
      }

      if (switch_distribution){
	for (int i_y = 0; i_y < 3; i_y++){
	  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	    coll_tH1F_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tH1F[i_l];
	    coll_tH1_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tH1[i_l];
	    coll_tgaga_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tgaga[i_l];
	    coll_tcga_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tcga[i_l];
	    coll_tgamma2_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tgamma2[i_l];
	    coll_tH2_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tH2[i_l];
	  }
	  value_tH1F_D[i_vf][i_mf][i_y] = accumulate(coll_tH1F_pdf_D[i_vf][i_mf][i_y].begin(), coll_tH1F_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_tH1_D[i_vf][i_mf][i_y] = accumulate(coll_tH1_pdf_D[i_vf][i_mf][i_y].begin(), coll_tH1_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_sig11_D[i_vf][i_mf][i_y] = calculate_sigma11(value_list_pdf_factor[i_vf][i_mf][0][i_y], value_tH1F_D[i_vf][i_mf][i_y], LQ);
	  value_tgaga_D[i_vf][i_mf][i_y] = accumulate(coll_tgaga_pdf_D[i_vf][i_mf][i_y].begin(), coll_tgaga_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_tcga_D[i_vf][i_mf][i_y] = accumulate(coll_tcga_pdf_D[i_vf][i_mf][i_y].begin(), coll_tcga_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_tgamma2_D[i_vf][i_mf][i_y] = accumulate(coll_tgamma2_pdf_D[i_vf][i_mf][i_y].begin(), coll_tgamma2_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_tH2_D[i_vf][i_mf][i_y] = accumulate(coll_tH2_pdf_D[i_vf][i_mf][i_y].begin(), coll_tH2_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  /*
	  // The following term does not exist in H2_gg, but in H2_qqbar !!! -> must be shifted to tH2(qqx) !!!
	  value_tH2_D[i_vf][i_mf][i_y] += QT_H1_delta * (value_tH1_D[i_vf][i_mf][i_y] - QT_H1_delta * value_list_pdf_factor[i_vf][i_mf][0][i_y]);
	  */
	  for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
	    if (i_vr != i_vf){continue;} // CV variation does not allow for different dynamic scales
	    for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
	      value_H1full_D[i_vr][i_vf][i_mr][i_mf][i_y] = calculate_H1(value_list_pdf_factor[i_vf][i_mf][0][i_y], value_tH1_D[i_vf][i_mf][i_y], value_tH1F_D[i_vf][i_mf][i_y], value_LR[i_vr][i_mr], value_LF[i_vf][i_mf], LQ);
	      value_H2full_D[i_vr][i_vf][i_mr][i_mf][i_y] = calculate_H2(value_list_pdf_factor[i_vf][i_mf][0][i_y], value_tH1_D[i_vf][i_mf][i_y], value_tH1F_D[i_vf][i_mf][i_y], value_H1full_D[i_vr][i_vf][i_mr][i_mf][i_y], value_sig11_D[i_vf][i_mf][i_y], value_tgaga_D[i_vf][i_mf][i_y], value_tcga_D[i_vf][i_mf][i_y], value_tgamma2_D[i_vf][i_mf][i_y], value_tH2_D[i_vf][i_mf][i_y], value_LR[i_vr][i_mr], value_LF[i_vf][i_mf], LQ);
	    }
	  }
	}
      }
    }
  }


  if (switch_resummation){
    // !!!
#ifdef MORE
    double Q = sqrt(psi->xbs_all[0][0]);
    double y = log(sqrt(psi->x_pdf[1] / psi->x_pdf[2]));
    double muF = var_mu_fact;
    double muRen = var_mu_ren;
    int channel = 0;
    double sqrt_shad = 2 * psi->E;
    double qt = sqrt(psi->QT_qt2);
    //  performQTBoost(psi->QT_qt2, Q, y, particle_event);
    logger << LOG_DEBUG << "Q=" << Q << ", " << "y=" << y << ", " << "qt=" << qt << ", x1=" << x_pdf[1] << ", x2=" << x_pdf[2] << endl;

    double temp = 0.;
    if (abs(y) > -0.5 * log(pow(somescale_.startscale / sqrt_shad, 2))){cout << "y too large, skipping point" << endl;}
    else {
      resumm_(&temp, &gg_initiated, &channel, &Q, &y, &qt, &QT_Qres, &muF, &muRen, &sqrt_shad, &VA_b_ME2, &QT_H1_delta, &QT_H2_delta);
      if (munich_isnan(temp)){temp = 0.; cout << "CX_value_integrand_RF_qTcut[0][i_vr][i_vf][i_mr][i_mf] is nan, setting to zero!" << endl;}
    }
    for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
      for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
	for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
	  if (i_vr != i_vf){continue;} // CV variation does not allow for different dynamic scales
	  for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	    CX_value_integrand_RF_qTcut[0][i_vr][i_vf][i_mr][i_mf] = temp;
	  }
	}
      }
    }

    integrand = psi->ps_factor * rescaling_factor_alpha_e * CX_value_integrand_RF_qTcut[0][dynamic_scale][dynamic_scale][map_value_scale_ren][map_value_scale_fact] / x_pdf[0] * psi->QT_jacqt2 / 2 / qt;
    if (switch_CV){
      for (int i_s = 0; i_s < n_scales_CV; i_s++){
	integrand_CV[i_s] = psi->ps_factor * rescaling_factor_alpha_e * CX_value_integrand_RF_qTcut[0][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]] / x_pdf[0] * psi->QT_jacqt2 / 2 / qt;
      }
    }

    if (switch_distribution){
      for (int i_y = 0; i_y < 3; i_y++){
        integrand_D[i_y][0] = psi->ps_factor * rescaling_factor_alpha_e * CX_value_integrand_RF_qTcut[0][dynamic_scale][dynamic_scale][map_value_scale_ren][map_value_scale_fact] / x_pdf[0] * psi->QT_jacqt2 / 2 / qt;
	if (i_y > 0){integrand_D[i_y][0] /= 2;}  //         TODO: asymmetric distributions not implemented !!! (refers to issues pointed out before...)

        if (switch_CV){
          for (int i_s = 0; i_s < n_scales_CV; i_s++){
            integrand_D_CV[i_s][i_y][0] = psi->ps_factor * rescaling_factor_alpha_e * CX_value_integrand_RF_qTcut[0][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]] / x_pdf[0] * psi->QT_jacqt2 / 2 / qt;
	    if (i_y > 0){integrand_D_CV[i_s][i_y][0] /= 2;}  //         TODO: asymmetric distributions not implemented !!! (refers to issues pointed out before...)
         }
        }
      }
    }
#endif
  }
  else {
    for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
      for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
	  if (i_vr != i_vf){continue;} // CV variation does not allow for different dynamic scales
	  for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
	    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	      CX_value_integrand_RF_qTcut[i_q][i_vr][i_vf][i_mr][i_mf] = global_factor * value_H2full[i_vr][i_vf][i_mr][i_mf];
	      if (switch_distribution){
		for (int i_y = 0; i_y < 3; i_y++){
		  CX_value_integrand_RF_qTcut_D[i_q][i_vr][i_vf][i_mr][i_mf][i_y] = global_factor * value_H2full_D[i_vr][i_vf][i_mr][i_mf][i_y];
		}
	      }
	    }
	  }
	}
      }
    }

    int x_q = 0;
    integrand = var_rel_alpha_S * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut[x_q][dynamic_scale][dynamic_scale][map_value_scale_ren][map_value_scale_fact];
    if (switch_CV){
      for (int i_s = 0; i_s < n_scales_CV; i_s++){
	integrand_CV[i_s] = var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut[x_q][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]];
	for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	  integrand_qTcut_CV[i_q][i_s] = var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut[i_q][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]];
	}
      }
    }

    if (switch_distribution){
      for (int i_y = 0; i_y < 3; i_y++){
	integrand_D[i_y][0] = var_rel_alpha_S * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut_D[x_q][dynamic_scale][dynamic_scale][map_value_scale_ren][map_value_scale_fact][i_y];
	if (switch_CV){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    integrand_D_CV[i_s][i_y][0] = var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut_D[x_q][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]][i_y];
	    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	      integrand_D_qTcut_CV[i_q][i_s][i_y][0] = var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * VA_b_ME2 * CX_value_integrand_RF_qTcut_D[i_q][dynamic_scale_CV][dynamic_scale_CV][map_value_scale_ren_CV[i_s]][map_value_scale_fact_CV[i_s]][i_y];
	    }
	  }
	}
      }
    }
  }


  if (switch_TSV){
    for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
      for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	value_LR_TSV[i_vr][i_mr] = log(psi->xbs_all[0][0] / pow(value_scale_ren[0][i_vr][i_mr], 2));
      }
    }
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	value_LF_TSV[i_vf][i_mf] = log(psi->xbs_all[0][0] / pow(value_scale_fact[0][i_vf][i_mf], 2));
	for (int i_y = 0; i_y < 3; i_y++){
	  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	    coll_tH1F_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tH1F[i_l];
	    coll_tH1_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tH1[i_l];
	    coll_tgaga_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tgaga[i_l];
	    coll_tcga_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tcga[i_l];
	    coll_tgamma2_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tgamma2[i_l];
	    coll_tH2_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tH2[i_l];
	  }
	  value_tH1F_TSV[i_vf][i_mf][i_y] = accumulate(coll_tH1F_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tH1F_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_tH1_TSV[i_vf][i_mf][i_y] = accumulate(coll_tH1_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tH1_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_sig11_TSV[i_vf][i_mf][i_y] = calculate_sigma11(value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y], value_tH1F_TSV[i_vf][i_mf][i_y], LQ);
	  value_tgaga_TSV[i_vf][i_mf][i_y] = accumulate(coll_tgaga_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tgaga_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_tcga_TSV[i_vf][i_mf][i_y] = accumulate(coll_tcga_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tcga_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_tgamma2_TSV[i_vf][i_mf][i_y] = accumulate(coll_tgamma2_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tgamma2_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_tH2_TSV[i_vf][i_mf][i_y] = accumulate(coll_tH2_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tH2_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
	    for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	      value_H1full_TSV[i_vr][i_vf][i_mr][i_mf][i_y] = calculate_H1(value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y], value_tH1_TSV[i_vf][i_mf][i_y], value_tH1F_TSV[i_vf][i_mf][i_y], value_LR_TSV[i_vr][i_mr], value_LF_TSV[i_vf][i_mf], LQ);
	      value_H2full_TSV[i_vr][i_vf][i_mr][i_mf][i_y] = calculate_H2(value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y], value_tH1_TSV[i_vf][i_mf][i_y], value_tH1F_TSV[i_vf][i_mf][i_y], value_H1full_TSV[i_vr][i_vf][i_mr][i_mf][i_y], value_sig11_TSV[i_vf][i_mf][i_y], value_tgaga_TSV[i_vf][i_mf][i_y], value_tcga_TSV[i_vf][i_mf][i_y], value_tgamma2_TSV[i_vf][i_mf][i_y], value_tH2_TSV[i_vf][i_mf][i_y], value_LR_TSV[i_vr][i_mr], value_LF_TSV[i_vf][i_mf], LQ);
	    }
	  }
	  /*
	  // The following term does not exist in H2_gg, but in H2_qqbar !!! -> must be shifted to tH2(qqx) !!!
	  value_tH2_TSV[i_vf][i_mf][i_y] += QT_H1_delta * (value_tH1_TSV[i_vf][i_mf][i_y] - QT_H1_delta * value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y]);
	  logger << LOG_DEBUG << "tH2 extra term (involving H1²) = " << QT_H1_delta * (value_tH1_TSV[i_vf][i_mf][i_y] - QT_H1_delta * value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y]) << endl;
	  */
	}
      }
    }

    
    if (switch_resummation){
#ifdef MORE
      double Q = sqrt(psi->xbs_all[0][0]);
      double y = log(sqrt(psi->x_pdf[1] / psi->x_pdf[2]));
      double muF = var_mu_fact;
      double muRen = var_mu_ren;
      //      int gg_initiated;
      int channel = 0;
      double sqrt_shad = 2 * psi->E;
      double qt = sqrt(psi->QT_qt2);

      double temp = 0.;
      if (abs(y) > -0.5 * log(pow(somescale_.startscale / sqrt_shad, 2))){cout << "y too large, skipping point" << endl;}
      else {
	resumm_(&temp, &gg_initiated, &channel, &Q, &y, &qt, &QT_Qres, &muF, &muRen, &sqrt_shad, &VA_b_ME2, &QT_H1_delta, &QT_H2_delta);
	if (munich_isnan(temp)){temp = 0.; cout << "value_integrand_qTcut_TSV[i_q][i_vr][i_vf][i_mr][i_mf][i_y] is nan, setting to zero!" << endl;}
      }
      for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
	for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	  for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
	    for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	      for (int i_y = 0; i_y < 3; i_y++){
		// check i_q dependence !!!
		for (int i_q = 0; i_q < output_n_qTcut; i_q++){
		  // Most likely, psi->ps_factor * rescaling_factor_alpha_e needs to be removed !!! What about global_factor, or VA_b_ME2?
		  value_integrand_qTcut_TSV[i_q][i_vr][i_vf][i_mr][i_mf][i_y] = psi->ps_factor * rescaling_factor_alpha_e * temp / x_pdf[0] * psi->QT_jacqt2 / 2 / qt;
		}
	      }
	    }
	  }
	}
      }
#endif
    }
    else {
      for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
	for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	  for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
	    for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	      for (int i_y = 0; i_y < 3; i_y++){
		for (int i_q = 0; i_q < output_n_qTcut; i_q++){
		  value_integrand_qTcut_TSV[i_q][i_vr][i_vf][i_mr][i_mf][i_y] = global_factor * VA_b_ME2 * value_H2full_TSV[i_vr][i_vf][i_mr][i_mf][i_y];
		}
	      }
	    }
	  }
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#define FrII right << setw(2)
#define FldXV setprecision(15) << setw(23) << left
#define Fcw left << setw(13)
#define FcpCVw left << setw(22)
#define FvCVw left << setw(14)
#define FcpTSVw left << setw(29)
#define FvTSVw left << setw(18)

string observable_set::output_ME2_VT2_QCD(){
  static Logger logger("observable_set::output_ME2_VT2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  stringstream out_comparison;
  out_comparison << "Coefficients in VT2 contribution (qT subtraction):" << endl;
  out_comparison << endl;
  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
    if (list_combination_pdf[i_l].size() > 0){
      out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
      out_comparison << Fcw << "" << "[nc]" << endl;
      out_comparison << Fcw << "coll_tH1F" << "[" << FrII << i_l << "] = " << FldXV << coll_tH1F[i_l] << endl;
      out_comparison << Fcw << "coll_tH1" << "[" << FrII << i_l << "] = " << FldXV << coll_tH1[i_l] << endl;
      //      out_comparison << Fcw << "coll_tH1_oH1" << "[" << FrII << i_l << "] = " << FldXV << coll_tH1_only_H1_delta[i_l]  << endl;
      out_comparison << Fcw << "coll_tgaga" << "[" << FrII << i_l << "] = " << FldXV << coll_tgaga[i_l] << endl;
      out_comparison << Fcw << "coll_tcga" << "[" << FrII << i_l << "] = " << FldXV << coll_tcga[i_l] << endl;
      out_comparison << Fcw << "coll_tgamma2" << "[" << FrII << i_l << "] = " << FldXV << coll_tgamma2[i_l] << endl;
      out_comparison << Fcw << "coll_tH2" << "[" << FrII << i_l << "] = " << FldXV << coll_tH2[i_l] << endl;
      out_comparison << endl;
    }
  }
  out_comparison << endl;
  if (switch_CV){
    out_comparison << "CV variation" << endl << endl;
    for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
      if (list_combination_pdf[i_l].size() > 0){
	out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
	for (int j_l = 0; j_l < list_combination_pdf[i_l].size(); j_l++){
	  stringstream temp_ss;
	  temp_ss << "PDF[" << FrII << j_l << "] (";
	  if (list_combination_pdf[i_l][j_l][0][0] == 1){temp_ss << "+";} else {temp_ss << "-";}
	  temp_ss << ") -> ";
	  for (int k_l = 0; k_l < list_combination_pdf[i_l][j_l].size(); k_l++){
	    if (k_l > 0 && k_l % 10 == 0){temp_ss << endl << setw(15) << "";}
	    stringstream temp_pair;
	    temp_pair << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][1]] << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][2]] << " ";
	    temp_ss << left << setw(5) << temp_pair.str();
	  }
	  out_comparison << temp_ss.str() << endl;
 	}
	out_comparison << endl;
	for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
	  for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	    out_comparison << "µ_F = " << value_mu_fact[i_vf][i_mf] << " = " << value_mu_fact_rel[i_vf][i_mf] << " x µ_" << i_vf << endl;
	    out_comparison << FcpCVw << "" << "[µF][xF][nc]" << endl;
	    out_comparison << FcpCVw << "value_list_pdf_factor" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "][-] = " << FldXV << value_list_pdf_factor[i_vf][i_mf][i_l][0] << endl;
	    out_comparison << FcpCVw << "coll_tH1F_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tH1F_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << FcpCVw << "coll_tH1_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tH1_pdf[i_vf][i_mf][i_l] << endl;
	    //	    out_comparison << FcpCVw << "coll_tH1_oH1_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tH1_only_H1_delta_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << FcpCVw << "coll_tgaga_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tgaga_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << FcpCVw << "coll_tcga_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tcga_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << FcpCVw << "coll_tgamma2_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tgamma2_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << FcpCVw << "coll_tH2_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tH2_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << endl;
	  }
	}
      }
    }
    out_comparison << endl;
    for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
      for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	for (int i_vr = 0; i_vr < value_mu_ren.size(); i_vr++){
	  if (i_vr != i_vf){continue;} // CV variation does not allow for different dynamic scales
	  for (int i_mr = 0; i_mr < value_mu_ren[i_vr].size(); i_mr++){
	    out_comparison << "µ_F = " << value_mu_fact[i_vf][i_mf] << " = " << value_mu_fact_rel[i_vf][i_mf] << " x µ_" << i_vf << "   "
			   << "µ_R = " << value_mu_ren[i_vr][i_mr] << " = " << value_mu_ren_rel[i_vr][i_mr] << " x µ_" << i_vr << endl;
	    out_comparison << FvCVw << "" << "[µR][µF][xR][xF]" << endl;
	    out_comparison << FvCVw << "QT_H1_delta" << setw(16) << "" << " = " << FldXV << QT_H1_delta << endl;
	    out_comparison << FvCVw << "QT_H2_delta" << setw(16) << "" << " = " << FldXV << QT_H2_delta << endl;
	    out_comparison << FvCVw << "A_F" << setw(16) << "" << " = " << FldXV << A_F << endl;
	    out_comparison << FvCVw << "B2_A_F" << setw(16) << "" << " = " << FldXV << B2_A_F << endl;
	    out_comparison << FvCVw << "value_LR" << "[" << FrII << i_vr << "]    [" << FrII << i_mr << "]     = " << FldXV << value_LR[i_vr][i_mr] << endl;
	    out_comparison << FvCVw << "value_LF" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_LF[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_sig11" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_sig11[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_tH1F" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH1F[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_tH1" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH1[i_vf][i_mf] << endl;
	    //	  out_comparison << FvCVw << "value_tH1_oH1" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH1_only_H1_delta[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_H1full" << "[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "] = " << FldXV << value_H1full[i_vr][i_vf][i_mr][i_mf] << endl;
	    out_comparison << FvCVw << "value_tgaga" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tgaga[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_tcga" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tcga[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_tgamma2" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tgamma2[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_tH2" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH2[i_vf][i_mf] << endl;
	    out_comparison << FvCVw << "value_H2full[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "] = " << FldXV << value_H2full[i_vr][i_vf][i_mr][i_mf] << endl;
	    out_comparison << endl;
	  }
	}
      }
    }
    out_comparison << endl;
  }
  if (switch_TSV){
    out_comparison << "TSV variation" << endl << endl;
    for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
      if (list_combination_pdf[i_l].size() > 0){
	out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
	for (int j_l = 0; j_l < list_combination_pdf[i_l].size(); j_l++){
	  stringstream temp_ss;
	  temp_ss << "PDF[" << FrII << j_l << "] (";
	  if (list_combination_pdf[i_l][j_l][0][0] == 1){temp_ss << "+";} else {temp_ss << "-";}
	  temp_ss << ") -> ";
	  for (int k_l = 0; k_l < list_combination_pdf[i_l][j_l].size(); k_l++){
	    if (k_l > 0 && k_l % 10 == 0){temp_ss << endl << setw(15) << "";}
	    stringstream temp_pair;
	    temp_pair << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][1]] << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][2]] << " ";
	    temp_ss << left << setw(5) << temp_pair.str();
	  }
	  out_comparison << temp_ss.str() << endl;
	}
	out_comparison << endl;
	for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
	  for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	    out_comparison << "µ_F = " << value_scale_fact[0][i_vf][i_mf] << " = " << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << endl;
	    out_comparison << FcpTSVw << "" << "[µF][xF]   [nc]" << endl;
	    out_comparison << FcpTSVw << "value_list_pdf_factor_TSV [-]" << "[" << FrII << i_vf << "][" << FrII << i_mf << "]   [" << FrII << i_l << "][-] = " << FldXV << value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][0] << endl;
	    out_comparison << FcpTSVw << "coll_tH1F_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tH1F_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << FcpTSVw << "coll_tH1_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tH1_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    //	    out_comparison << FcpTSVw << "coll_tH1_oH1_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tH1_only_H1_delta_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << FcpTSVw << "coll_tgaga_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tgaga_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << FcpTSVw << "coll_tcga_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tcga_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << FcpTSVw << "coll_tgamma2_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tgamma2_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << FcpTSVw << "coll_tH2_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tH2_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << endl;
	  }
	}
      }
    }
    out_comparison << endl;
    out_comparison << "TSV variation - long format" << endl << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
	  for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	    out_comparison << "µ_F = " << value_scale_fact[0][i_vf][i_mf] << " = " << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << "   "
			   << "µ_R = " << value_scale_ren[0][i_vr][i_mr] << " = " << value_relative_scale_ren[i_vr][i_mr] << " x µ_" << i_vr << endl;
	    out_comparison << FvTSVw << "" << "[µR][µF][xR][xF]" << endl;
	    out_comparison << FvTSVw << "QT_H1_delta" << setw(16) << "" << "    = " << QT_H1_delta << endl;
	    out_comparison << FvTSVw << "QT_H2_delta" << setw(16) << "" << "    = " << QT_H2_delta << endl;
	    out_comparison << FvTSVw << "A_F" << setw(16) << "" << "    = " << A_F << endl;
	    out_comparison << FvTSVw << "B2_A_F" << setw(16) << "" << "    = " << B2_A_F << endl;
	    out_comparison << FvTSVw << "value_LR_TSV" << "[" << FrII << i_vr << "]    [" << FrII << i_mr << "]        = " << FldXV << value_LR_TSV[i_vr][i_mr] << endl;
	    out_comparison << FvTSVw << "value_LF_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "]    = " << FldXV << value_LF_TSV[i_vf][i_mf] << endl;
	    out_comparison << FvTSVw << "value_sig11_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_sig11_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tH1_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH1_TSV[i_vf][i_mf][0] << endl;
	    //	    out_comparison << FvTSVw << "value_tH1_oH1_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH1_only_H1_delta_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tH1F_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH1F_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_H1full_TSV" << "[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "][-] = " << FldXV << value_H1full_TSV[i_vr][i_vf][i_mr][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tgaga_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tgaga_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tcga_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tcga_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tgamma2_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tgamma2_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_tH2_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH2_TSV[i_vf][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_H2full_TSV" << "[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "] = " << FldXV << value_H2full_TSV[i_vr][i_vf][i_mr][i_mf][0] << endl;
	    out_comparison << endl;
   	  }
	}
      }
    }
    out_comparison << endl;

    out_comparison << "TSV variation - compact format" << endl << endl;
    out_comparison << FvTSVw << "" << "[µR][µF][xR][xF]" << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << "µ_F                   [" << FrII << i_vf << "]    [" << FrII << i_mf << "]    = " << FldXV << value_scale_fact[0][i_vf][i_mf] << " = " << setw(5) << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << endl;
      }
    }
    for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
      for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	out_comparison << "µ_R               [" << FrII << i_vr << "]    [" << FrII << i_mr << "]        = " << FldXV << value_scale_ren[0][i_vr][i_mr] << " = " << setw(5) << value_relative_scale_ren[i_vr][i_mr] << " x µ_" << i_vr << endl;
      }
    }
    out_comparison << FvTSVw << "QT_H1_delta" << setw(16) << "" << "    = " << QT_H1_delta << endl;
    out_comparison << FvTSVw << "QT_H2_delta" << setw(16) << "" << "    = " << QT_H2_delta << endl;
    out_comparison << FvTSVw << "A_F" << setw(16) << "" << "    = " << A_F << endl;
    out_comparison << FvTSVw << "B2_A_F" << setw(16) << "" << "    = " << B2_A_F << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << FvTSVw << "value_LF_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "]    = " << FldXV << value_LF_TSV[i_vf][i_mf] << endl;
      }
    }
    for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
      for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	out_comparison << FvTSVw << "value_LR_TSV" << "[" << FrII << i_vr << "]    [" << FrII << i_mr << "]        = " << FldXV << value_LR_TSV[i_vr][i_mr] << endl;
      }
    }
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << FvTSVw << "value_sig11_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_sig11_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tH1_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH1_TSV[i_vf][i_mf][0] << endl;
	//	    out_comparison << FvTSVw << "value_tH1_oH1_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "] = " << FldXV << value_tH1_only_H1_delta_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tH1F_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH1F_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tgaga_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tgaga_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tcga_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tcga_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tgamma2_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tgamma2_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_tH2_TSV" << "    [" << FrII << i_vf << "]    [" << FrII << i_mf << "][-] = " << FldXV << value_tH2_TSV[i_vf][i_mf][0] << endl;
	for (int i_vr = 0; i_vr < max_dyn_ren + 1; i_vr++){
	  for (int i_mr = 0; i_mr < n_scale_dyn_ren[i_vr]; i_mr++){
	    out_comparison << FvTSVw << "value_H1full_TSV" << "[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "][-] = " << FldXV << value_H1full_TSV[i_vr][i_vf][i_mr][i_mf][0] << endl;
	    out_comparison << FvTSVw << "value_H2full_TSV" << "[" << FrII << i_vr << "][" << FrII << i_vf << "][" << FrII << i_mr << "][" << FrII << i_mf << "] = " << FldXV << value_H2full_TSV[i_vr][i_vf][i_mr][i_mf][0] << endl;
   	  }
	}
      }
    }
  }
  out_comparison << endl;

  logger << LOG_DEBUG_VERBOSE << "finished - return (string):" << endl << out_comparison.str() << endl;
  return out_comparison.str();
}
