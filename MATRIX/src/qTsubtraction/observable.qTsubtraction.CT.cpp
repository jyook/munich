#include "header.hpp"

void observable_set::determine_integrand_CX_ncollinear_CT(){
  static Logger logger("observable_set::determine_integrand_CX_ncollinear_CT_QCD");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  static int order = 1;

  double qtoverq = sqrt(psi->QT_qt2 / QT_Qres / QT_Qres);
  double LL1, LL2, LL3, LL4;
  if (switch_resummation){Itilde(qtoverq, order, LL1, LL2, LL3, LL4);}
  // in FO computations, we do the qT integrals once and for all in the beginning
  // in resummed computations, we have to bin the "artificial" qT, so that is not easily possible -> revert back to old implementation

  double global_factor = alpha_S / pi;

  determine_splitting_tH1F();

  double LQ = log(psi->xbs_all[0][0] / pow(QT_Qres, 2));
  if (QT_Qres == 0){LQ = 0.;}

  for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
    for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
      for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	coll_tH1F_pdf[i_vf][i_mf][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][0] * coll_tH1F[i_l];
      }
      value_tH1F[i_vf][i_mf] = accumulate(coll_tH1F_pdf[i_vf][i_mf].begin(), coll_tH1F_pdf[i_vf][i_mf].end(), 0.);
      value_sig12[i_vf][i_mf] = calculate_sigma12(value_list_pdf_factor[i_vf][i_mf][0][0]);
      value_sig11[i_vf][i_mf] = calculate_sigma11(value_list_pdf_factor[i_vf][i_mf][0][0], value_tH1F[i_vf][i_mf], LQ);

      if (switch_distribution){
	for (int i_y = 0; i_y < 3; i_y++){
	  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	    coll_tH1F_pdf_D[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor[i_vf][i_mf][i_l][i_y] * coll_tH1F[i_l];
	  }
	  value_tH1F_D[i_vf][i_mf][i_y] = accumulate(coll_tH1F_pdf_D[i_vf][i_mf][i_y].begin(), coll_tH1F_pdf_D[i_vf][i_mf][i_y].end(), 0.);
	  value_sig12_D[i_vf][i_mf][i_y] = calculate_sigma12(value_list_pdf_factor[i_vf][i_mf][0][i_y]);
	  value_sig11_D[i_vf][i_mf][i_y] = calculate_sigma11(value_list_pdf_factor[i_vf][i_mf][0][i_y], value_tH1F_D[i_vf][i_mf][i_y], LQ);
	}
      }
    }
  }

  if (switch_resummation){
    for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
      for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	for (int i_q = 0; i_q < n_qTcut; i_q++){
	  if (i_q <= esi->cut_ps[0]){
	    CX_value_integrand_qTcut[i_q][i_vf][i_mf] = global_factor * (value_sig12[i_vf][i_mf] * LL2 + value_sig11[i_vf][i_mf] * LL1) / pow(QT_Qres,2) * psi->QT_jacqt2;
	  }
	  else {CX_value_integrand_qTcut[i_q][i_vf][i_mf] = 0.;}
	  if (switch_distribution){
	    for (int i_y = 0; i_y < 3; i_y++){
              if (i_q <= esi->cut_ps[0]){
		CX_value_integrand_qTcut_D[i_q][i_vf][i_mf][i_y] = global_factor * (value_sig12_D[i_vf][i_mf][i_y] * LL2 + value_sig11_D[i_vf][i_mf][i_y] * LL1) / pow(QT_Qres,2) * psi->QT_jacqt2;
              }
              else {CX_value_integrand_qTcut_D[i_q][i_vf][i_mf][i_y] = 0.;}
	    }
	  }
	}
      }
    }
  }
  else {
    for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
      for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	for (int i_q = 0; i_q < n_qTcut; i_q++){
	  CX_value_integrand_qTcut[i_q][i_vf][i_mf] = global_factor * (value_sig12[i_vf][i_mf] * I2_int[i_q] + value_sig11[i_vf][i_mf] * I1_int[i_q]);
	  if (switch_distribution){
	    for (int i_y = 0; i_y < 3; i_y++){
              CX_value_integrand_qTcut_D[i_q][i_vf][i_mf][i_y] = global_factor * (value_sig12_D[i_vf][i_mf][i_y] * I2_int[i_q] + value_sig11_D[i_vf][i_mf][i_y] * I1_int[i_q]);
	    }
	  }
	}
      }
    }
  }

  integrand = -var_rel_alpha_S * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut[0][dynamic_scale][map_value_scale_fact];

  if (switch_CV){
    for (int i_s = 0; i_s < n_scales_CV; i_s++){
      integrand_CV[i_s] = -var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut[0][dynamic_scale_CV][map_value_scale_fact_CV[i_s]];
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	integrand_qTcut_CV[i_q][i_s] = -var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut[i_q][dynamic_scale_CV][map_value_scale_fact_CV[i_s]];
      }
    }
  }

  if (switch_distribution){
    int x_q = 0;
    for (int i_y = 0; i_y < 3; i_y++){
      integrand_D[i_y][0] = -var_rel_alpha_S * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut_D[x_q][dynamic_scale][map_value_scale_fact][i_y];
      if (switch_CV){
	for (int i_s = 0; i_s < n_scales_CV; i_s++){
	  integrand_D_CV[i_s][i_y][0] = -var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut_D[x_q][dynamic_scale_CV][map_value_scale_fact_CV[i_s]][i_y];
	  for (int i_q = 0; i_q < n_qTcut; i_q++){
	    integrand_D_qTcut_CV[i_q][i_s][i_y][0] = -var_rel_alpha_S_CV[i_s] * psi->ps_factor * rescaling_factor_alpha_e * ME2 * CX_value_integrand_qTcut_D[i_q][dynamic_scale_CV][map_value_scale_fact_CV[i_s]][i_y];
	  }
	}
      }
    }
  }


  if (switch_TSV){
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	for (int i_y = 0; i_y < 3; i_y++){
	  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
	    coll_tH1F_pdf_TSV[i_vf][i_mf][i_y][i_l] = value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][i_y] * coll_tH1F[i_l];
	  }
	  value_tH1F_TSV[i_vf][i_mf][i_y] = accumulate(coll_tH1F_pdf_TSV[i_vf][i_mf][i_y].begin(), coll_tH1F_pdf_TSV[i_vf][i_mf][i_y].end(), 0.);
	  value_sig12_TSV[i_vf][i_mf][i_y] = calculate_sigma12(value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y]);
	  value_sig11_TSV[i_vf][i_mf][i_y] = calculate_sigma11(value_list_pdf_factor_TSV[0][i_vf][i_mf][0][i_y], value_tH1F_TSV[i_vf][i_mf][i_y], LQ);

	  for (int i_q = 0; i_q < n_qTcut; i_q++){
	    if (switch_resummation){
	      if (i_q <= esi->cut_ps[0]){
		value_fact_integrand_qTcut_TSV[i_q][i_vf][i_mf][i_y] = -ME2 * global_factor * (value_sig12_TSV[i_vf][i_mf][i_y] * LL2 + value_sig11_TSV[i_vf][i_mf][i_y] * LL1) / pow(QT_Qres,2) * psi->QT_jacqt2;
	      }
	      else {value_fact_integrand_qTcut_TSV[i_q][i_vf][i_mf][i_y] = 0.;}
	    }
	    else {
	      value_fact_integrand_qTcut_TSV[i_q][i_vf][i_mf][i_y] = -ME2 * global_factor * (value_sig12_TSV[i_vf][i_mf][i_y] * I2_int[i_q] + value_sig11_TSV[i_vf][i_mf][i_y] * I1_int[i_q]);
	    }
	  }
	}
      }
    }
  }

  // shifted elsewhere ???

  //#ifdef MORE
  //  if (switch_resummation){
  //    double Q = sqrt(psi->xbs_all[0][0]);
  //    double y = log(sqrt(psi->x_pdf[1] / psi->x_pdf[2]));
  //    performQTBoost(psi->QT_qt2, Q, y, particle_event);
  //  }
  //#endif

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#define FrII right << setw(2)
#define FldXV setprecision(15) << setw(23) << left
#define Fcw left << setw(13)
#define FcpCVw left << setw(22)
#define FvCVw left << setw(14)
#define FcpTSVw left << setw(29)
#define FvTSVw left << setw(18)

string observable_set::output_ME2_CT_QCD(){
  static Logger logger("observable_set::output_ME2_CT_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  stringstream out_comparison;
  out_comparison << "Coefficients in CT contribution (qT subtraction):" << endl;
  out_comparison << endl;
  for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
    if (list_combination_pdf[i_l].size() > 0){
      out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
      out_comparison << Fcw << "" << "[nc]" << endl;
      out_comparison << Fcw << "coll_tH1F" << "[" << FrII << i_l << "] = " << FldXV << coll_tH1F[i_l] << endl;
      out_comparison << endl;
    }
  }
  out_comparison << endl;
  if (switch_CV){
    out_comparison << "CV variation" << endl << endl;
    for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
      if (list_combination_pdf[i_l].size() > 0){
	out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
	for (int j_l = 0; j_l < list_combination_pdf[i_l].size(); j_l++){
	  stringstream temp_ss;
	  temp_ss << "PDF[" << FrII << j_l << "] (";
	  if (list_combination_pdf[i_l][j_l][0][0] == 1){temp_ss << "+";} else {temp_ss << "-";}
	  temp_ss << ") -> ";
	  for (int k_l = 0; k_l < list_combination_pdf[i_l][j_l].size(); k_l++){
	    if (k_l > 0 && k_l % 10 == 0){temp_ss << endl << setw(15) << "";}
	    stringstream temp_pair;
	    temp_pair << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][1]] << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][2]] << " ";
	    temp_ss << left << setw(5) << temp_pair.str();
	  }
	  out_comparison << temp_ss.str() << endl;
 	}
	out_comparison << endl;
	for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
	  for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	    out_comparison << "µ_F = " << value_mu_fact[i_vf][i_mf] << " = " << value_mu_fact_rel[i_vf][i_mf] << " x µ_" << i_vf << endl;
	    out_comparison << FcpCVw << "" << "[µF][xF][nc]" << endl;
	    out_comparison << FcpCVw << "value_list_pdf_factor" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "][-] = " << FldXV << value_list_pdf_factor[i_vf][i_mf][i_l][0] << endl;
	    out_comparison << FcpCVw << "coll_tH1F_pdf" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][" << FrII << i_l << "]    = " << FldXV << coll_tH1F_pdf[i_vf][i_mf][i_l] << endl;
	    out_comparison << endl;
	  }
	}
      }
    }
    out_comparison << endl;
    for (int i_vf = 0; i_vf < value_mu_fact.size(); i_vf++){
      for (int i_mf = 0; i_mf < value_mu_fact[i_vf].size(); i_mf++){
	out_comparison << "µ_F = " << value_mu_fact[i_vf][i_mf] << " = " << value_mu_fact_rel[i_vf][i_mf] << " x µ_" << i_vf << endl;
	out_comparison << FvCVw << "" << "[µF][xF]" << endl;
	out_comparison << FvCVw << "value_sig11" << "[" << FrII << i_vf << "][" << FrII << i_mf << "] = " << FldXV << value_sig11[i_vf][i_mf] << endl;
	out_comparison << FvCVw << "value_sig12" << "[" << FrII << i_vf << "][" << FrII << i_mf << "] = " << FldXV << value_sig12[i_vf][i_mf] << endl;
	out_comparison << FvCVw << "value_tH1F" << "[" << FrII << i_vf << "][" << FrII << i_mf << "] = " << FldXV << value_tH1F[i_vf][i_mf] << endl;
	out_comparison << endl;
      }
    }
    out_comparison << endl;
  }
  if (switch_TSV){
    out_comparison << "TSV variation" << endl << endl;
    for (int i_l = 0; i_l < list_combination_pdf.size(); i_l++){
      if (list_combination_pdf[i_l].size() > 0){
	out_comparison << "Contribution from ncollinear[" << FrII << i_l << "] = " << ncollinear[i_l].name << endl;
	for (int j_l = 0; j_l < list_combination_pdf[i_l].size(); j_l++){
	  stringstream temp_ss;
	  temp_ss << "PDF[" << FrII << j_l << "] (";
	  if (list_combination_pdf[i_l][j_l][0][0] == 1){temp_ss << "+";} else {temp_ss << "-";}
	  temp_ss << ") -> ";
	  for (int k_l = 0; k_l < list_combination_pdf[i_l][j_l].size(); k_l++){
	    if (k_l > 0 && k_l % 10 == 0){temp_ss << endl << setw(15) << "";}
	    stringstream temp_pair;
	    temp_pair << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][1]] << csi->name_particle[list_combination_pdf[i_l][j_l][k_l][2]] << " ";
	    temp_ss << left << setw(5) << temp_pair.str();
	  }
	  out_comparison << temp_ss.str() << endl;
	}
	out_comparison << endl;
	for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
	  for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	    out_comparison << "µ_F = " << value_scale_fact[0][i_vf][i_mf] << " = " << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << endl;
	    out_comparison << FcpTSVw << "" << "[µF][xF]   [nc]" << endl;
	    out_comparison << FcpTSVw << "value_list_pdf_factor_TSV [-]" << "[" << FrII << i_vf << "][" << FrII << i_mf << "]   [" << FrII << i_l << "][-] = " << FldXV << value_list_pdf_factor_TSV[0][i_vf][i_mf][i_l][0] << endl;
	    out_comparison << FcpTSVw << "coll_tH1F_pdf_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-][" << FrII << i_l << "]    = " << FldXV << coll_tH1F_pdf_TSV[i_vf][i_mf][0][i_l] << endl;
	    out_comparison << endl;
	  }
	}
      }
    }
    out_comparison << endl;
    out_comparison << "TSV variation - long format" << endl << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << "µ_F = " << value_scale_fact[0][i_vf][i_mf] << " = " << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << endl;
	out_comparison << FvTSVw << "" << "[µF][xF]" << endl;
	out_comparison << FvTSVw << "value_tH1F_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_tH1F_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_sig11_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_sig12_TSV[i_vf][i_mf][0] << endl;
	out_comparison << FvTSVw << "value_sig12_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_sig11_TSV[i_vf][i_mf][0] << endl;
      }
    }
    out_comparison << endl;
    out_comparison << "TSV variation - compact format" << endl << endl;
    out_comparison << FvTSVw << "" << "[µF][xF]" << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << "µ_F               [" << FrII << i_vf << "][" << FrII << i_mf << "]    = " << FldXV << value_scale_fact[0][i_vf][i_mf] << " = " << setw(5) << value_relative_scale_fact[i_vf][i_mf] << " x µ_" << i_vf << endl;
      }
    }
    out_comparison << endl;
    for (int i_vf = 0; i_vf < max_dyn_fact + 1; i_vf++){
      for (int i_mf = 0; i_mf < n_scale_dyn_fact[i_vf]; i_mf++){
	out_comparison << FvTSVw << "value_tH1F_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_tH1F_TSV[i_vf][i_mf][0] << endl;
 	out_comparison << FvTSVw << "value_sig11_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_sig11_TSV[i_vf][i_mf][0] << endl;
 	out_comparison << FvTSVw << "value_sig12_TSV" << "[" << FrII << i_vf << "][" << FrII << i_mf << "][-] = " << FldXV << value_sig12_TSV[i_vf][i_mf][0] << endl;
      }
    }
  }
  out_comparison << endl;

  logger << LOG_DEBUG_VERBOSE << "finished - return (string):" << endl << out_comparison.str() << endl;
  return out_comparison.str();
}
