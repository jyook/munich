#include "header.hpp"

void contribution_set::initialization(inputparameter_set * isi){
  static  Logger logger("contribution_set::initialization");
  logger << LOG_DEBUG << "called" << endl;

  initialization_set_default();
  initialization_input(isi);

  // Only if subprocess has certain value ???
  
  readin_hadronic_process();
  if (subprocess != ""){readin_subprocess();}

  determination_order_alpha_s_born();
  determination_class_contribution();


  logger << LOG_DEBUG << "finished" << endl;
}


void contribution_set::initialization_set_default(){
  static  Logger logger("contribution_set::initialization_set_default");
  logger << LOG_DEBUG << "called" << endl;

  // Shifted from constructor
  switch_to_newscheme = 0;
  
  fill_code_particle();
  fill_name_particle();
  fill_pdg_charge();

  basic_process_class = "";
  process_class = "";
  decay.resize(0);
  type_perturbative_order = "";
  type_contribution = "";
  type_correction = "";
  contribution_order_alpha_s = 0;
  contribution_order_alpha_e = 0;
  contribution_order_interference = 0;

  symmetry_factor = 1;
  symmetry_id_factor.resize(1);

    
  basic_subprocess = "";
  subprocess = "";

  type_hadron.clear();
  basic_type_hadron.clear();
  
  type_parton.resize(1);
  //  basic_type_parton ???
  
  n_particle = 0;
  process_type = 0;

  order_alpha_s_born = 0;
  n_jet_born = 0;
  n_particle_born = 0;

 
  for (int j_l = 1; j_l < 7; j_l++){
    lepton_exchange[10 + j_l] = 10 + j_l;
    lepton_exchange[-10 - j_l] = -10 - j_l;
  }
  //  run_mode = "";

  int_end = 0;
  
  logger << LOG_DEBUG << "finished" << endl;
}


void contribution_set::initialization_input(inputparameter_set * isi){
  static  Logger logger("contribution_set::initialization_input");
  logger << LOG_DEBUG << "called" << endl;

  path_MUNICH = isi->path_MUNICH;

  //  basic_process_class = isi->basic_process_class;
  //  subprocess = isi->subprocess;
  
 // Special input (selection of contribution to be collected):
  if (isi->present_type_perturbative_order > -1){type_perturbative_order = isi->collection_type_perturbative_order[isi->present_type_perturbative_order];}
  if (isi->present_type_contribution > -1){type_contribution = isi->collection_type_contribution[isi->present_type_contribution];}
  if (isi->present_type_correction > -1){type_correction = isi->collection_type_correction[isi->present_type_correction];}

  //presumably not the correct implementation !!!

  int temp_type_perturbative_order = -1;
  int temp_type_contribution = -1;
  int temp_type_correction = -1;

  for (size_t i_i = 0; i_i < isi->input_contribution.size(); i_i++){
    logger << LOG_DEBUG << "input_contribution[" << setw(3) << i_i << "] -> " << left << setw(32) << isi->input_contribution[i_i].variable << " = " << setw(32) << isi->input_contribution[i_i].value << endl;

    if (isi->input_contribution[i_i].variable == ""){}
    
    ///////////////////////////////////////////////////////////////////
    //  very basic parameters: shell/file input and input from main  //
    ///////////////////////////////////////////////////////////////////
    
    else if (isi->input_contribution[i_i].variable == "basic_process_class"){basic_process_class = isi->input_contribution[i_i].value;}
    else if (isi->input_contribution[i_i].variable == "subprocess"){subprocess = isi->input_contribution[i_i].value;}

    //////////////////////////////////////////////////////////////
    //  selection of process and contribution to be calculated  //
    //////////////////////////////////////////////////////////////

    else if (isi->input_contribution[i_i].variable == "process_class"){process_class = isi->input_contribution[i_i].value;}
    else if (isi->input_contribution[i_i].variable == "decay"){decay.push_back(isi->input_contribution[i_i].value.c_str());}

    //////////////////////////////////////////////////////////////
    //  selection of process and contribution to be calculated  //
    //////////////////////////////////////////////////////////////

    else if (isi->input_contribution[i_i].variable == "type_perturbative_order"){
      temp_type_perturbative_order = -1;
      for (int i_to = 0; i_to < isi->collection_type_perturbative_order.size(); i_to++){
	if (isi->input_contribution[i_i].value == "all"){temp_type_perturbative_order = isi->present_type_perturbative_order; break;}
	else if (isi->input_contribution[i_i].value == isi->collection_type_perturbative_order[i_to]){temp_type_perturbative_order = i_to; break;}
      }
      logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_contribution[i_i].variable << " = " << setw(32) << isi->input_contribution[i_i].value << setw(32) << "temp_type_perturbative_order = " << temp_type_perturbative_order << endl;
    }
    else if (isi->input_contribution[i_i].variable == "type_contribution"){
      temp_type_contribution = -1;
      for (int i_tc = 0; i_tc < isi->collection_type_contribution.size(); i_tc++){
	if (isi->input_contribution[i_i].value == "all"){temp_type_contribution = isi->present_type_contribution; break;}
	else if (isi->input_contribution[i_i].value == isi->collection_type_contribution[i_tc]){temp_type_contribution = i_tc; break;}
      }
      logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_contribution[i_i].variable << " = " << setw(32) << isi->input_contribution[i_i].value << setw(32) << "temp_type_contribution = " << temp_type_contribution << endl;
    }
    else if (isi->input_contribution[i_i].variable == "type_correction"){
      temp_type_correction = -1;
      for (int i_tc = 0; i_tc < isi->collection_type_correction.size(); i_tc++){
	if (isi->input_contribution[i_i].value == "all"){temp_type_correction = isi->present_type_correction; break;}
	else if (isi->input_contribution[i_i].value == isi->collection_type_correction[i_tc]){temp_type_correction = i_tc; break;}
      }
      logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_contribution[i_i].variable << " = " << setw(32) << isi->input_contribution[i_i].value << setw(32) << "temp_type_correction = " << temp_type_correction << endl;
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    //  the following input is only used if the 'present' contribution                   //
    //  matches the 'temp' contribution selected at the respective point in input file:  //
    // - type_perturbative_order                                                         //
    // - type_contribution                                                               //
    // - type_correction                                                                 //
    ///////////////////////////////////////////////////////////////////////////////////////

    else if ((temp_type_perturbative_order == isi->present_type_perturbative_order && 
	      temp_type_contribution == isi->present_type_contribution && 
	      temp_type_correction == isi->present_type_correction)){
      logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << left << setw(32) << isi->input_contribution[i_i].variable << " = " << setw(32) << isi->input_contribution[i_i].value << endl;

      //////////////////////////////////////////////////
      //  selection of contribution to be calculated  //
      //////////////////////////////////////////////////

      if      (isi->input_contribution[i_i].variable == "contribution_order_alpha_s"){contribution_order_alpha_s = atoi(isi->input_contribution[i_i].value.c_str());}
      else if (isi->input_contribution[i_i].variable == "contribution_order_alpha_e"){contribution_order_alpha_e = atoi(isi->input_contribution[i_i].value.c_str());}
      else if (isi->input_contribution[i_i].variable == "contribution_order_interference"){contribution_order_interference = atoi(isi->input_contribution[i_i].value.c_str());}

    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}






