#include "header.hpp"

void contribution_set::initialization_contribution_order(){
  Logger logger("contribution_set::initialization_contribution_order");
  logger << LOG_DEBUG << "started" << endl;

  logger << LOG_DEBUG << "The same information is (was ??? ) also contained in observable_set !!!" << endl;

  if (type_contribution == "born" || 
      type_contribution == "L2I" || 
      type_contribution == "loop"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
    phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
    phasespace_order_interference.resize(1, contribution_order_interference);
  }
  else if (type_contribution == "CA" || 
	   type_contribution == "L2CA"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "MIX"){
      logger << LOG_FATAL << "Contribution CA.MIX is not defined!" << endl;
      exit(1);
    }
  }
  else if (type_contribution == "VA" || 
	   type_contribution == "L2VA"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "MIX"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
      logger << LOG_INFO << "Phase-space is taken according to involved QEW correction! (could be improved later...)" << endl;
    }
  }
  else if (type_contribution == "RA" || 
	   type_contribution == "L2RA"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
    phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
    phasespace_order_interference.resize(1, contribution_order_interference);
    logger << LOG_INFO << "Information on dipole phase-spaces is supplied after dipole determination..." << endl;
  }

  else if (type_contribution == "VT" || 
	   type_contribution == "VJ" || 
	   type_contribution == "L2VT" || 
	   type_contribution == "L2VJ" || 
	   type_contribution == "NLL_LO" || 
	   type_contribution == "NLL_NLO" || 
	   type_contribution == "NNLL_LO" || 
	   type_contribution == "NNLL_NLO"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "CT" || 
	   type_contribution == "CJ" || 
	   type_contribution == "L2CT" || 
	   type_contribution == "L2CJ"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "RT" || 
	   type_contribution == "L2RT" ||
	   type_contribution == "RJ" || 
	   type_contribution == "L2RJ"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
    phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
    phasespace_order_interference.resize(1, contribution_order_interference);
  }
  else if (type_contribution == "VT2" || 
	   type_contribution == "VJ2" || 
	   type_contribution == "NNLL_NNLO"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 2);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "MIX"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 2);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "CT2" ||
	   type_contribution == "CJ2"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 2);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "MIX"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 2);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "RVA" ||
	   type_contribution == "RVJ"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "RCA" ||
	   type_contribution == "RCJ"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    if (type_correction == "QCD"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s - 1);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
    else if (type_correction == "QEW"){
      phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
      phasespace_order_alpha_e.resize(1, contribution_order_alpha_e - 1);
      phasespace_order_interference.resize(1, contribution_order_interference);
    }
  }
  else if (type_contribution == "RRA" ||
	   type_contribution == "RRJ"){
    /*
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
    */
    phasespace_order_alpha_s.resize(1, contribution_order_alpha_s);
    phasespace_order_alpha_e.resize(1, contribution_order_alpha_e);
    phasespace_order_interference.resize(1, contribution_order_interference);
    logger << LOG_INFO << "Information on dipole phase-spaces is supplied after dipole determination..." << endl;
  }
  else {
    logger << LOG_FATAL << "Contribution " << type_contribution << "." << type_correction << " has not been defined yet!" << endl;
      exit(1);
  }

  /*
  // for all contributions !!!
    contribution_order_alpha_s.resize(1, contribution_order_alpha_s);
    contribution_order_alpha_e.resize(1, contribution_order_alpha_e);
    contribution_order_interference.resize(1, contribution_order_interference);
  */
  /*
  contribution_order_alpha_s = contribution_order_alpha_s;
  contribution_order_alpha_e = contribution_order_alpha_e;
  contribution_order_interference = contribution_order_interference;
  */
  logger << LOG_DEBUG << "before contribution output" << endl;

  logger << LOG_DEBUG << "process_class = " << process_class << endl;
  logger << LOG_DEBUG << "subprocess = " << subprocess << endl;
  logger << LOG_DEBUG << "type_perturbative_order = " << type_perturbative_order << endl;
  logger << LOG_DEBUG << "type_contribution = " << type_contribution << endl;
  logger << LOG_DEBUG << "type_correction = " << type_correction << endl;
  
  logger << LOG_DEBUG << "contribution_order_alpha_s = " << contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "contribution_order_alpha_e = " << contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "contribution_order_interference = " << contribution_order_interference << endl;
  /* // was doubled !!!
  logger << LOG_DEBUG << "contribution_order_alpha_s = " << contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "contribution_order_alpha_e = " << contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "contribution_order_interference = " << contribution_order_interference << endl;
  */
  logger << LOG_DEBUG << "phasespace_order_alpha_s = " << phasespace_order_alpha_s[0] << endl;
  logger << LOG_DEBUG << "phasespace_order_alpha_e = " << phasespace_order_alpha_e[0] << endl;
  logger << LOG_DEBUG << "phasespace_order_interference = " << phasespace_order_interference[0] << endl;
  
  logger << LOG_DEBUG << "after contribution output" << endl;

  // strange location !!!
  
  no_process_parton.resize(1);
  swap_parton.resize(1);
  //  no_prc.resize(1);
  //  o_prc.resize(1);
  

  logger << LOG_DEBUG << "finished" << endl;
}


