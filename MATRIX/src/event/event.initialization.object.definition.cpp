#include "header.hpp"

void event_set::determine_object_definition(){
  static  Logger logger("event_set::determine_object_definition");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << endl << "Object definition after definition (fully inclusive): " << endl;
  logger << LOG_DEBUG << setw(5) << "no" << setw(20) << "object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    //    logger << LOG_DEBUG << setw(5) << i << setw(20) << object_list[i] << setw(20) << n_observed_min[i] << setw(20) << n_observed_max[i] << setw(20) << define_pT[i] << setw(20) << define_ET[i] << setw(20) << define_eta[i] << setw(20) << define_y[i] << endl;
    logger << LOG_DEBUG << setw(5) << i << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << setw(20) << pda[i].define_pT << setw(20) << pda[i].define_ET << setw(20) << pda[i].define_eta << setw(20) << pda[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);

  // adapt define_pT (tjet-top/atop) !

  if (pda[observed_object["top"]].define_pT == 0. && pda[observed_object["tjet"]].define_pT > 0.){pda[observed_object["top"]].define_pT = pda[observed_object["tjet"]].define_pT;}
  if (pda[observed_object["atop"]].define_pT == 0. && pda[observed_object["tjet"]].define_pT > 0.){pda[observed_object["atop"]].define_pT = pda[observed_object["tjet"]].define_pT;}
  if (pda[observed_object["tjet"]].define_pT < pda[observed_object["top"]].define_pT &&
      pda[observed_object["tjet"]].define_pT < pda[observed_object["atop"]].define_pT){
    if (pda[observed_object["top"]].define_pT >= pda[observed_object["atop"]].define_pT){pda[observed_object["tjet"]].define_pT = pda[observed_object["atop"]].define_pT;}
    else {pda[observed_object["tjet"]].define_pT = pda[observed_object["top"]].define_pT;}
  }

  // adapt define_pT (bjet-bjet_b/bjet_bx) !

  if (pda[observed_object["bjet_b"]].define_pT == 0. && pda[observed_object["bjet"]].define_pT > 0.){pda[observed_object["bjet_b"]].define_pT = pda[observed_object["bjet"]].define_pT;}
  if (pda[observed_object["bjet_bx"]].define_pT == 0. && pda[observed_object["bjet"]].define_pT > 0.){pda[observed_object["bjet_bx"]].define_pT = pda[observed_object["bjet"]].define_pT;}
  if (pda[observed_object["bjet"]].define_pT < pda[observed_object["bjet_b"]].define_pT &&
      pda[observed_object["bjet"]].define_pT < pda[observed_object["bjet_bx"]].define_pT){
    if (pda[observed_object["bjet_b"]].define_pT >= pda[observed_object["bjet_bx"]].define_pT){pda[observed_object["bjet"]].define_pT = pda[observed_object["bjet_bx"]].define_pT;}
    else {pda[observed_object["bjet"]].define_pT = pda[observed_object["bjet_b"]].define_pT;}
  }
  /*
  // adapt define_pT (jet-ljet/bjet) !
  if (pda[observed_object["ljet"]].define_pT == 0. && pda[observed_object["jet"]].define_pT > 0.){pda[observed_object["ljet"]].define_pT = pda[observed_object["jet"]].define_pT;}
  if (pda[observed_object["bjet"]].define_pT == 0. && pda[observed_object["jet"]].define_pT > 0.){pda[observed_object["bjet"]].define_pT = pda[observed_object["jet"]].define_pT;}

  if (pda[observed_object["jet"]].define_pT == 0. && pda[observed_object["bjet"]].define_pT == 0. && pda[observed_object["ljet"]].define_pT > 0.){pda[observed_object["jet"]].define_pT = pda[observed_object["ljet"]].define_pT;}
  if (pda[observed_object["jet"]].define_pT == 0. && pda[observed_object["ljet"]].define_pT == 0. && pda[observed_object["bjet"]].define_pT > 0.){pda[observed_object["jet"]].define_pT = pda[observed_object["bjet"]].define_pT;}

  if (pda[observed_object["jet"]].define_pT < pda[observed_object["ljet"]].define_pT &&
      pda[observed_object["jet"]].define_pT < pda[observed_object["bjet"]].define_pT){
    if (pda[observed_object["ljet"]].define_pT >= pda[observed_object["bjet"]].define_pT){pda[observed_object["jet"]].define_pT = pda[observed_object["bjet"]].define_pT;}
    else {pda[observed_object["jet"]].define_pT = pda[observed_object["ljet"]].define_pT;}
  }
  */
  // adapt define_pT (lep-lm/lp) !
  if (pda[observed_object["lm"]].define_pT == 0. && pda[observed_object["lep"]].define_pT > 0.){pda[observed_object["lm"]].define_pT = pda[observed_object["lep"]].define_pT;}
  if (pda[observed_object["lp"]].define_pT == 0. && pda[observed_object["lep"]].define_pT > 0.){pda[observed_object["lp"]].define_pT = pda[observed_object["lep"]].define_pT;}
  if (pda[observed_object["lep"]].define_pT < pda[observed_object["lm"]].define_pT &&
      pda[observed_object["lep"]].define_pT < pda[observed_object["lp"]].define_pT){
    if (pda[observed_object["lm"]].define_pT >= pda[observed_object["lp"]].define_pT){pda[observed_object["lep"]].define_pT = pda[observed_object["lp"]].define_pT;}
    else {pda[observed_object["lep"]].define_pT = pda[observed_object["lm"]].define_pT;}
  }

  // adapt define_pT (lep-e/mu/tau) !
  if (pda[observed_object["e"]].define_pT == 0. && pda[observed_object["lep"]].define_pT > 0.){pda[observed_object["e"]].define_pT = pda[observed_object["lep"]].define_pT;}
  if (pda[observed_object["mu"]].define_pT == 0. && pda[observed_object["lep"]].define_pT > 0.){pda[observed_object["mu"]].define_pT = pda[observed_object["lep"]].define_pT;}
  if (pda[observed_object["tau"]].define_pT == 0. && pda[observed_object["lep"]].define_pT > 0.){pda[observed_object["tau"]].define_pT = pda[observed_object["lep"]].define_pT;}
  if (pda[observed_object["lep"]].define_pT < pda[observed_object["e"]].define_pT &&
      pda[observed_object["lep"]].define_pT < pda[observed_object["mu"]].define_pT &&
      pda[observed_object["lep"]].define_pT < pda[observed_object["tau"]].define_pT){
    if (pda[observed_object["tau"]].define_pT >= pda[observed_object["e"]].define_pT &&
	pda[observed_object["mu"]].define_pT >= pda[observed_object["e"]].define_pT){pda[observed_object["lep"]].define_pT = pda[observed_object["e"]].define_pT;}
    else if (pda[observed_object["tau"]].define_pT >= pda[observed_object["mu"]].define_pT){pda[observed_object["lep"]].define_pT = pda[observed_object["mu"]].define_pT;}
    else {pda[observed_object["lep"]].define_pT = pda[observed_object["tau"]].define_pT;}
  }

  // adapt define_pT (e-em/ep) !
  if (pda[observed_object["em"]].define_pT == 0. && pda[observed_object["e"]].define_pT > 0.){pda[observed_object["em"]].define_pT = pda[observed_object["e"]].define_pT;}
  if (pda[observed_object["ep"]].define_pT == 0. && pda[observed_object["e"]].define_pT > 0.){pda[observed_object["ep"]].define_pT = pda[observed_object["e"]].define_pT;}
  if (pda[observed_object["e"]].define_pT < pda[observed_object["em"]].define_pT &&
      pda[observed_object["e"]].define_pT < pda[observed_object["ep"]].define_pT){
    if (pda[observed_object["em"]].define_pT > pda[observed_object["ep"]].define_pT){pda[observed_object["e"]].define_pT = pda[observed_object["ep"]].define_pT;}
    else {pda[observed_object["e"]].define_pT = pda[observed_object["em"]].define_pT;}
  }

  // adapt define_pT (mu-mum/mup) !
  if (pda[observed_object["mum"]].define_pT == 0. && pda[observed_object["mu"]].define_pT > 0.){pda[observed_object["mum"]].define_pT = pda[observed_object["mu"]].define_pT;}
  if (pda[observed_object["mup"]].define_pT == 0. && pda[observed_object["mu"]].define_pT > 0.){pda[observed_object["mup"]].define_pT = pda[observed_object["mu"]].define_pT;}
  if (pda[observed_object["mu"]].define_pT < pda[observed_object["mum"]].define_pT &&
      pda[observed_object["mu"]].define_pT < pda[observed_object["mup"]].define_pT){
    if (pda[observed_object["mum"]].define_pT > pda[observed_object["mup"]].define_pT){pda[observed_object["mu"]].define_pT = pda[observed_object["mup"]].define_pT;}
    else {pda[observed_object["mu"]].define_pT = pda[observed_object["mum"]].define_pT;}
  }

  // adapt define_pT (tau-taum/taup) !
  if (pda[observed_object["taum"]].define_pT == 0. && pda[observed_object["tau"]].define_pT > 0.){pda[observed_object["taum"]].define_pT = pda[observed_object["tau"]].define_pT;}
  if (pda[observed_object["taup"]].define_pT == 0. && pda[observed_object["tau"]].define_pT > 0.){pda[observed_object["taup"]].define_pT = pda[observed_object["tau"]].define_pT;}
  if (pda[observed_object["tau"]].define_pT < pda[observed_object["taum"]].define_pT &&
      pda[observed_object["tau"]].define_pT < pda[observed_object["taup"]].define_pT){
    if (pda[observed_object["taum"]].define_pT > pda[observed_object["taup"]].define_pT){pda[observed_object["tau"]].define_pT = pda[observed_object["taup"]].define_pT;}
    else {pda[observed_object["tau"]].define_pT = pda[observed_object["taum"]].define_pT;}
  }

  // adapt define_pT (nua-nu/nux) !
  if (pda[observed_object["nu"]].define_pT == 0. && pda[observed_object["nua"]].define_pT > 0.){pda[observed_object["nu"]].define_pT = pda[observed_object["nua"]].define_pT;}
  if (pda[observed_object["nux"]].define_pT == 0. && pda[observed_object["nua"]].define_pT > 0.){pda[observed_object["nux"]].define_pT = pda[observed_object["nua"]].define_pT;}
  if (pda[observed_object["nua"]].define_pT < pda[observed_object["nu"]].define_pT &&
      pda[observed_object["nua"]].define_pT < pda[observed_object["nux"]].define_pT){
    if (pda[observed_object["nu"]].define_pT >= pda[observed_object["nux"]].define_pT){pda[observed_object["nua"]].define_pT = pda[observed_object["nux"]].define_pT;}
    else {pda[observed_object["nua"]].define_pT = pda[observed_object["nu"]].define_pT;}
  }

  // adapt define_pT (nua-nea/nma/nta) !
  if (pda[observed_object["nea"]].define_pT == 0. && pda[observed_object["nua"]].define_pT > 0.){pda[observed_object["nea"]].define_pT = pda[observed_object["nua"]].define_pT;}
  if (pda[observed_object["nma"]].define_pT == 0. && pda[observed_object["nua"]].define_pT > 0.){pda[observed_object["nma"]].define_pT = pda[observed_object["nua"]].define_pT;}
  if (pda[observed_object["nta"]].define_pT == 0. && pda[observed_object["nua"]].define_pT > 0.){pda[observed_object["nta"]].define_pT = pda[observed_object["nua"]].define_pT;}
  if (pda[observed_object["nua"]].define_pT < pda[observed_object["nea"]].define_pT &&
      pda[observed_object["nua"]].define_pT < pda[observed_object["nma"]].define_pT &&
      pda[observed_object["nua"]].define_pT < pda[observed_object["nta"]].define_pT){
    if (pda[observed_object["nta"]].define_pT >= pda[observed_object["nea"]].define_pT &&
	pda[observed_object["nma"]].define_pT >= pda[observed_object["nea"]].define_pT){pda[observed_object["nua"]].define_pT = pda[observed_object["nea"]].define_pT;}
    else if (pda[observed_object["nta"]].define_pT >= pda[observed_object["nma"]].define_pT){pda[observed_object["nua"]].define_pT = pda[observed_object["nma"]].define_pT;}
    else {pda[observed_object["nua"]].define_pT = pda[observed_object["nta"]].define_pT;}
  }

  // adapt define_pT (nea-ne/nex) !
  if (pda[observed_object["ne"]].define_pT == 0. && pda[observed_object["nea"]].define_pT > 0.){pda[observed_object["ne"]].define_pT = pda[observed_object["nea"]].define_pT;}
  if (pda[observed_object["nex"]].define_pT == 0. && pda[observed_object["nea"]].define_pT > 0.){pda[observed_object["nex"]].define_pT = pda[observed_object["nea"]].define_pT;}
  if (pda[observed_object["nea"]].define_pT < pda[observed_object["ne"]].define_pT &&
      pda[observed_object["nea"]].define_pT < pda[observed_object["nex"]].define_pT){
    if (pda[observed_object["ne"]].define_pT > pda[observed_object["nex"]].define_pT){pda[observed_object["nea"]].define_pT = pda[observed_object["nex"]].define_pT;}
    else {pda[observed_object["nea"]].define_pT = pda[observed_object["ne"]].define_pT;}
  }

  // adapt define_pT (nma-nm/nmx) !
  if (pda[observed_object["nm"]].define_pT == 0. && pda[observed_object["nma"]].define_pT > 0.){pda[observed_object["nm"]].define_pT = pda[observed_object["nma"]].define_pT;}
  if (pda[observed_object["nmx"]].define_pT == 0. && pda[observed_object["nma"]].define_pT > 0.){pda[observed_object["nmx"]].define_pT = pda[observed_object["nma"]].define_pT;}
  if (pda[observed_object["nma"]].define_pT < pda[observed_object["nm"]].define_pT &&
      pda[observed_object["nma"]].define_pT < pda[observed_object["nmx"]].define_pT){
    if (pda[observed_object["nm"]].define_pT > pda[observed_object["nmx"]].define_pT){pda[observed_object["nma"]].define_pT = pda[observed_object["nmx"]].define_pT;}
    else {pda[observed_object["nma"]].define_pT = pda[observed_object["nm"]].define_pT;}
  }

  // adapt define_pT (nta-nt/ntx) !
  if (pda[observed_object["nt"]].define_pT == 0. && pda[observed_object["nta"]].define_pT > 0.){pda[observed_object["nt"]].define_pT = pda[observed_object["nta"]].define_pT;}
  if (pda[observed_object["ntx"]].define_pT == 0. && pda[observed_object["nta"]].define_pT > 0.){pda[observed_object["ntx"]].define_pT = pda[observed_object["nta"]].define_pT;}
  if (pda[observed_object["nta"]].define_pT < pda[observed_object["nt"]].define_pT &&
      pda[observed_object["nta"]].define_pT < pda[observed_object["ntx"]].define_pT){
    if (pda[observed_object["nt"]].define_pT > pda[observed_object["ntx"]].define_pT){pda[observed_object["nta"]].define_pT = pda[observed_object["ntx"]].define_pT;}
    else {pda[observed_object["nta"]].define_pT = pda[observed_object["nt"]].define_pT;}
  }



  // adapt define_ET (tjet-top/atop) !
  if (pda[observed_object["top"]].define_ET == 0. && pda[observed_object["tjet"]].define_ET > 0.){pda[observed_object["top"]].define_ET = pda[observed_object["tjet"]].define_ET;}
  if (pda[observed_object["atop"]].define_ET == 0. && pda[observed_object["tjet"]].define_ET > 0.){pda[observed_object["atop"]].define_ET = pda[observed_object["tjet"]].define_ET;}
  if (pda[observed_object["tjet"]].define_ET < pda[observed_object["top"]].define_ET &&
      pda[observed_object["tjet"]].define_ET < pda[observed_object["atop"]].define_ET){
    if (pda[observed_object["top"]].define_ET >= pda[observed_object["atop"]].define_ET){pda[observed_object["tjet"]].define_ET = pda[observed_object["atop"]].define_ET;}
    else {pda[observed_object["tjet"]].define_ET = pda[observed_object["top"]].define_ET;}
  }

  // adapt define_ET (bjet-bjet_b/bjet_bx) !
  if (pda[observed_object["bjet_b"]].define_ET == 0. && pda[observed_object["bjet"]].define_ET > 0.){pda[observed_object["bjet_b"]].define_ET = pda[observed_object["bjet"]].define_ET;}
  if (pda[observed_object["bjet_bx"]].define_ET == 0. && pda[observed_object["bjet"]].define_ET > 0.){pda[observed_object["bjet_bx"]].define_ET = pda[observed_object["bjet"]].define_ET;}
  if (pda[observed_object["bjet"]].define_ET < pda[observed_object["bjet_b"]].define_ET &&
      pda[observed_object["bjet"]].define_ET < pda[observed_object["bjet_bx"]].define_ET){
    if (pda[observed_object["bjet_b"]].define_ET >= pda[observed_object["bjet_bx"]].define_ET){pda[observed_object["bjet"]].define_ET = pda[observed_object["bjet_bx"]].define_ET;}
    else {pda[observed_object["bjet"]].define_ET = pda[observed_object["bjet_b"]].define_ET;}
  }
  /*
  // adapt define_ET (jet-ljet/bjet) !
  if (pda[observed_object["ljet"]].define_ET == 0. && pda[observed_object["jet"]].define_ET > 0.){pda[observed_object["ljet"]].define_ET = pda[observed_object["jet"]].define_ET;}
  if (pda[observed_object["bjet"]].define_ET == 0. && pda[observed_object["jet"]].define_ET > 0.){pda[observed_object["bjet"]].define_ET = pda[observed_object["jet"]].define_ET;}
  if (pda[observed_object["jet"]].define_ET == 0. && pda[observed_object["bjet"]].define_ET == 0. && pda[observed_object["ljet"]].define_ET > 0.){pda[observed_object["jet"]].define_ET = pda[observed_object["ljet"]].define_ET;}
  if (pda[observed_object["jet"]].define_ET == 0. && pda[observed_object["ljet"]].define_ET == 0. && pda[observed_object["bjet"]].define_ET > 0.){pda[observed_object["jet"]].define_ET = pda[observed_object["bjet"]].define_ET;}

  if (pda[observed_object["jet"]].define_ET < pda[observed_object["ljet"]].define_ET &&
      pda[observed_object["jet"]].define_ET < pda[observed_object["bjet"]].define_ET){
    if (pda[observed_object["ljet"]].define_ET >= pda[observed_object["bjet"]].define_ET){pda[observed_object["jet"]].define_ET = pda[observed_object["bjet"]].define_ET;}
    else {pda[observed_object["jet"]].define_ET = pda[observed_object["ljet"]].define_ET;}
  }
  */
  // adapt define_ET (lep-lm/lp) !
  if (pda[observed_object["lm"]].define_ET == 0. && pda[observed_object["lep"]].define_ET > 0.){pda[observed_object["lm"]].define_ET = pda[observed_object["lep"]].define_ET;}
  if (pda[observed_object["lp"]].define_ET == 0. && pda[observed_object["lep"]].define_ET > 0.){pda[observed_object["lp"]].define_ET = pda[observed_object["lep"]].define_ET;}
  if (pda[observed_object["lep"]].define_ET < pda[observed_object["lm"]].define_ET &&
      pda[observed_object["lep"]].define_ET < pda[observed_object["lp"]].define_ET){
    if (pda[observed_object["lm"]].define_ET >= pda[observed_object["lp"]].define_ET){pda[observed_object["lep"]].define_ET = pda[observed_object["lp"]].define_ET;}
    else {pda[observed_object["lep"]].define_ET = pda[observed_object["lm"]].define_ET;}
  }

  // adapt define_ET (lep-e/mu/tau) !
  if (pda[observed_object["e"]].define_ET == 0. && pda[observed_object["lep"]].define_ET > 0.){pda[observed_object["e"]].define_ET = pda[observed_object["lep"]].define_ET;}
  if (pda[observed_object["mu"]].define_ET == 0. && pda[observed_object["lep"]].define_ET > 0.){pda[observed_object["mu"]].define_ET = pda[observed_object["lep"]].define_ET;}
  if (pda[observed_object["tau"]].define_ET == 0. && pda[observed_object["lep"]].define_ET > 0.){pda[observed_object["tau"]].define_ET = pda[observed_object["lep"]].define_ET;}
  if (pda[observed_object["lep"]].define_ET < pda[observed_object["e"]].define_ET &&
      pda[observed_object["lep"]].define_ET < pda[observed_object["mu"]].define_ET &&
      pda[observed_object["lep"]].define_ET < pda[observed_object["tau"]].define_ET){
    if (pda[observed_object["tau"]].define_ET >= pda[observed_object["e"]].define_ET &&
	pda[observed_object["mu"]].define_ET >= pda[observed_object["e"]].define_ET){pda[observed_object["lep"]].define_ET = pda[observed_object["e"]].define_ET;}
    else if (pda[observed_object["tau"]].define_ET >= pda[observed_object["mu"]].define_ET){pda[observed_object["lep"]].define_ET = pda[observed_object["mu"]].define_ET;}
    else {pda[observed_object["lep"]].define_ET = pda[observed_object["tau"]].define_ET;}
  }

  // adapt define_ET (e-em/ep) !
  if (pda[observed_object["em"]].define_ET == 0. && pda[observed_object["e"]].define_ET > 0.){pda[observed_object["em"]].define_ET = pda[observed_object["e"]].define_ET;}
  if (pda[observed_object["ep"]].define_ET == 0. && pda[observed_object["e"]].define_ET > 0.){pda[observed_object["ep"]].define_ET = pda[observed_object["e"]].define_ET;}
  if (pda[observed_object["e"]].define_ET < pda[observed_object["em"]].define_ET &&
      pda[observed_object["e"]].define_ET < pda[observed_object["ep"]].define_ET){
    if (pda[observed_object["em"]].define_ET > pda[observed_object["ep"]].define_ET){pda[observed_object["e"]].define_ET = pda[observed_object["ep"]].define_ET;}
    else {pda[observed_object["e"]].define_ET = pda[observed_object["em"]].define_ET;}
  }

  // adapt define_ET (mu-mum/mup) !
  if (pda[observed_object["mum"]].define_ET == 0. && pda[observed_object["mu"]].define_ET > 0.){pda[observed_object["mum"]].define_ET = pda[observed_object["mu"]].define_ET;}
  if (pda[observed_object["mup"]].define_ET == 0. && pda[observed_object["mu"]].define_ET > 0.){pda[observed_object["mup"]].define_ET = pda[observed_object["mu"]].define_ET;}
  if (pda[observed_object["mu"]].define_ET < pda[observed_object["mum"]].define_ET &&
      pda[observed_object["mu"]].define_ET < pda[observed_object["mup"]].define_ET){
    if (pda[observed_object["mum"]].define_ET > pda[observed_object["mup"]].define_ET){pda[observed_object["mu"]].define_ET = pda[observed_object["mup"]].define_ET;}
    else {pda[observed_object["mu"]].define_ET = pda[observed_object["mum"]].define_ET;}
  }

  // adapt define_ET (tau-taum/taup) !
  if (pda[observed_object["taum"]].define_ET == 0. && pda[observed_object["tau"]].define_ET > 0.){pda[observed_object["taum"]].define_ET = pda[observed_object["tau"]].define_ET;}
  if (pda[observed_object["taup"]].define_ET == 0. && pda[observed_object["tau"]].define_ET > 0.){pda[observed_object["taup"]].define_ET = pda[observed_object["tau"]].define_ET;}
  if (pda[observed_object["tau"]].define_ET < pda[observed_object["taum"]].define_ET &&
      pda[observed_object["tau"]].define_ET < pda[observed_object["taup"]].define_ET){
    if (pda[observed_object["taum"]].define_ET > pda[observed_object["taup"]].define_ET){pda[observed_object["tau"]].define_ET = pda[observed_object["taup"]].define_ET;}
    else {pda[observed_object["tau"]].define_ET = pda[observed_object["taum"]].define_ET;}
  }

  // adapt define_ET (nua-nu/nux) !
  if (pda[observed_object["nu"]].define_ET == 0. && pda[observed_object["nua"]].define_ET > 0.){pda[observed_object["nu"]].define_ET = pda[observed_object["nua"]].define_ET;}
  if (pda[observed_object["nux"]].define_ET == 0. && pda[observed_object["nua"]].define_ET > 0.){pda[observed_object["nux"]].define_ET = pda[observed_object["nua"]].define_ET;}
  if (pda[observed_object["nua"]].define_ET < pda[observed_object["nu"]].define_ET &&
      pda[observed_object["nua"]].define_ET < pda[observed_object["nux"]].define_ET){
    if (pda[observed_object["nu"]].define_ET >= pda[observed_object["nux"]].define_ET){pda[observed_object["nua"]].define_ET = pda[observed_object["nux"]].define_ET;}
    else {pda[observed_object["nua"]].define_ET = pda[observed_object["nu"]].define_ET;}
  }

  // adapt define_ET (nua-nea/nma/nta) !
  if (pda[observed_object["nea"]].define_ET == 0. && pda[observed_object["nua"]].define_ET > 0.){pda[observed_object["nea"]].define_ET = pda[observed_object["nua"]].define_ET;}
  if (pda[observed_object["nma"]].define_ET == 0. && pda[observed_object["nua"]].define_ET > 0.){pda[observed_object["nma"]].define_ET = pda[observed_object["nua"]].define_ET;}
  if (pda[observed_object["nta"]].define_ET == 0. && pda[observed_object["nua"]].define_ET > 0.){pda[observed_object["nta"]].define_ET = pda[observed_object["nua"]].define_ET;}
  if (pda[observed_object["nua"]].define_ET < pda[observed_object["nea"]].define_ET &&
      pda[observed_object["nua"]].define_ET < pda[observed_object["nma"]].define_ET &&
      pda[observed_object["nua"]].define_ET < pda[observed_object["nta"]].define_ET){
    if (pda[observed_object["nta"]].define_ET >= pda[observed_object["nea"]].define_ET &&
	pda[observed_object["nma"]].define_ET >= pda[observed_object["nea"]].define_ET){pda[observed_object["nua"]].define_ET = pda[observed_object["nea"]].define_ET;}
    else if (pda[observed_object["nta"]].define_ET >= pda[observed_object["nma"]].define_ET){pda[observed_object["nua"]].define_ET = pda[observed_object["nma"]].define_ET;}
    else {pda[observed_object["nua"]].define_ET = pda[observed_object["nta"]].define_ET;}
  }

  // adapt define_ET (nea-ne/nex) !
  if (pda[observed_object["ne"]].define_ET == 0. && pda[observed_object["nea"]].define_ET > 0.){pda[observed_object["ne"]].define_ET = pda[observed_object["nea"]].define_ET;}
  if (pda[observed_object["nex"]].define_ET == 0. && pda[observed_object["nea"]].define_ET > 0.){pda[observed_object["nex"]].define_ET = pda[observed_object["nea"]].define_ET;}
  if (pda[observed_object["nea"]].define_ET < pda[observed_object["ne"]].define_ET &&
      pda[observed_object["nea"]].define_ET < pda[observed_object["nex"]].define_ET){
    if (pda[observed_object["ne"]].define_ET > pda[observed_object["nex"]].define_ET){pda[observed_object["nea"]].define_ET = pda[observed_object["nex"]].define_ET;}
    else {pda[observed_object["nea"]].define_ET = pda[observed_object["ne"]].define_ET;}
  }

  // adapt define_ET (nma-nm/nmx) !
  if (pda[observed_object["nm"]].define_ET == 0. && pda[observed_object["nma"]].define_ET > 0.){pda[observed_object["nm"]].define_ET = pda[observed_object["nma"]].define_ET;}
  if (pda[observed_object["nmx"]].define_ET == 0. && pda[observed_object["nma"]].define_ET > 0.){pda[observed_object["nmx"]].define_ET = pda[observed_object["nma"]].define_ET;}
  if (pda[observed_object["nma"]].define_ET < pda[observed_object["nm"]].define_ET &&
      pda[observed_object["nma"]].define_ET < pda[observed_object["nmx"]].define_ET){
    if (pda[observed_object["nm"]].define_ET > pda[observed_object["nmx"]].define_ET){pda[observed_object["nma"]].define_ET = pda[observed_object["nmx"]].define_ET;}
    else {pda[observed_object["nma"]].define_ET = pda[observed_object["nm"]].define_ET;}
  }

  // adapt define_ET (nta-nt/ntx) !
  if (pda[observed_object["nt"]].define_ET == 0. && pda[observed_object["nta"]].define_ET > 0.){pda[observed_object["nt"]].define_ET = pda[observed_object["nta"]].define_ET;}
  if (pda[observed_object["ntx"]].define_ET == 0. && pda[observed_object["nta"]].define_ET > 0.){pda[observed_object["ntx"]].define_ET = pda[observed_object["nta"]].define_ET;}
  if (pda[observed_object["nta"]].define_ET < pda[observed_object["nt"]].define_ET &&
      pda[observed_object["nta"]].define_ET < pda[observed_object["ntx"]].define_ET){
    if (pda[observed_object["nt"]].define_ET > pda[observed_object["ntx"]].define_ET){pda[observed_object["nta"]].define_ET = pda[observed_object["ntx"]].define_ET;}
    else {pda[observed_object["nta"]].define_ET = pda[observed_object["nt"]].define_ET;}
  }



  // FIXME: shouldn't that be 'max'??
  //define_eta[observed_object["jet"]] = min(define_eta[observed_object["jet"]],min(define_eta[observed_object["ljet"]],define_eta[observed_object["bjet"]]));



  // adapt define_eta (tjet-top/atop) !
  if (pda[observed_object["top"]].define_eta == 1.e99 && pda[observed_object["tjet"]].define_eta < 1.e99){pda[observed_object["top"]].define_eta = pda[observed_object["tjet"]].define_eta;}
  if (pda[observed_object["atop"]].define_eta == 1.e99 && pda[observed_object["tjet"]].define_eta < 1.e99){pda[observed_object["atop"]].define_eta = pda[observed_object["tjet"]].define_eta;}
  if (pda[observed_object["tjet"]].define_eta < pda[observed_object["top"]].define_eta &&
      pda[observed_object["tjet"]].define_eta < pda[observed_object["atop"]].define_eta){
    if (pda[observed_object["top"]].define_eta >= pda[observed_object["atop"]].define_eta){pda[observed_object["tjet"]].define_eta = pda[observed_object["atop"]].define_eta;}
    else {pda[observed_object["tjet"]].define_eta = pda[observed_object["top"]].define_eta;}
  }

  // adapt define_eta (bjet-bjet_b/bjet_bx) !
  if (pda[observed_object["bjet_b"]].define_eta == 1.e99 && pda[observed_object["bjet"]].define_eta < 1.e99){pda[observed_object["bjet_b"]].define_eta = pda[observed_object["bjet"]].define_eta;}
  if (pda[observed_object["bjet_bx"]].define_eta == 1.e99 && pda[observed_object["bjet"]].define_eta < 1.e99){pda[observed_object["bjet_bx"]].define_eta = pda[observed_object["bjet"]].define_eta;}
  if (pda[observed_object["bjet"]].define_eta < pda[observed_object["bjet_b"]].define_eta &&
      pda[observed_object["bjet"]].define_eta < pda[observed_object["bjet_bx"]].define_eta){
    if (pda[observed_object["bjet_b"]].define_eta >= pda[observed_object["bjet_bx"]].define_eta){pda[observed_object["bjet"]].define_eta = pda[observed_object["bjet_bx"]].define_eta;}
    else {pda[observed_object["bjet"]].define_eta = pda[observed_object["bjet_b"]].define_eta;}
  }
  /*
  // adapt define_eta (jet-ljet/bjet) !
  if (pda[observed_object["ljet"]].define_eta == 1.e99 && pda[observed_object["jet"]].define_eta < 1.e99){pda[observed_object["ljet"]].define_eta = pda[observed_object["jet"]].define_eta;}
  if (pda[observed_object["bjet"]].define_eta == 1.e99 && pda[observed_object["jet"]].define_eta < 1.e99){pda[observed_object["bjet"]].define_eta = pda[observed_object["jet"]].define_eta;}

  if (pda[observed_object["jet"]].define_eta == 0. && pda[observed_object["bjet"]].define_eta == 0. && pda[observed_object["ljet"]].define_eta > 0.){pda[observed_object["jet"]].define_eta = pda[observed_object["ljet"]].define_eta;}
  if (pda[observed_object["jet"]].define_eta == 0. && pda[observed_object["ljet"]].define_eta == 0. && pda[observed_object["bjet"]].define_eta > 0.){pda[observed_object["jet"]].define_eta = pda[observed_object["bjet"]].define_eta;}

  if (pda[observed_object["jet"]].define_eta < pda[observed_object["ljet"]].define_eta &&
      pda[observed_object["jet"]].define_eta < pda[observed_object["bjet"]].define_eta){
    if (pda[observed_object["ljet"]].define_eta >= pda[observed_object["bjet"]].define_eta){pda[observed_object["jet"]].define_eta = pda[observed_object["bjet"]].define_eta;}
    else {pda[observed_object["jet"]].define_eta = pda[observed_object["ljet"]].define_eta;}
  }
  */
  // adapt define_eta (lep-lm/lp) !
  if (pda[observed_object["lm"]].define_eta == 1.e99 && pda[observed_object["lep"]].define_eta < 1.e99){pda[observed_object["lm"]].define_eta = pda[observed_object["lep"]].define_eta;}
  if (pda[observed_object["lp"]].define_eta == 1.e99 && pda[observed_object["lep"]].define_eta < 1.e99){pda[observed_object["lp"]].define_eta = pda[observed_object["lep"]].define_eta;}
  if (pda[observed_object["lep"]].define_eta < pda[observed_object["lm"]].define_eta &&
      pda[observed_object["lep"]].define_eta < pda[observed_object["lp"]].define_eta){
    if (pda[observed_object["lm"]].define_eta >= pda[observed_object["lp"]].define_eta){pda[observed_object["lep"]].define_eta = pda[observed_object["lp"]].define_eta;}
    else {pda[observed_object["lep"]].define_eta = pda[observed_object["lm"]].define_eta;}
  }

  // adapt define_eta (lep-e/mu/tau) !
  if (pda[observed_object["e"]].define_eta == 1.e99 && pda[observed_object["lep"]].define_eta < 1.e99){pda[observed_object["e"]].define_eta = pda[observed_object["lep"]].define_eta;}
  if (pda[observed_object["mu"]].define_eta == 1.e99 && pda[observed_object["lep"]].define_eta < 1.e99){pda[observed_object["mu"]].define_eta = pda[observed_object["lep"]].define_eta;}
  if (pda[observed_object["tau"]].define_eta == 1.e99 && pda[observed_object["lep"]].define_eta < 1.e99){pda[observed_object["tau"]].define_eta = pda[observed_object["lep"]].define_eta;}
  if (pda[observed_object["lep"]].define_eta < pda[observed_object["e"]].define_eta &&
      pda[observed_object["lep"]].define_eta < pda[observed_object["mu"]].define_eta &&
      pda[observed_object["lep"]].define_eta < pda[observed_object["tau"]].define_eta){
    if (pda[observed_object["tau"]].define_eta >= pda[observed_object["e"]].define_eta &&
	pda[observed_object["mu"]].define_eta >= pda[observed_object["e"]].define_eta){pda[observed_object["lep"]].define_eta = pda[observed_object["e"]].define_eta;}
    else if (pda[observed_object["tau"]].define_eta >= pda[observed_object["mu"]].define_eta){pda[observed_object["lep"]].define_eta = pda[observed_object["mu"]].define_eta;}
    else {pda[observed_object["lep"]].define_eta = pda[observed_object["tau"]].define_eta;}
  }

  // adapt define_eta (e-em/ep) !
  if (pda[observed_object["em"]].define_eta == 1.e99 && pda[observed_object["e"]].define_eta < 1.e99){pda[observed_object["em"]].define_eta = pda[observed_object["e"]].define_eta;}
  if (pda[observed_object["ep"]].define_eta == 1.e99 && pda[observed_object["e"]].define_eta < 1.e99){pda[observed_object["ep"]].define_eta = pda[observed_object["e"]].define_eta;}
  if (pda[observed_object["e"]].define_eta < pda[observed_object["em"]].define_eta &&
      pda[observed_object["e"]].define_eta < pda[observed_object["ep"]].define_eta){
    if (pda[observed_object["em"]].define_eta > pda[observed_object["ep"]].define_eta){pda[observed_object["e"]].define_eta = pda[observed_object["ep"]].define_eta;}
    else {pda[observed_object["e"]].define_eta = pda[observed_object["em"]].define_eta;}
  }

  // adapt define_eta (mu-mum/mup) !
  if (pda[observed_object["mum"]].define_eta == 1.e99 && pda[observed_object["mu"]].define_eta < 1.e99){pda[observed_object["mum"]].define_eta = pda[observed_object["mu"]].define_eta;}
  if (pda[observed_object["mup"]].define_eta == 1.e99 && pda[observed_object["mu"]].define_eta < 1.e99){pda[observed_object["mup"]].define_eta = pda[observed_object["mu"]].define_eta;}
  if (pda[observed_object["mu"]].define_eta < pda[observed_object["mum"]].define_eta &&
      pda[observed_object["mu"]].define_eta < pda[observed_object["mup"]].define_eta){
    if (pda[observed_object["mum"]].define_eta > pda[observed_object["mup"]].define_eta){pda[observed_object["mu"]].define_eta = pda[observed_object["mup"]].define_eta;}
    else {pda[observed_object["mu"]].define_eta = pda[observed_object["mum"]].define_eta;}
  }

  // adapt define_eta (tau-taum/taup) !
  if (pda[observed_object["taum"]].define_eta == 1.e99 && pda[observed_object["tau"]].define_eta < 1.e99){pda[observed_object["taum"]].define_eta = pda[observed_object["tau"]].define_eta;}
  if (pda[observed_object["taup"]].define_eta == 1.e99 && pda[observed_object["tau"]].define_eta < 1.e99){pda[observed_object["taup"]].define_eta = pda[observed_object["tau"]].define_eta;}
  if (pda[observed_object["tau"]].define_eta < pda[observed_object["taum"]].define_eta &&
      pda[observed_object["tau"]].define_eta < pda[observed_object["taup"]].define_eta){
    if (pda[observed_object["taum"]].define_eta > pda[observed_object["taup"]].define_eta){pda[observed_object["tau"]].define_eta = pda[observed_object["taup"]].define_eta;}
    else {pda[observed_object["tau"]].define_eta = pda[observed_object["taum"]].define_eta;}
  }

  // adapt define_eta (nua-nu/nux) !
  if (pda[observed_object["nu"]].define_eta == 1.e99 && pda[observed_object["nua"]].define_eta < 1.e99){pda[observed_object["nu"]].define_eta = pda[observed_object["nua"]].define_eta;}
  if (pda[observed_object["nux"]].define_eta == 1.e99 && pda[observed_object["nua"]].define_eta < 1.e99){pda[observed_object["nux"]].define_eta = pda[observed_object["nua"]].define_eta;}
  if (pda[observed_object["nua"]].define_eta < pda[observed_object["nu"]].define_eta &&
      pda[observed_object["nua"]].define_eta < pda[observed_object["nux"]].define_eta){
    if (pda[observed_object["nu"]].define_eta >= pda[observed_object["nux"]].define_eta){pda[observed_object["nua"]].define_eta = pda[observed_object["nux"]].define_eta;}
    else {pda[observed_object["nua"]].define_eta = pda[observed_object["nu"]].define_eta;}
  }

  // adapt define_eta (nua-nea/nma/nta) !
  if (pda[observed_object["nea"]].define_eta == 1.e99 && pda[observed_object["nua"]].define_eta < 1.e99){pda[observed_object["nea"]].define_eta = pda[observed_object["nua"]].define_eta;}
  if (pda[observed_object["nma"]].define_eta == 1.e99 && pda[observed_object["nua"]].define_eta < 1.e99){pda[observed_object["nma"]].define_eta = pda[observed_object["nua"]].define_eta;}
  if (pda[observed_object["nta"]].define_eta == 1.e99 && pda[observed_object["nua"]].define_eta < 1.e99){pda[observed_object["nta"]].define_eta = pda[observed_object["nua"]].define_eta;}
  if (pda[observed_object["nua"]].define_eta < pda[observed_object["nea"]].define_eta &&
      pda[observed_object["nua"]].define_eta < pda[observed_object["nma"]].define_eta &&
      pda[observed_object["nua"]].define_eta < pda[observed_object["nta"]].define_eta){
    if (pda[observed_object["nta"]].define_eta >= pda[observed_object["nea"]].define_eta &&
	pda[observed_object["nma"]].define_eta >= pda[observed_object["nea"]].define_eta){pda[observed_object["nua"]].define_eta = pda[observed_object["nea"]].define_eta;}
    else if (pda[observed_object["nta"]].define_eta >= pda[observed_object["nma"]].define_eta){pda[observed_object["nua"]].define_eta = pda[observed_object["nma"]].define_eta;}
    else {pda[observed_object["nua"]].define_eta = pda[observed_object["nta"]].define_eta;}
  }

  // adapt define_eta (nea-ne/nex) !
  if (pda[observed_object["ne"]].define_eta == 1.e99 && pda[observed_object["nea"]].define_eta < 1.e99){pda[observed_object["ne"]].define_eta = pda[observed_object["nea"]].define_eta;}
  if (pda[observed_object["nex"]].define_eta == 1.e99 && pda[observed_object["nea"]].define_eta < 1.e99){pda[observed_object["nex"]].define_eta = pda[observed_object["nea"]].define_eta;}
  if (pda[observed_object["nea"]].define_eta < pda[observed_object["ne"]].define_eta &&
      pda[observed_object["nea"]].define_eta < pda[observed_object["nex"]].define_eta){
    if (pda[observed_object["ne"]].define_eta > pda[observed_object["nex"]].define_eta){pda[observed_object["nea"]].define_eta = pda[observed_object["nex"]].define_eta;}
    else {pda[observed_object["nea"]].define_eta = pda[observed_object["ne"]].define_eta;}
  }

  // adapt define_eta (nma-nm/nmx) !
  if (pda[observed_object["nm"]].define_eta == 1.e99 && pda[observed_object["nma"]].define_eta < 1.e99){pda[observed_object["nm"]].define_eta = pda[observed_object["nma"]].define_eta;}
  if (pda[observed_object["nmx"]].define_eta == 1.e99 && pda[observed_object["nma"]].define_eta < 1.e99){pda[observed_object["nmx"]].define_eta = pda[observed_object["nma"]].define_eta;}
  if (pda[observed_object["nma"]].define_eta < pda[observed_object["nm"]].define_eta &&
      pda[observed_object["nma"]].define_eta < pda[observed_object["nmx"]].define_eta){
    if (pda[observed_object["nm"]].define_eta > pda[observed_object["nmx"]].define_eta){pda[observed_object["nma"]].define_eta = pda[observed_object["nmx"]].define_eta;}
    else {pda[observed_object["nma"]].define_eta = pda[observed_object["nm"]].define_eta;}
  }

  // adapt define_eta (nta-nt/ntx) !
  if (pda[observed_object["nt"]].define_eta == 1.e99 && pda[observed_object["nta"]].define_eta < 1.e99){pda[observed_object["nt"]].define_eta = pda[observed_object["nta"]].define_eta;}
  if (pda[observed_object["ntx"]].define_eta == 1.e99 && pda[observed_object["nta"]].define_eta < 1.e99){pda[observed_object["ntx"]].define_eta = pda[observed_object["nta"]].define_eta;}
  if (pda[observed_object["nta"]].define_eta < pda[observed_object["nt"]].define_eta &&
      pda[observed_object["nta"]].define_eta < pda[observed_object["ntx"]].define_eta){
    if (pda[observed_object["nt"]].define_eta > pda[observed_object["ntx"]].define_eta){pda[observed_object["nta"]].define_eta = pda[observed_object["ntx"]].define_eta;}
    else {pda[observed_object["nta"]].define_eta = pda[observed_object["nt"]].define_eta;}
  }


  // adapt define_y (tjet-top/atop) !
  if (pda[observed_object["top"]].define_y == 1.e99 && pda[observed_object["tjet"]].define_y < 1.e99){pda[observed_object["top"]].define_y = pda[observed_object["tjet"]].define_y;}
  if (pda[observed_object["atop"]].define_y == 1.e99 && pda[observed_object["tjet"]].define_y < 1.e99){pda[observed_object["atop"]].define_y = pda[observed_object["tjet"]].define_y;}
  if (pda[observed_object["tjet"]].define_y < pda[observed_object["top"]].define_y &&
      pda[observed_object["tjet"]].define_y < pda[observed_object["atop"]].define_y){
    if (pda[observed_object["top"]].define_y >= pda[observed_object["atop"]].define_y){pda[observed_object["tjet"]].define_y = pda[observed_object["atop"]].define_y;}
    else {pda[observed_object["tjet"]].define_y = pda[observed_object["top"]].define_y;}
  }

  // adapt define_y (bjet-bjet_b/bjet_bx) !
  if (pda[observed_object["bjet_b"]].define_y == 1.e99 && pda[observed_object["bjet"]].define_y < 1.e99){pda[observed_object["bjet_b"]].define_y = pda[observed_object["bjet"]].define_y;}
  if (pda[observed_object["bjet_bx"]].define_y == 1.e99 && pda[observed_object["bjet"]].define_y < 1.e99){pda[observed_object["bjet_bx"]].define_y = pda[observed_object["bjet"]].define_y;}
  if (pda[observed_object["bjet"]].define_y < pda[observed_object["bjet_b"]].define_y &&
      pda[observed_object["bjet"]].define_y < pda[observed_object["bjet_bx"]].define_y){
    if (pda[observed_object["bjet_b"]].define_y >= pda[observed_object["bjet_bx"]].define_y){pda[observed_object["bjet"]].define_y = pda[observed_object["bjet_bx"]].define_y;}
    else {pda[observed_object["bjet"]].define_y = pda[observed_object["bjet_b"]].define_y;}
  }
  /*
  // adapt define_y (jet-ljet/bjet) !
  if (pda[observed_object["ljet"]].define_y == 1.e99 && pda[observed_object["jet"]].define_y < 1.e99){pda[observed_object["ljet"]].define_y = pda[observed_object["jet"]].define_y;}
  if (pda[observed_object["bjet"]].define_y == 1.e99 && pda[observed_object["jet"]].define_y < 1.e99){pda[observed_object["bjet"]].define_y = pda[observed_object["jet"]].define_y;}

  if (pda[observed_object["jet"]].define_y == 0. && pda[observed_object["bjet"]].define_y == 0. && pda[observed_object["ljet"]].define_y > 0.){pda[observed_object["jet"]].define_y = pda[observed_object["ljet"]].define_y;}
  if (pda[observed_object["jet"]].define_y == 0. && pda[observed_object["ljet"]].define_y == 0. && pda[observed_object["bjet"]].define_y > 0.){pda[observed_object["jet"]].define_y = pda[observed_object["bjet"]].define_y;}

  if (pda[observed_object["jet"]].define_y < pda[observed_object["ljet"]].define_y &&
      pda[observed_object["jet"]].define_y < pda[observed_object["bjet"]].define_y){
    if (pda[observed_object["ljet"]].define_y >= pda[observed_object["bjet"]].define_y){pda[observed_object["jet"]].define_y = pda[observed_object["bjet"]].define_y;}
    else {pda[observed_object["jet"]].define_y = pda[observed_object["ljet"]].define_y;}
  }
  */
  // adapt define_y (lep-lm/lp) !
  if (pda[observed_object["lm"]].define_y == 1.e99 && pda[observed_object["lep"]].define_y < 1.e99){pda[observed_object["lm"]].define_y = pda[observed_object["lep"]].define_y;}
  if (pda[observed_object["lp"]].define_y == 1.e99 && pda[observed_object["lep"]].define_y < 1.e99){pda[observed_object["lp"]].define_y = pda[observed_object["lep"]].define_y;}
  if (pda[observed_object["lep"]].define_y < pda[observed_object["lm"]].define_y &&
      pda[observed_object["lep"]].define_y < pda[observed_object["lp"]].define_y){
    if (pda[observed_object["lm"]].define_y >= pda[observed_object["lp"]].define_y){pda[observed_object["lep"]].define_y = pda[observed_object["lp"]].define_y;}
    else {pda[observed_object["lep"]].define_y = pda[observed_object["lm"]].define_y;}
  }

  // adapt define_y (lep-e/mu/tau) !
  if (pda[observed_object["e"]].define_y == 1.e99 && pda[observed_object["lep"]].define_y < 1.e99){pda[observed_object["e"]].define_y = pda[observed_object["lep"]].define_y;}
  if (pda[observed_object["mu"]].define_y == 1.e99 && pda[observed_object["lep"]].define_y < 1.e99){pda[observed_object["mu"]].define_y = pda[observed_object["lep"]].define_y;}
  if (pda[observed_object["tau"]].define_y == 1.e99 && pda[observed_object["lep"]].define_y < 1.e99){pda[observed_object["tau"]].define_y = pda[observed_object["lep"]].define_y;}
  if (pda[observed_object["lep"]].define_y < pda[observed_object["e"]].define_y &&
      pda[observed_object["lep"]].define_y < pda[observed_object["mu"]].define_y &&
      pda[observed_object["lep"]].define_y < pda[observed_object["tau"]].define_y){
    if (pda[observed_object["tau"]].define_y >= pda[observed_object["e"]].define_y &&
	pda[observed_object["mu"]].define_y >= pda[observed_object["e"]].define_y){pda[observed_object["lep"]].define_y = pda[observed_object["e"]].define_y;}
    else if (pda[observed_object["tau"]].define_y >= pda[observed_object["mu"]].define_y){pda[observed_object["lep"]].define_y = pda[observed_object["mu"]].define_y;}
    else {pda[observed_object["lep"]].define_y = pda[observed_object["tau"]].define_y;}
  }

  // adapt define_y (e-em/ep) !
  if (pda[observed_object["em"]].define_y == 1.e99 && pda[observed_object["e"]].define_y < 1.e99){pda[observed_object["em"]].define_y = pda[observed_object["e"]].define_y;}
  if (pda[observed_object["ep"]].define_y == 1.e99 && pda[observed_object["e"]].define_y < 1.e99){pda[observed_object["ep"]].define_y = pda[observed_object["e"]].define_y;}
  if (pda[observed_object["e"]].define_y < pda[observed_object["em"]].define_y &&
      pda[observed_object["e"]].define_y < pda[observed_object["ep"]].define_y){
    if (pda[observed_object["em"]].define_y > pda[observed_object["ep"]].define_y){pda[observed_object["e"]].define_y = pda[observed_object["ep"]].define_y;}
    else {pda[observed_object["e"]].define_y = pda[observed_object["em"]].define_y;}
  }

  // adapt define_y (mu-mum/mup) !
  if (pda[observed_object["mum"]].define_y == 1.e99 && pda[observed_object["mu"]].define_y < 1.e99){pda[observed_object["mum"]].define_y = pda[observed_object["mu"]].define_y;}
  if (pda[observed_object["mup"]].define_y == 1.e99 && pda[observed_object["mu"]].define_y < 1.e99){pda[observed_object["mup"]].define_y = pda[observed_object["mu"]].define_y;}
  if (pda[observed_object["mu"]].define_y < pda[observed_object["mum"]].define_y &&
      pda[observed_object["mu"]].define_y < pda[observed_object["mup"]].define_y){
    if (pda[observed_object["mum"]].define_y > pda[observed_object["mup"]].define_y){pda[observed_object["mu"]].define_y = pda[observed_object["mup"]].define_y;}
    else {pda[observed_object["mu"]].define_y = pda[observed_object["mum"]].define_y;}
  }

  // adapt define_y (tau-taum/taup) !
  if (pda[observed_object["taum"]].define_y == 1.e99 && pda[observed_object["tau"]].define_y < 1.e99){pda[observed_object["taum"]].define_y = pda[observed_object["tau"]].define_y;}
  if (pda[observed_object["taup"]].define_y == 1.e99 && pda[observed_object["tau"]].define_y < 1.e99){pda[observed_object["taup"]].define_y = pda[observed_object["tau"]].define_y;}
  if (pda[observed_object["tau"]].define_y < pda[observed_object["taum"]].define_y &&
      pda[observed_object["tau"]].define_y < pda[observed_object["taup"]].define_y){
    if (pda[observed_object["taum"]].define_y > pda[observed_object["taup"]].define_y){pda[observed_object["tau"]].define_y = pda[observed_object["taup"]].define_y;}
    else {pda[observed_object["tau"]].define_y = pda[observed_object["taum"]].define_y;}
  }

  // adapt define_y (nua-nu/nux) !
  if (pda[observed_object["nu"]].define_y == 1.e99 && pda[observed_object["nua"]].define_y < 1.e99){pda[observed_object["nu"]].define_y = pda[observed_object["nua"]].define_y;}
  if (pda[observed_object["nux"]].define_y == 1.e99 && pda[observed_object["nua"]].define_y < 1.e99){pda[observed_object["nux"]].define_y = pda[observed_object["nua"]].define_y;}
  if (pda[observed_object["nua"]].define_y < pda[observed_object["nu"]].define_y &&
      pda[observed_object["nua"]].define_y < pda[observed_object["nux"]].define_y){
    if (pda[observed_object["nu"]].define_y >= pda[observed_object["nux"]].define_y){pda[observed_object["nua"]].define_y = pda[observed_object["nux"]].define_y;}
    else {pda[observed_object["nua"]].define_y = pda[observed_object["nu"]].define_y;}
  }

  // adapt define_y (nua-nea/nma/nta) !
  if (pda[observed_object["nea"]].define_y == 1.e99 && pda[observed_object["nua"]].define_y < 1.e99){pda[observed_object["nea"]].define_y = pda[observed_object["nua"]].define_y;}
  if (pda[observed_object["nma"]].define_y == 1.e99 && pda[observed_object["nua"]].define_y < 1.e99){pda[observed_object["nma"]].define_y = pda[observed_object["nua"]].define_y;}
  if (pda[observed_object["nta"]].define_y == 1.e99 && pda[observed_object["nua"]].define_y < 1.e99){pda[observed_object["nta"]].define_y = pda[observed_object["nua"]].define_y;}
  if (pda[observed_object["nua"]].define_y < pda[observed_object["nea"]].define_y &&
      pda[observed_object["nua"]].define_y < pda[observed_object["nma"]].define_y &&
      pda[observed_object["nua"]].define_y < pda[observed_object["nta"]].define_y){
    if (pda[observed_object["nta"]].define_y >= pda[observed_object["nea"]].define_y &&
	pda[observed_object["nma"]].define_y >= pda[observed_object["nea"]].define_y){pda[observed_object["nua"]].define_y = pda[observed_object["nea"]].define_y;}
    else if (pda[observed_object["nta"]].define_y >= pda[observed_object["nma"]].define_y){pda[observed_object["nua"]].define_y = pda[observed_object["nma"]].define_y;}
    else {pda[observed_object["nua"]].define_y = pda[observed_object["nta"]].define_y;}
  }

  // adapt define_y (nea-ne/nex) !
  if (pda[observed_object["ne"]].define_y == 1.e99 && pda[observed_object["nea"]].define_y < 1.e99){pda[observed_object["ne"]].define_y = pda[observed_object["nea"]].define_y;}
  if (pda[observed_object["nex"]].define_y == 1.e99 && pda[observed_object["nea"]].define_y < 1.e99){pda[observed_object["nex"]].define_y = pda[observed_object["nea"]].define_y;}
  if (pda[observed_object["nea"]].define_y < pda[observed_object["ne"]].define_y &&
      pda[observed_object["nea"]].define_y < pda[observed_object["nex"]].define_y){
    if (pda[observed_object["ne"]].define_y > pda[observed_object["nex"]].define_y){pda[observed_object["nea"]].define_y = pda[observed_object["nex"]].define_y;}
    else {pda[observed_object["nea"]].define_y = pda[observed_object["ne"]].define_y;}
  }

  // adapt define_y (nma-nm/nmx) !
  if (pda[observed_object["nm"]].define_y == 1.e99 && pda[observed_object["nma"]].define_y < 1.e99){pda[observed_object["nm"]].define_y = pda[observed_object["nma"]].define_y;}
  if (pda[observed_object["nmx"]].define_y == 1.e99 && pda[observed_object["nma"]].define_y < 1.e99){pda[observed_object["nmx"]].define_y = pda[observed_object["nma"]].define_y;}
  if (pda[observed_object["nma"]].define_y < pda[observed_object["nm"]].define_y &&
      pda[observed_object["nma"]].define_y < pda[observed_object["nmx"]].define_y){
    if (pda[observed_object["nm"]].define_y > pda[observed_object["nmx"]].define_y){pda[observed_object["nma"]].define_y = pda[observed_object["nmx"]].define_y;}
    else {pda[observed_object["nma"]].define_y = pda[observed_object["nm"]].define_y;}
  }

  // adapt define_y (nta-nt/ntx) !
  if (pda[observed_object["nt"]].define_y == 1.e99 && pda[observed_object["nta"]].define_y < 1.e99){pda[observed_object["nt"]].define_y = pda[observed_object["nta"]].define_y;}
  if (pda[observed_object["ntx"]].define_y == 1.e99 && pda[observed_object["nta"]].define_y < 1.e99){pda[observed_object["ntx"]].define_y = pda[observed_object["nta"]].define_y;}
  if (pda[observed_object["nta"]].define_y < pda[observed_object["nt"]].define_y &&
      pda[observed_object["nta"]].define_y < pda[observed_object["ntx"]].define_y){
    if (pda[observed_object["nt"]].define_y > pda[observed_object["ntx"]].define_y){pda[observed_object["nta"]].define_y = pda[observed_object["ntx"]].define_y;}
    else {pda[observed_object["nta"]].define_y = pda[observed_object["nt"]].define_y;}
  }


  // adapt n_observed_min (tjet-top/atop) !
  if (pda[observed_object["tjet"]].n_observed_min < pda[observed_object["top"]].n_observed_min + pda[observed_object["atop"]].n_observed_min){pda[observed_object["tjet"]].n_observed_min = pda[observed_object["top"]].n_observed_min + pda[observed_object["atop"]].n_observed_min;}

  // adapt n_observed_min (bjet-bjet_b/bjet_bx) !
  if (pda[observed_object["bjet"]].n_observed_min < pda[observed_object["bjet_b"]].n_observed_min + pda[observed_object["bjet_bx"]].n_observed_min){pda[observed_object["bjet"]].n_observed_min = pda[observed_object["bjet_b"]].n_observed_min + pda[observed_object["bjet_bx"]].n_observed_min;}

  // adapt n_observed_min (jets-ljets/bjets) !
  ///!X!  if (pda[observed_object["jet"]].n_observed_min < pda[observed_object["ljet"]].n_observed_min + pda[observed_object["bjet"]].n_observed_min){pda[observed_object["jet"]].n_observed_min = pda[observed_object["ljet"]].n_observed_min + pda[observed_object["bjet"]].n_observed_min;}

  // adapt n_observed_min (lm-em/mum/taum) !
  if (pda[observed_object["lm"]].n_observed_min < pda[observed_object["em"]].n_observed_min + pda[observed_object["mum"]].n_observed_min + pda[observed_object["taum"]].n_observed_min){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["em"]].n_observed_min + pda[observed_object["mum"]].n_observed_min + pda[observed_object["taum"]].n_observed_min;
  }
  // adapt n_observed_min (lp-ep/mup/taup) !
  if (pda[observed_object["lp"]].n_observed_min < pda[observed_object["ep"]].n_observed_min + pda[observed_object["mup"]].n_observed_min + pda[observed_object["taup"]].n_observed_min){
    pda[observed_object["lp"]].n_observed_min = pda[observed_object["ep"]].n_observed_min + pda[observed_object["mup"]].n_observed_min + pda[observed_object["taup"]].n_observed_min;
  }
  // adapt n_observed_min (lep-lm/lp) !
  if (pda[observed_object["lep"]].n_observed_min < pda[observed_object["lm"]].n_observed_min + pda[observed_object["lp"]].n_observed_min){
    pda[observed_object["lep"]].n_observed_min = pda[observed_object["lm"]].n_observed_min + pda[observed_object["lp"]].n_observed_min;
  }

  // adapt n_observed_min (e-ep/em) !
  if (pda[observed_object["e"]].n_observed_min < pda[observed_object["em"]].n_observed_min + pda[observed_object["ep"]].n_observed_min){
    pda[observed_object["e"]].n_observed_min = pda[observed_object["em"]].n_observed_min + pda[observed_object["ep"]].n_observed_min;
  }
  // adapt n_observed_min (mu-mup/mum) !
  if (pda[observed_object["mu"]].n_observed_min < pda[observed_object["mum"]].n_observed_min + pda[observed_object["mup"]].n_observed_min){
    pda[observed_object["mu"]].n_observed_min = pda[observed_object["mum"]].n_observed_min + pda[observed_object["mup"]].n_observed_min;
  }
  // adapt n_observed_min (tau-taup/taum) !
  if (pda[observed_object["tau"]].n_observed_min < pda[observed_object["taum"]].n_observed_min + pda[observed_object["taup"]].n_observed_min){
    pda[observed_object["tau"]].n_observed_min = pda[observed_object["taum"]].n_observed_min + pda[observed_object["taup"]].n_observed_min;
  }

  // adapt n_observed_min (lep-e/mu/tau) !
  if (pda[observed_object["lep"]].n_observed_min < pda[observed_object["e"]].n_observed_min + pda[observed_object["mu"]].n_observed_min + pda[observed_object["tau"]].n_observed_min){
    pda[observed_object["lep"]].n_observed_min = pda[observed_object["e"]].n_observed_min + pda[observed_object["mu"]].n_observed_min + pda[observed_object["tau"]].n_observed_min;
  }
  // adapt n_observed_min (nu-ne/nm/nt) !
  if (pda[observed_object["nu"]].n_observed_min < pda[observed_object["ne"]].n_observed_min + pda[observed_object["nm"]].n_observed_min + pda[observed_object["nt"]].n_observed_min){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["ne"]].n_observed_min + pda[observed_object["nm"]].n_observed_min + pda[observed_object["nt"]].n_observed_min;
  }
  // adapt n_observed_min (nux-nex/nmx/ntx) !
  if (pda[observed_object["nux"]].n_observed_min < pda[observed_object["nex"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min){
    pda[observed_object["nux"]].n_observed_min = pda[observed_object["nex"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min;
  }

  // adapt n_observed_min (nua-nu/nux) !
  if (pda[observed_object["nua"]].n_observed_min < pda[observed_object["nu"]].n_observed_min + pda[observed_object["nux"]].n_observed_min){
    pda[observed_object["nua"]].n_observed_min = pda[observed_object["nu"]].n_observed_min + pda[observed_object["nux"]].n_observed_min;
  }

  // adapt n_observed_min (nea-nex/ne) !
  if (pda[observed_object["nea"]].n_observed_min < pda[observed_object["ne"]].n_observed_min + pda[observed_object["nex"]].n_observed_min){
    pda[observed_object["nea"]].n_observed_min = pda[observed_object["ne"]].n_observed_min + pda[observed_object["nex"]].n_observed_min;
  }
  // adapt n_observed_min (nma-nmx/nm) !
  if (pda[observed_object["nma"]].n_observed_min < pda[observed_object["nm"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min){
    pda[observed_object["nma"]].n_observed_min = pda[observed_object["nm"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min;
  }
  // adapt n_observed_min (nta-ntx/nt) !
  if (pda[observed_object["nta"]].n_observed_min < pda[observed_object["nt"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min){
    pda[observed_object["nta"]].n_observed_min = pda[observed_object["nt"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min;
  }

  // adapt n_observed_min (nua-nea/nma/nta) !
  if (pda[observed_object["nua"]].n_observed_min < pda[observed_object["nea"]].n_observed_min + pda[observed_object["nma"]].n_observed_min + pda[observed_object["nta"]].n_observed_min){
    pda[observed_object["nua"]].n_observed_min = pda[observed_object["nea"]].n_observed_min + pda[observed_object["nma"]].n_observed_min + pda[observed_object["nta"]].n_observed_min;
  }


  // adapt n_observed_max (tjet-top/atop) !
  if (pda[observed_object["tjet"]].n_observed_max > pda[observed_object["top"]].n_observed_max + pda[observed_object["atop"]].n_observed_max){pda[observed_object["tjet"]].n_observed_max = pda[observed_object["top"]].n_observed_max + pda[observed_object["atop"]].n_observed_max;}

  // adapt n_observed_max (bjet-bjet_b/bjet_bx) !
  if (pda[observed_object["bjet"]].n_observed_max > pda[observed_object["bjet_b"]].n_observed_max + pda[observed_object["bjet_bx"]].n_observed_max){pda[observed_object["bjet"]].n_observed_max = pda[observed_object["bjet_b"]].n_observed_max + pda[observed_object["bjet_bx"]].n_observed_max;}
  // !!! xxxxx
  if (pda[observed_object["bjet"]].n_observed_max < pda[observed_object["bjet_b"]].n_observed_max){pda[observed_object["bjet_b"]].n_observed_max = pda[observed_object["bjet"]].n_observed_max;}
  if (pda[observed_object["bjet"]].n_observed_max < pda[observed_object["bjet_bx"]].n_observed_max){pda[observed_object["bjet_bx"]].n_observed_max = pda[observed_object["bjet"]].n_observed_max;}

  // adapt n_observed_max (jets-ljets/bjets) !
  ///!X!  if (pda[observed_object["jet"]].n_observed_max > pda[observed_object["ljet"]].n_observed_max + pda[observed_object["bjet"]].n_observed_max){pda[observed_object["jet"]].n_observed_max = pda[observed_object["ljet"]].n_observed_max + pda[observed_object["bjet"]].n_observed_max;}

  // adapt n_observed_max (lm-em/mum/taum) !
  if (pda[observed_object["lm"]].n_observed_max > pda[observed_object["em"]].n_observed_max + pda[observed_object["mum"]].n_observed_max + pda[observed_object["taum"]].n_observed_max){
    pda[observed_object["lm"]].n_observed_max = pda[observed_object["em"]].n_observed_max + pda[observed_object["mum"]].n_observed_max + pda[observed_object["taum"]].n_observed_max;
  }
  // adapt n_observed_max (lp-ep/mup/taup) !
  if (pda[observed_object["lp"]].n_observed_max > pda[observed_object["ep"]].n_observed_max + pda[observed_object["mup"]].n_observed_max + pda[observed_object["taup"]].n_observed_max){
    pda[observed_object["lp"]].n_observed_max = pda[observed_object["ep"]].n_observed_max + pda[observed_object["mup"]].n_observed_max + pda[observed_object["taup"]].n_observed_max;
  }

  // adapt n_observed_max (lep-lm/lp) !
  if (pda[observed_object["lep"]].n_observed_max > pda[observed_object["lm"]].n_observed_max + pda[observed_object["lp"]].n_observed_max){
    pda[observed_object["lep"]].n_observed_max = pda[observed_object["lm"]].n_observed_max + pda[observed_object["lp"]].n_observed_max;
  }

  // adapt n_observed_max (e-ep/em) !
  if (pda[observed_object["e"]].n_observed_max > pda[observed_object["em"]].n_observed_max + pda[observed_object["ep"]].n_observed_max){
    pda[observed_object["e"]].n_observed_max = pda[observed_object["em"]].n_observed_max + pda[observed_object["ep"]].n_observed_max;
  }
  // adapt n_observed_max (mu-mup/mum) !
  if (pda[observed_object["mu"]].n_observed_max > pda[observed_object["mum"]].n_observed_max + pda[observed_object["mup"]].n_observed_max){
    pda[observed_object["mu"]].n_observed_max = pda[observed_object["mum"]].n_observed_max + pda[observed_object["mup"]].n_observed_max;
  }
  // adapt n_observed_max (tau-taup/taum) !
  if (pda[observed_object["tau"]].n_observed_max > pda[observed_object["taum"]].n_observed_max + pda[observed_object["taup"]].n_observed_max){
    pda[observed_object["tau"]].n_observed_max = pda[observed_object["taum"]].n_observed_max + pda[observed_object["taup"]].n_observed_max;
  }

  // adapt n_observed_max (lep-e/mu/tau) !
  if (pda[observed_object["lep"]].n_observed_max > pda[observed_object["e"]].n_observed_max + pda[observed_object["mu"]].n_observed_max + pda[observed_object["tau"]].n_observed_max){
    pda[observed_object["lep"]].n_observed_max = pda[observed_object["e"]].n_observed_max + pda[observed_object["mu"]].n_observed_max + pda[observed_object["tau"]].n_observed_max;
  }
  // adapt n_observed_max (nu-ne/nm/nt) !
  if (pda[observed_object["nu"]].n_observed_max > pda[observed_object["ne"]].n_observed_max + pda[observed_object["nm"]].n_observed_max + pda[observed_object["nt"]].n_observed_max){
    pda[observed_object["nu"]].n_observed_max = pda[observed_object["ne"]].n_observed_max + pda[observed_object["nm"]].n_observed_max + pda[observed_object["nt"]].n_observed_max;
  }
  // adapt n_observed_max (nux-nex/nmx/ntx) !
  if (pda[observed_object["nux"]].n_observed_max > pda[observed_object["nex"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max){
    pda[observed_object["nux"]].n_observed_max = pda[observed_object["nex"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max;
  }

  // adapt n_observed_max (nua-nu/nux) !
  if (pda[observed_object["nua"]].n_observed_max > pda[observed_object["nu"]].n_observed_max + pda[observed_object["nux"]].n_observed_max){
    pda[observed_object["nua"]].n_observed_max = pda[observed_object["nu"]].n_observed_max + pda[observed_object["nux"]].n_observed_max;
  }
  // adapt n_observed_max (nea-nex/ne) !
  if (pda[observed_object["nea"]].n_observed_max > pda[observed_object["ne"]].n_observed_max + pda[observed_object["nex"]].n_observed_max){
    pda[observed_object["nea"]].n_observed_max = pda[observed_object["ne"]].n_observed_max + pda[observed_object["nex"]].n_observed_max;
  }
  // adapt n_observed_max (nma-nmx/nm) !
  if (pda[observed_object["nma"]].n_observed_max > pda[observed_object["nm"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max){
    pda[observed_object["nma"]].n_observed_max = pda[observed_object["nm"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max;
  }
  // adapt n_observed_max (nta-ntx/nt) !
  if (pda[observed_object["nta"]].n_observed_max > pda[observed_object["nt"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max){
    pda[observed_object["nta"]].n_observed_max = pda[observed_object["nt"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max;
  }

  // adapt n_observed_max (nua-nea/nma/nta) !
  if (pda[observed_object["nua"]].n_observed_max > pda[observed_object["nea"]].n_observed_max + pda[observed_object["nma"]].n_observed_max + pda[observed_object["nta"]].n_observed_max){
    pda[observed_object["nua"]].n_observed_max = pda[observed_object["nea"]].n_observed_max + pda[observed_object["nma"]].n_observed_max + pda[observed_object["nta"]].n_observed_max;
  }


  logger << LOG_DEBUG << endl << "Object definition after internal correlations: " << endl;
  logger << LOG_DEBUG << setw(20) << "object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    //    logger << LOG_DEBUG << setw(20) << object_list[i] << setw(20) << n_observed_min[i] << setw(20) << n_observed_max[i] << setw(20) << define_pT[i] << setw(20) << define_ET[i] << setw(20) << define_eta[i] << setw(20) << define_y[i] << endl;
    logger << LOG_DEBUG << setw(5) << i << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << setw(20) << pda[i].define_pT << setw(20) << pda[i].define_ET << setw(20) << pda[i].define_eta << setw(20) << pda[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


