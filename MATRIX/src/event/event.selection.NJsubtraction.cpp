#include "header.hpp"

void NJsubtraction_basic::event_selection_NJcut(int x_a){
  static Logger logger("NJsubtraction_basicevent_selection_NJcut");
  //void event_set::event_selection_NJcut(int x_a){
  //  static Logger logger("event_set::event_selection_NJcut");
  //void observable_set::event_selection_NJcut(int x_a){
  //  static Logger logger("observable_set::event_selection_NJcut");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // N-jettiness implementation (typically uses results from jet algorithm, thus located here.)
  // changed to simulate NJ implementation in QT environment:
  /*
  if (csi->type_contribution == "RRJ" ||
      csi->type_contribution == "RCJ" ||
      csi->type_contribution == "RVJ" ||
      csi->type_contribution == "RJ" ||
      csi->type_contribution == "L2RJ"){
    */
  ///  stringstream info_cut;

  if (switch_qTcut == 3){
    //      logger.newLine(LOG_DEBUG);
    for (int i_p = 1; i_p < esi->p_parton[x_a].size(); i_p++){
      logger << LOG_DEBUG << "p(hadronic frame)[" << i_p << "] = " << esi->particle_event[0][x_a][i_p].momentum << endl;
    }

    Njettiness_calculate_NJ_axes(x_a);

    logger.newLine(LOG_DEBUG);
    for (int i_j = 1; i_j < NJ_q_axes.size(); i_j++){
      logger << LOG_DEBUG << "NJ_q_axes[" << i_j << "] = " << NJ_q_axes[i_j] << endl;
      //	logger << LOG_DEBUG << "       [" << i_j << "] = " << NJ_Ei[i_j] * NJ_n_axes[i_j] << endl;
    }
    logger.newLine(LOG_DEBUG);
    for (int i_j = 1; i_j < NJ_n_axes.size(); i_j++){
      logger << LOG_DEBUG << "NJ_n_axes[" << i_j << "] = " << NJ_n_axes[i_j] << endl;
    }

    Njettiness_calculate_NJ_axes_frame(x_a);

    // new definition in respective rest frame:
    double tau_jettiness = 0.;
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[x_a].size(); i_p++){
      vector<double> delta_tau(3 + csi->n_jet_born);
      for (int i_q = 1; i_q < NJ_q_axes.size(); i_q++){
	delta_tau[i_q] = abs(2 * NJ_q_axes_frame[i_q] * NJ_p_parton_frame[i_p] / NJ_Qi[i_q]);
      }
      tau_jettiness += *min_element(delta_tau.begin() + 1, delta_tau.end());
    }

    // determination of esi->cut_ps[x_a] for taucut scan:
    if (tau_jettiness < min_qTcut){esi->cut_ps[x_a] = -1; return;}
    else {
      if (binning_qTcut == "linear"){
	esi->cut_ps[x_a] = GSL_MIN_INT(int((tau_jettiness - min_qTcut) / step_qTcut), n_qTcut - 1);
      }
      else {
	double temp_value_cut = tau_jettiness;
	for (int i_q = n_qTcut - 1; i_q >= 0; i_q--){
	  if (temp_value_cut > value_qTcut[i_q]){esi->cut_ps[x_a] = i_q; break;}
	}
      }
    }
  }
  


  // temporary N-jettiness implementation (still in the qT-subtraction framework, including contribution names etc.)
  if (switch_qTcut == -3){// to deactivate this for the moment !!!
    //  if (switch_qTcut == 3){
    if (csi->type_contribution == "RRA" ||
	csi->type_contribution == "RCA" ||
	csi->type_contribution == "RVA" ||
	csi->type_contribution == "RT" ||
	csi->type_contribution == "L2RT"){
      /*
      NJ_n_axes[1] = fourvector(1., 0., 0., 1.);
      NJ_n_axes[2] = fourvector(1., 0., 0., -1.);
      */
      double tau_jettiness = 0.;
      for (int i_j = 0; i_j < csi->n_jet_born; i_j++){
	fourvector temp_jet = esi->particle_event[esi->access_object["jet"]][x_a][i_j].momentum;
	double temp_jet_R = temp_jet.r();
	NJ_n_axes[3 + i_j] = fourvector(1., temp_jet.x1() / temp_jet_R, temp_jet.x2() / temp_jet_R, temp_jet.x3() / temp_jet_R);
      }
      // all available QCD partons
      stringstream temp;
      for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[x_a].size(); i_p++){	temp << setw(5) << esi->ps_runtime_jet_algorithm[x_a][i_p];}
      //      logger << LOG_INFO << "csi->n_jet_born = " << csi->n_jet_born << "   esi->ps_runtime_jet_algorithm[" << x_a << "].size() = " << esi->ps_runtime_jet_algorithm[x_a].size() << " --- " << temp.str() << endl;

      // calculate N-jettiness tau
      for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[x_a].size(); i_p++){
	vector<double> delta_tau(3 + csi->n_jet_born);
	for (int i_j = 1; i_j < NJ_n_axes.size(); i_j++){
	  //esi->particle_event[0][x_a][i_p]
	  delta_tau[i_j] = abs(NJ_n_axes[i_j] * esi->particle_event[0][x_a][esi->ps_runtime_jet_algorithm[x_a][i_p]].momentum);
	  //	  delta_tau[i_j] = NJ_n_axes[i_j] * esi->p_parton[x_a][esi->ps_runtime_jet_algorithm[x_a][i_p]];
	  /*
	  if (delta_tau[i_j] < 0.){
	    logger << LOG_INFO << "NJ_n_axes[" << i_j << "] * esi->particle_event[0][" << x_a << "][" << esi->ps_runtime_jet_algorithm[x_a][i_p] << "] is negative: delta_tau[" << i_j << "] = " << delta_tau[i_j] << endl;
	    delta_tau[i_j] = abs(delta_tau[i_j]);
	  }
	  */
	}
	//	logger << LOG_INFO << "i_p = " << i_p << "   tau_jettiness = " << tau_jettiness << endl;
	/*
	double min = 1.e99;
	for (int i_j = 0; i_j < NJ_n_axes.size(); i_j++){
	  if (delta_tau[i_j] < min){min = delta_tau[i_j];}
	}
	logger << LOG_INFO << "min = " << min << " =?= " << *min_element(delta_tau.begin(), delta_tau.end()) << " = *min_element(delta_tau.begin(), delta_tau.end())" << endl;
	*/

	tau_jettiness += *min_element(delta_tau.begin() + 1, delta_tau.end());
	//	tau_jettiness = tau_jettiness + *min_element(delta_tau.begin(), delta_tau.end());
	//	logger << LOG_INFO << "i_p = " << i_p << "   tau_jettiness = " << tau_jettiness << endl;
      }

      for (int i_j = 1; i_j < NJ_n_axes.size(); i_j++){
	logger << LOG_DEBUG << "NJ_n_axes[" << i_j << "] = " << NJ_n_axes[i_j] << endl;//"   delta_tau[" << i_j << "] = " << delta_tau[i_j] << endl;
      }
      logger << LOG_DEBUG << "binning_qTcut = " << binning_qTcut << endl;

      //      logger << LOG_INFO << "csi->n_jet_born = " << csi->n_jet_born << "   tau_jettiness = " << tau_jettiness << "   min_qTcut = " << min_qTcut << endl;
      if (tau_jettiness < min_qTcut){esi->cut_ps[x_a] = -1; return;}
      else {
	if (binning_qTcut == "linear"){
	  esi->cut_ps[x_a] = GSL_MIN_INT(int((tau_jettiness - min_qTcut) / step_qTcut), n_qTcut - 1);
	}
	else {
	  double temp_value_cut = tau_jettiness;
	  for (int i_q = n_qTcut - 1; i_q >= 0; i_q--){
	    if (temp_value_cut > value_qTcut[i_q]){esi->cut_ps[x_a] = i_q; break;}
	  }
	}
      ///	esi->cut_ps[x_a] = GSL_MIN_INT(int((tau_jettiness - min_qTcut) / step_qTcut), n_qTcut - 1);
      }
      logger << LOG_DEBUG << "esi->cut_ps[" << x_a << "] = " << esi->cut_ps[x_a] << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
