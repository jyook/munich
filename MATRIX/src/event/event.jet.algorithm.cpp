#include "header.hpp"

void event_set::jet_algorithm_flavour(){
  static Logger logger("jet_algorithm_flavour");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (jet_algorithm == 0){
    particle_jet.swap(particle_protojet);
    jet_flavour.swap(protojet_flavour);
    jet_parton_origin.swap(protojet_parton_origin);
  }
  else if (jet_algorithm == 1){sc_Ellis_Soper_flavour();}
  else if (jet_algorithm == 2){kTrun2_flavour();}
  else if (jet_algorithm == 3){antikT_flavour();}
  else if (jet_algorithm == 4){sc_Ellis_Soper_mod_flavour();}
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::sc_Ellis_Soper_flavour(){
  static Logger logger("sc_Ellis_Soper_flavour");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  int min_ot = 1;
  int min1 = 0;
  int min2 = 0;
  double d_min1 = 1.e99;
  vector<vector<double> > d_two(particle_protojet.size(), vector<double> (particle_protojet.size()));
  while (particle_protojet.size() > 0){
    d_min1 = 1.e99;
    min_ot = 1;
    for (int i = 0; i < particle_protojet.size(); i++){
      if (particle_protojet[i].pT2 < d_min1){d_min1 = particle_protojet[i].pT2; min1 = i;}
    }
    for (int i = 0; i < particle_protojet.size(); i++){
      for (int j = i + 1; j < particle_protojet.size(); j++){
	if (particle_protojet[i].pT2 < particle_protojet[j].pT2){d_two[i][j] = particle_protojet[i].pT2;}
	else{d_two[i][j] = particle_protojet[j].pT2;}
	if (jet_R_definition == 0){d_two[i][j] = d_two[i][j] * R2_eta(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	else if (jet_R_definition == 1){d_two[i][j] = d_two[i][j] * R2_rapidity(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	if (d_two[i][j] < d_min1){d_min1 = d_two[i][j]; min1 = i; min2 = j; min_ot = 2;}
      }
    }
    if (min_ot == 1){
      transfer_protojet_to_jet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, particle_jet, jet_flavour, jet_parton_origin);
    }
    else {
      double phi_i = particle_protojet[min1].phi;
      double phi_j = particle_protojet[min2].phi;
      double ET_i = particle_protojet[min1].pT;
      double ET_j = particle_protojet[min2].pT;
      double ET = ET_i + ET_j;
      double deltaphi = abs(phi_i - phi_j);
      double phi;
      if (deltaphi <= pi){phi = (ET_i * phi_i + ET_j * phi_j) / ET;}
      else {
	if (phi_i > pi){phi_j = phi_j + f2pi;}
	else           {phi_i = phi_i + f2pi;}
	phi = (ET_i * phi_i + ET_j * phi_j) / ET;
	if (phi > f2pi){phi -= f2pi;}
      }
      /*
      double eta = (ET_i * particle_protojet[min1].eta + ET_j * particle_protojet[min2].eta) / ET;
      double pz = ET / tan(2 * atan(exp(-eta)));
      particle new_protojet(fourvector (sqrt(pow(ET, 2) + pow(pz, 2)), cos(phi) * ET, sin(phi) * ET, pz));
      combine_protojet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, min2, new_protojet);
      */
      combine_protojet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, min2);
      // not the correct combination procedure !!!
    }
  }
    
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::sc_Ellis_Soper_mod_flavour(){
  static Logger logger("event_set::sc_Ellis_Soper_mod_flavour");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  int min_ot = 1;
  int min1 = 0;
  int min2 = 0;
  double d_min1 = 1.e99;//, delta_phi;
  vector<vector<double> > d_two(particle_protojet.size(), vector<double> (particle_protojet.size()));
  while (particle_protojet.size() > 0){
    d_min1 = 1.e99;
    min_ot = 1;
    for (int i = 0; i < particle_protojet.size(); i++){
      if (particle_protojet[i].pT2 < d_min1){d_min1 = particle_protojet[i].pT2; min1 = i;}
    }
    for (int i = 0; i < particle_protojet.size(); i++){
      for (int j = i + 1; j < particle_protojet.size(); j++){
	if (particle_protojet[i].pT2 < particle_protojet[j].pT2){d_two[i][j] = particle_protojet[i].pT2;}
	else{d_two[i][j] = particle_protojet[j].pT2;}
	if (jet_R_definition == 0){d_two[i][j] = d_two[i][j] * R2_eta(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	else if (jet_R_definition == 1){d_two[i][j] = d_two[i][j] * R2_rapidity(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	if (d_two[i][j] < d_min1){d_min1 = d_two[i][j]; min1 = i; min2 = j; min_ot = 2;}
      }
    }
    if (min_ot == 1){
      transfer_protojet_to_jet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, particle_jet, jet_flavour, jet_parton_origin);
    }
    else {
      //, new_protojet      particle new_protojet = particle_protojet[min1] + particle_protojet[min2];
      combine_protojet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, min2);
    }
  }
    
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void event_set::kTrun2_flavour(){
  static Logger logger("event_set::kTrun2_flavour");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  int min_ot = 1;
  int min1 = 0;
  int min2 = 0;
  double d_min1 = 1.e99;//, delta_phi;
  vector<vector<double> > d_two(particle_protojet.size(), vector<double> (particle_protojet.size()));
  while (particle_protojet.size() > 0){
    d_min1 = 1.e99;
    min_ot = 1;
    for (int i = 0; i < particle_protojet.size(); i++){
      if (particle_protojet[i].pT2 < d_min1){d_min1 = particle_protojet[i].pT2; min1 = i;}
    }
    for (int i = 0; i < particle_protojet.size(); i++){
      for (int j = i + 1; j < particle_protojet.size(); j++){
	if (particle_protojet[i].pT2 < particle_protojet[j].pT2){d_two[i][j] = particle_protojet[i].pT2;}
	else{d_two[i][j] = particle_protojet[j].pT2;}
	if (jet_R_definition == 0){d_two[i][j] = d_two[i][j] * R2_eta(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	else if (jet_R_definition == 1){d_two[i][j] = d_two[i][j] * R2_rapidity(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	if (d_two[i][j] < d_min1){d_min1 = d_two[i][j]; min1 = i; min2 = j; min_ot = 2;}
      }
    }
    if (min_ot == 1){
      transfer_protojet_to_jet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, particle_jet, jet_flavour, jet_parton_origin);
    }
    else{
      combine_protojet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, min2);
    }
  }
    
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::antikT_flavour(){
  static Logger logger("event_set::antikT_flavour");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  int min_ot = 1;
  int min1 = 0;
  int min2 = 0;
  double d_min1 = 1.e99;//, delta_phi;
  vector<vector<double> > d_two(particle_protojet.size(), vector<double> (particle_protojet.size()));
  while (particle_protojet.size() > 0){
    d_min1 = 1.e99;
    min_ot = 1;
    min1 = 0;
    for (int i = 0; i < particle_protojet.size(); i++){
      if (1. / particle_protojet[i].pT2 < d_min1){d_min1 = 1. / particle_protojet[i].pT2; min1 = i;}
    }
    for (int i = 0; i < particle_protojet.size(); i++){
      for (int j = i + 1; j < particle_protojet.size(); j++){
	if (particle_protojet[i].pT2 > particle_protojet[j].pT2){d_two[i][j] = 1. / particle_protojet[i].pT2;}
	else {d_two[i][j] = 1. / particle_protojet[j].pT2;}
	if (jet_R_definition == 0){d_two[i][j] = d_two[i][j] * R2_eta(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	else if (jet_R_definition == 1){d_two[i][j] = d_two[i][j] * R2_rapidity(particle_protojet[i], particle_protojet[j]) / jet_R2;}
	if (d_two[i][j] < d_min1){d_min1 = d_two[i][j]; min1 = i; min2 = j; min_ot = 2;}
      }
    }
    if (min_ot == 1){
      transfer_protojet_to_jet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, particle_jet, jet_flavour, jet_parton_origin);
    }
    else {
      //, new_protojet      particle new_protojet = particle_protojet[min1] + particle_protojet[min2];
      combine_protojet(particle_protojet, protojet_flavour, protojet_parton_origin, min1, min2);
    }
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void transfer_protojet_to_jet(vector<particle> & particle_protojet, vector<vector<int> > & protojet_flavour, vector<vector<int> > & protojet_parton_origin, int pj1, vector<particle> & particle_jet, vector<vector<int> > & jet_flavour, vector<vector<int> > & jet_parton_origin){
  
  particle_jet.push_back(particle_protojet[pj1]);
  jet_flavour.push_back(protojet_flavour[pj1]);
  jet_parton_origin.push_back(protojet_parton_origin[pj1]);
  particle_protojet.erase(particle_protojet.begin() + pj1);
  protojet_flavour.erase(protojet_flavour.begin() + pj1);
  protojet_parton_origin.erase(protojet_parton_origin.begin() + pj1);
  
}

void combine_protojet(vector<particle> & particle_protojet, vector<vector<int> > & protojet_flavour, vector<vector<int> > & protojet_parton_origin, int pj1, int pj2){

  particle_protojet.push_back(particle_protojet[pj1] + particle_protojet[pj2]);
  vector<int> temp_flavour(0);
  for (int i_p = 0; i_p < protojet_flavour[pj1].size(); i_p++){temp_flavour.push_back(protojet_flavour[pj1][i_p]);}
  for (int i_p = 0; i_p < protojet_flavour[pj2].size(); i_p++){temp_flavour.push_back(protojet_flavour[pj2][i_p]);}

  vector<int> temp_parton_origin(0);
  for (int i_p = 0; i_p < protojet_parton_origin[pj1].size(); i_p++){temp_parton_origin.push_back(protojet_parton_origin[pj1][i_p]);}
  for (int i_p = 0; i_p < protojet_parton_origin[pj2].size(); i_p++){temp_parton_origin.push_back(protojet_parton_origin[pj2][i_p]);}

  protojet_flavour.push_back(temp_flavour);
  protojet_parton_origin.push_back(temp_parton_origin);

  particle_protojet.erase(particle_protojet.begin() + pj2);
  particle_protojet.erase(particle_protojet.begin() + pj1);

  protojet_flavour.erase(protojet_flavour.begin() + pj2);
  protojet_flavour.erase(protojet_flavour.begin() + pj1);

  protojet_parton_origin.erase(protojet_parton_origin.begin() + pj2);
  protojet_parton_origin.erase(protojet_parton_origin.begin() + pj1);
  
}





