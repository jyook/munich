#include "header.hpp"

void qTsubtraction_basic::event_selection_qTcut(int x_a){
  static Logger logger("qTsubtraction_basic::event_selection_qTcut");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  stringstream info_cut;
  info_cut << "csi->n_particle_born = " << csi->n_particle_born << endl;

  QT_Q = fourvector();
  info_cut << "QT_Q = " << QT_Q << endl;
  for (int i_p = 3; i_p < 3 + csi->n_particle_born; i_p++){
    QT_Q = QT_Q + esi->p_parton[x_a][i_p];
    info_cut << "QT_Q = " << QT_Q << endl;
  }
  QT_QT = QT_Q.pT();
  info_cut << "QT_QT = " << QT_QT << endl;
  if (switch_qTcut == 1){
    QT_sqrtQ2 = QT_Q.m();
    info_cut << "QT_sqrtQ2 = " << QT_sqrtQ2 << endl;
    if (QT_QT / QT_sqrtQ2 < min_qTcut / 100.){(esi->cut_ps)[x_a] = -1;}

      //      return;}

    else {
      if (binning_qTcut == "linear"){
	info_cut << "(QT_QT / QT_sqrtQ2 - min_qTcut / 100.) * 100) / step_qTcut = " << (QT_QT / QT_sqrtQ2 - min_qTcut / 100.) * 100 / step_qTcut << endl;
	double temp_rcut = ((QT_QT / QT_sqrtQ2 - min_qTcut / 100.) * 100) / step_qTcut;
	esi->cut_ps[x_a] = GSL_MIN_INT(int(temp_rcut), n_qTcut - 1);
	if (esi->cut_ps[x_a] < 0 && temp_rcut > double(n_qTcut - 1)){esi->cut_ps[x_a] = n_qTcut - 1;}
      }
      else {
	double temp_value_cut = QT_QT / QT_sqrtQ2 * 100;
	for (int i_q = n_qTcut - 1; i_q >= 0; i_q--){
	  if (temp_value_cut > value_qTcut[i_q]){esi->cut_ps[x_a] = i_q; break;}
	}
      }
    }
    //    logger << LOG_DEBUG << "esi->cut_ps[" << x_a << "] = " << (esi->cut_ps)[x_a] << endl;
    //    logger << LOG_DEBUG << endl << info_cut.str();

  }
  else if (switch_qTcut == 2){
    if (QT_QT < min_qTcut){esi->cut_ps[x_a] = -1;}// return;}
    else {
      if (binning_qTcut == "linear"){
	esi->cut_ps[x_a] = GSL_MIN_INT(int((QT_QT - min_qTcut) / step_qTcut), n_qTcut - 1);
      }
      else {
	double temp_value_cut = QT_QT;
	for (int i_q = n_qTcut - 1; i_q >= 0; i_q--){
	  if (temp_value_cut > value_qTcut[i_q]){esi->cut_ps[x_a] = i_q; break;}
	}
      }
    }
  }

  if (esi->switch_output_cutinfo){
    info_cut << "[" << setw(2) << x_a << "]" << "   (After (qT subtraction)):   (esi->cut_ps[" << x_a << "] = " << esi->cut_ps[x_a] << ")." << endl;
    logger << LOG_DEBUG << endl << info_cut.str();
  }
  logger << LOG_DEBUG << "esi->cut_ps[" << x_a << "] = " << (esi->cut_ps)[x_a] << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
