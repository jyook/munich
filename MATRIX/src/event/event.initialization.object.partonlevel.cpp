#include "header.hpp"

void event_set::determine_object_partonlevel(){
  static  Logger logger("event_set::determine_object_partonlevel");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // !!! Should be modified such that user input is never overwritten !!!

  // adapt n_observed_max trivially to parton-level particles
  // needs to be modified if partons can change their categories
  // (like mis-identification of bjets as ljets etc.)
  for (int i_p = 1; i_p < object_list.size(); i_p++){
    logger << LOG_INFO << "object_list[" << setw(2) << i_p << "] = " << object_list[i_p] << "   n_partonlevel = " << pda[i_p].n_partonlevel << "   n_observed_max = " << pda[i_p].n_observed_max << endl;
    if (pda[i_p].n_partonlevel < pda[i_p].n_observed_max){
      if (object_list[i_p] == "ljet"){
	if (pda[observed_object["jet"]].n_partonlevel < pda[i_p].n_observed_max){pda[i_p].n_observed_max = pda[observed_object["jet"]].n_partonlevel;}
      }
      else if (object_category[i_p] == -1){logger << LOG_DEBUG << object_list[i_p] << " is a user-defined particle -> no modification." << endl;}
      else {pda[i_p].n_observed_max = pda[i_p].n_partonlevel;}
    }
  }
  //  if (pda[observed_object["jet"]] < pda[observed_object["jet"]]){pda[observed_object["jet"]] = pda[observed_object["jet"]].n_observed_min;}


  // adapt n_observed_max to parton-level particles (tjet-top/atop)
  if (pda[observed_object["atop"]].n_partonlevel < pda[observed_object["atop"]].n_observed_max){pda[observed_object["atop"]].n_observed_max = pda[observed_object["atop"]].n_partonlevel;}
  if (pda[observed_object["top"]].n_partonlevel < pda[observed_object["top"]].n_observed_max){pda[observed_object["top"]].n_observed_max = pda[observed_object["top"]].n_partonlevel;}

  if (pda[observed_object["tjet"]].n_partonlevel == pda[observed_object["tjet"]].n_observed_max){
    pda[observed_object["atop"]].n_observed_max = pda[observed_object["atop"]].n_partonlevel;
    pda[observed_object["top"]].n_observed_max = pda[observed_object["top"]].n_partonlevel;
  }
  else if (pda[observed_object["top"]].n_partonlevel == 0){
    if (pda[observed_object["atop"]].n_observed_max <= pda[observed_object["tjet"]].n_observed_max){pda[observed_object["tjet"]].n_observed_max = pda[observed_object["atop"]].n_observed_max;}
    else {pda[observed_object["atop"]].n_observed_max = pda[observed_object["tjet"]].n_observed_max;}
  }
  else if (pda[observed_object["atop"]].n_partonlevel == 0){
    if (pda[observed_object["top"]].n_observed_max <= pda[observed_object["tjet"]].n_observed_max){pda[observed_object["tjet"]].n_observed_max = pda[observed_object["top"]].n_observed_max;}
    else {pda[observed_object["top"]].n_observed_max = pda[observed_object["tjet"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (tjet-top/atop)
  if (pda[observed_object["tjet"]].n_partonlevel == pda[observed_object["tjet"]].n_observed_min){
    pda[observed_object["atop"]].n_observed_min = pda[observed_object["atop"]].n_partonlevel;
    pda[observed_object["top"]].n_observed_min = pda[observed_object["top"]].n_partonlevel;
  }
  else if (pda[observed_object["top"]].n_partonlevel == 0){
    if (pda[observed_object["atop"]].n_observed_min >= pda[observed_object["tjet"]].n_observed_min){pda[observed_object["tjet"]].n_observed_min = pda[observed_object["atop"]].n_observed_min;}
    else {pda[observed_object["atop"]].n_observed_min = pda[observed_object["tjet"]].n_observed_min;}
  }
  else if (pda[observed_object["atop"]].n_partonlevel == 0){
    if (pda[observed_object["top"]].n_observed_min >= pda[observed_object["tjet"]].n_observed_min){pda[observed_object["tjet"]].n_observed_min = pda[observed_object["top"]].n_observed_min;}
    else {pda[observed_object["top"]].n_observed_min = pda[observed_object["tjet"]].n_observed_min;}
  }


  // adapt n_observed_max to parton-level particles (bjet-bjet_b/bjet_bx)
  if (pda[observed_object["bjet_bx"]].n_partonlevel < pda[observed_object["bjet_bx"]].n_observed_max){pda[observed_object["bjet_bx"]].n_observed_max = pda[observed_object["bjet_bx"]].n_partonlevel;}
  if (pda[observed_object["bjet_b"]].n_partonlevel < pda[observed_object["bjet_b"]].n_observed_max){pda[observed_object["bjet_b"]].n_observed_max = pda[observed_object["bjet_b"]].n_partonlevel;}

  if (pda[observed_object["bjet"]].n_partonlevel == pda[observed_object["bjet"]].n_observed_max){
    pda[observed_object["bjet_bx"]].n_observed_max = pda[observed_object["bjet_bx"]].n_partonlevel;
    pda[observed_object["bjet_b"]].n_observed_max = pda[observed_object["bjet_b"]].n_partonlevel;
  }
  else if (pda[observed_object["bjet_b"]].n_partonlevel == 0){
    if (pda[observed_object["bjet_bx"]].n_observed_max <= pda[observed_object["bjet"]].n_observed_max){pda[observed_object["bjet"]].n_observed_max = pda[observed_object["bjet_bx"]].n_observed_max;}
    else {pda[observed_object["bjet_bx"]].n_observed_max = pda[observed_object["bjet"]].n_observed_max;}
  }
  else if (pda[observed_object["bjet_bx"]].n_partonlevel == 0){
    if (pda[observed_object["bjet_b"]].n_observed_max <= pda[observed_object["bjet"]].n_observed_max){pda[observed_object["bjet"]].n_observed_max = pda[observed_object["bjet_b"]].n_observed_max;}
    else {pda[observed_object["bjet_b"]].n_observed_max = pda[observed_object["bjet"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (bjet-bjet_b/bjet_bx)
  if (pda[observed_object["bjet"]].n_partonlevel == pda[observed_object["bjet"]].n_observed_min){
    pda[observed_object["bjet_bx"]].n_observed_min = pda[observed_object["bjet_bx"]].n_partonlevel;
    pda[observed_object["bjet_b"]].n_observed_min = pda[observed_object["bjet_b"]].n_partonlevel;
  }
  else if (pda[observed_object["bjet_b"]].n_partonlevel == 0){
    if (pda[observed_object["bjet_bx"]].n_observed_min >= pda[observed_object["bjet"]].n_observed_min){pda[observed_object["bjet"]].n_observed_min = pda[observed_object["bjet_bx"]].n_observed_min;}
    else {pda[observed_object["bjet_bx"]].n_observed_min = pda[observed_object["bjet"]].n_observed_min;}
  }
  else if (pda[observed_object["bjet_bx"]].n_partonlevel == 0){
    if (pda[observed_object["bjet_b"]].n_observed_min >= pda[observed_object["bjet"]].n_observed_min){pda[observed_object["bjet"]].n_observed_min = pda[observed_object["bjet_b"]].n_observed_min;}
    else {pda[observed_object["bjet_b"]].n_observed_min = pda[observed_object["bjet"]].n_observed_min;}
  }


  // adapt n_observed_max to parton-level particles (lep-lp/lm) !
  if (pda[observed_object["lep"]].n_partonlevel < pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["lep"]].n_partonlevel;}

  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lep"]].n_observed_max){
    pda[observed_object["lm"]].n_observed_max = pda[observed_object["lm"]].n_partonlevel;
    pda[observed_object["lp"]].n_observed_max = pda[observed_object["lp"]].n_partonlevel;
  }
  else if (pda[observed_object["lp"]].n_partonlevel == 0){
    if (pda[observed_object["lm"]].n_observed_max <= pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["lm"]].n_observed_max;}
    else {pda[observed_object["lm"]].n_observed_max = pda[observed_object["lep"]].n_observed_max;}
  }
  else if (pda[observed_object["lm"]].n_partonlevel == 0){
    if (pda[observed_object["lp"]].n_observed_max <= pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["lp"]].n_observed_max;}
    else {pda[observed_object["lp"]].n_observed_max = pda[observed_object["lep"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (lep-lp/lm) !
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lep"]].n_observed_min){
    if (pda[observed_object["lm"]].n_observed_min < pda[observed_object["lm"]].n_partonlevel){pda[observed_object["lm"]].n_observed_min = pda[observed_object["lm"]].n_partonlevel;}
    if (pda[observed_object["lp"]].n_observed_min < pda[observed_object["lp"]].n_partonlevel){pda[observed_object["lp"]].n_observed_min = pda[observed_object["lp"]].n_partonlevel;}
  }
  /*
  else if (pda[observed_object["lp"]].n_partonlevel == 0){
    if (pda[observed_object["lm"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
    else {pda[observed_object["lm"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  else if (pda[observed_object["lm"]].n_partonlevel == 0){
    if (pda[observed_object["lp"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["lp"]].n_observed_min;}
    else {pda[observed_object["lp"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  */
  /*
  // adapt n_observed_min to parton-level particles (lep-lp/lm) !
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lep"]].n_observed_min){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["lm"]].n_partonlevel;
    pda[observed_object["lp"]].n_observed_min = pda[observed_object["lp"]].n_partonlevel;
  }
  else if (pda[observed_object["lp"]].n_partonlevel == 0){
    if (pda[observed_object["lm"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
    else {pda[observed_object["lm"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  else if (pda[observed_object["lm"]].n_partonlevel == 0){
    if (pda[observed_object["lp"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["lp"]].n_observed_min;}
    else {pda[observed_object["lp"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  */

  // adapt n_observed_min to parton-level particles (lep-e/mu/tau) !
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lep"]].n_observed_min){
    if (pda[observed_object["e"]].n_observed_min < pda[observed_object["e"]].n_partonlevel){pda[observed_object["e"]].n_observed_min = pda[observed_object["e"]].n_partonlevel;}
    if (pda[observed_object["mu"]].n_observed_min < pda[observed_object["mu"]].n_partonlevel){pda[observed_object["mu"]].n_observed_min = pda[observed_object["mu"]].n_partonlevel;}
    if (pda[observed_object["tau"]].n_observed_min < pda[observed_object["tau"]].n_partonlevel){pda[observed_object["tau"]].n_observed_min = pda[observed_object["tau"]].n_partonlevel;}
    /*
    // modify: This could prevent one from switching off contributions that have less partons off that flavour than individually required by input
    pda[observed_object["e"]].n_observed_min = pda[observed_object["e"]].n_partonlevel;
    pda[observed_object["mu"]].n_observed_min = pda[observed_object["mu"]].n_partonlevel;
    pda[observed_object["tau"]].n_observed_min = pda[observed_object["tau"]].n_partonlevel;
    */
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_partonlevel == 0)){
    if (pda[observed_object["tau"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["tau"]].n_observed_min;}
    else {pda[observed_object["tau"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_observed_min > pda[observed_object["lep"]].n_observed_min)){
    if (pda[observed_object["mu"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["mu"]].n_observed_min;}
    else {pda[observed_object["mu"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  else if ((pda[observed_object["mu"]].n_partonlevel == 0) && (pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_min > pda[observed_object["lep"]].n_observed_min)){
    if (pda[observed_object["e"]].n_observed_min >= pda[observed_object["lep"]].n_observed_min){pda[observed_object["lep"]].n_observed_min = pda[observed_object["e"]].n_observed_min;}
    else {pda[observed_object["e"]].n_observed_min = pda[observed_object["lep"]].n_observed_min;}
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_observed_min + pda[observed_object["tau"]].n_observed_min > pda[observed_object["lep"]].n_observed_min)){
    pda[observed_object["lep"]].n_observed_min = pda[observed_object["mu"]].n_observed_min + pda[observed_object["tau"]].n_observed_min;
  }
  else if ((pda[observed_object["mu"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_min + pda[observed_object["tau"]].n_observed_min > pda[observed_object["lep"]].n_observed_min)){
    pda[observed_object["lep"]].n_observed_min = pda[observed_object["e"]].n_observed_min + pda[observed_object["tau"]].n_observed_min;
  }
  else if ((pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_min + pda[observed_object["mu"]].n_observed_min > pda[observed_object["lep"]].n_observed_min)){
    pda[observed_object["lep"]].n_observed_min = pda[observed_object["e"]].n_observed_min + pda[observed_object["mu"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (lep-e/mu/tau) !
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lep"]].n_observed_max){
    pda[observed_object["e"]].n_observed_max = pda[observed_object["e"]].n_partonlevel;
    pda[observed_object["mu"]].n_observed_max = pda[observed_object["mu"]].n_partonlevel;
    pda[observed_object["tau"]].n_observed_max = pda[observed_object["tau"]].n_partonlevel;
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_partonlevel == 0)){
    if (pda[observed_object["tau"]].n_observed_max <= pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["tau"]].n_observed_max;}
    else {pda[observed_object["tau"]].n_observed_max = pda[observed_object["lep"]].n_observed_max;}
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_observed_max > pda[observed_object["lep"]].n_observed_max)){
    if (pda[observed_object["mu"]].n_observed_max <= pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["mu"]].n_observed_max;}
    else {pda[observed_object["mu"]].n_observed_max = pda[observed_object["lep"]].n_observed_max;}
  }
  else if ((pda[observed_object["mu"]].n_partonlevel == 0) && (pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_max > pda[observed_object["lep"]].n_observed_max)){
    if (pda[observed_object["e"]].n_observed_max <= pda[observed_object["lep"]].n_observed_max){pda[observed_object["lep"]].n_observed_max = pda[observed_object["e"]].n_observed_max;}
    else {pda[observed_object["e"]].n_observed_max = pda[observed_object["lep"]].n_observed_max;}
  }
  else if ((pda[observed_object["e"]].n_partonlevel == 0) && (pda[observed_object["mu"]].n_observed_max + pda[observed_object["tau"]].n_observed_max < pda[observed_object["lep"]].n_observed_max)){
    pda[observed_object["lep"]].n_observed_max = pda[observed_object["mu"]].n_observed_max + pda[observed_object["tau"]].n_observed_max;
  }
  else if ((pda[observed_object["mu"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_max + pda[observed_object["tau"]].n_observed_max < pda[observed_object["lep"]].n_observed_max)){
    pda[observed_object["lep"]].n_observed_max = pda[observed_object["e"]].n_observed_max + pda[observed_object["tau"]].n_observed_max;
  }
  else if ((pda[observed_object["tau"]].n_partonlevel == 0) && (pda[observed_object["e"]].n_observed_max + pda[observed_object["mu"]].n_observed_max < pda[observed_object["lep"]].n_observed_max)){
    pda[observed_object["lep"]].n_observed_max = pda[observed_object["e"]].n_observed_max + pda[observed_object["mu"]].n_observed_max;
  }

  // adapt n_observed_min to parton-level particles (lm-em/mum/taum) !
  if (pda[observed_object["lm"]].n_partonlevel == pda[observed_object["lm"]].n_observed_min){
    pda[observed_object["em"]].n_observed_min = pda[observed_object["em"]].n_partonlevel;
    pda[observed_object["mum"]].n_observed_min = pda[observed_object["mum"]].n_partonlevel;
    pda[observed_object["taum"]].n_observed_min = pda[observed_object["taum"]].n_partonlevel;
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_partonlevel == 0)){
    if (pda[observed_object["taum"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["taum"]].n_observed_min;}
    else {pda[observed_object["taum"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    if (pda[observed_object["mum"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["mum"]].n_observed_min;}
    else {pda[observed_object["mum"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["mum"]].n_partonlevel == 0) && (pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    if (pda[observed_object["em"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["em"]].n_observed_min;}
    else {pda[observed_object["em"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_observed_min + pda[observed_object["taum"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["mum"]].n_observed_min + pda[observed_object["taum"]].n_observed_min;
  }
  else if ((pda[observed_object["mum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_min + pda[observed_object["taum"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["em"]].n_observed_min + pda[observed_object["taum"]].n_observed_min;
  }
  else if ((pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_min + pda[observed_object["mum"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["em"]].n_observed_min + pda[observed_object["mum"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (lm-em/mum/taum) !
  if (pda[observed_object["lm"]].n_partonlevel == pda[observed_object["lm"]].n_observed_max){
    pda[observed_object["em"]].n_observed_max = pda[observed_object["em"]].n_partonlevel;
    pda[observed_object["mum"]].n_observed_max = pda[observed_object["mum"]].n_partonlevel;
    pda[observed_object["taum"]].n_observed_max = pda[observed_object["taum"]].n_partonlevel;
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_partonlevel == 0)){
    if (pda[observed_object["taum"]].n_observed_max <= pda[observed_object["lm"]].n_observed_max){pda[observed_object["lm"]].n_observed_max = pda[observed_object["taum"]].n_observed_max;}
    else {pda[observed_object["taum"]].n_observed_max = pda[observed_object["lm"]].n_observed_max;}
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_observed_max > pda[observed_object["lm"]].n_observed_max)){
    if (pda[observed_object["mum"]].n_observed_max <= pda[observed_object["lm"]].n_observed_max){pda[observed_object["lm"]].n_observed_max = pda[observed_object["mum"]].n_observed_max;}
    else {pda[observed_object["mum"]].n_observed_max = pda[observed_object["lm"]].n_observed_max;}
  }
  else if ((pda[observed_object["mum"]].n_partonlevel == 0) && (pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_max > pda[observed_object["lm"]].n_observed_max)){
    if (pda[observed_object["em"]].n_observed_max <= pda[observed_object["lm"]].n_observed_max){pda[observed_object["lm"]].n_observed_max = pda[observed_object["em"]].n_observed_max;}
    else {pda[observed_object["em"]].n_observed_max = pda[observed_object["lm"]].n_observed_max;}
  }
  else if ((pda[observed_object["em"]].n_partonlevel == 0) && (pda[observed_object["mum"]].n_observed_max + pda[observed_object["taum"]].n_observed_max < pda[observed_object["lm"]].n_observed_max)){
    pda[observed_object["lm"]].n_observed_max = pda[observed_object["mum"]].n_observed_max + pda[observed_object["taum"]].n_observed_max;
  }
  else if ((pda[observed_object["mum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_max + pda[observed_object["taum"]].n_observed_max < pda[observed_object["lm"]].n_observed_max)){
    pda[observed_object["lm"]].n_observed_max = pda[observed_object["em"]].n_observed_max + pda[observed_object["taum"]].n_observed_max;
  }
  else if ((pda[observed_object["taum"]].n_partonlevel == 0) && (pda[observed_object["em"]].n_observed_max + pda[observed_object["mum"]].n_observed_max < pda[observed_object["lm"]].n_observed_max)){
    pda[observed_object["lm"]].n_observed_max = pda[observed_object["em"]].n_observed_max + pda[observed_object["mum"]].n_observed_max;
  }

  // adapt n_observed_min to parton-level particles (lp-ep/mup/taup) !
  if (pda[observed_object["lp"]].n_partonlevel == pda[observed_object["lp"]].n_observed_min){
    pda[observed_object["ep"]].n_observed_min = pda[observed_object["ep"]].n_partonlevel;
    pda[observed_object["mup"]].n_observed_min = pda[observed_object["mup"]].n_partonlevel;
    pda[observed_object["taup"]].n_observed_min = pda[observed_object["taup"]].n_partonlevel;
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_partonlevel == 0)){
    if (pda[observed_object["taup"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["taup"]].n_observed_min;}
    else {pda[observed_object["taup"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    if (pda[observed_object["mup"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["mup"]].n_observed_min;}
    else {pda[observed_object["mup"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["mup"]].n_partonlevel == 0) && (pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    if (pda[observed_object["ep"]].n_observed_min >= pda[observed_object["lm"]].n_observed_min){pda[observed_object["lm"]].n_observed_min = pda[observed_object["ep"]].n_observed_min;}
    else {pda[observed_object["ep"]].n_observed_min = pda[observed_object["lm"]].n_observed_min;}
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_observed_min + pda[observed_object["taup"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["mup"]].n_observed_min + pda[observed_object["taup"]].n_observed_min;
  }
  else if ((pda[observed_object["mup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_min + pda[observed_object["taup"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["ep"]].n_observed_min + pda[observed_object["taup"]].n_observed_min;
  }
  else if ((pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_min + pda[observed_object["mup"]].n_observed_min > pda[observed_object["lm"]].n_observed_min)){
    pda[observed_object["lm"]].n_observed_min = pda[observed_object["ep"]].n_observed_min + pda[observed_object["mup"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (lp-ep/mup/taup) !
  if (pda[observed_object["lp"]].n_partonlevel == pda[observed_object["lp"]].n_observed_max){
    pda[observed_object["ep"]].n_observed_max = pda[observed_object["ep"]].n_partonlevel;
    pda[observed_object["mup"]].n_observed_max = pda[observed_object["mup"]].n_partonlevel;
    pda[observed_object["taup"]].n_observed_max = pda[observed_object["taup"]].n_partonlevel;
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_partonlevel == 0)){
    if (pda[observed_object["taup"]].n_observed_max <= pda[observed_object["lp"]].n_observed_max){pda[observed_object["lp"]].n_observed_max = pda[observed_object["taup"]].n_observed_max;}
    else {pda[observed_object["taup"]].n_observed_max = pda[observed_object["lp"]].n_observed_max;}
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_observed_max > pda[observed_object["lp"]].n_observed_max)){
    if (pda[observed_object["mup"]].n_observed_max <= pda[observed_object["lp"]].n_observed_max){pda[observed_object["lp"]].n_observed_max = pda[observed_object["mup"]].n_observed_max;}
    else {pda[observed_object["mup"]].n_observed_max = pda[observed_object["lp"]].n_observed_max;}
  }
  else if ((pda[observed_object["mup"]].n_partonlevel == 0) && (pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_max > pda[observed_object["lp"]].n_observed_max)){
    if (pda[observed_object["ep"]].n_observed_max <= pda[observed_object["lp"]].n_observed_max){pda[observed_object["lp"]].n_observed_max = pda[observed_object["ep"]].n_observed_max;}
    else {pda[observed_object["ep"]].n_observed_max = pda[observed_object["lp"]].n_observed_max;}
  }
  else if ((pda[observed_object["ep"]].n_partonlevel == 0) && (pda[observed_object["mup"]].n_observed_max + pda[observed_object["taup"]].n_observed_max < pda[observed_object["lp"]].n_observed_max)){
    pda[observed_object["lp"]].n_observed_max = pda[observed_object["mup"]].n_observed_max + pda[observed_object["taup"]].n_observed_max;
  }
  else if ((pda[observed_object["mup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_max + pda[observed_object["taup"]].n_observed_max < pda[observed_object["lp"]].n_observed_max)){
    pda[observed_object["lp"]].n_observed_max = pda[observed_object["ep"]].n_observed_max + pda[observed_object["taup"]].n_observed_max;
  }
  else if ((pda[observed_object["taup"]].n_partonlevel == 0) && (pda[observed_object["ep"]].n_observed_max + pda[observed_object["mup"]].n_observed_max < pda[observed_object["lp"]].n_observed_max)){
    pda[observed_object["lp"]].n_observed_max = pda[observed_object["ep"]].n_observed_max + pda[observed_object["mup"]].n_observed_max;
  }

  // adapt n_observed_max to parton-level particles (e-ep/em) !
  if (pda[observed_object["e"]].n_partonlevel == pda[observed_object["e"]].n_observed_max){
    pda[observed_object["em"]].n_observed_max = pda[observed_object["em"]].n_partonlevel;
    pda[observed_object["ep"]].n_observed_max = pda[observed_object["ep"]].n_partonlevel;
  }
  else if (pda[observed_object["ep"]].n_partonlevel == 0){
    if (pda[observed_object["em"]].n_observed_max <= pda[observed_object["e"]].n_observed_max){pda[observed_object["e"]].n_observed_max = pda[observed_object["em"]].n_observed_max;}
    else {pda[observed_object["em"]].n_observed_max = pda[observed_object["e"]].n_observed_max;}
  }
  else if (pda[observed_object["em"]].n_partonlevel == 0){
    if (pda[observed_object["ep"]].n_observed_max <= pda[observed_object["e"]].n_observed_max){pda[observed_object["e"]].n_observed_max = pda[observed_object["ep"]].n_observed_max;}
    else {pda[observed_object["ep"]].n_observed_max = pda[observed_object["e"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (e-ep/em) !
  if (pda[observed_object["e"]].n_partonlevel == pda[observed_object["e"]].n_observed_min){
    pda[observed_object["em"]].n_observed_min = pda[observed_object["em"]].n_partonlevel;
    pda[observed_object["ep"]].n_observed_min = pda[observed_object["ep"]].n_partonlevel;
  }
  else if (pda[observed_object["ep"]].n_partonlevel == 0){
    if (pda[observed_object["em"]].n_observed_min >= pda[observed_object["e"]].n_observed_min){pda[observed_object["e"]].n_observed_min = pda[observed_object["em"]].n_observed_min;}
    else {pda[observed_object["em"]].n_observed_min = pda[observed_object["e"]].n_observed_min;}
  }
  else if (pda[observed_object["em"]].n_partonlevel == 0){
    if (pda[observed_object["ep"]].n_observed_min >= pda[observed_object["e"]].n_observed_min){pda[observed_object["e"]].n_observed_min = pda[observed_object["ep"]].n_observed_min;}
    else {pda[observed_object["ep"]].n_observed_min = pda[observed_object["e"]].n_observed_min;}
  }

  // adapt n_observed_max to parton-level particles (mu-mup/mum) !
  if (pda[observed_object["mu"]].n_partonlevel == pda[observed_object["mu"]].n_observed_max){
    pda[observed_object["mum"]].n_observed_max = pda[observed_object["mum"]].n_partonlevel;
    pda[observed_object["mup"]].n_observed_max = pda[observed_object["mup"]].n_partonlevel;
  }
  else if (pda[observed_object["mup"]].n_partonlevel == 0){
    if (pda[observed_object["mum"]].n_observed_max <= pda[observed_object["mu"]].n_observed_max){pda[observed_object["mu"]].n_observed_max = pda[observed_object["mum"]].n_observed_max;}
    else {pda[observed_object["mum"]].n_observed_max = pda[observed_object["mu"]].n_observed_max;}
  }
  else if (pda[observed_object["mum"]].n_partonlevel == 0){
    if (pda[observed_object["mup"]].n_observed_max <= pda[observed_object["mu"]].n_observed_max){pda[observed_object["mu"]].n_observed_max = pda[observed_object["mup"]].n_observed_max;}
    else {pda[observed_object["mup"]].n_observed_max = pda[observed_object["mu"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (mu-mup/mum) !
  if (pda[observed_object["mu"]].n_partonlevel == pda[observed_object["mu"]].n_observed_min){
    pda[observed_object["mum"]].n_observed_min = pda[observed_object["mum"]].n_partonlevel;
    pda[observed_object["mup"]].n_observed_min = pda[observed_object["mup"]].n_partonlevel;
  }
  else if (pda[observed_object["mup"]].n_partonlevel == 0){
    if (pda[observed_object["mum"]].n_observed_min >= pda[observed_object["mu"]].n_observed_min){pda[observed_object["mu"]].n_observed_min = pda[observed_object["mum"]].n_observed_min;}
    else {pda[observed_object["mum"]].n_observed_min = pda[observed_object["mu"]].n_observed_min;}
  }
  else if (pda[observed_object["mum"]].n_partonlevel == 0){
    if (pda[observed_object["mup"]].n_observed_min >= pda[observed_object["mu"]].n_observed_min){pda[observed_object["mu"]].n_observed_min = pda[observed_object["mup"]].n_observed_min;}
    else {pda[observed_object["mup"]].n_observed_min = pda[observed_object["mu"]].n_observed_min;}
  }

  // adapt n_observed_max to parton-level particles (tau-taup/taum) !
  if (pda[observed_object["tau"]].n_partonlevel == pda[observed_object["tau"]].n_observed_max){
    pda[observed_object["taum"]].n_observed_max = pda[observed_object["taum"]].n_partonlevel;
    pda[observed_object["taup"]].n_observed_max = pda[observed_object["taup"]].n_partonlevel;
  }
  else if (pda[observed_object["taup"]].n_partonlevel == 0){
    if (pda[observed_object["taum"]].n_observed_max <= pda[observed_object["tau"]].n_observed_max){pda[observed_object["tau"]].n_observed_max = pda[observed_object["taum"]].n_observed_max;}
    else {pda[observed_object["taum"]].n_observed_max = pda[observed_object["tau"]].n_observed_max;}
  }
  else if (pda[observed_object["taum"]].n_partonlevel == 0){
    if (pda[observed_object["taup"]].n_observed_max <= pda[observed_object["tau"]].n_observed_max){pda[observed_object["tau"]].n_observed_max = pda[observed_object["taup"]].n_observed_max;}
    else {pda[observed_object["taup"]].n_observed_max = pda[observed_object["tau"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (tau-taup/taum) !
  if (pda[observed_object["tau"]].n_partonlevel == pda[observed_object["tau"]].n_observed_min){
    pda[observed_object["taum"]].n_observed_min = pda[observed_object["taum"]].n_partonlevel;
    pda[observed_object["taup"]].n_observed_min = pda[observed_object["taup"]].n_partonlevel;
  }
  else if (pda[observed_object["taup"]].n_partonlevel == 0){
    if (pda[observed_object["taum"]].n_observed_min >= pda[observed_object["tau"]].n_observed_min){pda[observed_object["tau"]].n_observed_min = pda[observed_object["taum"]].n_observed_min;}
    else {pda[observed_object["taum"]].n_observed_min = pda[observed_object["tau"]].n_observed_min;}
  }
  else if (pda[observed_object["taum"]].n_partonlevel == 0){
    if (pda[observed_object["taup"]].n_observed_min >= pda[observed_object["tau"]].n_observed_min){pda[observed_object["tau"]].n_observed_min = pda[observed_object["taup"]].n_observed_min;}
    else {pda[observed_object["taup"]].n_observed_min = pda[observed_object["tau"]].n_observed_min;}
  }

  // adapt n_observed_max to parton-level particles (nua-nux/nu) !
  if (pda[observed_object["nua"]].n_partonlevel < pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nua"]].n_partonlevel;}

  if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nua"]].n_observed_max){
    pda[observed_object["nu"]].n_observed_max = pda[observed_object["nu"]].n_partonlevel;
    pda[observed_object["nux"]].n_observed_max = pda[observed_object["nux"]].n_partonlevel;
  }
  else if (pda[observed_object["nux"]].n_partonlevel == 0){
    if (pda[observed_object["nu"]].n_observed_max <= pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nu"]].n_observed_max;}
    else {pda[observed_object["nu"]].n_observed_max = pda[observed_object["nua"]].n_observed_max;}
  }
  else if (pda[observed_object["nu"]].n_partonlevel == 0){
    if (pda[observed_object["nux"]].n_observed_max <= pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nux"]].n_observed_max;}
    else {pda[observed_object["nux"]].n_observed_max = pda[observed_object["nua"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (nua-nux/nu) !
  if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nua"]].n_observed_min){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["nu"]].n_partonlevel;
    pda[observed_object["nux"]].n_observed_min = pda[observed_object["nux"]].n_partonlevel;
  }
  else if (pda[observed_object["nux"]].n_partonlevel == 0){
    if (pda[observed_object["nu"]].n_observed_min >= pda[observed_object["nua"]].n_observed_min){pda[observed_object["nua"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
    else {pda[observed_object["nu"]].n_observed_min = pda[observed_object["nua"]].n_observed_min;}
  }
  else if (pda[observed_object["nu"]].n_partonlevel == 0){
    if (pda[observed_object["nux"]].n_observed_min >= pda[observed_object["nua"]].n_observed_min){pda[observed_object["nua"]].n_observed_min = pda[observed_object["nux"]].n_observed_min;}
    else {pda[observed_object["nux"]].n_observed_min = pda[observed_object["nua"]].n_observed_min;}
  }

  // adapt n_observed_min to parton-level particles (nua-nea/nma/nta) !
  if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nua"]].n_observed_min){
    pda[observed_object["nea"]].n_observed_min = pda[observed_object["nea"]].n_partonlevel;
    pda[observed_object["nma"]].n_observed_min = pda[observed_object["nma"]].n_partonlevel;
    pda[observed_object["nta"]].n_observed_min = pda[observed_object["nta"]].n_partonlevel;
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_partonlevel == 0)){
    if (pda[observed_object["nta"]].n_observed_min >= pda[observed_object["nua"]].n_observed_min){pda[observed_object["nua"]].n_observed_min = pda[observed_object["nta"]].n_observed_min;}
    else {pda[observed_object["nta"]].n_observed_min = pda[observed_object["nua"]].n_observed_min;}
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_observed_min > pda[observed_object["nua"]].n_observed_min)){
    if (pda[observed_object["nma"]].n_observed_min >= pda[observed_object["nua"]].n_observed_min){pda[observed_object["nua"]].n_observed_min = pda[observed_object["nma"]].n_observed_min;}
    else {pda[observed_object["nma"]].n_observed_min = pda[observed_object["nua"]].n_observed_min;}
  }
  else if ((pda[observed_object["nma"]].n_partonlevel == 0) && (pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_min > pda[observed_object["nua"]].n_observed_min)){
    if (pda[observed_object["nea"]].n_observed_min >= pda[observed_object["nua"]].n_observed_min){pda[observed_object["nua"]].n_observed_min = pda[observed_object["nea"]].n_observed_min;}
    else {pda[observed_object["nea"]].n_observed_min = pda[observed_object["nua"]].n_observed_min;}
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_observed_min + pda[observed_object["nta"]].n_observed_min > pda[observed_object["nua"]].n_observed_min)){
    pda[observed_object["nua"]].n_observed_min = pda[observed_object["nma"]].n_observed_min + pda[observed_object["nta"]].n_observed_min;
  }
  else if ((pda[observed_object["nma"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_min + pda[observed_object["nta"]].n_observed_min > pda[observed_object["nua"]].n_observed_min)){
    pda[observed_object["nua"]].n_observed_min = pda[observed_object["nea"]].n_observed_min + pda[observed_object["nta"]].n_observed_min;
  }
  else if ((pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_min + pda[observed_object["nma"]].n_observed_min > pda[observed_object["nua"]].n_observed_min)){
    pda[observed_object["nua"]].n_observed_min = pda[observed_object["nea"]].n_observed_min + pda[observed_object["nma"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (nua-nea/nma/nta) !
  if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nua"]].n_observed_max){
    pda[observed_object["nea"]].n_observed_max = pda[observed_object["nea"]].n_partonlevel;
    pda[observed_object["nma"]].n_observed_max = pda[observed_object["nma"]].n_partonlevel;
    pda[observed_object["nta"]].n_observed_max = pda[observed_object["nta"]].n_partonlevel;
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_partonlevel == 0)){
    if (pda[observed_object["nta"]].n_observed_max <= pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nta"]].n_observed_max;}
    else {pda[observed_object["nta"]].n_observed_max = pda[observed_object["nua"]].n_observed_max;}
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_observed_max > pda[observed_object["nua"]].n_observed_max)){
    if (pda[observed_object["nma"]].n_observed_max <= pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nma"]].n_observed_max;}
    else {pda[observed_object["nma"]].n_observed_max = pda[observed_object["nua"]].n_observed_max;}
  }
  else if ((pda[observed_object["nma"]].n_partonlevel == 0) && (pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_max > pda[observed_object["nua"]].n_observed_max)){
    if (pda[observed_object["nea"]].n_observed_max <= pda[observed_object["nua"]].n_observed_max){pda[observed_object["nua"]].n_observed_max = pda[observed_object["nea"]].n_observed_max;}
    else {pda[observed_object["nea"]].n_observed_max = pda[observed_object["nua"]].n_observed_max;}
  }
  else if ((pda[observed_object["nea"]].n_partonlevel == 0) && (pda[observed_object["nma"]].n_observed_max + pda[observed_object["nta"]].n_observed_max < pda[observed_object["nua"]].n_observed_max)){
    pda[observed_object["nua"]].n_observed_max = pda[observed_object["nma"]].n_observed_max + pda[observed_object["nta"]].n_observed_max;
  }
  else if ((pda[observed_object["nma"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_max + pda[observed_object["nta"]].n_observed_max < pda[observed_object["nua"]].n_observed_max)){
    pda[observed_object["nua"]].n_observed_max = pda[observed_object["nea"]].n_observed_max + pda[observed_object["nta"]].n_observed_max;
  }
  else if ((pda[observed_object["nta"]].n_partonlevel == 0) && (pda[observed_object["nea"]].n_observed_max + pda[observed_object["nma"]].n_observed_max < pda[observed_object["nua"]].n_observed_max)){
    pda[observed_object["nua"]].n_observed_max = pda[observed_object["nea"]].n_observed_max + pda[observed_object["nma"]].n_observed_max;
  }

  // adapt n_observed_min to parton-level particles (nu-ne/nm/nt) !
  if (pda[observed_object["nu"]].n_partonlevel == pda[observed_object["nu"]].n_observed_min){
    pda[observed_object["ne"]].n_observed_min = pda[observed_object["ne"]].n_partonlevel;
    pda[observed_object["nm"]].n_observed_min = pda[observed_object["nm"]].n_partonlevel;
    pda[observed_object["nt"]].n_observed_min = pda[observed_object["nt"]].n_partonlevel;
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_partonlevel == 0)){
    if (pda[observed_object["nt"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["nt"]].n_observed_min;}
    else {pda[observed_object["nt"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    if (pda[observed_object["nm"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["nm"]].n_observed_min;}
    else {pda[observed_object["nm"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["nm"]].n_partonlevel == 0) && (pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    if (pda[observed_object["ne"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["ne"]].n_observed_min;}
    else {pda[observed_object["ne"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_observed_min + pda[observed_object["nt"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["nm"]].n_observed_min + pda[observed_object["nt"]].n_observed_min;
  }
  else if ((pda[observed_object["nm"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_min + pda[observed_object["nt"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["ne"]].n_observed_min + pda[observed_object["nt"]].n_observed_min;
  }
  else if ((pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_min + pda[observed_object["nm"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["ne"]].n_observed_min + pda[observed_object["nm"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (nu-ne/nm/nt) !
  if (pda[observed_object["nu"]].n_partonlevel == pda[observed_object["nu"]].n_observed_max){
    pda[observed_object["ne"]].n_observed_max = pda[observed_object["ne"]].n_partonlevel;
    pda[observed_object["nm"]].n_observed_max = pda[observed_object["nm"]].n_partonlevel;
    pda[observed_object["nt"]].n_observed_max = pda[observed_object["nt"]].n_partonlevel;
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_partonlevel == 0)){
    if (pda[observed_object["nt"]].n_observed_max <= pda[observed_object["nu"]].n_observed_max){pda[observed_object["nu"]].n_observed_max = pda[observed_object["nt"]].n_observed_max;}
    else {pda[observed_object["nt"]].n_observed_max = pda[observed_object["nu"]].n_observed_max;}
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_observed_max > pda[observed_object["nu"]].n_observed_max)){
    if (pda[observed_object["nm"]].n_observed_max <= pda[observed_object["nu"]].n_observed_max){pda[observed_object["nu"]].n_observed_max = pda[observed_object["nm"]].n_observed_max;}
    else {pda[observed_object["nm"]].n_observed_max = pda[observed_object["nu"]].n_observed_max;}
  }
  else if ((pda[observed_object["nm"]].n_partonlevel == 0) && (pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_max > pda[observed_object["nu"]].n_observed_max)){
    if (pda[observed_object["ne"]].n_observed_max <= pda[observed_object["nu"]].n_observed_max){pda[observed_object["nu"]].n_observed_max = pda[observed_object["ne"]].n_observed_max;}
    else {pda[observed_object["ne"]].n_observed_max = pda[observed_object["nu"]].n_observed_max;}
  }
  else if ((pda[observed_object["ne"]].n_partonlevel == 0) && (pda[observed_object["nm"]].n_observed_max + pda[observed_object["nt"]].n_observed_max < pda[observed_object["nu"]].n_observed_max)){
    pda[observed_object["nu"]].n_observed_max = pda[observed_object["nm"]].n_observed_max + pda[observed_object["nt"]].n_observed_max;
  }
  else if ((pda[observed_object["nm"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_max + pda[observed_object["nt"]].n_observed_max < pda[observed_object["nu"]].n_observed_max)){
    pda[observed_object["nu"]].n_observed_max = pda[observed_object["ne"]].n_observed_max + pda[observed_object["nt"]].n_observed_max;
  }
  else if ((pda[observed_object["nt"]].n_partonlevel == 0) && (pda[observed_object["ne"]].n_observed_max + pda[observed_object["nm"]].n_observed_max < pda[observed_object["nu"]].n_observed_max)){
    pda[observed_object["nu"]].n_observed_max = pda[observed_object["ne"]].n_observed_max + pda[observed_object["nm"]].n_observed_max;
  }

  // adapt n_observed_min to parton-level particles (nux-nex/nmx/ntx) !
  if (pda[observed_object["nux"]].n_partonlevel == pda[observed_object["nux"]].n_observed_min){
    pda[observed_object["nex"]].n_observed_min = pda[observed_object["nex"]].n_partonlevel;
    pda[observed_object["nmx"]].n_observed_min = pda[observed_object["nmx"]].n_partonlevel;
    pda[observed_object["ntx"]].n_observed_min = pda[observed_object["ntx"]].n_partonlevel;
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_partonlevel == 0)){
    if (pda[observed_object["ntx"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["ntx"]].n_observed_min;}
    else {pda[observed_object["ntx"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    if (pda[observed_object["nmx"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["nmx"]].n_observed_min;}
    else {pda[observed_object["nmx"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["nmx"]].n_partonlevel == 0) && (pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    if (pda[observed_object["nex"]].n_observed_min >= pda[observed_object["nu"]].n_observed_min){pda[observed_object["nu"]].n_observed_min = pda[observed_object["nex"]].n_observed_min;}
    else {pda[observed_object["nex"]].n_observed_min = pda[observed_object["nu"]].n_observed_min;}
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["nmx"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min;
  }
  else if ((pda[observed_object["nmx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["nex"]].n_observed_min + pda[observed_object["ntx"]].n_observed_min;
  }
  else if ((pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min > pda[observed_object["nu"]].n_observed_min)){
    pda[observed_object["nu"]].n_observed_min = pda[observed_object["nex"]].n_observed_min + pda[observed_object["nmx"]].n_observed_min;
  }

  // adapt n_observed_max to parton-level particles (nux-nex/nmx/ntx) !
  if (pda[observed_object["nux"]].n_partonlevel == pda[observed_object["nux"]].n_observed_max){
    pda[observed_object["nex"]].n_observed_max = pda[observed_object["nex"]].n_partonlevel;
    pda[observed_object["nmx"]].n_observed_max = pda[observed_object["nmx"]].n_partonlevel;
    pda[observed_object["ntx"]].n_observed_max = pda[observed_object["ntx"]].n_partonlevel;
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_partonlevel == 0)){
    if (pda[observed_object["ntx"]].n_observed_max <= pda[observed_object["nux"]].n_observed_max){pda[observed_object["nux"]].n_observed_max = pda[observed_object["ntx"]].n_observed_max;}
    else {pda[observed_object["ntx"]].n_observed_max = pda[observed_object["nux"]].n_observed_max;}
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_observed_max > pda[observed_object["nux"]].n_observed_max)){
    if (pda[observed_object["nmx"]].n_observed_max <= pda[observed_object["nux"]].n_observed_max){pda[observed_object["nux"]].n_observed_max = pda[observed_object["nmx"]].n_observed_max;}
    else {pda[observed_object["nmx"]].n_observed_max = pda[observed_object["nux"]].n_observed_max;}
  }
  else if ((pda[observed_object["nmx"]].n_partonlevel == 0) && (pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_max > pda[observed_object["nux"]].n_observed_max)){
    if (pda[observed_object["nex"]].n_observed_max <= pda[observed_object["nux"]].n_observed_max){pda[observed_object["nux"]].n_observed_max = pda[observed_object["nex"]].n_observed_max;}
    else {pda[observed_object["nex"]].n_observed_max = pda[observed_object["nux"]].n_observed_max;}
  }
  else if ((pda[observed_object["nex"]].n_partonlevel == 0) && (pda[observed_object["nmx"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max < pda[observed_object["nux"]].n_observed_max)){
    pda[observed_object["nux"]].n_observed_max = pda[observed_object["nmx"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max;
  }
  else if ((pda[observed_object["nmx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max < pda[observed_object["nux"]].n_observed_max)){
    pda[observed_object["nux"]].n_observed_max = pda[observed_object["nex"]].n_observed_max + pda[observed_object["ntx"]].n_observed_max;
  }
  else if ((pda[observed_object["ntx"]].n_partonlevel == 0) && (pda[observed_object["nex"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max < pda[observed_object["nux"]].n_observed_max)){
    pda[observed_object["nux"]].n_observed_max = pda[observed_object["nex"]].n_observed_max + pda[observed_object["nmx"]].n_observed_max;
  }

  // adapt n_observed_max to parton-level particles (nea-nex/ne) !
  if (pda[observed_object["nea"]].n_partonlevel == pda[observed_object["nea"]].n_observed_max){
    pda[observed_object["ne"]].n_observed_max = pda[observed_object["ne"]].n_partonlevel;
    pda[observed_object["nex"]].n_observed_max = pda[observed_object["nex"]].n_partonlevel;
  }
  else if (pda[observed_object["nex"]].n_partonlevel == 0){
    if (pda[observed_object["ne"]].n_observed_max <= pda[observed_object["nea"]].n_observed_max){pda[observed_object["nea"]].n_observed_max = pda[observed_object["ne"]].n_observed_max;}
    else {pda[observed_object["ne"]].n_observed_max = pda[observed_object["nea"]].n_observed_max;}
  }
  else if (pda[observed_object["ne"]].n_partonlevel == 0){
    if (pda[observed_object["nex"]].n_observed_max <= pda[observed_object["nea"]].n_observed_max){pda[observed_object["nea"]].n_observed_max = pda[observed_object["nex"]].n_observed_max;}
    else {pda[observed_object["nex"]].n_observed_max = pda[observed_object["nea"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (nea-nex/ne) !
  if (pda[observed_object["nea"]].n_partonlevel == pda[observed_object["nea"]].n_observed_min){
    pda[observed_object["ne"]].n_observed_min = pda[observed_object["ne"]].n_partonlevel;
    pda[observed_object["nex"]].n_observed_min = pda[observed_object["nex"]].n_partonlevel;
  }
  else if (pda[observed_object["nex"]].n_partonlevel == 0){
    if (pda[observed_object["ne"]].n_observed_min >= pda[observed_object["nea"]].n_observed_min){pda[observed_object["nea"]].n_observed_min = pda[observed_object["ne"]].n_observed_min;}
    else {pda[observed_object["ne"]].n_observed_min = pda[observed_object["nea"]].n_observed_min;}
  }
  else if (pda[observed_object["ne"]].n_partonlevel == 0){
    if (pda[observed_object["nex"]].n_observed_min >= pda[observed_object["nea"]].n_observed_min){pda[observed_object["nea"]].n_observed_min = pda[observed_object["nex"]].n_observed_min;}
    else {pda[observed_object["nex"]].n_observed_min = pda[observed_object["nea"]].n_observed_min;}
  }

  // adapt n_observed_max to parton-level particles (nma-nmx/nm) !
  if (pda[observed_object["nma"]].n_partonlevel == pda[observed_object["nma"]].n_observed_max){
    pda[observed_object["nm"]].n_observed_max = pda[observed_object["nm"]].n_partonlevel;
    pda[observed_object["nmx"]].n_observed_max = pda[observed_object["nmx"]].n_partonlevel;
  }
  else if (pda[observed_object["nmx"]].n_partonlevel == 0){
    if (pda[observed_object["nm"]].n_observed_max <= pda[observed_object["nma"]].n_observed_max){pda[observed_object["nma"]].n_observed_max = pda[observed_object["nm"]].n_observed_max;}
    else {pda[observed_object["nm"]].n_observed_max = pda[observed_object["nma"]].n_observed_max;}
  }
  else if (pda[observed_object["nm"]].n_partonlevel == 0){
    if (pda[observed_object["nmx"]].n_observed_max <= pda[observed_object["nma"]].n_observed_max){pda[observed_object["nma"]].n_observed_max = pda[observed_object["nmx"]].n_observed_max;}
    else {pda[observed_object["nmx"]].n_observed_max = pda[observed_object["nma"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (nma-nmx/nm) !
  if (pda[observed_object["nma"]].n_partonlevel == pda[observed_object["nma"]].n_observed_min){
    pda[observed_object["nm"]].n_observed_min = pda[observed_object["nm"]].n_partonlevel;
    pda[observed_object["nmx"]].n_observed_min = pda[observed_object["nmx"]].n_partonlevel;
  }
  else if (pda[observed_object["nmx"]].n_partonlevel == 0){
    if (pda[observed_object["nm"]].n_observed_min >= pda[observed_object["nma"]].n_observed_min){pda[observed_object["nma"]].n_observed_min = pda[observed_object["nm"]].n_observed_min;}
    else {pda[observed_object["nm"]].n_observed_min = pda[observed_object["nma"]].n_observed_min;}
  }
  else if (pda[observed_object["nm"]].n_partonlevel == 0){
    if (pda[observed_object["nmx"]].n_observed_min >= pda[observed_object["nma"]].n_observed_min){pda[observed_object["nma"]].n_observed_min = pda[observed_object["nmx"]].n_observed_min;}
    else {pda[observed_object["nmx"]].n_observed_min = pda[observed_object["nma"]].n_observed_min;}
  }

  // adapt n_observed_max to parton-level particles (nta-ntx/nt) !
  if (pda[observed_object["nta"]].n_partonlevel == pda[observed_object["nta"]].n_observed_max){
    pda[observed_object["nt"]].n_observed_max = pda[observed_object["nt"]].n_partonlevel;
    pda[observed_object["ntx"]].n_observed_max = pda[observed_object["ntx"]].n_partonlevel;
  }
  else if (pda[observed_object["ntx"]].n_partonlevel == 0){
    if (pda[observed_object["nt"]].n_observed_max <= pda[observed_object["nta"]].n_observed_max){pda[observed_object["nta"]].n_observed_max = pda[observed_object["nt"]].n_observed_max;}
    else {pda[observed_object["nt"]].n_observed_max = pda[observed_object["nta"]].n_observed_max;}
  }
  else if (pda[observed_object["nt"]].n_partonlevel == 0){
    if (pda[observed_object["ntx"]].n_observed_max <= pda[observed_object["nta"]].n_observed_max){pda[observed_object["nta"]].n_observed_max = pda[observed_object["ntx"]].n_observed_max;}
    else {pda[observed_object["ntx"]].n_observed_max = pda[observed_object["nta"]].n_observed_max;}
  }

  // adapt n_observed_min to parton-level particles (nta-ntx/nt) !
  if (pda[observed_object["nta"]].n_partonlevel == pda[observed_object["nta"]].n_observed_min){
    pda[observed_object["nt"]].n_observed_min = pda[observed_object["nt"]].n_partonlevel;
    pda[observed_object["ntx"]].n_observed_min = pda[observed_object["ntx"]].n_partonlevel;
  }
  else if (pda[observed_object["ntx"]].n_partonlevel == 0){
    if (pda[observed_object["nt"]].n_observed_min >= pda[observed_object["nta"]].n_observed_min){pda[observed_object["nta"]].n_observed_min = pda[observed_object["nt"]].n_observed_min;}
    else {pda[observed_object["nt"]].n_observed_min = pda[observed_object["nta"]].n_observed_min;}
  }
  else if (pda[observed_object["nt"]].n_partonlevel == 0){
    if (pda[observed_object["ntx"]].n_observed_min >= pda[observed_object["nta"]].n_observed_min){pda[observed_object["nta"]].n_observed_min = pda[observed_object["ntx"]].n_observed_min;}
    else {pda[observed_object["ntx"]].n_observed_min = pda[observed_object["nta"]].n_observed_min;}
  }


  logger << LOG_INFO << endl << "Object definition after taking into account parton content: " << endl;
  logger << LOG_INFO << setw(20) << "object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    if (pda[i].n_observed_max > 0){
      logger << LOG_INFO << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << endl;// << setw(20) << define_pT[i] << setw(20) << define_ET[i] << setw(20) << define_eta[i] << setw(20) << define_y[i] << endl;
      logger << LOG_INFO << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << endl;// << setw(20) << define_pT[i] << setw(20) << define_ET[i] << setw(20) << define_eta[i] << setw(20) << define_y[i] << endl;
    }
    else {
      logger << LOG_DEBUG << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << endl;
      logger << LOG_DEBUG << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << endl;
    }
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


