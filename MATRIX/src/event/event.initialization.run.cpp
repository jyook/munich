#include "header.hpp"

void event_set::initialization_complete(){
  static Logger logger("event_set::initialization_complete");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization_runtime_partonlevel();

  cut_ps.resize(csi->n_ps, 0);
  first_non_cut_ps = 0;
  
  p_parton.resize(csi->n_ps, vector<fourvector> (csi->n_particle + 3, fourvector ()));
  for (int i_a = 1; i_a < csi->n_ps; i_a++){p_parton[i_a].erase(p_parton[i_a].end() - 1, p_parton[i_a].end());}

  particle_event.resize(object_list.size(), vector<vector<particle> > (csi->n_ps));
  particle_event[0][0].resize(csi->n_particle + 3);
  for (int i_a = 1; i_a < csi->n_ps; i_a++){particle_event[0][i_a].resize(csi->n_particle + 3 - 1);}
  
  n_object.resize(object_list.size(), vector<int> (csi->n_ps));

  // !!! needed ???
  for (int i_o = 0; i_o < particle_event.size(); i_o++){
    for (int i_a = 0; i_a < particle_event[i_o].size(); i_a++){particle_event[i_o][i_a].reserve(csi->n_particle + 3);}
  }

  user_particle.resize(user->particle_name.size(), vector<vector<particle> > (csi->n_ps));

  /*
  logger << LOG_DEBUG_VERBOSE << "particle_event.size() = " << particle_event.size() << endl;
  for (int i_o = 0; i_o < particle_event.size(); i_o++){
    logger << LOG_DEBUG_VERBOSE << "particle_event[" << i_o << "].size() = " << particle_event[i_o].size() << endl;
    for (int i_a = 0; i_a < particle_event[i_o].size(); i_a++){
      logger << LOG_DEBUG_VERBOSE << "particle_event[" << i_o << "][" << i_a << "].size() = " << particle_event[i_o][i_a].size() << endl;
      for (int i_p = 0; i_p < particle_event[i_o][i_a].size(); i_p++){
	//	logger << LOG_DEBUG_VERBOSE << "particle_event[" << i_o << "][" << i_a << "][" << i_p << "] = " << particle_event[i_o][i_a][i_p] << endl;
	//	particle_event[i_o][i_a].resize(csi->n_particle + 3 - 1);}
      }
    }
  }
  logger << LOG_DEBUG << "after particle_event.resize" << endl;
  */
  
  recombination_history.resize(csi->n_ps);

  start_p_parton = p_parton;
  start_particle_event = particle_event;
  start_n_object = n_object;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::initialization_integration(){
  static Logger logger("event_set::initialization_integration");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_ERROR << "Not implemented!" << endl;
  exit(1);
  //  Check if a separate initialization step is needed here!!!
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::initialization_runtime_partonlevel(){
  static Logger logger("event_set::initialization_runtime_partonlevel");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ps_runtime_jet_algorithm.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      for (int i_l = 0; i_l < jet_algorithm_list.size(); i_l++){
	if (csi->type_parton[i_a][i_p] == jet_algorithm_list[i_l]){
	  if (csi->type_parton[i_a][i_p] == 22 ||
	      csi->type_parton[i_a][i_p] == 2002 ||
	      csi->type_parton[i_a][i_p] == -2002){
	    photon_jet_algorithm = 1;
	    //	    if (!frixione_isolation){
	    // added to have photons in jet_algorithm (needs clarification, under which circumstances photons can become jets...)
	    //	    ps_runtime_jet_algorithm[i_a].push_back(i_p);
	    // removed again -> would lead to doubled photons in jet algorithm...
	    // would, however, be useful -> maybe adapt at the point where the protojets are selected (photon are added later by now).
	  }
	  else {ps_runtime_jet_algorithm[i_a].push_back(i_p);}
	}
      }
    }
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_jet_algorithm[i_a].size(); i_l++){temp << setw(5) << ps_runtime_jet_algorithm[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_jet_algorithm[" << i_a << "].size() = " << ps_runtime_jet_algorithm[i_a].size() << " --- " << temp.str() << endl;
  }

  ps_runtime_photon_recombination.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      logger << LOG_DEBUG << "photon_recombination_list.size() = " << photon_recombination_list.size() << endl;
      for (int i_l = 0; i_l < photon_recombination_list.size(); i_l++){
	logger << LOG_DEBUG << "csi->type_parton[" << i_a << "][" << i_p << "] = " << csi->type_parton[i_a][i_p] << "   photon_recombination_list[" << i_l << "] = " << photon_recombination_list[i_l] << endl;
	if (csi->type_parton[i_a][i_p] == photon_recombination_list[i_l]){
	  ps_runtime_photon_recombination[i_a].push_back(i_p);
	}
      }
    }
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_photon_recombination[i_a].size(); i_l++){temp << setw(5) << ps_runtime_photon_recombination[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_photon_recombination[" << i_a << "].size() = " << ps_runtime_photon_recombination[i_a].size() << " --- " << temp.str() << endl;
  }


  ps_runtime_photon.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      if (csi->type_parton[i_a][i_p] == 22 ||
	  csi->type_parton[i_a][i_p] == 2002 ||
	  csi->type_parton[i_a][i_p] == -2002){
	ps_runtime_photon[i_a].push_back(i_p);
      }
    }
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_photon[i_a].size(); i_l++){temp << setw(5) << ps_runtime_photon[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_photon[" << i_a << "].size() = " << ps_runtime_photon[i_a].size() << " --- " << temp.str() << endl;
  }


  //  ps_runtime_original containts partons (type_parton numbers) that enter event selection with their (potentially dressed) parton-level momenta:
  ps_runtime_original.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      int flag = 0;
      for (int i_l = 0; i_l < ps_runtime_jet_algorithm[i_a].size(); i_l++){
	if (i_p == ps_runtime_jet_algorithm[i_a][i_l]){flag = 1; break;}
      }
      for (int i_l = 0; i_l < ps_runtime_photon[i_a].size(); i_l++){
	if (i_p == ps_runtime_photon[i_a][i_l]){flag = 1; break;}
      }

      if (!flag){
	ps_runtime_original[i_a].push_back(i_p);
      }
    }
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_original[i_a].size(); i_l++){temp << setw(5) << ps_runtime_original[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_original[" << i_a << "].size() = " << ps_runtime_original[i_a].size() << " --- " << temp.str() << endl;
  }


  //  ps_runtime_subtraction_QCD containts partons (type_parton numbers) whose momenta are changed by QCD dipole mappings:
  ps_runtime_subtraction_QCD.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){if (abs(csi->type_parton[i_a][i_p]) < 7){ps_runtime_subtraction_QCD[i_a].push_back(i_p);}}
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_subtraction_QCD[i_a].size(); i_l++){temp << setw(5) << ps_runtime_subtraction_QCD[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_subtraction_QCD[" << i_a << "].size() = " << ps_runtime_subtraction_QCD[i_a].size() << " --- " << temp.str() << endl;
  }


  //  ps_runtime_subtraction_QEW containts partons (type_parton numbers) whose momenta are changed by QEW dipole mappings:
  ps_runtime_subtraction_QEW.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){if (csi->charge_parton[i_a][i_p] != 0. || csi->type_parton[i_a][i_p] == 22){ps_runtime_subtraction_QEW[i_a].push_back(i_p);}}
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_subtraction_QEW[i_a].size(); i_l++){temp << setw(5) << ps_runtime_subtraction_QEW[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_subtraction_QEW[" << i_a << "].size() = " << ps_runtime_subtraction_QEW[i_a].size() << " --- " << temp.str() << endl;
  }


  ps_runtime_missing.resize(csi->n_ps);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      if (csi->type_parton[i_a][i_p] == 12 ||
	  csi->type_parton[i_a][i_p] == 14 ||
	  csi->type_parton[i_a][i_p] == 16 ||
	  csi->type_parton[i_a][i_p] == -12 ||
	  csi->type_parton[i_a][i_p] == -14 ||
	  csi->type_parton[i_a][i_p] == -16){
	ps_runtime_missing[i_a].push_back(i_p);
      }
    }
    stringstream temp;
    for (int i_l = 0; i_l < ps_runtime_missing[i_a].size(); i_l++){temp << setw(5) << ps_runtime_missing[i_a][i_l];}
    logger << LOG_INFO << "ps_runtime_missing[" << i_a << "].size() = " << ps_runtime_missing[i_a].size() << " --- " << temp.str() << endl;
  }


  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << "ps" << "   " << setw(15) << "photon" << "   " << setw(15) << "photon" << "   " << setw(15) << "jet" << "   " << setw(15) << "missing" << "   " << setw(15) << "original" << "   " << setw(15) << "subtraction" << "   " << setw(15) << "subtraction" << endl;
  logger << LOG_INFO << setw(5) << "" << "   " << setw(15) << "" << "   " << setw(15) << "recombination" << "   " << setw(15) << "algorithm" << "   " << setw(15) << "" << "   " << setw(15) << "" << "   " << setw(15) << "QCD" << "   " << setw(15) << "QEW" << endl;
  //  logger << LOG_INFO << setw(15) << "phasespace" << "   " << setw(25) << "photon" << "   " << setw(25) << "photon_recombination" << "   " << setw(25) << "jet_algorithm" << endl;
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    stringstream temp_ph;
    for (int i_i = 0; i_i < ps_runtime_photon[i_a].size(); i_i++){
      temp_ph << ps_runtime_photon[i_a][i_i];
      if (i_i < ps_runtime_photon[i_a].size() - 1){temp_ph << "  ";}
    }
    stringstream temp_pr;
    for (int i_i = 0; i_i < ps_runtime_photon_recombination[i_a].size(); i_i++){
      temp_pr << ps_runtime_photon_recombination[i_a][i_i];
      if (i_i < ps_runtime_photon_recombination[i_a].size() - 1){temp_pr << "  ";}
    }
    stringstream temp_ja;
    for (int i_i = 0; i_i < ps_runtime_jet_algorithm[i_a].size(); i_i++){
      temp_ja << ps_runtime_jet_algorithm[i_a][i_i];
      if (i_i < ps_runtime_jet_algorithm[i_a].size() - 1){temp_ja << "  ";}
    }
    stringstream temp_mi;
    for (int i_i = 0; i_i < ps_runtime_missing[i_a].size(); i_i++){
      temp_mi << ps_runtime_missing[i_a][i_i];
      if (i_i < ps_runtime_missing[i_a].size() - 1){temp_mi << "  ";}
    }
    stringstream temp_og;
    for (int i_i = 0; i_i < ps_runtime_original[i_a].size(); i_i++){
      temp_og << ps_runtime_original[i_a][i_i];
      if (i_i < ps_runtime_original[i_a].size() - 1){temp_og << "  ";}
    }
    stringstream temp_qcd;
    for (int i_i = 0; i_i < ps_runtime_subtraction_QCD[i_a].size(); i_i++){
      temp_qcd << ps_runtime_subtraction_QCD[i_a][i_i];
      if (i_i < ps_runtime_subtraction_QCD[i_a].size() - 1){temp_qcd << "  ";}
    }
    stringstream temp_qew;
    for (int i_i = 0; i_i < ps_runtime_subtraction_QEW[i_a].size(); i_i++){
      temp_qew << ps_runtime_subtraction_QEW[i_a][i_i];
      if (i_i < ps_runtime_subtraction_QEW[i_a].size() - 1){temp_qew << "  ";}
    }
    logger << LOG_INFO << setw(5) << i_a << "   " << setw(15) << temp_ph.str() << "   " << setw(15) << temp_pr.str() << "   " << setw(15) << temp_ja.str() << "   " << setw(15) << temp_mi.str() << "   " << setw(15) << temp_og.str() << "   " << setw(15) << temp_qcd.str() << "   " << setw(15) << temp_qew.str() << endl;
  }
  logger.newLine(LOG_INFO);


  ps_n_partonlevel.resize(csi->n_ps, vector<int> (object_list.size(), 0));

  determine_phasespace_object_partonlevel();

  for (int i_p = 1; i_p < object_list.size(); i_p++){
    for (int i_a = 0; i_a < csi->n_ps; i_a++){
      if (ps_n_partonlevel[i_a][i_p] > pda[i_p].n_partonlevel){pda[i_p].n_partonlevel = ps_n_partonlevel[i_a][i_p];}
    }
  }


  logger << LOG_INFO << "Before removal of non-contributiong subprocesses:" << endl;
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << "All objects:" << endl;
  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << right << "no" << setw(10) << "object" << setw(10) << "access" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|" << setw(10) << "|y|" << setw(5) << "" << setw(10) << "n_partonic"<< endl;
  for (int i = 0; i < object_list.size(); i++){
    logger << LOG_INFO << setw(5) << right << i << setw(10) << object_list[i] << setw(10) << access_object[object_list[i]] << setw(10) << pda[i].n_observed_min << setw(10) << pda[i].n_observed_max << setw(10) << pda[i].define_pT << setw(10) << pda[i].define_ET << setw(10) << pda[i].define_eta << setw(10) << pda[i].define_y << setw(5) << "" << setw(10) << pda[i].n_partonlevel << endl;
  }
  /*
  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << right << "no" << setw(10) << "object" << setw(10) << "n_part" << setw(5) << "" << "partons that can enter to respective object (phase-space dependent)" << endl;
  for (int i = 0; i < object_list.size(); i++){
    stringstream temp;
    for (int i_a = 0; i_a < csi->n_ps; i_a++){
      for (int i_j = 0; i_j < ps_runtime_order_inverse[i_a][i].size(); i_j++){
	temp << setw(1) << ps_runtime_order_inverse[i_a][i][i_j] << " ";
      }
      if (i_a < csi->n_ps - 1){temp << "- ";}
    }
    logger << LOG_INFO << setw(5) << right << i << setw(10) << object_list[i] << setw(10) << pda[i].n_partonlevel << setw(5) << "" << temp.str() << endl;
  }
  logger.newLine(LOG_INFO);
  */


  // perform these checks before adapting n_observed_min etc. to n_partonlevel (if requirements cannot be fulfilled, runs won't start and write 0 as XS).

  int flag_remove = 0;
  vector<int> remove_phasespace(csi->n_ps, 0);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i = 1; i < object_list.size(); i++){
      if ((pda[i].n_observed_min > ps_n_partonlevel[i_a][i]) && object_category[i] != -1){
	logger << LOG_FATAL << object_list[i] << ":   pda.n_observed_min[" << i << "] = " << pda[i].n_observed_min << " > " << ps_n_partonlevel[i_a][i] << " =   n_partonlevel[observed_object[object_list[" << i << "] = " << object_list[i] << "] = " << i << "]" << endl;
	logger << LOG_FATAL << "Dipole " << i_a << " does not have a sufficient parton content !!!" << endl;

	//... check things like photons mimicking jets etc., which could let the dipole contribute (mainly photons treated as jets, collinear e+e- pairs as photons, etc.

	remove_phasespace[i_a] = 1;
	flag_remove = 1;
	break;
      }
    }
  }

  /*
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    if (csi->type_contribution == "RA"){logger << LOG_INFO << "Phasespace " << setw(2) << i_a << ": " << setw(15) << (*RA_dipole)[i_a].name() << "   remove_phasespace = " << remove_phasespace[i_a] << endl;}
    else {logger << LOG_INFO << "Phasespace " << setw(2) << i_a << ":    remove_phasespace = " << remove_phasespace[i_a] << endl;}
  }
  */
  if (accumulate(remove_phasespace.begin(), remove_phasespace.end(), 0) == remove_phasespace.size()){
    logger.newLine(LOG_FATAL);
    logger << LOG_FATAL << "No contribution can result from this subprocess due to parton content." << endl;
    logger.newLine(LOG_FATAL);
    csi->int_end = 1;
  }

  if (flag_remove){
    // treatment of removed dipoles has to be added. By now, they are simply taken into account, which might cause problems !!!
    // Ideally, they should be removed completely (including phase-space generation)

    /*
    logger << LOG_FATAL << "(*RA_dipole).size() = " << (*RA_dipole).size() << endl;
    for (int i_a = csi->n_ps - 1; i_a >= 0; i_a--){
      if (remove_dipole[i_a] == 1){(*RA_dipole).erase((*RA_dipole).begin() + i_a);}
    }
    logger << LOG_FATAL << "(*RA_dipole).size() = " << (*RA_dipole).size() << endl;
    //    initialization_RA((*RA_dipole));

    n_ps = (*RA_dipole).size();
    n_pc = (*RA_dipole).size();
    n_pz = 1;

    csi->type_parton.resize(csi->n_ps);
    for (int i_a = 0; i_a < (*RA_dipole).size(); i_a++){
      csi->type_parton[i_a] = (*RA_dipole)[i_a].type_parton();
    }
    */
  }



  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i = 1; i < object_list.size(); i++){
      int flag = 0;
      for (int i_j = 0; i_j < ps_runtime_order_inverse[i_a][i].size(); i_j++){
	for (int i_p = 0; i_p < ps_runtime_original[i_a].size(); i_p++){
	  if (ps_runtime_order_inverse[i_a][i][i_j] == ps_runtime_original[i_a][i_p]){
	    flag++;
	    break;
	  }
	}
	if (flag){break;}
      }

      if (flag){
	if (object_category[i] != -1 && pda[i].n_observed_max < ps_n_partonlevel[i_a][i]){
	  if (pda[i].define_pT == 0. &&
	      pda[i].define_ET == 0. &&
	      pda[i].define_y > 1.e10 &&
	      pda[i].define_eta > 1.e10){
	    logger << LOG_INFO << "No event will survive event selection because of object_list[" << i << "] = " << object_list[i] << " -> Don't start integration." << endl;
	    csi->int_end = 1;
	  }
	}
      }
    }
  }

  determine_object_partonlevel(); // needs modification !!!  User input should never be overwritten, even if contribution gives zero !!!

  determine_equivalent_object(); // Might need adaptation accordingly !!! Could be avoided by checking full object list for n_observed_min - n_partonlevel correspondence !!!
  determine_relevant_object();
  determine_runtime_object();


  logger << LOG_INFO << "After determine_runtime_object in initialization_runtime_partonlevel" << endl;

  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << right << "no" << setw(10) << "object" << setw(10) << "n_part" << setw(5) << "" << "partons that can enter to respective object (phase-space dependent)" << endl;
  for (int i = 0; i < object_list.size(); i++){
    stringstream temp;
    for (int i_a = 0; i_a < csi->n_ps; i_a++){
      for (int i_j = 0; i_j < ps_runtime_order_inverse[i_a][i].size(); i_j++){
	temp << setw(1) << ps_runtime_order_inverse[i_a][i][i_j] << " ";
      }
      if (i_a < csi->n_ps - 1){temp << "- ";}
    }
    logger << LOG_INFO << setw(5) << right << i << setw(10) << object_list[i] << setw(10) << pda[i].n_partonlevel << setw(5) << "" << temp.str() << endl;
  }
  logger.newLine(LOG_INFO);


  /*
  // replaced by the procedure above !!!
  int flag_remove = 0;
  vector<int> remove_dipole(csi->n_ps, 0);
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    for (int i = 1; i < object_list_selection.size(); i++){
      if ((pds[i].n_observed_min > ps_n_partonlevel[i_a][observed_object[object_list_selection[i]]]) && object_category[equivalent_no_object[i]] != -1){
	logger << LOG_FATAL << object_list_selection[i] << ":   pds.n_observed_min[" << i << "] = " << pds[i].n_observed_min << " > " << ps_n_partonlevel[i_a][observed_object[object_list_selection[i]]] << " =   n_partonlevel[observed_object[object_list_selection[" << i << "] = " << object_list_selection[i] << "] = " << observed_object[object_list_selection[i]] << "]" << endl;
	logger << LOG_FATAL << "Dipole " << i_a << " does not have a sufficient parton content !!!" << endl;
	//... check things like photons mimicking jets etc., which could let the dipole contribute (mainly photons treated as jets, collinear e+e- pairs as photons, etc.

	remove_dipole[i_a] = 1;
	flag_remove = 1;
	break;
      }
    }
  }
  */




  /*
  // replaced by above procedure !!!
  for (int i = 1; i < object_list_selection.size(); i++){
    if ((pds[i].n_observed_min > pda[observed_object[object_list_selection[i]]].n_partonlevel) && object_category[equivalent_no_object[i]] != -1){
      logger << LOG_FATAL << object_list_selection[i] << ":   pds.n_observed_min[" << i << "] = " << pds[i].n_observed_min << " > " << pds[observed_object[object_list_selection[i]]].n_partonlevel << " =   n_partonlevel[observed_object[object_list_selection[" << i << "] = " << object_list_selection[i] << "] = " << observed_object[object_list_selection[i]] << "]" << endl;
      logger << LOG_FATAL << "No contribution can result from this subprocess - modification needed due to type conversion (e.g. jet/photon recombination alogorithm)" << endl;
      logger << LOG_FATAL << "Check if correct! Dipoles might have a different parton content, which is not covered by now !!!" << endl;
      csi->int_end = 1;
      //      exit(0);
    }
  }
  */

  // Check if an upper limit on n_partonlevel can be realized (useful to technically veto e.g. *all* bjets in the massive case):
  for (int i_r = 1; i_r < object_list_selection.size(); i_r++){
    int flag = 0;
    for (int i_j = 0; i_j < runtime_original.size(); i_j++){
      if (i_r == runtime_original[i_j]){flag++; break;}
    }
    logger << LOG_DEBUG << "object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << " enters event selection with its original momentum." << endl;
    /*
    logger << LOG_DEBUG << "object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << ": pds.define_pT[i_r] = " << pds.define_pT[i_r] << endl;
    logger << LOG_DEBUG << "object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << ": pds.define_ET[i_r] = " << pds.define_ET[i_r] << endl;
    logger << LOG_DEBUG << "object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << ": pds.define_y[i_r] = " << pds.define_y[i_r] << endl;
    logger << LOG_DEBUG << "object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << ": pds.define_eta[i_r] = " << pds.define_eta[i_r] << endl;
    */
    if (pds[i_r].n_observed_max < pda[observed_object[object_list_selection[i_r]]].n_partonlevel && object_category[equivalent_no_object[i_r]] != -1 &&
	//	if (pds.n_observed_max[i_r] < n_partonlevel[observed_object[object_list_selection[i_r]]] &&
	pds[i_r].define_pT == 0. &&
	pds[i_r].define_ET == 0. &&
	pds[i_r].define_y > 1.e10 &&
	pds[i_r].define_eta > 1.e10){
      logger << LOG_DEBUG << "No event will survive event selection because of object_list_selection[" << i_r << "] = " << object_list_selection[i_r] << " -> Don't start integration." << endl;
      csi->int_end = 1;
    }
  }



  for (int i_p = 0; i_p < object_list.size(); i_p++){
    access_object[object_list[i_p]] = equivalent_no_object[no_relevant_object[equivalent_object[object_list[i_p]]]];
    logger << LOG_INFO << setw(5) << i_p << "   " << setw(10) << object_list[i_p] << "   equivalent_object -> " << setw(5) << equivalent_object[object_list[i_p]] << "   no_relevant_object - > " << no_relevant_object[equivalent_object[object_list[i_p]]] << "   equivalent_no_object -> " << equivalent_no_object[no_relevant_object[equivalent_object[object_list[i_p]]]] << endl;
  }

  output_initialization_runtime_partonlevel();


  for (int i_f = 0; i_f < name_fiducial_cut.size(); i_f++){
    (fiducial_cut).push_back(fiducialcut(name_fiducial_cut[i_f], *this));
    //    (fiducial_cut).push_back(fiducialcut(name_fiducial_cut[i_f], esi, *this));
  }
  for (int i_f = 0; i_f < fiducial_cut.size(); i_f++){
    logger << LOG_INFO << "fiducial_cut[" << i_f << "] = " << fiducial_cut[i_f].name << endl << fiducial_cut[i_f] << endl;
  }


  logger << LOG_DEBUG << "csi->int_end = " << csi->int_end << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void event_set::output_initialization_runtime_partonlevel(){
  static Logger logger("event_set::output_initialization_runtime_partonlevel");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "All objects:" << endl;
  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << right << "no" << setw(10) << "object" << setw(10) << "access" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|" << setw(10) << "|y|" << setw(5) << "" << setw(10) << "n_partonic"<< endl;
  for (int i = 0; i < object_list.size(); i++){
    logger << LOG_INFO << setw(5) << right << i << setw(10) << object_list[i] << setw(10) << access_object[object_list[i]] << setw(10) << pda[i].n_observed_min << setw(10) << pda[i].n_observed_max << setw(10) << pda[i].define_pT << setw(10) << pda[i].define_ET << setw(10) << pda[i].define_eta << setw(10) << pda[i].define_y << setw(5) << "" << setw(10) << pda[i].n_partonlevel << endl;
  }

  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG << endl << "Access generic objects by name: " << endl;
  logger << LOG_DEBUG << setw(5) << right << "no" << setw(20) << "object_list[no]" << setw(30) << "access_object[.<--.]" << setw(20) << "object_list[no]" << setw(30) << "equivalent_object[.<--.]" << setw(30) << "no_relevant_object[.<--.]" << setw(30) << "equivalent_no_object[.<--.]" << endl;
  //"n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    logger << LOG_DEBUG << setw(5) << right << i << setw(20) << object_list[i] << setw(30) << access_object[object_list[i]] << setw(20) << object_list[i] << setw(30) << equivalent_object[object_list[i]] << setw(30) << no_relevant_object[equivalent_object[object_list[i]]] << setw(30) << equivalent_no_object[no_relevant_object[equivalent_object[object_list[i]]]] << endl;
    //setw(20) << pds[i].n_observed_min << setw(20) << pds[i].n_observed_max << setw(20) << pds[i].define_pT << setw(20) << pds[i].define_ET << setw(20) << pds[i].define_eta << setw(20) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);



  logger << LOG_INFO << "Relevant objects:" << endl;
  logger.newLine(LOG_INFO);
  logger << LOG_INFO << setw(5) << right << "no" << setw(10) << "object" << setw(10) << "access" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|"  << setw(10) << "|y|"<< endl;
  //  logger << LOG_INFO << setw(10) << "no" << setw(20) << "object"  << setw(20) << "access_object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 0; i < object_list_selection.size(); i++){
    //    logger << LOG_INFO << setw(20) << object_list_selection[i] << setw(20) << n_observed_min[pds.object_category[i]] << setw(20) << n_observed_max[pds.object_category[i]] << setw(20) << define_pT[pds.object_category[i]] << setw(20) << define_eta[pds.object_category[i]] << setw(20) << define_y[pds.object_category[i]] << endl;
    logger << LOG_INFO << setw(5) << right << i << setw(10) << object_list_selection[i] << setw(10) << access_object[object_list_selection[i]] << setw(10) << pds[i].n_observed_min << setw(10) << pds[i].n_observed_max << setw(10) << pds[i].define_pT << setw(10) << pds[i].define_ET << setw(10) << pds[i].define_eta << setw(10) << pds[i].define_y << endl;
    //    logger << LOG_INFO << setw(10) << i << setw(20) << object_list_selection[i] << setw(20) << access_object[object_list_selection[i]] << setw(20) << pds[i].n_observed_min << setw(20) << pds[i].n_observed_max << setw(20) << pds[i].define_pT << setw(20) << pds[i].define_ET << setw(20) << pds[i].define_eta << setw(20) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG << "Relevant objects:   runtime_jet_recombination   collects partons that enter jet recombination." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < runtime_jet_recombination.size(); i++){
    logger << LOG_INFO << "runtime_jet_recombination[" << setw(3) << right << i << "] = " << setw(3) << runtime_jet_recombination[i] << "[" << setw(10) << object_list_selection[runtime_jet_recombination[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG << "Relevant objects:   runtime_photon_isolation   collects partons that could become isolated photons a la Frixione." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < runtime_photon_isolation.size(); i++){
    logger << LOG_INFO << "runtime_photon_isolation[" << setw(3) << right << i << "] = " << setw(3) << runtime_photon_isolation[i] << "[" << setw(10) << object_list_selection[runtime_photon_isolation[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);

  /*
  //  runtime_photon_recombination is never used !!!
  logger << LOG_DEBUG << "Relevant objects:   runtime_photon_recombination   collects partons that enter photon recombination." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < runtime_photon_recombination.size(); i++){
    logger << LOG_INFO << "runtime_photon_recombination[" << setw(3) << right << i << "] = " << setw(3) << runtime_photon_recombination[i] << "[" << setw(10) << object_list_selection[runtime_photon_recombination[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);
  */
  /*
  logger << LOG_DEBUG << "Relevant objects:   runtime_missing   collects partons that enter event selection with their parton-level momenta." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < runtime_missing.size(); i++){
    logger << LOG_INFO << "runtime_missing[" << setw(3) << right << i << "] = " << setw(3) << runtime_missing[i] << "[" << setw(10) << object_list_selection[runtime_missing[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);
  */

  logger << LOG_DEBUG << "Relevant objects:   runtime_original   collects partons that enter event selection with their parton-level momenta." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < runtime_original.size(); i++){
    logger << LOG_INFO << "runtime_original[" << setw(3) << right << i << "] = " << setw(3) << runtime_original[i] << "[" << setw(10) << object_list_selection[runtime_original[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);



  logger << LOG_DEBUG << "Relevant objects:   runtime_order   collects partons that belong to the respective object class." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 0; i < object_list_selection.size(); i++){
    stringstream sstemp;
    logger << LOG_INFO << "runtime_order[" << i << "].size() = " << runtime_order[i].size() << endl;
    for (int j = 0; j < runtime_order[i].size(); j++){sstemp << setw(3) << runtime_order[i][j] << "[" << setw(6) << csi->type_parton[0][runtime_order[i][j]] << "]";}// << setw(3) << runtime_order[i][csi->type_parton[0][i]] << " ["
    logger << LOG_INFO << "runtime_order[" << setw(3) << right << i << " -> " << setw(10) << object_list_selection[i] << "] -> " << sstemp.str() << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG << "Relevant objects:   runtime_order_inverse   collects object classes a parton belongs to." << endl;
  logger.newLine(LOG_DEBUG);

  for (int i = 3; i < csi->type_parton[0].size(); i++){
    stringstream sstemp;
    for (int j = 0; j < runtime_order_inverse[i].size(); j++){sstemp << setw(10) << object_list_selection[runtime_order_inverse[i][j]] << " [" << setw(3) << runtime_order_inverse[i][j] << "]";}
    logger << LOG_INFO << "runtime_order_inverse[csi->type_parton[" << setw(3) << right << i << "] = " << setw(5) << csi->type_parton[0][i] << "] -> " << sstemp.str() << endl;
  }
  logger.newLine(LOG_INFO);



  logger << LOG_DEBUG << endl << "Object definition after definition: " << endl;
  logger << LOG_DEBUG << setw(5) << "no" << setw(20) << "object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    logger << LOG_DEBUG << setw(5) << i << setw(20) << object_list[i] << setw(20) << pda[i].n_observed_min << setw(20) << pda[i].n_observed_max << setw(20) << pda[i].define_pT << setw(20) << pda[i].define_ET << setw(20) << pda[i].define_eta << setw(20) << pda[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);
  logger << LOG_DEBUG << endl << "Relevant object definition after definition: " << endl;
  logger << LOG_DEBUG << setw(5) << "no" << setw(20) << "object" << setw(20) << "n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list_selection.size(); i++){
    logger << LOG_DEBUG << setw(5) << i << setw(20) << object_list_selection[i] << setw(20) << pds[i].n_observed_min << setw(20) << pds[i].n_observed_max << setw(20) << pds[i].define_pT << setw(20) << pds[i].define_ET << setw(20) << pds[i].define_eta << setw(20) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);

  logger << LOG_DEBUG << endl << "Relation between relevant objects and generic objects: " << endl;
  logger << LOG_DEBUG << setw(5) << "no" << setw(25) << "object_list_selection[no]" << " " << setw(47) << "no_relevant_object[object_list_selection[no]]" << setw(25) << "equivalent_no_object[no]" << endl;
  //"n_observed_min" << setw(20) << "n_observed_max" << setw(20) << "define_pT" << setw(20) << "define_ET" << setw(20) << "define_eta"  << setw(20) << "define_y"<< endl;
  for (int i = 1; i < object_list_selection.size(); i++){
    logger << LOG_DEBUG << setw(5) << i << setw(25) << object_list_selection[i] << " " << setw(47) << no_relevant_object[object_list_selection[i]] << setw(25) << equivalent_no_object[i] << endl;
    //setw(20) << pds[i].n_observed_min << setw(20) << pds[i].n_observed_max << setw(20) << pds[i].define_pT << setw(20) << pds[i].define_ET << setw(20) << pds[i].define_eta << setw(20) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
