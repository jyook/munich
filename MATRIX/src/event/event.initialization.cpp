#include "header.hpp"

void event_set::initialization(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, user_defined * _user){
  static  Logger logger("event_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  this = _isi.esi;
  csi = _csi;
  msi = _msi;
  user = _user;

  initialization_set_default();

  // too early: n_ps not yet set...  p_parton.resize(n_ps, vector<fourvector> (n_particle + 3, fourvector ()));

  // Shifted here: should add all user-defined particles:
  define_specific_object_list();

  initialization_input(isi);

  initialization_after_input();

  //  fills (in particular):
  //  photon_recombination_list -> contains PDG labels of partons that will enter a jet algorithm
  //  jet_algorithm_list        -> contains PDG labels of partonsthat might be 'dressed' with photons
  //  (photon_recombination_selection, photon_recombination_disable, jet_algorithm_selection, jet_algorithm_disable   are NOT used any longer afterwards.)
  //  sets all basic parameters for jet algorithm, photon recombination and Frixione isolation.
  //  nothing process-specific so far.

  determine_object_definition();
  //  initialization_object_process();
  //  determine_object_definition();
  //  define_pT/ET/eta/y and n_observed_min/max are adapted between different objects (like lep and e/mu/tau etc.).
  //  check if this works correctly !!!

  determine_n_partonlevel(csi->type_parton);
  //  fill n_partonlevel: uses process information !!!
  //  very trivial right now, no use of jet_algorithm_list, etc., no splitting between different phasespaces.
  // is this actually needed here ??? More sophisticated determination happens later !!!
  //  initialization_object_process();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::initialization_set_default(){
  static  Logger logger("event_set::set_default");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  switch_output_cutinfo = 0;
  // needed ???
  switch_testcut = 0;

  ///////////////////////////////////////
  //  photon-recombination parameters  //
  ///////////////////////////////////////

  photon_recombination = 0;
  photon_R_definition = 0;
  photon_R = 0.;
  photon_E_threshold_ratio = 0.;
  photon_jet_algorithm = 0;
  photon_recombination_selection.resize(0);
  photon_recombination_disable.resize(0);

  photon_photon_recombination = 0;
  photon_photon_recombination_R = 0.;

  ///////////////////////////////////
  //  photon-isolation parameters  //
  ///////////////////////////////////

  frixione_isolation = 0;
  frixione_n = 0.;
  frixione_epsilon = 0.;
  frixione_fixed_ET_max = 0.;
  frixione_delta_0 = 0.;
  frixione_jet_removal = 0;

  ////////////////////////////////////////
  //  jet and jet-algorithm parameters  //
  ////////////////////////////////////////

  jet_algorithm = 0;
  jet_R_definition = 0;
  jet_R = 0.;
  parton_y_max = 1.e99;
  parton_eta_max = 1.e99;
  jet_algorithm_selection.resize(0);
  jet_algorithm_disable.resize(0);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::initialization_input(inputparameter_set * isi){
  static Logger logger("event_set::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_i = 0; i_i < isi->input_event.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_event[" << i_i << "] = " << isi->input_event[i_i] << endl;

    if (isi->input_event[i_i].variable == ""){}

    else if (isi->input_event[i_i].variable == "switch_output_cutinfo"){switch_output_cutinfo = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "switch_testcut"){switch_testcut = atoi(isi->input_event[i_i].value.c_str());}

  ////////////////////////////////////////////////////
  //  basic event-selection criteria for particles  //
  ////////////////////////////////////////////////////

    else if (isi->input_event[i_i].variable == "n_observed_min"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].n_observed_min = atoi(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (n_observed_min) !" << endl;
	//	exit(1);
      }
    }
    else if (isi->input_event[i_i].variable == "n_observed_max"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].n_observed_max = atoi(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (n_observed_max) !" << endl;
	//	exit(1);
      }
    }
    else if (isi->input_event[i_i].variable == "define_pT"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].define_pT = atof(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (define_pT) !" << endl;
	//	exit(1);
      }
    }
    else if (isi->input_event[i_i].variable == "define_ET"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].define_ET = atof(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (define_ET) !" << endl;
	//	exit(1);
      }
    }
    else if (isi->input_event[i_i].variable == "define_eta"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].define_eta = atof(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (define_eta) !" << endl;
	//	exit(1);
      }
    }
    else if (isi->input_event[i_i].variable == "define_y"){
      if (observed_object[isi->input_event[i_i].specifier] > 0){
	pda[observed_object[isi->input_event[i_i].specifier]].define_y = atof(isi->input_event[i_i].value.c_str());
      }
      else {
	logger << LOG_FATAL << "Object " << isi->input_event[i_i].specifier << " has not been defined (define_y) !" << endl;
	//	exit(1);
      }
    }

    ///////////////////////////////
    //  general cut definitions  //
    ///////////////////////////////

    else if (isi->input_event[i_i].variable == "fiducial_cut"){
      logger << LOG_INFO << "isi->input_event[" << setw(3) << i_i << "].variable = fiducial_cut = " << isi->input_event[i_i].value << endl;
      name_fiducial_cut.push_back(isi->input_event[i_i].value);
    }

    ///////////////////////////////////////
    //  photon-recombination parameters  //
    ///////////////////////////////////////

    else if (isi->input_event[i_i].variable == "photon_recombination"){photon_recombination = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_R_definition"){photon_R_definition = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_R"){photon_R = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_E_threshold_ratio"){photon_E_threshold_ratio = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_jet_algorithm"){photon_jet_algorithm = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_recombination_selection"){
      //  Partons that can be selected for undergoing the photon recombination:
      if (isi->input_event[i_i].value == "clear"){photon_recombination_selection.clear();}
      else {photon_recombination_selection.push_back(isi->input_event[i_i].value);}
    }
    else if (isi->input_event[i_i].variable == "photon_recombination_disable"){
      //  Partons that can be excluded from the photon recombination:
      if (isi->input_event[i_i].value == "clear"){photon_recombination_disable.clear();}
      else {photon_recombination_disable.push_back(isi->input_event[i_i].value);}
    }
    else if (isi->input_event[i_i].variable == "photon_photon_recombination"){photon_photon_recombination = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "photon_photon_recombination_R"){photon_photon_recombination_R = atof(isi->input_event[i_i].value.c_str());}

    ///////////////////////////////////
    //  photon-isolation parameters  //
    ///////////////////////////////////

    else if (isi->input_event[i_i].variable == "frixione_isolation"){frixione_isolation = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "frixione_n"){frixione_n = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "frixione_epsilon"){frixione_epsilon = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "frixione_fixed_ET_max"){frixione_fixed_ET_max = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "frixione_delta_0"){frixione_delta_0 = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "frixione_jet_removal"){frixione_jet_removal = atoi(isi->input_event[i_i].value.c_str());}

    ////////////////////////////////////////
    //  jet and jet-algorithm parameters  //
    ////////////////////////////////////////

    else if (isi->input_event[i_i].variable == "jet_algorithm"){jet_algorithm = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "jet_R_definition"){jet_R_definition = atoi(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "jet_R"){jet_R = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "parton_y_max"){parton_y_max = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "parton_eta_max"){parton_eta_max = atof(isi->input_event[i_i].value.c_str());}
    else if (isi->input_event[i_i].variable == "jet_algorithm_selection"){
      //  Partons that can be selected for undergoing the jet algorithm:
      //  g,a,Q,q,qx,D,d,dx,U,u,ux,S,s,sx,C,c,cx,B,b,bx,T,t,tx
      if (isi->input_event[i_i].value == "clear"){jet_algorithm_selection.clear();}
      else {jet_algorithm_selection.push_back(isi->input_event[i_i].value);}
    }
    else if (isi->input_event[i_i].variable == "jet_algorithm_disable"){
      //  Partons that can be excluded from the jet algorithm:
      //  g,a,Q,q,qx,D,d,dx,U,u,ux,S,s,sx,C,c,cx,B,b,bx,T,t,tx
      if (isi->input_event[i_i].value == "clear"){jet_algorithm_disable.clear();}
      else {jet_algorithm_disable.push_back(isi->input_event[i_i].value);}
    }
  }


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::initialization_after_input(){
  static Logger logger("event_set::initialization_after_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Further proceed read-in data

  photon_R2 = pow(photon_R, 2);
  determine_selection_content_particle(photon_recombination_list, photon_recombination_selection, photon_recombination_disable);
  for (int i_j = 0; i_j < photon_recombination_list.size(); i_j++){
    logger << LOG_DEBUG << "photon_recombination_list[" << i_j << "] = " << photon_recombination_list[i_j] << endl;
  }
  // check if there is a photon to be recombined:
  int flag_photon = 0;
  for (int i_a = 0; i_a < csi->type_parton.size(); i_a++){
    logger << LOG_DEBUG << "csi->type_parton[" << i_a << "].size() = " << csi->type_parton[i_a].size() << endl;
    for (int i_p = 1; i_p < csi->type_parton[i_a].size(); i_p++){
      //    for (int i_p = 1; i_p < csi->type_parton.size(); i_p++){
      logger << LOG_DEBUG << "csi->type_parton[" << i_a << "][" << i_p << "] = " << csi->type_parton[i_a][i_p] << endl;
      //      if (csi->type_parton[i_a][i_p] == 21){flag_photon++;}// break;} // photon should be 22 !!!
      if (csi->type_parton[i_a][i_p] == 22 ||
	  csi->type_parton[i_a][i_p] == 2002 ||
	  csi->type_parton[i_a][i_p] == -2002){flag_photon++;}// break;}
    }
  }

  logger << LOG_DEBUG << "flag_photon = " << flag_photon << endl;

  if (!flag_photon){
    photon_recombination = 0;
    photon_recombination_list.clear();
  }

  // Allow also for combined fixed-energy plus pT_photon fraction in Frixion isolation ???

  jet_R2 = pow(jet_R, 2);
  logger << LOG_DEBUG << "jet_algorithm_list" << endl;
  if (jet_algorithm_selection.size() == 0){
    jet_algorithm_selection.push_back("g+Q");
    logger << LOG_INFO << "jet_algorithm_selection is set to its default value 'g+Q'" << endl;
  }
  determine_selection_content_particle(jet_algorithm_list, jet_algorithm_selection, jet_algorithm_disable);

  for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
    logger << LOG_DEBUG << "jet_algorithm_list[" << i_j << "] = " << jet_algorithm_list[i_j] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



