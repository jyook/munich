#include "header.hpp"
//#include "definitions.observable.set.cxx"

// Eq. (3.4) without E_gamma prefactor
double event_set::frixione_discr(double delta) {
  if (frixione_n == 1){return (1. - cos(delta)) / (1. - cos(frixione_delta_0));}
  else {return pow((1. - cos(delta)) / (1. - cos(frixione_delta_0)), frixione_n);}
}

double event_set::frixione_discr(double delta, double delta_dynamic) {
  if (frixione_n == 1){return (1. - cos(delta)) / (1. - cos(delta_dynamic));}
  else {return pow((1. - cos(delta)) / (1. - cos(delta_dynamic)), frixione_n);}
}

double event_set::frixione_discriminant_R2(double delta) {
  //replace [(1- cos R)/(1- cos R_0)]^n  =>  [(R^2/R_0^2)]^n  in Frixione's isolation formula (this matters when R>1, i.e. at small pT)
  if (frixione_n == 1){return (pow(delta, 2)) / (pow(frixione_delta_0, 2));}
  else {return pow((pow(delta, 2)) / (pow(frixione_delta_0, 2)), frixione_n);}
}

double event_set::frixione_discriminant_R2(double delta, double delta_dynamic) {
  //replace [(1- cos R)/(1- cos R_0)]^n  =>  [(R^2/R_0^2)]^n  in Frixione's isolation formula (this matters when R>1, i.e. at small pT)
  if (frixione_n == 1){return (pow(delta, 2)) / (pow(delta_dynamic, 2));}
  else {return pow((pow(delta, 2)) / (pow(delta_dynamic, 2)), frixione_n);}
}

struct frix_parton {
  int index;
  double delta;
  bool operator<(const frix_parton& rhs) const{return delta<rhs.delta;}
};

void event_set::perform_photon_recombination(vector<int> & no_unrecombined_photon, int i_a){
  static Logger logger("event_set::perform_photon_recombination");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /*
  if (ps_runtime_photon[i_a].size() == 0 || ps_runtime_photon_recombination[i_a].size() == 0){
    no_unrecombined_photon = ps_runtime_photon[i_a];
    return;
  }
  */
    
  vector<int> no_rec_photon;
  vector<int> no_rec_charged_particle;
  no_rec_charged_particle = ps_runtime_photon_recombination[i_a];

  for (int i_p = 0; i_p < ps_runtime_photon_recombination[i_a].size(); i_p++){
    //    logger << LOG_INFO << "charged particle [" << i_a << "][" << i_p << "] with   index = " << ps_runtime_photon_recombination[i_a][i_p] << endl;
  }

  no_rec_photon = ps_runtime_photon[i_a];
  for (int i_p = 0; i_p < ps_runtime_photon[i_a].size(); i_p++){
    //    logger << LOG_INFO << "old: ps_runtime_photon[" << i_a << "][" << i_p << "] = " << no_rec_photon[i_p] << endl;
  }
  no_rec_photon.clear();

  //  int k_g = access_object["photon"];
  //  logger << LOG_INFO << "particle_event[" << 0 << "][" << i_a << "].size() = " << particle_event[0][i_a].size() << endl;
  for (int i_p = 0; i_p < ps_runtime_photon[i_a].size(); i_p++){
    //    logger << LOG_INFO << "Photon [" << 0 << "][" << i_a << "][" << i_p << "] with  rapidity = " << particle_event[0][i_a][ps_runtime_photon[i_a][i_p]].rapidity << "  pT = " << particle_event[0][i_a][ps_runtime_photon[i_a][i_p]].pT << endl;
    // " << particle_event[0][i_a][i_p].momentum
    if (abs(particle_event[0][i_a][ps_runtime_photon[i_a][i_p]].rapidity) < 3){
      no_rec_photon.push_back(ps_runtime_photon[i_a][i_p]);
    }
    else {
      //      logger << LOG_INFO << "Photon with " << particle_event[0][i_a][ps_runtime_photon[i_a][i_p]].momentum << " excluded: rapidity = " << particle_event[0][i_a][ps_runtime_photon[i_a][i_p]].rapidity << endl;
    }
  }

  for (int i_p = 0; i_p < ps_runtime_photon[i_a].size(); i_p++){
    //    logger << LOG_INFO << "new: ps_runtime_photon[" << i_a << "][" << i_p << "] = " << no_rec_photon[i_p] << endl;
  }

  /*
  for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){
    if ((type_parton[i_a][i_p] > 0 && type_parton[i_a][i_p] < 7) ||
	(type_parton[i_a][i_p] < 0 && type_parton[i_a][i_p] > -7) ||
	type_parton[i_a][i_p] == 11 || type_parton[i_a][i_p] == -11 ||
	type_parton[i_a][i_p] == 13 || type_parton[i_a][i_p] == -13 ||
	type_parton[i_a][i_p] == 15 || type_parton[i_a][i_p] == -15){no_rec_charged_particle.push_back(i_p);}
    // || type_parton[i_a][i_p] == 24 || type_parton[i_a][i_p] == -24
    else if (type_parton[i_a][i_p] == 22){no_rec_photon.push_back(i_p);}
  }

  if (no_rec_photon.size() == 0){return;} // obviously, no recombination needed in that case !
  */

  logger << LOG_DEBUG_VERBOSE << "no_rec_charged_particle.size() = " << no_rec_charged_particle.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "no_rec_photon.size()           = " << no_rec_photon.size() << endl;

  for (int i_n = 0; i_n < no_rec_charged_particle.size(); i_n++){
    logger << LOG_DEBUG_VERBOSE << "i_a = " << setw(2) << i_a << "   no_rec_charged_particle[" << i_n << "] = " << no_rec_charged_particle[i_n] << endl;
  }
  for (int i_n = 0; i_n < no_rec_photon.size(); i_n++){
    logger << LOG_DEBUG_VERBOSE << "i_a = " << setw(2) << i_a << "   no_rec_photon[" << i_n << "] = " << no_rec_photon[i_n] << endl;
  }

  vector<vector<double> > distance(no_rec_photon.size(), vector<double> (no_rec_charged_particle.size(), 0.));
  vector<vector<double> > distance_measure_factor(no_rec_photon.size(), vector<double> (no_rec_charged_particle.size(), 1.));
  for (int i_r = 0; i_r < no_rec_photon.size(); i_r++){
    for (int i_c = 0; i_c < no_rec_charged_particle.size(); i_c++){
      if (photon_R_definition == 0){distance[i_r][i_c] = R2_eta(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[i_c]]);}
      else if (photon_R_definition == 1){distance[i_r][i_c] = R2_rapidity(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[i_c]]);}
      else if (photon_R_definition == 11){
	if (particle_event[0][i_a][no_rec_charged_particle[i_c]].pT2 > particle_event[0][i_a][no_rec_photon[i_r]].pT2){
	  distance_measure_factor[i_r][i_c] = 1. / particle_event[0][i_a][no_rec_charged_particle[i_c]].pT2;
	}
	else {
	  distance_measure_factor[i_r][i_c] = 1. / particle_event[0][i_a][no_rec_photon[i_r]].pT2;
	}
	distance[i_r][i_c] = R2_rapidity(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[i_c]]);
      }
      logger << LOG_DEBUG_VERBOSE << "distance[" << i_r << "][" << i_c << "] = " << distance[i_r][i_c] << endl;
    }
  }

  while (no_rec_photon.size() > 0){
    int min1_rec_photon = 0;
    int min1_rec_charged_particle = 0;
    double min1_distance = 1.e99;

    for (int i_r = 0; i_r < no_rec_photon.size(); i_r++){
      logger << LOG_DEBUG_VERBOSE << "phot.: [0][" << i_a << "][" << no_rec_photon[i_r] << "] = " << particle_event[0][i_a][no_rec_photon[i_r]].momentum << endl;
      for (int i_c = 0; i_c < no_rec_charged_particle.size(); i_c++){
	logger << LOG_DEBUG_VERBOSE << "ch.p.: [0][" << i_a << "][" << no_rec_charged_particle[i_c] << "] = " << particle_event[0][i_a][no_rec_charged_particle[i_c]].momentum << "   distance[" << i_r << "][" << i_c << "] = " << setprecision(5) << distance[i_r][i_c] << " photon_R2 = " << photon_R2 << endl;
	if (photon_R_definition < 10){
	  if (distance[i_r][i_c] < min1_distance){
	    min1_rec_photon = i_r;
	    min1_rec_charged_particle = i_c;
	    min1_distance = distance[i_r][i_c];
	  }
	}
	else {
	  if (distance[i_r][i_c] * distance_measure_factor[i_r][i_c]< min1_distance){
	    min1_rec_photon = i_r;
	    min1_rec_charged_particle = i_c;
	    min1_distance = distance[i_r][i_c] * distance_measure_factor[i_r][i_c];
	  }
	}
      }
    }


    // check if two leptons would be combined first (which in the present case would lead to a rejected event immediately (full jet-like algorithm needed later...):
    if (photon_R_definition == 11){
      for (int i_c = 0; i_c < no_rec_charged_particle.size(); i_c++){
	if (i_c == min1_rec_charged_particle){continue;}
	double temp_lepton_distance_R2 = R2_rapidity(particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]], particle_event[0][i_a][no_rec_charged_particle[i_c]]);
 	// hard-coded 0.2^2 !!!
	if (temp_lepton_distance_R2 > 0.04){continue;}

	double temp_lepton_distance_measure;
	if (particle_event[0][i_a][no_rec_charged_particle[i_c]].pT2 > particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]].pT2){
	  temp_lepton_distance_measure = 1. / particle_event[0][i_a][no_rec_charged_particle[i_c]].pT2;
	}
	else {
	  temp_lepton_distance_measure = 1. / particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]].pT2;
	}
	double temp_lepton_distance = temp_lepton_distance_measure * temp_lepton_distance_R2;
	if (temp_lepton_distance < min1_distance){
	  //	  logger << LOG_INFO << "Lepton " << no_rec_charged_particle[min1_rec_charged_particle] << " would be combined with lepton " << no_rec_charged_particle[i_c] << " before photon recombination -> Event is rejected." << endl;
	  particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]] = particle();
	  particle_event[0][i_a][no_rec_charged_particle[i_c]] = particle();
	  return;
	}
      }
    }


    //    if (min1_distance < photon_R2){
    if (distance[min1_rec_photon][min1_rec_charged_particle] < photon_R2){
      particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]] = particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]] + particle_event[0][i_a][no_rec_photon[min1_rec_photon]];
      particle_event[0][i_a][no_rec_photon[min1_rec_photon]] = particle();

      logger << LOG_DEBUG_VERBOSE << "particle_event[0][" << i_a << "][" << no_rec_charged_particle[min1_rec_charged_particle] << "] = " << particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]].momentum << endl;
	    //      no_unrecombined_photon.push_back(no_rec_photon[min1_rec_photon]); // why should this happen ???
      no_rec_photon.erase(no_rec_photon.begin() + min1_rec_photon, no_rec_photon.begin() + min1_rec_photon + 1);
      distance.erase(distance.begin() + min1_rec_photon, distance.begin() + min1_rec_photon + 1);
      if (photon_R_definition == 11){
	distance_measure_factor.erase(distance_measure_factor.begin() + min1_rec_photon, distance_measure_factor.begin() + min1_rec_photon + 1);
      }

      logger << LOG_DEBUG_VERBOSE << "Photon " << min1_rec_charged_particle << " (" << no_rec_charged_particle[min1_rec_charged_particle] << ") recombined." << endl;
      for (int i_r = 0; i_r < no_rec_photon.size(); i_r++){
	if (photon_R_definition == 0){distance[i_r][min1_rec_charged_particle] = R2_eta(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]]);}
	else if (photon_R_definition == 1){distance[i_r][min1_rec_charged_particle] = R2_rapidity(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]]);}
	else if (photon_R_definition == 11){
	  if (particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]].pT2 > particle_event[0][i_a][no_rec_photon[i_r]].pT2){
	    distance_measure_factor[i_r][min1_rec_charged_particle] = 1. / particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]].pT2;
	  }
	  else {
	    distance_measure_factor[i_r][min1_rec_charged_particle] = 1. / particle_event[0][i_a][no_rec_photon[i_r]].pT2;
	  }
	  distance[i_r][min1_rec_charged_particle] = R2_rapidity(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]]);
	}
	//      logger << LOG_DEBUG_VERBOSE << "Photon " << min1_rec_charged_particle << " (" << no_rec_charged_particle[min1_rec_charged_particle] << ") recombined." << endl;
	//	logger << LOG_DEBUG_VERBOSE << "new_distance[" << i_r << "][" << min1_rec_charged_particle << "] = " << distance[i_r][min1_rec_charged_particle] << endl;
		//
      //	distance[i_r][min1_rec_charged_particle] = R2_rapidity(particle_event[0][i_a][no_rec_photon[i_r]], particle_event[0][i_a][no_rec_charged_particle[min1_rec_charged_particle]]);
      }
    }
    else {
      //      logger << LOG_DEBUG_VERBOSE << "No combination done!" << endl;
      for (int i_r = 0; i_r < no_rec_photon.size(); i_r++){
	no_unrecombined_photon.push_back(no_rec_photon[i_r]);
	//	particle_event[0][i_a][no_rec_photon[i_r]] = fourvector();
	//	logger << LOG_DEBUG_VERBOSE << "no_rec_photon[i_r] = " << no_rec_photon[i_r] << endl;
	no_rec_photon.erase(no_rec_photon.begin() + i_r);

	//	logger << LOG_DEBUG_VERBOSE << "particle_event[0][i_a][no_rec_photon[i_r]] = " << particle_event[0][i_a][no_rec_photon[i_r]].momentum() << endl;
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "no_rec_photon.size() = " << no_rec_photon.size() << endl;
  for (int i_r = 0; i_r < no_rec_photon.size(); i_r++){
    logger << LOG_DEBUG_VERBOSE << "no_rec_photon[i_r] = " << no_rec_photon[i_r] << endl;
  }
  logger << LOG_DEBUG_VERBOSE << "no_unrecombined_photon.size() = " << no_unrecombined_photon.size() << endl;
  for (int i_r = 0; i_r < no_unrecombined_photon.size(); i_r++){
    logger << LOG_DEBUG_VERBOSE << "no_unrecombined_photon[i_r] = " << no_unrecombined_photon[i_r] << endl;
  }
  logger << LOG_DEBUG_VERBOSE << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// Frixione photon isolation routine with updated interface.
// old: Adds 'momentum' to vector 'isolated_photon' and 1 to number_photon
void event_set::perform_frixione_isolation(int & number_photon, vector<particle> & isolated_photon, particle & photon, vector<particle> & protojet){
  static Logger logger("frixione_isolation");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  vector<frix_parton> frixione_order_parton;
  //  frix_parton tmp_parton;
  static vector<frix_parton> frixione_order_parton;
  frixione_order_parton.clear();
  static frix_parton tmp_parton;

  //  static double frixione_delta2_0 = pow(frixione_delta_0,2);
  static double frixione_delta2_0 = pow(frixione_delta_0, 2);
  static double delta_dynamic = 0.;
  if (frixione_isolation == 3 || frixione_isolation == 4 || frixione_isolation == 5){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon); // FIXME -> M_Z as a fixed value by now
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 6){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon) * sqrt(2.); // FIXME -> M_Z as a fixed value by now
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 7){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon) * 2.; // FIXME -> M_Z as a fixed value by now
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 8){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon) * 2. * sqrt(2.); // FIXME -> M_Z as a fixed value by now
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 9){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon) * 4.; // FIXME -> M_Z as a fixed value by now
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 10){
    if (photon.pT < 2 * msi->M_Z){delta_dynamic = frixione_delta_0;}
    else {delta_dynamic = msi->M_Z / photon.pT;}
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }
  if (frixione_isolation == 11){
    delta_dynamic = msi->M_Z / photon.pT / sqrt(frixione_epsilon);
    if (delta_dynamic > frixione_delta_0){delta_dynamic = frixione_delta_0;}
    frixione_delta2_0 = pow(delta_dynamic, 2);
  }

  //    cout << "delta_dynamic = " << delta_dynamic << "   msi->M_Z = " << msi->M_Z << "   photon.pT = " << photon.pT << endl;

  // identify partons inside the cone
  logger << LOG_DEBUG_VERBOSE << "photon = " << photon.momentum << endl;
  for (int i = 0; i < protojet.size(); i++){
    //    logger << LOG_DEBUG_VERBOSE << "protojet[" << i << "] = " << protojet[i].momentum << endl;
    //    logger << LOG_DEBUG_VERBOSE << "delta_2 = " << delta_2 << endl;
    //    logger << LOG_DEBUG_VERBOSE << "frixione_delta2_0 = " << frixione_delta2_0 << endl;
    double delta_2 = 0.;
    if (frixione_isolation == 5 ||
	frixione_isolation == 6 ||
	frixione_isolation == 7 ||
	frixione_isolation == 8 ||
	frixione_isolation == 9){delta_2 = R2_coshrapidity_cosphi(photon.momentum, protojet[i].momentum);}
    else {delta_2 = R2_eta(photon, protojet[i]);}

    if (delta_2 < frixione_delta2_0) {
      tmp_parton.index = i;
      tmp_parton.delta = sqrt(delta_2);//sqrt(R2_eta(photon, protojet[i]));
      frixione_order_parton.push_back(tmp_parton);
    }
  }
  sort(frixione_order_parton.begin(), frixione_order_parton.end());
  logger << LOG_DEBUG_VERBOSE << "frixione_order_parton.size() = " << frixione_order_parton.size() << endl;

  int isolation = 1;
  double E_i_sum = 0.;
  for (int i = 0; i < frixione_order_parton.size(); i++){
    double H_i = 0.;
    if      (frixione_isolation == 1){H_i = frixione_epsilon * photon.pT * frixione_discr(frixione_order_parton[i].delta);} // standard definition
    else if (frixione_isolation == 2){H_i = frixione_fixed_ET_max * frixione_discr(frixione_order_parton[i].delta);} // FIXME -> 10GeV as a fixed limit by now
    else if (frixione_isolation == 3){H_i = frixione_epsilon * photon.pT * frixione_discr(frixione_order_parton[i].delta, delta_dynamic);} // dynamic cone (photon.pT dependent)
    else if (frixione_isolation == 4){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 5){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 6){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 7){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 8){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 9){H_i = frixione_epsilon * photon.pT * frixione_discriminant_R2(frixione_order_parton[i].delta, delta_dynamic);} // delta definition via (R²/R_0²)^n !!!
    else if (frixione_isolation == 10){H_i = frixione_epsilon * photon.pT * frixione_discr(frixione_order_parton[i].delta, delta_dynamic);} // dynamic cone (photon.pT dependent)
    else if (frixione_isolation == 11){H_i = frixione_epsilon * photon.pT * frixione_discr(frixione_order_parton[i].delta, delta_dynamic);} // dynamic cone (photon.pT dependent)
    else if (frixione_isolation == 12){H_i = frixione_fixed_ET_max * frixione_discriminant_R2(frixione_order_parton[i].delta);}
    else {assert(false);}

    E_i_sum += protojet[frixione_order_parton[i].index].pT;
    if (E_i_sum > H_i){
      isolation = 0;
      break;
    }

  }
  if (isolation){
    number_photon++;
    isolated_photon.push_back(photon);
    logger << LOG_DEBUG_VERBOSE << "number_photon = " << number_photon << endl;
    logger << LOG_DEBUG_VERBOSE << "isolated_photon = " << isolated_photon[isolated_photon.size() - 1] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
