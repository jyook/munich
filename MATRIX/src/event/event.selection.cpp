#include "header.hpp"

void event_set::perform_event_selection(){
  static Logger logger("event_set::perform_event_selection");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (Log::getLogThreshold() <= LOG_DEBUG_POINT){
    logger.newLine(LOG_DEBUG_POINT);
    logger << LOG_DEBUG_POINT // << "[" << setw(2) << i_a << "]"
	   << " New PSP: "
	   << "   i_gen = " << setw(12) << psi->i_gen
	   << "   i_acc = " << setw(12) << psi->i_acc
	   << "   i_rej = " << setw(12) << psi->i_rej
	   << "   i_nan = " << setw(4) << psi->i_nan
	   << "   i_tec = " << setw(4) << psi->i_tec
	   << endl;
    logger.newLine(LOG_DEBUG_POINT);

    logger << LOG_DEBUG_POINT << "p_parton.size() = " << p_parton.size() << endl;
    stringstream temp_psp;
    temp_psp << "Final-state parton-level momenta (partonic CMS frame):" << endl;
    for (int i_p = 1; i_p < p_parton[0].size(); i_p++){
      temp_psp << "type_parton[" << setw(2) << 0 << "][" << i_p << "] = "
	       << setw(3) << csi->type_parton[0][i_p]
	       << " -> "
	       << p_parton[0][i_p] << endl;
    }
    temp_psp << endl;
    logger << LOG_DEBUG_POINT << temp_psp.str() << endl;
    logger.newLine(LOG_DEBUG_POINT);
  }

  for (int i_a = 0; i_a < cut_ps.size(); i_a++){
    // should not be needed, as set to correct value later:
    cut_ps[i_a] = 0;

    logger << LOG_DEBUG_POINT << "before: cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
    perform_event_selection_phasespace(i_a);
    ///
    if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after generic event selection, before user-defined particles   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

    if (cut_ps[i_a] == -1){continue;}
    //    logger << LOG_INFO << "user->particle_name.size() = " << user->particle_name.size() << endl;
    if (user->particle_name.size() > 0){
      for (int i_g = 0; i_g < user->particle_name.size(); i_g++){user_particle[i_g][i_a].clear();}

      particles(i_a);
      
      //      for (int i_g = 0; i_g < user->particle_name.size(); i_g++){for (int i_p = 0; i_p < user_particle[i_g][i_a].size(); i_p++){logger << LOG_INFO << "user_particle[" << i_g << "][" << i_a << "][" << i_p << "] = " << user_particle[i_g][i_a][i_p] << endl;}}

      for (int i_g = 0; i_g < user->particle_name.size(); i_g++){
	int k_g = access_object[user->particle_name[i_g]];
	for (int i_p = 0; i_p < user_particle[i_g][i_a].size(); i_p++){
	  if (abs(user_particle[i_g][i_a][i_p].rapidity) < pda[k_g].define_y && 
	      abs(user_particle[i_g][i_a][i_p].eta) < pda[k_g].define_eta && 
	      user_particle[i_g][i_a][i_p].pT > pda[k_g].define_pT && 
	      user_particle[i_g][i_a][i_p].ET > pda[k_g].define_ET){
	    n_object[k_g][i_a]++;
	    particle_event[k_g][i_a].push_back(user_particle[i_g][i_a][i_p]);
	  }
	}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   " << pda[k_g].n_observed_min << " <= n_object[" << setw(2) << k_g << "][" << setw(2) << i_a << "] = " << n_object[k_g][i_a] << " <= " << pda[k_g].n_observed_max << endl;}
	if ((n_object[k_g][i_a] < pda[k_g].n_observed_min) || (n_object[k_g][i_a] > pda[k_g].n_observed_max)){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after user_particle[" << i_g << "] " << user->particle_name[i_g] << endl;}
	  logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after user_particle[" << i_g << "] " << user->particle_name[i_g] << endl; 
	  ///	  if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
	  return;
	}
	if (n_object[k_g][i_a] > 1){sort(particle_event[k_g][i_a].begin(), particle_event[k_g][i_a].end(), greaterBypT);}
      }
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   event selection of user-defined particles passed and cut_ps[" << i_a << "] set   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
      ///      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str();}
    }
    //      if (user->particle_name.size() > 0){generic.particles(i_a, this);}
    ///
    if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after user-defined particles, before general fiducial cuts   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

    if (cut_ps[i_a] == -1){continue;}

    // application of general cuts set via fiducialcut interface:
    logger << LOG_DEBUG_POINT << "cut_ps[" << i_a << "] = " << cut_ps[i_a] << "   before fiducialcut." << endl;
    for (int i_f = 0; i_f < fiducial_cut.size(); i_f++){
      fiducial_cut[i_f].apply_fiducialcut(i_a);
      if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after fiducialcut   " << fiducial_cut[i_f].name << "   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

      if (cut_ps[i_a] == -1){continue;}
    }

    if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after general fiducial cuts, before individual event selection   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}


    if (switch_output_cutinfo){
      for (int i_o = 0; i_o < object_list_selection.size(); i_o++){
	int j_o = equivalent_no_object[i_o]; 
	// i_o -> number in relevant_object_list
	// j_o -> number in object_list
	info_cut << "[" << setw(2) << i_a << "]   object_list_selection[" << setw(2) << i_o << "] = " << setw(6) << object_list_selection[i_o] << "   n_object[" << setw(2) << j_o << "][" << setw(2) << i_a << "] = " << n_object[j_o][i_a] << "   particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "].size() = " << particle_event[j_o][i_a].size() << endl;
	for (int i_p = 0; i_p < n_object[j_o][i_a]; i_p++){
	  info_cut << "       particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "][" << setw(2) << i_p << "] = " << particle_event[j_o][i_a][i_p].momentum << endl;
	}
      }
    }
    
    cuts(i_a);

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   event selection of user-defined cuts passed and cut_ps[" << i_a << "] set   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
    ///    if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str();}
    //      generic.cuts(i_a, this);
    ///
    if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after individual cuts   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
    logger << LOG_DEBUG_VERBOSE << "after:  cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
  }
  /*
  }
  else if (switch_testcut == 1){
    cuts_test(0);
    //    generic.cuts_test(0, this);
  }
  */

  for (int i_a = 0; i_a < cut_ps.size(); i_a++){
    logger << LOG_DEBUG_POINT << "cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
  }

}

/*
// To be removed as soon as generic-free implementation works !!!
void event_set::perform_event_selection(call_generic & generic){
  static Logger logger("event_set::perform_event_selection");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (Log::getLogThreshold() <= LOG_DEBUG_POINT){
    logger.newLine(LOG_DEBUG_POINT);
    logger << LOG_DEBUG_POINT // << "[" << setw(2) << i_a << "]"
	   << " New PSP: "
	   << "   i_gen = " << setw(12) << psi->i_gen
	   << "   i_acc = " << setw(12) << psi->i_acc
	   << "   i_rej = " << setw(12) << psi->i_rej
	   << "   i_nan = " << setw(4) << psi->i_nan
	   << "   i_tec = " << setw(4) << psi->i_tec
	   << endl;
    logger.newLine(LOG_DEBUG_POINT);

    stringstream temp_psp;
    temp_psp << "Final-state parton-level momenta (partonic CMS frame):" << endl;
    for (int i_p = 1; i_p < p_parton[0].size(); i_p++){
      temp_psp << "type_parton[" << setw(2) << 0 << "][" << i_p << "] = "
	       << setw(3) << csi->type_parton[0][i_p]
	       << " -> "
	       << p_parton[0][i_p] << endl;
    }
    temp_psp << endl;
    logger << LOG_DEBUG_POINT << temp_psp.str() << endl;
    logger.newLine(LOG_DEBUG_POINT);
  }

  if (switch_testcut == 0){
    for (int i_a = 0; i_a < cut_ps.size(); i_a++){
      // should not be needed, as set to correct value later:
      cut_ps[i_a] = 0;

      logger << LOG_DEBUG_POINT << "before: cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
      perform_event_selection_phasespace(i_a);
      ///
      if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after generic event selection, before user-defined particles   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

      if (cut_ps[i_a] == -1){continue;}

      if (user->particle_name.size() > 0){generic.particles(i_a, this);}
      ///
      if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after user-defined particles, before general fiducial cuts   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

      if (cut_ps[i_a] == -1){continue;}

      // application of general cuts set via fiducialcut interface:
      logger << LOG_DEBUG_POINT << "cut_ps[" << i_a << "] = " << cut_ps[i_a] << "   before fiducialcut." << endl;
      for (int i_f = 0; i_f < fiducial_cut.size(); i_f++){
	fiducial_cut[i_f].apply_fiducialcut(i_a);
	if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after fiducialcut   " << fiducial_cut[i_f].name << "   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

	if (cut_ps[i_a] == -1){continue;}
      }

      if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after general fiducial cuts, before individual event selection   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

      generic.cuts(i_a, this);
      ///
      if (switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after individual cuts   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
      logger << LOG_DEBUG_VERBOSE << "after:  cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
    }
  }
  else if (switch_testcut == 1){
    generic.cuts_test(0, this);
  }


  for (int i_a = 0; i_a < cut_ps.size(); i_a++){
    logger << LOG_DEBUG_POINT << "cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
  }

}

*/

void event_set::perform_event_selection_phasespace(int i_a){
  static Logger logger("event_set::perform_event_selection_phasespace");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static int n_original = 0;
  if (initialization == 1){
    // re-extract the number of particles that enter the event selection without modifications
    logger << LOG_DEBUG_VERBOSE << "initialization = " << initialization << endl;
    vector<int> temp_original;
    for (int i_g = 0; i_g < runtime_original.size(); i_g++){
      int j_g = runtime_original[i_g];
      for (int i_o = 0; i_o < runtime_order[j_g].size(); i_o++){
        int i_p = runtime_order[j_g][i_o];
        int flag = 0;
        for (int i = 0; i < temp_original.size(); i++){if (i_p == temp_original[i]){flag = 1; break;}}
        if (flag == 0){temp_original.push_back(i_p);}
      }
    }
    n_original = temp_original.size();
    for (int i = 0; i < temp_original.size(); i++){logger << LOG_DEBUG_VERBOSE << "temp_original[" << i << "] = " << temp_original[i] << endl;}
    logger << LOG_DEBUG_VERBOSE << "n_original = " << n_original << endl;

    initialization = 0;
  }



  ///  stringstream info_cut;
  ///  static stringstream info_cut;
  if (switch_output_cutinfo){
    ///    info_cut.clear();
    ///    info_cut.str("");
    info_cut << endl;
    info_cut << "[" << setw(2) << i_a << "]   generic event selection" << endl;
    info_cut << "[" << setw(2) << i_a << "]   parton-level momenta:" << endl;
    for (int i_p = 1; i_p < p_parton[i_a].size(); i_p++){
      info_cut << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << csi->type_parton[i_a][i_p] << " -> " << particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << particle_event[0][i_a][i_p].pT << " eta = " << particle_event[0][i_a][i_p].eta << endl;
    }
    info_cut << endl;
  }


  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (qT subtraction)):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

  //  if (csi->class_contribution_qTcut_explicit){event_selection_qTcut(i_a);}
  if (csi->class_contribution_qTcut_explicit){qT_basic->event_selection_qTcut(i_a);}
  //  if (csi->class_contribution_qTcut_implicit){cut_ps[i_a] = n_qTcut - 1;}
  if (cut_ps[i_a] == -1){return;}

  for (int i_p = 1; i_p < 3; i_p++){particle_event[0][i_a][i_p] = particle(p_parton[i_a][i_p].zboost(-psi->boost));}

  for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){particle_event[0][i_a][i_p] = particle(p_parton[i_a][i_p].zboost(-psi->boost));}
  for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){logger << LOG_DEBUG_VERBOSE << "particle_event[0][" << i_a << "][" << i_p << "] = " << particle_event[0][i_a][i_p] << endl;}


  if (Log::getLogThreshold() <= LOG_DEBUG_POINT){
    stringstream temp_psp;
    temp_psp << "Final-state parton-level momenta (hadronic CMS = LAB frame):" << endl;
    for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){
      temp_psp << "type_parton[" << setw(2) << i_a << "][" << i_p << "] = "
	       << setw(3) << csi->type_parton[0][i_p]
	       << " -> "
	       << "particle_event = " << particle_event[0][i_a][i_p].momentum << endl;
      // << " m = " << particle_event[0][0][i_p].m << " pT = " << particle_event[0][0][i_p].pT << " eta = " << particle_event[0][0][i_p].eta << endl;
    }

    //  cerr << temp_psp.str() << endl;
    logger << LOG_DEBUG_POINT << temp_psp.str() << endl;
    logger.newLine(LOG_DEBUG_POINT);
  }





  for (int i_o = 1; i_o < object_list_selection.size(); i_o++){
    int j_o = equivalent_no_object[i_o];
    // i_o -> number in relevant_object_list
    // j_o -> number in object_list
    //    logger << LOG_DEBUG_VERBOSE << "particle_event[" << i_o << " -> " << j_o << "][" << i_a << "].size() = " << particle_event[j_o][i_a].size() << "    n_object = " <<  n_object[j_o][i_a] << endl;
    particle_event[j_o][i_a].clear();
    n_object[j_o][i_a] = 0;
    //    logger << LOG_DEBUG_VERBOSE << "particle_event[" << i_o << " -> " << j_o << "][" << i_a << "].size() = " << particle_event[j_o][i_a].size() << "    n_object = " <<  n_object[j_o][i_a] << endl;
  }


  logger << LOG_DEBUG_VERBOSE << "photon_recombination = " << photon_recombination << endl;

  ///  vector<int> no_unrecombined_photon;
  //  static vector<int> no_unrecombined_photon;
  no_unrecombined_photon.clear();

  //  for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){logger << LOG_DEBUG_VERBOSE << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << csi->type_parton[i_a][i_p] << " -> " << particle_event[0][i_a][i_p].momentum << endl;


  ////////////////////////////
  //  photon recombination  //
  ////////////////////////////

  //  Recombinable particles are defined in  photon_recombination_list  (defined via  photon_recombination_selection  and  photon_recombination_disable ).
  //  If a particle is dressed with a photon, the combined momentum is stored at the position of its naked momentum, and the momentum of the recombined photon is empty.
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_recombination: " << "   photon_recombination = " << photon_recombination << endl;}

  //  if (photon_recombination){
  if (photon_recombination && ps_runtime_photon_recombination[i_a].size() > 0 && ps_runtime_photon[i_a].size() > 0){
    perform_photon_recombination(no_unrecombined_photon, i_a);
    /*
    for (int i_c = 0; i_c < ps_runtime_photon_recombination[i_a].size(); i_c++){
      //      logger << LOG_INFO << "ps_runtime_photon_recombination[" << i_a << "][" << i_c << "] = " << ps_runtime_photon_recombination[i_a][i_c] << "   pT2 = " << particle_event[0][i_a][ps_runtime_photon_recombination[i_a][i_c]].pT2  << endl;
      if (particle_event[0][i_a][ps_runtime_photon_recombination[i_a][i_c]].pT2 == 0.){
	logger << LOG_INFO << "i_a = " << i_a << "   Lepton " << ps_runtime_photon_recombination[i_a][i_c] << " is removed in recombination procedure." << "   pT2 = " << particle_event[0][i_a][ps_runtime_photon_recombination[i_a][i_c]].pT2 << endl;
      }
    }
    */
  }
  else {no_unrecombined_photon = ps_runtime_photon[i_a];}
  //  Afterwards,  no_unrecombined_photon  contains positions of un-recombined photons in  particle_event[0][i_a]  (all photons if no dressing is done).

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   no_unrecombined_photon.size() = " << no_unrecombined_photon.size() << endl;}
  if (switch_output_cutinfo){
    if (no_unrecombined_photon.size() < ps_runtime_photon[i_a].size()){
      for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){
	info_cut << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << csi->type_parton[i_a][i_p] << " -> " << particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << particle_event[0][i_a][i_p].pT << " eta = " << particle_event[0][i_a][i_p].eta << endl;
      }
    }
  }
  logger << LOG_DEBUG_VERBOSE << "no_unrecombined_photon.size() = " << no_unrecombined_photon.size() << endl;



  ////////////////////////
  //  runtime original  //
  ////////////////////////

  //  runtime_original   actually needed only once because "original" particles don't change on reduced phase spaces -> check evaluation of runtime_original !!!
  //  runtime_original   collects partons that enter event selection with their parton-level momenta (after photon dressing if applicable).
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_original: " << "   runtime_original.size() = " << runtime_original.size() << endl;}

  logger << LOG_DEBUG_VERBOSE << "runtime_original" << "   runtime_original.size() = " << runtime_original.size() << endl;
  for (int i_g = 0; i_g < runtime_original.size(); i_g++){
    // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_original
    int j_g = runtime_original[i_g];  // j_g -> number in relevant_object_list
    int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
    // later...   int k_g = j_g;

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_original[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;}

    logger << LOG_DEBUG_VERBOSE << "runtime_original: i_g = " << i_g << "   j_g = " << j_g << " (" << object_list_selection[j_g] << ")   k_g = " << k_g << " (" << object_list[k_g] << ")" << endl;
    logger << LOG_DEBUG_VERBOSE << "n_object[" << k_g << "][" << i_a << "] = " << n_object[k_g][i_a] << endl;

    for (int i_o = 0; i_o < runtime_order[j_g].size(); i_o++){
      int i_p = runtime_order[j_g][i_o];  // i_p -> position in  particle_event[0][i_a]  of parton belong to the respective object class
      if (switch_output_cutinfo){info_cut << "runtime_original: " << i_g << "   particle_event[0][" << i_a << "][" << i_p << "] = " << particle_event[0][i_a][i_p] << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					      << "   y: " << setw(10) << abs(particle_event[0][i_a][i_p].rapidity) << " < " << setw(8) << pds[j_g].define_y
					      << "   eta:  " << setw(10) << abs(particle_event[0][i_a][i_p].eta) << " < " << setw(8) << pds[j_g].define_eta
					      << "   pT:  " << setw(10) << abs(particle_event[0][i_a][i_p].pT) << " > " << setw(8) << pds[j_g].define_pT
					      << "   ET:  " << setw(10) << abs(particle_event[0][i_a][i_p].ET) << " > " << setw(8) << pds[j_g].define_ET
					      << "   ---   "
					      << (abs(particle_event[0][i_a][i_p].rapidity) < pds[j_g].define_y &&
						  abs(particle_event[0][i_a][i_p].eta) < pds[j_g].define_eta &&
						  particle_event[0][i_a][i_p].pT >= pds[j_g].define_pT &&
						  particle_event[0][i_a][i_p].ET >= pds[j_g].define_ET) << endl;}

      if (abs(particle_event[0][i_a][i_p].rapidity) < pds[j_g].define_y &&
	  abs(particle_event[0][i_a][i_p].eta) < pds[j_g].define_eta &&
	  particle_event[0][i_a][i_p].pT >= pds[j_g].define_pT &&
	  particle_event[0][i_a][i_p].ET >= pds[j_g].define_ET){
	n_object[k_g][i_a]++;
	particle_event[k_g][i_a].push_back(particle_event[0][i_a][i_p]);
      }
    }
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   " << pds[j_g].n_observed_min << " <= n_object[" << setw(2) << k_g << "][" << setw(2) <<i_a << "] = " << n_object[k_g][i_a] << " <= " << pds[j_g].n_observed_max << endl;}

    if ((n_object[k_g][i_a] < pds[j_g].n_observed_min) || (n_object[k_g][i_a] > pds[j_g].n_observed_max)){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_original" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_original" << endl;
      ///      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
    if (n_object[k_g][i_a] > 1){advanced_sort_by_pT(i_a, k_g);}
    //    if (n_object[k_g][i_a] > 1){sort(particle_event[k_g][i_a].begin(), particle_event[k_g][i_a].end(), greaterBypT);}
  }

  if (switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_original:" << endl;
    for (int i_g = 0; i_g < runtime_original.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_original
      int j_g = runtime_original[i_g];  // j_g -> number in relevant_object_list
      int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_original[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }


  ///////////////////////
  //  runtime missing  //
  ///////////////////////

  if (ps_runtime_missing[i_a].size() > 0 ){
    //      runtime_missing   actually needed only once because "invisible" particles don't change on reduced phase spaces -> check evaluation of runtime_missing !!!
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   ps_runtime_missing[" << i_a << "].size() = " << ps_runtime_missing[i_a].size() << endl;}
    //    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   runtime_missing.size() = " << runtime_missing.size() << endl;}
    logger << LOG_DEBUG_VERBOSE << "runtime_missing" << "   runtime_missing.size() = " << runtime_missing.size() << endl;
    //    logger << LOG_INFO << "runtime_missing" << "   runtime_missing.size() = " << runtime_missing.size() << endl;
    //    logger << LOG_INFO << "runtime_missing" << "   ps_runtime_missing[" << i_a << "].size() = " << ps_runtime_missing[i_a].size() << endl;

    int j_g = observed_object_selection["missing"];  // j_g -> number in relevant_object_list
    int k_g = observed_object["missing"];  //  k_g -> number in object_list
    // later...   int k_g = j_g;
    //  cout << "j_g = " << j_g << "   k_g = " << k_g << endl;
    fourvector p_missing;
    for (int i_p = 0; i_p < ps_runtime_missing[i_a].size(); i_p++){
      //    cout << "
      //      logger << LOG_INFO << "particle_event[0][" << i_a << "][" << ps_runtime_missing[i_a][i_p] << "].momentum = " << particle_event[0][i_a][ps_runtime_missing[i_a][i_p]].momentum << endl;
      p_missing = p_missing + particle_event[0][i_a][ps_runtime_missing[i_a][i_p]].momentum;
    }
    particle_event[k_g][i_a].push_back(particle(p_missing));
    n_object[k_g][i_a] = 1;

    //    logger << LOG_INFO << "particle_event[" << k_g << "][" << i_a << "][0] = " << particle_event[k_g][i_a][0].momentum << "   pT = " << particle_event[k_g][i_a][0].pT << endl;

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					    << "   pT:  " << setw(10) << particle_event[k_g][i_a][0].pT << " > " << setw(8) << pds[j_g].define_pT
					    << "   ET:  " << setw(10) << particle_event[k_g][i_a][0].ET << " > " << setw(8) << pds[j_g].define_ET
					    << "   ---   "
					    << (particle_event[k_g][i_a][0].pT < pds[j_g].define_pT ||
						particle_event[k_g][i_a][0].ET < pds[j_g].define_ET) << endl;}

    if (particle_event[k_g][i_a][0].pT < pds[j_g].define_pT || particle_event[k_g][i_a][0].ET < pds[j_g].define_ET){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_missing" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_missing" << endl;
      ///      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }

    if (switch_output_cutinfo){
      info_cut << endl;
      info_cut << "Summary after runtime_missing:" << endl;
      //      for (int i_g = 0; i_g < runtime_missing.size(); i_g++){
      for (int i_g = 0; i_g < ps_runtime_missing[i_a].size(); i_g++){
	info_cut << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
      }
      info_cut << endl;
    }
    //    logger << LOG_INFO << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
  }

  /*
  //      runtime_missing   actually needed only once because "invisible" particles don't change on reduced phase spaces -> check evaluation of runtime_missing !!!
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   runtime_missing.size() = " << runtime_missing.size() << endl;}
  logger << LOG_DEBUG_VERBOSE << "runtime_missing" << "   runtime_missing.size() = " << runtime_missing.size() << endl;

  for (int i_g = 0; i_g < runtime_missing.size(); i_g++){
    int j_g = runtime_missing[i_g];       // j_g -> number in relevant_object_list
    int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
    // later...   int k_g = j_g;

    fourvector p_missing;
    for (int i_o = 0; i_o < runtime_order[j_g].size(); i_o++){
      int i_p = runtime_order[j_g][i_o];
      p_missing = p_missing + particle_event[0][i_a][i_p].momentum;
    }
    particle_event[k_g][i_a].push_back(particle(p_missing));

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					    << "   pT:  " << setw(10) << particle_event[k_g][i_a][0].pT << " > " << setw(8) << pds[j_g].define_pT
					    << "   ET:  " << setw(10) << particle_event[k_g][i_a][0].ET << " > " << setw(8) << pds[j_g].define_ET
					    << "   ---   "
					    << (particle_event[k_g][i_a][0].pT < pds[j_g].define_pT ||
						particle_event[k_g][i_a][0].ET < pds[j_g].define_ET) << endl;}


    if (particle_event[k_g][i_a][0].pT < pds[j_g].define_pT || particle_event[k_g][i_a][0].ET < pds[j_g].define_ET){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_missing" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_missing" << endl;
      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
  }

  if (switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_missing:" << endl;
    for (int i_g = 0; i_g < runtime_missing.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_missing
      int j_g = runtime_missing[i_g];  // j_g -> number in relevant_object_list
      int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_missing[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }

  */

  //////////////////////////////////
  //  determination of protojets  //
  //////////////////////////////////

  // particle_protojet  does not contain photons here - added later after Frixione isolation.
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   jet_algorithm = " << jet_algorithm << endl;}
  logger << LOG_DEBUG_VERBOSE << "jet algorithm: select protojet candidates" << endl;

  particle_protojet.clear();
  protojet_flavour.clear();
  protojet_parton_origin.clear();

  for (int i_l = 0; i_l < ps_runtime_jet_algorithm[i_a].size(); i_l++){
    int i_p = ps_runtime_jet_algorithm[i_a][i_l];
    particle_protojet.push_back(particle_event[0][i_a][i_p]);

    protojet_parton_origin.push_back(vector<int> (1, i_p));
    if (abs(csi->type_parton[i_a][i_p]) < 5){protojet_flavour.push_back(vector<int> (1, 0));}  // treatment of light jets -> flavour numbers between -4 and +4  ->  0 (ljet)
    else {protojet_flavour.push_back(vector<int> (1, csi->type_parton[i_a][i_p]));}
    logger << LOG_DEBUG_VERBOSE << "ps_runtime_jet_algorithm[" << i_a << "].size() = " << ps_runtime_jet_algorithm[i_a].size() << endl;
  }
  //  particle_protojet  contains all particles relevant for Frixione isolation.

  for (int i_p = 0; i_p < particle_protojet.size(); i_p++){
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   particle_protojet[" << i_p << "]   pT = " << particle_protojet[i_p].pT << "   ET = " << particle_protojet[i_p].ET << "   flavour = " << protojet_flavour[i_p][0] << "   origin = " << protojet_parton_origin[i_p][0] << endl;}
  }
  logger << LOG_DEBUG_VERBOSE << "phasespace " << i_a << " " << particle_protojet.size() << ": protojets" << endl;
  for (int i_p = 0; i_p < particle_protojet.size(); i_p++){
    logger << LOG_DEBUG_VERBOSE << "particle_protojet[" << i_p << "] = " << particle_protojet[i_p] << endl << "   flavour = " << protojet_flavour[i_p][0] << "   origin = " << protojet_parton_origin[i_p][0] << endl;
  }



  //////////////////////////
  //  Frixione isolation  //
  //////////////////////////

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (frixione_isolation)):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
  if (frixione_isolation){
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_isolation" << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_isolation:   runtime_photon_isolation.size() = " << runtime_photon_isolation.size() << endl;}
    logger << LOG_DEBUG_VERBOSE << "runtime_photon_isolation.size() = " << runtime_photon_isolation.size() << endl;

    particle_protojet_photon.clear();
    protojet_flavour_photon.clear();
    protojet_parton_origin_photon.clear();

    for (int i_g = 0; i_g < runtime_photon_isolation.size(); i_g++){
      int j_g = runtime_photon_isolation[i_g];
      int k_g = equivalent_no_object[j_g];
      logger << LOG_DEBUG_VERBOSE << "no_unrecombined_photon.size() = " << no_unrecombined_photon.size() << endl;
      for (int i_u = 0; i_u < no_unrecombined_photon.size(); i_u++){
	int i_p = no_unrecombined_photon[i_u];
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
						<< "   y: " << setw(10) << abs(particle_event[0][i_a][i_p].rapidity) << " < " << setw(8) << pds[j_g].define_y
						<< "   eta:  " << setw(10) << abs(particle_event[0][i_a][i_p].eta) << " < " << setw(8) << pds[j_g].define_eta
						<< "   pT:  " << setw(10) << abs(particle_event[0][i_a][i_p].pT) << " > " << setw(8) << pds[j_g].define_pT
						<< "   ET:  " << setw(10) << abs(particle_event[0][i_a][i_p].ET) << " > " << setw(8) << pds[j_g].define_ET
						<< "   ---   "
						<< (abs(particle_event[0][i_a][i_p].rapidity) < pds[j_g].define_y &&
						    abs(particle_event[0][i_a][i_p].eta) < pds[j_g].define_eta &&
						    particle_event[0][i_a][i_p].pT >= pds[j_g].define_pT &&
						    particle_event[0][i_a][i_p].ET >= pds[j_g].define_ET) << endl;}

	// possibly deal differently with pT threshold in case of photon-photon recombination !!!
	
	if (abs(particle_event[0][i_a][i_p].rapidity) < pds[j_g].define_y &&
	    abs(particle_event[0][i_a][i_p].eta) < pds[j_g].define_eta &&
	    particle_event[0][i_a][i_p].pT >= pds[j_g].define_pT &&
	    particle_event[0][i_a][i_p].ET >= pds[j_g].define_ET){
	  int temp_number_photon = n_object[k_g][i_a];  //  temp_number_photon = number of identified photons before the one considered now (i_p)
	  logger << LOG_DEBUG_VERBOSE << "temp_number_photon = " << temp_number_photon << endl;
	  perform_frixione_isolation(n_object[k_g][i_a], particle_event[k_g][i_a], particle_event[0][i_a][i_p], particle_protojet);
	  // temporary: particle_protojet_photon etc. could be directly filled...
	  if (temp_number_photon == n_object[k_g][i_a] && photon_jet_algorithm){  //  Happens if the considered photon is not identified. Non-identified photons will enter the jet algorithm afterwards.
	    logger << LOG_DEBUG_VERBOSE << "Add un-identified photons to jet candidates" << endl;
	    particle_protojet_photon.push_back(particle_event[0][i_a][i_p]);
	    protojet_parton_origin_photon.push_back(vector<int> (1, i_p));
	    protojet_flavour_photon.push_back(vector<int> (1, 22)); // photon
	    logger << LOG_DEBUG_VERBOSE << "particle_protojet_photon.size() = " << particle_protojet_photon.size() << endl;
	  }
	  logger << LOG_DEBUG_VERBOSE << "particle_event[" << k_g << "][" << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "n_object[" << k_g << "][" << i_a << "] = " << n_object[k_g][i_a] << endl;
	}
	else if (photon_jet_algorithm){  //  Non-identified photons will enter the jet algorithm afterwards.
	  particle_protojet_photon.push_back(particle_event[0][i_a][i_p]);
	  protojet_parton_origin_photon.push_back(vector<int> (1, i_p));
	  protojet_flavour_photon.push_back(vector<int> (1, 22)); // photon
	  logger << LOG_DEBUG_VERBOSE << "particle_protojet_photon.size() = " << particle_protojet_photon.size() << endl;
	}
      }

      // Not fully general if photon--photon recombination is applied!

      if (!photon_photon_recombination){
	if ((n_object[k_g][i_a] < pds[j_g].n_observed_min) || (n_object[k_g][i_a] > pds[j_g].n_observed_max)){
	  cut_ps[i_a] = -1;
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_photon_isolation" << endl;}
	  logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_photon_isolation" << endl;
	  ///	  if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
	  return;
	}
	// shouldn't this be particle_event[k_g][i_a].size() instead of n_object[k_g][i_a] ???
	//      if (n_object[k_g][i_a] > 1){sort(particle_event[k_g][i_a].begin(), particle_event[k_g][i_a].end(), greaterBypT);}
	// let's try:
	//	if (particle_event[k_g][i_a].size() > 1){sort(particle_event[k_g][i_a].begin(), particle_event[k_g][i_a].end(), greaterBypT);}
	// Should be identical.
	// New implementation to solve degeneracy problem in 2->2 processes:
	if (particle_event[k_g][i_a].size() > 1){advanced_sort_by_pT(i_a, k_g);}
      }

      // added to let non-identified photons enter the jet algorithm
      if (photon_jet_algorithm){  //  Non-identified photons are added to protojets.
	for (int i_p = 0; i_p < particle_protojet_photon.size(); i_p++){
	  particle_protojet.push_back(particle_protojet_photon[i_p]);
	  protojet_parton_origin.push_back(protojet_parton_origin_photon[i_p]);
	  protojet_flavour.push_back(protojet_flavour_photon[i_p]);
	}
      }
      //
      else {
	// otherwise add to "photon" particle vector - without increasing photon number ???
      }
    }
    /*
    if (i_a == 0){
      logger << LOG_INFO << "photon_photon_recombination input (isolated photon: " << particle_event[access_object["photon"]][i_a].size() << " - rest: " << particle_protojet_photon.size() << "): " << particle_event[access_object["photon"]][i_a].size() + particle_protojet_photon.size() << endl;
    }
    */
    /*
    if (i_a == 0){
      for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){
	logger << LOG_INFO << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << csi->type_parton[i_a][i_p] << " -> " << particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << particle_event[0][i_a][i_p].pT << " eta = " << particle_event[0][i_a][i_p].eta << endl;
      }


      logger << LOG_INFO << "photon_photon_recombination input (isolated photon: " << particle_event[access_object["photon"]][i_a].size() << " - rest: " << particle_protojet_photon.size() << "):" << endl;
      //      logger << LOG_INFO << "photon.size() = " << particle_event[access_object["photon"]][i_a].size() << endl;
      for (int i_p = 0; i_p < particle_event[access_object["photon"]][i_a].size(); i_p++){
	logger << LOG_INFO << "photon[" << i_p << "] = " << particle_event[access_object["photon"]][i_a][i_p].momentum << endl;
      }
      //      logger << LOG_INFO << "particle_protojet_photon.size() = " << particle_protojet_photon.size() << endl;
      for (int i_p = 0; i_p < particle_protojet_photon.size(); i_p++){
	logger << LOG_INFO << "particle_protojet_photon[" << i_p << "] = " << particle_protojet_photon[i_p].momentum << endl;
      }
      logger.newLine(LOG_INFO);
    }
    */
  }
  else {
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   No Frixione isolation performed" << endl;}
    logger << LOG_DEBUG_VERBOSE << "No Frixione isolation is done." << endl;

    //    logger << LOG_INFO << "i_a = " << i_a << "   no_unrecombined_photon.size() = " << no_unrecombined_photon.size() << endl;

    if (photon_jet_algorithm){  //  All photons (i.e. un-recombined photons) are added to protojets if this option is chosen.
      for (int i_u = 0; i_u < no_unrecombined_photon.size(); i_u++){
	int i_p = no_unrecombined_photon[i_u];
	particle_protojet.push_back(particle_event[0][i_a][i_p]);
	protojet_parton_origin.push_back(vector<int> (1, i_p));
	protojet_flavour.push_back(vector<int> (1, csi->type_parton[i_a][i_p]));
      }
    }
  }



  //  logger << LOG_INFO << "i_a = " << i_a << "   particle_protojet.size() = " << particle_protojet.size() << endl;

  if (switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after Frixione isolation:" << endl;
    for (int i_g = 0; i_g < runtime_photon_isolation.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_photon_isolation
      int j_g = runtime_photon_isolation[i_g];  // j_g -> number in relevant_object_list
      int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_photon_isolation[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
    }
    info_cut << "Protojets after Frixione isolation:" << endl;
    for (int i_p = 0; i_p < particle_protojet.size(); i_p++){
      info_cut << "protojet[" << i_p << "] = " << particle_protojet[i_p].momentum << "   flavour = " << protojet_flavour[i_p][0] << endl;
    }
    info_cut << endl;
  }


  //////////////////////////////////////////////////
  //  elimination of protojets too close to beam  //
  //////////////////////////////////////////////////

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   parton_y_max = " << parton_y_max << "   parton_eta_max = " << parton_eta_max << endl;}
  logger << LOG_DEBUG_VERBOSE << "jet algorithm: exclude protojet candidates" << endl;

  // exclude protojets which are too close to the beam (in terms of y or eta)
  if (parton_y_max != 0. || parton_eta_max != 0.){
    for (int j = particle_protojet.size() - 1; j >= 0; j--){
      if (abs(particle_protojet[j].rapidity) > parton_y_max || abs(particle_protojet[j].eta) > parton_eta_max){
	particle_protojet.erase(particle_protojet.begin() + j);
	protojet_flavour.erase(protojet_flavour.begin() + j);
	protojet_parton_origin.erase(protojet_parton_origin.begin() + j);
      }
    }
  }

  //#include "user/specify.extra.exclusion.jet.algorithm.cxx"

  /////////////////////
  //  jet algorithm  //
  /////////////////////

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (apply jet_algorithm)):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
  if (switch_output_cutinfo){
    for (int i_p = 0; i_p < particle_protojet.size(); i_p++){
      info_cut << "[" << setw(2) << i_a << "]" << "   particle_protojet[" << i_p << "]   pT = " << particle_protojet[i_p].pT << "   ET = " << particle_protojet[i_p].ET << "   flavour = " << protojet_flavour[i_p][0] << "   origin = " << protojet_parton_origin[i_p][0] << endl;
    }
  }



  logger << LOG_DEBUG_VERBOSE << "jet algorithm: combine protojets to jets" << endl;

  particle_jet.clear();
  jet_flavour.clear();
  jet_parton_origin.clear();

  //  jet_algorithm_flavour(particle_protojet, protojet_flavour, protojet_parton_origin, particle_jet, jet_flavour, jet_parton_origin);
  jet_algorithm_flavour();

  if (switch_output_cutinfo){
    for (int i_p = 0; i_p < particle_jet.size(); i_p++){
      stringstream jet_flavour_ss;
      jet_flavour_ss << "( ";
      for (int i_q = 0; i_q < jet_flavour[i_p].size(); i_q++){
	jet_flavour_ss << jet_flavour[i_p][i_q];
	if (i_q < jet_flavour[i_p].size() - 1){jet_flavour_ss << " ";}
      }
      jet_flavour_ss << " )";

      stringstream jet_parton_origin_ss;
      jet_parton_origin_ss << "( ";
      for (int i_q = 0; i_q < jet_parton_origin[i_p].size(); i_q++){
	jet_parton_origin_ss << jet_parton_origin[i_p][i_q];
	if (i_q < jet_parton_origin[i_p].size() - 1){jet_parton_origin_ss << " ";}
      }
      jet_parton_origin_ss << " )";

      info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet[" << i_p << "]   pT = " << particle_jet[i_p].pT << "   ET = " << particle_jet[i_p].ET << "   flavour = " << jet_flavour_ss.str() << "   origin = " << jet_parton_origin_ss.str() << endl;
      //      info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet[" << i_p << "]   pT = " << particle_jet[i_p].pT << "   ET = " << particle_jet[i_p].ET << "   flavour = " << jet_flavour[i_p][0] << "   origin = " << jet_parton_origin[i_p][0] << endl;
    }
  }

  //  logger << LOG_INFO << "i_a = " << i_a << "   particle_jet.size() = " << particle_jet.size() << endl;

  //#include "user/specify.add.excluded.jet.cxx"



  ////////////////////////////////////////////////////////////
  //  Frixione isolation - removal of jets close to photon  //
  ////////////////////////////////////////////////////////////

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (frixione_isolation && frixione_jet_removal)):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

  // remove jets which are too close to an 'isolated photon' according to Frixione cone size
  // might need to be adapted for photon_recombination & frixione_isolation !!!
  if (frixione_isolation && frixione_jet_removal){ // up to date ??? equivalent_no_object[no_relevant_object[...]] constructions !!!
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_photon_isolation.size() = " << runtime_photon_isolation.size() << endl;}

    // temporarily uncomment to see effect !!!
    if (runtime_photon_isolation.size() != 0){//no_relevant_object["photon"]
      logger << LOG_DEBUG_VERBOSE << "photon-isolation caused jet removal" << endl;

      for (int i_phot = 0; i_phot < n_object[access_object["photon"]][i_a]; i_phot++){
	for (int i_jet = particle_jet.size() - 1; i_jet >= 0; i_jet--){
	  logger << LOG_DEBUG_VERBOSE << "sqrt(R2_eta(particle_event[equivalent_no_object[no_relevant_object[photon]]][i_a][i_phot], particle_jet[i_jet])) = " << sqrt(R2_eta(particle_event[access_object["photon"]][i_a][i_phot], particle_jet[i_jet])) << "   frixione_delta_0 = " << frixione_delta_0 << endl;
	  if (sqrt(R2_eta(particle_event[access_object["photon"]][i_a][i_phot], particle_jet[i_jet])) < frixione_delta_0){
	    logger << LOG_DEBUG_VERBOSE << "particle_jet[" << i_jet << "] = " << particle_jet[i_jet] << "   removed.   frixione_delta_0 = " << frixione_delta_0 << endl;
	    particle_jet.erase(particle_jet.begin() + i_jet);
	    jet_flavour.erase(jet_flavour.begin() + i_jet);
	  }
	}
      }
    }

    for (int i_p = 0; i_p < particle_jet.size(); i_p++){
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet[" << i_p << "]   pT = " << particle_jet[i_p].pT << "   ET = " << particle_jet[i_p].ET << "   flavour = " << jet_flavour[i_p][0] << "   origin = " << jet_parton_origin[i_p][0] << endl;}
    }
  }


  /////////////////////////////////////////////////
  //  jet algorithm - interpretation of outcome  //
  /////////////////////////////////////////////////

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before particle_jet_id):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

  logger << LOG_DEBUG_VERBOSE << "particle_jet_id" << endl;
  //  vector<particle_id> particle_jet_id(particle_jet.size());
  ///  static vector<particle_id> particle_jet_id;
  particle_jet_id.clear();
  particle_jet_id.resize(particle_jet.size());

  for (int i_j = 0; i_j < particle_jet.size(); i_j++){
    particle_jet_id[i_j].xparticle = &particle_jet[i_j];
    particle_jet_id[i_j].content = jet_flavour[i_j];

    // IR-safe defintion - b and bx are combined to light jet: switch needed !!!
    particle_jet_id[i_j].identity = accumulate(particle_jet_id[i_j].content.begin(), particle_jet_id[i_j].content.end(), 0);
    if (msi->M_b > 0. && particle_jet_id[i_j].identity == 0){
      // no IR-safe defintion in case of massless b quarks - b and bx are combined to bottom jet: switch needed !!!
      for (int i_q = 0; i_q < particle_jet_id[i_j].content.size(); i_q++){
	if (abs(particle_jet_id[i_j].content[i_q]) == 5){particle_jet_id[i_j].identity = 5; break;}
      }
    }

    // modification compared to IR-safe version of flavoured jet combination:
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet_id[" << i_j << "].content.size() = " << particle_jet_id[i_j].content.size() << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet_id[" << i_j << "].identity = " << particle_jet_id[i_j].identity << endl;}

    // valid only for W+jets !!! does, however, not look wrong for other processes with photon recombination !!!
    // with this (E_phot -- E_had) criterion, it could happen that non-isolated photon collect enough pT to be considered a photon,
    // which, however, at present has no impact because the (properly isolated) photons are counted right after the Frixione isolation...
    // For processes without photons in the jet algorithm, nothing should happen here !!!

    // deactivate: this could allow to treat photons as jets:
    //    if (particle_jet_id[i_j].content.size() == 1){continue;}

    if (particle_jet_id[i_j].identity == 0){} // light jet or b-bx jet
    else if (particle_jet_id[i_j].identity == 5 || // bjet_b
	     particle_jet_id[i_j].identity == -5){continue;} // bjet_bx
    else {
      double E_phot = 0.;
      double E_had = 0.;
      int count_phot = 0;
      for (int i_l = 0; i_l < particle_jet_id[i_j].content.size(); i_l++){
	if (particle_jet_id[i_j].content[i_l] == 22){
	  E_phot += (particle_event[0][i_a][jet_parton_origin[i_j][i_l]].momentum).x0();
	  count_phot++;
	}
	else {
	  E_had += (particle_event[0][i_a][jet_parton_origin[i_j][i_l]].momentum).x0();
	}
      }

      if (switch_output_cutinfo){
	info_cut << "E_phot = " << E_phot << endl;
	info_cut << "E_had = " << E_had << endl;
      }

      if (E_phot / (E_phot + E_had) >= photon_E_threshold_ratio){
	//     if (E_phot / (E_phot + E_had) > photon_E_threshold_ratio){
	// bug !!!      if (E_phot / (E_had + E_had) > photon_E_threshold_ratio){
	particle_jet_id[i_j].identity = 22;
      }
      else {
	particle_jet_id[i_j].identity = particle_jet_id[i_j].identity - count_phot * 22;
      }
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   particle_jet_id[" << i_j << "].identity = " << particle_jet_id[i_j].identity << endl;}
      logger << LOG_DEBUG_VERBOSE << "E_phot = " << E_phot << endl;
      logger << LOG_DEBUG_VERBOSE << "E_had = " << E_had << endl;
      logger << LOG_DEBUG_VERBOSE << "particle_jet_id[" << i_j << "].identity = " << particle_jet_id[i_j].identity << endl;
    }

    //#include "user/specify.jet.recombination.cxx"
  }

  sort(particle_jet_id.begin(), particle_jet_id.end(), idgreaterBypT);



  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_jet_recombination:   runtime_jet_recombination.size() = " << runtime_jet_recombination.size() << endl;}

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before jet_algorithm):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

  logger << LOG_DEBUG_VERBOSE << "runtime_jet_recombination.size() = " << runtime_jet_recombination.size() << endl;
  for (int i_g = 0; i_g < runtime_jet_recombination.size(); i_g++){
    int j_g = runtime_jet_recombination[i_g];
    int k_g = equivalent_no_object[j_g];
    logger << LOG_DEBUG_VERBOSE << "runtime_jet_recombination: i_g = " << i_g << "   j_g = " << j_g << "   k_g = " << k_g << endl;
    //    logger << LOG_DEBUG_VERBOSE << "relevant_define_y[" << j_g << "] = " << pds[j_g].define_y << endl;
    //    logger << LOG_DEBUG_VERBOSE << "relevant_define_eta[" << j_g << "] = " << pds[j_g].define_eta << endl;
    //    logger << LOG_DEBUG_VERBOSE << "relevant_define_pT[" << j_g << "] = " << pds[j_g].define_pT << endl;
    //    logger << LOG_DEBUG_VERBOSE << "relevant_define_ET[" << j_g << "] = " << pds[j_g].define_ET << endl;
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_jet_recombination[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;}

    logger << LOG_DEBUG_VERBOSE << "particle_jet_id.size() = " << particle_jet_id.size() << endl;
    for (int i_j = 0; i_j < particle_jet_id.size(); i_j++){
      logger << LOG_DEBUG_VERBOSE << "(*particle_jet_id[" << i_j << "].xparticle) = " << (*particle_jet_id[i_j].xparticle) << endl;
      /*
	logger << LOG_DEBUG_VERBOSE << "no_relevant_object[photon] = " << no_relevant_object["photon"] << endl;
	logger << LOG_DEBUG_VERBOSE << "k_g = " << k_g << endl;
	logger << LOG_DEBUG_VERBOSE << "j_g = " << j_g << endl;
	logger << LOG_DEBUG_VERBOSE << "particle_jet_id[" << i_j << "].identity = " <<particle_jet_id[i_j].identity  << endl;
      */
      // k_g (relevant_object) -> j_g (object)
      if      (j_g == no_relevant_object["jet"] && particle_jet_id[i_j].identity == 22){continue;}
      else if (j_g == no_relevant_object["photon"] && particle_jet_id[i_j].identity != 22){continue;}
      else if (j_g == no_relevant_object["ljet"] && particle_jet_id[i_j].identity != 0){continue;}
      else if (j_g == no_relevant_object["bjet"] && abs(particle_jet_id[i_j].identity) != 5){continue;}
      else if (j_g == no_relevant_object["bjet_b"] && particle_jet_id[i_j].identity != +5){continue;}
      else if (j_g == no_relevant_object["bjet_bx"] && particle_jet_id[i_j].identity != -5){continue;}

      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					      << "   y: " << setw(10) << abs((*particle_jet_id[i_j].xparticle).rapidity) << " < " << setw(8) << pds[j_g].define_y
					      << "   eta:  " << setw(10) << abs((*particle_jet_id[i_j].xparticle).eta) << " < " << setw(8) << pds[j_g].define_eta
					      << "   pT:  " << setw(10) << abs((*particle_jet_id[i_j].xparticle).pT) << " > " << setw(8) << pds[j_g].define_pT
					      << "   ET:  " << setw(10) << abs((*particle_jet_id[i_j].xparticle).ET) << " > " << setw(8) << pds[j_g].define_ET
					      << "   ---   "
					      << (abs((*particle_jet_id[i_j].xparticle).rapidity) < pds[j_g].define_y &&
						  abs((*particle_jet_id[i_j].xparticle).eta) < pds[j_g].define_eta)
					      << "   ---   "
					      << ((*particle_jet_id[i_j].xparticle).pT >= pds[j_g].define_pT &&
						  (*particle_jet_id[i_j].xparticle).ET >= pds[j_g].define_ET) << endl;}


      if (abs((*particle_jet_id[i_j].xparticle).rapidity) < pds[j_g].define_y &&
	  abs((*particle_jet_id[i_j].xparticle).eta) < pds[j_g].define_eta){
	if ((*particle_jet_id[i_j].xparticle).pT >= pds[j_g].define_pT &&
	    (*particle_jet_id[i_j].xparticle).ET >= pds[j_g].define_ET){
	  // not counted as an identified photon if Frixione isolation is used !!!
	  if (j_g == no_relevant_object["photon"] && frixione_isolation){}
	  else {
	    n_object[k_g][i_a]++;
	  }
	  logger << LOG_DEBUG_VERBOSE << "n_object[" << k_g << "][" << i_a << "] = " << n_object[k_g][i_a] << endl;
	}
	particle_event[k_g][i_a].push_back(*particle_jet_id[i_j].xparticle);
	logger << LOG_DEBUG_VERBOSE << "particle_event[" << k_g << "][" << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "pds[" << j_g << "].n_observed_min = " << pds[j_g].n_observed_min << endl;
    logger << LOG_DEBUG_VERBOSE << "pds[" << j_g << "].n_observed_max = " << pds[j_g].n_observed_max << endl;

    if (!(j_g == no_relevant_object["photon"] && frixione_isolation && photon_photon_recombination)){
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   " << pds[j_g].n_observed_min << " <= n_object[" << setw(2) <<k_g << "][" << setw(2) <<i_a << "] = " << n_object[k_g][i_a] << " <= " << pds[j_g].n_observed_max << endl;}

      if ((n_object[k_g][i_a] < pds[j_g].n_observed_min) || (n_object[k_g][i_a] > pds[j_g].n_observed_max)){
	cut_ps[i_a] = -1;
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_jet_recombination" << endl;}
	logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_jet_recombination" << endl;
	///      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
      }
    }

    // Should be required only for back-to-back configurations !!!
    // Otherwise, sorting should be done already.
    if (particle_event[k_g][i_a].size() > 1){advanced_sort_by_pT(i_a, k_g);}
   
  }

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before frixione_isolation && photon_photon_recombination):   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}

    logger << LOG_DEBUG_VERBOSE << "photon_photon_recombination = " << photon_photon_recombination << endl;

    // Only applied for two photons: (combine with photon_E_threshold_ratio = 1.!)
  static int photon_photon_counter = 0;
  if (frixione_isolation && photon_photon_recombination){
    int j_g = no_relevant_object["photon"];
    int k_g = access_object["photon"];
    int stop_photon_photon_recombination = 0;
    if (particle_event[k_g][i_a].size() < 2){stop_photon_photon_recombination = 1;}

    int change_photon_photon_recombination = 0;
    //    if (k_g != access_object["photon"]){logger << LOG_DEBUG << "k_g = " << k_g << " != " << access_object["photon"] << " = access_object[photon]" << endl;}

    while (!stop_photon_photon_recombination){
      double distance = photon_photon_recombination_R;
      int i_rec = -1;
      int j_rec = -1;
      for (int i_p = 0; i_p < particle_event[k_g][i_a].size(); i_p++){
	for (int j_p = i_p + 1; j_p < particle_event[k_g][i_a].size(); j_p++){
	  double temp_distance = 0.;
	  if (photon_R_definition == 0){temp_distance = R2_eta(particle_event[k_g][i_a][i_p], particle_event[k_g][i_a][j_p]);}
	  else if (photon_R_definition == 1){temp_distance = R2_rapidity(particle_event[k_g][i_a][i_p], particle_event[k_g][i_a][j_p]);}
	  //	  logger << LOG_INFO << "photon_photon_distance = " << temp_distance << endl;
	  if (temp_distance < distance){
	    //	    logger << LOG_INFO << "photon_photon_distance = " << temp_distance << " < " << distance << endl;
	    distance = temp_distance;
	    i_rec = i_p;
	    j_rec = j_p;
	  }
	}
      }
      if (i_rec != -1 && j_rec != -1){
	change_photon_photon_recombination = 1;
	logger << LOG_DEBUG << "photon_photon_recombination applied (" << ++photon_photon_counter << "): distance = " << distance << endl;
	logger << LOG_DEBUG << "particle_event[" << k_g << "][" << i_a << "][" << i_rec << "] = " << particle_event[k_g][i_a][i_rec].momentum << endl;
	logger << LOG_DEBUG << "particle_event[" << k_g << "][" << i_a << "][" << j_rec << "] = " << particle_event[k_g][i_a][j_rec].momentum << endl;
	particle_event[k_g][i_a][i_rec] = particle_event[k_g][i_a][i_rec] + particle_event[k_g][i_a][j_rec];
	particle_event[k_g][i_a].erase(particle_event[k_g][i_a].begin() + j_rec);
	if (j_rec < n_object[k_g][i_a]){n_object[k_g][i_a]--;}
      }
      else {
	stop_photon_photon_recombination = 1;
	// end while loop (to be added) !!!
      }
    }
    //      int old_n_photon = n_object[k_g][i_a];
    if (change_photon_photon_recombination){
      n_object[k_g][i_a] = 0;
      for (int i_p = 0; i_p < particle_event[k_g][i_a].size(); i_p++){
	if (abs(particle_event[0][i_a][i_p].rapidity) < pds[j_g].define_y &&
	    abs(particle_event[0][i_a][i_p].eta) < pds[j_g].define_eta &&
	    particle_event[0][i_a][i_p].pT >= pds[j_g].define_pT &&
	    particle_event[0][i_a][i_p].ET >= pds[j_g].define_ET){
	  n_object[k_g][i_a]++;
	}
      }
    }

    if ((n_object[k_g][i_a] < pds[j_g].n_observed_min) || (n_object[k_g][i_a] > pds[j_g].n_observed_max)){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after photon_photon_recombination" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after photon_photon_recombination" << endl;
      ///      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
  }



  if (switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_jet_recombination:" << endl;
    for (int i_g = 0; i_g < runtime_jet_recombination.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_jet_recombination
      int j_g = runtime_jet_recombination[i_g];  // j_g -> number in relevant_object_list
      int k_g = equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   object_list_selection[runtime_jet_recombination[" << i_g << "] = " << j_g << "] = " << object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   particle_event[" << setw(2) << k_g << " -> " << setw(10) << object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }

  ///


  // Old N-jettiness implementation shifted from here !!!



  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   generic event selection passed   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
  //      if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }

  /* Check if it works !!!
  logger << LOG_DEBUG << "after:  cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
  logger << LOG_DEBUG << "switch_qTcut = " << switch_qTcut << endl;
  logger << LOG_DEBUG << "output_n_qTcut = " << output_n_qTcut << endl;
  logger << LOG_DEBUG << "Before:   cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;
  */
  if (qT_basic->switch_qTcut == 0){cut_ps[i_a] = 0;}
  else if (qT_basic->switch_qTcut == 1 && qT_basic->output_n_qTcut == 1){cut_ps[i_a] = 0;}
  else if (qT_basic->switch_qTcut == 3 && qT_basic->output_n_qTcut == 1){cut_ps[i_a] = 0;}
  else if (csi->class_contribution_qTcut_implicit){cut_ps[i_a] = qT_basic->n_qTcut - 1;}
  else {
    logger << LOG_DEBUG_VERBOSE << "switch_qTcut = " << qT_basic->switch_qTcut << endl;
    logger << LOG_DEBUG_VERBOSE << "output_n_qTcut = " << qT_basic->output_n_qTcut << endl;
    logger << LOG_DEBUG_VERBOSE << "n_qTcut - 1 = " << qT_basic->n_qTcut - 1 << endl;
    // cut_ps[i_a] should remain as it is -> No need to re-evaluate !!!
    //    cut_ps[i_a] = n_qTcut - 1;  // check if '- 1' is correct !!!
  }
  logger << LOG_DEBUG_VERBOSE << "After:    cut_ps[" << i_a << "] = " << cut_ps[i_a] << endl;

  if (csi->class_contribution_NJcut_explicit){NJ_basic->event_selection_NJcut(i_a);}

  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   generic event selection passed and cut_ps[" << i_a << "] set   (cut_ps[" << i_a << "] = " << cut_ps[i_a] << ")." << endl;}
  ///  if (switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
