#include "header.hpp"

void event_set::determine_phasespace_object_partonlevel(){
  static  Logger logger("event_set::determine_phasespace_object_partonlevel");
  logger << LOG_DEBUG << "called" << endl;

  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    logger << LOG_DEBUG << "csi->type_parton[" << i_a << "].size() = " << csi->type_parton[i_a].size() << endl;
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){
      logger << LOG_DEBUG << "i_a = " << i_a << "   i_p = " << i_p << endl;
      if (csi->type_parton[i_a][i_p] == 22 || csi->type_parton[0][i_p] == 2002 || csi->type_parton[0][i_p] == -2002){ps_n_partonlevel[i_a][observed_object["photon"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 11 || abs(csi->type_parton[i_a][i_p]) == 13 || abs(csi->type_parton[i_a][i_p]) == 15){ps_n_partonlevel[i_a][observed_object["lep"]]++;}
      if (csi->type_parton[i_a][i_p] == 11 || csi->type_parton[i_a][i_p] == 13 || csi->type_parton[i_a][i_p] == 15){ps_n_partonlevel[i_a][observed_object["lm"]]++;}
      if (csi->type_parton[i_a][i_p] == -11 || csi->type_parton[i_a][i_p] == -13 || csi->type_parton[i_a][i_p] == -15){ps_n_partonlevel[i_a][observed_object["lp"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 11){ps_n_partonlevel[i_a][observed_object["e"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 13){ps_n_partonlevel[i_a][observed_object["mu"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 15){ps_n_partonlevel[i_a][observed_object["tau"]]++;}
      if (csi->type_parton[i_a][i_p] == 11){ps_n_partonlevel[i_a][observed_object["em"]]++;}
      if (csi->type_parton[i_a][i_p] == 13){ps_n_partonlevel[i_a][observed_object["mum"]]++;}
      if (csi->type_parton[i_a][i_p] == 15){ps_n_partonlevel[i_a][observed_object["taum"]]++;}
      if (csi->type_parton[i_a][i_p] == -11){ps_n_partonlevel[i_a][observed_object["ep"]]++;}
      if (csi->type_parton[i_a][i_p] == -13){ps_n_partonlevel[i_a][observed_object["mup"]]++;}
      if (csi->type_parton[i_a][i_p] == -15){ps_n_partonlevel[i_a][observed_object["taup"]]++;}
      //	if (abs(csi->type_parton[i_a][i_p]) == 12 || abs(csi->type_parton[i_a][i_p]) == 14 || abs(csi->type_parton[i_a][i_p]) == 16){ps_n_partonlevel[i_a][observed_object["nu"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 12 || abs(csi->type_parton[i_a][i_p]) == 14 || abs(csi->type_parton[i_a][i_p]) == 16){ps_n_partonlevel[i_a][observed_object["nua"]]++;}
      if (csi->type_parton[i_a][i_p] == 12 || csi->type_parton[i_a][i_p] == 14 || csi->type_parton[i_a][i_p] == 16){ps_n_partonlevel[i_a][observed_object["nu"]]++;}
      if (csi->type_parton[i_a][i_p] == -12 || csi->type_parton[i_a][i_p] == -14 || csi->type_parton[i_a][i_p] == -16){ps_n_partonlevel[i_a][observed_object["nux"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 12){ps_n_partonlevel[i_a][observed_object["nea"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 14){ps_n_partonlevel[i_a][observed_object["nma"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 16){ps_n_partonlevel[i_a][observed_object["nta"]]++;}
      if (csi->type_parton[i_a][i_p] == 12){ps_n_partonlevel[i_a][observed_object["ne"]]++;}
      if (csi->type_parton[i_a][i_p] == 14){ps_n_partonlevel[i_a][observed_object["nm"]]++;}
      if (csi->type_parton[i_a][i_p] == 16){ps_n_partonlevel[i_a][observed_object["nt"]]++;}
      if (csi->type_parton[i_a][i_p] == -12){ps_n_partonlevel[i_a][observed_object["nex"]]++;}
      if (csi->type_parton[i_a][i_p] == -14){ps_n_partonlevel[i_a][observed_object["nmx"]]++;}
      if (csi->type_parton[i_a][i_p] == -16){ps_n_partonlevel[i_a][observed_object["ntx"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 12 || abs(csi->type_parton[i_a][i_p]) == 14 || abs(csi->type_parton[i_a][i_p]) == 16){ps_n_partonlevel[i_a][observed_object["missing"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 12 || abs(csi->type_parton[i_a][i_p]) == 14 || abs(csi->type_parton[i_a][i_p]) == 16){n_parton_nu++;}
      if (abs(csi->type_parton[i_a][i_p]) < 5){ps_n_partonlevel[i_a][observed_object["ljet"]]++;}
      if (csi->type_parton[i_a][i_p] == 5){ps_n_partonlevel[i_a][observed_object["bjet_b"]]++;}
      if (csi->type_parton[i_a][i_p] == -5){ps_n_partonlevel[i_a][observed_object["bjet_bx"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 5){ps_n_partonlevel[i_a][observed_object["bjet"]]++;}
      if (csi->type_parton[i_a][i_p] == 6){ps_n_partonlevel[i_a][observed_object["top"]]++;}
      if (csi->type_parton[i_a][i_p] == -6){ps_n_partonlevel[i_a][observed_object["atop"]]++;}
      if (abs(csi->type_parton[i_a][i_p]) == 6){ps_n_partonlevel[i_a][observed_object["tjet"]]++;}
      /*
      if (abs(csi->type_parton[i_a][i_p]) < 7){
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == csi->type_parton[i_a][i_p]){flag++; break;}
	}
	if (flag){
	  ps_n_partonlevel[i_a][observed_object["jet"]]++;
	}
      }
      */
      if (csi->type_parton[i_a][i_p] == 24){ps_n_partonlevel[i_a][observed_object["wm"]]++;}
      if (csi->type_parton[i_a][i_p] == -24){ps_n_partonlevel[i_a][observed_object["wp"]]++;}
      if (csi->type_parton[i_a][i_p] == 23){ps_n_partonlevel[i_a][observed_object["z"]]++;}
      if (csi->type_parton[i_a][i_p] == 25){ps_n_partonlevel[i_a][observed_object["h"]]++;}
    }

    // all potential jets are collected in ps_n_partonlevel[i_a]:
    /*
    logger << LOG_INFO << "ps_runtime_jet_algorithm.size() = " << ps_runtime_jet_algorithm.size() << endl;
    logger << LOG_INFO << "ps_runtime_jet_algorithm[" << i_a << "].size() = " << ps_runtime_jet_algorithm[i_a].size() << endl;
    logger << LOG_INFO << "observed_object[jet]" << observed_object["jet"] << endl;
    logger << LOG_INFO << "ps_n_partonlevel[" << i_a << "].size() = " << ps_n_partonlevel[i_a].size() << endl;
    */

    ps_n_partonlevel[i_a][observed_object["jet"]] = ps_runtime_jet_algorithm[i_a].size();
    if (photon_jet_algorithm){ps_n_partonlevel[i_a][observed_object["jet"]] += ps_runtime_photon[i_a].size();}
  }

  ps_runtime_order_inverse.resize(csi->n_ps, vector<vector<int> > (object_list.size()));
  for (int i_a = 0; i_a < csi->n_ps; i_a++){
    logger << LOG_DEBUG << "csi->type_parton[" << i_a << "].size() = " << csi->type_parton[i_a].size() << endl;
    for (int i_p = 3; i_p < csi->type_parton[i_a].size(); i_p++){

      if (csi->type_parton[i_a][i_p] == 0){
	ps_runtime_order_inverse[i_a][observed_object["jet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ljet"]].push_back(i_p);
      }
      if (abs(csi->type_parton[i_a][i_p]) > 0 && abs(csi->type_parton[i_a][i_p]) < 5){
	ps_runtime_order_inverse[i_a][observed_object["jet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ljet"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == 5){
	ps_runtime_order_inverse[i_a][observed_object["jet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ljet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["bjet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["bjet_b"]].push_back(i_p);
	// ljet...
      }
      if (csi->type_parton[i_a][i_p] == -5){
	ps_runtime_order_inverse[i_a][observed_object["jet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ljet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["bjet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["bjet_bx"]].push_back(i_p);
	// ljet...
      }
      if (csi->type_parton[i_a][i_p] == 6){
	ps_runtime_order_inverse[i_a][observed_object["tjet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["top"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -6){
	ps_runtime_order_inverse[i_a][observed_object["tjet"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["atop"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 11){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lm"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["e"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["em"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -11){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lp"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["e"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ep"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 13){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lm"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["mu"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["mum"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -13){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lp"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["mu"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["mup"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 15){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lm"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["tau"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["taum"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -15){
	ps_runtime_order_inverse[i_a][observed_object["lep"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["lp"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["tau"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["taup"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 12){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nu"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nea"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ne"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -12){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nux"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nea"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nex"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 14){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nu"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nma"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nm"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -14){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nux"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nma"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nmx"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 16){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nu"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nta"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nt"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -16){
	ps_runtime_order_inverse[i_a][observed_object["nua"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nux"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["nta"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["ntx"]].push_back(i_p);
	ps_runtime_order_inverse[i_a][observed_object["missing"]].push_back(i_p);
      }

      if (csi->type_parton[i_a][i_p] == 22 || csi->type_parton[0][i_p] == 2002 || csi->type_parton[0][i_p] == -2002){
	ps_runtime_order_inverse[i_a][observed_object["photon"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == 23){
	ps_runtime_order_inverse[i_a][observed_object["z"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == 24){
	ps_runtime_order_inverse[i_a][observed_object["wm"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == -24){
	ps_runtime_order_inverse[i_a][observed_object["wp"]].push_back(i_p);
      }
      if (csi->type_parton[i_a][i_p] == 25){
	ps_runtime_order_inverse[i_a][observed_object["h"]].push_back(i_p);
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void event_set::determine_equivalent_object(){
  static  Logger logger("event_set::determine_equivalent_object");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  map<string, int> protect_observable_object;

  for (int i = 1; i < object_list.size(); i++){
    if (object_list[i] == "ljet" && pda[observed_object["jet"]].n_partonlevel != 0){equivalent_object["ljet"] = "ljet";}
    else if (pda[observed_object[object_list[i]]].n_partonlevel == 0 && object_category[i] != -1){equivalent_object[object_list[i]] = "none";}
    else {equivalent_object[object_list[i]] = object_list[i];}
  }

  // check if "tjet" is identical to "top" or "atop" etc.
  if (pda[observed_object["tjet"]].n_partonlevel == pda[observed_object["atop"]].n_partonlevel &&
      pda[observed_object["atop"]].n_partonlevel > 0 && pda[observed_object["top"]].n_partonlevel == 0){equivalent_object["atop"] = equivalent_object["tjet"];}
  else if (pda[observed_object["tjet"]].n_partonlevel == pda[observed_object["top"]].n_partonlevel &&
	   pda[observed_object["top"]].n_partonlevel > 0 && pda[observed_object["atop"]].n_partonlevel == 0){equivalent_object["top"] = equivalent_object["tjet"];}

  /*
  // too involved - better never identify them !!!
  // only if less than one jet - otherwise jet algorithm might change the flavour...
  if (pda[observed_object["jet"]].n_partonlevel < 2){
    // check if "jet" is identical to "ljet" or "bjet" etc.
    if (pda[observed_object["jet"]].n_partonlevel == pda[observed_object["bjet"]].n_partonlevel &&
	pda[observed_object["bjet"]].n_partonlevel > 0 && pda[observed_object["ljet"]].n_partonlevel == 0){equivalent_object["bjet"] = equivalent_object["jet"];}
  }

  if (pda[observed_object["jet"]].n_partonlevel == pda[observed_object["ljet"]].n_partonlevel &&
      pda[observed_object["ljet"]].n_partonlevel > 0 && pda[observed_object["bjet"]].n_partonlevel == 0){equivalent_object["ljet"] = equivalent_object["jet"];}
  */

  // check if "bjet" is identical to "bjet_b" or "bjet_bx" etc.
  if (pda[observed_object["bjet"]].n_partonlevel == pda[observed_object["bjet_bx"]].n_partonlevel &&
      pda[observed_object["bjet_bx"]].n_partonlevel > 0 && pda[observed_object["bjet_b"]].n_partonlevel == 0){equivalent_object["bjet_bx"] = equivalent_object["bjet"];}
  else if (pda[observed_object["bjet"]].n_partonlevel == pda[observed_object["bjet_b"]].n_partonlevel &&
	   pda[observed_object["bjet_b"]].n_partonlevel > 0 && pda[observed_object["bjet_bx"]].n_partonlevel == 0){equivalent_object["bjet_b"] = equivalent_object["bjet"];}


  // check if "lep" is identical to "lm" or "lp" etc.
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lp"]].n_partonlevel &&
      pda[observed_object["lp"]].n_partonlevel > 0 && pda[observed_object["lm"]].n_partonlevel == 0){equivalent_object["lp"] = equivalent_object["lep"];}
  else if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["lm"]].n_partonlevel &&
	   pda[observed_object["lm"]].n_partonlevel > 0 && pda[observed_object["lp"]].n_partonlevel == 0){equivalent_object["lm"] = equivalent_object["lep"];}

  // check if "lep" is identical to "e" or "mu" or "tau" etc.
  if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["e"]].n_partonlevel &&
      pda[observed_object["e"]].n_partonlevel > 0 &&
      pda[observed_object["mu"]].n_partonlevel == 0 &&
      pda[observed_object["tau"]].n_partonlevel == 0){equivalent_object["e"] = equivalent_object["lep"];}
  else if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["mu"]].n_partonlevel &&
	   pda[observed_object["mu"]].n_partonlevel > 0 &&
	   pda[observed_object["e"]].n_partonlevel == 0 &&
	   pda[observed_object["tau"]].n_partonlevel == 0){equivalent_object["mu"] = equivalent_object["lep"];}
  else if (pda[observed_object["lep"]].n_partonlevel == pda[observed_object["tau"]].n_partonlevel &&
	   pda[observed_object["e"]].n_partonlevel > 0 &&
	   pda[observed_object["mu"]].n_partonlevel == 0 &&
	   pda[observed_object["tau"]].n_partonlevel == 0){equivalent_object["tau"] = equivalent_object["lep"];}

  // check if "e" is identical to "ep" or "em" etc.
  if (pda[observed_object["e"]].n_partonlevel == pda[observed_object["em"]].n_partonlevel &&
      pda[observed_object["em"]].n_partonlevel > 0 && pda[observed_object["ep"]].n_partonlevel == 0){equivalent_object["em"] = equivalent_object["e"];}
  else if (pda[observed_object["e"]].n_partonlevel == pda[observed_object["ep"]].n_partonlevel &&
	   pda[observed_object["ep"]].n_partonlevel > 0 && pda[observed_object["em"]].n_partonlevel == 0){equivalent_object["ep"] = equivalent_object["e"];}

  // check if "mu" is identical to "mup" or "mum" etc.
  if (pda[observed_object["mu"]].n_partonlevel == pda[observed_object["mum"]].n_partonlevel &&
      pda[observed_object["mum"]].n_partonlevel > 0 && pda[observed_object["mup"]].n_partonlevel == 0){equivalent_object["mum"] = equivalent_object["mu"];}
  else if (pda[observed_object["mu"]].n_partonlevel == pda[observed_object["mup"]].n_partonlevel &&
	   pda[observed_object["mup"]].n_partonlevel > 0 && pda[observed_object["mum"]].n_partonlevel == 0){equivalent_object["mup"] = equivalent_object["mu"];}

  // check if "tau" is identical to "taup" or "taum" etc.
  if (pda[observed_object["tau"]].n_partonlevel == pda[observed_object["taum"]].n_partonlevel &&
      pda[observed_object["taum"]].n_partonlevel > 0 && pda[observed_object["taup"]].n_partonlevel == 0){equivalent_object["taum"] = equivalent_object["tau"];}
  else if (pda[observed_object["tau"]].n_partonlevel == pda[observed_object["taup"]].n_partonlevel &&
	   pda[observed_object["taup"]].n_partonlevel > 0 && pda[observed_object["taum"]].n_partonlevel == 0){equivalent_object["taup"] = equivalent_object["tau"];}

  // check if "lm" is identical to "em" or "mum" or "taum" etc.
  if (pda[observed_object["lm"]].n_partonlevel == pda[observed_object["em"]].n_partonlevel &&
      pda[observed_object["em"]].n_partonlevel > 0 &&
      pda[observed_object["mum"]].n_partonlevel == 0 &&
      pda[observed_object["taum"]].n_partonlevel == 0){equivalent_object["lm"] = equivalent_object["em"];}
  else if (pda[observed_object["lm"]].n_partonlevel == pda[observed_object["mum"]].n_partonlevel &&
	   pda[observed_object["mum"]].n_partonlevel > 0 &&
	   pda[observed_object["em"]].n_partonlevel == 0 &&
	   pda[observed_object["taum"]].n_partonlevel == 0){equivalent_object["lm"] = equivalent_object["mum"];}
  else if (pda[observed_object["lm"]].n_partonlevel == pda[observed_object["taum"]].n_partonlevel &&
	   pda[observed_object["em"]].n_partonlevel > 0 &&
	   pda[observed_object["mum"]].n_partonlevel == 0 &&
	   pda[observed_object["taum"]].n_partonlevel == 0){equivalent_object["lm"] = equivalent_object["taum"];}

  // check if "lp" is identical to "ep" or "mup" or "taup" etc.
  if (pda[observed_object["lp"]].n_partonlevel == pda[observed_object["ep"]].n_partonlevel &&
      pda[observed_object["ep"]].n_partonlevel > 0 &&
      pda[observed_object["mup"]].n_partonlevel == 0 &&
      pda[observed_object["taup"]].n_partonlevel == 0){equivalent_object["lp"] = equivalent_object["ep"];}
  else if (pda[observed_object["lp"]].n_partonlevel == pda[observed_object["mup"]].n_partonlevel &&
	   pda[observed_object["mup"]].n_partonlevel > 0 &&
	   pda[observed_object["ep"]].n_partonlevel == 0 &&
	   pda[observed_object["taup"]].n_partonlevel == 0){equivalent_object["lp"] = equivalent_object["mup"];}
  else if (pda[observed_object["lp"]].n_partonlevel == pda[observed_object["taup"]].n_partonlevel &&
	   pda[observed_object["ep"]].n_partonlevel > 0 &&
	   pda[observed_object["mup"]].n_partonlevel == 0 &&
	   pda[observed_object["taup"]].n_partonlevel == 0){equivalent_object["lp"] = equivalent_object["taup"];}


  // check if "nea" is identical to "nex" or "ne" etc.
  if (pda[observed_object["nea"]].n_partonlevel == pda[observed_object["ne"]].n_partonlevel &&
      pda[observed_object["ne"]].n_partonlevel > 0 && pda[observed_object["nex"]].n_partonlevel == 0){equivalent_object["nea"] = equivalent_object["ne"];}
  else if (pda[observed_object["nea"]].n_partonlevel == pda[observed_object["nex"]].n_partonlevel &&
	   pda[observed_object["nex"]].n_partonlevel > 0 && pda[observed_object["ne"]].n_partonlevel == 0){equivalent_object["nea"] = equivalent_object["nex"];}
  if (pda[observed_object["nma"]].n_partonlevel == pda[observed_object["nm"]].n_partonlevel &&
      pda[observed_object["nm"]].n_partonlevel > 0 && pda[observed_object["nmx"]].n_partonlevel == 0){equivalent_object["nma"] = equivalent_object["nm"];}
  else if (pda[observed_object["nma"]].n_partonlevel == pda[observed_object["nmx"]].n_partonlevel &&
	   pda[observed_object["nmx"]].n_partonlevel > 0 && pda[observed_object["nm"]].n_partonlevel == 0){equivalent_object["nma"] = equivalent_object["nmx"];}
  if (pda[observed_object["nta"]].n_partonlevel == pda[observed_object["nt"]].n_partonlevel &&
      pda[observed_object["nt"]].n_partonlevel > 0 && pda[observed_object["ntx"]].n_partonlevel == 0){equivalent_object["nta"] = equivalent_object["nt"];}
  else if (pda[observed_object["nta"]].n_partonlevel == pda[observed_object["ntx"]].n_partonlevel &&
	   pda[observed_object["ntx"]].n_partonlevel > 0 && pda[observed_object["nt"]].n_partonlevel == 0){equivalent_object["nta"] = equivalent_object["ntx"];}

  if (pda[observed_object["nu"]].n_partonlevel == pda[observed_object["ne"]].n_partonlevel &&
      pda[observed_object["ne"]].n_partonlevel > 0 && pda[observed_object["nm"]].n_partonlevel == 0 &&
      pda[observed_object["nt"]].n_partonlevel == 0){equivalent_object["nu"] = equivalent_object["ne"];}
  else if (pda[observed_object["nu"]].n_partonlevel == pda[observed_object["nm"]].n_partonlevel &&
	   pda[observed_object["nm"]].n_partonlevel > 0 && pda[observed_object["ne"]].n_partonlevel == 0 &&
	   pda[observed_object["nt"]].n_partonlevel == 0){equivalent_object["nu"] = equivalent_object["nm"];}
  else if (pda[observed_object["nu"]].n_partonlevel == pda[observed_object["nt"]].n_partonlevel &&
	   pda[observed_object["ne"]].n_partonlevel > 0 && pda[observed_object["nm"]].n_partonlevel == 0 &&
	   pda[observed_object["nt"]].n_partonlevel == 0){equivalent_object["nu"] = equivalent_object["nt"];}

  if (pda[observed_object["nux"]].n_partonlevel == pda[observed_object["nex"]].n_partonlevel &&
      pda[observed_object["nex"]].n_partonlevel > 0 &&
      pda[observed_object["nmx"]].n_partonlevel == 0 &&
      pda[observed_object["ntx"]].n_partonlevel == 0){equivalent_object["nux"] = equivalent_object["nex"];}
  else if (pda[observed_object["nux"]].n_partonlevel == pda[observed_object["nmx"]].n_partonlevel &&
	   pda[observed_object["nmx"]].n_partonlevel > 0 &&
	   pda[observed_object["nex"]].n_partonlevel == 0 &&
	   pda[observed_object["ntx"]].n_partonlevel == 0){equivalent_object["nux"] = equivalent_object["nmx"];}
  else if (pda[observed_object["nux"]].n_partonlevel == pda[observed_object["ntx"]].n_partonlevel &&
	   pda[observed_object["nex"]].n_partonlevel > 0 &&
	   pda[observed_object["nmx"]].n_partonlevel == 0 &&
	   pda[observed_object["ntx"]].n_partonlevel == 0){equivalent_object["nux"] = equivalent_object["ntx"];}

  if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nea"]].n_partonlevel &&
      pda[observed_object["nea"]].n_partonlevel > 0 &&
      pda[observed_object["nma"]].n_partonlevel == 0 &&
      pda[observed_object["nta"]].n_partonlevel == 0){equivalent_object["nua"] = equivalent_object["nea"];}
  else if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nma"]].n_partonlevel &&
	   pda[observed_object["nma"]].n_partonlevel > 0 &&
	   pda[observed_object["nea"]].n_partonlevel == 0 &&
	   pda[observed_object["nta"]].n_partonlevel == 0){equivalent_object["nua"] = equivalent_object["nma"];}
  else if (pda[observed_object["nua"]].n_partonlevel == pda[observed_object["nta"]].n_partonlevel &&
	   pda[observed_object["nea"]].n_partonlevel > 0 &&
	   pda[observed_object["nma"]].n_partonlevel == 0 &&
	   pda[observed_object["nta"]].n_partonlevel == 0){equivalent_object["nua"] = equivalent_object["nta"];}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::determine_relevant_object(){
  static  Logger logger("event_set::determine_relevant_object");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "pds.size() = " << pds.size() << endl;

  object_list_selection.push_back("all");
  pds.push_back(define_particle_set ());

  object_list_selection.push_back("none");
  pds.push_back(define_particle_set ());

  logger << LOG_DEBUG << setw(5) << "no" << setw(10) << "object" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|"  << setw(10) << "|y|"<< endl;
  for (int i = 1; i < object_list.size(); i++){
    if (equivalent_object[object_list[i]] != "none"){
      //      logger << LOG_INFO << "object_list[" << setw(3) << i << "] = " << setw(10) << object_list[i] << "   ->   equivalent_object[" << setw(3) << setw(10) << object_list[i] << "] = " << setw(10) << equivalent_object[object_list[i]] << endl;
    }
    else {
      logger << LOG_DEBUG_VERBOSE << "object_list[" << setw(3) << i << "] = " << setw(10) << object_list[i] << "   ->   equivalent_object[" << setw(3) << setw(10) << object_list[i] << "] = " << setw(10) << equivalent_object[object_list[i]] << endl;
    }

    int flag = 0;
    if (equivalent_object[object_list[i]] != ""){
      for (int j = 0; j < object_list_selection.size(); j++){
	if (equivalent_object[object_list[i]] == object_list_selection[j]){flag = 1; break;}
      }
      if (flag == 0){
	logger << LOG_DEBUG_VERBOSE << "object_list_selection.size() = " << object_list_selection.size() << endl;

	int x_i = 0;
	for (int i_i = 0; i_i < object_list.size(); i_i++){
	  if (object_list[i_i] == equivalent_object[object_list[i]]){
	    x_i = i_i; break;
	  }
	}

	logger << LOG_DEBUG << setw(5) << i << setw(10) << object_list[i] << setw(10) << pda[i].n_observed_min << setw(10) << pda[i].n_observed_max << setw(10) << pda[i].define_pT << setw(10) << pda[i].define_ET << setw(10) << pda[i].define_eta << setw(10) << pda[i].define_y << endl;

	object_list_selection.push_back(object_list[x_i]);
	pds.push_back(pda[x_i]);
	object_category_selection.push_back(x_i);  //  ???
      }
    }
  }

  // add objects without particle equivalent (in order to exclude contributions that do not fulfill the corresponding requirements)
  // Could be avoided by checking for full object_list to select vanishing contributions !!!
  for (int x_i = 1; x_i < object_list.size(); x_i++){
    if ((pda[x_i].n_observed_min > pda[x_i].n_partonlevel) && equivalent_object[object_list[x_i]] == "none"){
      logger << LOG_INFO << "    object_list[" << setw(3) << x_i << "] = " << setw(10) << object_list[x_i] << "   ->   equivalent_object[" << setw(3) << setw(10) << object_list[x_i] << "] = " << setw(10) << equivalent_object[object_list[x_i]] << "   n_observed_min[" << setw(3) << x_i << "] = " << pda[x_i].n_observed_max  << "   n_observed_min[" << setw(3) << x_i << "] = " << pda[x_i].n_observed_max << endl;
      logger << LOG_INFO << "No contribution can result from this subprocess - modification needed due to type conversion (e.g. jet/photon recombination alogorithm)" << endl;
      //      exit(1);

      object_list_selection.push_back(object_list[x_i]);
      pds.push_back(pda[x_i]);
      object_category_selection.push_back(x_i);  //  ???
    }
  }

  logger.newLine(LOG_INFO);

  for (int i = 0; i < object_list_selection.size(); i++){no_relevant_object[object_list_selection[i]] = i;}

  //  equivalent_no_object.resize(object_list.size());
  for (int i_o = 0; i_o < object_list.size(); i_o++){
    for (int j_o = 0; j_o < object_list_selection.size(); j_o++){
      if (object_list[i_o] == object_list_selection[j_o]){
	equivalent_no_object[j_o] = i_o;
      }
    }
  }

  logger.newLine(LOG_DEBUG);
  logger << LOG_DEBUG << "object_list_selection.size() = " << object_list_selection.size() << endl;
  logger.newLine(LOG_DEBUG);

  logger << LOG_DEBUG << "Relevant objects:" << endl;
  logger << LOG_DEBUG << setw(5) << "no" << setw(10) << "object" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|"  << setw(10) << "|y|"<< endl;
  for (int i = 0; i < object_list_selection.size(); i++){
    logger << LOG_DEBUG << setw(5) << i << setw(10) << object_list_selection[i] << setw(10) << pds[i].n_observed_min << setw(10) << pds[i].n_observed_max << setw(10) << pds[i].define_pT << setw(10) << pds[i].define_ET << setw(10) << pds[i].define_eta << setw(10) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_DEBUG);

  /*
  // maybe later...
  for (int i = 0; i < object_list.size(); i++){
    if (no_relevant_object[object_list[i]] == 0){
      no_relevant_object[object_list[i]] = no_relevant_object[equivalent_object[object_list[i]]];
    }
  }
  */
  //  for (int i = 0; i < relevant_object_list.size(); i++){no_relevant_object[equivalent_object[relevant_object_list[i]]] = i;}

  for (int i_o = 0; i_o < object_list_selection.size(); i_o++){
    observed_object_selection[object_list_selection[i_o]] = i_o;
  }

  for (int i_o = 0; i_o < object_list_selection.size(); i_o++){
    logger << LOG_INFO << "object_list_selection[" << i_o << "] = " << object_list_selection[i_o] << "   ---   " << observed_object_selection[object_list_selection[i_o]] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::determine_runtime_object(){
  static  Logger logger("event_set::determine_runtime_object");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // relevance for jet algorithm:
  // 1 = only (one type of) jets
  // 2, 3, ... = more than one type of jets
  // -1 = only (one type of) jets and photons
  // -2, -3, ... =  more than one type of jets and photons

  //  vector<int> temp_original(relevant_object_list.size(), 0);
  vector<int> temp_original(object_list_selection.size(), 0);

  //  if (jet_algorithm || frixione_isolation){
  {
    int temp_sign = 1;
    for (int i = 1; i < object_list_selection.size(); i++){
      if (object_list_selection[i] == "tjet" ||
	  object_list_selection[i] == "top"){
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == 6){flag++; break;}
	}
	if (flag){
	  runtime_jet_recombination.push_back(i);
	  temp_original[i]++;
	  runtime_jet_algorithm++;
	}
      }
      else if (object_list_selection[i] == "tjet" ||
	       object_list_selection[i] == "atop"){
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == -6){flag++; break;}
	}
	if (flag){
	  runtime_jet_recombination.push_back(i);
	  temp_original[i]++;
	  runtime_jet_algorithm++;
	}
      }
      else if (object_list_selection[i] == "bjet" ||
	  object_list_selection[i] == "bjet_b"){
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == 5){flag++; break;}
	}
	if (flag){
	  runtime_jet_recombination.push_back(i);
	  temp_original[i]++;
	  runtime_jet_algorithm++;
	}
      }
      else if (object_list_selection[i] == "bjet" ||
	       object_list_selection[i] == "bjet_bx"){
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == -5){flag++; break;}
	}
	if (flag){
	  runtime_jet_recombination.push_back(i);
	  temp_original[i]++;
	  runtime_jet_algorithm++;
	}
      }
      else if (object_list_selection[i] == "jet" ||
	       object_list_selection[i] == "ljet" ||
	       (object_list_selection[i] == "photon" && !frixione_isolation && (jet_algorithm || photon_jet_algorithm))){
	//	       (object_list_selection[i] == "photon" && frixione_isolation == 0)){
	runtime_jet_recombination.push_back(i);
	temp_original[i]++;
	runtime_jet_algorithm++;
      }
      if (object_list_selection[i] == "photon"){temp_sign = -1;}
    }
    runtime_jet_algorithm = temp_sign * runtime_jet_algorithm;
  }

  if (photon_recombination != 0){
    if (pda[observed_object["photon"]].n_partonlevel != 0){
      for (int i = 1; i < object_list_selection.size(); i++){
	if (object_list_selection[i] == "jet" ||
	    object_list_selection[i] == "ljet" ||
	    object_list_selection[i] == "bjet" ||
	    object_list_selection[i] == "bjet_b" ||
	    object_list_selection[i] == "bjet_bx" ||
	    object_list_selection[i] == "lep" ||
	    object_list_selection[i] == "lm" ||
	    object_list_selection[i] == "lp" ||
	    object_list_selection[i] == "e" ||
	    object_list_selection[i] == "mu" ||
	    object_list_selection[i] == "tau" ||
	    object_list_selection[i] == "em" ||
	    object_list_selection[i] == "mum" ||
	    object_list_selection[i] == "taum" ||
	    object_list_selection[i] == "ep" ||
	    object_list_selection[i] == "mup" ||
	    object_list_selection[i] == "taup" ||
	    object_list_selection[i] == "photon"){
	  runtime_photon_recombination.push_back(i);

	  // leptons should be written to "particles" after the photon-recombination phase
	  if (object_list_selection[i] == "lep" ||
	      object_list_selection[i] == "lm" ||
	      object_list_selection[i] == "lp" ||
	      object_list_selection[i] == "e" ||
	      object_list_selection[i] == "mu" ||
	      object_list_selection[i] == "tau" ||
	      object_list_selection[i] == "em" ||
	      object_list_selection[i] == "mum" ||
	      object_list_selection[i] == "taum" ||
	      object_list_selection[i] == "ep" ||
	      object_list_selection[i] == "mup" ||
	      object_list_selection[i] == "taup"){
	    // Handled differently at the moment...
	  }
	  // temp_original statement: why should all particles but leptons be affected by this ???
	  else {temp_original[i]++;}
	  //      runtime_photon_algorithm++;
	}
	//    if (object_list_selection[i] == "photon"){temp_sign = -1;}
      }
    }
  }
  //  runtime_jet_algorithm = temp_sign * runtime_jet_algorithm;

  if (frixione_isolation != 0){
    for (int i = 1; i < object_list_selection.size(); i++){
      if (object_list_selection[i] == "photon"){
	runtime_photon_isolation.push_back(i);
	temp_original[i]++;
	//      runtime_photon_algorithm++;
      }
      // new: partons could be changed by photon isolation (e.g. removed from list)
	if (object_list_selection[i] == "jet" ||
	    object_list_selection[i] == "ljet" ||
	    object_list_selection[i] == "bjet" ||
	    object_list_selection[i] == "bjet_b" ||
	    object_list_selection[i] == "bjet_bx"){temp_original[i]++;}

      //    if (object_list_selection[i] == "photon"){temp_sign = -1;}
    }
  }

  for (int i = 1; i < object_list_selection.size(); i++){
    if (object_list_selection[i] == "missing"){
      //      runtime_missing.push_back(i);
      temp_original[i]++;
      //      runtime_photon_algorithm++;
    }
    //    if (object_list_selection[i] == "photon"){temp_sign = -1;}
  }


  vector<vector<fourvector> > momentum_object;
  momentum_object.resize(object_list_selection.size());

  // lepton--photon recombination ignored at the moment -> all other partons enter event selection with their original momenta
  for (int i = 1; i < object_list_selection.size(); i++){
    //    cout << "temp_original[" << setw(3) << i << "] = " << temp_original[i] << endl;
    if (temp_original[i] == 0 && object_category[equivalent_no_object[i]] != -1){
      runtime_original.push_back(i);
    }
  }

  runtime_order.resize(object_list_selection.size());
  runtime_order_inverse.resize(csi->type_parton[0].size());
  for (int i = 1; i < object_list_selection.size(); i++){
    for (int i_p = 3; i_p < csi->type_parton[0].size(); i_p++){
      if (object_list_selection[i] == "jet"){
	//      if (object_list_selection[i] == "jet" && abs(csi->type_parton[0][i_p]) < 6){
	int i_a = 0;  //  temporary ???
	int flag = 0;
	for (int i_j = 0; i_j < jet_algorithm_list.size(); i_j++){
	  if (jet_algorithm_list[i_j] == csi->type_parton[i_a][i_p]){flag++; break;}
	}
	if (flag){
	  runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);
	}
      }
      if (object_list_selection[i] == "ljet" && abs(csi->type_parton[0][i_p]) < 5){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "bjet" && abs(csi->type_parton[0][i_p]) == 5){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "bjet_b" && csi->type_parton[0][i_p] == 5){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "bjet_bx" && csi->type_parton[0][i_p] == -5){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "lep" && (abs(csi->type_parton[0][i_p]) == 11 ||
					       abs(csi->type_parton[0][i_p]) == 13 ||
					       abs(csi->type_parton[0][i_p]) == 15)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "lm" && (csi->type_parton[0][i_p] == 11 ||
						 csi->type_parton[0][i_p] == 13 ||
						 csi->type_parton[0][i_p] == 15)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "lp" && (csi->type_parton[0][i_p] == -11 ||
						 csi->type_parton[0][i_p] == -13 ||
						 csi->type_parton[0][i_p] == -15)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "e" && abs(csi->type_parton[0][i_p]) == 11){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "mu" && abs(csi->type_parton[0][i_p]) == 13){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "tau" && abs(csi->type_parton[0][i_p]) == 15){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "em" && csi->type_parton[0][i_p] == 11){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "mum" && csi->type_parton[0][i_p] == 13){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "taum" && csi->type_parton[0][i_p] == 15){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "ep" && csi->type_parton[0][i_p] == -11){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "mup" && csi->type_parton[0][i_p] == -13){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "taup" && csi->type_parton[0][i_p] == -15){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}

      if (object_list_selection[i] == "nua" && (abs(csi->type_parton[0][i_p]) == 12 ||
					       abs(csi->type_parton[0][i_p]) == 14 ||
					       abs(csi->type_parton[0][i_p]) == 16)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nu" && (csi->type_parton[0][i_p] == 12 ||
						 csi->type_parton[0][i_p] == 14 ||
						 csi->type_parton[0][i_p] == 16)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nux" && (csi->type_parton[0][i_p] == -12 ||
						 csi->type_parton[0][i_p] == -14 ||
						 csi->type_parton[0][i_p] == -16)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nea" && abs(csi->type_parton[0][i_p]) == 12){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nma" && abs(csi->type_parton[0][i_p]) == 14){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nta" && abs(csi->type_parton[0][i_p]) == 16){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "ne" && csi->type_parton[0][i_p] == 12){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nm" && csi->type_parton[0][i_p] == 14){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nt" && csi->type_parton[0][i_p] == 16){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nex" && csi->type_parton[0][i_p] == -12){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "nmx" && csi->type_parton[0][i_p] == -14){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "ntx" && csi->type_parton[0][i_p] == -16){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "photon" && (csi->type_parton[0][i_p] == 22 || csi->type_parton[0][i_p] == 2002 || csi->type_parton[0][i_p] == -2002)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "tjet" && abs(csi->type_parton[0][i_p]) == 6){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "top" && csi->type_parton[0][i_p] == 6){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "atop" && csi->type_parton[0][i_p] == -6){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "wm" && csi->type_parton[0][i_p] == 24){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "wp" && csi->type_parton[0][i_p] == -24){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "z" && csi->type_parton[0][i_p] == 23){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
      if (object_list_selection[i] == "h" && csi->type_parton[0][i_p] == 25){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}

      if (object_list_selection[i] == "missing" && (abs(csi->type_parton[0][i_p]) == 12 ||
						   abs(csi->type_parton[0][i_p]) == 14 ||
						   abs(csi->type_parton[0][i_p]) == 16)){runtime_order[i].push_back(i_p); runtime_order_inverse[i_p].push_back(i);}
    }
  }

  output_determine_runtime_object();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void event_set::output_determine_runtime_object(){
  static  Logger logger("event_set::output_determine_runtime_object");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger.newLine(LOG_INFO);
  logger << LOG_INFO << "Relevant objects:" << endl;
  logger << LOG_INFO << setw(5) << "no" << setw(10) << "object" << setw(10) << "n_min" << setw(10) << "n_max" << setw(10) << "pT" << setw(10) << "ET" << setw(10) << "|eta|"  << setw(10) << "|y|"<< endl;
  for (int i = 0; i < object_list_selection.size(); i++){
    logger << LOG_INFO << setw(5) << i << setw(10) << object_list_selection[i] << setw(10) << pds[i].n_observed_min << setw(10) << pds[i].n_observed_max << setw(10) << pds[i].define_pT << setw(10) << pds[i].define_ET << setw(10) << pds[i].define_eta << setw(10) << pds[i].define_y << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << "Relevant objects:   runtime_jet_recombination   collects partons that enter jet recombination." << endl;
  for (int i = 0; i < runtime_jet_recombination.size(); i++){
    logger << LOG_INFO << "runtime_jet_recombination[" << setw(3) << i << "] = " << setw(3) << runtime_jet_recombination[i] << "[" << setw(10) <<  left << object_list_selection[runtime_jet_recombination[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << "Relevant objects:   runtime_photon_isolation   collects partons that could become isolated photons a la Frixione." << endl;
  for (int i = 0; i < runtime_photon_isolation.size(); i++){
    logger << LOG_INFO << "runtime_photon_isolation[" << setw(3) << i << "] = " << setw(3) << runtime_photon_isolation[i] << "[" << setw(10) <<   left << object_list_selection[runtime_photon_isolation[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);

  /*
  //  runtime_photon_recombination is never used !!!
  logger << LOG_INFO << "Relevant objects:   runtime_photon_recombination   collects partons that enter photon recombination." << endl;
  for (int i = 0; i < runtime_photon_recombination.size(); i++){
    logger << LOG_INFO << "runtime_photon_recombination[" << setw(3) << i << "] = " << setw(3) << runtime_photon_recombination[i] << "[" << setw(10) << left << object_list_selection[runtime_photon_recombination[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);
  */
  /*
  logger << LOG_INFO << "Relevant objects:   runtime_missing   collects partons that enter event selection with their parton-level momenta." << endl;
  for (int i = 0; i < runtime_missing.size(); i++){
    logger << LOG_INFO << "runtime_missing[" << setw(3) << i << "] = " << setw(3) << runtime_missing[i] << "[" << setw(10) << left << object_list_selection[runtime_missing[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);
  */

  logger << LOG_INFO << "Relevant objects:   runtime_original   collects partons that enter event selection with their parton-level momenta." << endl;
  for (int i = 0; i < runtime_original.size(); i++){
    logger << LOG_INFO << "runtime_original[" << setw(3) << i << "] = " << setw(3) << runtime_original[i] << "[" << setw(10) << left << object_list_selection[runtime_original[i]] << "]" << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << "Relevant objects:   runtime_order   collects partons that belong to the respective object class." << endl;

  for (int i = 0; i < object_list_selection.size(); i++){
    stringstream sstemp;
    for (int j = 0; j < runtime_order[i].size(); j++){sstemp << setw(3) << runtime_order[i][j] << "[" << setw(6) << csi->type_parton[0][runtime_order[i][j]] << "]";}
    logger << LOG_INFO << setw(10) << object_list_selection[i] << " -> " << sstemp.str() << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << "Relevant objects:   runtime_order_inverse   collects object classes a parton belongs to." << endl;

  for (int i = 3; i < csi->type_parton[0].size(); i++){
    stringstream sstemp;
    for (int j = 0; j < runtime_order_inverse[i].size(); j++){sstemp << setw(10) << object_list_selection[runtime_order_inverse[i][j]] << " [" << setw(3) << runtime_order_inverse[i][j] << "]";}
    logger << LOG_INFO << "pa[" << setw(3) << i << "] = " << setw(5) << csi->type_parton[0][i] << " -> " << sstemp.str() << endl;
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
