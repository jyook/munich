#include "header.hpp"

void munich::integration_RA_MIX(){
  static Logger logger("munich::integration_RA_MIX");
  logger << LOG_INFO << "called" << endl;

  csi->initialization_complete();
  osi->initialization_complete();
  esi->initialization_complete();
  psi->initialization_complete_RA();

  osi->initialization_integration();

  observable_set save_osi = *osi;
  phasespace_set save_psi = *psi;
  runresumption_set rsi(*osi, *psi, save_osi, save_psi);

  asi->calculate_ME2check_RA_MIX(psi);

  if (psi->n_events_max == 0){osi->int_end = 1;}
  if (user->int_value[user->int_map["rescaling_exponent"]] != 0){psi->hcf = psi->hcf * pow(user->double_value[user->double_map["rescaling_factor"]], user->int_value[user->int_map["rescaling_exponent"]]);}

  osi->initialization_runtime();
  if (osi->int_end){osi->output_zero_contribution_complete();}
  while (osi->int_end == 0){
    rsi.perform_iteration_step();
    if (osi->int_end == 1){break;}

    psi->calculate_IS();

    esi->determine_p_parton();
    // replace by proper nan-check !!!
    if (psi->xbp_all[0] != psi->xbp_all[0]){errorhandling_c_psp_initial(); continue;}
    psi->calculate_IS_RA();
    if (psi->RA_x_a == 0){psi->ac_psp(0, psi->MC_phasespace.channel);}
    else {psi->ac_psp_RA_dipoles();}
    psi->correct_phasespacepoint_real();
    for (int i_p = 0; i_p < esi->p_parton[0].size(); i_p++){esi->p_parton[0][csi->swap_parton[0][i_p]] = psi->xbp_all[0][intpow(2, i_p - 1)];}
    // replace by proper nan-check !!!
    if (psi->xbp_all[0] != psi->xbp_all[0]){errorhandling_c_xbpsp(); continue;}
    for (int ib = 1; ib < esi->p_parton[0].size(); ib++){logger << LOG_DEBUG_VERBOSE << "esi->p_parton[0][" << ib << "][0] = " << psi->xbp_all[0][intpow(2, ib - 1)] << "   " << sqrt(abs(psi->xbp_all[0][intpow(2, ib - 1)].m2())) << endl;}
    for (int ib = 0; ib < esi->p_parton[0].size(); ib++){logger << LOG_DEBUG_VERBOSE << "esi->p_parton[0][" << ib << "] = " << esi->p_parton[0][ib] << "   " << esi->p_parton[0][ib].m2() << "   " << sqrt(abs(esi->p_parton[0][ib].m2())) << endl;}

    psi->determine_dipole_phasespace_RA();
    for (int i_a = 1; i_a < csi->n_ps; i_a++){for (int i_p = 0; i_p < esi->p_parton[i_a].size(); i_p++){esi->p_parton[i_a][i_p] = psi->xbp_all[i_a][intpow(2, i_p - 1)];}}
    psi->correct_phasespacepoint_dipole(); // Which phase-space is acutally corrected here (psi -- osi) ???

    // replace by proper nan-check !!!
    if (esi->p_parton[0] != esi->p_parton[0]){errorhandling_c_psp(); continue;}
    esi->perform_event_selection();
    if (osi->switch_RS == 1){for (int i_a = 1; i_a < csi->n_ps; i_a++){esi->cut_ps[i_a] = -1;}}
    else if (osi->switch_RS == 2){esi->cut_ps[0] = -1;}
    esi->first_non_cut_ps = -1;
    for (int i_a = 0; i_a < esi->cut_ps.size(); i_a++){if (esi->cut_ps[i_a] > -1){esi->first_non_cut_ps = i_a; break;}}
    if (esi->first_non_cut_ps == -1){psi->handling_cut_psp(); continue;}
    // from qT subtraction contribution RRA:
    for (int i_a = 0; i_a < esi->cut_ps.size(); i_a++){if (esi->cut_ps[i_a] > -1){if (osi->change_cut[esi->cut_ps[i_a] + 1] == 0){osi->change_cut[esi->cut_ps[i_a] + 1] = 1;}}}

    if (osi->switch_console_output_tau_0){psi->output_check_tau_0();}
    if (osi->switch_output_testpoint){osi->output_testpoint_input();}
    psi->ag_psp(0, 0);
    psi->ag_psp_RA_dipoles();
    psi->calculate_g_tot();
    if (munich_isnan(psi->g_tot)){errorhandling_RA_gtot(); continue;}

    asi->calculate_ME2_RA_MIX();

    double temp_sum_RA_ME2 = accumulate(osi->RA_ME2.begin(), osi->RA_ME2.end(), 0.);
    if (munich_isinf(temp_sum_RA_ME2)){errorhandling_RA_me2(3); continue;}
    if (munich_isnan(temp_sum_RA_ME2)){errorhandling_RA_me2(0); continue;}
    if (!(osi->check_vanishing_ME2_end)){handling_vanishing_me2(); if (osi->flag_vanishing_ME2){continue;}}

    if (psi->g_tot == 0.){logger << LOG_INFO << "psi->g_tot == 0." << endl; errorhandling_RA_me2(1); continue;} // does that really happen?
    if (munich_isinf(psi->g_tot)){psi->handling_cut_psp(); continue;}

    for (int i_a = 0; i_a < csi->n_ps; i_a++){
      if (esi->cut_ps[i_a] != -1){
        osi->calculate_dynamic_scale_RA(i_a);
        osi->calculate_dynamic_scale_TSV(i_a);
	osi->determine_scale_RA(i_a);
      }
    }

    // add proper errorhandling, also for mu_fact !!!
    // replace by proper nan-check !!!
    if (osi->switch_CV && osi->RA_value_factor_alpha_S != osi->RA_value_factor_alpha_S){errorhandling_alpha_S(); continue;}
    if (osi->switch_TSV && osi->value_relative_factor_alpha_S != osi->value_relative_factor_alpha_S){errorhandling_alpha_S(); continue;}

    if (osi->switch_moment){osi->moments();}

    osi->calculate_pdf_LHAPDF_RA_CV();
    osi->calculate_pdf_LHAPDF_TSV();

    osi->determine_integrand_RA();

    // introduce proper nan-check !!!
    if (osi->var_RA_ME2 != osi->var_RA_ME2){errorhandling_RA_me2(1); continue;}
    if (osi->switch_CV && osi->var_RA_ME2_CV != osi->var_RA_ME2_CV){errorhandling_RA_me2(1); continue;}
    osi->determine_techcut_RA();
    if (psi->RA_techcut){psi->RA_techcut = 0; psi->handling_cut_psp(); continue;}

    // introduce proper nan-check !!!
    if (osi->var_RA_ME2_CV != osi->var_RA_ME2_CV){errorhandling_RA_me2(2); continue;}
    if (osi->switch_output_cancellation_check){osi->output_cancellation_RA();}
    if (osi->switch_output_maxevent){osi->output_integrand_maximum_RA();}

    osi->determine_psp_weight_TSV();
    osi->determine_psp_weight_RA();
    if (osi->switch_distribution){osi->determine_distribution_complete();}
    static double optimization_modifier = 1.;
    if (user->switch_value[user->switch_map["optimization_modifier"]]){
      //      optimization_modifier = pow(esi->particle_event[esi->access_object["Vrec"]][0][0].pT, user->switch_value[user->switch_map["optimization_modifier"]]);
      optimization_modifier = pow(psi->x_pdf[0], user->switch_value[user->switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }

    psi->psp_optimization_complete(osi->integrand, osi->this_psp_weight, osi->this_psp_weight2, optimization_modifier);
    psi->i_acc++;

    osi->determine_runtime();
  }
  osi->output_finalization_integration();
}
