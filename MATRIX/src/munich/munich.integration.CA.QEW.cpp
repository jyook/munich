#include "header.hpp"

void munich::integration_CA_QEW(){
  static Logger logger("munich::integration_CA_QEW");
  logger << LOG_INFO << "called" << endl;

  csi->initialization_complete();
  osi->initialization_complete();
  esi->initialization_complete();
  psi->initialization_complete();

  osi->initialization_integration();

  observable_set save_osi = *osi;
  phasespace_set save_psi = *psi;
  runresumption_set rsi(*osi, *psi, save_osi, save_psi);

  asi->calculate_ME2check_CA_QEW();

  if (psi->n_events_max == 0){osi->int_end = 1;}
  if (user->int_value[user->int_map["rescaling_exponent"]] != 0){psi->hcf = psi->hcf * pow(user->double_value[user->double_map["rescaling_factor"]], user->int_value[user->int_map["rescaling_exponent"]]);}

  if (osi->int_end){osi->output_zero_contribution_complete();}
  osi->initialization_runtime();
  while (osi->int_end == 0){
    rsi.perform_iteration_step();
    if (osi->int_end == 1){break;}

    psi->calculate_IS();
    psi->ac_psp(0, psi->MC_phasespace.channel);
    esi->determine_p_parton();
    // replace by proper nan-check !!!
    if (esi->p_parton[0] != esi->p_parton[0]){errorhandling_c_psp(); continue;}
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){psi->handling_cut_psp(); continue;}
    if (osi->switch_console_output_tau_0){psi->output_check_tau_0();}
    psi->calculate_initial_collinear_z1z2_IS();
    if (osi->switch_output_testpoint){osi->output_testpoint_input();}
    psi->ag_psp(0, 0);
    psi->calculate_g_tot();
    if (munich_isnan(psi->g_tot) || munich_isinf(psi->g_tot)){errorhandling_gtot(); continue;}

    asi->calculate_ME2_CA_QEW();

    // replace by proper nan-check !!!
    if (osi->CA_ME2_cf != osi->CA_ME2_cf){errorhandling_collinear_me2(); continue;}
    if (munich_isinf(psi->g_tot)){psi->handling_cut_psp(); continue;}

    // why evaluated after ME2 here ???
    osi->calculate_dynamic_scale(0);
    osi->calculate_dynamic_scale_TSV(0);
    osi->determine_scale();
    osi->determine_scale_CA();

    if (osi->switch_moment){osi->moments();}

    osi->calculate_pdf_LHAPDF_CA_collinear_CV(psi->all_xz_coll_pdf);
    if (osi->switch_TSV){osi->calculate_pdf_LHAPDF_CA_collinear_TSV(psi->all_xz_coll_pdf);}

    osi->calculate_collinear_QEW();
    osi->determine_integrand_CA();
    
    if (osi->switch_output_maxevent){osi->output_integrand_maximum_CA();}
    if (munich_isnan(osi->integrand)){errorhandling_collinear_me2(); continue;}
    osi->determine_psp_weight_TSV();
    osi->determine_psp_weight();
    if (osi->switch_distribution){osi->determine_distribution_complete();}
    static double optimization_modifier = 1.;
    if (user->switch_value[user->switch_map["optimization_modifier"]]){
      //      optimization_modifier = pow(esi->particle_event[esi->access_object["Vrec"]][0][0].pT, user->switch_value[user->switch_map["optimization_modifier"]]);
      optimization_modifier = pow(psi->x_pdf[0], user->switch_value[user->switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }

    psi->psp_optimization_complete(osi->integrand, osi->this_psp_weight, osi->this_psp_weight2, optimization_modifier);
    psi->i_acc++;
    
    osi->determine_runtime();
  }
  osi->output_finalization_integration();
}
