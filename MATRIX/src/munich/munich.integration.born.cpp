#include "header.hpp"

void munich::integration_born(){
  static Logger logger("munich::integration_born");
  logger << LOG_INFO << "called" << endl;

  csi->initialization_complete();
  osi->initialization_complete();
  // check position: parts need to be done before psi->initialization_complete();
  esi->initialization_complete();

  psi->initialization_complete();
  // originally, esi->initialization_compelte(); was called at the beginning of osi->initialization_integration();
  osi->initialization_integration();

  observable_set save_osi = *osi;
  phasespace_set save_psi = *psi;
  runresumption_set rsi(*osi, *psi, save_osi, save_psi);

  asi->calculate_ME2check_born();

  osi->output_check_running_alpha_S();
  //  integrate_topwidth(osi, *psi);

  //  if (psi->n_events_max == 0){osi->int_end = 1;}
  if (user->int_value[user->int_map["rescaling_exponent"]] != 0){psi->hcf = psi->hcf * pow(user->double_value[user->double_map["rescaling_factor"]], user->int_value[user->int_map["rescaling_exponent"]]);}

  if (csi->int_end != 0){osi->int_end = csi->int_end;}

  if (osi->int_end){osi->output_zero_contribution_complete();}
  osi->initialization_runtime();
  while (osi->int_end == 0){
    rsi.perform_iteration_step();
    if (osi->int_end == 1){break;}
    psi->calculate_IS();
    psi->ac_psp(0, psi->MC_phasespace.channel);
    esi->determine_p_parton();
    if (esi->p_parton != esi->p_parton){errorhandling_c_psp(); continue;}
    esi->cut_ps[0] = 0;
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){psi->handling_cut_psp(); continue;}
    if (osi->switch_console_output_tau_0){psi->output_check_tau_0();}
    if (osi->switch_output_testpoint){osi->output_testpoint_input();}
    psi->ag_psp(0, 0);
    psi->calculate_g_tot();
    if (munich_isnan(psi->g_tot) || munich_isinf(psi->g_tot)){errorhandling_gtot(); continue;} // isinf case ???
    asi->calculate_ME2_born();

    if (munich_isnan(osi->ME2)){errorhandling_me2(); continue;}
    // new:
    if (csi->type_contribution == "L2RT" ||
	csi->type_contribution == "L2RJ"){
      osi->counter_acc_qTcut[esi->cut_ps[0]]++;
      if (osi->ME2 == 0.){
	osi->counter_killed_qTcut[esi->cut_ps[0]]++;
	psi->i_tec++;
	psi->handling_techcut_psp();
	continue;
      }
    }
    if (!(osi->check_vanishing_ME2_end) && csi->type_contribution == "born"){handling_vanishing_me2(); if (osi->flag_vanishing_ME2){continue;}}

    osi->calculate_dynamic_scale(0);
    osi->calculate_dynamic_scale_TSV(0);
    osi->determine_scale();

    if (osi->switch_moment){osi->moments();}

    osi->calculate_pdf_LHAPDF_CV();
    osi->calculate_pdf_LHAPDF_TSV();

    if (munich_isnan(osi->pdf_factor[0])){errorhandling_pdf(); continue;}
    if (munich_isnan(osi->var_rel_alpha_S)){errorhandling_alpha_S(); continue;}

    osi->determine_integrand();
    osi->determine_psp_weight_TSV();
    osi->determine_psp_weight();
    if (osi->switch_output_maxevent){osi->output_integrand_maximum();}
    if (osi->switch_distribution){osi->determine_distribution_complete();}

    static double optimization_modifier = 1.;
    if (user->switch_value[user->switch_map["optimization_modifier"]]){
      //      optimization_modifier = pow(esi->particle_event[esi->access_object["Vrec"]][0][0].pT, user->switch_value[user->switch_map["optimization_modifier"]]);
      optimization_modifier = pow(psi->x_pdf[0], user->switch_value[user->switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }

    psi->psp_optimization_complete(osi->integrand, osi->this_psp_weight, osi->this_psp_weight2, optimization_modifier);
    psi->i_acc++;

    //    asi->output_phasespacepoints();
   
    osi->determine_runtime();
  }

  osi->output_finalization_integration();
}
