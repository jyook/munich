#include "header.hpp"

void munich::integration_VT2_QCD(){
  static Logger logger("munich::integration_VT2_QCD");
  logger << LOG_INFO << "called" << endl;

  csi->initialization_complete();
  osi->initialization_complete();
  esi->initialization_complete();
  psi->initialization_complete();

  osi->initialization_integration();

  observable_set save_osi = *osi;
  phasespace_set save_psi = *psi;
  runresumption_set rsi(*osi, *psi, save_osi, save_psi);

  asi->calculate_ME2check_VT2_QCD();

  if (osi->mass_parton[0][1] > 0. || osi->mass_parton[0][2] > 0.){osi->int_end = 1;}
  if (psi->n_events_max == 0){osi->int_end = 1;}
  if (user->int_value[user->int_map["rescaling_exponent"]] != 0){psi->hcf = psi->hcf * pow(user->double_value[user->double_map["rescaling_factor"]], user->int_value[user->int_map["rescaling_exponent"]]);}

  if (osi->int_end){osi->output_zero_contribution_complete();}
  osi->initialization_runtime();
  while (osi->int_end == 0){
    rsi.perform_iteration_step();
    if (osi->int_end == 1){break;}

    psi->calculate_IS();
    psi->calculate_initial_collinear_z1z2_IS();
    if (psi->switch_resummation){psi->calculate_IS_QT();}
    osi->calculate_IS_CX();
    psi->ac_psp(0, psi->MC_phasespace.channel);
    esi->determine_p_parton();
    // replace by proper nan-check !!!
    if (esi->p_parton != esi->p_parton){errorhandling_c_psp(); continue;}

#ifdef MORE
    if (osi->switch_resummation){
      double Q = sqrt(psi->xbs_all[0][0]);
      double y = log(sqrt(psi->x_pdf[1] / psi->x_pdf[2]));
      performQTBoost_cms(psi->QT_qt2, Q, y, esi->p_parton);
    }
#endif

    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){psi->handling_cut_psp(); continue;}

    if (osi->switch_console_output_tau_0){psi->output_check_tau_0();}
    if (osi->switch_output_testpoint){osi->output_testpoint_input();}
    psi->ag_psp(0, 0);
    psi->calculate_g_tot();
    if (munich_isnan(psi->g_tot) || munich_isinf(psi->g_tot)){errorhandling_gtot(); continue;}

    osi->calculate_dynamic_scale(0);
    osi->calculate_dynamic_scale_TSV(0);
    osi->determine_scale();

    asi->calculate_ME2_VT2_QCD();
    osi->ME2 = osi->VA_V_ME2 + osi->VA_X_ME2;
    if (munich_isnan(osi->ME2)){errorhandling_me2(); continue;}

    if (osi->switch_moment){osi->moments();}

    osi->calculate_pdf_LHAPDF_list_CV();
    if (osi->switch_TSV){osi->calculate_pdf_LHAPDF_list_TSV();}
    osi->determine_integrand_CX_ncollinear_VT2();
    osi->determine_psp_weight_TSV();
    osi->determine_psp_weight_QT();
    if (osi->switch_output_maxevent){osi->output_integrand_maximum_VA();} // both ???
    if (osi->switch_output_maxevent){osi->output_integrand_maximum_VT2();}
    if (osi->switch_distribution){osi->determine_distribution_complete();}
    static double optimization_modifier = 1.;
    if (user->switch_value[user->switch_map["optimization_modifier"]]){
      //      optimization_modifier = pow(esi->particle_event[esi->access_object["Vrec"]][0][0].pT, user->switch_value[user->switch_map["optimization_modifier"]]);
      optimization_modifier = pow(psi->x_pdf[0], user->switch_value[user->switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }

    psi->psp_optimization_complete(osi->integrand, osi->this_psp_weight, osi->this_psp_weight2, optimization_modifier);
    psi->i_acc++;

    osi->determine_runtime();
  }
  osi->output_finalization_integration();
}
