#include "header.hpp"

void munich::integration_VA_MIX(){
  static Logger logger("munich::integration_VA_MIX");
  logger << LOG_INFO << "called" << endl;

  csi->initialization_complete();
  osi->initialization_complete();
  esi->initialization_complete();
  psi->initialization_complete();

  osi->initialization_integration();

  observable_set save_osi = *osi;
  phasespace_set save_psi = *psi;
  runresumption_set rsi(*osi, *psi, save_osi, save_psi);

  asi->calculate_ME2check_VA_MIX();

  if (psi->n_events_max == 0){osi->int_end = 1;}
  if (user->int_value[user->int_map["rescaling_exponent"]] != 0){psi->hcf = psi->hcf * pow(user->double_value[user->double_map["rescaling_factor"]], user->int_value[user->int_map["rescaling_exponent"]]);}

  if (osi->int_end){osi->output_zero_contribution_complete();}
  osi->initialization_runtime();
  while (osi->int_end == 0){
    rsi.perform_iteration_step();
    if (osi->int_end == 1){break;}
    psi->calculate_IS();
    psi->ac_psp(0, psi->MC_phasespace.channel);
    esi->determine_p_parton();
    // replace by proper nan-check !!!
    if (esi->p_parton[0] != esi->p_parton[0]){errorhandling_c_psp(); continue;}
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){psi->handling_cut_psp(); continue;}
    if (osi->switch_console_output_tau_0){psi->output_check_tau_0();}
    psi->ag_psp(0, 0);
    psi->calculate_g_tot();
    if (munich_isnan(psi->g_tot) || munich_isinf(psi->g_tot)){errorhandling_gtot(); continue;} // isinf case ???

    osi->calculate_dynamic_scale(0);
    osi->calculate_dynamic_scale_TSV(0);
    osi->determine_scale();

    asi->calculate_ME2_VA_MIX();

    if (munich_isnan(osi->VA_V_ME2) || munich_isnan(osi->VA_X_ME2) || munich_isnan(osi->VA_I_ME2)){errorhandling_me2(); continue;}
    if (osi->VA_V_ME2 == 0. && osi->switch_VI != 2){errorhandling_OL(); continue;}

    if (osi->switch_moment){osi->moments();}

    osi->calculate_pdf_LHAPDF_CV();
    osi->calculate_pdf_LHAPDF_TSV();

    osi->determine_integrand_VA();
    osi->determine_psp_weight_TSV();
    osi->determine_psp_weight();
    if (osi->switch_output_maxevent){osi->output_integrand_maximum_VA();}
    if (osi->switch_distribution){osi->determine_distribution_complete();}
    static double optimization_modifier = 1.;
    if (user->switch_value[user->switch_map["optimization_modifier"]]){
      optimization_modifier = pow(esi->particle_event[esi->access_object["Vrec"]][0][0].pT, user->switch_value[user->switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }

    psi->psp_optimization_complete(osi->integrand, osi->this_psp_weight, osi->this_psp_weight2, optimization_modifier);
    psi->i_acc++;
    
    osi->determine_runtime();
  }
  osi->output_finalization_integration();
}
