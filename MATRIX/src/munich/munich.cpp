#include "header.hpp"

munich::munich(){}

munich::munich(int argc, char *argv[], string basic_process_class){
  static Logger logger("munich::munich");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  Log::setLogThreshold(LOG_DEBUG);
  walltime_start();

  logger << LOG_DEBUG << "argc = " << argc << endl;
  if (argc == 0){logger << LOG_ERROR << "No input specified." << endl; exit(1);}
  else if (argc < 3){
    if (argc == 1){
      cin >> subprocess;
      logger << LOG_DEBUG << "console input: subprocess = " << subprocess << endl;
    }
    else if (argc == 2){
      logger << LOG_DEBUG << "argv[1] = " << argv[1] << endl;
      subprocess = argv[1];
    }

    isi = new inputparameter_set(basic_process_class, subprocess);
  }
  else if (argc > 2){
    logger << LOG_DEBUG << "argv[1] = " << argv[1] << endl;
    order = argv[1];

    logger << LOG_DEBUG << "argv[2] = " << argv[2] << endl;
    if (order == "result"){cout << "calculate the results specified in " << argv[2] << endl;}
    else if (order == "distribution"){cout << "calculate the results specified in " << argv[2] << endl;}
    else if (order == "scaleband"){cout << "calculate the results specified in " << argv[2] << endl;}
    infilename = argv[2];

   infilename_scaleband = "";
    if (argc == 4){
      logger << LOG_DEBUG << "argv[3] = " << argv[3] << endl;
      infilename_scaleband = argv[3];
    }

    isi = new inputparameter_set(basic_process_class, "");
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void munich::initialization(){
  static Logger logger("munich::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  csi->initialization(isi);

  user = new user_defined();
  user->initialization(isi);

  msi = new model_set();
  msi->initialization(isi, user);

  esi->initialization(isi, csi, msi, user);

  psi->initialization(isi, csi, msi, esi, user);
  esi->psi = psi;

  osi->initialization(isi, csi, msi, esi, psi, user);

  // For compatibility with "Bornloop" parameter... to be removed at some point !!!
  if (user->string_value[user->string_map["model"]] == "Bornloop"){csi->class_contribution_loopinduced = 1;}

  // finally not needed, I guess !!!
  osi->xmunich = this;

  // Isolate some qTsubtraction related information from observable_set (to be rearranged completely) !!!
  qT_basic = new qTsubtraction_basic(isi, csi, esi, psi);
  NJ_basic = new NJsubtraction_basic(isi, csi, esi, psi);

  qT_basic->output();

  esi->qT_basic = qT_basic;
  esi->NJ_basic = NJ_basic;

  logger.newLine(LOG_INFO);
  logger << LOG_INFO << "Settings from input files:" << endl << endl << isi << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void munich::run_initialization(){
  static Logger logger("munich::run_initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (asi == NULL){
    if (osi->switch_OL){
#ifdef OPENLOOPS
      asi = new amplitude_OpenLoops_set();
#endif
    }
    else if (osi->switch_RCL){
#ifdef RECOLA
      asi = new amplitude_Recola_set();
#endif
    }
  }

  // Should not be done here !!!
  psi->initialization_masses();

  // No duplicated information stored in csi should be required in psi in the future !!!
  csi->initialization_contribution_order();
  psi->initialization_contribution_order();


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void munich::run_integration(){
  static Logger logger("munich::run_integration");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "csi->type_contribution = " << csi->type_contribution << endl;
  logger << LOG_INFO << "csi->type_correction = " << csi->type_correction << endl;

  if (csi->type_contribution == "born"){integration_born();}
  if (csi->type_contribution == "loop"){integration_born();}
  if (csi->type_contribution == "L2I"){integration_born();}

  if (csi->type_contribution == "VA" && csi->type_correction == "QCD"){integration_VA_QCD();}
  if (csi->type_contribution == "CA" && csi->type_correction == "QCD"){integration_CA_QCD();}
  if (csi->type_contribution == "RA" && csi->type_correction == "QCD"){integration_RA_QCD();}

  if (csi->type_contribution == "L2VA" && csi->type_correction == "QCD"){integration_VA_QCD();}
  if (csi->type_contribution == "L2CA" && csi->type_correction == "QCD"){integration_CA_QCD();}
  if (csi->type_contribution == "L2RA" && csi->type_correction == "QCD"){integration_RA_QCD();}

  if (csi->type_contribution == "VA" && csi->type_correction == "QEW"){integration_VA_QEW();}
  if (csi->type_contribution == "CA" && csi->type_correction == "QEW"){integration_CA_QEW();}
  if (csi->type_contribution == "RA" && csi->type_correction == "QEW"){integration_RA_QEW();}
  if (csi->type_contribution == "VA" && csi->type_correction == "MIX"){integration_VA_MIX();}
  if (csi->type_contribution == "RA" && csi->type_correction == "MIX"){integration_RA_MIX();}

  if (csi->type_contribution == "VT" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "CT" && csi->type_correction == "QCD"){integration_CT_QCD();}
  if (csi->type_contribution == "RT" && csi->type_correction == "QCD"){integration_born();}

  if (csi->type_contribution == "VT2" && csi->type_correction == "QCD"){integration_VT2_QCD();}
  if (csi->type_contribution == "CT2" && csi->type_correction == "QCD"){integration_CT2_QCD();}
  if (csi->type_contribution == "RVA" && csi->type_correction == "QCD"){integration_VA_QCD();}
  if (csi->type_contribution == "RCA" && csi->type_correction == "QCD"){integration_CA_QCD();}
  if (csi->type_contribution == "RRA" && csi->type_correction == "QCD"){integration_RA_QCD();}

  if (csi->type_contribution == "L2VT" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "L2CT" && csi->type_correction == "QCD"){integration_CT_QCD();}
  if (csi->type_contribution == "L2RT" && csi->type_correction == "QCD"){integration_born();}

  //  suggested new names:
  //  B.NLL
  //  VT.NLL
  //  B.NNLL
  //  VT.NNLL
  //  VT2.NNLL
  if (csi->type_contribution == "NLL_LO" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "NLL_NLO" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "NNLL_LO" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "NNLL_NLO" && csi->type_correction == "QCD"){integration_VT_QCD();}
  if (csi->type_contribution == "NNLL_NNLO" && csi->type_correction == "QCD"){integration_VT2_QCD();}
  //  CT -> CT.res
  //  CT2 -> CT2.res

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void munich::walltime_start(){
  static Logger logger("munich::walltime_start");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  struct tm y2k_tm = {0};
  y2k_tm.tm_hour = 0;
  y2k_tm.tm_min = 0;
  y2k_tm.tm_sec = 0;
  y2k_tm.tm_year = 100;
  y2k_tm.tm_mon = 0;
  y2k_tm.tm_mday = 1;
  y2k_time = mktime (&y2k_tm);

  start_time_point = chrono::system_clock::now();
  start_time = chrono::system_clock::to_time_t(start_time_point);

  //  string temp_start_time = ctime(&start_time);
  //  temp_start_time = temp_start_time.substr(0, temp_start_time.size() - 1);
  //  logger << LOG_INFO << temp_start_time << "   ---   " << setprecision(16) << difftime(start_time, y2k_time) << " seconds since y2k" << endl;

  start_CPU_time = clock();

  logger << LOG_INFO << ctime(&start_time);
  logger << LOG_DEBUG << setprecision(16) << noshowpoint << difftime(start_time, y2k_time) << " seconds since y2k" << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void munich::walltime_end(){
  static Logger logger("munich::walltime_end");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  end_time_point = chrono::system_clock::now();
  end_time = chrono::system_clock::to_time_t(end_time_point);

  std::chrono::duration<double> elapsed_seconds = end_time_point - start_time_point;
  logger << LOG_INFO << "Wall time:  elapsed in seconds:  " << left << setprecision(15) << setw(23) << elapsed_seconds.count() << endl;
  //  double wall_sec_d = elapsed_seconds.count();
  int wall_sec = int(elapsed_seconds.count());
  int wall_min = 0;
  int wall_h = 0;
  if (wall_sec >= 3600) {
    wall_h = int(wall_sec / 3600);
    wall_sec -= wall_h * 3600;
  }
  if (wall_sec >= 60) {
    wall_min = int(wall_sec / 60);
    wall_sec -= wall_min * 60;
  }
  end_CPU_time = clock();
  double CPU_seconds = 1. * (end_CPU_time - start_CPU_time) / CLOCKS_PER_SEC;
  logger << LOG_INFO << "CPU time:  elapsed in seconds:   " << setprecision(15) << setw(23) << CPU_seconds << endl;
  logger << LOG_INFO << "CPU time / wall time ratio =  " << left << setprecision(15) << setw(23) << CPU_seconds / elapsed_seconds.count() << endl;

  int CPU_sec = int(CPU_seconds);
  int CPU_min = 0;
  int CPU_h = 0;
  if (CPU_sec >= 3600) {
    CPU_h = int(CPU_sec / 3600);
    CPU_sec -= CPU_h * 3600;
  }
  if (CPU_sec >= 60) {
    CPU_min = int(CPU_sec / 60);
    CPU_sec -= CPU_min * 60;
  }


  logger << LOG_INFO << "Wall time:  evaluation time = " << right << setw(3) << wall_h << "h " << right << setw(3) << wall_min << "min " << right << setw(3) << wall_sec << "sec     n_gen = " << right << setw(10) << psi->i_gen << "   n_acc = " << right << setw(10) << psi->i_acc << endl;
  logger << LOG_INFO << "CPU time:   evaluation time = " << right << setw(3) << CPU_h << "h " << right << setw(3) << CPU_min << "min " << right << setw(3) << CPU_sec << "sec     n_gen = " << right << setw(10) << psi->i_gen << "   n_acc = " << right << setw(10) << psi->i_acc << endl;
  logger << LOG_INFO << "Wall time:  evaluation time in seconds = " << right << setw(7) << wall_h * 3600 + wall_min * 60 + wall_sec << "     n_gen = " << right << setw(10) << psi->i_gen << "   n_acc = " << right << setw(10) << psi->i_acc << endl;
  logger << LOG_INFO << "CPU time:   evaluation time in seconds = " << right << setw(7) << CPU_h * 3600 + CPU_min * 60 + CPU_sec << "     n_gen = " << right << setw(10) << psi->i_gen << "   n_acc = " << right << setw(10) << psi->i_acc << endl;

  logger << LOG_INFO << ctime(&end_time);
  logger << LOG_DEBUG << setprecision(16) << noshowpoint << difftime(end_time, y2k_time) << " seconds since y2k" << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void munich::walltime_now(){
  static Logger logger("munich::walltime_end");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  now_time_point = chrono::system_clock::now();
  now_time = chrono::system_clock::to_time_t(now_time_point);
  //  double passedtime_now_y2k = difftime(now_time, y2k_time);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void munich::get_summary(){
  static Logger logger("munich::get_summary");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG_VERBOSE << "ysi->order = " << ysi->order << endl;
  logger << LOG_DEBUG_VERBOSE << "munich: order = " << order << endl;
  logger << LOG_DEBUG_VERBOSE << "ysi->infilename = " << ysi->infilename << endl;
  logger << LOG_DEBUG_VERBOSE << "munich: infilename = " << infilename << endl;
  logger << LOG_DEBUG_VERBOSE << "ysi->infilename_scaleband = " << ysi->infilename_scaleband << endl;
  logger << LOG_DEBUG_VERBOSE << "munich: infilename_scaleband = " << infilename_scaleband << endl;

  ysi->get_summary();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


