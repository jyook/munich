#include "header.hpp"

void phasespace_set::psp_optimization_complete(double & integrand, double & this_psp_weight, double & this_psp_weight2, double & optimization_modifier){
  Logger logger("phasespace_set::psp_optimization_complete");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double mod_integrand = integrand * optimization_modifier;
  double mod_this_psp_weight = this_psp_weight * optimization_modifier;
  double mod_this_psp_weight2 = this_psp_weight2 * pow(optimization_modifier, 2);

  // MC_phasespace - multichannel for final-state phasespace
  MC_phasespace.psp_MCweight_optimization(mod_integrand, g_tot);

  // MC_tau - multichannel for partonic CMS energy tau
  MC_tau.psp_MCweight_optimization(mod_integrand, g_tot);

  if (csi->class_contribution_CS_real){
    // MC_x_dipole[i_a] - multichannel for partonic CMS energy in dipole kinematics
    //    if (RA_x_a > 0){MC_x_dipole[RA_x_a].psp_MCweight_optimization(mod_integrand, g_tot);}
    for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
      // check if this should be done only for the used channel RA_x_a !!!
      if (MC_x_dipole[i_a].active_optimization != -1){MC_x_dipole[i_a].psp_MCweight_optimization(mod_integrand, g_tot);}
    }
  }

  if (!IS_tau.end_optimization){IS_tau.psp_IS_optimization(mod_this_psp_weight, mod_this_psp_weight2);}
  if (!IS_x1x2.end_optimization){IS_x1x2.psp_IS_optimization(mod_this_psp_weight, mod_this_psp_weight2);}

  random_manager.psp_IS_optimization(mod_this_psp_weight, mod_this_psp_weight2);

  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){if (!IS_z1z2[i_z].end_optimization){IS_z1z2[i_z].psp_IS_optimization(mod_this_psp_weight, mod_this_psp_weight2);}}
    //      for (int i_z = 1; i_z < 3; i_z++){if (!IS_z1z2[i_z].end_optimization){IS_z1z2[i_z].psp_IS_optimization(this_psp_weight, this_psp_weight2);}} // previously used !!!
  }

  if (munich_isnan(mod_integrand)){logger << LOG_FATAL << "unidentified error - mod_integrand nan - run stopped!" << endl; exit(1);}
  if (munich_isinf(mod_integrand)){logger << LOG_FATAL << "unidentified error - mod_integrand inf - run stopped!" << endl; exit(1);}
  if (munich_isnan(g_tot)){logger << LOG_FATAL << "unidentified error - gtot nan - run stopped!" << endl; exit(1);}
  //  if (munich_isinf(g_tot)){logger << LOG_DEBUG << "unidentified error - gtot inf - run stopped!" << endl; exit(1);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::step_optimization_complete(){
  Logger logger("phasespace_set::step_optimization_complete");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // MC_phasespace - multichannel for final-state phasespace
  MC_phasespace.step_MCweight_optimization(*i_step_mode);

  // MC_tau - multichannel for partonic CMS energy tau
  MC_tau.step_MCweight_optimization(*i_step_mode);

  if (csi->class_contribution_CS_real){
    // MC_x_dipole[i_a] - multichannel for partonic CMS energy in dipole kinematics
    for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
      MC_x_dipole[i_a].step_MCweight_optimization(*i_step_mode);
    }
  }

  // MC_opt_end should be replaced by something general like 'end_optimization'  !!!

  ///  random_manager.do_optimization_step(*i_step_mode);

  IS_tau.step_IS_optimization(*i_step_mode);
  IS_x1x2.step_IS_optimization(*i_step_mode);
  if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){IS_z1z2[i_z].step_IS_optimization(*i_step_mode);}}
  
  random_manager.step_optimization(*i_step_mode);

  // What about VT and VT2 ??? Should be treated already in IS_z1z2 !!!

  // Refine meaning of switch_use_alpha_after_IS ???

  if (switch_use_alpha_after_IS){
    // to choose best alpha set only after previous IS optimization step - not used at present !!!
    int temp_switch_IS_MC = -1;
    int temp_n_IS_events = 1;
    for (int i_s = 0; i_s < input_IS.size(); i_s++){
      if (input_IS[i_s]->name.substr(0, 5) == "IS_PS"){
	if (input_IS[i_s]->switch_optimization != -1){temp_switch_IS_MC = 1;}
	if (input_IS[i_s]->n_event_per_step > temp_n_IS_events){temp_n_IS_events = input_IS[i_s]->n_event_per_step;}
      }
    }
    
    if (temp_switch_IS_MC && !MC_opt_end && *i_step_mode % temp_n_IS_events == 0){
      //    if (switch_IS_MC && !MC_opt_end && *i_step_mode % n_IS_events == 0){
      logger << LOG_DEBUG << "switch_use_alpha_after_IS = " << switch_use_alpha_after_IS << endl;

      if (MC_phasespace.active_optimization && *i_step_mode < MC_phasespace.n_event_per_step * MC_phasespace.n_optimization_step){
	MC_phasespace.x_alpha_it_min = MC_phasespace.i_alpha_it;
      }

      if (MC_tau.active_optimization && *i_step_mode < MC_tau.n_event_per_step * MC_tau.n_optimization_step){
	MC_tau.x_alpha_it_min = MC_tau.i_alpha_it;
      }

      if (csi->class_contribution_CS_real){
	for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
	  if (MC_x_dipole[i_a].switch_optimization != -1 && MC_x_dipole[i_a].active_optimization && *i_step_mode < MC_x_dipole[i_a].n_event_per_step * MC_x_dipole[i_a].n_optimization_step){
	    MC_x_dipole[i_a].x_alpha_it_min = MC_x_dipole[i_a].i_alpha_it;
	  }
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::result_optimization_complete(){
  Logger logger("phasespace_set::result_optimization_complete");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}
  //  if (MC_opt_end == 0){
    // MC_phasespace - multichannel for final-state phasespace
    MC_phasespace.result_MCweight_optimization(*i_step_mode);

    // MC_tau - multichannel for partonic CMS energy tau
    MC_tau.result_MCweight_optimization(*i_step_mode);

    if (csi->class_contribution_CS_real){
      for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
	if (MC_x_dipole[i_a].active_optimization != -1){
	  // MC_x_dipole[i_a] - multichannel for partonic CMS energy in dipole kinematics
	  MC_x_dipole[i_a].result_MCweight_optimization(*i_step_mode);
	}
      }
    }

    // All multichannel optimizations are synchronized (nothing else tested...)
    /*
    MC_opt_end = 1;
    if (MC_phasespace.active_optimization == 1 && MC_phasespace.end_optimization != 1){MC_opt_end = 0;}
    if (MC_tau.active_optimization == 1 && MC_tau.end_optimization != 1){MC_opt_end = 0;}
    if (csi->class_contribution_CS_real){
      for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){if (MC_x_dipole[i_a].active_optimization == 1 && MC_x_dipole[i_a].end_optimization != 1){MC_opt_end = 0;}}
    }
    */
    //  }

  IS_tau.result_IS_optimization(*i_step_mode);
  IS_x1x2.result_IS_optimization(*i_step_mode);
  if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){IS_z1z2[i_z].result_IS_optimization(*i_step_mode);}}
  random_manager.result_optimization();

  end_optimization = 1;
  if (MC_phasespace.active_optimization == 1 && MC_phasespace.end_optimization != 1){end_optimization = 0;}
  if (MC_tau.active_optimization == 1 && MC_tau.end_optimization != 1){end_optimization = 0;}
  if (csi->class_contribution_CS_real){for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){if (MC_x_dipole[i_a].active_optimization == 1 && MC_x_dipole[i_a].end_optimization != 1){end_optimization = 0;}}}
  if (IS_tau.active_optimization == 1 && IS_tau.end_optimization != 1){end_optimization = 0;}
  if (IS_x1x2.active_optimization == 1 && IS_x1x2.end_optimization != 1){end_optimization = 0;}
  if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){if (IS_z1z2[i_z].active_optimization == 1 && IS_z1z2[i_z].end_optimization != 1){end_optimization = 0;}}}
  //  qtres !!!
  //  Check if random_manager part works !!!
  if (random_manager.active_optimization == 1 && random_manager.end_optimization != 1){end_optimization = 0;}
  for (int i_r = 0; i_r < random_psp.size(); i_r++){if (random_psp[i_r]->IS_PS.active_optimization == 1 && random_psp[i_r]->IS_PS.end_optimization != 1){end_optimization = 0;}}


  /*
  int temp_IS_opt_end = 1;
  if (IS_tau.active_optimization == 1 && IS_tau.end_optimization != 1){temp_IS_opt_end = 0;}
  if (IS_x1x2.active_optimization == 1 && IS_x1x2.end_optimization != 1){temp_IS_opt_end = 0;}
  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){if (IS_z1z2[i_z].active_optimization == 1 && IS_z1z2[i_z].end_optimization != 1){temp_IS_opt_end = 0;}}
  }

  if (end_optimization == 0){
    end_optimization = 1;
    if (MC_opt_end != 1){end_optimization = 0;}
    if (temp_IS_opt_end != 1){end_optimization = 0;}
  }
  */
  /*
  // possible solution without separating MC and IS parts:

  end_optimization = 1;

  if (!MC_phasespace.end_optimization){end_optimization = 0;}
  if (!MC_tau.end_optimization){end_optimization = 0;}
  if (csi->class_contribution_CS_real){
    for (int i_a = 1; i_a < n_dipoles; i_a++){
      if (!MC_x_dipole[i_a].end_optimization){end_optimization = 0;}
    }
  }
  // add IS stuff here !!!
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void phasespace_set::output_optimization_complete(){
  Logger logger("phasespace_set::output_optimization_complete");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // possibly unify output and write everything into a single file (without separating MC and IS parts):
  logger << LOG_INFO << "end_optimization = " << end_optimization << endl;
  if (!end_optimization){return;}
  if (end_optimization == 2){return;}
  //  logger << LOG_INFO << "MC_opt_end = " << MC_opt_end << endl;
  //  if (!MC_opt_end){return;}
    //  if (MC_opt_end){
    //    if (MC_opt_end == 1){
    ofstream out_weight;
    //    out_weight.open(filename_MCweight.c_str(), ofstream::out | ofstream::trunc);
    out_weight.open(filename_weight_writeout.c_str(), ofstream::out | ofstream::trunc);
    out_weight.close();

    string temp_filename_dec = filename_weight_writeout.insert(filename_weight_writeout.size() - 4, "_dec");
    logger << LOG_INFO << "filename_weight_writeout (dec) = " << temp_filename_dec << endl;
    out_weight.open(temp_filename_dec.c_str(), ofstream::out | ofstream::trunc);
    out_weight.close();

    MC_phasespace.output_optimization();
    MC_tau.output_optimization();
    if (csi->class_contribution_CS_real){for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){if (MC_x_dipole[i_a].active_optimization != -1){MC_x_dipole[i_a].output_optimization();}}}

    /*
    MC_phasespace.output_MCweight_optimization(*i_step_mode, filename_MCweight);
    MC_tau.output_MCweight_optimization(*i_step_mode, filename_MCweight);

    if (csi->class_contribution_CS_real){
      for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
	if (MC_x_dipole[i_a].active_optimization != -1){MC_x_dipole[i_a].output_MCweight_optimization(*i_step_mode, filename_MCweight);}
      }
    }
    */
    /*
    MC_opt_end = 2;
    if (MC_phasespace.active_optimization == 1 && MC_phasespace.end_optimization != 2){MC_opt_end = 1;}
    if (MC_tau.active_optimization == 1 && MC_tau.end_optimization != 2){MC_opt_end = 1;}
    if (csi->class_contribution_CS_real){
      for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
	if (MC_x_dipole[i_a].active_optimization == 1 && MC_x_dipole[i_a].end_optimization != 2){MC_opt_end = 1;}
      }
    }
    */
    //  }

  IS_tau.output_optimization();
  IS_x1x2.output_optimization();
  if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){IS_z1z2[i_z].output_optimization();}}
  random_manager.output_optimization();
  // random_manager
  //  if (random_manager.end_optimization){random_manager.writeout_weights();}

  /*
  IS_tau.output_IS_optimization(*i_step_mode);
  IS_x1x2.output_IS_optimization(*i_step_mode);
  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){IS_z1z2[i_z].output_IS_optimization(*i_step_mode);}
  }

  // random_manager
  if (random_manager.end_optimization){random_manager.writeout_weights();}
  */
  /*
  int temp_IS_opt_end = 2;
  if (IS_tau.active_optimization == 1 && IS_tau.end_optimization != 2){temp_IS_opt_end = 1;}
  if (IS_x1x2.active_optimization == 1 && IS_x1x2.end_optimization != 2){temp_IS_opt_end = 1;}
  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){if (IS_z1z2[i_z].active_optimization == 1 && IS_z1z2[i_z].end_optimization != 2){temp_IS_opt_end = 1;}}
  }

  if (end_optimization == 1){
    end_optimization = 2;
    if (MC_opt_end != 2){end_optimization = 1;}
    if (temp_IS_opt_end != 2){end_optimization = 1;}
  }
  */

  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


