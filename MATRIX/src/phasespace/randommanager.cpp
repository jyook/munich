#include "header.hpp"

randommanager::randommanager(){}

randommanager::randommanager(phasespace_set & _psi){
  static Logger logger("randommanager::randommanager");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  psi = &_psi;

  // check if reasonable initialization
  end_optimization = 1;
  used_queue.resize(0);
  // Check meaning of queue_counter !!!
  queue_counter=0;
  // Check meaning of queue_threshold !!!
  queue_threshold=1e2;

  //  readin_weights();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

randommanager::~randommanager(){
  static Logger logger("randommanager::~randommanager");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  //  for (int i_r = 0; i_r < random_psp.size(); i_r++){delete &random_psp[i_r];}
  //  for (int i_r = 0; i_r < random_variables.size(); i_r++) {
  //  FIXME: should the random manager be responsible for deleting the variables it manages?
  //  If so, we have to be careful that they actually still exists at this point
  //  delete random_variables[i_r];
  //  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void randommanager::initialization_random_psp(int x_r){
  static Logger logger("randommanager::initialization_random_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ostringstream convert;

  if (random_psp[x_r].IS_PS.name == "") {
    convert << "unnamed_random_psp_" << random_psp.size();
    random_psp[x_r].IS_PS.name = convert.str();
  }
  random_psp[x_r].manager = this;

  // temporary !!!
  //  random_psp[x_r].readin_weights(readin);
  if (random_psp[x_r].IS_PS.switch_optimization == 2 || random_psp[x_r].IS_PS.switch_optimization == 3){random_psp[x_r].IS_PS.readin_optimization();}
  //  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].IS_PS.readin_optimization();}
  end_optimization = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


double randommanager::get_random(int x_a, int x_t, int x_i){
  static Logger logger("randommanager::get_random(int x_a, int x_t, int x_i)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (psi->switch_IS_mode_phasespace == 3 || psi->switch_IS_mode_phasespace == 4){
    logger << LOG_DEBUG_POINT << "psi->MC_IS_name_type[" << x_a << " -- " << x_t << " -- " << x_i << "] = " << setw(50) << psi->MC_IS_name_type[x_a][x_t][x_i] << "   " << psi->map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}] << endl;
    double temp_r = random_psp[psi->map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}]].get_random();
    return temp_r;
  }
  else if (psi->switch_IS_mode_phasespace == 1 || psi->switch_IS_mode_phasespace == 3){
    // Needs to be re-activated (and checked) !!!
    return psi->r[++counter_mode_12];
  }
  else {
    logger << LOG_DEBUG << "counter_mode_12 = " << counter_mode_12 + 1 << "   psi->r.size() = " << psi->r.size() << "   psi->r[" << counter_mode_12 + 1 << "] = " << psi->r[counter_mode_12] << endl;
    return psi->r[++counter_mode_12];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::output_used() {
  static Logger logger("randommanager::output_used");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){logger << LOG_DEBUG_VERBOSE << setw(50) << random_psp[i_r].IS_PS.name << "   used = " << random_psp[i_r].used << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void randommanager::psp_IS_optimization(double & psp_weight, double & psp_weight2) {
  static Logger logger("randommanager::psp_IS_optimization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG_VERBOSE << "end_optimization = " << end_optimization << endl;

  if (end_optimization){return;}

  end_optimization = 1;

  logger << LOG_DEBUG_VERBOSE << "(random_psp.size() > queue_threshold) = " << (random_psp.size() > queue_threshold) << endl;

  // very confusing distinction !!! queue_threshold and queue_counter ???
  if (random_psp.size() > queue_threshold) {
    logger << LOG_DEBUG_VERBOSE << "queue_counter = " << queue_counter << endl;
    for (int i_u = 0; i_u < queue_counter; i_u++) {
      used_queue[i_u]->psp_IS_optimization(psp_weight, psp_weight2);
      if (used_queue[i_u]->IS_PS.end_optimization == 0){end_optimization = 0;}
    }
  }
  else {
    for (int i_r = 0; i_r < random_psp.size(); i_r++) {
      random_psp[i_r].psp_IS_optimization(psp_weight, psp_weight2);
      if (random_psp[i_r].IS_PS.end_optimization == 0){end_optimization = 0;}
    }
  }
  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::increase_counter_n_acc(){
  static Logger logger("randommanager::increase_counter_n_acc");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  if (random_psp.size() > queue_threshold){
    for (int x_r = 0; x_r < queue_counter; x_r++){
      used_queue[x_r]->IS_PS.n_acc_channel[used_queue[x_r]->IS_PS.channel]++;
      if (used_queue[x_r]->IS_PS.end_optimization == 0){used_queue[x_r]->used = false;}
    }
  }
  else {
    for (int x_r = 0; x_r < random_psp.size(); x_r++){
      if (random_psp[x_r].used){
        random_psp[x_r].IS_PS.n_acc_channel[random_psp[x_r].IS_PS.channel]++;
        random_psp[x_r].used = false;
      }
    }
  }

  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::increase_cut_counter(){
  static Logger logger("randommanager::increase_cut_counter");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}
  logger << LOG_DEBUG_VERBOSE << "random_psp.size() = " << random_psp.size() << " > " << queue_threshold << " = queue_threshold" << endl;

  if (random_psp.size() > queue_threshold){
    for (int x_r = 0; x_r < queue_counter; x_r++){
      used_queue[x_r]->IS_PS.n_rej_channel[used_queue[x_r]->IS_PS.channel]++;
      if (used_queue[x_r]->IS_PS.end_optimization == 0){used_queue[x_r]->used = false;}
    }
  }
  else {
    for (int i_r = 0; i_r < random_psp.size(); i_r++) {
      logger << LOG_DEBUG_VERBOSE << "random_psp[" << i_r << "].used = " << random_psp[i_r].used << endl;
      if (random_psp[i_r].used) {
        random_psp[i_r].IS_PS.n_rej_channel[random_psp[i_r].IS_PS.channel]++;
        random_psp[i_r].used = false;
      }
    }
  }

  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void randommanager::do_optimization_step(int events){
  static Logger logger("randommanager::do_optimization_step");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].step_IS_optimization(events);}
  
  end_optimization = 1;
  for (int i_r = 0; i_r < random_psp.size(); i_r++){if (random_psp[i_r].IS_PS.end_optimization == 0){end_optimization = 0; break;}}

  if (end_optimization == 1){writeout_weights();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

// New functions to replace do_optimization_step ("int events" is redundant)
void randommanager::step_optimization(int events){
  static Logger logger("randommanager::step_optimization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].step_IS_optimization(events);}
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// New functions to replace do_optimization_step
void randommanager::result_optimization(){
  static Logger logger("randommanager::result_optimization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  end_optimization = 1;
  for (int i_r = 0; i_r < random_psp.size(); i_r++){if (random_psp[i_r].IS_PS.end_optimization == 0){end_optimization = 0; break;}}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// New functions to replace do_optimization_step
void randommanager::output_optimization(){
  static Logger logger("randommanager::output_optimization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization != 1){return;}
  
  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].IS_PS.output_optimization();}
  end_optimization = 2;
  //  writeout_weights();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_out(ofstream & out_proceeding){
  static Logger logger("randommanager::proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].proceeding_out(out_proceeding);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_in(int & proc, vector<string> & readin){
  static Logger logger("randommanager::proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].proceeding_in(proc, readin);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_group_by_type_out(ofstream & out_proceeding){

  static Logger logger("randommanager::proceeding_group_by_type_out");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].proceeding_group_by_type_out(out_proceeding);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination){
  static Logger logger("randommanager::proceeding_group_by_type_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].proceeding_group_by_type_in(proc, readin, switch_combination);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::check_proceeding_in(int & int_end, int & temp_check_size, vector<string> & readin){
  static Logger logger("randommanager::check_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "int_end = " << int_end << endl;
  logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << endl;
  logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;

  if (temp_check_size > readin.size()){
    int_end = 2;
    logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
    return;
  }
  else {
    logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
  }

  logger << LOG_DEBUG << "random_psp.size() = " << random_psp.size() << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    //    logger << LOG_DEBUG << "random_psp[" << i_r << "] = " << random_psp[i_r].IS_PS.name << "   n_bins = " << random_psp[i_r].n_bins << endl;
    random_psp[i_r].check_proceeding_in(int_end, temp_check_size, readin);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void randommanager::readin_weights(){
  static Logger logger("randommanager::readin_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].IS_PS.readin_optimization();}
  /*
  ifstream readin_weights(psi->filename_IS_MCweight_in_contribution.c_str());

  if (readin_weights){
    char LineBuffer[128];
    while (readin_weights.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    readin_weights.close();
  }
  else {
    logger << LOG_DEBUG << "weight file " << psi->filename_IS_MCweight_in_contribution << " could not be opened" << endl;
    readin_weights.close();
    return;
  }
*//*
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/
/*
void randommanager::writeout_weights(){
  static Logger logger("randommanager::writeout_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].IS_PS.output_optimization();}
  //  ofstream out_weights(psi->filename_IS_MCweight.c_str());
  //  for (int i_r = 0; i_r < random_psp.size(); i_r++){random_psp[i_r].save_weights(out_weights);}
  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

void randommanager::add_var_to_queue(randomvariable * this_random_psp){
  static Logger logger("randommanager::add_var_to_queue");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  // check if this distinction makes sense !!!
  if (queue_counter >= used_queue.size()){used_queue.push_back(this_random_psp);}
  else {used_queue[queue_counter] = this_random_psp;}
  queue_counter++;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::nancut(){
  static Logger logger("randommanager::nancut");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){if (random_psp[i_r].used){random_psp[i_r].used = false;}}
  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

