#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
phasespace_set::phasespace_set(){}


phasespace_set::~phasespace_set(){
  Logger logger("phasespace_set::~phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  //  delete &random_manager;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::ax_psp(int x_a){
  Logger logger("phasespace_set::ax_psp");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  // psi->MC_phasespace.channel -> channel
  // Check for dipole contributions !!!

  if (csi->type_contribution == "born"){ax_psp_born(0);}
  else if (csi->type_contribution == "L2I"){ax_psp_born(0);}
  else if (csi->type_contribution == "RT"){ax_psp_real(0);}
  else if (csi->type_contribution == "L2RT"){ax_psp_real(0);}
  else if (csi->type_contribution == "VA"){ax_psp_born(0);}
  else if (csi->type_contribution == "RVA"){ax_psp_real(0);}
  else if (csi->type_contribution == "L2VA"){ax_psp_born(0);}
  else if (csi->type_contribution == "CA"){ax_psp_born(0);}
  else if (csi->type_contribution == "RCA"){ax_psp_real(0);}
  else if (csi->type_contribution == "L2CA"){ax_psp_born(0);}
  else if (csi->type_contribution == "RA"){ax_psp_real(0);}
  else if (csi->type_contribution == "RRA"){ax_psp_doublereal(0);}
  else if (csi->type_contribution == "L2RA"){ax_psp_real(0);}
  else if (csi->type_contribution == "VT"){ax_psp_born(0);}
  else if (csi->type_contribution == "L2VT"){ax_psp_born(0);}
  else if (csi->type_contribution == "CT"){ax_psp_born(0);}
  else if (csi->type_contribution == "L2CT"){ax_psp_born(0);}
  else if (csi->type_contribution == "VT2"){ax_psp_born(0);}
  else if (csi->type_contribution == "CT2"){ax_psp_born(0);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ac_psp(int x_a, int channel){
  Logger logger("phasespace_set::ac_psp");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  // psi->MC_phasespace.channel -> channel
  // Check for dipole contributions !!!

  if (csi->type_contribution == "born"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "L2I"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "RT"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "L2RT"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "VA"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "RVA"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "L2VA"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "CA"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "RCA"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "L2CA"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "RA"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "RRA"){ac_psp_doublereal(0, channel);}
  else if (csi->type_contribution == "L2RA"){ac_psp_real(0, channel);}
  else if (csi->type_contribution == "VT"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "L2VT"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "CT"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "L2CT"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "VT2"){ac_psp_born(0, channel);}
  else if (csi->type_contribution == "CT2"){ac_psp_born(0, channel);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ag_psp(int x_a, int zero){
  Logger logger("phasespace_set::ag_psp");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  // psi->MC_phasespace.zero -> zero
  // Check for dipole contributions !!!

  if (csi->type_contribution == "born"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "L2I"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "RT"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "L2RT"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "VA"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "RVA"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "L2VA"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "CA"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "RCA"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "L2CA"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "RA"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "RRA"){ag_psp_doublereal(0, zero);}
  else if (csi->type_contribution == "L2RA"){ag_psp_real(0, zero);}
  else if (csi->type_contribution == "VT"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "L2VT"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "CT"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "L2CT"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "VT2"){ag_psp_born(0, zero);}
  else if (csi->type_contribution == "CT2"){ag_psp_born(0, zero);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::ac_tau_psp(int x_a, vector<int> & tau_MC_map){
  Logger logger("phasespace_set::ac_tau_psp");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "born"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "L2I"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "RT"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "L2RT"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "VA"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "RVA"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "L2VA"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "CA"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "RCA"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "L2CA"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "RA"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "RRA"){ac_tau_psp_doublereal(0, tau_MC_map);}
  else if (csi->type_contribution == "L2RA"){ac_tau_psp_real(0, tau_MC_map);}
  else if (csi->type_contribution == "VT"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "L2VT"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "CT"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "L2CT"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "VT2"){ac_tau_psp_born(0, tau_MC_map);}
  else if (csi->type_contribution == "CT2"){ac_tau_psp_born(0, tau_MC_map);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::ac_tau_psp_dipole(int x_a, vector<int> & tau_MC_map){
  Logger logger("phasespace_set::ac_tau_psp_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RA"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "RRA"){ac_tau_psp_real(x_a, tau_MC_map);}
  else if (csi->type_contribution == "L2RA"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "RT"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "L2RT"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "RVA"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "L2VA"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "RCA"){ac_tau_psp_born(x_a, tau_MC_map);}
  else if (csi->type_contribution == "L2CA"){ac_tau_psp_born(x_a, tau_MC_map);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::ac_tau_psp_doubledipole(int x_a, vector<int> & tau_MC_map){
  Logger logger("phasespace_set::ac_tau_psp_doubledipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RRA"){ac_tau_psp_born(x_a, tau_MC_map);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::ax_psp_dipole(int x_a){
  Logger logger("phasespace_set::ax_psp_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RA"){ax_psp_born(x_a);}
  else if (csi->type_contribution == "RRA"){ax_psp_real(x_a);}
  else if (csi->type_contribution == "L2RA"){ax_psp_born(x_a);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ac_psp_dipole(int x_a, int channel){
  Logger logger("phasespace_set::ac_psp_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RA"){ac_psp_born(x_a, channel);}
  else if (csi->type_contribution == "L2RA"){ac_psp_born(x_a, channel);}
  else if (csi->type_contribution == "RRA"){ac_psp_real(x_a, channel);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ag_psp_dipole(int x_a, int zero){
  Logger logger("phasespace_set::ag_psp_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RA"){ag_psp_born(x_a, zero);}
  else if (csi->type_contribution == "L2RA"){ag_psp_born(x_a, zero);}
  else if (csi->type_contribution == "RRA"){ag_psp_real(x_a, zero);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::optimize_minv(){
  Logger logger("phasespace_set::optimize_minv");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "born"){optimize_minv_born();}
  else if (csi->type_contribution == "L2I"){optimize_minv_born();}
  else if (csi->type_contribution == "RT"){optimize_minv_real();}
  else if (csi->type_contribution == "L2RT"){optimize_minv_real();}
  else if (csi->type_contribution == "VA"){optimize_minv_born();}
  else if (csi->type_contribution == "RVA"){optimize_minv_real();}
  else if (csi->type_contribution == "L2VA"){optimize_minv_born();}
  else if (csi->type_contribution == "CA"){optimize_minv_born();}
  else if (csi->type_contribution == "RCA"){optimize_minv_real();}
  else if (csi->type_contribution == "L2CA"){optimize_minv_born();}
  else if (csi->type_contribution == "RA"){optimize_minv_real();}
  else if (csi->type_contribution == "RRA"){optimize_minv_doublereal();}
  else if (csi->type_contribution == "L2RA"){optimize_minv_real();}
  else if (csi->type_contribution == "VT"){optimize_minv_born();}
  else if (csi->type_contribution == "L2VT"){optimize_minv_born();}
  else if (csi->type_contribution == "CT"){optimize_minv_born();}
  else if (csi->type_contribution == "L2CT"){optimize_minv_born();}
  else if (csi->type_contribution == "VT2"){optimize_minv_born();}
  else if (csi->type_contribution == "CT2"){optimize_minv_born();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int phasespace_set::determination_MCchannels(int x_a){
  Logger logger("phasespace_set::determination_MCchannels");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "born"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "L2I"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "RT"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "L2RT"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "VA"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "RVA"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "L2VA"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "CA"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "RCA"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "L2CA"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "RA"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "RRA"){return determination_MCchannels_doublereal(0);}
  else if (csi->type_contribution == "L2RA"){return determination_MCchannels_real(0);}
  else if (csi->type_contribution == "VT"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "L2VT"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "CT"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "L2CT"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "VT2"){return determination_MCchannels_born(0);}
  else if (csi->type_contribution == "CT2"){return determination_MCchannels_born(0);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return 0;
}

int phasespace_set::determination_MCchannels_dipole(int x_a){
  Logger logger("phasespace_set::determination_MCchannels_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (csi->type_contribution == "RA"){return determination_MCchannels_born(x_a);}
  else if (csi->type_contribution == "L2RA"){return determination_MCchannels_born(x_a);}
  else if (csi->type_contribution == "RRA"){return determination_MCchannels_real(x_a);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return 0;
}






void phasespace_set::calculate_g_tot(){
  Logger logger("phasespace_set::calculate_g_tot");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  g_MC = 0.;
  for (int j = 0; j < MC_phasespace.alpha.size(); j++){g_MC += MC_phasespace.g_channel[j] * MC_phasespace.alpha[j];}

  if (g_global_NWA != 1.){g_MC *= g_global_NWA;}
  if (coll_choice > 0){g_tot = g_MC * g_pdf;}
  else {g_tot = g_MC;}
  if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 3){g_tot = g_tot * MC_g_IS_global;}

  logger << LOG_DEBUG_VERBOSE << setw(20) << "MC_tau.channel" << " = " << MC_tau.channel << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_MC" << " = " << setprecision(20) << setw(28) << g_MC << "   " << double2hexastr(g_MC) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_pdf" << " = " << setprecision(20) << setw(28) << g_pdf << "   " << double2hexastr(g_pdf) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_tot" << " = " << setprecision(20) << setw(28) << g_tot << "   " << double2hexastr(g_tot) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "xbs_all[0][0]" << " = " << setprecision(20) << setw(28) << xbs_all[0][0] << "   " << double2hexastr(xbs_all[0][0]) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << setw(20) << "g_tot" << " = " << setprecision(20) << setw(28) << g_tot << "   " << double2hexastr(g_tot) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << setw(20) << "xbs_all[0][0]" << " = " << setprecision(20) << setw(28) << xbs_all[0][0] << "   " << double2hexastr(xbs_all[0][0]) << endl;

  ps_factor = hcf / (g_tot * xbs_all[0][0]);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::calculate_IS(){
  Logger logger("phasespace_set::calculate_IS");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG_VERBOSE << "i_gen = " << setw(12) << i_gen << "   i_acc = " << setw(12) << i_acc << "   i_rej = " << setw(12) << i_rej << "   i_nan = " << setw(12) << i_nan << "   i_tec = " << setw(12) << i_tec << endl;

  logger << LOG_DEBUG_VERBOSE << "NEW RNG -- get_random_number_set" << endl << rng.output_random_generator_set() << endl;
  rng.get_random_number_set();
  logger << LOG_DEBUG_VERBOSE << "NEW RNG -- random_number_set   -- n_random_per_psp = " << n_random_per_psp << endl << rng.output_random_number_set() << endl;


  xbp_all = start_xbp_all;
  xbs_all = start_xbs_all;
  xbsqrts_all = start_xbsqrts_all;
  i_gen++;

  if (coll_choice != 0){
    logger << LOG_DEBUG_VERBOSE << "start_xbs_all[0][xb_max - 4 = " << xb_max - 4 << "] = " << start_xbs_all[0][xb_max - 4] << endl;
    if (start_xbs_all[0][xb_max - 4] != 0.){
      if (IS_x1x2.switch_optimization != 0){
	calculate_initial_tau_fixed_x1x2_IS();
      }
      else if (IS_x1x2.switch_optimization == 0){
	calculate_initial_tau_fixed_x1x2();
      }
      else {
	logger << LOG_FATAL << "No valid  tau_fixed  ---  " << "IS_x1x2.switch_optimization = " << IS_x1x2.switch_optimization << "  combination." << endl;
	exit(1);
      }
     }
    else {
      if (IS_tau.switch_optimization != 0 && IS_x1x2.switch_optimization != 0){
	calculate_initial_tau_IS_x1x2_IS();
      }
      else if (IS_tau.switch_optimization != 0 && IS_x1x2.switch_optimization == 0){
	calculate_initial_tau_IS_x1x2();
      }
      else if (IS_tau.switch_optimization == 0 && IS_x1x2.switch_optimization != 0){
	calculate_initial_tau_x1x2_IS();
      }
      else if (IS_tau.switch_optimization == 0 && IS_x1x2.switch_optimization == 0){
	calculate_initial_tau_x1x2();
      }
      else {
	logger << LOG_FATAL << "No valid  IS_tau.switch_optimization = " << IS_tau.switch_optimization << "  ---  " << "IS_x1x2.switch_optimization = " << IS_x1x2.switch_optimization << "  combination." << endl;
	exit(1);
      }
    }

    boost = (x_pdf[1] - x_pdf[2]) / (x_pdf[1] + x_pdf[2]);
    g_pdf = g_tau * g_x1x2;
    logger << LOG_DEBUG_VERBOSE << "g_pdf = " << g_pdf << endl;
  }

  double temp_random_MC_channel = rng.access_random_number();
  for (int j = 0; j < MC_phasespace.beta.size(); j++){if (temp_random_MC_channel <= MC_phasespace.beta[j]){MC_phasespace.channel = j; break;}}


  MC_g_IS_global = 1.;

  // more informative name for 'weight_IS', e.g. switch_IS_mode_phasespace ???
  logger << LOG_DEBUG_VERBOSE << "MC_channel_phasespace = " << MC_channel_phasespace << endl;
  if (switch_IS_mode_phasespace == 0){
    random_manager.counter_mode_12 = 0;
    for (int k = 0; k < no_random; k++){
      r[k + 1] = rng.access_random_number();
    }
  }
  else if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 2){
    random_manager.counter_mode_12 = 0;
    for (int k = 0; k < no_random; k++){r[k + 1] = random_manager.random_psp[MC_phasespace.channel * no_random + k].get_random();}
  }
  //  For switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 2,
  //  r[1 ... no_random] are not used (random numbers are determied directly).
  //  switch_IS_mode_phasespace == 1 and 2 should be removed completely !!!

  if (csi->swap_parton[0][1] == 2 && csi->swap_parton[0][2] == 1){swap(xbp_all[0][1], xbp_all[0][2]);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::calculate_IS_RA(){
  Logger logger("phasespace_set::calculate_IS_RA");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i_a = 0; i_a < csi->dipole.size(); i_a++){
    if (MC_phasespace.channel < csi->dipole[i_a].sum_channel()){
      RA_x_a = i_a;
      if (i_a == 0){MC_channel_phasespace = MC_phasespace.channel;}
      else {MC_channel_phasespace = MC_phasespace.channel - MC_sum_channel_phasespace[i_a - 1];}
      break;
    }
  }
  /*
  int temp_zero = 0;
  if (RA_x_a > 0){temp_zero = csi->dipole[RA_x_a - 1].sum_channel();}
  logger << LOG_DEBUG_VERBOSE << "MC_phasespace.channel = " << MC_phasespace.channel << "   RA_x_a = " << RA_x_a << "   channel[" << RA_x_a << "] = " << MC_phasespace.channel - temp_zero << endl;
  */
    
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// used only for b-space resummation !!!
void phasespace_set::calculate_IS_QT(){
  Logger logger("phasespace_set::calculate_IS_QT");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  random_qTres = rng.access_random_number(n_random_qTres);
  for (int j = 0; j < IS_qTres.beta.size(); j++){if (random_qTres[0] <= IS_qTres.beta[j]){IS_qTres.channel = j; break;}}
  QT_random_qt2 = (double(IS_qTres.channel) + random_x12[1]) / double(IS_qTres.beta.size());
  g_qTres = 1. / (IS_qTres.alpha[IS_qTres.channel] * IS_qTres.alpha.size());
  g_pdf *= g_qTres;

  /* ///
  QT_random_qt->get_random(QT_random_qt2,QT_g_IS_);
  //  logger << LOG_DEBUG_VERBOSE << "qt: QT_random_qt2 = " << setw(23) << setprecision(15) << QT_random_qt2 << "   QT_g_IS_ = " << setw(23) << setprecision(15) << QT_g_IS_ << endl;
  g_pdf *= QT_g_IS_;
  */

  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "CT2"){
    // the (Bessel transformed) large logarithms vanish up to double precision accuracy for arguments larger than 40 -> use this as upper bound
    // 1.261 ~ b0*b0
    double maxqt2;
    double Qres_tmp=0.0, Qres_lower, Qres_upper;

    // Qres appears at two places here: as the reference value for the lower and for the upper qT cut
    // conceptually, these are partially unrelated
    // in fixed order computations, we have to use Qres as the lower cut off scale, because we precompute the qT integrals
    // in resummed computations, we explicitly integrate over qT, so this is unnecessary; in fact
    // it is more efficient to use mF as the lower reference scale, as qT/mF determine the universal limit
    // the upper cut off scale should alway be given by Qres, as otherwise qT/Qres can become huge, resulting in numerical
    // problems in the counterterm weight computation

    // Qres != mF does not work at the moment; has to be implemented in process specific cut files
    logger << LOG_DEBUG_VERBOSE << "switch_resummation = " << switch_resummation << endl;
    logger << LOG_DEBUG_VERBOSE << "Qres = " << Qres << endl;

    assert(Qres == 0. || !switch_resummation);


    if (dynamical_Qres) {
      Qres_tmp = Qres_prefactor*xbsqrts_all[0][0];
    } else {
      Qres_tmp = Qres;
    }
    if (Qres_tmp == 0) {
      // default choice
      Qres_tmp = xbsqrts_all[0][0];
    }

    Qres_upper = Qres_tmp;

    if (switch_resummation){
      Qres_lower = xbsqrts_all[0][0];
    } else {
      Qres_lower = Qres_tmp;
    }

    if (csi->contribution_order_alpha_s == 1){maxqt2 = 1.261 * 30. * 20. * Qres_upper * Qres_upper;}
    else {maxqt2 = 1.261 * 40. * 50. * Qres_upper * Qres_upper;}

    if (switch_qTcut == 1){
      double r0 = min_qTcut / 100.;
      //  version with cut on pT/Qres_cut
      QT_qt2 = r0 * r0 * pow(Qres_lower,2) * exp(log(maxqt2 / r0 / r0 / Qres_lower / Qres_lower) * QT_random_qt2);
//       logger << LOG_DEBUG_VERBOSE << "qT=" << QT_qt2 << ", Qres=" << Qres_upper << ", " << Qres_lower << ", ratio=" << QT_qt2/Qres_upper << endl;
      QT_jacqt2 = log(maxqt2 / r0 / r0 / Qres_lower / Qres_lower) * QT_qt2;
      //  version with cut on pT/m_inv
      //    QT_qt2 = r0 * r0 * xbs_all[0][0] * exp(log(maxqt2 / r0 / r0 / xbs_all[0][0]) * QT_random_qt2);
      //    QT_jacqt2 = log(maxqt2 / r0 / r0 / xbs_all[0][0]) * QT_qt2;
    }
    else if (switch_qTcut == 2){
      // old version with cut on pT only (not relative to m_inv)
      //   qt2=min_qTcut*min_qTcut*exp(log(maxqt2/min_qTcut/min_qTcut)*random_qt2);
      //   jacqt2=log(maxqt2/min_qTcut/min_qTcut)*qt2;
      logger << LOG_FATAL << "old qT variation is deprecated" << endl;
      exit(0);
    }
  }

  else if (csi->type_contribution == "NLL_LO" || csi->type_contribution == "NLL_NLO" || csi->type_contribution == "NNLL_LO" || csi->type_contribution == "NNLL_NLO" || csi->type_contribution == "NNLL_NNLO"){
    if (switch_resummation) {
      min_qTcut = 0.1; // ??? why is that set manually (and overwrites the set value of min_qTcut) ???

      double maxqt2 = pow(2 * E, 2);
      maxqt2 = pow(E, 2); // ???

      if (lambda_qt2 == 0 || lambda_qt2 == 1) { // ??? meaning of lambda_qt2 ??? does it only choose the mapping of QT_qt2 ???
	QT_qt2 = min_qTcut * min_qTcut * exp(log(maxqt2 / min_qTcut / min_qTcut) * QT_random_qt2);
	QT_jacqt2 = log(maxqt2 / min_qTcut / min_qTcut) * QT_qt2;
      }

      //  random_qt2=0;

      //  double lambda=0.0355064;
      //  double C=-1.0/lambda*(exp(-lambda*sqrt(maxqt2))-exp(-lambda*min_qTcut));
      //  double y0=-1.0/lambda/C*exp(-lambda*min_qTcut);
      ////  logger << LOG_DEBUG_VERBOSE << exp(lambda*sqrt(maxqt2)) << ", " << exp(lambda*min_qTcut) << endl;
      ////  logger << LOG_DEBUG_VERBOSE << C << ", " << y0 << endl;
      //  double qt=-1.0/lambda*log(-lambda*C*(random_qt2+y0));
      //  double jacqt=C*exp(lambda*qt);
      //  qt2=qt*qt;
      //  jacqt2=jacqt*2*qt;

      else {
	double y0 = 1. / (pow(maxqt2 / min_qTcut / min_qTcut, 1 - lambda_qt2) - 1);
	double C = pow(min_qTcut, 2 * (1 - lambda_qt2)) / (1 - lambda_qt2) / y0;

	QT_qt2 = pow((1 - lambda_qt2) * C * (QT_random_qt2 + y0), 1. / (1 - lambda_qt2));
	QT_jacqt2 = C * pow(QT_qt2, lambda_qt2);

	//    logger << LOG_DEBUG_VERBOSE << C << ", " << y0 << ", " << (1-lambda_qt2)*C*(random_qt2+y0) << endl;
	//    logger << LOG_DEBUG_VERBOSE << qt2 << ", " << maxqt2 << endl;
      }

      //  maxqt2 = 1e6;
      //  qt2 = min_qTcut * min_qTcut + random_qt2 * (maxqt2 - min_qTcut * min_qTcut);
      //  jacqt2 = (maxqt2 - min_qTcut * min_qTcut);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::output_check_tau_0(){
  Logger logger("phasespace_set::output_check_tau_0");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  if (x_pdf[0] < tau_0_num[tau_0_num.size() - 1]){
    tau_0_num.push_back(x_pdf[0]);
    logger << LOG_INFO << "tau_0 = " << setw(23) << setprecision(15) << tau_0 << "   min(" << setw(3) << tau_0_num.size() - 1 << "): " << setw(23) << setprecision(15) << tau_0_num[tau_0_num.size() - 1] << endl;
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::handling_cut_psp(){
  static Logger logger("phasespace_set::handling_cut_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Overall "if (!end_optimization){" ???

  // Order of calls changed: MC_phasespace was last beforehand !!!

  if (!MC_phasespace.end_optimization){
    MC_phasespace.n_rej++;
    MC_phasespace.n_rej_channel[MC_phasespace.channel]++;
  }
  
  if (!MC_tau.end_optimization){
    MC_tau.n_rej++;
    MC_tau.n_rej_channel[MC_tau.channel]++;
  }

  // count cut events for weight optimization of dipole mappings
  if (RA_x_a != 0) {
    if ((MC_x_dipole[RA_x_a].switch_optimization != -1) && csi->class_contribution_CS_real){
      if (!MC_x_dipole[RA_x_a].end_optimization){
	MC_x_dipole[RA_x_a].n_rej++;
        MC_x_dipole[RA_x_a].n_rej_channel[MC_x_dipole[RA_x_a].channel]++;
      }
    }
  }

  if (!IS_tau.end_optimization){
    IS_tau.n_rej++;
    IS_tau.n_rej_channel[IS_tau.channel]++;
  }

  if (!IS_x1x2.end_optimization){
    IS_x1x2.n_rej++;
    IS_x1x2.n_rej_channel[IS_x1x2.channel]++;
  }

  /*
  // May not be called here: z1z2 are determined only for accepted psp's !!!
  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){
      if (!IS_z1z2[i_z].end_optimization){
	IS_z1z2[i_z].n_rej++;
	IS_z1z2[i_z].n_rej_channel[IS_z1z2[i_z].channel]++;
      }
    }
  }
  */

  i_rej++;
  // change order between these two ???
  random_manager.increase_cut_counter();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::handling_techcut_psp(){
  static Logger logger("phasespace_set::handling_techcut_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // change order of calls ???
  // Order of calls changed: MC_phasespace was last beforehand !!!

  if (!MC_phasespace.end_optimization){
    MC_phasespace.n_acc++;
    MC_phasespace.n_acc_channel[MC_phasespace.channel]++;
  }

  if (!MC_tau.end_optimization){
    MC_tau.n_acc++;
    MC_tau.n_acc_channel[MC_tau.channel]++;
  }

  // count cut events for weight optimization of dipole mappings
  if (RA_x_a != 0) {
    if ((MC_x_dipole[RA_x_a].switch_optimization != -1) && csi->class_contribution_CS_real){
      if (!MC_x_dipole[RA_x_a].end_optimization){
	MC_x_dipole[RA_x_a].n_acc++;
        MC_x_dipole[RA_x_a].n_acc_channel[MC_x_dipole[RA_x_a].channel]++;
      }
    }
  }

  if (!IS_tau.end_optimization){
    IS_tau.n_acc++;
    IS_tau.n_acc_channel[IS_tau.channel]++;
  }

  if (!IS_x1x2.end_optimization){
    IS_x1x2.n_acc++;
    IS_x1x2.n_acc_channel[IS_x1x2.channel]++;
  }

  if (csi->class_contribution_collinear){
    for (int i_z = 1; i_z < 3; i_z++){
      if (!IS_z1z2[i_z].end_optimization){
	IS_z1z2[i_z].n_acc++;
	IS_z1z2[i_z].n_acc_channel[IS_z1z2[i_z].channel]++;
      }
    }
  }

  i_acc++;
  // change order between these two ???
  random_manager.increase_counter_n_acc();
  //  random_manager.increase_cut_counter();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
