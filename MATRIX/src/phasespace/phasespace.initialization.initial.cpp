#include "header.hpp"

// !!! temporary -> works now, but needs to be completely re-written !!!
void phasespace_set::initialization_minimum_tau(){
  Logger logger("phasespace_set::initialization_minimum_tau");
  logger << LOG_DEBUG << "started" << endl;

  ////////////////////////////////////////
  //  minimum CMS-energy determination  //
  ////////////////////////////////////////

  // !!! should be completely worked out anew and shifted elsewhere !!!
  // !!! should be there, but worked out completely anew !!!

  // extend to be used also with define_ET

  double minEjet = 0.;
  // minimum energy required for jet (including b-jet (and c-jet)) system
  //  if (esi->pda[esi->observed_object["ljet"]].n_observed_min > 0){
  //  for (int i = 0; i < esi->pda[esi->observed_object["ljet"]].n_observed_min; i++){minEljet.push_back(esi->pda[esi->observed_object["ljet"]].define_pT);}
  vector<double> minEljet(esi->pda[esi->observed_object["ljet"]].n_partonlevel, 0.);
  for (int i = 0; i < esi->pda[esi->observed_object["ljet"]].n_observed_min; i++){minEljet[i] = esi->pda[esi->observed_object["ljet"]].define_pT;}
  //  }
  //  vector<double> minEbjet;
  //  for (int i = 0; i < esi->pda[esi->observed_object["bjet"]].n_partonlevel; i++){minEbjet.push_back(M[5]);}
  vector<double> minEbjet(esi->pda[esi->observed_object["bjet"]].n_partonlevel, M[5]);
  if (esi->pda[esi->observed_object["bjet"]].n_partonlevel < esi->pda[esi->observed_object["bjet"]].n_observed_min){} // cross-section contribution is zero
  if (esi->pda[esi->observed_object["bjet"]].n_partonlevel == esi->pda[esi->observed_object["bjet"]].n_observed_min){for (int i = 0; i < esi->pda[esi->observed_object["bjet"]].n_partonlevel; i++){minEbjet[i] = sqrt(M2[5] + pow(esi->pda[esi->observed_object["bjet"]].define_pT, 2));}}
  //  else if (esi->pda[esi->observed_object["bjet"]].n_partonlevel == esi->pda[esi->observed_object["bjet"]].n_observed_min){minEjet += esi->pda[esi->observed_object["bjet"]].n_observed_min * sqrt(M2[5] + pow(esi->pda[esi->observed_object["bjet"]].define_pT, 2));}
  else if ((esi->pda[esi->observed_object["bjet"]].n_partonlevel > esi->pda[esi->observed_object["bjet"]].n_observed_min) && (esi->pda[esi->observed_object["bjet"]].n_observed_min > 0)){
    int min_bs_per_bjet = esi->pda[esi->observed_object["bjet"]].n_partonlevel / esi->pda[esi->observed_object["bjet"]].n_observed_min;
    int n_more_bs_per_bjet = esi->pda[esi->observed_object["bjet"]].n_partonlevel - esi->pda[esi->observed_object["bjet"]].n_observed_min * min_bs_per_bjet;
    for (int i = 0; i < min_bs_per_bjet * (esi->pda[esi->observed_object["bjet"]].n_observed_min - n_more_bs_per_bjet); i++){
      if (i < min_bs_per_bjet * (esi->pda[esi->observed_object["bjet"]].n_observed_min - n_more_bs_per_bjet)){minEbjet[i] = sqrt(M2[5] + pow(esi->pda[esi->observed_object["bjet"]].define_pT / min_bs_per_bjet, 2));}
      else {minEbjet[i] = sqrt(M2[5] + pow(esi->pda[esi->observed_object["bjet"]].define_pT / (min_bs_per_bjet + 1), 2));}
    }
  }
  for (int i = 0; i < esi->pda[esi->observed_object["ljet"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEljet[" << i << "] = " << minEljet[i] << endl;}
  for (int i = 0; i < esi->pda[esi->observed_object["bjet"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEbjet[" << i << "] = " << minEbjet[i] << endl;}
  minEjet = accumulate(minEljet.begin(), minEljet.end(), 0.) + accumulate(minEbjet.begin(), minEbjet.end(), 0.);
  logger << LOG_DEBUG << "minEjet = " << minEjet << endl;
  if (user->switch_value[user->switch_map["M_jetjet"]] == 1 &&
      esi->pda[esi->observed_object["bjet"]].n_partonlevel + esi->pda[esi->observed_object["ljet"]].n_partonlevel >= 2){
    if (minEjet < user->cut_value[user->cut_map["M_jetjet"]]){
      minEjet = user->cut_value[user->cut_map["M_jetjet"]];
    }
  }
  logger << LOG_DEBUG << "minEjet = " << minEjet << endl;


  double minEnua = 0.;
  if (esi->pda[esi->observed_object["nua"]].n_partonlevel != 0){
    if (user->switch_value[user->switch_map["M_nuanua"]] == 1 &&
	esi->pda[esi->observed_object["nua"]].n_partonlevel >= 2){
      if (minEnua < user->cut_value[user->cut_map["min_M_nuanua"]]){
	minEnua = user->cut_value[user->cut_map["min_M_nuanua"]];
      }
    }
  }

  double minElep = 0.;
  vector<double> minEe(esi->pda[esi->observed_object["e"]].n_partonlevel, 0.);
  vector<double> minEmu(esi->pda[esi->observed_object["mu"]].n_partonlevel, 0.);
  vector<double> minEtau(esi->pda[esi->observed_object["tau"]].n_partonlevel, 0.);
  vector<double> minEalep(esi->pda[esi->observed_object["lep"]].n_partonlevel, 0.);

//  logger << LOG_DEBUG << "osi_observed_object[lep] = " << osi_observed_object["lep"] << endl;
  logger << LOG_DEBUG << "esi->pda[lep]].n_observed_min = " << esi->pda[esi->observed_object["lep"]].n_observed_min << endl;
  logger << LOG_DEBUG << "esi->pda[lep]].n_partonlevel = " << esi->pda[esi->observed_object["lep"]].n_partonlevel << endl;
  logger << LOG_DEBUG << "minEalep.size() = " << minEalep.size() << endl;

  if (esi->pda[esi->observed_object["lep"]].n_partonlevel != 0){
    for (int i = 0; i < esi->pda[esi->observed_object["lep"]].n_observed_min; i++){minEalep[i] = esi->pda[esi->observed_object["lep"]].define_pT;}
    for (int i = 0; i < esi->pda[esi->observed_object["lep"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEalep[" << i << "] = " << minEalep[i] << endl;}
    minElep = accumulate(minEalep.begin(), minEalep.end(), 0.);
    logger << LOG_DEBUG << "minElep = " << minElep << endl;

    if (user->switch_value[user->switch_map["M_Zrec"]] == 1 &&
	esi->pda[esi->observed_object["lep"]].n_partonlevel >= 2){
      if (minElep < user->cut_value[user->cut_map["min_M_Zrec"]]){
	minElep = user->cut_value[user->cut_map["min_M_Zrec"]];
      }
    }

    if (user->switch_value[user->switch_map["delta_M_Zrec_MZ "]] == 1 &&
	esi->pda[esi->observed_object["lep"]].n_partonlevel >= 2){
      if (minElep < M[23] - user->cut_value[user->cut_map["max_delta_M_Zrec_MZ"]]){
	minElep = M[23] - user->cut_value[user->cut_map["max_delta_M_Zrec_MZ"]];
      }
    }

    if (user->switch_value[user->switch_map["M_leplep"]] == 1 &&
	esi->pda[esi->observed_object["lep"]].n_partonlevel >= 2){
      if (minElep < user->cut_value[user->cut_map["min_M_leplep"]]){
	minElep = user->cut_value[user->cut_map["min_M_leplep"]];
      }
    }

    if (user->switch_value[user->switch_map["M_emep"]] == 1 &&
	esi->pda[esi->observed_object["e"]].n_partonlevel >= 2){
      if (minElep < user->cut_value[user->cut_map["min_M_emep"]]){
	minElep = user->cut_value[user->cut_map["min_M_emep"]];
      }
    }

    if (user->switch_value[user->switch_map["M_mummup"]] == 1 &&
	esi->pda[esi->observed_object["mu"]].n_partonlevel >= 2){
      if (minElep < user->cut_value[user->cut_map["min_M_mummup"]]){
	minElep = user->cut_value[user->cut_map["min_M_mummup"]];
      }
    }

    if (user->switch_value[user->switch_map["M_leplep"]] == 1 &&
	esi->pda[esi->observed_object["lep"]].n_partonlevel >= 4){
      if (minElep < 2 * user->cut_value[user->cut_map["min_M_leplep"]]){
	minElep = 2 * user->cut_value[user->cut_map["min_M_leplep"]];
      }
    }

  }
  logger << LOG_DEBUG << "minElep = " << minElep << endl;

  double minEwp = 0.;
  vector<double> minEawp(esi->pda[esi->observed_object["wp"]].n_partonlevel, 0.);
  for (int i = 0; i < esi->pda[esi->observed_object["wp"]].n_observed_min; i++){minEawp[i] = sqrt(M2[24] + pow(esi->pda[esi->observed_object["wp"]].define_pT, 2));}
  for (int i = esi->pda[esi->observed_object["wp"]].n_observed_min; i < esi->pda[esi->observed_object["wp"]].n_partonlevel; i++){minEawp[i] = M[24];}
  for (int i = 0; i < esi->pda[esi->observed_object["wp"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEawp[" << i << "] = " << minEawp[i] << endl;}
  minEwp = accumulate(minEawp.begin(), minEawp.end(), 0.);
  logger << LOG_DEBUG << "minEwp = " << minEwp << endl;

  double minEwm = 0.;
  vector<double> minEawm(esi->pda[esi->observed_object["wm"]].n_partonlevel, 0.);
  for (int i = 0; i < esi->pda[esi->observed_object["wm"]].n_observed_min; i++){minEawm[i] = sqrt(M2[24] + pow(esi->pda[esi->observed_object["wm"]].define_pT, 2));}
  for (int i = esi->pda[esi->observed_object["wm"]].n_observed_min; i < esi->pda[esi->observed_object["wm"]].n_partonlevel; i++){minEawm[i] = M[24];}
  for (int i = 0; i < esi->pda[esi->observed_object["wm"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEawm[" << i << "] = " << minEawm[i] << endl;}
  minEwm = accumulate(minEawm.begin(), minEawm.end(), 0.);
  logger << LOG_DEBUG << "minEwm = " << minEwm << endl;


  // !!! simplified so far !!!
  // no individual cuts on different leptons

  double minEphoton = 0.;
  vector<double> minEaphoton(esi->pda[esi->observed_object["photon"]].n_partonlevel, 0.);
  logger << LOG_DEBUG << "before minEphoton = " << minEphoton << endl;
//  logger << LOG_DEBUG << "osi_observed_object[photon] = " << osi_observed_object["photon"] << endl;
//  logger << LOG_DEBUG << "osi_n_observed_min.n_observed_min.size() = " <<  osi_n_observed_min.n_observed_min.size() << endl;
//  logger << LOG_DEBUG << "osi_n_partonlevel.n_partonlevel.size() = " << .n_partonlevel osi_n_partonlevel.size() << endl;
  logger << LOG_DEBUG << "minEaphoton.size() = " << minEaphoton.size() << endl;
  logger << LOG_DEBUG << "esi->pda[photon]].n_observed_min = " << esi->pda[esi->observed_object["photon"]].n_observed_min << endl;
  logger << LOG_DEBUG << "esi->pda[photon]].define_pT = " << esi->pda[esi->observed_object["photon"]].define_pT << endl;

  //  for (int i = 0; i < esi->pda[esi->observed_object["photon"]].n_observed_min; i++){
  for (int i = 0; i < esi->pda[esi->observed_object["photon"]].n_observed_min; i++){minEaphoton[i] = esi->pda[esi->observed_object["photon"]].define_pT;}
  for (int i = 0; i < esi->pda[esi->observed_object["photon"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEaphoton[" << i << "] = " << minEaphoton[i] << endl;}
  //  for (int i = 0; i < esi->pda[esi->observed_object["photon"]].n_partonlevel; i++){minEaphoton[i] = esi->pda[esi->observed_object["photon"]].define_pT;}
  logger << LOG_DEBUG << "after minEphoton = " << minEphoton << endl;
  //  for (int i = 0; i < esi->pda[esi->observed_object["photon"]].n_partonlevel; i++){logger << LOG_DEBUG << "minEaphoton[" << i << "] = " << minEaphoton[i] << endl;}
  logger << LOG_DEBUG << "before minEphoton = " << minEphoton << endl;
  minEphoton = accumulate(minEaphoton.begin(), minEaphoton.end(), 0.);
  logger << LOG_DEBUG << "minEphoton = " << minEphoton << endl;

  double minEmissing = 0.;
  minEmissing = esi->pda[esi->observed_object["missing"]].define_pT;
  /*
  double add_masses = 0.;
  for (int i = 3; i < csi->type_parton[0].size(); i++){
    if (abs(csi->type_parton[0][i]) == 6){add_masses += M[6];}
    else if (abs(csi->type_parton[0][i]) == 23){add_masses += M[23];}
    //    else if (abs(csi->type_parton[0][i]) == 24){add_masses += M[24];}
    else if (abs(csi->type_parton[0][i]) == 25){add_masses += M[25];}
  }
  */

  //  logger << LOG_DEBUG << "add_masses + minEjet + minElep + minEphoton + cut_pT_miss = " << add_masses + minEjet + minElep + minEphoton + cut_pT_miss << endl;
  //  double sqrts_min_tau_0 = add_masses + minEjet + minElep + minEphoton + minEwp + minEwm + cut_pT_miss;

  logger << LOG_DEBUG << "minEjet = " << minEjet << endl;
  logger << LOG_DEBUG << "minEnua = " << minEnua << endl;
  logger << LOG_DEBUG << "minElep = " << minElep << endl;
  logger << LOG_DEBUG << "minEphoton = " << minEphoton << endl;
  logger << LOG_DEBUG << "minEwp = " << minEwp << endl;
  logger << LOG_DEBUG << "minEwm = " << minEwm << endl;
  logger << LOG_DEBUG << "minEmissing = " << minEmissing << endl;
  double sqrts_min_tau_0 = minEjet + minEnua + minElep + minEphoton + minEwp + minEwm + minEmissing;

  double add_masses = 0.;
  for (int i_p = 3; i_p < csi->type_parton[0].size(); i_p++){add_masses += M[abs(csi->type_parton[0][i_p])];}
  logger << LOG_DEBUG << "add_masses = " << add_masses << endl;
  //      for (int i_p = 3; i_p < csi->type_parton[0].size(); i_p++){add_masses += csi->mass_parton[0][i_p];}
  if (sqrts_min_tau_0 < add_masses){
    sqrts_min_tau_0 = add_masses;
  }

  logger << LOG_DEBUG << "sqrts_min_tau_0 = " << sqrts_min_tau_0 << endl;
  if (user->switch_value[user->switch_map["HT_all"]] == 1){
    if (sqrts_min_tau_0 < user->cut_value[user->cut_map["min_HT_all"]]){
      sqrts_min_tau_0 = user->cut_value[user->cut_map["min_HT_all"]];
    }
  }

  if (user->switch_value[user->switch_map["HT_jet"]] == 1){
    if (sqrts_min_tau_0 < user->cut_value[user->cut_map["min_HT_jet"]]){
      sqrts_min_tau_0 = user->cut_value[user->cut_map["min_HT_jet"]];
    }
  }

  if (user->switch_value[user->switch_map["pT_jet_1st"]] == 1){
    if (sqrts_min_tau_0 < user->cut_value[user->cut_map["min_pT_jet_1st"]]){
      sqrts_min_tau_0 = 2 * user->cut_value[user->cut_map["min_pT_jet_1st"]];
    }
  }

  if (user->switch_value[user->switch_map["pT_w"]] == 1){
    if (sqrts_min_tau_0 < user->cut_value[user->cut_map["min_pT_w"]]){
      sqrts_min_tau_0 = 2 * user->cut_value[user->cut_map["min_pT_w"]];
    }
  }

  if (user->switch_value[user->switch_map["optimize_sqrts_min"]] == 1){
    if (sqrts_min_tau_0 < user->double_value[user->double_map["optimize_sqrts_min"]]){
      sqrts_min_tau_0 = user->double_value[user->double_map["optimize_sqrts_min"]];
    }
  }

  logger << LOG_DEBUG << "sqrts_min_tau_0 = " << sqrts_min_tau_0 << "   (after HT-cut)" << endl;


  /*
  if (sqrtsmin_opt[0][smin_opt[0].size() - 4] > sqrts_min_tau_0){
    sqrts_min_tau_0 = sqrtsmin_opt[0][smin_opt[0].size() - 4];
  }

  logger << LOG_DEBUG << "sqrts_min_tau_0 = " << sqrts_min_tau_0 << "   (after re-using phase space cuts from optimization files)" << endl;
  */

  tau_0 = pow((sqrts_min_tau_0) / (2 * E), 2);

  if (tau_0 == 0.){tau_0 = 1.e-07;}
  //  if (tau_0 == 0.){tau_0 = 1.E-06;}

  tau_0_s_had = tau_0 * s_had;

  logger << LOG_INFO << "tau_0 = " << tau_0 << "   tau_0_s_had = " << tau_0_s_had << "   -> sqrt(s^_min) = " << 2 * sqrt(tau_0) * E << " (tau_0)" << " = " << sqrt(tau_0_s_had) << " (tau_0_s_had)" << endl;

  logger << LOG_DEBUG << "finished" << endl;
}



// !!! should be completely worked out anew and shifted elsewhere !!!
void phasespace_set::initialization_minimum_phasespacecut(){
  Logger logger("phasespace_set::initialization_minimum_phasespacecut");
  logger << LOG_DEBUG << "started" << endl;

  mapping_cut_pT.resize(26, 0.);

  if (!csi->class_contribution_CS_real){
    // only applied in configuration where each parton must become a jet:
    if (esi->pda[esi->observed_object["jet"]].n_partonlevel == esi->pda[esi->observed_object["jet"]].n_observed_min){
      double temp_pT2_jet = pow(esi->pda[esi->observed_object["jet"]].define_pT, 2);
      mapping_cut_pT[0] = temp_pT2_jet;
      mapping_cut_pT[1] = temp_pT2_jet;
      mapping_cut_pT[2] = temp_pT2_jet;
      mapping_cut_pT[3] = temp_pT2_jet;
      mapping_cut_pT[4] = temp_pT2_jet;
      mapping_cut_pT[5] = temp_pT2_jet;
    }
    if (esi->pda[esi->observed_object["ljet"]].n_partonlevel == esi->pda[esi->observed_object["ljet"]].n_observed_min){
      double temp_pT2_ljet = pow( esi->pda[esi->observed_object["ljet"]].define_pT, 2);
      if (temp_pT2_ljet > mapping_cut_pT[0]){mapping_cut_pT[0] = temp_pT2_ljet;}
      if (temp_pT2_ljet > mapping_cut_pT[0]){mapping_cut_pT[1] = temp_pT2_ljet;}
      if (temp_pT2_ljet > mapping_cut_pT[0]){mapping_cut_pT[2] = temp_pT2_ljet;}
      if (temp_pT2_ljet > mapping_cut_pT[0]){mapping_cut_pT[3] = temp_pT2_ljet;}
      if (temp_pT2_ljet > mapping_cut_pT[0]){mapping_cut_pT[4] = temp_pT2_ljet;}
    }
    if (esi->pda[esi->observed_object["bjet"]].n_partonlevel == esi->pda[esi->observed_object["bjet"]].n_observed_min){
      double temp_pT2_bjet = pow(esi->pda[esi->observed_object["bjet"]].define_pT, 2);
      if (temp_pT2_bjet > mapping_cut_pT[5]){mapping_cut_pT[5] = temp_pT2_bjet;}
    }
  }
  if (!csi->class_contribution_CS_real){
    if (esi->pda[esi->observed_object["e"]].n_partonlevel == esi->pda[esi->observed_object["e"]].n_observed_min){mapping_cut_pT[11] = pow(esi->pda[esi->observed_object["e"]].define_pT, 2);}
    if (esi->pda[esi->observed_object["mu"]].n_partonlevel == esi->pda[esi->observed_object["mu"]].n_observed_min){mapping_cut_pT[13] = pow(esi->pda[esi->observed_object["mu"]].define_pT, 2);}
    if (esi->pda[esi->observed_object["tau"]].n_partonlevel == esi->pda[esi->observed_object["tau"]].n_observed_min){mapping_cut_pT[15] = pow(esi->pda[esi->observed_object["tau"]].define_pT, 2);}
    if (esi->pda[esi->observed_object["photon"]].n_partonlevel == esi->pda[esi->observed_object["photon"]].n_observed_min){mapping_cut_pT[22] = pow(esi->pda[esi->observed_object["photon"]].define_pT, 2);}
    if (esi->n_parton_nu == 1){
      mapping_cut_pT[12] = pow(esi->pda[esi->observed_object["missing"]].define_pT, 2);
      mapping_cut_pT[14] = pow(esi->pda[esi->observed_object["missing"]].define_pT, 2);
      mapping_cut_pT[16] = pow(esi->pda[esi->observed_object["missing"]].define_pT, 2);
    }
  }

  for (int i = 0; i < mapping_cut_pT.size(); i++){logger << LOG_DEBUG << "mapping_cut_pT[" << setw(2) << i << "] = " << mapping_cut_pT[i] << endl;}

  /////////////////////////////////////////////////////
  //  minimum phasespace-cut determination finished  //
  /////////////////////////////////////////////////////

  logger << LOG_DEBUG << "finished" << endl;
}

