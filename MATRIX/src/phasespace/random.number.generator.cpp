#include "header.hpp"

random_number_generator::random_number_generator(){}

random_number_generator::random_number_generator(int _n_random_per_psp, int _n_shift_run, int _switch_external_random_generator){
  static Logger logger("random_number_generator::random_number_generator");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "_n_random_per_psp = " << _n_random_per_psp << endl;

  psi = NULL;
  //  if (!(psi->switch_off_random_generator)){initialization(_n_random_per_psp, _n_shift_run);}
  initialization(_n_random_per_psp, _n_shift_run, _switch_external_random_generator);

  logger << LOG_DEBUG << "n_random_per_psp = " << n_random_per_psp << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

random_number_generator::~random_number_generator(){
  static Logger logger("random_number_generator::~random_number_generator");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  delete psi;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::initialization(int _n_random_per_psp, int _n_shift_run, int _switch_external_random_generator){
  Logger logger("random_number_generator::initialization");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG << "_switch_external_random_generator = " << _switch_external_random_generator << endl;
  logger << LOG_DEBUG << "_n_random_per_psp = " << _n_random_per_psp << endl;
  logger << LOG_DEBUG << "_n_shift_run = " << _n_shift_run << endl;

  //////////////////////////////////////////////////
  //  random-number initialization determination  //
  //////////////////////////////////////////////////

  n_random_per_psp = _n_random_per_psp;
  random_number.resize(_n_random_per_psp);

  n_shift_run = _n_shift_run;

  switch_external_random_generator = _switch_external_random_generator;

  random_sheet = n_shift_run / n_random_per_psp;
  random_counter = 0;

  int max_n_prime_number = (random_sheet + 1) * 3;
  logger << LOG_DEBUG << "max_n_prime_number = " << max_n_prime_number << endl;
  vector<int> prime_number(1, 2);
  logger << LOG_DEBUG << "random_sheet no. 0" << endl;
  logger << LOG_DEBUG << "prime_number[" << setw(4) << prime_number.size() - 1 << "] = " << setw(10) << prime_number[prime_number.size() - 1] << "   sqrt = " << sqrt(prime_number[prime_number.size() - 1]) << endl;
  int number = 1;
  while (prime_number.size() < max_n_prime_number){
    number += 2;
    double max_number = sqrt(number);
    int flag = 0;
    for (int i_n = 0; i_n < prime_number.size(); i_n++){

      if (prime_number[i_n] > max_number){break;}
      if (number % prime_number[i_n] == 0){flag = 1; break;}
    }
    if (!flag){
      if (prime_number.size() % 3 == 0){logger << LOG_DEBUG << "random_sheet no. " << prime_number.size() / 3 << endl;}
      prime_number.push_back(number);
      logger << LOG_DEBUG << "prime_number[" << setw(4) << prime_number.size() - 1 << "] = " << setw(10) << prime_number[prime_number.size() - 1] << "   sqrt = " << sqrt(prime_number[prime_number.size() - 1]) << endl;
    }
  }

  logger << LOG_DEBUG << "selected random_sheet: " << random_sheet << endl;

  n_shift_on_sheet = n_shift_run % n_random_per_psp;

  logger << LOG_DEBUG << "n_random_per_psp = " << n_random_per_psp << endl;
  logger << LOG_DEBUG << "n_shift_run      = " << n_shift_run << endl;
  logger << LOG_DEBUG << "random_sheet     = " << random_sheet << endl;
  logger << LOG_DEBUG << "n_shift_on_sheet = " << n_shift_on_sheet << endl;

  random_generator_set.resize(3);
  random_generator_set[0] = sqrt(prime_number[prime_number.size() - 3]);
  random_generator_set[1] = sqrt(prime_number[prime_number.size() - 2]);
  random_generator_set[2] = sqrt(prime_number[prime_number.size() - 1]);
  for (int i_z = 0; i_z < 3; i_z++){
    random_generator_set[i_z] = random_generator_set[i_z] - int(random_generator_set[i_z]);
  }

  for (int i = 0; i < n_shift_on_sheet; i++){get_random_generator_set();}
  //  double x_shift = 0.;
  //  vector<double> random_shift(n_shift_on_sheet);
  //  for (int i = 0; i < n_shift_on_sheet; i++){x_shift = ran(sran);}
  //  logger << LOG_DEBUG << "x_shift" << " = " << setprecision(16) << setw(25) << left << showpoint << x << endl;

  //  proceeding_save();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::initialization_manipulation(int n_manipulation, random_number_generator & rng){
  Logger logger("phasespace_set::initialization_random_per_psp_number_generator");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  random_generator_set = rng.random_generator_set;

  random_generator_set[0] /= 2;
  random_generator_set[1] /= 3;
  random_generator_set[2] /= 4;

  random_generator_set[0] = (random_generator_set[0]) / 4 * (1 + cos(n_manipulation + 2));
  random_generator_set[1] = (random_generator_set[1]) / 4 * (1 + cos(n_manipulation + 2));
  random_generator_set[2] = (random_generator_set[2]) / 4 * (1 + cos(n_manipulation + 2));

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void random_number_generator::get_random_number_set(){
  Logger logger("random_number_generator::get_random_number_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG_VERBOSE << "random_number.size() = " << random_number.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "Used random numbers: " << random_counter << " out of " << n_random_per_psp << endl;

  random_counter = 0;

  /*
  // Seems to be only psi dependence here - and creates only output !!!
  psi->random_manager.output_used();
  */

  logger << LOG_DEBUG_VERBOSE << "n_random_per_psp = " << n_random_per_psp << endl;
  for (int i_r = 0; i_r < n_random_per_psp; i_r++){
    get_random_generator_set();
    ///    logger << LOG_DEBUG << setw(2) << right << i_r << "   " << output_random_generator_set() << endl;
    random_number[i_r] = random_generator_set[0];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::get_random_number_set_external(vector<double> & _random_number){
  Logger logger("random_number_generator::get_set_random_number_external");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (!switch_external_random_generator){logger << LOG_WARN << "Should not be called if internal random number generator is used." << endl;}

  random_number = _random_number;

  if (n_random_per_psp != _random_number.size()){
    logger << LOG_WARN << "n_random_per_psp = " << n_random_per_psp << " != " << _random_number.size() << " = _random_number.size()" << endl;
    n_random_per_psp = _random_number.size();
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::get_random_generator_set(){
  Logger logger("random_number_generator::get_random_generator_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  random_generator_set[0] = fmod(random_generator_set[0] + random_generator_set[1] + random_generator_set[2], 1.);
  random_generator_set[1] = fmod(random_generator_set[0] + random_generator_set[1] + random_generator_set[2], 1.);
  random_generator_set[2] = fmod(random_generator_set[0] + random_generator_set[1] + random_generator_set[2], 1.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


vector<double> random_number_generator::access_random_number(int n_requested_random_number){
  Logger logger("random_number_generator::access_random_number (vector<int>)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG_VERBOSE << "n_requested_random_number = " << n_requested_random_number << endl;
  logger << LOG_DEBUG_VERBOSE << "n_random_per_psp = " << n_random_per_psp << endl;
  logger << LOG_DEBUG_VERBOSE << "random_counter = " << random_counter << endl;
  //  logger << LOG_DEBUG_VERBOSE << "requested_random_number.size() = " << requested_random_number.size() << endl;



 if (random_counter + n_requested_random_number > n_random_per_psp){
    logger << LOG_FATAL << "Not enough random numbers per phase-space point requested!" << endl;
    exit(1);
  }
  vector<double> requested_random_number(n_requested_random_number);
  for (int i_r = 0; i_r < n_requested_random_number; i_r++){
    requested_random_number[i_r] = random_number[random_counter++];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return requested_random_number;
}

double random_number_generator::access_random_number(){
  Logger logger("random_number_generator::access_random_number (int)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  /*
  if (psi != NULL){  }
  */
  if (random_counter + 1 > n_random_per_psp){
    logger << LOG_FATAL << "n_random_per_psp = " << n_random_per_psp << "   random_counter = " << random_counter << endl;
    logger << LOG_FATAL << "Not enough random numbers per phase-space point requested!" << endl;
    exit(1);
  }

  logger << LOG_DEBUG_VERBOSE << "finished  --  return " << random_number[random_counter] << endl;
  return random_number[random_counter++];
}


string random_number_generator::output_random_generator_set(){
  Logger logger("random_number_generator::output_random_generator_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  stringstream output;
  output << "random_generator_set = ";
  for (int i_r = 0; i_r < 3; i_r++){
    output << setprecision(16) << setw(25) << left << random_generator_set[i_r];
    if (i_r < 2){output << "   ";}
  }
  //  output << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return output.str();
}

string random_number_generator::output_random_number_set(){
  Logger logger("random_number_generator::output_random_number_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  stringstream output;
  for (int i_r = 0; i_r < n_random_per_psp; i_r++){
    output << "random_number[" << setw(2) << right << i_r << "] = " << setprecision(16) << setw(25) << left << random_number[i_r] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return output.str();
}



void random_number_generator::proceeding_out(ofstream &out_proceeding){
  static Logger logger("random_number_generator::proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < 3; i_r++){
    // Looks a bit doubled... !!!
    //    out_proceeding << double2hexastr(save_random_generator_set[i_r]) << endl;
    out_proceeding << double2hexastr(random_generator_set[i_r]) << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::proceeding_in(int &proc, vector<string> &readin){
  static Logger logger("random_number_generator::proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < 3; i_r++){
    random_generator_set[i_r] = hexastr2double(readin[proc + i_r]);
  }
  proc += 3;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void random_number_generator::check_proceeding_in(int &int_end, int &temp_check_size, vector<string> &readin){
  static Logger logger("random_number_generator::check_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "int_end = " << int_end << endl;
  logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << endl;
  logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;

  //  random_generator_set(3)
  temp_check_size += 3;

  if (temp_check_size > readin.size()){
    int_end = 2;
    logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
    return;
  }
  else {
    logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void random_number_generator::proceeding_save(){
  static Logger logger("random_number_generator::proceeding_save");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  save_random_generator_set = random_generator_set;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/
