#include "header.hpp"
//#include "definitions.phasespace.set.cxx"

// Should be shifted to contribution_set !!!
void QCD_selection_phasespace_singularity(vector<vector<double> > & singular_region, vector<vector<string> > & singular_region_name, vector<vector<int> > & list_singular_regions, phasespace_set & psi){
  static Logger logger("QCD_selection_phasespace_singularity");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static vector<int> type_parton = psi.csi->type_parton[0];

  singular_region.resize(type_parton.size(), vector<double> (type_parton.size()));
  singular_region_name.resize(type_parton.size(), vector<string> (type_parton.size()));

  vector<string> pa_name(type_parton.size(), "");
  if (type_parton[1] > -10 && type_parton[1] < 10){pa_name[1] = "a";}
  if (type_parton[2] > -10 && type_parton[2] < 10){pa_name[2] = "b";}
  /*
  int count = 0;
  vector<string> alphabet(type_parton.size() - 3, "");
  for (int i_p = 0; i_p < alphabet.size(); i_p++){alphabet[i_p] = char(105 + i_p);}
  for (int i_p = 3; i_p < pa_name.size(); i_p++){if (type_parton[i_p] > -10 && type_parton[i_p] < 10){pa_name[i_p] = alphabet[count++];}}
  */
  for (int i_p = 3; i_p < pa_name.size(); i_p++){if (type_parton[i_p] >= -10 && type_parton[i_p] <= 10){pa_name[i_p] = char(105 + i_p - 3);}}
  logger << LOG_DEBUG << "QCD phasespace singularity selection from QCD dipole candidates started." << endl;

  for (int i_p = 0; i_p < type_parton.size(); i_p++){
    for (int j_p = i_p + 1; j_p < type_parton.size(); j_p++){
      if (j_p < 3){continue;}
      int new_region = 0;

      if (i_p < 3){
	if (type_parton[i_p] == type_parton[j_p] ||
	    (type_parton[i_p] == 0 && abs(type_parton[j_p]) < 10) ||
	    (type_parton[j_p] == 0 && abs(type_parton[i_p]) < 10)){
	  new_region = 1;
	}
      }
      else {
	if (type_parton[i_p] == type_parton[j_p] ||
	    (type_parton[i_p] == type_parton[j_p] && type_parton[i_p] == 0 && abs(type_parton[j_p]) < 10) ||
	    (type_parton[j_p] == type_parton[i_p] && type_parton[j_p] == 0 && abs(type_parton[i_p]) < 10)){
	  new_region = 1;
	}
      }
      if (new_region == 1){
	vector<int> temp_singularity(2);
	temp_singularity[0] = i_p;
	temp_singularity[1] = j_p;
	sort(temp_singularity.begin(), temp_singularity.end());
	list_singular_regions.push_back(temp_singularity);
	singular_region_name[temp_singularity[0]][temp_singularity[1]] = "p" + pa_name[temp_singularity[0]] + "." + "p" + pa_name[temp_singularity[1]];
       }
    }
  }

  for (int i = 0; i < list_singular_regions.size(); i++){
    logger << LOG_INFO << list_singular_regions[i][0] << " " << list_singular_regions[i][1] << "   " << singular_region_name[list_singular_regions[i][0]][list_singular_regions[i][1]] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void phasespace_set::initialization_fake_dipole_mapping_RT(){
  Logger logger("phasespace_set::initialization_fake_dipole_mapping_RT");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  csi->dipole.push_back(dipole_set("fakeR", csi, 0, 0));
  csi->determination_dipole_QCD();
  MC_x_dipole_mapping.resize(csi->dipole.size());
  for (int j_a = 1; j_a < csi->dipole.size(); j_a++){
    ac_tau_psp_dipole(j_a, MC_x_dipole_mapping[j_a]);
  }

  for (int j_a = 1; j_a < csi->dipole.size(); j_a++){
    for (int i_m = 0; i_m < MC_x_dipole_mapping[j_a].size(); i_m++){
      int flag = tau_MC_map.size();
      for (int j_m = 0; j_m < tau_MC_map.size(); j_m++){
	if (MC_x_dipole_mapping[j_a][i_m] == tau_MC_map[j_m]){flag = j_m; break;}
      }
      if (flag == tau_MC_map.size()){tau_MC_map.push_back(MC_x_dipole_mapping[j_a][i_m]);}
    }
  }

  /*
  fake_psi->csi->dipole.clear();
  fake_psi->csi->dipole.push_back(dipole_set("fakeR", fake_psi->csi, 0, 0));
  fake_psi->csi->determination_dipole_QCD();
  fake_psi->MC_x_dipole_mapping.resize(fake_psi->csi->dipole.size());
  for (int j_a = 1; j_a < fake_psi->csi->dipole.size(); j_a++){
    fake_psi->ac_tau_psp_dipole(j_a, fake_psi->MC_x_dipole_mapping[j_a]);
  }
  for (int j_a = 1; j_a < fake_psi->csi->dipole.size(); j_a++){
    for (int i_m = 0; i_m < fake_psi->MC_x_dipole_mapping[j_a].size(); i_m++){
      int flag = MC_x_dipole_mapping[i_a].size();
      for (int j_m = 0; j_m < MC_x_dipole_mapping[i_a].size(); j_m++){
	if (fake_psi->MC_x_dipole_mapping[j_a][i_m] == MC_x_dipole_mapping[i_a][j_m]){flag = j_m; break;}
      }
      if (flag == MC_x_dipole_mapping[i_a].size()){MC_x_dipole_mapping[i_a].push_back(fake_psi->MC_x_dipole_mapping[j_a][i_m]);}
    }
  }
  */
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



//vector<dipole_set> & dipole, phasespace_set & psi, call_generic & generic
void phasespace_set::initialization_fake_dipole_mapping_RRA(){
  Logger logger("phasespace_set::initialization_fake_dipole_mapping_RRA");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  int n_dipoles = dipole.size();
  logger << LOG_INFO << "fake_dipole to improve initial-state mappings started..." << endl;

  //    logger << LOG_INFO << "n_dipoles = " << n_dipoles << endl;
  logger << LOG_INFO << "REAL *RA_DIPOLE: csi->dipole.size() = " << csi->dipole.size() << endl;

  for (int i_a = 0; i_a < csi->dipole.size(); i_a++){
    logger << LOG_INFO << "csi->phasespace_order_alpha_s[" << i_a << "] = " << csi->phasespace_order_alpha_s[i_a] << "   csi->phasespace_order_alpha_e[" << i_a << "] = " << csi->phasespace_order_alpha_e[i_a] << endl;
  }

  ///  vector<vector<dipole_set> > fake_dipole(csi->dipole.size());
  ///  vector<vector<dipole_set> > fake_dipole_candidate(csi->dipole.size());

  //  fake_psi->csi = new contribution_set();

  fake_psi->csi->type_contribution = "RA";

  for (int i_a = 1; i_a < csi->dipole.size(); i_a++){

    //      phasespace_set test_fake_psi = *this;
    //      phasespace_set fake_psi;// = psi;
    //      contribution_set fake_csi;// = psi->csi;
    //      fake_psi->csi = &fake_csi;

    //  Setting  dipole i_a  as "basic process" of  fake_psi->csi :

    fake_psi->csi->no_process_parton.clear();
    fake_psi->csi->no_process_parton.resize(1, csi->no_process_parton[i_a]);
    //      fake_psi->csi->no_process_parton.resize(1, 0);
    //      fake_psi->no_prc.resize(1, 0);
    fake_psi->csi->swap_parton.clear();
    fake_psi->csi->swap_parton.resize(1, csi->swap_parton[i_a]);
    //      fake_psi->csi->swap_parton.resize(1, vector<int> (csi->swap_parton.size() - 1));
    //      fake_psi->o_prc.resize(1, vector<int> (o_prc.size() - 1));

    fake_psi->csi->phasespace_order_alpha_s.clear();
    fake_psi->csi->phasespace_order_alpha_s.resize(1, csi->phasespace_order_alpha_s[i_a]);
    fake_psi->csi->phasespace_order_alpha_e.clear();
    fake_psi->csi->phasespace_order_alpha_e.resize(1, csi->phasespace_order_alpha_e[i_a]);
    fake_psi->csi->phasespace_order_interference.clear();
    fake_psi->csi->phasespace_order_interference.resize(1, csi->phasespace_order_interference[i_a]);

    fake_psi->csi->type_parton.clear();
    fake_psi->csi->type_parton.resize(1, csi->type_parton[i_a]);
    //      fake_psi->csi->type_parton.resize(1, csi->dipole[i_a].type_parton());
    //      fake_psi->csi->type_parton.resize(1, vector<int> ());
    //      fake_psi->csi->type_parton[0] = csi->dipole[i_a].type_parton();

    fake_psi->csi->basic_type_parton.clear();
    fake_psi->csi->basic_type_parton.resize(1, csi->basic_type_parton[i_a]);
    //      fake_psi->csi->basic_type_parton.resize(1, csi->dipole[i_a].basic_type_parton());
    //      fake_psi->csi->basic_type_parton.resize(1, vector<int> ());
    //      fake_psi->csi->basic_type_parton[0] = csi->dipole[i_a].basic_type_parton();


    fake_psi->csi->symmetry_id_factor.clear();
    fake_psi->csi->symmetry_id_factor.resize(1, csi->symmetry_id_factor[i_a]);

    // New implementation
    // to avoid call_generic...

    //      logger << LOG_INFO << "csi->dipole.size() = " << fake_psi->csi->dipole.size() << endl;
    fake_psi->csi->dipole.clear();
    fake_psi->csi->dipole.push_back(dipole_set("fakeR", fake_psi->csi, 0, 0));
    logger << LOG_INFO << "i_a = " << i_a << "   csi->dipole.size() = " << fake_psi->csi->dipole.size() << endl;

    logger << LOG_INFO << "before: fake_psi->csi->type_parton.size() = " << fake_psi->csi->type_parton.size() << endl;
    logger << LOG_INFO << "before: fake_psi->csi->dipole.size() = " << fake_psi->csi->dipole.size() << endl;
    fake_psi->csi->determination_dipole_QCD();
    logger << LOG_INFO << "after: fake_psi->csi->type_parton.size() = " << fake_psi->csi->type_parton.size() << endl;
    logger << LOG_INFO << "after: fake_psi->csi->dipole.size() = " << fake_psi->csi->dipole.size() << endl;

      stringstream temp;
      for (int i_p = 1; i_p < csi->type_parton[i_a].size(); i_p++){
	temp << setw(5) << csi->type_parton[i_a][i_p];
      }
      logger << LOG_INFO << setw(25) << right << "csi->dipole[" << setw(2) << i_a << "].name() = " << left << setw(15) << csi->dipole[i_a].name() << "   " << temp.str() << endl;

      logger << LOG_INFO << "fake_psi->csi->type_parton.size() = " << fake_psi->csi->type_parton.size() << endl;
       for (int j_a = 0; j_a < fake_psi->csi->type_parton.size(); j_a++){
	stringstream temp;
	for (int i_p = 1; i_p < fake_psi->csi->type_parton[j_a].size(); i_p++){
	  temp << setw(5) << fake_psi->csi->type_parton[j_a][i_p];
	}
	logger << LOG_INFO << setw(25) << right << "fake_psi->csi->dipole[" << setw(2) << j_a << "].name() = " << left << setw(15) << fake_psi->csi->dipole[j_a].name() << "   " << temp.str() << endl;
      }

      //  Setting  dipole i_a  as "basic process" of  fake_psi :
       logger << LOG_INFO << "MC_n_channel_phasespace.size() = " << MC_n_channel_phasespace.size() << endl;
      fake_psi->MC_n_channel_phasespace.resize(1, MC_n_channel_phasespace[i_a]);
      fake_psi->MC_sum_channel_phasespace.resize(1, MC_sum_channel_phasespace[i_a]);
      fake_psi->M = M;
      logger << LOG_INFO << "fake_psi->csi->type_parton.size() = " << fake_psi->csi->type_parton.size() << endl;

      //      logger << LOG_INFO << "determination_MCchannels_real(" << setw(2) << i_a << ") = " << setw(4) << determination_MCchannels_real(i_a) << "   test_fake_psi->determination_MCchannels_real(" << setw(2) << i_a << ") = " << setw(4) << fake_psi->determination_MCchannels_real(i_a) << endl;
      /*
      fake_psi->csi->no_process_parton.erase(fake_psi->csi->no_process_parton.begin() + 1, fake_psi->csi->no_process_parton.end());
      fake_psi->csi->no_process_parton.erase(fake_psi->csi->no_process_parton.begin() + 1, fake_psi->csi->no_process_parton.end());
      fake_psi->csi->swap_parton.erase(fake_psi->csi->swap_parton.begin() + 1, fake_psi->csi->swap_parton.end());
      fake_psi->csi->swap_parton.erase(fake_psi->csi->swap_parton.begin() + 1, fake_psi->csi->swap_parton.end());
      fake_psi->csi->phasespace_order_alpha_s.erase(fake_psi->csi->phasespace_order_alpha_s.begin() + 1, fake_psi->csi->phasespace_order_alpha_s.end());
      fake_psi->csi->phasespace_order_alpha_e.erase(fake_psi->csi->phasespace_order_alpha_e.begin() + 1, fake_psi->csi->phasespace_order_alpha_e.end());
      fake_psi->csi->phasespace_order_interference.erase(fake_psi->csi->phasespace_order_interference.begin() + 1, fake_psi->csi->phasespace_order_interference.end());
      fake_psi->csi->phasespace_order_alpha_s[0] = csi->phasespace_order_alpha_s[i_a];
      fake_psi->csi->phasespace_order_alpha_e[0] = csi->phasespace_order_alpha_e[i_a];
      fake_psi->csi->phasespace_order_interference[0] = csi->phasespace_order_interference[i_a];
*/
      /*
      fake_psi->MC_n_channel_phasespace.erase(fake_psi->MC_n_channel_phasespace.begin() + 1, fake_psi->MC_n_channel_phasespace.end());
      fake_psi->MC_sum_channel_phasespace.erase(fake_psi->MC_sum_channel_phasespace.begin() + 1, fake_psi->MC_sum_channel_phasespace.end());
      *//*
      logger << LOG_DEBUG_VERBOSE << "csi->phasespace_order_alpha_s[" << i_a << "] - 1 = " << csi->phasespace_order_alpha_s[i_a] - 1 << endl;
      logger << LOG_DEBUG_VERBOSE << "csi->phasespace_order_alpha_e[" << i_a << "]     = " << csi->phasespace_order_alpha_e[i_a] << endl;
      logger << LOG_DEBUG_VERBOSE << "fake_psi->csi->phasespace_order_alpha_s[" << 0 << "] = " << fake_psi->csi->phasespace_order_alpha_s[0] << endl;
      logger << LOG_DEBUG_VERBOSE << "fake_psi->csi->phasespace_order_alpha_e[" << 0 << "] = " << fake_psi->csi->phasespace_order_alpha_e[0] << endl;
      logger << LOG_DEBUG_VERBOSE << "fake_psi->MC_n_channel_phasespace.size() = " << fake_psi->MC_n_channel_phasespace.size() << endl;
      logger << LOG_DEBUG_VERBOSE << "fake_psi->MC_sum_channel_phasespace.size() = " << fake_psi->MC_sum_channel_phasespace.size() << endl;


      vector<int> fake_type_parton = csi->dipole[i_a].type_parton();
      vector<int> fake_basic_type_parton = csi->dipole[i_a].basic_type_parton();

      fake_dipole[i_a].push_back(dipole_set(csi->dipole[i_a].name(), fake_type_parton, fake_basic_type_parton, csi->symmetry_factor, csi->no_process_parton[i_a], csi->swap_parton[i_a], 0));
      //      fake_dipole[i_a].push_back(dipole_set(csi->dipole[i_a].name(), fake_type_parton, fake_basic_type_parton, csi->symmetry_factor, csi->no_process_parton[i_a], csi->swap_parton[i_a], 0));

      QCD_determine_dipoles(fake_dipole_candidate[i_a], fake_type_parton, fake_basic_type_parton);
      logger << LOG_DEBUG_VERBOSE << "REAL *RA_DIPOLE " << i_a << endl;



      QCD_selection_fake_dipoles(fake_dipole[i_a], fake_dipole_candidate[i_a], fake_psi->csi->phasespace_order_alpha_s[0] - 1, fake_psi->csi->phasespace_order_alpha_e[0], csi->phasespace_order_interference[i_a], fake_psi->csi->singular_region, fake_psi->csi->singular_region_name, fake_psi->csi->singular_region_list, *fake_psi, *generic, generic->determination_no_subprocess_doubledipole, generic->determination_MCchannels_doubledipole);



      //      QCD_selection_fake_dipoles(fake_dipole[i_a], fake_dipole_candidate[i_a], csi->phasespace_order_alpha_s[i_a] - 1, csi->phasespace_order_alpha_e[i_a], csi->phasespace_order_interference[i_a], fake_psi->csi->singular_region, fake_psi->csi->singular_region_name, fake_psi->csi->singular_region_list, fake_psi, generic, generic->determination_no_subprocess_doubledipole, generic->determination_MCchannels_doubledipole);
*/
      logger << LOG_INFO << "Before fake_MC_x_dipole_mapping" << endl;
      logger << LOG_INFO << "fake_psi->csi->dipole.size() = " << fake_psi->csi->dipole.size() << endl;
      fake_psi->MC_x_dipole_mapping.resize(fake_psi->csi->dipole.size());
      ///      vector<vector<int> > fake_MC_x_dipole_mapping(fake_psi->csi->dipole.size());
      ///      logger << LOG_DEBUG_VERBOSE << "fake_psi->csi->dipole[" << i_a << "].size() = " << fake_psi->csi->dipole.size() << endl;
      for (int j_a = 1; j_a < fake_psi->csi->dipole.size(); j_a++){
	//	logger << LOG_DEBUG_VERBOSE << "before: fake_MC_x_dipole_mapping[" << j_a << "].size() = " << fake_MC_x_dipole_mapping[j_a].size() << endl;
	//	ac_tau_psp_doubledipole(j_a, fake_MC_x_dipole_mapping[j_a], fake_psi);
	//	generic->ac_tau_psp_doubledipole(j_a, fake_MC_x_dipole_mapping[j_a], *fake_psi);
	///	fake_psi->ac_tau_psp_dipole(j_a, fake_MC_x_dipole_mapping[j_a]);
	logger << LOG_INFO << "fake_psi->MC_x_dipole_mapping.size() = " << fake_psi->MC_x_dipole_mapping.size() << endl;
	fake_psi->ac_tau_psp_dipole(j_a, fake_psi->MC_x_dipole_mapping[j_a]);

	///	logger << LOG_DEBUG_VERBOSE << "after:  fake_MC_x_dipole_mapping[" << j_a << "].size() = " << fake_MC_x_dipole_mapping[j_a].size() << endl;
	logger << LOG_DEBUG_VERBOSE << "after:  fake_psi->MC_x_dipole_mapping[" << j_a << "].size() = " << fake_psi->MC_x_dipole_mapping[j_a].size() << endl;
      }

      logger << LOG_DEBUG_VERBOSE << "fake_psi->csi->dipole[" << i_a << "].size() = " << fake_psi->csi->dipole.size() << endl;
      for (int j_a = 1; j_a < fake_psi->csi->dipole.size(); j_a++){
	logger << LOG_DEBUG_VERBOSE << "fake_psi->MC_x_dipole_mapping[" << j_a << "].size() = " << fake_psi->MC_x_dipole_mapping[j_a].size() << endl;
	///	logger << LOG_DEBUG_VERBOSE << "fake_MC_x_dipole_mapping[" << j_a << "].size() = " << fake_MC_x_dipole_mapping[j_a].size() << endl;
	for (int i_m = 0; i_m < fake_psi->MC_x_dipole_mapping[j_a].size(); i_m++){
	  ///	for (int i_m = 0; i_m < fake_MC_x_dipole_mapping[j_a].size(); i_m++){
	  int flag = MC_x_dipole_mapping[i_a].size();
	  for (int j_m = 0; j_m < MC_x_dipole_mapping[i_a].size(); j_m++){
	    ///	  for (int j_m = 0; j_m < MC_x_dipole_mapping[i_a].size(); j_m++){
	    logger << LOG_DEBUG_VERBOSE << "i_a = " << i_a << "   i_m = " << i_m << "   j_m = " << j_m << "   fake_psi->MC_x_dipole_mapping[" << j_a << "][" << i_m << "] = " << fake_psi->MC_x_dipole_mapping[j_a][i_m] << endl;
	    ///	    logger << LOG_DEBUG_VERBOSE << "i_a = " << i_a << "   i_m = " << i_m << "   j_m = " << j_m << "   fake_MC_x_dipole_mapping[" << j_a << "][" << i_m << "] = " << fake_MC_x_dipole_mapping[j_a][i_m] << endl;
	    if (fake_psi->MC_x_dipole_mapping[j_a][i_m] == MC_x_dipole_mapping[i_a][j_m]){flag = j_m; break;}
	    ///	    if (fake_MC_x_dipole_mapping[j_a][i_m] == MC_x_dipole_mapping[i_a][j_m]){flag = j_m; break;}
	  }
	  if (flag == MC_x_dipole_mapping[i_a].size()){MC_x_dipole_mapping[i_a].push_back(fake_psi->MC_x_dipole_mapping[j_a][i_m]);}
	  ///	  if (flag == MC_x_dipole_mapping[i_a].size()){MC_x_dipole_mapping[i_a].push_back(fake_MC_x_dipole_mapping[j_a][i_m]);}
	}
      }
  }
  //  delete [] fake_psi->csi;
  /*
    for (int i_a = 1; i_a < csi->dipole.size(); i_a++){
      for (int j_a = 1; j_a < fake_psi->csi->dipole.size(); j_a++){
	logger << LOG_DEBUG << "fake_psi->csi->dipole[" << i_a << "][" << j_a << "].name() = " << csi->dipole[i_a].name() << " - " << fake_psi->csi->dipole[j_a].name() << endl;
      }
    }


    for (int i_a = 0; i_a < csi->dipole.size(); i_a++){
      for (int j_m = 0; j_m < MC_x_dipole_mapping[i_a].size(); j_m++){
	logger << LOG_DEBUG << "MC_x_dipole_mapping[" << i_a << "][" << j_m << "] = " << MC_x_dipole_mapping[i_a][j_m] << endl;
      }
    }

  */

    logger << LOG_DEBUG << "fake_psi->csi->dipole to improve initial-state mappings finished..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



