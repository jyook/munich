#include "header.hpp"

randomvariable::randomvariable(){
  static Logger logger("randomvariable::randomvariable()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  manager = NULL;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


randomvariable::randomvariable(importancesampling_input input, string name, randommanager * _manager){
  static Logger logger("randomvariable::randomvariable (input)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  manager = _manager;

  logger << LOG_INFO << input << endl;

  input.name = name;
  logger << LOG_DEBUG_VERBOSE << "name = " << name << endl;
  logger << LOG_DEBUG_VERBOSE << "input.name = " << input.name << endl;
  //  logger << LOG_DEBUG_VERBOSE << "manager->psi->filename_IS_MCweight = " << manager->psi->filename_IS_MCweight << endl;
  //  logger << LOG_DEBUG_VERBOSE << "manager->psi->filename_IS_MCweight_in_contribution = " << manager->psi->filename_IS_MCweight_in_contribution << endl;
  IS_PS = importancesampling_set(input, manager->psi->filename_weight_writeout, manager->psi->filename_weight_readin);
  //  IS_PS = importancesampling_set(input, manager->psi->filename_IS_MCweight, manager->psi->filename_IS_MCweight_in_contribution);
  logger << LOG_DEBUG_VERBOSE << "name = " << name << endl;

  // complicated way to re-determine counter of randomvariable !!!
  string number_s;
  for (int i_s = 2; i_s < name.size(); i_s++){
    if (name[i_s] == '_'){break;}
    else {number_s.push_back(name[i_s]);}
  }
  number = atoi(number_s.c_str());
  logger << LOG_DEBUG << name << "   " << number_s << "   " << number << endl;

  IS_PS.channel = 0;

  if (IS_PS.n_gridsize == 1){imp_sampling = false;}
  else {imp_sampling = true;}

  used = false;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::psp_IS_optimization(double & psp_weight, double & psp_weight2) {
  static Logger logger("randomvariable::psp_IS_optimization");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (!imp_sampling){return;}
  if (!used){return;}

  IS_PS.psp_IS_optimization(psp_weight, psp_weight2);

  used = false;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::step_IS_optimization(int i_step_mode){
  static Logger logger("randomvariable::step_IS_optimization");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  IS_PS.step_IS_optimization(i_step_mode);

  // shifted -> result_IS_optimization !!!
  result_IS_optimization(i_step_mode);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::result_IS_optimization(int i_step_mode) {
  static Logger logger("randomvariable::result_IS_optimization");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  IS_PS.result_IS_optimization(i_step_mode);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


double randomvariable::get_random(){
  static Logger logger("randomvariable::get_random()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  double x_random;
  // Problem in used variable ???
  /*
  if (used && !end_optimization) {
    logger << LOG_FATAL << "randomvariable::get_random  (" << name << " used && !end_optimization) should never be called with used == true !" << endl;
    exit(1);
  }
  */


  if (imp_sampling) {
    logger << LOG_DEBUG_VERBOSE << IS_PS.name << "   called: used = true." << endl;
    used = true;
    logger << LOG_DEBUG_VERBOSE << "manager = " << manager << endl;
    logger << LOG_DEBUG_VERBOSE << "manager->psi = " << manager->psi << endl;
									//    logger << LOG_DEBUG_VERBOSE << "manager->psi->rng = " << manager->psi->rng << endl;
    double random = manager->psi->rng.access_random_number();
    double random_channel = manager->psi->rng.access_random_number();
    logger << LOG_DEBUG_VERBOSE << IS_PS.name << "   called: used = true." << endl;


    // new implementation:
    IS_PS.channel = 0;
    if (IS_PS.is_gridsize_power_of_two){
      //    if (IS_PS.n_gridsize == 64){
      for (int i_b = IS_PS.n_gridsize / 2; i_b > 0; i_b /= 2){
	if (random_channel > IS_PS.beta[IS_PS.channel + i_b - 1]){IS_PS.channel += i_b;}
      }
    }
    else {
      int extra = 0;
      for (int i_b = IS_PS.n_gridsize / 2; i_b > 0; i_b /= 2){
	if (random_channel > IS_PS.beta[IS_PS.channel + i_b - 1]){IS_PS.channel += i_b;}
	if (i_b == 3){
	  for (int j_b = 0; j_b < 3 + extra; j_b++){
	    if (random_channel <= IS_PS.beta[IS_PS.channel + j_b]){
	      IS_PS.channel += j_b;
	      i_b = 0;
	      break;
	    }
	  }
	}
	if (i_b % 2){extra++;}
      }
    }

    // 'x' replaces the flatly generated random number:
    //  x = (double(IS_PS.channel) + random) / double(IS_PS.beta.size());
    x_random = (double(IS_PS.channel) + random) / IS_PS.n_gridsize;

    if (manager->psi->switch_IS_mode_phasespace == 1 || manager->psi->switch_IS_mode_phasespace == 3){
      //      g_IS = IS_PS.alpha[IS_PS.channel] * IS_PS.n_gridsize;
      manager->psi->MC_g_IS_global *= IS_PS.alpha[IS_PS.channel] * IS_PS.n_gridsize;
    }
  }
  else {x_random = manager->psi->rng.access_random_number();}

  if (!IS_PS.end_optimization){
    // It tells manager that this variable has been used at this phase-space point (can be used later)
    if (manager != NULL){manager->add_var_to_queue(this);}
  }

  // Can be removed later:
  //  g_IS = 1.0;
  return x_random;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// Could also be directly accessed, without the detour via void randomvariable::get_random(double &x, double &g_IS) !!!

void randomvariable::get_g_IS(double r, double & g_IS) {
  static Logger logger("randomvariable::get_g_IS");
  //  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (r < 0. || r > 1. || munich_isnan(r)){g_IS = sqrt(-1.);}
  else {
    int temp_channel = (int)(r * IS_PS.n_gridsize);
    g_IS = IS_PS.alpha[temp_channel] * IS_PS.n_gridsize;
  }

  //  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void randomvariable::save_weights(ofstream & out_weights){
  static Logger logger("randomvariable::save_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  out_weights << "variable " << setw(50) << IS_PS.name << endl;
  // Why such low precision ???
  for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_weights << setprecision(8) << IS_PS.alpha[i_b] << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::readin_weights(vector<string> & readin) {
  static Logger logger("randomvariable::readin_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (IS_PS.switch_optimization != 2 && IS_PS.switch_optimization != 3){return;}

  // check if correct !!!
  // find the correct section in the weights file
  int proc;
  for (proc=0; proc<readin.size(); proc++) {
    size_t pos=readin[proc].find("variable ");
    if (pos != string::npos) {
      pos += 9;
      string var_name=readin[proc].substr(pos,string::npos);
      int start_index = 0;
      for (int i = 0; i < var_name.size(); i++){
        if (var_name[i] != ' '){start_index = i; break;}
      }
      var_name=var_name.substr(start_index, var_name.size() - start_index);
      if (var_name == IS_PS.name) {
        logger << LOG_INFO << "weights found for " << var_name << endl;
	//        logger << LOG_DEBUG_VERBOSE << "weights found for " << var_name << endl;
        break;
      }
    }
  }

  if (proc == readin.size()){logger << LOG_FATAL << "could not read in weights for " << IS_PS.name << endl; exit(1);}

  logger << LOG_DEBUG_VERBOSE << "reading weights for " << readin[proc] << endl;
  proc++;

  for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){
    IS_PS.alpha[i_b] = atof(readin[proc].c_str());
    proc++;
  }

  IS_PS.beta[0] = IS_PS.alpha[0];
  for (int i_b = 1; i_b < IS_PS.n_gridsize; i_b++){IS_PS.beta[i_b] = IS_PS.beta[i_b - 1] + IS_PS.alpha[i_b];}

  // Check for issues in read-in weights:
  if (fabs(IS_PS.beta[IS_PS.n_gridsize - 1] - 1) > 1e-6){
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){
      logger << LOG_ERROR << "alpha[" << setw(4) << i_b << "] = " << IS_PS.alpha[i_b] << "   beta[" << setw(4) << i_b << "] = " << IS_PS.beta[i_b] << endl;
    }
    logger << LOG_FATAL << setw(50) << IS_PS.name << ": something went wrong in weight read-in, aborting.." << endl;
    exit(1);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::proceeding_out(ofstream &out_proceeding) {
  static Logger logger("randomvariable::proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  IS_PS.proceeding_out(out_proceeding);
  /*
  out_proceeding << "# " << IS_PS.name << endl;

  // TODO: handle imp_sampling!
  out_proceeding << IS_PS.end_optimization << endl;
  if (IS_PS.end_optimization != -1){
    out_proceeding << "# " << IS_PS.name << " alpha (" << IS_PS.n_gridsize << ")" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << double2hexastr(IS_PS.alpha[i_b]) << endl;}
  }

  if (IS_PS.end_optimization == 0){
    out_proceeding << "# " << IS_PS.name << " n_acc_channel - n_rej_channel - sum_channel_weight (3 x " << IS_PS.n_gridsize << ")" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){
      out_proceeding << IS_PS.n_acc_channel[i_b] << "   " << endl;
      out_proceeding << IS_PS.n_rej_channel[i_b] << "   " << endl;
      out_proceeding << double2hexastr(IS_PS.sum_channel_weight[i_b]) << endl;
      //      out_proceeding << double2hexastr(IS_PS.sum_channel_weight2[i_b]) << endl;
    }
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::proceeding_in(int & proc, vector<string> &readin) {
  static Logger logger("randomvariable::proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  IS_PS.proceeding_in(proc, readin);
  /*
  IS_PS.end_optimization = atoi(readin[proc].c_str());
  proc += 1;
  if (IS_PS.end_optimization != -1){
    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.alpha[i] = hexastr2double(readin[proc + 1 * i]);}
    proc += IS_PS.n_gridsize;
    IS_PS.beta[0] = IS_PS.alpha[0];
    for (int i = 1; i < IS_PS.n_gridsize; i++){IS_PS.beta[i] = IS_PS.beta[i - 1] + IS_PS.alpha[i];}
  }
  if (IS_PS.end_optimization == 0){
    for (int i = 0; i < IS_PS.n_gridsize; i++){
      IS_PS.n_acc_channel[i] = atoi(readin[proc + 3 * i].c_str());
      IS_PS.n_rej_channel[i] = atoi(readin[proc + 3 * i + 1].c_str());
      IS_PS.sum_channel_weight[i] = hexastr2double(readin[proc + 3 * i + 2]);
    }
    proc += 3 * IS_PS.n_gridsize;
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::check_proceeding_in(int & int_end, int & temp_check_size, vector<string> & readin){
  static Logger logger("randomvariable::check_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // new implementation:
  // check if usage of 'int_end' makes sense here !!!

  logger << LOG_INFO << "# " << IS_PS.name << "   temp_check_size (before) = " << temp_check_size << endl;

  IS_PS.end_optimization = atoi(readin[temp_check_size].c_str());
  temp_check_size += 1;
  logger << LOG_DEBUG << "end_optimization = " << IS_PS.end_optimization << endl;

  if (temp_check_size > readin.size()){
    int_end = 2;
    logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
    return;
  }
  else {
    logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
  }

  if (IS_PS.end_optimization != -1){
    //  Should be removed accordingly !!!
    //  s(3)
    ///    temp_check_size += 3;

    if (temp_check_size > readin.size()){
      int_end = 2;
      logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
      return;
    }
    else {
      logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
    }

    temp_check_size += IS_PS.n_gridsize;
    //  alpha(IS_PS.n_gridsize)
    if (temp_check_size > readin.size()){
      int_end = 2;
      logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
      return;
    }
    else {
      logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
    }
  }

  if (IS_PS.end_optimization == 0){
    temp_check_size += 2 + 4 * IS_PS.n_gridsize;
    //    temp_check_size += 3 * IS_PS.n_gridsize;
    if (temp_check_size > readin.size()){
      int_end = 2;
      logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
      return;
    }
    else {
      logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
    }

    if (IS_PS.switch_validation_optimization){
      temp_check_size += 2 + IS_PS.n_optimization_step + IS_PS.n_optimization_step * IS_PS.n_gridsize;
      if (temp_check_size > readin.size()){
	int_end = 2;
	logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
	return;
      }
      else {
	logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::proceeding_group_by_type_out(ofstream & out_proceeding){
  static Logger logger("randomvariable::proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  IS_PS.proceeding_group_by_type_out(out_proceeding);
  /*
  // TODO: handle imp_sampling!

  out_proceeding << "# " << IS_PS.name << " - end_optimization (1)" << endl;
  out_proceeding << "#=" << endl;
  out_proceeding << IS_PS.end_optimization << endl;
  if (IS_PS.end_optimization != -1){
    out_proceeding << "# " << IS_PS.name << " - alpha (" << IS_PS.n_gridsize << ")" << endl;
    out_proceeding << "#=" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << double2hexastr(IS_PS.alpha[i_b]) << endl;}
  }

  if (IS_PS.end_optimization == 0){
    out_proceeding << "# " << IS_PS.name << " - n_acc_channel (" << IS_PS.n_gridsize << ")" << endl;
    out_proceeding << "#+" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << IS_PS.n_acc_channel[i_b] << "   " << endl;}
    out_proceeding << "# " << IS_PS.name << " - n_rej_channel (" << IS_PS.n_gridsize << ")" << endl;
    out_proceeding << "#+" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << IS_PS.n_rej_channel[i_b] << "   " << endl;}
    out_proceeding << "# " << IS_PS.name << " - sum_channel_weight (" << IS_PS.n_gridsize << ")" << endl;
    out_proceeding << "#+" << endl;
    for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << double2hexastr(IS_PS.sum_channel_weight[i_b]) << endl;}
    //      out_proceeding << "# " << IS_PS.name << " - sum_channel_weight2 (" << IS_PS.n_gridsize << ")" << endl;
    //      out_proceeding << "#+" << endl;
    //      for (int i_b = 0; i_b < IS_PS.n_gridsize; i_b++){out_proceeding << double2hexastr(IS_PS.sum_channel_weight2[i_b]) << endl;}
  }
  */
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randomvariable::proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination) {
  static Logger logger("randomvariable::proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  IS_PS.proceeding_group_by_type_in(proc, readin, switch_combination);
  /*
  IS_PS.end_optimization = atoi(readin[proc].c_str());
  proc += 1;
  if (IS_PS.end_optimization != -1){
    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.alpha[i] = hexastr2double(readin[proc + 1 * i]);}
    proc += IS_PS.n_gridsize;
    IS_PS.beta[0] = IS_PS.alpha[0];
    for (int i = 1; i < IS_PS.n_gridsize; i++){IS_PS.beta[i] = IS_PS.beta[i - 1] + IS_PS.alpha[i];}
  }
  if (IS_PS.end_optimization == 0){
    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.n_acc_channel[i] = atoi(readin[proc++].c_str());}
    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.n_rej_channel[i] = atoi(readin[proc++].c_str());}
    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.sum_channel_weight[i] = hexastr2double(readin[proc++]);}
    //    for (int i = 0; i < IS_PS.n_gridsize; i++){IS_PS.sum_channel_weight2[i] = hexastr2double(readin[proc++]);}
  }
*/
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


ostream & operator << (ostream & s, const randomvariable & IS){
  s << IS.IS_PS;
  return s;
}

