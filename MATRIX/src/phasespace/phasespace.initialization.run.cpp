#include "header.hpp"

// check if needed !!! (main part moved to csi)
void phasespace_set::initialization_contribution_order(){
  Logger logger("phasespace_set::initialization_contribution_order ()");
  logger << LOG_DEBUG << "started" << endl;

  MC_n_channel_phasespace.resize(1);
  MC_sum_channel_phasespace.resize(1);

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_complete(){
  Logger logger("phasespace_set::initialization_complete");
  logger << LOG_DEBUG << "started" << endl;

  if (csi->type_contribution == "born" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "L2I" ||
      csi->type_contribution == "loop" ||
      csi->type_contribution == "L2RT"){
    initialization_phasespace_born();
    ax_psp(0);
    MC_n_channel = determination_MCchannels(0);
  }
  if (csi->type_contribution == "CA" ||
      csi->type_contribution == "RCA" ||
      csi->type_contribution == "L2CA"){
    initialization_phasespace_born();
    ax_psp(0);
    MC_n_channel = determination_MCchannels(0);
  }
  if (csi->type_contribution == "VA" ||
      csi->type_contribution == "RVA" ||
      csi->type_contribution == "L2VA"){
    initialization_phasespace_born();
    ax_psp(0);
    MC_n_channel = determination_MCchannels(0);
    // Check if standard VA runs are affected !!!
    if (csi->type_correction == "QCD"){
      QCD_selection_phasespace_singularity(csi->singular_region, csi->singular_region_name, csi->singular_region_list, *this);
    }
    // QEW/MIX ???
  }
  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "VT" ||
      csi->type_contribution == "L2VT" ||
      csi->type_contribution == "VT2"){
    initialization_phasespace_born();
    ax_psp(0);
    MC_n_channel = determination_MCchannels(0);
  }

  if (csi->type_contribution == "RA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RA"){
    // Still done in munich.integration.RA.QCD.cpp ... should be moved later !!!
  }

  logger << LOG_DEBUG_VERBOSE << "csi->type_contribution = " << csi->type_contribution << endl;

  logger << LOG_DEBUG_VERBOSE << "csi->phasespace_order_alpha_s[0] = " << csi->phasespace_order_alpha_s[0] << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->phasespace_order_alpha_e[0] = " << csi->phasespace_order_alpha_e[0] << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->phasespace_order_interference[0] = " << csi->phasespace_order_interference[0] << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->no_process_parton[0] = " << csi->no_process_parton[0] << endl;
  logger << LOG_DEBUG_VERBOSE << "MC_n_channel = " << MC_n_channel << endl;

  initialization_minimum_tau();
  initialization_minimum_phasespacecut();

  if (csi->type_contribution == "RA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RA"){
    n_dipoles = csi->dipole.size();

    initialization_optimization();

    initialization_phasespace_subprocess_dipole_RA();
    // extend  initialization_phasespace_optimization_fiducialcut();  to dipole situation !!!
    initialization_phasespace_optimization_fiducialcut();
    optimize_minv();
    initialization_phasespace_subprocess_optimization_dipole_RA();

    initialization_phasespace_RA();

    // not here for dipole-less phase-spaces !!! ???
    //  ac_tau_psp(0, tau_MC_map);
  }
  else {
    initialization_phasespace_subprocess();
    initialization_phasespace_optimization_fiducialcut();
    optimize_minv();
    initialization_phasespace_subprocess_optimization();

    ac_tau_psp(0, tau_MC_map);

    initialization_optimization();
  }

  initialization_MC_IS_name_list();

  if (csi->type_contribution == "RT"){initialization_fake_dipole_mapping_RT();}
  if (csi->type_contribution == "L2RT"){initialization_fake_dipole_mapping_RT();}
  if (csi->type_contribution == "RCA"){initialization_fake_dipole_mapping_RT();}
  if (csi->type_contribution == "RVA"){initialization_fake_dipole_mapping_RT();}

  initialization_optimization_grid();

  initialization_MC_rng();
  initialization_MC();

  if (csi->type_contribution == "VT" ||
      csi->type_contribution == "L2VT" ||
      csi->type_contribution == "VT2"){
    if (switch_resummation){initialization_phasespace_IS_QT();}
  }
  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "CT2"){
    // why in general (not only for resummation) ??? not active anyway, but maybe issues if non-initialized variables ???
    initialization_phasespace_IS_QT();
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_phasespace_optimization_fiducialcut(){
  Logger logger("phasespace_set::initialization_phasespace_optimization_fiducialcut");
  logger << LOG_DEBUG << "started" << endl;

  for (int i_f = 0; i_f < esi->fiducial_cut.size(); i_f++){
    logger << LOG_INFO << "esi->fiducial_cut[" << i_f << "] = " << esi->fiducial_cut[i_f].name << endl << esi->fiducial_cut[i_f] << endl;
    if (esi->fiducial_cut[i_f].observable == "M" && (esi->fiducial_cut[i_f].type == LOWER || esi->fiducial_cut[i_f].type == WINDOW)){

      // Don't derive generation cuts if M is different for dipole kinematics !!!
     
      for (int i_a = 0; i_a < n_dipoles; i_a++){
	logger << LOG_INFO << "[i_a = " << i_a << "]   Check if cut may be applied on generation level for phase space:" << endl;
	bool apply_generation_cut = true;
	if (csi->class_contribution_CS_real){
	  for (int i_p = 0; i_p < esi->fiducial_cut[i_f].type_particle.size(); i_p++){
	    for (size_t j_p = 0; j_p < esi->ps_runtime_order_inverse[i_a][esi->observed_object[esi->fiducial_cut[i_f].type_particle[i_p]]].size(); j_p++){
	      size_t x_p = esi->ps_runtime_order_inverse[i_a][esi->observed_object[esi->fiducial_cut[i_f].type_particle[i_p]]][j_p];
	      logger << LOG_INFO << "[i_a = " << i_a << "]   x_p = esi->ps_runtime_order_inverse[i_a][[esi->fiducial_cut[" << i_f << "].type_particle[" << i_p << "]]][" << j_p << "] = " << x_p << endl;
	      if (csi->type_correction == "QCD" || csi->type_correction == "MIX"){
		for (int i_j = 0; i_j < esi->ps_runtime_subtraction_QCD[i_a].size(); i_j++){
		  //		  logger << LOG_INFO << "[i_a = " << i_a << "]   esi->object_list_selection[esi->ps_runtime_subtraction_QCD[" << i_a << "][" << i_j << "] = " << esi->ps_runtime_subtraction_QCD[i_a][i_j] << "] = " << esi->object_list_selection[esi->ps_runtime_subtraction_QCD[i_a][i_j]] << endl;
		  logger << LOG_INFO << "[i_a = " << i_a << "]   Check if  esi->ps_runtime_subtraction_QCD[" << i_a << "][" << i_j << "] = " << esi->ps_runtime_subtraction_QCD[i_a][i_j] << " =?= " << x_p << " = x_p" << endl;
		  if (x_p == esi->ps_runtime_subtraction_QCD[i_a][i_j]){apply_generation_cut = false; break;}
		}
	      }
	      if (csi->type_correction == "QEW" || csi->type_correction == "MIX"){
		for (int i_j = 0; i_j < esi->ps_runtime_subtraction_QEW[i_a].size(); i_j++){
		  //		  logger << LOG_INFO << "[i_a = " << i_a << "]   esi->object_list_selection[esi->ps_runtime_subtraction_QEW[" << i_a << "][" << i_j << "] = " << esi->ps_runtime_subtraction_QEW[i_a][i_j] << "] = " << esi->object_list_selection[esi->ps_runtime_subtraction_QEW[i_a][i_j]] << endl;
		  logger << LOG_INFO << "[i_a = " << i_a << "]   Check if  esi->ps_runtime_subtraction_QEW[" << i_a << "][" << i_j << "] = " << esi->ps_runtime_subtraction_QEW[i_a][i_j] << " =?= " << x_p << " = x_p" << endl;
		  if (x_p == esi->ps_runtime_subtraction_QEW[i_a][i_j]){apply_generation_cut = false; break;}
		}
	      }
	      if (!apply_generation_cut){break;}
	    }
	    if (!apply_generation_cut){break;}
	  }
	}
      	if (!apply_generation_cut){
	  logger << LOG_INFO << "[i_a = " << i_a << "]   Cut may not be applied on generation level!" << endl;
	  continue;
	}

	vector<bool> temp_binary(csi->type_parton[i_a].size(), false);
	logger << LOG_INFO << "[i_a = " << i_a << "]   esi->fiducial_cut[" << i_f << "] = " << esi->fiducial_cut[i_f].name << " can be used to determine invariant-mass limits in phase space generation." << endl;
	for (int i_p = 0; i_p < esi->fiducial_cut[i_f].type_particle.size(); i_p++){
	  logger << LOG_INFO << "[i_a = " << i_a << "]   esi->fiducial_cut[" << i_f << "].type_particle[" << i_p << "] = " << esi->fiducial_cut[i_f].type_particle[i_p] << endl;
	  for (size_t j_p = 0; j_p < esi->ps_runtime_order_inverse[i_a][esi->observed_object[esi->fiducial_cut[i_f].type_particle[i_p]]].size(); j_p++){
	    size_t x_p = esi->ps_runtime_order_inverse[i_a][esi->observed_object[esi->fiducial_cut[i_f].type_particle[i_p]]][j_p];
	    temp_binary[x_p] = true;
	    logger << LOG_INFO << "[i_a = " << i_a << "]   temp_binary[" << x_p << "] = true" << endl;
	  }
	}
	int temp_sum_binary = 0;
	for (int i_p = 3; i_p < temp_binary.size(); i_p++){if (temp_binary[i_p]){temp_sum_binary += pow(2, i_p - 1);}}
	sqrtsmin_opt[i_a][temp_sum_binary] = esi->fiducial_cut[i_f].cut_value_lower;
	logger << LOG_INFO << "sqrtsmin_opt[i_a = " << i_a << "][temp_sum_binary = " << temp_sum_binary << "] = " << esi->fiducial_cut[i_f].cut_value_lower << " = esi->fiducial_cut[" << i_f << "].cut_value_lower" << endl;
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_complete_RA(){
  Logger logger("phasespace_set::initialization_complete_RA");
  logger << LOG_DEBUG << "started" << endl;

  //  initialization_mapping_parameter();
  initialization_minimum_tau();
  initialization_minimum_phasespacecut();

  // ???
  n_dipoles = csi->dipole.size();

  MC_n_channel_phasespace[0] = determination_MCchannels(0);
  MC_sum_channel_phasespace[0] = MC_n_channel_phasespace[0];
  csi->dipole[0].set_phasespace_variable(MC_n_channel_phasespace[0], MC_sum_channel_phasespace[0], 0);

  for (int i_a = 1; i_a < csi->dipole.size(); i_a++){
    if (csi->dipole[i_a].type_dipole() == 1 && !(switch_off_RS_mapping_ij_k)){MC_n_channel_phasespace.push_back(determination_MCchannels_dipole(i_a));}
    else if (csi->dipole[i_a].type_dipole() == 2 && !(switch_off_RS_mapping_ij_a)){MC_n_channel_phasespace.push_back(determination_MCchannels_dipole(i_a));}
    else if (csi->dipole[i_a].type_dipole() == 3 && !(switch_off_RS_mapping_ai_k)){MC_n_channel_phasespace.push_back(determination_MCchannels_dipole(i_a));}
    else if (csi->dipole[i_a].type_dipole() == 5 && !(switch_off_RS_mapping_ai_b)){MC_n_channel_phasespace.push_back(determination_MCchannels_dipole(i_a));}
    else {MC_n_channel_phasespace.push_back(0);}
    MC_sum_channel_phasespace.push_back(MC_sum_channel_phasespace[i_a - 1] + MC_n_channel_phasespace[i_a]);

    logger << LOG_DEBUG_VERBOSE << "MC_n_channel_phasespace[i_a = " << setw(2) << i_a << "] = " << setw(5) << MC_n_channel_phasespace[i_a] << "   " << "MC_sum_channel_phasespace[i_a = " << setw(2) << i_a << "] = " << setw(5) << MC_sum_channel_phasespace[i_a] << endl;
  }

  MC_n_channel = accumulate(MC_n_channel_phasespace.begin(), MC_n_channel_phasespace.end(), 0);

  // set remaining dipole[i_a] components - actually not needed to be part of psi !!!
  for (int i_a = 1; i_a < csi->dipole.size(); i_a++){
    int temp_massive = 0;
    if (M[abs(csi->type_parton[0][csi->dipole[i_a].no_R_emitter_1()])] != 0. ||
	M[abs(csi->type_parton[0][csi->dipole[i_a].no_R_emitter_2()])] != 0. ||
	M[abs(csi->type_parton[0][csi->dipole[i_a].no_R_spectator()])] != 0.){temp_massive = 1;}
    csi->dipole[i_a].set_phasespace_variable(MC_n_channel_phasespace[i_a], MC_sum_channel_phasespace[i_a], temp_massive);
  }

  initialization_optimization();

  initialization_phasespace_subprocess_dipole_RA();
  initialization_phasespace_optimization_fiducialcut();
  optimize_minv();
  initialization_phasespace_subprocess_optimization_dipole_RA();

  initialization_phasespace_RA();

  // not here for dipole-less phase-spaces !!! ???
  //  ac_tau_psp(0, tau_MC_map);

  initialization_MC_IS_name_list();
  initialization_optimization_grid();

  initialization_MC_rng();
  initialization_MC();

  logger << LOG_INFO << "list_singular_regions" << endl;
  for (int i = 0; i < csi->singular_region_list.size(); i++){
    logger << LOG_INFO << csi->singular_region_list[i][0] << " " << csi->singular_region_list[i][1] << "   " << csi->singular_region_name[csi->singular_region_list[i][0]][csi->singular_region_list[i][1]] << endl;
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_optimization(){
  Logger logger("phasespace_set::initialization_optimization ()");
  logger << LOG_DEBUG << "started" << endl;

  logger << LOG_DEBUG << "run_mode = " << run_mode << endl;
  logger << LOG_DEBUG << "switch_output_weights = " << switch_output_weights << endl;
  logger << LOG_DEBUG << "switch_n_events_opt = " << switch_n_events_opt << endl;
  
  // set switch_n_events_opt = 2 automatically for run_mode == "grid" and alike ???
  // set switch_output_weights = 1 automatically for run_mode == "grid" and alike ???
  // remove run_mode == "gridacc" (corresponds to  run_mode = "grid"  with  switch_step_mode_grid = 1) ???
  
  //////////////////////////////////////////
  //  new weight-optimization parameters  //
  //////////////////////////////////////////

  if (run_mode == "grid" || run_mode == "gridacc"){
    for (int i_m = 0; i_m < input_MC.size(); i_m++){if (input_MC[i_m]->switch_optimization == 2){input_MC[i_m]->switch_optimization = 1;}}
    for (int i_s = 0; i_s < input_IS.size(); i_s++){if (input_IS[i_s]->switch_optimization == 2){input_IS[i_s]->switch_optimization = 1;}}
  }
  if (run_mode == "gridrefine"){
    for (int i_m = 0; i_m < input_MC.size(); i_m++){if (input_MC[i_m]->switch_optimization == 2){input_MC[i_m]->switch_optimization = 3;}}
    for (int i_s = 0; i_s < input_IS.size(); i_s++){if (input_IS[i_s]->switch_optimization == 2){input_IS[i_s]->switch_optimization = 3;}}
  }
  
  if (run_mode == "grid"){
    if (switch_step_mode_grid){i_step_mode = & i_gen;}
    else {i_step_mode = & i_acc;}
  }
  else if (run_mode == "gridacc" || run_mode == "gridrefine"){
    i_step_mode = & i_acc;
  }
  else {
    i_step_mode = & i_acc;
    switch_output_weights = 0;
    end_optimization = 1;
  }

  if (!csi->class_contribution_collinear){input_IS_z1z2.switch_optimization = -1;}
  if (!csi->class_contribution_CS_real){input_MC_x_dipole.switch_optimization = -1;}

  //////////////////////////////////////////

  // doubled !!!
  initialization_filename();

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_optimization_grid(){
  Logger logger("phasespace_set::initialization_optimization_grid");
  logger << LOG_DEBUG << "started" << endl;

  //////////////////////////////////////////
  //  new weight-optimization parameters  //
  //////////////////////////////////////////

  //  logger << LOG_INFO << "opt_n_events_min = " << opt_n_events_min << endl;
  //  opt_n_events_min = n_events_min;
  opt_n_events_min = 0;
  //  logger << LOG_INFO << "opt_n_events_min = " << opt_n_events_min << endl;
  n_events_MC_opt = 0;

  input_MC_phasespace.n_channel = MC_n_channel;
  
  if (MC_n_channel == 1){input_MC_phasespace.switch_optimization = -1;}
  if (tau_MC_map.size() == 1){input_MC_tau.switch_optimization = -1;}
  
  if (MC_IS_name_list.size() == 0){
    for (int i_s = 0; i_s < input_IS.size(); i_s++){if (input_IS[i_s]->name.substr(0, 5) == "IS_PS"){input_IS[i_s]->switch_optimization = -1;}}
    // not identical to old version:
    switch_IS_mode_phasespace = 0;
  }

  // Introduce parameter to allow factor wrt.  n_step  to set each  n_event_per_step !!! E.g: factor_event_per_step, n_event_factor_per_step ??? 
  // Use to overwrite n_event_per_step, or use the larger value ???
  // Removed  n_alpha_epc (n_MC_alpha_epc, n_MC_tau_alpha_epc, etc.) !!!
  // Removed  n_IS_events_factor -> revival (see above) !!!

  for (int i_m = 0; i_m < input_MC.size(); i_m++){
    if (input_MC[i_m]->n_event_per_step != 0 && input_MC[i_m]->n_event_per_step < n_step){input_MC[i_m]->n_event_per_step = n_step;}
  }
  for (int i_s = 0; i_s < input_IS.size(); i_s++){
    if (input_IS[i_s]->n_event_per_step != 0 && input_IS[i_s]->n_event_per_step < n_step){input_IS[i_s]->n_event_per_step = n_step;}
  }

  for (int i_m = 0; i_m < input_MC.size(); i_m++){
    if (input_MC[i_m]->switch_optimization == 1 || input_MC[i_m]->switch_optimization == 3){
      if (input_MC[i_m]->n_optimization_step * input_MC[i_m]->n_event_per_step > n_events_MC_opt){n_events_MC_opt = input_MC[i_m]->n_optimization_step * input_MC[i_m]->n_event_per_step;}
    }
  }
  for (int i_s = 0; i_s < input_IS.size(); i_s++){
    if (input_IS[i_s]->switch_optimization == 1 || input_IS[i_s]->switch_optimization == 3){
      if (input_IS[i_s]->n_optimization_step * input_IS[i_s]->n_event_per_step > n_events_MC_opt){n_events_MC_opt = input_IS[i_s]->n_optimization_step * input_IS[i_s]->n_event_per_step;}
    }
  }

  // extra steps for tau, x1x2, z1z2, qTres !!!
  for (int i_s = 0; i_s < input_IS.size(); i_s++){
    if (input_IS[i_s]->name == "IS_tau" || input_IS[i_s]->name == "IS_x1x2" || input_IS[i_s]->name == "IS_z1z2" || input_IS[i_s]->name == "IS_qTres"){
      logger << LOG_DEBUG << "input_IS[" << i_s << "]->n_optimization_step = " << input_IS[i_s]->n_optimization_step << endl;
      logger << LOG_DEBUG << "input_IS[" << i_s << "]->n_optimization_step_extra = " << input_IS[i_s]->n_optimization_step_extra << endl;
      logger << LOG_DEBUG << "n_events_MC_opt = " << n_events_MC_opt << endl;
      logger << LOG_DEBUG << "input_IS[" << i_s << "]->n_event_per_step = " << input_IS[i_s]->n_event_per_step << endl;

      if (input_IS[i_s]->n_optimization_step_extra > 0){
	input_IS[i_s]->n_optimization_step = n_events_MC_opt / input_IS[i_s]->n_event_per_step + input_IS[i_s]->n_optimization_step_extra;
      }
      if (input_IS[i_s]->n_optimization_step == 0){input_IS[i_s]->n_optimization_step = 5;}
    }
  }
  //  for (int i_s = 0; i_s < input_IS.size(); i_s++){logger << LOG_DEBUG << "after: " << *input_IS[i_s] << endl;}

  // final determination of opt_n_events_min
  for (int i_m = 0; i_m < input_MC.size(); i_m++){
    if ((input_MC[i_m]->switch_optimization == 1 || input_MC[i_m]->switch_optimization == 3) && (n_events_MC_opt > opt_n_events_min)){opt_n_events_min = n_events_MC_opt;}
    logger << LOG_DEBUG << "i_m = " << i_m << "   opt_n_events_min = " << opt_n_events_min << endl;
  }
  for (int i_s = 0; i_s < input_IS.size(); i_s++){
    if ((input_IS[i_s]->switch_optimization == 1 || input_IS[i_s]->switch_optimization == 3) && (input_IS[i_s]->n_optimization_step * input_IS[i_s]->n_event_per_step > opt_n_events_min)){opt_n_events_min = input_IS[i_s]->n_optimization_step * input_IS[i_s]->n_event_per_step;}
    logger << LOG_DEBUG << "i_s = " << i_s << "   opt_n_events_min = " << opt_n_events_min << endl;
  }

  if (switch_n_events_opt){
    n_events_min = opt_n_events_min;
    n_events_max = opt_n_events_min;
  }

  //  MC_phasespace = multichannel_set(input_MC_phasespace, filename_MCweight, filename_MCweight_in_contribution);
  MC_phasespace = multichannel_set(input_MC_phasespace, filename_weight_writeout, filename_weight_readin);

  // Why is MC_phasespace initialization separated from the rest ???
  
  logger << LOG_DEBUG << "NEW: opt_n_events_min:   " << opt_n_events_min << endl;

  //////////////////////////////////////////

  logger << LOG_DEBUG << "finished" << endl;
}

void phasespace_set::initialization_filename(){
  Logger logger("phasespace_set::initialization_filename");
  logger << LOG_DEBUG << "started" << endl;

  // Introduce a single file (on for readin, one for writeout) containing all relevant optimization weights !!!
  filename_weight_writeout = "weights/weights_" + csi->subprocess + ".dat";
  filename_weight_readin = "../../../../" + weight_in_directory + "/weights/weights_" + csi->subprocess + ".dat";
  if (switch_output_weights){system_execute(logger, "mkdir weights");}

  /*
  string directory_MCweights_readin = "../../../../" + weight_in_directory + "/weights";
  // not clear yet !!!

  string dir_MCweights = "weights";

  filename_MCweight = dir_MCweights + "/weights_MC_" + csi->subprocess + ".dat";
  filename_IS_MCweight = dir_MCweights + "/weights_IS_MC_" + csi->subprocess + ".dat";
  filename_tauweight = dir_MCweights + "/weights_tau_" + csi->subprocess + ".dat";
  filename_x1x2weight = dir_MCweights + "/weights_x1x2_" + csi->subprocess + ".dat";
  filename_z1z2weight.resize(3);
  filename_z1z2weight[1] = dir_MCweights + "/weights_z1_" + csi->subprocess + ".dat";
  filename_z1z2weight[2] = dir_MCweights + "/weights_z2_" + csi->subprocess + ".dat";
  if (switch_resummation){filename_qTresweight = dir_MCweights + "/weights_qTres_" + csi->subprocess + ".dat";}

  filename_MCweight_in_contribution = directory_MCweights_readin + "/weights_MC_" + csi->subprocess + ".dat";
  filename_IS_MCweight_in_contribution = directory_MCweights_readin + "/weights_IS_MC_" + csi->subprocess + ".dat";
  filename_tauweight_in_contribution = directory_MCweights_readin + "/weights_tau_" + csi->subprocess + ".dat";
  filename_x1x2weight_in_contribution = directory_MCweights_readin + "/weights_x1x2_" + csi->subprocess + ".dat";
  filename_z1z2weight_in_contribution.resize(3);
  filename_z1z2weight_in_contribution[1] = directory_MCweights_readin + "/weights_z1_" + csi->subprocess + ".dat";
  filename_z1z2weight_in_contribution[2] = directory_MCweights_readin + "/weights_z2_" + csi->subprocess + ".dat";
  if (switch_resummation){filename_qTresweight_in_contribution = directory_MCweights_readin + "/weights_qTres_" + csi->subprocess + ".dat";}

  if (switch_output_weights){system_execute(logger, "mkdir " + dir_MCweights);}

  logger << LOG_DEBUG << "filename_MCweight   = " << filename_MCweight << endl;
  logger << LOG_DEBUG << "filename_IS_MCweight   = " << filename_IS_MCweight << endl;
  logger << LOG_DEBUG << "filename_tauweight  = " << filename_tauweight << endl;
  logger << LOG_DEBUG << "filename_x1x2weight = " << filename_x1x2weight << endl;

  logger << LOG_DEBUG << "filename_MCweight_in_contribution   = " << filename_MCweight_in_contribution << endl;
  logger << LOG_DEBUG << "filename_ISMCweight_in_contribution   = " << filename_IS_MCweight_in_contribution << endl;
  logger << LOG_DEBUG << "filename_tauweight_in_contribution  = " << filename_tauweight_in_contribution << endl;
  logger << LOG_DEBUG << "filename_x1x2weight_in_contribution = " << filename_x1x2weight_in_contribution << endl;
  // in case of resummation only !!!
  if (switch_resummation){
    logger << LOG_DEBUG << "filename_qTresweight = " << filename_qTresweight << endl;
    logger << LOG_DEBUG << "filename_qTresweight_in_contribution = " << filename_qTresweight_in_contribution << endl;
  }

  logger << LOG_DEBUG << "filenames created" << endl;
*/

  logger << LOG_DEBUG << "finished" << endl;
}



void phasespace_set::initialization_masses(){
  Logger logger("phasespace_set::initialization_masses");
  logger << LOG_DEBUG << "started" << endl;

  g_global_NWA = 1.; // temporarily !!! does not belong here !!!

  // Extension to improve mappings in loop-induced processes (should not be in msi, but only here - as well as possible values of map_Gamma - in contrast to reg_Gamma) !!!

  M.resize(50, 0.);
  M2.resize(50, 0.);
  Gamma.resize(50, 0.);
  cM2.resize(50, 0.);
  map_Gamma.resize(50, 0.);
  reg_Gamma.resize(50, 0.);

  for (int i = 0; i < 26; i++){
    M[i] = msi->M[i];
    M2[i] = msi->M2[i];
    Gamma[i] = msi->Gamma[i];
    cM2[i] = msi->cM2[i];
    map_Gamma[i] = msi->map_Gamma[i];
    reg_Gamma[i] = msi->reg_Gamma[i];
  }

  for (int i = 1; i < 7; i++){
    if (M[i] != 0.){
      M[30 + i] = 2 * M[i];
      M2[30 + i] = pow(M[30 + i], 2);
      if (Gamma[i] != 0.){Gamma[30 + i] = 10 * Gamma[i];}
      else {Gamma[30 + i] = 0.1 * M[30 + i];}
      map_Gamma[30 + i] = Gamma[30 + i];
      cM2[30 + i] = pow(M[30 + i], 2) - ri * M[30 + i] * Gamma[30 + i];

      M[40 + i] = 2 * M[i];
      M2[40 + i] = pow(M[40 + i], 2);
      if (Gamma[i] != 0.){Gamma[40 + i] = 10 * Gamma[i];}
      else {Gamma[40 + i] = 0.1 * M[40 + i];}
      map_Gamma[40 + i] = Gamma[40 + i];
      cM2[40 + i] = pow(M[40 + i], 2) - ri * M[40 + i] * Gamma[40 + i];
    }
  }

  for (int i_pdg = 0; i_pdg < M.size(); i_pdg++){
    if ((i_pdg > 6 && i_pdg < 11) || (i_pdg > 16 && i_pdg < 22) || (i_pdg > 25 && i_pdg < 31) || (i_pdg > 36 && i_pdg < 41) || (i_pdg > 46)){continue;}
    //    if (i_pdg < 26){
    logger << LOG_INFO << "M[" << setw(2) << i_pdg << "]  = " << right << setprecision(8) << setw(15) << M[i_pdg] << "   map_Gamma = " << right << setprecision(8) << setw(15) << map_Gamma[i_pdg] << "   Gamma[" << setw(2) << i_pdg << "]  = " << right << setprecision(8) << setw(15) << Gamma[i_pdg] << endl;
    if (i_pdg == 25){logger.newLine(LOG_INFO);}
    /*
    }
    else {
      logger << LOG_INFO << "M[" << setw(2) << i_pdg << "]  = " << right << setprecision(8) << setw(15) << M[i_pdg] << "   map_Gamma[" << setw(2) << i_pdg << "]  = " << right << setprecision(8) << setw(15) << map_Gamma[i_pdg] << endl;
    }
    //"   reg = " << right << setprecision(8) << setw(8) << reg_Gamma[i_pdg] << endl;
    */
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void phasespace_set::initialization_MC_IS_name_type(){
  Logger logger("phasespace_set::initialization_MC_IS_name_type");
  logger << LOG_DEBUG << "started" << endl;

  MC_IS_name_type.resize(n_dipoles);
  MC_IS_name_type[0].resize(6);
  if (n_dipoles > 1){for (int i_a = 1; i_a < n_dipoles; i_a++){MC_IS_name_type[i_a].resize(9);}}

  logger << LOG_DEBUG << "finished" << endl;
}

void phasespace_set::initialization_MC_IS_name_list(){
  Logger logger("phasespace_set::initialization_MC_IS_name_list");
  logger << LOG_DEBUG << "started" << endl;

  int counter = 0;
  MC_IS_name_list.resize(0);
  for (int i_a = 0; i_a < MC_IS_name_type.size(); i_a++){
    for (int i_t = 0; i_t < MC_IS_name_type[i_a].size(); i_t++){
      for (int i_i = 0; i_i < MC_IS_name_type[i_a][i_t].size(); i_i++){
	MC_IS_name_list.push_back(MC_IS_name_type[i_a][i_t][i_i]);
	vector<int> temp = {i_a, i_t, i_i};
	map_list_to_type_MC_IS[counter] = temp;
	map_type_to_list_MC_IS[temp] = counter;
	counter++;
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_phasespace_born(){
  Logger logger("phasespace_set::initialization_phasespace_born");
  logger << LOG_DEBUG << "started" << endl;

  n_dipoles = 1;

  initialization_MC_IS_name_type();

  c_p.resize(1);
  c_f.resize(1);
  c_t.resize(1);
  c_d.resize(1);

  v_smin.resize(1);
  v_smax.resize(1);

  g_p.resize(1);
  g_f.resize(1);
  g_t.resize(1);
  g_d.resize(1);

  needed_v_smin.resize(1);
  needed_v_smax.resize(1);
  needed_g_p.resize(1);

  needed_g_f.resize(1);
  needed_g_t.resize(1);
  needed_g_d.resize(1);

  start_xbp_all.resize(1);
  start_xbs_all.resize(1);
  start_xbsqrts_all.resize(1);

  MC_optswitch = 1;

  logger << LOG_DEBUG << "finished" << endl;
}



void phasespace_set::initialization_phasespace_RA(){
  Logger logger("phasespace_set::initialization_phasespace_RA");
  logger << LOG_DEBUG << "started" << endl;

  // Check if this debug output is needed !!!
  logger << LOG_DEBUG << "csi->no_process_parton.size() = " << csi->no_process_parton.size() << endl;
  logger << LOG_DEBUG << "csi->swap_parton.size() = " << csi->swap_parton.size() << endl;
  for (int i_a = 0; i_a < csi->no_process_parton.size(); i_a++){
    stringstream temp_map;
    temp_map << "csi->no_process_parton[" << i_a << "] = " << csi->no_process_parton[i_a] << "     ";
    temp_map << "csi->swap_parton[" << i_a << "] = ";
    for (int i_p = 0; i_p < csi->swap_parton[i_a].size(); i_p++){temp_map << csi->swap_parton[i_a][i_p] << "  ";}
    logger << LOG_DEBUG << temp_map.str() << endl;
    stringstream temp_prc;
    temp_prc << "csi->no_process_parton[" << i_a << "] = " << csi->no_process_parton[i_a] << "     ";
    temp_prc << "csi->swap_parton[" << i_a << "] = ";
    for (int i_p = 0; i_p < csi->swap_parton[i_a].size(); i_p++){temp_prc << csi->swap_parton[i_a][i_p] << "  ";}
    logger << LOG_DEBUG << temp_prc.str() << endl;
  }
  logger << LOG_DEBUG << endl;

  initialization_MC_IS_name_type();

  c_p.resize(n_dipoles);
  c_f.resize(n_dipoles);
  c_t.resize(n_dipoles);
  c_d.resize(n_dipoles);

  v_smin.resize(n_dipoles);
  v_smax.resize(n_dipoles);

  g_p.resize(n_dipoles);
  g_f.resize(n_dipoles);
  g_t.resize(n_dipoles);
  g_d.resize(n_dipoles);

  needed_v_smin.resize(n_dipoles);
  needed_v_smax.resize(n_dipoles);
  needed_g_p.resize(n_dipoles);

  needed_g_f.resize(n_dipoles);
  needed_g_t.resize(n_dipoles);
  needed_g_d.resize(n_dipoles);

  MC_x_dipole_mapping.resize(n_dipoles);
  dipole_x.resize(n_dipoles, 0.);

  MC_optswitch = 0;

  MC_n_channel = MC_sum_channel_phasespace[n_dipoles - 1];


  for (int i_a = 0; i_a < n_dipoles; i_a++){
    logger << LOG_DEBUG << "MC_n_channel_phasespace[" << setw(2) << i_a << "] = " << setw(4) << MC_n_channel_phasespace[i_a] << "   MC_sum_channel_phasespace[" << setw(2) << i_a << "] = " << setw(4) << MC_sum_channel_phasespace[i_a] << endl;
  }
  logger << LOG_DEBUG << "MC_n_channel = " << MC_n_channel << endl;

  // shifted from munich.integration.RA.QCD.cpp
  // if-statement should be redundant !!!
  if (csi->class_contribution_CS_real){
    /*
  if (csi->type_contribution == "RA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "RRJ" ||
      csi->type_contribution == "L2RA"){
    */
    //logger << LOG_DEBUG << "start_xbs_all.size() = " << start_xbs_all.size() << endl;
    // Simplification in case there is no contributing dipole:
    //    logger << LOG_DEBUG << "n_dipoles = " << n_dipoles << endl;
    if (n_dipoles == 1){switch_off_RS_mapping = 1;}
    for (int i_a = 1; i_a < n_dipoles; i_a++){
      // What does this part mean ??? Check meaning of start_xbs_all !!!
      /*
      logger << LOG_DEBUG << "start_xbs_all[" << i_a << "].size() = " << start_xbs_all[i_a].size() << endl;
      logger << LOG_DEBUG << "xb_max / 2 - 4 = " << xb_max / 2 - 4 << endl;
      logger << LOG_DEBUG << "start_xbs_all[" << i_a << "][" << xb_max / 2 - 4 << "] = " << start_xbs_all[i_a][xb_max / 2 - 4] << endl;
      */
      if (start_xbs_all[i_a][xb_max / 2 - 4]){switch_off_RS_mapping = 1;}
    }
  }
  logger << LOG_DEBUG << "switch_off_RS_mapping = " << switch_off_RS_mapping << endl;

  // Switch off dipole mapping with 2->1 kinematics
  // Otherwise, delta function need to be implemented for x-type dipole variables !!!
  
  if (switch_off_RS_mapping){MC_n_channel = MC_n_channel_phasespace[0];}

  ax_psp(0);
  if (!switch_off_RS_mapping){
    for (int i_a = 1; i_a < n_dipoles; i_a++){
      if ((csi->dipole[i_a].type_dipole() == 1 && !switch_off_RS_mapping_ij_k) ||
	  (csi->dipole[i_a].type_dipole() == 2 && !switch_off_RS_mapping_ij_a) ||
	  (csi->dipole[i_a].type_dipole() == 3 && !switch_off_RS_mapping_ai_k) ||
	  (csi->dipole[i_a].type_dipole() == 5 && !switch_off_RS_mapping_ai_b)){
	ax_psp_dipole(i_a);
      }
    }
    initialization_phasespace_subprocess_dipole_IS_RA();
  }

  // first if-statement should be redundant !!!
  if (csi->class_contribution_CS_real){
    if (!switch_off_RS_mapping){
      for (int i_a = 1; i_a < n_dipoles; i_a++){ac_tau_psp_dipole(i_a, MC_x_dipole_mapping[i_a]);}
      if (csi->type_contribution == "RRA" || csi->type_contribution == "RRJ"){initialization_fake_dipole_mapping_RRA();}
      initialization_phasespace_MC_x_dipole_RA();
    }
    else if (switch_off_RS_mapping){
      ac_tau_psp(0, MC_x_dipole_mapping[0]);
      logger << LOG_DEBUG << "MC_x_dipole_mapping[0].size() = " << MC_x_dipole_mapping[0].size() << endl;
      tau_MC_map = MC_x_dipole_mapping[0];
    }
  }

  logger << LOG_DEBUG << "switch_off_RS_mapping = " << switch_off_RS_mapping << endl;
  logger << LOG_DEBUG << "MC_n_channel = " << MC_n_channel << endl;
  logger << LOG_DEBUG << "tau_MC_map.size() = " << tau_MC_map.size() << endl;

  logger << LOG_DEBUG << "finished" << endl;
}



void phasespace_set::initialization_phasespace_subprocess(){
  Logger logger("phasespace_set::initialization_phasespace_subprocess");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  x_pdf.resize(3);
  //  vector<double> pdf_factor(3);

  logger << LOG_DEBUG_VERBOSE << "xb_max = " << xb_max << endl;
  logger << LOG_DEBUG_VERBOSE << "start_xbp_all.size() = " << start_xbp_all.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "start_xbs_all.size() = " << start_xbs_all.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "start_xbsqrts_all.size() = " << start_xbsqrts_all.size() << endl;

  start_xbp_all[0].resize(xb_max);
  start_xbs_all[0].resize(xb_max);
  start_xbsqrts_all[0].resize(xb_max);

  if (coll_choice == 0){
    fourvector p1(E, 0, 0, E), p2(E, 0, 0, -E);
    start_xbp_all[0][0] = p1 + p2;
    start_xbp_all[0][1] = p1;
    start_xbp_all[0][2] = p2;
    for (int i = 0; i < 3; i++){x_pdf[i] = 1.;}
    start_xbs_all[0][0] = start_xbp_all[0][0].m2();
    start_xbsqrts_all[0][0] = sqrt(start_xbs_all[0][0]);
    start_xbp_all[0][xb_max - 4] = start_xbp_all[0][0];
    start_xbs_all[0][xb_max - 4] = start_xbs_all[0][0];
    start_xbsqrts_all[0][xb_max - 4] = start_xbsqrts_all[0][0];
    start_xbp_all[0][3] = start_xbp_all[0][0];
    start_xbs_all[0][3] = start_xbs_all[0][0];
    start_xbsqrts_all[0][3] = start_xbsqrts_all[0][0];
    /*
      pdf_factor[0] = 1.;
      pdf_factor[1] = 1.;
      pdf_factor[2] = 0.;
    */
  }

  for (int xi = 1; xi <= csi->n_particle + 2; xi++){
    start_xbs_all[0][intpow(2, xi - 1)] = M2[abs(csi->type_parton[0][xi])];
    start_xbsqrts_all[0][intpow(2, xi - 1)] = M[abs(csi->type_parton[0][xi])];
  }

  sqrtsmin_opt = start_xbsqrts_all;
  smin_opt = start_xbs_all;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::initialization_phasespace_subprocess_optimization(){
  Logger logger("phasespace_set::initialization_phasespace_subprocess_optimization");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG << "sqrtsmin_opt[0].size() = " << sqrtsmin_opt[0].size() << endl;
  logger << LOG_DEBUG << "Before adaptation:  tau_0 = " << tau_0 << "   tau_0_s_had = " << tau_0_s_had << "   -> sqrt(s^_min) = " << 2 * sqrt(tau_0) * E << " (tau_0)" << " = " << sqrt(tau_0_s_had) << " (tau_0_s_had)" << endl;

  for (int i_x = 4; i_x < sqrtsmin_opt[0].size(); i_x += 4){
    if (sqrtsmin_opt[0][i_x] == start_xbsqrts_all[0][i_x]){}
    else if (sqrtsmin_opt[0][i_x] < start_xbsqrts_all[0][i_x]){sqrtsmin_opt[0][i_x] = start_xbsqrts_all[0][i_x];}
    else if (sqrtsmin_opt[0][i_x] > start_xbsqrts_all[0][i_x] && vectorbinary_from_binary(i_x).size() == 1){
      logger << LOG_FATAL << "phase-space cut is in contradiction to on-shell conditions of intermediate particle " << i_x << "!" << endl;
      exit(1);
    }
  }

  for (int i_x = 4; i_x < sqrtsmin_opt[0].size(); i_x += 4){
    if (vectorbinary_from_binary(i_x).size() > 1){
      double temp_sqrts_opt = max_subset_from_binary(sqrtsmin_opt[0], i_x);
      if (temp_sqrts_opt > sqrtsmin_opt[0][i_x]){sqrtsmin_opt[0][i_x] = temp_sqrts_opt;}
      smin_opt[0][i_x] = pow(sqrtsmin_opt[0][i_x], 2);
    }
  }
  if (tau_0_s_had < smin_opt[0][smin_opt[0].size() - 4]){
    tau_0_s_had = smin_opt[0][smin_opt[0].size() - 4];
    tau_0 = tau_0_s_had / s_had;
  }

  for (int i = 4; i < smin_opt[0].size(); i += 4){
    stringstream temp_ss;
    temp_ss << "sqrtsmin_opt[0][" << setw(3) << i << "] = " << setw(15) << setprecision(8) << sqrtsmin_opt[0][i] << "   smin_opt[0][" << setw(3) << i << "] = " << setw(15) << setprecision(8) << smin_opt[0][i] << endl;
    if (sqrtsmin_opt[0][i] != 0.){logger << LOG_INFO << temp_ss.str();}
    else {logger << LOG_DEBUG << temp_ss.str();}
  }
  logger.newLine(LOG_INFO);
  for (int i = 0; i < start_xbp_all[0].size(); i++){
    if (start_xbs_all[0][i] != 0. || start_xbsqrts_all[0][i] != 0.){
      logger << LOG_INFO << "xbs[" << setw(3) << right << i << "] = " << start_xbs_all[0][i] << "   " << "xbsqrts[" << setw(3) << right << i << "] = " << start_xbsqrts_all[0][i] << "   " << endl;
    }
  }
  logger.newLine(LOG_INFO);
  logger << LOG_INFO << "tau_0 = " << tau_0 << "   tau_0_s_had = " << tau_0_s_had << "   -> sqrt(s^_min) = " << 2 * sqrt(tau_0) * E << " (tau_0)" << " = " << sqrt(tau_0_s_had) << " (tau_0_s_had)" << endl;
  logger.newLine(LOG_INFO);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::initialization_phasespace_subprocess_dipole_IS_RA(){
  Logger logger("phasespace_set::initialization_phasespace_subprocess_dipole_IS_RA");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i_a = 1; i_a < n_dipoles; i_a++){
    MC_IS_name_type[i_a][6].push_back("x of dipole " + csi->dipole[i_a].name());
    MC_IS_name_type[i_a][7].push_back("z of dipole " + csi->dipole[i_a].name());
    MC_IS_name_type[i_a][8].push_back("phi of dipole " + csi->dipole[i_a].name());
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::initialization_phasespace_subprocess_dipole_RA(){
  Logger logger("phasespace_set::initialization_phasespace_subprocess_dipole_RA");
  logger << LOG_DEBUG << "started" << endl;

  x_pdf.resize(3);
  //  vector<double> pdf_factor(3);
  //  vector<double> p_2(3 + csi->n_particle);
  //  vector<double> sqp_2(3 + csi->n_particle);

  vector<vector<vector<double> > > dx_s_RA(n_dipoles);
  vector<vector<vector<double> > > dx_sqrts_RA(n_dipoles);
  for (int i_a = 0; i_a < dx_s_RA.size(); i_a++){
    dx_s_RA[i_a].resize(csi->dipole[i_a].type_parton().size(), vector<double> (1));
    dx_sqrts_RA[i_a].resize(csi->dipole[i_a].type_parton().size(), vector<double> (1));
  }

  start_xbp_all.resize(n_dipoles);
  start_xbs_all.resize(n_dipoles);
  start_xbsqrts_all.resize(n_dipoles);

  start_xbp_all[0].resize(xb_max);
  start_xbs_all[0].resize(xb_max);
  start_xbsqrts_all[0].resize(xb_max);
  for (int i_a = 1; i_a < n_dipoles; i_a++){
    start_xbp_all[i_a].resize(xb_max_dipoles);
    start_xbs_all[i_a].resize(xb_max_dipoles);
    start_xbsqrts_all[i_a].resize(xb_max_dipoles);
  }

  /*
  p_2.resize(3 + csi->n_particle);
  sqp_2.resize(3 + csi->n_particle);
  for (int i = 1; i < csi->n_particle + 3; i++){
    logger << LOG_DEBUG << "i = " << i << endl;
    logger << LOG_DEBUG << "csi->dipole[0].type_parton().size() = " << csi->dipole[0].type_parton().size() << endl;
    logger << LOG_DEBUG << "csi->dipole[0].type_parton()[" << i << "] = " << csi->dipole[0].type_parton()[i] << endl;
    logger << LOG_DEBUG << "M.size() = " << M.size() << endl;
    logger << LOG_DEBUG << "M2.size() = " << M2.size() << endl;
    logger << LOG_DEBUG << "M[abs(csi->dipole[0].type_parton()[" << i << ")] = " << csi->dipole[0].type_parton()[i] << "] = " << setw(23) << setprecision(15) << M[abs(csi->dipole[0].type_parton()[i])] << endl;
    logger << LOG_DEBUG << "M2[abs(csi->dipole[0].type_parton()[" << i << ")] = " << csi->dipole[0].type_parton()[i] << "] = " << setw(23) << setprecision(15) << M2[abs(csi->dipole[0].type_parton()[i])] << endl;

    p_2[i] = M2[abs(csi->dipole[0].type_parton()[i])];
    sqp_2[i] = M[abs(csi->dipole[0].type_parton()[i])];
    //    p_2[i] = M2[abs(csi->type_parton[0][i])];
    //    sqp_2[i] = M[abs(csi->type_parton[0][i])];
  }
  sqp_2[0] = accumulate(sqp_2.begin(), sqp_2.end(), 0.); // ppwwbb
  p_2[0] = pow(sqp_2[0], 2);

  logger << LOG_DEBUG << "p_2 started" << endl;
  */
    
  for (int i_a = 0; i_a < dx_s_RA.size(); i_a++){
    for (int i_b = 1; i_b < dx_s_RA[i_a].size(); i_b++){
      if (dx_s_RA[i_a][i_b].size() == 1){
	dx_s_RA[i_a][i_b][0] = M2[abs(csi->dipole[i_a].type_parton()[i_b])];
	dx_sqrts_RA[i_a][i_b][0] = M[abs(csi->dipole[i_a].type_parton()[i_b])];
      }
    }
  }


  logger << LOG_INFO << "definitions of particle momenta and masses" << endl;

  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int xbi = 0; xbi < start_xbp_all[i_a].size(); xbi++){
      start_xbp_all[i_a][xbi] = nullvector;
      start_xbs_all[i_a][xbi] = 0.;
      start_xbsqrts_all[i_a][xbi] = 0.;
    }
    int max_particles;
    if (i_a == 0){max_particles = csi->n_particle + 2 + 1;}
    else {max_particles = csi->n_particle + 2;}
    for (int xbi = 1; xbi < max_particles; xbi++){
      start_xbs_all[i_a][intpow(2, xbi - 1)] = dx_s_RA[i_a][xbi][0];
      start_xbsqrts_all[i_a][intpow(2, xbi - 1)] = dx_sqrts_RA[i_a][xbi][0];
    }
  }

  if (coll_choice == 0){
    fourvector p1(E, 0, 0, E), p2(E, 0, 0, -E);
    for (int i = 0; i < 3; i++){x_pdf[i] = 1.;}

    for (int i_a = 0; i_a < n_dipoles; i_a++){
      logger << LOG_DEBUG << i_a << endl;
      start_xbp_all[i_a][0] = p1 + p2;
      start_xbp_all[i_a][1] = p1;
      start_xbp_all[i_a][2] = p2;
      //      for (int i_p = 0; i_p < 3; i_p++){start_xbp_all[i_a][i_p] = start_xbp_all[0][i_p];}
      start_xbs_all[i_a][0] = start_xbp_all[0][0].m2();
      start_xbsqrts_all[i_a][0] = sqrt(start_xbs_all[i_a][0]);
      start_xbp_all[i_a][3] = start_xbp_all[i_a][0];
      start_xbs_all[i_a][3] = start_xbs_all[i_a][0];
      start_xbsqrts_all[i_a][3] = start_xbsqrts_all[i_a][0];
      if (i_a == 0){
	start_xbp_all[i_a][xb_max - 4] = start_xbp_all[i_a][0];
	start_xbs_all[i_a][xb_max - 4] = start_xbs_all[i_a][0];
	start_xbsqrts_all[i_a][xb_max - 4] = start_xbsqrts_all[i_a][0];
      }
      else {
	start_xbp_all[i_a][xb_max_dipoles - 4] = start_xbp_all[i_a][0];
	start_xbs_all[i_a][xb_max_dipoles - 4] = start_xbs_all[i_a][0];
	start_xbsqrts_all[i_a][xb_max_dipoles - 4] = start_xbsqrts_all[i_a][0];
      }
    }
    /*
    pdf_factor[0] = 1.;
    pdf_factor[1] = 1.;
    pdf_factor[2] = 0.;
    */
  }

  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i_x = 4; i_x < start_xbs_all[i_a].size(); i_x += 4){
      vector<int> temp_vb = vectorbinary_from_binary(i_x);
      logger << LOG_DEBUG << "start_xbsqrts_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbsqrts_all[i_a][i_x] << "   start_xbs_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbs_all[i_a][i_x] << endl;
      if (temp_vb.size() == 1 || start_xbs_all[i_a][i_x] != 0. || start_xbsqrts_all[i_a][i_x] != 0.){
	logger << LOG_DEBUG << "start_xbsqrts_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbsqrts_all[i_a][i_x] << "   start_xbs_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbs_all[i_a][i_x] << endl;
      }
    }
  }

  sqrtsmin_opt = start_xbsqrts_all;
  smin_opt = start_xbs_all;

  logger << LOG_DEBUG << "finished" << endl;
}

void phasespace_set::initialization_phasespace_subprocess_optimization_dipole_RA(){
  Logger logger("phasespace_set::initialization_phasespace_subprocess_dipole_optimization_RA");
  logger << LOG_DEBUG << "started" << endl;

  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i_x = 4; i_x < sqrtsmin_opt[i_a].size(); i_x += 4){
      if (sqrtsmin_opt[i_a][i_x] == start_xbsqrts_all[i_a][i_x]){}
      else if (sqrtsmin_opt[i_a][i_x] < start_xbsqrts_all[i_a][i_x]){sqrtsmin_opt[i_a][i_x] = start_xbsqrts_all[i_a][i_x];}
      else if (sqrtsmin_opt[i_a][i_x] > start_xbsqrts_all[i_a][i_x]){
	if (vectorbinary_from_binary(i_x).size() == 1){
	  logger << LOG_FATAL << "phase-space cut is in contradiction to on-shell conditions of intermediate particle " << i_x << "!" << endl;
	  exit(1);
	}
      }
    }
  }

  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i_x = 4; i_x < sqrtsmin_opt[i_a].size(); i_x += 4){
      if (vectorbinary_from_binary(i_x).size() > 1){
	double temp_sqrts_opt = max_subset_from_binary(sqrtsmin_opt[i_a], i_x);
	if (temp_sqrts_opt > sqrtsmin_opt[i_a][i_x]){sqrtsmin_opt[i_a][i_x] = temp_sqrts_opt;}
	smin_opt[i_a][i_x] = pow(sqrtsmin_opt[i_a][i_x], 2);
      }
    }
  }

  // debug output only ...
  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i = 4; i < smin_opt[i_a].size(); i += 4){
      stringstream temp_ss;
      temp_ss << "sqrtsmin_opt[" << i_a << "][" << setw(3) << i << "] = " << setw(15) << setprecision(8) << sqrtsmin_opt[i_a][i] << "   smin_opt[" << i_a << "][" << setw(3) << i << "] = " << setw(15) << setprecision(8) << smin_opt[i_a][i] << endl;
      if (sqrtsmin_opt[i_a][i] != 0.){logger << LOG_INFO << temp_ss.str();}
      else {logger << LOG_DEBUG << temp_ss.str();}
    }
  }
  logger.newLine(LOG_DEBUG);
  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i_x = 4; i_x < smin_opt[i_a].size(); i_x += 4){
      vector<int> temp_vb = vectorbinary_from_binary(i_x);
      if (temp_vb.size() == 1 || start_xbs_all[i_a][i_x] != 0. || start_xbsqrts_all[i_a][i_x] != 0.){
	logger << LOG_DEBUG << "start_xbsqrts_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbsqrts_all[i_a][i_x] << "   start_xbs_all[" << setw(2) << i_a << "][" << setw(3) << i_x << "] = " << left << setw(23) << setprecision(15) << start_xbs_all[i_a][i_x] << endl;
      }
    }
  }
  logger.newLine(LOG_DEBUG);
  // ... until here
  
  vector<vector<vector<double> > > dx_s_RA(n_dipoles);
  vector<vector<vector<double> > > dx_sqrts_RA(n_dipoles);
  for (int i_a = 0; i_a < dx_s_RA.size(); i_a++){
    dx_s_RA[i_a].resize(csi->dipole[i_a].type_parton().size(), vector<double> (1));
    dx_sqrts_RA[i_a].resize(csi->dipole[i_a].type_parton().size(), vector<double> (1));
  }
  for (int i_a = 0; i_a < dx_s_RA.size(); i_a++){
    for (int i_b = 1; i_b < dx_s_RA[i_a].size(); i_b++){
      if (dx_s_RA[i_a][i_b].size() == 1){
	dx_s_RA[i_a][i_b][0] = M2[abs(csi->dipole[i_a].type_parton()[i_b])];
	dx_sqrts_RA[i_a][i_b][0] = M[abs(csi->dipole[i_a].type_parton()[i_b])];
      }
    }
  }


  dipole_sinx_min.resize(n_dipoles, 0.);
  for (int i_a = 0; i_a < dx_s_RA.size(); i_a++){
    for (int ib = 3; ib < dx_s_RA[i_a].size(); ib++){dipole_sinx_min[i_a] += M[abs(csi->dipole[i_a].type_parton()[ib])];}
    dipole_sinx_min[i_a] = pow(dipole_sinx_min[i_a], 2);
    logger << LOG_DEBUG << "smin_opt (only from masses):   dipole_sinx_min[" << setw(2) << i_a << "] = " << dipole_sinx_min[i_a] << endl;
    dipole_sinx_min[i_a] = smin_opt[i_a][smin_opt[i_a].size() - 4];
    logger << LOG_DEBUG << "smin_opt (from smin_opt[" << i_a << "][" << smin_opt[i_a].size() - 4 << "]):    dipole_sinx_min[" << setw(2) << i_a << "] = " << dipole_sinx_min[i_a] << endl;
  }

  logger << LOG_INFO << "tau_0 = " << tau_0 << "   tau_0_s_had = " << tau_0_s_had << "   -> sqrt(s^_min) = " << 2 * sqrt(tau_0) * E << " (tau_0)" << " = " << sqrt(tau_0_s_had) << " (tau_0_s_had)" << endl;
  // Not only tau_0, but also tau_0_s_had need to be updated !!!
  if (tau_0_s_had < smin_opt[0][smin_opt[0].size() - 4]){
    tau_0_s_had = smin_opt[0][smin_opt[0].size() - 4];
    tau_0 = tau_0_s_had / s_had;
  }
  logger << LOG_INFO << "tau_0 = " << tau_0 << "   tau_0_s_had = " << tau_0_s_had << "   -> sqrt(s^_min) = " << 2 * sqrt(tau_0) * E << " (tau_0)" << " = " << sqrt(tau_0_s_had) << " (tau_0_s_had)" << endl;
  logger.newLine(LOG_INFO);

  xbp_all = start_xbp_all;
  xbs_all = start_xbs_all;
  xbsqrts_all = start_xbsqrts_all;

  corrected_xbp_all = start_xbp_all;
  corrected_xbs_all = start_xbs_all;
  corrected_xbsqrts_all = start_xbsqrts_all;

  logger << LOG_DEBUG << "finished" << endl;
}


// ???
void phasespace_set::initialization_phasespace_MC_tau(){
  Logger logger("phasespace_set::initialization_phasespace_MC_tau");
  logger << LOG_DEBUG << "started" << endl;

  // only non-output statements in initialization_phasespace_MC_tau (which are not repeated here):
  logger << LOG_DEBUG << "tau_MC_map.size() = " << tau_MC_map.size() << endl;
  for (int i_m = tau_MC_map.size() - 1; i_m > 0; i_m--){
    if (map_Gamma[abs(tau_MC_map[i_m])] == 0.){tau_MC_map.erase(tau_MC_map.begin() + i_m);}
    else if (2 * sqrt(tau_0) * E > M[abs(tau_MC_map[i_m])]){tau_MC_map.erase(tau_MC_map.begin() + i_m);}
  }
  logger << LOG_DEBUG << "tau_MC_map.size() = " << tau_MC_map.size() << endl;
  // initialization of initial-state multichannel (MC) related tau parameters
  // to be shifted elsewhere
  n_tau_MC_channel = tau_MC_map.size();
  tau_MC_tau_gamma.resize(n_tau_MC_channel, vector<double> (input_IS_tau.n_gridsize));

  for (int j = 0; j < tau_MC_tau_gamma[0].size(); j++){
    double random_tau = (double(j + 1)) / double(input_IS_tau.n_gridsize);
    tau_MC_tau_gamma[0][j] = h_propto_pot(random_tau, tau_0, exp_pdf);
  }

  for (int j = 0; j < tau_MC_tau_gamma[0].size(); j++){logger << LOG_DEBUG << "tau_MC_tau_gamma[0][" << j << "]" << " = " << setprecision(15) << setw(23) << tau_MC_tau_gamma[0][j] << endl;}

  // to be shifted elsewhere - used so far somewhere...
  if (input_MC_tau.switch_optimization == -1){tau_MC_map.resize(1);}

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_phasespace_MC_x_dipole_RA(){
  Logger logger("phasespace_set::initialization_phasespace_MC_x_dipole_RA");
  logger << LOG_DEBUG << "started" << endl;

  // determine tau_MC_map from MC_x_dipole_mapping (including 0!)
  for (int i_a = 0; i_a < n_dipoles; i_a++){
    for (int i_m = MC_x_dipole_mapping[i_a].size() - 1; i_m > 0; i_m--){
      logger << LOG_INFO << "MC_x_dipole_mapping[" << i_a << "][" << i_m << "] = " << MC_x_dipole_mapping[i_a][i_m] << endl;
      logger << LOG_INFO << "psi_M[" << abs(MC_x_dipole_mapping[i_a][i_m]) << "] = " << M[abs(MC_x_dipole_mapping[i_a][i_m])] << endl;
      logger << LOG_INFO << "psi_Gamma[" << abs(MC_x_dipole_mapping[i_a][i_m]) << "] = " << Gamma[abs(MC_x_dipole_mapping[i_a][i_m])] << endl;
      logger << LOG_INFO << "psi_map_Gamma[" << abs(MC_x_dipole_mapping[i_a][i_m]) << "] = " << map_Gamma[abs(MC_x_dipole_mapping[i_a][i_m])] << endl;
      if (map_Gamma[abs(MC_x_dipole_mapping[i_a][i_m])] == 0.){MC_x_dipole_mapping[i_a].erase(MC_x_dipole_mapping[i_a].begin() + i_m);}
      else if (2 * sqrt(tau_0) * E > M[abs(MC_x_dipole_mapping[i_a][i_m])]){MC_x_dipole_mapping[i_a].erase(MC_x_dipole_mapping[i_a].begin() + i_m);}
      // leads to problems if M_res < smin !!! needs to be investigated !!!
      //      else if (2 * sqrt(tau_0) * E > M[abs(MC_x_dipole_mapping[i_a][i_m])] + 5 * map_Gamma[abs(MC_x_dipole_mapping[i_a][i_m])]){MC_x_dipole_mapping[i_a].erase(MC_x_dipole_mapping[i_a].begin() + i_m);}
    }

    // to add MC_x_dipole mappings also to MC_tau
    for (int i_m = 0; i_m < MC_x_dipole_mapping[i_a].size(); i_m++){
      int flag = tau_MC_map.size();
      for (int j_m = 0; j_m < tau_MC_map.size(); j_m++){if (MC_x_dipole_mapping[i_a][i_m] == tau_MC_map[j_m]){flag = j_m; break;}}
      if (flag == tau_MC_map.size()){tau_MC_map.push_back(MC_x_dipole_mapping[i_a][i_m]);}
    }
  }

  // output of tau_MC_map and MC_x_dipole_mapping - not needed at all, I guess ... !!!
  stringstream temp_ss;
  temp_ss << "tau_MC_map = ";
  for (int i_m = 0; i_m < tau_MC_map.size(); i_m++){temp_ss << setw(4) << tau_MC_map[i_m];}
  logger << LOG_DEBUG << temp_ss.str() << endl;
  for (int i_a = 0; i_a < n_dipoles; i_a++){
    stringstream temp_ss;
    temp_ss << "MC_x_dipole_mapping[" << setw(2) << i_a << "] = ";
    for (int i_m = 0; i_m < MC_x_dipole_mapping[i_a].size(); i_m++){temp_ss << setw(4) << MC_x_dipole_mapping[i_a][i_m];}
    logger << LOG_DEBUG << temp_ss.str() << endl;
  }
  // ... until here.
  
  MC_x_dipole.resize(n_dipoles);
  for (int i_a = 1; i_a < n_dipoles; i_a++){
    if (input_MC_x_dipole.switch_optimization == -1){MC_x_dipole_mapping[i_a].resize(1);}

    stringstream temp_ss;
    temp_ss << "MC_x_dipole_" << i_a;
    input_MC_x_dipole.name = temp_ss.str();
    input_MC_x_dipole.n_channel = MC_x_dipole_mapping[i_a].size();
    //    MC_x_dipole[i_a] = multichannel_set(input_MC_x_dipole, filename_MCweight, filename_MCweight_in_contribution);
    MC_x_dipole[i_a] = multichannel_set(input_MC_x_dipole, filename_weight_writeout, filename_weight_readin);
  }

  logger << LOG_DEBUG << "finished" << endl;
}

void phasespace_set::initialization_phasespace_IS(){
  Logger logger("phasespace_set::initialization_phasespace_IS");
  logger << LOG_DEBUG << "started" << endl;

  g_z_coll.resize(3); // could be deleted later... !!!

  z1z2_opt_end = -1; // not needed here - only in CA (or qTsubtraction)

  initialization_phasespace_MC_tau();

  input_MC_tau.n_channel = tau_MC_map.size();
  //  MC_tau = multichannel_set(input_MC_tau, filename_MCweight, filename_MCweight_in_contribution);
  MC_tau = multichannel_set(input_MC_tau, filename_weight_writeout, filename_weight_readin);
  // Deactivate "IS_tau" if tau is fixed:
  if (start_xbs_all[0][xb_max - 4] != 0.){input_IS_tau.switch_optimization = -1;}
  IS_tau = importancesampling_set(input_IS_tau, filename_weight_writeout, filename_weight_readin);
  //  IS_tau = importancesampling_set(input_IS_tau, filename_tauweight, filename_tauweight_in_contribution);
  IS_x1x2 = importancesampling_set(input_IS_x1x2, filename_weight_writeout, filename_weight_readin);
  //  IS_x1x2 = importancesampling_set(input_IS_x1x2, filename_x1x2weight, filename_x1x2weight_in_contribution);
 
  if (csi->class_contribution_collinear){
    IS_z1z2.resize(3);
    for (int i_c = 1; i_c < 3; i_c++){
      stringstream temp_name_ss;
      temp_name_ss << "IS_z1z2[" << i_c << "]";
      string temp_name = temp_name_ss.str();
      input_IS_z1z2.name = temp_name;
      IS_z1z2[i_c] = importancesampling_set(input_IS_z1z2, filename_weight_writeout, filename_weight_readin);
      //      IS_z1z2[i_c] = importancesampling_set(input_IS_z1z2, filename_z1z2weight[i_c], filename_z1z2weight_in_contribution[i_c]);
    }

    z_coll.resize(3);
    all_xz_coll_pdf.resize(3, vector<double> (3));
    g_z_coll.resize(3);
  }

  if (switch_resummation){IS_qTres = importancesampling_set(input_IS_qTres, filename_weight_writeout, filename_weight_readin);}
  //  if (switch_resummation){IS_qTres = importancesampling_set(input_IS_qTres, filename_qTresweight, filename_qTresweight_in_contribution);}


  // determination of global variable 'active_optimization':
  // active_optimization:
  // active_optimization = -1: no optimization of any mapping (very unlikely)
  // active_optimization =  0: no active optimization (like in standard runs)
  // active_optimization =  1: at least one active optimization (like in grid runs)
  active_optimization = -1;
  if (MC_phasespace.active_optimization != -1){if (MC_phasespace.active_optimization > active_optimization){active_optimization = MC_phasespace.active_optimization;}}
  logger << LOG_DEBUG << "MC_phasespace.active_optimization = " << MC_phasespace.active_optimization << endl;
  if (MC_tau.active_optimization != -1){if (MC_tau.active_optimization > active_optimization){active_optimization = MC_tau.active_optimization;}}
  logger << LOG_DEBUG << "MC_tau.active_optimization = " << MC_tau.active_optimization << endl;
  if (csi->class_contribution_CS_real){
    for (int i_a = 1; i_a < MC_x_dipole.size(); i_a++){
      logger << LOG_DEBUG << "MC_x_dipole[" << i_a << "].active_optimization = " << MC_x_dipole[i_a].active_optimization << endl;
      if (MC_x_dipole[i_a].active_optimization != -1){if (MC_x_dipole[i_a].active_optimization > active_optimization){active_optimization = MC_x_dipole[i_a].active_optimization;}}
    }
  }
  
  // IS_phasespace is treated in randommanager !!! ??? not any longer !!!
  
  logger << LOG_DEBUG << "IS_tau.active_optimization = " << IS_tau.active_optimization << endl;
  if (IS_tau.active_optimization != -1){if (IS_tau.active_optimization > active_optimization){active_optimization = IS_tau.active_optimization;}}
  logger << LOG_DEBUG << "IS_x1x2.active_optimization = " << IS_x1x2.active_optimization << endl;
  if (IS_x1x2.active_optimization != -1){if (IS_x1x2.active_optimization > active_optimization){active_optimization = IS_x1x2.active_optimization;}}
  if (csi->class_contribution_collinear){
    for (int i_c = 1; i_c < 3; i_c++){
      logger << LOG_DEBUG << "IS_z1z2[" << i_c << "].active_optimization = " << IS_z1z2[i_c].active_optimization << endl;
      if (IS_z1z2[i_c].active_optimization != -1){if (IS_z1z2[i_c].active_optimization > active_optimization){active_optimization = IS_z1z2[i_c].active_optimization;}}
    }
  }
  if (switch_resummation){
    logger << LOG_DEBUG << "IS_qTres.active_optimization = " << IS_qTres.active_optimization << endl;
    if (IS_qTres.active_optimization != -1){if (IS_qTres.active_optimization > active_optimization){active_optimization = IS_qTres.active_optimization;}}
  }

  // determination of global variable 'end_optimization' ???

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_phasespace_IS_QT(){
  Logger logger("phasespace_set::initialization_phasespace_IS_QT");
  logger << LOG_DEBUG << "started" << endl;

  QT_eps = 0;
  QT_random_IS = 0.;

  QT_qt2 = 0.;
  QT_jacqt2 = 0.;
  QT_random_qt2 = 0.;

  ///  IS_qTres = ... here ???

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_MC_rng(){
  Logger logger("phasespace_set::initialization_MC_rng");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  //  n_random_MC: 1 for the channel selection, (3n - 4) for the n-particle phasespace:
  n_random_MC = 1 + no_random;

  int temp_switch_IS_MC = -1;
  for (int i_s = 0; i_s < input_IS.size(); i_s++){if (input_IS[i_s]->name.substr(0, 5) == "IS_PS" && input_IS[i_s]->switch_optimization != -1){temp_switch_IS_MC = 0;}}
  if (temp_switch_IS_MC != -1){n_random_MC += no_random;}
  logger << LOG_INFO << "input_MC_phasespace.switch_optimization      = " << setw(3) << input_MC_phasespace.switch_optimization << "   " << setw(20) << "n_random_MC" << " = " << n_random_MC << endl;
  // Should be 0 if input_MC_phasespace.switch_optimization == -1 !!!
  n_random_per_psp += n_random_MC;

  //  n_random_MC_tau: 1 for the MC_tau channel selection:
  //  n_random_MC_tau: 2 set instead in order to reproduce old results !!!
  n_random_MC_tau = 2;
  logger << LOG_INFO << "input_MC_tau.switch_optimization  = " << setw(3) << input_MC_tau.switch_optimization << "   " << setw(20) << "n_random_MC_tau" << " = " << n_random_MC_tau << endl;
  // Should be 0 if input_MC_tau.switch_optimization == -1 !!!
  random_MC_tau.resize(n_random_MC_tau);
  n_random_per_psp += n_random_MC_tau;

  //  n_random_tau: 1 for the tau IS selection, 1 random number for the tau value:
  n_random_tau = 2;
  logger << LOG_INFO << "input_IS_tau.switch_optimization  = " << setw(3) << input_IS_tau.switch_optimization << "   " << setw(20) << "n_random_tau   " << " = " << n_random_tau << endl;
  // Should be 1 if input_IS_tau.switch_optimization == -1 !!!
  random_tau.resize(n_random_tau);
  n_random_per_psp += n_random_tau;

  //  n_random_x1x2: 1 for the x1x2 IS selection, 1 random number for the x1x2 value:
  n_random_x12 = 2;
  logger << LOG_INFO << "input_IS_x1x2.switch_optimization = " << setw(3) << input_IS_x1x2.switch_optimization << "   " << setw(20) << "n_random_x12  " << " = " << n_random_x12 << endl;
  // Should be 1 if input_IS_x1x2.switch_optimization == -1 !!!
  random_x12.resize(n_random_x12);
  // number of random numbers for x12 mapping:
  n_random_per_psp += n_random_x12;


  // new: Should be used for all collinear-emission contributions (i.e. with z1/z2 emissions):
  random_z.resize(3, vector<double> (2));
  if (csi->class_contribution_collinear){
    //  n_random_z: 1 for the z1/z2 IS selection, 1 random number for the z1/z2 value:
    n_random_z = 2;
    logger << LOG_INFO << "input_IS_z1z2.switch_optimization = " << setw(3) << input_IS_z1z2.switch_optimization << "   " << setw(20) << "n_random_z     " << " = " << n_random_z << " * 2 " << endl;
    for (int i_z = 1; i_z < 3; i_z++){
      random_z[i_z].resize(n_random_z);
      // number of random numbers for z1/2 mapping:
      n_random_per_psp += n_random_z;
    }
  }

  // in case of resummation only !!!
  if (switch_resummation){
    n_random_qTres = 0; // default !!!
    logger << LOG_INFO << "input_IS_qTres.switch_optimization = " << setw(3) << input_IS_qTres.switch_optimization << "   " << setw(20) << "n_random_qTres  " << " = " << n_random_qTres << endl;
    // Should be 1 if switch_IS_qTres == -1 !!!
    random_qTres.resize(n_random_qTres);
    // number of random numbers for qTres mapping:
    n_random_per_psp += n_random_qTres;
  }

  logger << LOG_INFO << "n_random_per_psp = " << n_random_per_psp << endl;

  rng.initialization(n_random_per_psp, n_shift_run, switch_off_random_generator);
  rng.psi = this;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void phasespace_set::initialization_MC(){
  Logger logger("phasespace_set::initialization_MC");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  initialization_phasespace_IS();

  g_tot = 0.;
  g_MC = 0.;
  if (coll_choice){
    g_pdf = 0.;
    g_tau = 0.;
    g_x1x2 = 0.;
  }
  else {
    g_pdf = 1.;
    g_tau = 1.;
    g_x1x2 = 1.;
  }
  //  g_global_NWA = 1.;

  MC_g_IS_global = 1.;

  //  MC_sum_w_channel.resize(MC_n_channel, 0.);
  //  MC_g_IS_channel.resize(MC_n_channel, 0.);
  //  random_manager = randommanager(*this);

  randommanager *new_random_manager = new randommanager(*this);
  random_manager = *new_random_manager;
  delete new_random_manager;
  // to avoid memory leak ???
  //  random_manager = new randommanager(*this);

  logger << LOG_DEBUG << "switch_IS_mode_phasespace = " << switch_IS_mode_phasespace << endl;
  if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 2){
    /*// options should be removed !!!
    // create random variables for phase space integration with grids for each variable in each channel (MC_n_channel * (3n - 4))
    random_manager.random_psp.resize(no_random * MC_n_channel);
    random_psp.resize(no_random * MC_n_channel);
    ///    phasespace_randoms.resize(no_random * MC_n_channel);
    string tmp_name;
    ostringstream convert;
    for (int i = 0; i < no_random * MC_n_channel; i++) {
      convert.str(string());
      convert << "r" << i;
      tmp_name = convert.str();
      logger << LOG_DEBUG << "initialize randomvariable " << i << endl;
      // needs to be replaced by new input !!!
      random_manager.random_psp[i] = randomvariable(n_IS_events, switch_IS_MC, n_IS_steps, n_IS_gridsize, tmp_name);
      ///      phasespace_randoms[i] = new randomvariable(n_IS_events, switch_IS_MC, n_IS_steps, n_IS_gridsize, tmp_name);
      ///      random_manager.register_variable(phasespace_randoms[i], true);
      random_manager.initialization_random_psp(i);
      random_psp[i] = & random_manager.random_psp[i];
      random_psp[i]->initialization();
      
      ///      random_manager.register_variable(phasespace_randoms[i]);
    }
    //    container_IS_switch.resize(container_IS_name.size(), 0);

    // Seems to be required, although it is set already in randommanager !!! ???
    ///    random_manager.psi =& *this;
    //    psi.phasespace_randoms=&phasespace_randoms;
    */
  }
  else if (switch_IS_mode_phasespace == 3 || switch_IS_mode_phasespace == 4){
    // create random variables for phase space integration with one grid only for each mapping (propagator, t-channel, etc.)

    map<size_t, size_t> map_MC_IS_type_to_input_IS;
    for (int i_s = 0; i_s < input_IS.size(); i_s++){
      if (input_IS[i_s]->name == input_IS_PS_p.name){map_MC_IS_type_to_input_IS[0] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_f.name){map_MC_IS_type_to_input_IS[1] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_t_t.name){map_MC_IS_type_to_input_IS[2] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_t_phi.name){map_MC_IS_type_to_input_IS[3] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_d_costheta.name){map_MC_IS_type_to_input_IS[4] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_d_phi.name){map_MC_IS_type_to_input_IS[5] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_dipole_xy.name){map_MC_IS_type_to_input_IS[6] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_dipole_uvz.name){map_MC_IS_type_to_input_IS[7] = i_s;}
      if (input_IS[i_s]->name == input_IS_PS_dipole_phi.name){map_MC_IS_type_to_input_IS[8] = i_s;}
    }

    for (int i_s = 0; i_s < 6; i_s++){logger << LOG_DEBUG << "map_MC_IS_type_to_input_IS[" << i_s << "] = " << map_MC_IS_type_to_input_IS[i_s] << endl;}
    
    random_manager.random_psp.resize(MC_IS_name_list.size());
    random_psp.resize(MC_IS_name_list.size());

    string tmp_name;
    ostringstream convert;

   /// new version which allows different grid sizes for different observables:
    int x_r = 0;
    for (int i_a = 0; i_a < MC_IS_name_type.size(); i_a++){
      for (int i_t = 0; i_t < MC_IS_name_type[i_a].size(); i_t++){
	for (int i_i = 0; i_i < MC_IS_name_type[i_a][i_t].size(); i_i++){
	  if (x_r != map_type_to_list_MC_IS[vector<int> {i_a, i_t, i_i}]){logger << LOG_ERROR << "map_type_to_list_MC_IS[" << i_a << "][" << i_t << "][" << i_i << "] is not counted correctly!" << endl;}
	  //	  x_r -> map_type_to_list_MC_IS[i_a][i_t][i_i] !!!
	  convert.str(string());
	  convert << "yr" << x_r;
	  tmp_name = convert.str() + "_" + MC_IS_name_type[i_a][i_t][i_i];
	  ///	  random_psp[x_r] = new randomvariable(n_IS_events, switch_IS_MC, n_IS_steps, n_IS_gridsize_all[i_t], tmp_name);
	  //	  random_manager.random_psp[x_r] = randomvariable(n_IS_events, switch_IS_MC, n_IS_steps, n_IS_gridsize_all[i_t], tmp_name);
	  // new implementation: use importantcesampling_set here as well !!!
	  logger << LOG_DEBUG << "map_MC_IS_type_to_input_IS.size() = " << map_MC_IS_type_to_input_IS.size() << "   i_t = " << i_t << endl;
	  logger << LOG_DEBUG << "input_IS.size() = " << input_IS.size() << "   map_MC_IS_type_to_input_IS[" << i_t << "] = " << map_MC_IS_type_to_input_IS[i_t] << endl;

	  randomvariable *new_random_variable = new randomvariable(*input_IS[map_MC_IS_type_to_input_IS[i_t]], tmp_name, &random_manager);
	  random_manager.random_psp[x_r] = *new_random_variable;
	  delete new_random_variable;
	  // to avoid memory leak ???
	  //	  random_manager.random_psp[x_r] = new randomvariable(*input_IS[map_MC_IS_type_to_input_IS[i_t]], tmp_name, &random_manager);
	  
	  random_manager.initialization_random_psp(x_r);
	  random_psp[x_r] = & random_manager.random_psp[x_r];

	  x_r++;
	}
      }
    }
  }
  else {r.resize(no_random + 1);}
  
  logger << LOG_DEBUG << "definitions of integration variables" << endl;

  // Why is input_IS_tau...., etc. check here, and not IS_tau.... ???
  
  // input_IS_tau.switch_optimization
  // input_IS_tau.switch_optimization = 0: No IS optimization of partonic CMS energy.
  // input_IS_tau.switch_optimization = 1: Basic tauweights are taken; optimization is done according to parameters ...
  // input_IS_tau.switch_optimization = 2: tauweights read in from 'filename_tauweight_in_contribution'; no further optimization
  // input_IS_tau.switch_optimization = 3: tauweights read in from 'filename_tauweight_in_contribution'; further optimization according to parameters ...
  if (input_IS_tau.switch_optimization == 2 || input_IS_tau.switch_optimization == 3){
    ///    logger << LOG_INFO << "filename_tauweight_in_contribution = " << filename_tauweight_in_contribution << endl;
    IS_tau.readin_optimization();
    //    IS_tau.readin_IS_optimization();
  }

  // input_IS_x1x2.switch_optimization
  // input_IS_x1x2.switch_optimization = 0: No IS optimization of x1x2 mappings.
  // input_IS_x1x2.switch_optimization = 1: Basic x1x2weights are taken; optimization is done according to parameters ...
  // input_IS_x1x2.switch_optimization = 2: x1x2weights read in from 'filename_x1x2weight_in_contribution'; no further optimization
  // input_IS_x1x2.switch_optimization = 3: x1x2weights read in from 'filename_x1x2weight_in_contribution'; further optimization according to parameters ...
  if (input_IS_x1x2.switch_optimization == 2 || input_IS_x1x2.switch_optimization == 3){
    ///    logger << LOG_INFO << "filename_x1x2weight_in_contribution = " << filename_x1x2weight_in_contribution << endl;
    IS_x1x2.readin_optimization();
    //    IS_x1x2.readin_IS_optimization();
  }
  logger << LOG_DEBUG << "initialization of x1x2weight optimization finished" << endl;

  if (csi->class_contribution_collinear){
    //    for (int i_c = 1; i_c < 3; i_c++){if (input_IS_z1z2[i_c].switch_optimization == 2 || input_IS_z1z2[i_c].switch_optimization == 3){IS_z1z2[i_c].readin_optimization();}}
    for (int i_c = 1; i_c < 3; i_c++){if (input_IS_z1z2.switch_optimization == 2 || input_IS_z1z2.switch_optimization == 3){IS_z1z2[i_c].readin_optimization();}}
    logger << LOG_DEBUG << "initialization of z1z2weight optimization finished" << endl;
  }

  // in case of resummation only !!!
  if (switch_resummation){
    if (input_IS_qTres.switch_optimization == 2 || input_IS_qTres.switch_optimization == 3){
      ///      logger << LOG_INFO << "filename_qTresweight_in_contribution = " << filename_qTresweight_in_contribution << endl;
      IS_qTres.readin_optimization();
      //      IS_qTres.readin_IS_optimization();
    }
    logger << LOG_DEBUG << "initialization of qTresweight optimization finished" << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



