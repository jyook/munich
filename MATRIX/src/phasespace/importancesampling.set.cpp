#include "header.hpp"

importancesampling_input::importancesampling_input(){}

ostream & operator << (ostream & s, const importancesampling_input & IS){
  s << left << endl;
  s << setw(30) << "name" << " = " << IS.name << endl;
  s << setw(30) << "switch_optimization" << " = " << IS.switch_optimization << endl;
  s << endl;
  s << setw(30) << "n_gridsize" << " = " << IS.n_gridsize << endl;
  s << setw(30) << "n_optimization_step" << " = " << IS.n_optimization_step << endl;
  s << setw(30) << "n_optimization_step_extra" << " = " << IS.n_optimization_step_extra << endl;
  s << setw(30) << "n_event_per_step" << " = " << IS.n_event_per_step << endl;
  s << setw(30) << "threshold_optimization" << " = " << IS.threshold_optimization << endl;
  s << setw(30) << "switch_mode_optimization" << " = " << IS.switch_mode_optimization << endl;
  s << setw(30) << "switch_validation_optimization" << " = " << IS.switch_validation_optimization << endl;
  s << setw(30) << "switch_minimum_weight" << " = " << IS.switch_minimum_weight << endl;
  s << setw(30) << "switch_smearing_level" << " = " << IS.switch_smearing_level << endl;
  s << endl;
  s << setw(30) << "weight_adaptation_exponent" << " = " << IS.weight_adaptation_exponent << endl;
  s << setw(30) << "reserved_minimum_weight" << " = " << IS.reserved_minimum_weight << endl;
  s << setw(30) << "alpha_maximum_weight" << " = " << IS.alpha_maximum_weight << endl;
   
  return s;
}


importancesampling_set::importancesampling_set(){}

importancesampling_set::importancesampling_set(const importancesampling_input & input, string _filename, string _filename_readin){
  Logger logger("importancesampling_set::importancesampling_set (input) " + input.name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  name = input.name;
  switch_optimization = input.switch_optimization;
  
  n_gridsize = input.n_gridsize;
  n_optimization_step = input.n_optimization_step;
  n_event_per_step = input.n_event_per_step;

  switch_validation_optimization = input.switch_validation_optimization;
  // switch_minimum_weight == 0: Weights below limit_minimum_weight are set to zero (after last iteration step - never applied).
  // switch_minimum_weight == 1: Weights below limit_minimum_weight are set to limit_minimum_weight.
  switch_minimum_weight = input.switch_minimum_weight;
  reserved_minimum_weight = input.reserved_minimum_weight;
  alpha_reserved_min = reserved_minimum_weight / n_gridsize;
  alpha_maximum_weight = input.alpha_maximum_weight;

  switch_smearing_level = input.switch_smearing_level;

  is_gridsize_power_of_two = true;
  int rest = n_gridsize;
  while (rest > 1){
    if (rest % 2 == 1){is_gridsize_power_of_two = false; break;}
    else {rest /= 2;}
  }
  logger << LOG_DEBUG << "n_gridsize = " << n_gridsize << "   is_gridsize_power_of_two = " << is_gridsize_power_of_two << endl;
  
  if (input.switch_optimization == 0 || input.switch_optimization == 2){
    active_optimization = 0;
    end_optimization = 1;
  }
  else if (input.switch_optimization == 1 || input.switch_optimization == 3){
    active_optimization = 1;
    end_optimization = 0;
  }
  else if (input.switch_optimization == -1){
    active_optimization = -1;
    end_optimization = -1;
  }
  else {logger << LOG_FATAL << "No valid value of switch_optimization chosen." << endl; exit(1);}

  threshold_optimization = input.threshold_optimization;
  switch_mode_optimization = input.switch_mode_optimization;
  weight_adaptation_exponent = input.weight_adaptation_exponent;

  filename = _filename;
  filename_readin = _filename_readin;

  if (n_gridsize == 1){end_optimization = 1;} // check if output has to be created immediately !!!

  counter_minimum_weight = 0;
  alpha.resize(n_gridsize, 1. / double(n_gridsize));
  beta.resize(n_gridsize, 0.);
  if (n_gridsize > 0){
    beta[0] = alpha[0];
    for (int i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}
  }

  n_acc = 0;
  n_rej = 0;
  
  n_acc_channel.resize(n_gridsize, 0);
  n_rej_channel.resize(n_gridsize, 0);

  sum_channel_weight.resize(n_gridsize, 0.);
  sum_channel_weight2.resize(n_gridsize, 0.);

  // new:
  //  w_channel_sum.resize(n_gridsize, 0.);
  w_channel_av.resize(n_gridsize, 0.);
  
  c_w_channel_sum.resize(n_gridsize);
  c_w_norm = 0.;

  alpha_it.resize(n_optimization_step, vector<double> (n_gridsize, 0.));
  diff_w.resize(n_optimization_step, 0.);
      
  i_alpha_it = 0;
  x_alpha_it_min = 0;

  logger << LOG_DEBUG << *this << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

///////////////////////
//  access elements  //
///////////////////////

///////////////
//  methods  //
///////////////

void importancesampling_set::vegasgrid_average(){
  Logger logger("importancesampling_set::vegasgrid_average " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<double> alpha_new(alpha.size());

  if (switch_smearing_level == 1){
    if (alpha_new.size() == 1){
      logger << LOG_FATAL << "switch_smearing_level = " << switch_smearing_level << "  with  alpha_new.size() = " << alpha_new.size() << "  should never happen !" << endl;
      alpha_new[0] = 1.;
    }
    else {
      alpha_new[0] = .75 * alpha[0] + .25 * alpha[1];
      for (int i_c = 1; i_c < alpha.size() - 1; i_c++){
	alpha_new[i_c] = .5 * alpha[i_c] + .25 * alpha[i_c - 1] + .25 * alpha[i_c + 1];
      }
      alpha_new[alpha.size() - 1] = .75 * alpha[alpha.size() - 1] + .25 * alpha[alpha.size() - 2];
    }
  }
  else {
    logger << LOG_FATAL << "switch_smearing_level = " << switch_smearing_level << "  has not been defined." << endl; exit(1);
  }

  alpha = alpha_new;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::psp_IS_optimization(double & this_psp_weight, double & this_psp_weight2){
  Logger logger("importancesampling_set::psp_IS_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != 1){return;}
  if (end_optimization != 0){return;}

  n_acc++;
  n_acc_channel[channel]++;

  if (switch_mode_optimization == 0 || switch_mode_optimization == 2 || switch_mode_optimization == 3){
    sum_channel_weight[channel] += this_psp_weight;
    sum_channel_weight2[channel] += this_psp_weight2;
  }
  else if (switch_mode_optimization == 1){
    sum_channel_weight[channel] += abs(this_psp_weight);
    sum_channel_weight2[channel] += this_psp_weight2;
  }
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::step_IS_optimization(int i_step_mode){
  Logger logger("importancesampling_set::step_IS_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "end_optimization = " << end_optimization << "   active_optimization = " << active_optimization << "   i_step_mode = " << i_step_mode << "   n_event_per_step = " << n_event_per_step << endl;

  if (end_optimization != 0){return;}
  if (active_optimization != 1){return;}
  if (i_step_mode % n_event_per_step != 0 || i_step_mode == 0){return;}

  alpha_it[i_alpha_it] = alpha;

  int count = 0;
  for (int i_c = 0; i_c < alpha.size(); i_c++){
    if (switch_mode_optimization == 0){
      if (n_acc_channel[i_c] + n_rej_channel[i_c] > 1){w_channel_av[i_c] = sqrt(sum_channel_weight2[i_c] - pow(sum_channel_weight[i_c], 2) / (n_acc_channel[i_c] + n_rej_channel[i_c])) / (n_acc_channel[i_c] + n_rej_channel[i_c] - 1); count++;}
      else {w_channel_av[i_c] = 0.;}
      c_w_channel_sum[i_c] = alpha[i_c] * pow(w_channel_av[i_c], weight_adaptation_exponent);
    }
    else if (switch_mode_optimization == 1){
      // to get old result:
      if (n_acc_channel[i_c] + n_rej_channel[i_c] != 0){w_channel_av[i_c] = sum_channel_weight[i_c] / (n_acc_channel[i_c] + n_rej_channel[i_c]); count++;}
      else {w_channel_av[i_c] = 0.;}
      c_w_channel_sum[i_c] = w_channel_av[i_c];
    }
    else if (switch_mode_optimization == 2){
      if (n_acc_channel[i_c] + n_rej_channel[i_c] > 0 && n_acc_channel[i_c] > 1){w_channel_av[i_c] = sqrt(sum_channel_weight2[i_c] - pow(sum_channel_weight[i_c], 2) / (n_acc_channel[i_c] + n_rej_channel[i_c])) / (n_acc_channel[i_c] - 1); count++;}
      else {w_channel_av[i_c] = 0.;}
      c_w_channel_sum[i_c] = alpha[i_c] * pow(w_channel_av[i_c], weight_adaptation_exponent);
    }
    else if (switch_mode_optimization == 3){
      if (n_acc_channel[i_c] > 1){w_channel_av[i_c] = sqrt(sum_channel_weight2[i_c] - pow(sum_channel_weight[i_c], 2) / (n_acc_channel[i_c])) / (n_acc_channel[i_c] - 1); count++;}
      else {w_channel_av[i_c] = 0.;}
      c_w_channel_sum[i_c] = alpha[i_c] * pow(w_channel_av[i_c], weight_adaptation_exponent);
    }
  }
  
  vector<double> temp_w_channel_av = w_channel_av;
  sort(temp_w_channel_av.begin(), temp_w_channel_av.end());
  diff_w[i_alpha_it] = temp_w_channel_av[temp_w_channel_av.size() - 1] - temp_w_channel_av[0];

  x_minimum_diff_w = x_alpha_it_min;
  for (int i_s = x_alpha_it_min + 1; i_s < i_alpha_it; i_s++){  //  to use only alpha sets after previous IS step; before:    //  for (int i_s = 0; i_s < i_alpha_it; i_s++){
    if (diff_w[i_s] < diff_w[x_minimum_diff_w]){
      x_minimum_diff_w = i_s;
    }
  }
  double diff_w_min = diff_w[x_minimum_diff_w];

  bool pass_threshold = false;
  if (n_acc > threshold_optimization * n_gridsize){pass_threshold = true;}
  // if (count > n_gridsize / 4){pass_threshold = true;}

  if ((switch_validation_optimization == 0 || i_alpha_it == 0 || diff_w[i_alpha_it] <= diff_w_min) && pass_threshold){
    //  if (switch_validation_optimization == 0 || (i_alpha_it == 0) || (diff_w[i_alpha_it] <= diff_w_min)){
    // determination of new alpha's for next iteration step:
    c_w_norm = accumulate(c_w_channel_sum.begin(), c_w_channel_sum.end(), 0.);
    assert(!munich_isnan(c_w_norm));

    for (int i_c = 0; i_c < n_gridsize; i_c++){alpha[i_c] = c_w_channel_sum[i_c] / c_w_norm;}

    // new possibly iterative determination of weights after application of minimum_weight

    counter_minimum_weight = 0;
    no_channel_minimum_weight.clear();
    vector<int> new_no_channel_minimum_weight(1);
    int step_counter = 0;
    while (new_no_channel_minimum_weight.size() > 0){
      step_counter++;
      new_no_channel_minimum_weight.clear();

      for (int i_c = 0; i_c < n_gridsize; i_c++){
	if (alpha[i_c] <= alpha_reserved_min){
	  int flag = 0;
	  for (int j_c = 0; j_c < no_channel_minimum_weight.size(); j_c++){
	    if (i_c == no_channel_minimum_weight[j_c]){flag = 1; break;}
	  }
	  if (!flag){
	    alpha[i_c] = 0.;
	    counter_minimum_weight++;
	    new_no_channel_minimum_weight.push_back(i_c);  // always: set weights to zero only in the result step !!!
	  }
	}
      }

      double a_norm;
      a_norm = accumulate(alpha.begin(), alpha.end(), 0.) / (1. - counter_minimum_weight * alpha_reserved_min);
      for (int i_c = 0; i_c < n_gridsize; i_c++){if (alpha[i_c] != 0.){alpha[i_c] = alpha[i_c] / a_norm;}}
      logger << LOG_DEBUG << "limit_minimum_weight  step no. " << setw(3) << step_counter << " : counter_minimum_weight = " << setw(3) << counter_minimum_weight << "   a_norm = " << a_norm << endl;
      for (int j_c = 0; j_c < new_no_channel_minimum_weight.size(); j_c++){no_channel_minimum_weight.push_back(new_no_channel_minimum_weight[j_c]);}
    }
    for (int i_m = 0; i_m < no_channel_minimum_weight.size(); i_m++){alpha[no_channel_minimum_weight[i_m]] = alpha_reserved_min;}
  }
  else {
    alpha = alpha_it[x_minimum_diff_w]; // was "+ 1", which could set all alpha's to zero !!!
    //    alpha = alpha_it[x_minimum_diff_w + 1]; // why "+ 1" ??? check !!!
    counter_minimum_weight = 0;
    no_channel_minimum_weight.clear();
    for (int i_c = 0; i_c < n_gridsize; i_c++){
      if (alpha[i_c] <= alpha_reserved_min){
	counter_minimum_weight++;
	no_channel_minimum_weight.push_back(i_c);  // always: set weights to zero only in the result step !!!
      }
    }
  }
 
  if (switch_smearing_level){vegasgrid_average();}

  beta.resize(alpha.size());
  beta[0] = alpha[0];
  for (int i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}

  i_alpha_it++;

  logger << LOG_DEBUG << *this << endl;
  
  if (switch_mode_optimization == 0 || switch_mode_optimization == 2 || switch_mode_optimization == 3){
    sum_channel_weight = vector<double> (n_gridsize, 0.);
    sum_channel_weight2 = vector<double> (n_gridsize, 0.);
    n_acc = 0;
    n_rej = 0;
    n_acc_channel = vector<int> (n_gridsize, 0);
    n_rej_channel = vector<int> (n_gridsize, 0);
  }
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::result_IS_optimization(int i_step_mode){
  Logger logger("importancesampling_set::result_IS_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != 1){return;}
  if (end_optimization != 0){return;}

  if (i_step_mode != n_event_per_step * n_optimization_step){return;}

  end_optimization = 1;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::output_optimization(){
  Logger logger("importancesampling_set::output_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "active_optimization = " << active_optimization << "   end_optimization = " << end_optimization << endl;
  if (active_optimization == -1){return;}
  if (!end_optimization){return;}
  //  if (end_optimization != 1){return;}

  ofstream out_alpha;
  out_alpha.open(filename.c_str(), ofstream::out | ofstream::app);
  out_alpha << "% " << name << endl;
  // unify precision !!!
  for (int i_c = 0; i_c < n_gridsize; i_c++){out_alpha << double2hexastr(alpha[i_c]) << endl;}
  out_alpha.close();

  string temp_filename_dec = filename.insert(filename.size() - 4, "_dec");
  logger << LOG_INFO << "filename_readin (dec) = " << temp_filename_dec << endl;
  out_alpha.open(temp_filename_dec.c_str(), ofstream::out | ofstream::app);
  out_alpha << "% " << name << endl;
  // unify precision !!!
  for (int i_c = 0; i_c < alpha.size(); i_c++){out_alpha << setprecision(15) << alpha[i_c] << endl;}
  out_alpha.close();

  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::readin_optimization(){
  Logger logger("importancesampling_set::readin_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  bool start_data = false;
  string start_line = "% " + name;
  ifstream in_alpha;
  char LineBuffer[128];
  vector<string> data;
  vector<string> readin;

  logger << LOG_INFO << "filename_readin (hex) = " << filename_readin << endl;
  in_alpha.open(filename_readin.c_str());
  while (in_alpha.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
  in_alpha.close();
  if (readin.size() > 0){
    for (int i = 0; i < readin.size(); i++){
      if (readin[i] == start_line){start_data = true;}
      else if ((readin[i][0] == '%' || readin[i][0] == '#') && start_data == true){break;}
      else if (start_data){data.push_back(readin[i]);}
    }
    if (data.size() == n_gridsize){for (int i_c = 0; i_c < n_gridsize; i_c++){alpha[i_c] = hexastr2double(data[i_c]);}}
    //    else {logger << LOG_FATAL << "Number of input lines does not fit n_gridsize value." << endl; exit(1);}
  }
  //  else {logger << LOG_FATAL << "No input available." << endl; exit(1);}

  if (readin.size() == 0 || data.size() != n_gridsize){
    string temp_filename_readin_dec = filename_readin.insert(filename_readin.size() - 4, "_dec");
    logger << LOG_INFO << "filename_readin (dec) = " << temp_filename_readin_dec << endl;
    readin.clear();
    in_alpha.open(temp_filename_readin_dec.c_str());
    while (in_alpha.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    in_alpha.close();
    logger << LOG_INFO << "readin.size() = " << readin.size() << "   data.size() = " << data.size() << "   n_gridsize = " << n_gridsize << endl;
    start_data = false;
    data.clear();
    if (readin.size() > 0){
      for (int i = 0; i < readin.size(); i++){
	if (readin[i] == start_line){start_data = true;}
	else if ((readin[i][0] == '%' || readin[i][0] == '#') && start_data == true){break;}
	else if (start_data){data.push_back(readin[i]);}
      }
      logger << LOG_INFO << "readin.size() = " << readin.size() << "   data.size() = " << data.size() << "   n_gridsize = " << n_gridsize << endl;
      if (data.size() == n_gridsize){for (int i_c = 0; i_c < n_gridsize; i_c++){alpha[i_c] = atof(data[i_c].c_str());}}
      else {logger << LOG_FATAL << "Number of input lines does not fit n_gridsize value." << endl; exit(1);}
    }
    else {logger << LOG_FATAL << "No input available." << endl; exit(1);}
  }

  for (int i_c = 0; i_c < n_gridsize; i_c++){logger << LOG_DEBUG << "alpha[" << setw(4) << i_c << "] = " << alpha[i_c] << endl;}
  beta[0] = alpha[0];
  for (int i_c = 1; i_c < n_gridsize; i_c++){beta[i_c] = beta[i_c - 1] + alpha[i_c];}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void importancesampling_set::output_IS_optimization(int i_step_mode){
  Logger logger("importancesampling_set::output_IS_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization == -1){return;}
  if (end_optimization != 1){return;}

  //  if (i_step_mode != n_event_per_step * n_optimization_step){return;}

  ofstream out_alpha;
  out_alpha.open(filename.c_str(), ofstream::out | ofstream::trunc);
  for (int i_c = 0; i_c < alpha.size(); i_c++){
    out_alpha << setprecision(15) << alpha[i_c] << endl;
  }
  out_alpha.close();

  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::readin_IS_optimization(){
  Logger logger("importancesampling_set::readin_IS_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "filename_readin = " << filename_readin << endl;
  ifstream in_MCweights(filename_readin.c_str());
  vector<string> readin;
  char LineBuffer[128];
  while (in_MCweights.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
  in_MCweights.close();

  int start_data = 0;
  string start_line = "% " + name;

  // to guarantee compatibility with old weight files
  if (readin.size() > 0){if (readin[0][0] != '#'){start_data = 1;}}
  // end

  vector<string> data;
  for (int i = 0; i < readin.size(); i++){
    if (readin[i] == start_line){
      logger << LOG_DEBUG << "Data readin started for " << name << " in line " << i << endl;
      start_data = 1;
    }
    else if (readin[i][0] == '%' && start_data == 1){
      logger << LOG_DEBUG << "Data readin finished for " << name << " in line " << i << endl;
      break;
    }
    else if (readin[i][0] == '#' && start_data == 1){
      logger << LOG_DEBUG << "Commented line ignored for " << name << " in line " << i << ": " << readin[i] << endl;
    }
    else if (start_data){
      data.push_back(readin[i]);
    }
    else {logger << LOG_DEBUG << "readin[" << setw(4) << i << "] = " << readin[i] << " is ignored." << endl;}
  }

  logger << LOG_DEBUG << "Number of lines in " << filename_readin << ": " << readin.size() << endl;
  logger << LOG_DEBUG << "Number of relevant input lines for " << name << ": " << data.size() << "   (n_gridsize = " << n_gridsize << ")" << endl;

  if (data.size() != n_gridsize){
    logger << LOG_WARN << "Number of input lines does not fit n_gridsize value." << endl;
  }

  if (n_gridsize > 0){
    for (int i_c = 0; i_c < n_gridsize; i_c++){
      alpha[i_c] = atof(data[i_c].c_str());
      logger << LOG_DEBUG << "alpha[" << setw(4) << i_c << "] = " << alpha[i_c] << endl;
    }

    beta[0] = alpha[0];
    for (int i_c = 1; i_c < n_gridsize; i_c++){beta[i_c] = beta[i_c - 1] + alpha[i_c];}
  }
  else {
    logger << LOG_WARN << "Should not happen: n_gridsize = " << n_gridsize << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

void importancesampling_set::proceeding_out(ofstream & out_proceeding){
  Logger logger("importancesampling_set::proceeding_out " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){
    out_proceeding << "# " << name << " - end_optimization (1) - alpha (" << alpha.size() << ")" << endl;
    out_proceeding << end_optimization << endl;
    if (end_optimization != -1){
      for (size_t i_c = 0; i_c < alpha.size(); i_c++){
	out_proceeding << double2hexastr(alpha[i_c]) << endl;
      }
    }
    if (end_optimization == 0){
      out_proceeding << "# " << name << " - n_acc (1) - n_rej (1)" << endl;
      out_proceeding << n_acc << endl;
      out_proceeding << n_rej << endl;
      out_proceeding << "# " << name << " - n_acc_channel - n_rej_channel - sum_channel_weight - sum_channel_weight2 (" << alpha.size() << " x 4)" << endl;
      for (size_t i_c = 0; i_c < alpha.size(); i_c++){
	out_proceeding << n_acc_channel[i_c] << endl;
	out_proceeding << n_rej_channel[i_c] << endl;
	out_proceeding << double2hexastr(sum_channel_weight[i_c]) << endl;
	out_proceeding << double2hexastr(sum_channel_weight2[i_c]) << endl;
      }
      if (switch_validation_optimization){
	out_proceeding << "# " << name << " - i_alpha_it (1) - x_alpha_it_min (1) - diff_w (" << n_optimization_step << ")" << endl;
	out_proceeding << i_alpha_it << endl;
	out_proceeding << x_alpha_it_min << endl;
	for (size_t i = 0; i < n_optimization_step; i++){out_proceeding << double2hexastr(diff_w[i]) << endl;}
	out_proceeding << "# " << name << " - alpha_it (" << n_optimization_step << " x " << alpha.size() << ")" << endl;
	for (size_t i = 0; i < n_optimization_step; i++){
	  for (size_t i_c = 0; i_c < alpha.size(); i_c++){
	    out_proceeding << double2hexastr(alpha_it[i][i_c]) << endl;
	  }
	}
      }
      out_proceeding << "# " << name << " - weight optimization not yet finished!" << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::proceeding_in(int & proc, vector<string> & readin){
  Logger logger("importancesampling_set::proceeding_in " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  end_optimization = atoi(readin[proc++].c_str());
  //  proc += 1;
  if (end_optimization != -1){
    for (size_t i_g = 0; i_g < n_gridsize; i_g++){alpha[i_g] = hexastr2double(readin[proc++]);}
    //    proc += n_gridsize;
    beta[0] = alpha[0];
    for (size_t i_g = 1; i_g < beta.size(); i_g++){beta[i_g] = beta[i_g - 1] + alpha[i_g];}

  }
  if (end_optimization == 0){
    n_acc = atoi(readin[proc++].c_str());
    n_rej = atoi(readin[proc++].c_str());
    for (size_t i_g = 0; i_g < n_gridsize; i_g++){
      n_acc_channel[i_g] = atoi(readin[proc++].c_str());
      n_rej_channel[i_g] = atoi(readin[proc++].c_str());
      sum_channel_weight[i_g] = hexastr2double(readin[proc++]);
      sum_channel_weight2[i_g] = hexastr2double(readin[proc++]);
      //      n_acc_channel[i] = atoi(readin[proc + 4 * i].c_str());
      //      n_rej_channel[i] = atoi(readin[proc + 4 * i + 1].c_str());
      //      sum_channel_weight[i] = hexastr2double(readin[proc + 4 * i + 2]);
      //      sum_channel_weight2[i] = hexastr2double(readin[proc + 4 * i + 3]);
    }
    //    proc += 4 * n_gridsize;

    if (switch_validation_optimization){
      i_alpha_it = atoi(readin[proc++].c_str());
      x_alpha_it_min = atoi(readin[proc++].c_str());
      //      proc += 2;
      diff_w.resize(n_optimization_step);
      for (int i_g = 0; i_g < n_optimization_step; i_g++){diff_w[i_g] = hexastr2double(readin[proc++]);}
      //      proc += n_optimization_step;
      if (n_optimization_step != 0){
	alpha_it.resize(n_optimization_step, vector<double> (n_gridsize, 0.));
	for (int i_o = 0; i_o < n_optimization_step; i_o++){for (int i_g = 0; i_g < n_gridsize; i_g++){alpha_it[i_o][i_g] = hexastr2double(readin[proc++]);}}
	//	for (int i_o = 0; i_o < n_optimization_step; i_o++){for (int i_g = 0; i_g < n_gridsize; i_g++){alpha_it[i_o][i_g] = hexastr2double(readin[proc + n_gridsize * i_o + i_g]);}}
	//	proc += n_optimization_step * n_gridsize;
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::proceeding_group_by_type_out(ofstream & out_proceeding){
  Logger logger("importancesampling_set::proceeding_group_by_type_out " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  out_proceeding << "# " << name << " - end_optimization (1)" << endl;
  out_proceeding << "#=" << endl;
  out_proceeding << end_optimization << endl;
  if (end_optimization != -1){
    out_proceeding << "# " << name << " - alpha (" << alpha.size() << ")" << endl;
    out_proceeding << "#=" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){
      out_proceeding << double2hexastr(alpha[i_c]) << endl;
    }
  }
  if (end_optimization == 0){
    out_proceeding << "# " << name << " - n_acc (1) - n_rej (1)" << endl;
    out_proceeding << "#+" << endl;
    out_proceeding << n_acc << endl;
    out_proceeding << n_rej << endl;
    out_proceeding << "# " << name << " - n_acc_channel (" << alpha.size() << ")" << endl;
    out_proceeding << "#+" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){
      out_proceeding << n_acc_channel[i_c] << endl;
    }
    out_proceeding << "# " << name << " - n_rej_channel (" << alpha.size() << ")" << endl;
    out_proceeding << "#+" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){
      out_proceeding << n_rej_channel[i_c] << endl;
    }
    out_proceeding << "# " << name << " - sum_channel_weight (" << alpha.size() << ")" << endl;
    out_proceeding << "#+" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){
      out_proceeding << double2hexastr(sum_channel_weight[i_c]) << endl;
    }
    out_proceeding << "# " << name << " - sum_channel_weight2 (" << alpha.size() << ")" << endl;
    out_proceeding << "#+" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){
      out_proceeding << double2hexastr(sum_channel_weight2[i_c]) << endl;
    }
    if (switch_validation_optimization){
      out_proceeding << "# " << name << " - i_alpha_it (1) - x_alpha_it_min (1)" << endl;
      out_proceeding << "#=" << endl;
      out_proceeding << i_alpha_it << endl;
      out_proceeding << x_alpha_it_min << endl;
      out_proceeding << "# " << name << " - diff_w (" << n_optimization_step << ")" << endl;
      out_proceeding << "#=" << endl;
      for (size_t i = 0; i < n_optimization_step; i++){out_proceeding << double2hexastr(diff_w[i]) << endl;}
      out_proceeding << "# " << name << " - alpha_it (" << n_optimization_step << " x " << alpha.size() << ")" << endl;
      out_proceeding << "#=" << endl;
      for (size_t i = 0; i < n_optimization_step; i++){for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << double2hexastr(alpha_it[i][i_c]) << endl;}}
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void importancesampling_set::proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination){
  Logger logger("importancesampling_set::proceeding_group_by_type_in " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){
    end_optimization = atoi(readin[proc++].c_str());
    if (end_optimization != -1){
      for (size_t i = 0; i < n_gridsize; i++){alpha[i] = hexastr2double(readin[proc++]);}
      beta[0] = alpha[0];
      for (size_t i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}
    }
    if (end_optimization == 0){
      if (switch_combination){
	n_acc = atoi(readin[proc++].c_str());
	n_rej = atoi(readin[proc++].c_str());
	for (size_t i = 0; i < n_gridsize; i++){n_acc_channel[i] = atoi(readin[proc++].c_str());}
	for (size_t i = 0; i < n_gridsize; i++){n_rej_channel[i] = atoi(readin[proc++].c_str());}
	for (size_t i = 0; i < n_gridsize; i++){sum_channel_weight[i] = hexastr2double(readin[proc++]);}
	for (size_t i = 0; i < n_gridsize; i++){sum_channel_weight2[i] = hexastr2double(readin[proc++]);}
      }
      else {proc += 2 + 4 * n_gridsize;}
    }
    if (switch_validation_optimization){
      i_alpha_it = atoi(readin[proc++].c_str());
      x_alpha_it_min = atoi(readin[proc++].c_str());
      diff_w.resize(n_optimization_step);
      for (size_t i = 0; i < n_optimization_step; i++){diff_w[i] = hexastr2double(readin[proc++]);}
      if (n_optimization_step != 0){
	alpha_it.resize(n_optimization_step, vector<double> (n_gridsize));
	for (size_t i = 0; i < n_optimization_step; i++){for (size_t i_c = 0; i_c < n_gridsize; i_c++){alpha_it[i][i_c] = hexastr2double(readin[proc++]);}}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


ostream & operator << (ostream & s, const importancesampling_set & IS){
  s << left << endl;
  s << setw(30) << "name" << " = " << IS.name << endl;
  s << setw(30) << "active_optimization" << " = " << IS.active_optimization << endl;
  if (IS.active_optimization != -1){
    s << setw(30) << "end_optimization" << " = " << IS.end_optimization << endl;
    s << setw(30) << "filename" << " = " << IS.filename << endl;
    s << setw(30) << "filename_readin" << " = " << IS.filename_readin << endl;
    s << endl;
    s << setw(30) << "n_gridsize" << " = " << IS.n_gridsize << endl;
    s << setw(30) << "n_optimization_step" << " = " << IS.n_optimization_step << endl;
    s << setw(30) << "n_event_per_step" << " = " << IS.n_event_per_step << endl;
    s << setw(30) << "threshold_optimization" << " = " << IS.threshold_optimization << endl;
    s << setw(30) << "switch_mode_optimization" << " = " << IS.switch_mode_optimization << endl;
    s << setw(30) << "switch_validation_optimization" << " = " << IS.switch_validation_optimization << endl;
    s << setw(30) << "switch_minimum_weight" << " = " << IS.switch_minimum_weight << endl;
    s << setw(30) << "reserved_minimum_weight" << " = " << IS.reserved_minimum_weight << endl;
    s << setw(30) << "alpha_reserved_min" << " = " << IS.alpha_reserved_min << endl;
    s << setw(30) << "alpha_maximum_weight" << " = " << IS.alpha_maximum_weight << endl;
    s << setw(30) << "weight_adaptation_exponent" << " = " << IS.weight_adaptation_exponent << endl;
    s << setw(30) << "switch_smearing_level" << " = " << IS.switch_smearing_level << endl;
    s << endl;
    s << setw(30) << "i_alpha_it" << " = " << IS.i_alpha_it << endl;
    s << setw(30) << "x_alpha_it_min" << " = " << IS.x_alpha_it_min << endl;
    s << endl;
    s << setw(30) << "n_acc" << " = " << IS.n_acc << endl;
    s << setw(30) << "n_rej" << " = " << IS.n_rej << endl;
    s << endl;
    for (int i_s = IS.x_alpha_it_min; i_s < IS.i_alpha_it; i_s++){s << setw(27) << "diff_w[" << setw(2) << i_s << "]" << " = " << IS.diff_w[i_s] << endl;}
    s << endl;
    if (IS.end_optimization == 0){
      for (int i_c = 0; i_c < IS.n_gridsize; i_c++){
	s << IS.name << " [" << setw(3) << i_c << "]:" << setprecision(8) << showpoint << "   ";
	s << "sw = " << setw(15) << left << IS.sum_channel_weight[i_c];
	s << "   ";
	s << "sw2 = " << setw(15) << left << IS.sum_channel_weight2[i_c];
	s << "   ";
	s << "delta = " << setw(15) << left << IS.w_channel_av[i_c];
	s << "   ";
	s << "n_acc = " << setw(6) << right << IS.n_acc_channel[i_c];
	s << "   ";
	s << "n_rej = " << setw(6) << right << IS.n_rej_channel[i_c];
	s << "   ";
	double efficiency = 0.;
	if ((IS.n_acc_channel[i_c] + IS.n_rej_channel[i_c]) > 0){efficiency = double(IS.n_acc_channel[i_c]) / (IS.n_acc_channel[i_c] + IS.n_rej_channel[i_c]);}
	s << "efficiency = " << setw(15) << right << efficiency;
	s << "   ";
	s << "alpha = " << setw(15) << left << IS.alpha[i_c];
	s << noshowpoint << endl;
      }
      s << endl;
    }
  }
  return s;
}


int importancesampling_set::operator == (const importancesampling_set & IS) const{
  Logger logger("importancesampling_set::operator == " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int equality = 1;
  int diff_counter = 0;
  if (name != IS.name){logger << LOG_INFO << setw(40) << "name" << "  is different." << endl; diff_counter++;}
  if (switch_optimization != IS.switch_optimization){logger << LOG_INFO << setw(40) << "switch_optimization" << "  is different." << endl; diff_counter++;}
  if (n_gridsize != IS.n_gridsize){logger << LOG_INFO << setw(40) << "n_gridsize" << "  is different." << endl; diff_counter++;}
  if (n_event_per_step != IS.n_event_per_step){logger << LOG_INFO << setw(40) << "n_event_per_step" << "  is different." << endl; diff_counter++;}
  if (n_optimization_step != IS.n_optimization_step){logger << LOG_INFO << setw(40) << "n_optimization_step = " << n_optimization_step << " != " << IS.n_optimization_step << " = IS.n_optimization_step"  << "  is different." << endl; diff_counter++;}
  //  if (switch_increase_n_event_per_step != ISswitch_increase_n_event_per_step.){logger << LOG_INFO << setw(40) << "switch_increase_n_event_per_step" << "  is different." << endl; diff_counter++;}
  if (threshold_optimization != IS.threshold_optimization){logger << LOG_INFO << setw(40) << "threshold_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_mode_optimization != IS.switch_mode_optimization){logger << LOG_INFO << setw(40) << "switch_mode_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_validation_optimization != IS.switch_validation_optimization){logger << LOG_INFO << setw(40) << "switch_validation_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_minimum_weight != IS.switch_minimum_weight){logger << LOG_INFO << setw(40) << "switch_minimum_weight" << "  is different." << endl; diff_counter++;}
  if (weight_adaptation_exponent != IS.weight_adaptation_exponent){logger << LOG_INFO << setw(40) << "weight_adaptation_exponent" << "  is different." << endl; diff_counter++;}
  if (reserved_minimum_weight != IS.reserved_minimum_weight){logger << LOG_INFO << setw(40) << "reserved_minimum_weight" << "  is different." << endl; diff_counter++;}
  if (alpha_maximum_weight != IS.alpha_maximum_weight){logger << LOG_INFO << setw(40) << "alpha_maximum_weight" << "  is different." << endl; diff_counter++;}
  //  if ( != IS.){logger << LOG_INFO << setw(40) << "" << "  is different." << endl; diff_counter++;}
  if (diff_counter > 0){equality = 0;}
  
  logger << LOG_DEBUG_VERBOSE << "finished - return " << equality << endl;
  return equality;
}
