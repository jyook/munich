#include "header.hpp"

multichannel_input::multichannel_input(){}

ostream & operator << (ostream & s, const multichannel_input & MC){
  s << left << endl;
  s << setw(30) << "name" << " = " << MC.name << endl;
  s << setw(30) << "switch_optimization" << " = " << MC.switch_optimization << endl;
  s << endl;
  s << setw(30) << "n_channel" << " = " << MC.n_channel << endl;
  s << setw(30) << "n_optimization_step" << " = " << MC.n_optimization_step << endl;
  s << setw(30) << "n_optimization_step_extra" << " = " << MC.n_optimization_step_extra << endl;
  s << setw(30) << "n_event_per_step" << " = " << MC.n_event_per_step << endl;
  s << setw(30) << "threshold_optimization" << " = " << MC.threshold_optimization << endl;
  s << setw(30) << "switch_mode_optimization" << " = " << MC.switch_mode_optimization << endl;
  s << setw(30) << "switch_validation_optimization" << " = " << MC.switch_validation_optimization << endl;
  s << setw(30) << "switch_minimum_weight" << " = " << MC.switch_minimum_weight << endl;
  s << endl;
  s << setw(30) << "weight_adaptation_exponent" << " = " << MC.weight_adaptation_exponent << endl;
  s << setw(30) << "reserved_minimum_weight" << " = " << MC.reserved_minimum_weight << endl;
  s << setw(30) << "alpha_maximum_weight" << " = " << MC.alpha_maximum_weight << endl;
   
  return s;
}


multichannel_set::multichannel_set(){}

multichannel_set::multichannel_set(const multichannel_input & input, string _filename, string _filename_readin){
  Logger logger("multichannel_set::multichannel_set (input) " + input.name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  name = input.name;
  switch_optimization = input.switch_optimization;
  
  n_channel = input.n_channel;
  if (n_channel < 1){logger << LOG_FATAL << "n_channel = " << n_channel << " is not a valid value." << endl;}
  
  //  n_alpha_events = input.n_event_per_step;
  //  n_alpha_steps = input.n_optimization_step;
  n_event_per_step = input.n_event_per_step;
  n_optimization_step = input.n_optimization_step;

  switch_validation_optimization = input.switch_validation_optimization;
  switch_minimum_weight = input.switch_minimum_weight;
  // switch_minimum_weight == 0: Weights below limit_minimum_weight are set to zero (after last iteration step).
  // switch_minimum_weight == 1: Weights below limit_minimum_weight are set to limit_minimum_weight.
  reserved_minimum_weight = input.reserved_minimum_weight;
  // reserved_minimum_weight = absolute minimum weight (reserved are in total: n_below_minimum / n_channel)
  alpha_reserved_min = reserved_minimum_weight / n_channel;
  alpha_maximum_weight = input.alpha_maximum_weight;
  
  if (switch_optimization == 0 || switch_optimization == 2){
    active_optimization = 0;
    end_optimization = 1;
  }
  else if (switch_optimization == 1 || switch_optimization == 3){
    active_optimization = 1;
    end_optimization = 0;
  }
  else if (switch_optimization == -1){
    active_optimization = -1;
    end_optimization = -1;
  }
  else {logger << LOG_FATAL << "No valid value of switch_optimization chosen." << endl; exit(1);}

  threshold_optimization = input.threshold_optimization;
  switch_mode_optimization = input.switch_mode_optimization;
  weight_adaptation_exponent = input.weight_adaptation_exponent;

  filename = _filename;
  filename_readin = _filename_readin;

  if (n_channel == 1){end_optimization = 1;} // check if output has to be created immediately !!!

  counter_minimum_weight = 0;
  alpha.resize(n_channel, 1. / double (n_channel));
  beta.resize(n_channel, 0.);
  beta[0] = alpha[0];
  for (int i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}

  n_acc = 0;
  n_rej = 0;
  
  n_acc_channel.resize(n_channel, 0);
  n_rej_channel.resize(n_channel, 0);

  channel = 0;

  g_channel.resize(n_channel, 0.);
  g_IS_channel.resize(n_channel, 0.);

  w_channel_sum.resize(n_channel, 0.);
  w_channel_av.resize(n_channel, 0.);
  
  c_w_channel_sum.resize(n_channel, 0.);
  c_w_norm = 0.;

  alpha_it.resize(n_optimization_step, vector<double> (n_channel, 0.));
  diff_w.resize(n_optimization_step, 0.);

  i_alpha_it = 0;
  x_alpha_it_min = 0;

  if (switch_optimization == 2 || switch_optimization == 3){readin_optimization();}
  //  if (switch_optimization == 2 || switch_optimization == 3){readin_MCweight_optimization();}

  logger << LOG_DEBUG << *this << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void multichannel_set::psp_MCweight_optimization(double & integrand, double & g_tot){
  Logger logger("multichannel_set::psp_MCweight_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != 1){return;}
  if (end_optimization != 0){return;}

  n_acc++;
  n_acc_channel[channel]++;
  double w_help = pow(abs(integrand), 2) / g_tot;
  if (munich_isnan(w_help)){// || w_help == 0.){
    logger << LOG_DEBUG << "munich_isnan(w_help)" << endl;
    logger << LOG_DEBUG << "integrand = " << integrand << endl;
    logger << LOG_DEBUG << "g_tot = " << g_tot << endl;
    for (int i_c = 0; i_c < n_channel; i_c++){logger << LOG_DEBUG << "g_channel[" << i_c << "] = " << g_channel[i_c] << endl;}
  }
  else {
    if (switch_mode_optimization == 0){
      for (int i_c = 0; i_c < n_channel; i_c++){
	if (munich_isnan(g_channel[i_c])){logger << LOG_ERROR << "weight in channel " << i_c << " of " << name << " is nan; " << g_channel[i_c] << ", " << w_help << endl;}
	else {w_channel_sum[i_c] += g_channel[i_c] * w_help;}
      }
    }
    else if (switch_mode_optimization == 1){
      if (munich_isnan(g_channel[channel])){logger << LOG_ERROR << "weight in channel " << channel << " of " << name << " is nan; " << g_channel[channel] << ", " << w_help << endl;}
      else {w_channel_sum[channel] += g_channel[channel] * w_help;}
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void multichannel_set::step_MCweight_optimization(int i_step_mode){
  Logger logger("multichannel_set::step_MCweight_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization != 0){return;}
  if (active_optimization != 1){return;}
  if (i_step_mode > n_event_per_step * n_optimization_step){return;}
  if (i_step_mode % n_event_per_step != 0){return;}
  
  alpha_it[i_alpha_it] = alpha;

  int count = 0;
  // evaluation of quality measure diff_w -> decides if new alpha's should be determined:
  for (int i_c = 0; i_c < n_channel; i_c++){
    if (switch_mode_optimization == 0){w_channel_av[i_c] = w_channel_sum[i_c] / n_event_per_step; count++;}
    else if (switch_mode_optimization == 1){
      if (n_acc_channel[i_c] + n_rej_channel[i_c] != 0){w_channel_av[i_c] = w_channel_sum[i_c] / (n_acc_channel[i_c] + n_rej_channel[i_c]); count++;}
      else {w_channel_av[i_c] = 0.;}
    }
    else if (switch_mode_optimization == 2){
      if (n_acc_channel[i_c] != 0){w_channel_av[i_c] = w_channel_sum[i_c] / n_acc_channel[i_c]; count++;}
      else {w_channel_av[i_c] = 0.;}
    }
    c_w_channel_sum[i_c] = alpha[i_c] * pow(w_channel_av[i_c], weight_adaptation_exponent);
  }
  vector<double> temp_w_channel_av = w_channel_av;
  sort(temp_w_channel_av.begin(), temp_w_channel_av.end());
  diff_w[i_alpha_it] = temp_w_channel_av[temp_w_channel_av.size() - 1] - temp_w_channel_av[0];
  c_w_norm = accumulate(c_w_channel_sum.begin(), c_w_channel_sum.end(), 0.);
  assert(!munich_isnan(c_w_norm));
  x_minimum_diff_w = x_alpha_it_min;

  //  to use only alpha sets after previous IS step; before:    //  for (int i_s = 0; i_s < i_alpha_it; i_s++){ // start at 1 instead of 0 ???
  for (int i_s = x_alpha_it_min + 1; i_s < i_alpha_it; i_s++){if (diff_w[i_s] < diff_w[x_minimum_diff_w]){x_minimum_diff_w = i_s;}}
  double diff_w_min = diff_w[x_minimum_diff_w];

  bool pass_threshold = false;
  if (n_acc > threshold_optimization * n_channel){pass_threshold = true;}
  // if (count > n_channel / 4){pass_threshold = true;}
  
  // check if i_alpha_it == x_alpha_it_min makes more sense here !!!
  if ((switch_validation_optimization == 0 || i_alpha_it == 0 || diff_w[i_alpha_it] <= diff_w_min) && pass_threshold){
    // determination of new alpha's for next iteration step:
    for (int i_c = 0; i_c < n_channel; i_c++){alpha[i_c] = c_w_channel_sum[i_c] / c_w_norm;}

    // new possibly iterative determination of weights after application of minimum_weight
    counter_minimum_weight = 0;
    no_channel_minimum_weight.clear();
    vector<int> new_no_channel_minimum_weight(1);
    int step_counter = 0;
    while (new_no_channel_minimum_weight.size() > 0){
      step_counter++;
      new_no_channel_minimum_weight.clear();

      for (int i_c = 0; i_c < n_channel; i_c++){
	if (alpha[i_c] <= alpha_reserved_min){
	  int flag = 0;
	  for (int j_c = 0; j_c < no_channel_minimum_weight.size(); j_c++){if (i_c == no_channel_minimum_weight[j_c]){flag = 1; break;}}
	  if (!flag){
	    alpha[i_c] = 0.;
	    counter_minimum_weight++;
	    new_no_channel_minimum_weight.push_back(i_c);  // always: set weights to zero only in the result step !!!
	  }
	}
      }

      double a_norm;
      a_norm = accumulate(alpha.begin(), alpha.end(), 0.) / (1. - counter_minimum_weight * alpha_reserved_min);
      for (int i_c = 0; i_c < n_channel; i_c++){if (alpha[i_c] != 0.){alpha[i_c] = alpha[i_c] / a_norm;}}
      for (int j_c = 0; j_c < new_no_channel_minimum_weight.size(); j_c++){no_channel_minimum_weight.push_back(new_no_channel_minimum_weight[j_c]);}
    }
    for (int i_m = 0; i_m < no_channel_minimum_weight.size(); i_m++){alpha[no_channel_minimum_weight[i_m]] = alpha_reserved_min;}
  }
  else {
    // Try without '+1' (often falls back to original set, but might deliver better convergence) ???
    alpha = alpha_it[x_minimum_diff_w]; // was "+ 1", which could set all alpha's to zero !!!
    //    alpha = alpha_it[x_minimum_diff_w + 1]; // why "+ 1" ??? check !!!

    // In particular for resumed runs:
    counter_minimum_weight = 0;
    no_channel_minimum_weight.clear();
    for (int i_c = 0; i_c < n_channel; i_c++){
      if (alpha[i_c] <= alpha_reserved_min){
	counter_minimum_weight++;
	no_channel_minimum_weight.push_back(i_c);  // always: set weights to zero only in the result step !!!
      }
    }

    logger << LOG_DEBUG << "optimization step doesn't seem to improve the set of weights:" << endl;
    logger << LOG_DEBUG << "alpha set no. " << x_minimum_diff_w + 1 << "  is used in the next step." << endl;
    logger << LOG_DEBUG << setw(40) << "counter_minimum_weight" << " = " << counter_minimum_weight << endl;
    logger << LOG_DEBUG << setw(40) << "no_channel_minimum_weight.size()" << " = " << no_channel_minimum_weight.size() << endl;
    for (int j_c = 0; j_c < no_channel_minimum_weight.size(); j_c++){logger << LOG_DEBUG << "minimum:   alpha[" << no_channel_minimum_weight[j_c] << "] = " << alpha[no_channel_minimum_weight[j_c]] << endl;}
  }

  beta[0] = alpha[0];
  for (int i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}

  i_alpha_it++;

  for (int i_c = 0; i_c < n_channel; i_c++){if (alpha[i_c] == 0.){g_channel[i_c] = 0.;}}

  logger << LOG_DEBUG << *this << endl;

  w_channel_sum = vector<double> (n_channel, 0.);
  n_acc = 0;
  n_rej = 0;
  n_acc_channel = vector<int> (n_channel, 0);
  n_rej_channel = vector<int> (n_channel, 0);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void multichannel_set::result_MCweight_optimization(int i_step_mode){
  Logger logger("multichannel_set::result_MCweight_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "i_step_mode = " << i_step_mode << endl;
  logger << LOG_DEBUG << "(i_step_mode != n_event_per_step * n_optimization_step) = " << (i_step_mode != n_event_per_step * n_optimization_step) << endl;

  if (active_optimization != 1){return;}
  if (end_optimization != 0){return;}
  if (i_step_mode != n_event_per_step * n_optimization_step){return;}

  // changed from x_minimum_diff_w = 0, for (int j = 0... !!!
  // use only alpha sets after previous IS step:
  x_minimum_diff_w = x_alpha_it_min;
  // to use only alpha sets after previous IS step; before:   //  for (int i_s = 1; i_s < diff_w.size(); i_s++){
  for (int i_s = x_alpha_it_min + 1; i_s < diff_w.size(); i_s++){if (diff_w[i_s] < diff_w[x_minimum_diff_w]){x_minimum_diff_w = i_s;}}
  alpha = alpha_it[x_minimum_diff_w];
  logger << LOG_DEBUG << "alpha set no. x_minimum_diff_w = " << x_minimum_diff_w << "  is used in the integration." << endl;

  counter_minimum_weight = 0;
  no_channel_minimum_weight.clear();
  for (int i_c = 0; i_c < n_channel; i_c++){
    if (alpha[i_c] == alpha_reserved_min){
      counter_minimum_weight++;
      no_channel_minimum_weight.push_back(i_c);
    }
  }

  if (switch_minimum_weight == 0){
    for (int j_c = 0; j_c < no_channel_minimum_weight.size(); j_c++){alpha[no_channel_minimum_weight[j_c]] = 0.;}
    double a_norm;
    a_norm = accumulate(alpha.begin(), alpha.end(), 0.);
    for (int i_c = 0; i_c < n_channel; i_c++){if (alpha[i_c] != 0.){alpha[i_c] = alpha[i_c] / a_norm;}}
  }

  beta[0] = alpha[0];
  for (int i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}

  end_optimization = 1;

  logger << LOG_DEBUG << *this << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void multichannel_set::output_optimization(){
  Logger logger("multichannel::output_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "active_optimization = " << active_optimization << "   end_optimization = " << end_optimization << endl;
  if (active_optimization == -1){return;}
  if (!end_optimization){return;}
  //  if (end_optimization != 1){return;}

  ofstream out_alpha;
  out_alpha.open(filename.c_str(), ofstream::out | ofstream::app);
  out_alpha << "% " << name << endl;
  for (int i_c = 0; i_c < n_channel; i_c++){out_alpha<< double2hexastr(alpha[i_c]) << endl;}
  out_alpha.close();

  string temp_filename_dec = filename.insert(filename.size() - 4, "_dec");
  logger << LOG_INFO << "filename_readin (dec) = " << temp_filename_dec << endl;
  out_alpha.open(temp_filename_dec.c_str(), ofstream::out | ofstream::app);
  out_alpha << "% " << name << endl;
  for (int i_c = 0; i_c < n_channel; i_c++){out_alpha << setprecision(16) << alpha[i_c] << endl;}
  out_alpha.close();

  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void multichannel_set::readin_optimization(){
  Logger logger("multichannel::readin_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  bool start_data = false;
  string start_line = "% " + name;
  ifstream in_alpha;
  char LineBuffer[128];
  vector<string> data;
  vector<string> readin;

  logger << LOG_DEBUG << "filename_readin = " << filename_readin << endl;
  in_alpha.open(filename_readin.c_str());
  while (in_alpha.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
  in_alpha.close();
  if (readin.size() > 0){
    for (int i = 0; i < readin.size(); i++){
      if (readin[i] == start_line){start_data = true;}
      else if ((readin[i][0] == '%' || readin[i][0] == '#') && start_data == true){break;}
      else if (start_data){data.push_back(readin[i]);}
    }
    if (data.size() == n_channel){for (int i_c = 0; i_c < n_channel; i_c++){alpha[i_c] = hexastr2double(data[i_c]);}}
    //    else {logger << LOG_FATAL << "Number of input lines does not fit n_channel value." << endl; exit(1);}
  }
  //  else {logger << LOG_FATAL << "No input available." << endl; exit(1);}

  if (readin.size() == 0 || data.size() != n_channel){
    string temp_filename_readin_dec = filename_readin.insert(filename_readin.size() - 4, "_dec");
    logger << LOG_INFO << "filename_readin (dec) = " << temp_filename_readin_dec << endl;
    readin.clear();
    in_alpha.open(temp_filename_readin_dec.c_str());
    while (in_alpha.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    in_alpha.close();
    start_data = false;
    data.clear();
    if (readin.size() > 0){
      for (int i = 0; i < readin.size(); i++){
	if (readin[i] == start_line){start_data = true;}
	else if ((readin[i][0] == '%' || readin[i][0] == '#') && start_data == true){break;}
	else if (start_data){data.push_back(readin[i]);}
      }
      if (data.size() == n_channel){for (int i_c = 0; i_c < n_channel; i_c++){alpha[i_c] = atof(data[i_c].c_str());}}
      else {logger << LOG_FATAL << "Number of input lines does not fit n_channel value." << endl; exit(1);}
    }
    else {logger << LOG_FATAL << "No input available." << endl; exit(1);}
  }
  
  for (int i_c = 0; i_c < n_channel; i_c++){logger << LOG_DEBUG << "alpha[" << setw(4) << i_c << "] = " << alpha[i_c] << endl;}
  beta[0] = alpha[0];
  for (int i_c = 1; i_c < n_channel; i_c++){beta[i_c] = beta[i_c - 1] + alpha[i_c];}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void multichannel_set::output_MCweight_optimization(int i_step_mode, string & filename_MCweight){
  Logger logger("multichannel::output_MCweight_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "active_optimization = " << active_optimization << "   end_optimization = " << end_optimization << endl;
  if (active_optimization == -1){return;}
  if (end_optimization != 1){return;}
  //  if (i_step_mode != n_event_per_step * n_optimization_step){return;}

  ofstream out_MCweights;
  out_MCweights.open(filename_MCweight.c_str(), ofstream::out | ofstream::app);
  out_MCweights << "% " << name << endl;
  for (int i_c = 0; i_c < n_channel; i_c++){out_MCweights << setprecision(16) << alpha[i_c] << endl;}
  out_MCweights.close();

  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void multichannel_set::readin_MCweight_optimization(){
  Logger logger("multichannel::readin_MCweight_optimization " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "filename_readin = " << filename_readin << endl;
  ifstream in_MCweights(filename_readin.c_str());
  vector<string> readin;
  char LineBuffer[128];
  while (in_MCweights.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
  in_MCweights.close();

  int start_data = 0;
  string start_line = "% " + name;

  // to guarantee compatibility with old weight files
  for (int i = 0; i < readin.size(); i++){for (int j = 0; j < readin[i].size(); j++){if (readin[i][j] == '#'){readin[i][j] = '%';}}}
  if (readin[0] == "% MC"){readin[0] = "% MC_phasespace";}

  vector<string> data;
  for (int i = 0; i < readin.size(); i++){
    logger << LOG_DEBUG << "***" << readin[i] << "***" << endl;
    if (readin[i] == start_line){logger << LOG_DEBUG << "Data readin started for " << name << " in line " << i << endl; start_data = 1;}
    else if (readin[i][0] == '%' && start_data == 1){logger << LOG_DEBUG << "Data readin finished for " << name << " in line " << i << endl; break;}
    else if (readin[i][0] == '#' && start_data == 1){logger << LOG_DEBUG << "Commented line ignored for " << name << " in line " << i << ": " << readin[i] << endl;}
    else if (start_data){data.push_back(readin[i]);}
    else {logger << LOG_DEBUG << "readin[" << setw(4) << i << "] = " << readin[i] << " is ignored." << endl;}
  }
  logger << LOG_INFO << "Number of lines in " << filename_readin << ": " << readin.size() << endl;
  logger << LOG_INFO << "Number of relevant input lines for " << name << ": " << data.size() << "   (n_channel = " << n_channel << ")" << endl;
  if (data.size() != n_channel){logger << LOG_INFO << "Number of input lines does not fit n_channel value." << endl;}

  for (int i_c = 0; i_c < n_channel; i_c++){alpha[i_c] = atof(data[i_c].c_str());}
  for (int i_c = 0; i_c < n_channel; i_c++){logger << LOG_DEBUG << "alpha[" << setw(4) << i_c << "] = " << alpha[i_c] << endl;}

  beta[0] = alpha[0];
  for (int i_c = 1; i_c < beta.size(); i_c++){beta[i_c] = beta[i_c - 1] + alpha[i_c];}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

void multichannel_set::proceeding_out(ofstream & out_proceeding){
  Logger logger("multichannel::proceeding_out " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){
    out_proceeding << "# " << name << " - end_optimization (1)" << endl;
    out_proceeding << end_optimization << endl;
    out_proceeding << "# " << name << " - alpha (" << alpha.size() << ")" << endl;
    for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << double2hexastr(alpha[i_c]) << endl;}
    if (end_optimization == 0){
      size_t n_opt_steps = alpha_it.size();
      out_proceeding << "# " << name << " - n_opt_steps (1)" << endl;
      out_proceeding << n_opt_steps << endl;
      out_proceeding << "# " << name << " - n_acc (1) - n_rej (1)" << endl;
      out_proceeding << n_acc << endl;
      out_proceeding << n_rej << endl;
      out_proceeding << "# " << name << " - n_acc_channel (" << alpha.size() << ")" << endl;
      for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << n_acc_channel[i_c] << endl;}
      out_proceeding << "# " << name << " - n_rej_channel (" << alpha.size() << ")" << endl;
      for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << n_rej_channel[i_c] << endl;}
      out_proceeding << "# " << name << " - w_sum_channel_weight (" << alpha.size() << ")" << endl;
      for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << double2hexastr(w_channel_sum[i_c]) << endl;}
      if (switch_validation_optimization){
	out_proceeding << "# " << name << " - i_alpha_it (1) - x_alpha_it_min (1)" << endl;
	out_proceeding << i_alpha_it << endl;
	out_proceeding << x_alpha_it_min << endl;
	out_proceeding << "# " << name << " - diff_w (" << n_opt_steps << ")" << endl;
	for (size_t i_o = 0; i_o < n_opt_steps; i_o++){out_proceeding << double2hexastr(diff_w[i_o]) << endl;}
	out_proceeding << "# " << name << " - alpha_it (" << n_opt_steps << " x " << alpha.size() << ")" << endl;
	for (size_t i_o = 0; i_o < n_opt_steps; i_o++){for (size_t i_c = 0; i_c < alpha.size(); i_c++){out_proceeding << double2hexastr(alpha_it[i_o][i_c]) << endl;}}
      }
      out_proceeding << "# " << name << " - weight optimization not yet finished!" << endl;
    }
  }
  else {out_proceeding << "# " << name << " - no_optimization due to active_optimization = " << active_optimization << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




void multichannel_set::proceeding_in(int & proc, vector<string> & readin){
  Logger logger("multichannel::proceeding_in " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){
    end_optimization = atoi(readin[proc++].c_str());
    for (int i_c = 0; i_c < n_channel; i_c++){alpha[i_c] = hexastr2double(readin[proc++]);}
    beta[0] = alpha[0];
    for (int i_c = 1; i_c < n_channel; i_c++){beta[i_c] = beta[i_c - 1] + alpha[i_c];}
    if (end_optimization == 0){
      int n_opt_steps = atoi(readin[proc++].c_str());
      n_acc = atoi(readin[proc++].c_str());
      n_rej = atoi(readin[proc++].c_str());
      for (int i_c = 0; i_c < n_channel; i_c++){n_acc_channel[i_c] = atoi(readin[proc++].c_str());}
      for (int i_c = 0; i_c < n_channel; i_c++){n_rej_channel[i_c] = atoi(readin[proc++].c_str());}
      for (int i_c = 0; i_c < n_channel; i_c++){w_channel_sum[i_c] = hexastr2double(readin[proc++]);}
      if (switch_validation_optimization){
	i_alpha_it = atoi(readin[proc++].c_str());
	x_alpha_it_min = atoi(readin[proc++].c_str());
	diff_w.resize(n_opt_steps);
	for (int i_o = 0; i_o < n_opt_steps; i_o++){diff_w[i_o] = hexastr2double(readin[proc++]);}
	if (n_opt_steps != 0){
	  alpha_it.resize(n_opt_steps, vector<double> (n_channel, 0.));
	  for (int i_o = 0; i_o < n_opt_steps; i_o++){for (int i_c = 0; i_c < n_channel; i_c++){alpha_it[i_o][i_c] = hexastr2double(readin[proc++]);}}
	}
      }
      logger << LOG_INFO << "end " << name << "" << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void multichannel_set::proceeding_group_by_type_out(ofstream & out_proceeding){
  Logger logger("multichannel::proceeding_group_by_type_out " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){
    out_proceeding << "# " << name << " - end_optimization (1)" << endl;
    out_proceeding << "#=" << endl;
    out_proceeding << end_optimization << endl;
    out_proceeding << "# " << name << " - alpha (" << alpha.size() << ")" << endl;
    out_proceeding << "#=" << endl;
    for (size_t j = 0; j < alpha.size(); j++){out_proceeding << double2hexastr(alpha[j]) << endl;}

    if (end_optimization == 0){
      size_t n_opt_steps = alpha_it.size();
      out_proceeding << "# " << name << " - n_opt_steps (1)" << endl;
      out_proceeding << "#=" << endl;
      out_proceeding << n_opt_steps << endl;
      out_proceeding << "# " << name << " - n_acc (1) - n_rej (1)" << endl;
      out_proceeding << "#+" << endl;
      out_proceeding << n_acc << endl;
      out_proceeding << n_rej << endl;
      out_proceeding << "# " << name << " - n_acc_channel (" << alpha.size() << ")" << endl;
      out_proceeding << "#+" << endl;
      for (size_t j = 0; j < alpha.size(); j++){out_proceeding << n_acc_channel[j] << endl;}
      out_proceeding << "# " << name << " - n_rej_channel (" << alpha.size() << ")" << endl;
      out_proceeding << "#+" << endl;
      for (size_t j = 0; j < alpha.size(); j++){out_proceeding << n_rej_channel[j] << endl;}
      out_proceeding << "# " << name << " - w_sum_channel_weight (" << alpha.size() << ")" << endl;
      out_proceeding << "#+" << endl;
      for (size_t j = 0; j < alpha.size(); j++){out_proceeding << double2hexastr(w_channel_sum[j]) << endl;}
      if (switch_validation_optimization){
	out_proceeding << "# " << name << " - i_alpha_it (1) - x_alpha_it_min (1)" << endl;
	out_proceeding << "#=" << endl;
	out_proceeding << i_alpha_it << endl;
	out_proceeding << x_alpha_it_min << endl;
	out_proceeding << "# " << name << " - diff_w (" << n_opt_steps << ")" << endl;
	out_proceeding << "#=" << endl;
	for (size_t i = 0; i < n_opt_steps; i++){out_proceeding << double2hexastr(diff_w[i]) << endl;}
	out_proceeding << "# " << name << " - alpha_it (" << n_opt_steps << " x " << alpha.size() << ")" << endl;
	out_proceeding << "#=" << endl;
	for (size_t i = 0; i < n_opt_steps; i++){for (size_t j = 0; j < alpha.size(); j++){out_proceeding << double2hexastr(alpha_it[i][j]) << endl;}}
      }
    }
  }
  else {
    //    out_proceeding << "# " << name << " - no_optimization due to active_optimization = " << active_optimization << endl;
  }


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void multichannel_set::proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination){
  Logger logger("multichannel::proceeding_group_by_type_in " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (active_optimization != -1){

    end_optimization = atoi(readin[proc++].c_str());
    for (size_t j = 0; j < n_channel; j++){alpha[j] = hexastr2double(readin[proc++]);}

    beta[0] = alpha[0];
    for (size_t i = 1; i < beta.size(); i++){beta[i] = beta[i - 1] + alpha[i];}

    if (end_optimization == 0){
      size_t n_opt_steps = atoi(readin[proc++].c_str());
      if (switch_combination){
	n_acc = atoi(readin[proc++].c_str());
	n_rej = atoi(readin[proc++].c_str());
	for (size_t j = 0; j < n_channel; j++){n_acc_channel[j] = atoi(readin[proc++].c_str());}
	for (size_t j = 0; j < n_channel; j++){n_rej_channel[j] = atoi(readin[proc++].c_str());}
	for (size_t j = 0; j < n_channel; j++){w_channel_sum[j] = hexastr2double(readin[proc++]);}
      }
      else {proc += 2 + 3 * n_channel;}
      if (switch_validation_optimization){
	i_alpha_it = atoi(readin[proc++].c_str());
	x_alpha_it_min = atoi(readin[proc++].c_str());
	diff_w.resize(n_opt_steps);
	for (size_t i = 0; i < n_opt_steps; i++){diff_w[i] = hexastr2double(readin[proc++]);}
	if (n_opt_steps != 0){
	  alpha_it.resize(n_opt_steps, vector<double> (n_channel));
	  for (size_t i = 0; i < n_opt_steps; i++){for (size_t j = 0; j < n_channel; j++){alpha_it[i][j] = hexastr2double(readin[proc++]);}}
	}
      }
      logger << LOG_INFO << "end " << name << "" << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



ostream & operator << (ostream & s, const multichannel_set & MC){
  s << left << endl;
  s << setw(30) << "name" << " = " << MC.name << endl;
  s << setw(30) << "active_optimization" << " = " << MC.active_optimization << endl;
  if (MC.active_optimization != -1){
    s << setw(30) << "end_optimization" << " = " << MC.end_optimization << endl;
    s << setw(30) << "filename" << " = " << MC.filename << endl;
    s << setw(30) << "filename_readin" << " = " << MC.filename_readin << endl;
    s << endl;
    s << setw(30) << "n_channel" << " = " << MC.n_channel << endl;
    s << setw(30) << "n_optimization_step" << " = " << MC.n_optimization_step << endl;
    s << setw(30) << "n_event_per_step" << " = " << MC.n_event_per_step << endl;
    s << setw(30) << "threshold_optimization" << " = " << MC.threshold_optimization << endl;
    s << setw(30) << "switch_mode_optimization" << " = " << MC.switch_mode_optimization << endl;
    s << setw(30) << "switch_validation_optimization" << " = " << MC.switch_validation_optimization << endl;
    s << setw(30) << "switch_minimum_weight" << " = " << MC.switch_minimum_weight << endl;
    s << setw(30) << "reserved_minimum_weight" << " = " << MC.reserved_minimum_weight << endl;
    s << setw(30) << "alpha_reserved_min" << " = " << MC.alpha_reserved_min << endl;
    s << setw(30) << "alpha_maximum_weight" << " = " << MC.alpha_maximum_weight << endl;
    s << setw(30) << "weight_adaptation_exponent" << " = " << MC.weight_adaptation_exponent << endl;
    s << endl;
    s << setw(30) << "i_alpha_it" << " = " << MC.i_alpha_it << endl;
    s << setw(30) << "x_alpha_it_min" << " = " << MC.x_alpha_it_min << endl;
    s << endl;
    s << setw(30) << "n_acc" << " = " << MC.n_acc << endl;
    s << setw(30) << "n_rej" << " = " << MC.n_rej << endl;
    s << endl;
    for (int i_s = MC.x_alpha_it_min; i_s < MC.i_alpha_it; i_s++){s << setw(27) << "diff_w[" << setw(2) << i_s << "]" << " = " << MC.diff_w[i_s] << endl;}
    s << endl;
    if (MC.end_optimization == 0){
      for (int i_c = 0; i_c < MC.n_channel; i_c++){
	s << MC.name << " [" << setw(3) << i_c << "]:" << setprecision(8) << showpoint << "   ";
	s << "sum_omega = " << setw(15) << left << MC.w_channel_sum[i_c];
	s << "   ";
	s << "n_acc = " << setw(10) << right << MC.n_acc_channel[i_c];
	s << "   ";
	s << "n_rej = " << setw(10) << right << MC.n_rej_channel[i_c];
	s << "   ";
	s << "alpha = " << setw(15) << left << MC.alpha[i_c];
	if (MC.i_alpha_it > 0){s << " (" << setw(15) << MC.alpha_it[MC.i_alpha_it - 1][i_c] << ")";}
	s << "   ";
	s << noshowpoint << endl;
      }
      s << endl;
    }
  }
  return s;
}

int multichannel_set::operator == (const multichannel_set & MC) const{
  Logger logger("multichannel_set::operator == " + name);
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int equality = 1;
  int diff_counter = 0;
  if (name != MC.name){logger << LOG_INFO << setw(40) << "name" << "  is different." << endl; diff_counter++;}
  if (switch_optimization != MC.switch_optimization){logger << LOG_INFO << setw(40) << "switch_optimization" << "  is different." << endl; diff_counter++;}
  if (n_channel != MC.n_channel){logger << LOG_INFO << setw(40) << "n_channel" << "  is different." << endl; diff_counter++;}
  if (n_event_per_step != MC.n_event_per_step){logger << LOG_INFO << setw(40) << "n_event_per_step" << "  is different." << endl; diff_counter++;}
  if (n_optimization_step != MC.n_optimization_step){logger << LOG_INFO << setw(40) << "n_optimization_step" << "  is different." << endl; diff_counter++;}
  //  if (switch_increase_n_event_per_step != MCswitch_increase_n_event_per_step.){logger << LOG_INFO << setw(40) << "switch_increase_n_event_per_step" << "  is different." << endl; diff_counter++;}
  if (threshold_optimization != MC.threshold_optimization){logger << LOG_INFO << setw(40) << "threshold_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_mode_optimization != MC.switch_mode_optimization){logger << LOG_INFO << setw(40) << "switch_mode_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_validation_optimization != MC.switch_validation_optimization){logger << LOG_INFO << setw(40) << "switch_validation_optimization" << "  is different." << endl; diff_counter++;}
  if (switch_minimum_weight != MC.switch_minimum_weight){logger << LOG_INFO << setw(40) << "switch_minimum_weight" << "  is different." << endl; diff_counter++;}
  if (weight_adaptation_exponent != MC.weight_adaptation_exponent){logger << LOG_INFO << setw(40) << "weight_adaptation_exponent" << "  is different." << endl; diff_counter++;}
  if (reserved_minimum_weight != MC.reserved_minimum_weight){logger << LOG_INFO << setw(40) << "reserved_minimum_weight" << "  is different." << endl; diff_counter++;}
  if (alpha_maximum_weight != MC.alpha_maximum_weight){logger << LOG_INFO << setw(40) << "alpha_maximum_weight" << "  is different." << endl; diff_counter++;}
  //  if ( != MC.){logger << LOG_INFO << setw(40) << "" << "  is different." << endl; diff_counter++;}
  
  if (diff_counter > 0){equality = 0;}
  
  logger << LOG_DEBUG_VERBOSE << "finished - return " << equality << endl;
  return equality;
}
