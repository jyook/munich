#include "header.hpp"

void phasespace_set::initialization(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, event_set * _esi, user_defined * _user){
  static  Logger logger("phasespace_set::initialization");
  logger << LOG_DEBUG << "called" << endl;

  csi = _csi;
  msi = _msi;
  esi = _esi;
  user = _user;

  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_set_default(){
  static  Logger logger("phasespace_set::initialization_set_default");
  logger << LOG_DEBUG << "called" << endl;

  ///////////////////////
  //  beam parameters  //
  ///////////////////////

  E = 0.;
  coll_choice = 0;

  ///////////////////////////////
  //  random-number generator  //
  ///////////////////////////////

  switch_off_random_generator = 0;
  switch_IO_generator = 0;
  n_shift_run = 0;
  //  zwahl = 0;

  //////////////////////////////////////////
  //  new weight-optimization parameters  //
  //////////////////////////////////////////

  input_MC_phasespace.name = "MC_phasespace";
  input_MC_tau.name = "MC_tau";
  input_MC_x_dipole.name = "MC_x_dipole";
  
  input_IS_tau.name = "IS_tau";
  input_IS_x1x2.name = "IS_x1x2";
  if (csi->class_contribution_collinear){input_IS_z1z2.name = "IS_z1z2";}
  //  if (csi->class_contribution_resummation){input_IS_qTres = "IS_qTres";}
  input_IS_PS_p.name = "IS_PS_p";
  input_IS_PS_f.name = "IS_PS_f";
  input_IS_PS_d_phi.name = "IS_PS_d_phi";
  input_IS_PS_d_costheta.name = "IS_PS_d_costheta";
  input_IS_PS_t_t.name = "IS_PS_t_t";
  input_IS_PS_t_phi.name = "IS_PS_t_phi";
  if (csi->class_contribution_CS_real){
    input_IS_PS_dipole_xy.name = "IS_PS_dipole_xy";
    input_IS_PS_dipole_uvz.name = "IS_PS_dipole_uvz";
    input_IS_PS_dipole_phi.name = "IS_PS_dipole_phi";
  }
  
  input_MC.push_back(&input_MC_phasespace);
  input_MC.push_back(&input_MC_tau);
  if (csi->class_contribution_CS_real){input_MC.push_back(&input_MC_x_dipole);}

  input_IS.push_back(&input_IS_PS_p);
  input_IS.push_back(&input_IS_PS_f);
  input_IS.push_back(&input_IS_PS_d_phi);
  input_IS.push_back(&input_IS_PS_d_costheta);
  input_IS.push_back(&input_IS_PS_t_t);
  input_IS.push_back(&input_IS_PS_t_phi);
  input_IS.push_back(&input_IS_tau);
  input_IS.push_back(&input_IS_x1x2);
  if (csi->class_contribution_collinear){input_IS.push_back(&input_IS_z1z2);}
  //  if (csi->class_contribution_resummation){input_IS.push_back(&input_IS_qTres);
  if (csi->class_contribution_CS_real){
    input_IS.push_back(&input_IS_PS_dipole_xy);
    input_IS.push_back(&input_IS_PS_dipole_uvz);
    input_IS.push_back(&input_IS_PS_dipole_phi);
  }

  for (int i_m = 0; i_m < input_MC.size(); i_m++){logger << LOG_INFO << *input_MC[i_m] << endl;}
  for (int i_s = 0; i_s < input_IS.size(); i_s++){logger << LOG_INFO << *input_IS[i_s] << endl;}
  
  //////////////////////////////////////
  //  weight-optimization parameters  //
  //////////////////////////////////////

  switch_n_events_opt = 0;
  switch_step_mode_grid = 0;
  switch_output_weights = 1;
  weight_in_directory = "";

  switch_use_alpha_after_IS = 0;
  switch_IS_mode_phasespace = 0;

  //////////////////////////////////////////////////////////////////////////////////////
  //  technical switches for selected phase space parametrization in RS contribution  //
  //////////////////////////////////////////////////////////////////////////////////////

  switch_off_RS_mapping = 0;
  switch_off_RS_mapping_ij_k = 0;
  switch_off_RS_mapping_ij_a = 0;
  switch_off_RS_mapping_ai_k = 0;
  switch_off_RS_mapping_ai_b = 0;

  /////////////////////////////////////////
  //  phase-space generation parameters  //
  /////////////////////////////////////////

  nuxs = 0.;
  nuxt = 0.;
  exp_pdf = 0.;

  exp_ij_k_y = 0.;
  exp_ij_k_z = 0.;
  exp_ij_a_x = 0.;
  exp_ij_a_z = 0.;
  exp_ai_k_x = 0.;
  exp_ai_k_u = 0.;
  exp_ai_b_x = 0.;
  exp_ai_b_v = 0.;

  ////////////////////////////////////////
  //  technical integration parameters  //
  ////////////////////////////////////////

  mass0 = 0.;
  map_technical_s = 0.;
  map_technical_t = 0.;
  map_technical_x = 0.;
  cut_technical = 0.;

  //////////////////////////////
  //  resummation parameters  //
  //////////////////////////////

  switch_resummation = 0;
  switch_dynamic_Qres = 0;
  Qres = 0.;
  Qres_prefactor = 1.;

  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////

  switch_qTcut = 0;
  min_qTcut = 0.;

  ///////////////////////////////////////////////
  //  switches to steer output of calculation  //
  ///////////////////////////////////////////////

  switch_console_output_phasespace_issue = 1;

  //////////////////////////////
  //  integration parameters  //
  //////////////////////////////
  //  not only in psi !!!

  n_events_max = 0;
  n_events_min = 0;
  n_step = 0;

  ///////////////////////////////////////////////
  //  selection of run_mode (grid, time, run)  //
  ///////////////////////////////////////////////

  run_mode = "";

  // to avoid warnings about use of uninitialized variables:
  boost = 0;

  // Check if some parts from constructor need to be moved !!!

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_input(inputparameter_set * isi){
  static Logger logger("phasespace_set::initialization_input");
  logger << LOG_DEBUG << "called" << endl;

  int temp_type_perturbative_order = -1;
  int temp_type_contribution = -1;
  int temp_type_correction = -1;
  temp_type_perturbative_order = isi->present_type_perturbative_order;
  temp_type_contribution = isi->present_type_contribution;
  temp_type_correction = isi->present_type_correction;

  for (int i_i = 0; i_i < isi->input_phasespace.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_phasespace[" << i_i << "] = " << isi->input_phasespace[i_i] << endl;

    if (isi->input_phasespace[i_i].variable == ""){}

    ///////////////////////
    //  beam parameters  //
    ///////////////////////

    else if (isi->input_phasespace[i_i].variable == "coll_choice"){coll_choice = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "E"){E = atof(isi->input_phasespace[i_i].value.c_str());}

    else if (isi->input_phasespace[i_i].variable == "switch_off_random_generator"){switch_off_random_generator = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "switch_IO_generator"){switch_IO_generator = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "zwahl"){n_shift_run = atoi(isi->input_phasespace[i_i].value.c_str());}

    ///////////////////////////////////////////////
    //  switches to steer output of calculation  //
    ///////////////////////////////////////////////

    else if (isi->input_phasespace[i_i].variable == "switch_console_output_phasespace_issue"){switch_console_output_phasespace_issue = atoi(isi->input_phasespace[i_i].value.c_str());}

    /////////////////////////////////
    //  qT-resummation parameters  //
    /////////////////////////////////

    else if (isi->input_phasespace[i_i].variable == "switch_resummation"){switch_resummation = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "switch_dynamic_Qres"){switch_dynamic_Qres = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "Qres"){Qres = atof(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "Qres_prefactor"){Qres_prefactor = atof(isi->input_phasespace[i_i].value.c_str());}

    /////////////////////////////////
    //  qT-subtraction parameters  //
    /////////////////////////////////

    // All qT-subtraction parameters: probably not needed directly in psi !!!
    else if (isi->input_phasespace[i_i].variable == "switch_qTcut"){switch_qTcut = atoi(isi->input_phasespace[i_i].value.c_str());}
    else if (isi->input_phasespace[i_i].variable == "min_qTcut"){min_qTcut = atof(isi->input_phasespace[i_i].value.c_str());}


    //////////////////////////////////////////////////////////////
    //  selection of process and contribution to be calculated  //
    //////////////////////////////////////////////////////////////

    else if (isi->input_phasespace[i_i].variable == "type_perturbative_order"){
      temp_type_perturbative_order = -1;
      for (int i_to = 0; i_to < isi->collection_type_perturbative_order.size(); i_to++){
	if (isi->input_phasespace[i_i].value == "all"){temp_type_perturbative_order = isi->present_type_perturbative_order; break;}
	else if (isi->input_phasespace[i_i].value == isi->collection_type_perturbative_order[i_to]){temp_type_perturbative_order = i_to; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_phasespace[i_i].variable << " = " << setw(32) << isi->input_phasespace[i_i].value << setw(32) << "temp_type_perturbative_order = " << temp_type_perturbative_order << endl;
    }
    else if (isi->input_phasespace[i_i].variable == "type_contribution"){
      temp_type_contribution = -1;
      vector<string> list_contribution;
      // Check if more than one contribution is specified:
      int new_contribution = true;
      for (int i_s = 0; i_s < isi->input_phasespace[i_i].value.size(); i_s++){
	if (isi->input_phasespace[i_i].value[i_s] == ',' || isi->input_phasespace[i_i].value[i_s] == ' '){new_contribution = true;}
	else {
	  if (new_contribution){list_contribution.push_back(""); new_contribution = false;}
	  list_contribution[list_contribution.size() - 1].push_back(isi->input_phasespace[i_i].value[i_s]);
	}
      }
      
      for (int i_l = 0; i_l < list_contribution.size(); i_l++){logger << LOG_DEBUG << "list_contribution[" << i_l << "] = " << list_contribution[i_l] << endl;}
      
      if (list_contribution.size() == 1){
	for (int i_tc = 0; i_tc < isi->collection_type_contribution.size(); i_tc++){
	  if (isi->input_phasespace[i_i].value == "all"){temp_type_contribution = isi->present_type_contribution; break;}
	  else if (isi->input_phasespace[i_i].value == isi->collection_type_contribution[i_tc]){temp_type_contribution = i_tc; break;}
	}
      }
      else {
	// Could work in previous case as well ???
	for (int i_l = 0; i_l < list_contribution.size(); i_l++){
	  if (list_contribution[i_l] == isi->collection_type_contribution[isi->present_type_contribution]){temp_type_contribution = isi->present_type_contribution; break;}
	}
      }
      
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_phasespace[i_i].variable << " = " << setw(32) << isi->input_phasespace[i_i].value << setw(32) << "temp_type_contribution = " << temp_type_contribution << endl;

      /* // old implementation allowing for only one value:
      for (int i_tc = 0; i_tc < isi->collection_type_contribution.size(); i_tc++){
	if (isi->input_phasespace[i_i].value == "all"){temp_type_contribution = isi->present_type_contribution; break;}
	else if (isi->input_phasespace[i_i].value == isi->collection_type_contribution[i_tc]){temp_type_contribution = i_tc; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_phasespace[i_i].variable << " = " << setw(32) << isi->input_phasespace[i_i].value << setw(32) << "temp_type_contribution = " << temp_type_contribution << endl;
      */
    }
    else if (isi->input_phasespace[i_i].variable == "type_correction"){
      temp_type_correction = -1;
      for (int i_tc = 0; i_tc < isi->collection_type_correction.size(); i_tc++){
	if (isi->input_phasespace[i_i].value == "all"){temp_type_correction = isi->present_type_correction; break;}
	else if (isi->input_phasespace[i_i].value == isi->collection_type_correction[i_tc]){temp_type_correction = i_tc; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_phasespace[i_i].variable << " = " << setw(32) << isi->input_phasespace[i_i].value << setw(32) << "temp_type_correction = " << temp_type_correction << endl;
    }


    else if ((temp_type_perturbative_order == isi->present_type_perturbative_order &&
	      temp_type_contribution == isi->present_type_contribution &&
	      temp_type_correction == isi->present_type_correction)){
      logger << LOG_DEBUG << "HERE user_variable[" << i_i << "] = " << left << setw(32) << isi->input_phasespace[i_i].variable << " = " << setw(32) << isi->input_phasespace[i_i].value << endl;


      if (isi->input_phasespace[i_i].variable == ""){}

      //////////////////////////////////////////
      //  new weight-optimization parameters  //
      //////////////////////////////////////////

      else if (isi->input_phasespace[i_i].variable == "MC"){
	logger << LOG_DEBUG << "isi->input_phasespace[i_i].variable = MC" << endl;
	string MC_parameter = "";
	string MC_variable = "";
	int flag = -1;
	for (int i_s = 0; i_s < isi->input_phasespace[i_i].specifier.size(); i_s++){
	  if (isi->input_phasespace[i_i].specifier[i_s] == ' '){flag = i_s; break;}
	}
	if (flag == -1){
	  MC_parameter = isi->input_phasespace[i_i].specifier;
	}
	else {
	  MC_parameter = isi->input_phasespace[i_i].specifier.substr(0, flag);
	  MC_variable = isi->input_phasespace[i_i].specifier.substr(flag + 1);
	}
	int counter = 0;
	for (int i_m = 0; i_m < input_MC.size(); i_m++){
	  if (flag == -1 || MC_variable == input_MC[i_m]->name.substr(0, MC_variable.size())){
	    int flag_no_match = 0;
	    if (MC_parameter == "switch_optimization"){input_MC[i_m]->switch_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "n_optimization_step"){input_MC[i_m]->n_optimization_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "n_optimization_step_extra"){input_MC[i_m]->n_optimization_step_extra = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "n_event_per_step"){input_MC[i_m]->n_event_per_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "switch_increase_n_event_per_step"){input_MC[i_m]->switch_increase_n_event_per_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "threshold_optimization"){input_MC[i_m]->threshold_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "switch_mode_optimization"){input_MC[i_m]->switch_mode_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "switch_validation_optimization"){input_MC[i_m]->switch_validation_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "switch_minimum_weight"){input_MC[i_m]->switch_minimum_weight = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "weight_adaptation_exponent"){input_MC[i_m]->weight_adaptation_exponent = atof(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "reserved_minimum_weight"){input_MC[i_m]->reserved_minimum_weight = atof(isi->input_phasespace[i_i].value.c_str());}
	    else if (MC_parameter == "alpha_maximum_weight"){input_MC[i_m]->alpha_maximum_weight = atof(isi->input_phasespace[i_i].value.c_str());}
	    else {flag_no_match = 1;}
	    if (!flag_no_match){
	      counter++;
	      stringstream temp_input;
	      temp_input << isi->input_phasespace[i_i].variable << " " << isi->input_phasespace[i_i].specifier << " = " << isi->input_phasespace[i_i].value;
	      stringstream temp_output;
	      temp_output << input_MC[i_m]->name << " : " << MC_parameter;
	      logger << LOG_DEBUG << left << setw(50) << temp_input.str() << "  ->  " << setw(50) << temp_output.str() << endl;
	    }
	  }
	}
	if (counter == 0){
	  logger << LOG_DEBUG << isi->input_phasespace[i_i].variable << " " << isi->input_phasespace[i_i].specifier << " = " << isi->input_phasespace[i_i].value << "  could not bei interpreted." << endl;
	  //	  exit(1);
	}
      }

      else if (isi->input_phasespace[i_i].variable == "IS"){
	logger << LOG_DEBUG << "isi->input_phasespace[" << i_i << "].variable = IS" << endl;
	string IS_parameter = "";
	string IS_variable = "";
	int flag = -1;
	for (int i_s = 0; i_s < isi->input_phasespace[i_i].specifier.size(); i_s++){
	  if (isi->input_phasespace[i_i].specifier[i_s] == ' '){flag = i_s; break;}
	}
	if (flag == -1){
	  IS_parameter = isi->input_phasespace[i_i].specifier;
	}
	else {
	  IS_parameter = isi->input_phasespace[i_i].specifier.substr(0, flag);
	  IS_variable = isi->input_phasespace[i_i].specifier.substr(flag + 1);
	}
	int counter = 0;
	for (int i_s = 0; i_s < input_IS.size(); i_s++){
	  if (flag == -1 || IS_variable == input_IS[i_s]->name.substr(0, IS_variable.size())){
	    int flag_no_match = 0;
	    if (IS_parameter == "switch_optimization"){input_IS[i_s]->switch_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "n_gridsize"){input_IS[i_s]->n_gridsize = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "n_optimization_step"){input_IS[i_s]->n_optimization_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "n_optimization_step_extra"){input_IS[i_s]->n_optimization_step_extra = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "n_event_per_step"){input_IS[i_s]->n_event_per_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "switch_increase_n_event_per_step"){input_IS[i_s]->switch_increase_n_event_per_step = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "threshold_optimization"){input_IS[i_s]->threshold_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "switch_mode_optimization"){input_IS[i_s]->switch_mode_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "switch_validation_optimization"){input_IS[i_s]->switch_validation_optimization = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "switch_minimum_weight"){input_IS[i_s]->switch_minimum_weight = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "switch_smearing_level"){input_IS[i_s]->switch_smearing_level = atoi(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "weight_adaptation_exponent"){input_IS[i_s]->weight_adaptation_exponent = atof(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "reserved_minimum_weight"){input_IS[i_s]->reserved_minimum_weight = atof(isi->input_phasespace[i_i].value.c_str());}
	    else if (IS_parameter == "alpha_maximum_weight"){input_IS[i_s]->alpha_maximum_weight = atof(isi->input_phasespace[i_i].value.c_str());}
	    else {flag_no_match = 1;}
	    if (!flag_no_match){
	      counter++;
	      stringstream temp_input;
	      temp_input << isi->input_phasespace[i_i].variable << " " << isi->input_phasespace[i_i].specifier << " = " << isi->input_phasespace[i_i].value;
	      stringstream temp_output;
	      temp_output << input_IS[i_s]->name << " : " << IS_parameter;
	      logger << LOG_INFO << left << setw(50) << temp_input.str() << "  ->  " << setw(50) << temp_output.str() << endl;
	    }
	  }
	}
	if (counter == 0){
	  logger << LOG_INFO << isi->input_phasespace[i_i].variable << " " << isi->input_phasespace[i_i].specifier << " = " << isi->input_phasespace[i_i].value << "  could not bei interpreted." << endl;
	  //	  exit(1);
	}
      }

      //////////////////////////////////////
      //  weight-optimization parameters  //
      //////////////////////////////////////

      else if (isi->input_phasespace[i_i].variable == "switch_n_events_opt"){switch_n_events_opt = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_step_mode_grid"){switch_step_mode_grid = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_output_weights"){switch_output_weights = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "MCweight_in_directory"){weight_in_directory = isi->input_phasespace[i_i].value;}

      else if (isi->input_phasespace[i_i].variable == "switch_use_alpha_after_IS"){switch_use_alpha_after_IS = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_IS_mode_phasespace"){switch_IS_mode_phasespace = atoi(isi->input_phasespace[i_i].value.c_str());}

      //////////////////////////////////////////////////////////////////////////////////////
      //  technical switches for selected phase space parametrization in RS contribution  //
      //////////////////////////////////////////////////////////////////////////////////////

      // for compatibility with old name of switch !!!
      else if (isi->input_phasespace[i_i].variable == "switch_RS_mapping"){switch_off_RS_mapping = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_off_RS_mapping"){switch_off_RS_mapping = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_off_RS_mapping_ij_k"){switch_off_RS_mapping_ij_k = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_off_RS_mapping_ij_a"){switch_off_RS_mapping_ij_a = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_off_RS_mapping_ai_k"){switch_off_RS_mapping_ai_k = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "switch_off_RS_mapping_ai_b"){switch_off_RS_mapping_ai_b = atoi(isi->input_phasespace[i_i].value.c_str());}

      /////////////////////////////////////////
      //  phase-space generation parameters  //
      /////////////////////////////////////////

      else if (isi->input_phasespace[i_i].variable == "nuxs"){nuxs = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "nuxt"){nuxt = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_pdf"){exp_pdf = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ij_k_y"){exp_ij_k_y = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ij_k_z"){exp_ij_k_z = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ij_a_x"){exp_ij_a_x = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ij_a_z"){exp_ij_a_z = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ai_k_x"){exp_ai_k_x = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ai_k_u"){exp_ai_k_u = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ai_b_x"){exp_ai_b_x = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "exp_ai_b_v"){exp_ai_b_v = atof(isi->input_phasespace[i_i].value.c_str());}

      ////////////////////////////////////////
      //  technical integration parameters  //
      ////////////////////////////////////////

      else if (isi->input_phasespace[i_i].variable == "mass0"){mass0 = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "map_technical_s"){map_technical_s = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "map_technical_t"){map_technical_t = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "map_technical_x"){map_technical_x = atof(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "cut_technical"){cut_technical = atof(isi->input_phasespace[i_i].value.c_str());}

      //////////////////////////////
      //  integration parameters  //
      //////////////////////////////

      // all of these three needed in psi ???
      else if (isi->input_phasespace[i_i].variable == "n_events_max"){n_events_max = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "n_events_min"){n_events_min = atoi(isi->input_phasespace[i_i].value.c_str());}
      else if (isi->input_phasespace[i_i].variable == "n_step"){n_step = atoi(isi->input_phasespace[i_i].value.c_str());}

      ///////////////////////////////////////////////
      //  selection of run_mode (grid, time, run)  //
      ///////////////////////////////////////////////
      
      else if (isi->input_phasespace[i_i].variable == "run_mode"){run_mode = isi->input_phasespace[i_i].value;}

    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void phasespace_set::initialization_after_input(){
  static  Logger logger("phasespace_set::initialization_after_input");
  logger << LOG_DEBUG << "called" << endl;

  // Not really the default: needs to be called after initialization_input !!!
  // Actually sets the start values for integration run.

  // !!! must be changed: type_parton is different in oss and pss !!! should be solved now...
  hcf = pow(0.197326968E-13, 2.) * 1.e+39 / (2. * pow(2. * pi, 3 * csi->n_particle - 4));

  xb_max = intpow(2, csi->n_particle + 2);
  xb_max_dipoles = intpow(2, csi->n_particle + 2 - 1);
  xb_out_dipoles = xb_max_dipoles - 4;

  MC_channel_phasespace = 0;

  if (switch_off_RS_mapping == 1){
    switch_off_RS_mapping_ij_k = 1;
    switch_off_RS_mapping_ij_a = 1;
    switch_off_RS_mapping_ai_k = 1;
    switch_off_RS_mapping_ai_b = 1;
  }

  s_had = pow(2 * E, 2);
  //  cut2_pT = pow(min_qTcut, 2.);

  MC_opt_end = 0; // to avoid 'uninitialized' warning !!!
  end_optimization = 0; // to avoid 'uninitialized' warning !!!

  tau_0_num.resize(1, 1.);

  i_gen = 0;
  i_rej = 0;
  i_acc = 0;
  i_nan = 0;
  i_tec = 0;

  last_step_mode = 0;

  n_random_per_psp = 0;
  // number of degrees of freedom of csi->n_particle phasespace:
  no_random = 3 * csi->n_particle - 4;

  RA_x_a = 0; // to avoid warning !!!

  csi->swap_parton.resize(1);
  csi->swap_parton[0].resize(3 + csi->n_particle, 0);

  logger << LOG_DEBUG << "finished" << endl;
}


