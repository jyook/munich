#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::set_mu_and_delta_VA(){
  Logger logger("amplitude_Recola_set::set_mu_and_delta_VA");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_delta_uv_rcl(osi->VA_DeltaUV);
  set_delta_ir_rcl(osi->VA_DeltaIR1, osi->VA_DeltaIR2 + (1. - osi->switch_polenorm) * pi2_6);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::set_testpoint_alpha_S(const double alpha_S){
  //void amplitude_Recola_set::set_testpoint_alpha_S(){
  Logger logger("amplitude_Recola_set::set_testpoint_alpha_S");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Check !!!
  if (osi->switch_VI != 2){
    set_alphas_rcl(alpha_S, osi->var_mu_ren, N_nondecoupled);
    rescale_process_rcl(1, "LO");
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::set_testpoint_rescale_I_ME2(){
  Logger logger("amplitude_Recola_set::set_testpoint_rescale_I_ME2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI != 2){
    osi->VA_I_ME2 = osi->VA_I_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




void amplitude_Recola_set::calculate_ME2_ioperator_VA_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_ioperator_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi->VA_ioperator->size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      osi->VA_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
      osi->VA_I_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
    }
    initialization = 0;
  }

  double P_rec[esi->p_parton[0].size()][4];
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[0][i].x0();
    P_rec[i - 1][1] = esi->p_parton[0][i].x1();
    P_rec[i - 1][2] = esi->p_parton[0][i].x2();
    P_rec[i - 1][3] = esi->p_parton[0][i].x3();
  }

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }

  if ((csi->type_contribution == "VA" ||
       csi->type_contribution == "RVA" ||
       csi->type_contribution == "RVJ") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){
    if (osi->switch_VI == 2){
      // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.

      compute_process_rcl(1, P_rec, "LO");
      logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "LO" << char(34) << ");" << endl;
      // New ...
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "LO", osi->VA_b_ME2);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
      //      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;
      osi->VA_b_ME2 = osi->VA_b_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
      // ... until here.


      set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
      logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;
    }
    compute_all_colour_correlations_rcl(1, P_rec);
    logger << LOG_DEBUG_VERBOSE << "compute_all_colour_correlations_rcl(1, P_rec);" << endl;

    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, (*(osi->VA_ioperator))[i_a][j_a].no_emitter(), (*(osi->VA_ioperator))[i_a][j_a].no_spectator(), osi->VA_ME2_cf[i_a][j_a]);
	logger << LOG_DEBUG_VERBOSE << "get_colour_correlation_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_emitter() << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_spectator() << ", " << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << ");" << endl;
	logger << LOG_DEBUG_VERBOSE << "(*(osi->VA_ioperator))[" << i_a << "][" << j_a << "].colour_factor() = " <<  (*(osi->VA_ioperator))[i_a][j_a].colour_factor() << endl;
	osi->VA_ME2_cf[i_a][j_a] = osi->VA_ME2_cf[i_a][j_a] * (*(osi->VA_ioperator))[i_a][j_a].colour_factor();
	logger << LOG_DEBUG_POINT << "Recola:     VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
      }
    }
  }
  else if (csi->type_contribution == "L2VA" ||
	   osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){
    // not yet implemented !!!
    if (osi->switch_VI == 2){
      set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
      logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;

      compute_process_rcl(1, P_rec, "NLO");
      logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "NLO" << char(34) << ");" << endl;
    }
    compute_all_colour_correlations_rcl(1, P_rec, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_all_colour_correlations_rcl(1, P_rec, " << char(34) << "NLO" << char(34) << ");" << endl;

    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, (*(osi->VA_ioperator))[i_a][j_a].no_emitter(), (*(osi->VA_ioperator))[i_a][j_a].no_spectator(), "NLO", osi->VA_ME2_cf[i_a][j_a]);
	logger << LOG_DEBUG_VERBOSE << "get_colour_correlation_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_emitter() << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_spectator() << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << ");" << endl;
	logger << LOG_DEBUG_VERBOSE << "(*(osi->VA_ioperator))[" << i_a << "][" << j_a << "].colour_factor() = " <<  (*(osi->VA_ioperator))[i_a][j_a].colour_factor() << endl;
	osi->VA_ME2_cf[i_a][j_a] = osi->VA_ME2_cf[i_a][j_a] * (*(osi->VA_ioperator))[i_a][j_a].colour_factor();
	logger << LOG_DEBUG_POINT << "Recola:     VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
      }
    }
  }

  if (osi->massive_QCD){osi->calculate_ioperator_QCD_CDST();}
  else {osi->calculate_ioperator_QCD_CS();}

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi->VA_I_ME2_cf[i_a].begin(), osi->VA_I_ME2_cf[i_a].end(), 0.);
  }
  osi->VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::calculate_ME2_VA_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI == 0 || osi->switch_VI == 1){
    double P_rec[esi->p_parton[0].size()][4];
    for (int i = 1; i < esi->p_parton[0].size(); i++){
      P_rec[i - 1][0] = esi->p_parton[0][i].x0();
      P_rec[i - 1][1] = esi->p_parton[0][i].x1();
      P_rec[i - 1][2] = esi->p_parton[0][i].x2();
      P_rec[i - 1][3] = esi->p_parton[0][i].x3();
    }

    for (int i = 1; i < esi->p_parton[0].size(); i++){
      stringstream temp_ss;
      for (int j = 0; j < 4; j++){
	temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
      }
      logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
    }

    if ((csi->type_contribution == "VA" ||
	 csi->type_contribution == "RVA" ||
	 csi->type_contribution == "RVJ") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){

      set_mu_uv_rcl(osi->var_mu_ren);
      logger << LOG_DEBUG_VERBOSE << "set_mu_uv_rcl(" << setprecision(15) << osi->var_mu_ren << ");" << endl;

      set_mu_ir_rcl(osi->var_mu_ren);
      logger << LOG_DEBUG_VERBOSE << "set_mu_ir_rcl(" << setprecision(15) << osi->var_mu_ren << ");" << endl;

      logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;

      set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);

      logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;

      compute_process_rcl(1, P_rec, "NLO");
      logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "NLO" << char(34) << ");" << endl;

      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "LO", osi->VA_b_ME2);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
      //      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;
      osi->VA_b_ME2 = osi->VA_b_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

      osi->VA_V_ME2 = 0.;
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->VA_X_ME2);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2 << ");" << endl;
      //      logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
      osi->VA_X_ME2 = osi->VA_X_ME2 / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

      if (osi->switch_output_comparison){
	double V_ME2_D4 = 0.;
	get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-D4", V_ME2_D4);
	logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-D4" << char(34) << ", " << setprecision(15) << V_ME2_D4 << ");" << endl;
	V_ME2_D4 = V_ME2_D4 / osi->var_rel_alpha_S;
	logger << LOG_DEBUG_POINT << "Recola:     ME2_D4 = " << setw(23) << setprecision(15) << V_ME2_D4 << "   (corrected for var_rel_alpha_S)" << endl;

	double V_ME2_R2 = 0.;
	get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-R2", V_ME2_R2);
	logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-R2" << char(34) << ", " << setprecision(15) << V_ME2_R2 << ");" << endl;
	V_ME2_R2 = V_ME2_R2 / osi->var_rel_alpha_S;
	logger << LOG_DEBUG_POINT << "Recola:     ME2_R2 = " << setw(23) << setprecision(15) << V_ME2_R2 << "   (corrected for var_rel_alpha_S)" << endl;

	double V_ME2_CT = 0.;
	get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-CT", V_ME2_CT);
	logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-CT" << char(34) << ", " << setprecision(15) << V_ME2_CT << ");" << endl;
	V_ME2_CT = V_ME2_CT / osi->var_rel_alpha_S;
	logger << LOG_DEBUG_POINT << "Recola:     ME2_CT = " << setw(23) << setprecision(15) << V_ME2_CT << "   (corrected for var_rel_alpha_S)" << endl;

	//	logger << LOG_DEBUG << "" << endl;

	// in order to match the OpenLoops output:

	osi->VA_X_ME2 = V_ME2_CT;
	osi->VA_V_ME2 = V_ME2_D4 + V_ME2_R2;

	logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;
      }

      osi->VA_I_ME2 = 0.;
      calculate_ME2_ioperator_VA_QCD();
      //      logger << LOG_DEBUG << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << endl;
      osi->VA_I_ME2 = osi->VA_I_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << "   (corrected for var_rel_alpha_S)" << endl;



      if (osi->switch_CV){
	for (int s = 0; s < osi->n_scales_CV; s++){
	  set_alphas_rcl(osi->var_alpha_S_CV[s], osi->var_mu_ren_CV[s], N_nondecoupled);
	  logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_CV[s] << ", " << setprecision(15) << osi->var_mu_ren_CV[s] << ", " << N_nondecoupled << ");" << endl;

	  rescale_process_rcl(1, "NLO");
	  logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;

	  /*
	  double temp_alphaS = osi->alpha_S * pow(osi->var_rel_alpha_S_CV[s], double(csi->contribution_order_alpha_s - 1) / csi->contribution_order_alpha_s);
	  set_alphas_rcl(temp_alphaS, osi->var_mu_ren_CV[s], N_nondecoupled);
	  compute_process_rcl(1, P_rec, "NLO");
	  */
	  //	  set_alphas_rcl(osi->var_rel_alpha_S_CV[s], osi->var_mu_ren_CV[s], N_nondecoupled);
	  //	  compute_process_rcl(1, P_rec, "NLO");

	  osi->VA_X_ME2_CV[s] = 0.;
	  //	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-CT", osi->VA_X_ME2_CV[s]);

	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->VA_X_ME2_CV[s]);
	  logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2_CV[s] << ");" << endl;
	  osi->VA_X_ME2_CV[s] = osi->VA_X_ME2_CV[s] / osi->var_rel_alpha_S_CV[s];
	  logger << LOG_DEBUG_POINT << "Recola:     VA_X_ME2_CV[" << s << "] = " << setw(23) << setprecision(15) << osi->VA_X_ME2_CV[s] << "   (after removing alpha_S factor)" << endl;
	}
      }


      if (osi->switch_TSV){
	for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	  for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	    set_alphas_rcl(osi->value_alpha_S_TSV[0][i_v][i_r], osi->value_scale_ren[0][i_v][i_r], N_nondecoupled);
	    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->value_alpha_S_TSV[0][i_v][i_r] << ", " << setprecision(15) << osi->value_scale_ren[0][i_v][i_r] << ", " << N_nondecoupled << ");" << endl;

    	    rescale_process_rcl(1, "NLO");
	    logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;

	    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->value_ME2term_ren[0][i_v][i_r]);
	    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->value_ME2term_ren[0][i_v][i_r] << ");" << endl;

	    osi->value_ME2term_ren[0][i_v][i_r] = osi->value_ME2term_ren[0][i_v][i_r] / osi->value_relative_factor_alpha_S[0][i_v][i_r];
    	    logger << LOG_DEBUG_POINT << "Recola:     ME2_VX_TSV[" << i_v << "][" << i_r << "] = " << setw(23) << setprecision(15) << osi->value_ME2term_ren[0][i_v][i_r] << "   (corrected for var_rel_alpha_S)" << endl;
	    osi->value_ME2term_ren[0][i_v][i_r] += osi->VA_I_ME2;
	  }
	}
      }
    }

    else if (csi->type_contribution == "L2VA" ||
	     osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){

    // Renormalization and regularization scales are set to Q here:
    double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
    // osi->var_mu_ren -> mu_Q
    set_mu_uv_rcl(mu_Q);
    logger << LOG_DEBUG_VERBOSE << "set_mu_uv_rcl(" << setprecision(15) << mu_Q << ");" << endl;

    set_mu_ir_rcl(mu_Q);
    logger << LOG_DEBUG_VERBOSE << "set_mu_ir_rcl(" << setprecision(15) << mu_Q << ");" << endl;

    double alpha_S_mu_Q = LHAPDF::alphasPDF(mu_Q);
    set_alphas_rcl(alpha_S_mu_Q, mu_Q, N_nondecoupled);
    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << mu_Q << ", " << N_nondecoupled << ");" << endl;

    compute_process_rcl(1, P_rec, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "NLO" << char(34) << ");" << endl;

    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "NLO", osi->VA_b_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_L2I = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
    osi->VA_b_ME2 = osi->VA_b_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, csi->contribution_order_alpha_s - 1);
    logger << LOG_DEBUG_POINT << "Recola:     ME2_L2I = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl;




      if (osi->switch_H1gg){
	osi->QT_A0 = 0.;
	osi->QT_A1 = 0.;
	osi->QT_H1_delta = 0.;
      }
      else {
	//	gsi->calculate_H1gg(osi);
	calculate_H1gg_2loop();
      }

      if (osi->QT_A0 == 0. && osi->QT_A1 == 0. && osi->QT_H1_delta == 0.){
	osi->VA_V_ME2 = 0.;
      }
      else {
	logger << LOG_DEBUG << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << endl;
	logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << endl;

	logger << LOG_DEBUG_VERBOSE << "born VVamp = " << setprecision(15) << setw(23) << osi->QT_A0 << " ,   "
	       << "1-loop VVamp = " << setprecision(15) << setw(23) << osi->QT_A1 << endl;
	logger << LOG_DEBUG_VERBOSE << "born OL    = " << setprecision(15) << setw(23) << osi->VA_b_ME2 << " ,   "
	       << "             = " << setprecision(15) << setw(23) << osi->VA_V_ME2 << endl;

	// reweighting 2-loop amplitude with mt-dependence (from osi->VA_b_ME2):
	osi->VA_V_ME2 = osi->QT_A1 * osi->VA_b_ME2 / osi->QT_A0;

	// no mt-dependence (from osi->VA_b_ME2) in 2-loop amplitude:
	//      osi->VA_V_ME2 = osi->QT_A1;

	// osi.QT_A1 is only the two-loop amplitude (times one-loop...),
	// without reweighting with any loop² result
      }

      // temporary solution because mu_reg does not exist as a standard parameter -> use mu_reg = mu_Q = s^ in I-operator !!!
      double save_osi_var_mu_ren = osi->var_mu_ren;
      osi->var_mu_ren = mu_Q;
      calculate_ME2_ioperator_VA_QCD();
      // check if correct !!!
      logger << LOG_DEBUG_POINT << "Recola:     ME2_L2II  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << endl;
      osi->VA_I_ME2 = osi->VA_I_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, csi->contribution_order_alpha_s - 1);

      logger << LOG_DEBUG_POINT << "Recola:     ME2_L2II  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

      osi->var_mu_ren = save_osi_var_mu_ren;

      // beta0 usually not initialized in CS subtraction !!!
      static double beta0 = (33. - 2 * osi->N_f) / 12;
      // scale variation (from mu_ren = s^ = mu_Q to the usual scales...) !!!
      osi->VA_X_ME2 = -csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->var_mu_ren, 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
      if (osi->switch_CV){
	for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	  osi->VA_X_ME2_CV[i_s] = -csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->var_mu_ren_CV[i_s], 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
	}
      }

      if (osi->switch_TSV){
	for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	  for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	    osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_V_ME2 + osi->VA_I_ME2 - csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->value_scale_ren[0][i_v][i_r], 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
	  }
	}
      }

      logger << LOG_DEBUG_VERBOSE
	     << "   V = " << setprecision(15) << setw(23) << osi->VA_V_ME2
	     << "   X = " << setprecision(15) << setw(23) << osi->VA_X_ME2
	     << "   I = " << setprecision(15) << setw(23) << osi->VA_I_ME2 << endl;

    }
  }
  else if (osi->switch_VI == 2){
    logger << LOG_DEBUG_VERBOSE << "OL: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
    //  osi->VA_I_ME2 = 0.;
    //  osi->VA_V_ME2 = 0.;
    //  osi->VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi->n_scales_CV; s++){osi->VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi->VA_I_ME2
    calculate_ME2_ioperator_VA_QCD();

    for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
        osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_I_ME2;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "SK: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
