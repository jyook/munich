#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_VT2_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_VT2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);

  // Renormalization and regularization scales are set to Q here:
  double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
  // osi->var_mu_ren -> mu_Q
  set_mu_uv_rcl(mu_Q);
  logger << LOG_DEBUG_VERBOSE << "set_mu_uv_rcl(" << setprecision(15) << mu_Q << ");" << endl;

  set_mu_ir_rcl(mu_Q);
  logger << LOG_DEBUG_VERBOSE << "set_mu_ir_rcl(" << setprecision(15) << mu_Q << ");" << endl;

  double alpha_S_mu_Q = LHAPDF::alphasPDF(mu_Q);
  set_alphas_rcl(alpha_S_mu_Q, mu_Q, N_nondecoupled);
  logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << mu_Q << ", " << N_nondecoupled << ");" << endl;

  compute_process_rcl(1, P_Recola, "NLO");
  logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_Recola, " << char(34) << "NLO" << char(34) << ");" << endl;

  osi->VA_V_ME2 = 0.;

  get_squared_amplitude_rcl(1, osi->csi->contribution_order_alpha_s - 2, "LO", osi->VA_b_ME2);
  logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << osi->csi->contribution_order_alpha_s - 2 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
  logger << LOG_DEBUG_POINT << "Recola:     ME2_B = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
  osi->VA_b_ME2 = osi->VA_b_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, osi->csi->contribution_order_alpha_s - 2);
  logger << LOG_DEBUG_POINT << "Recola:     ME2_B = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

  get_squared_amplitude_rcl(1, osi->csi->contribution_order_alpha_s - 1, "NLO", osi->VA_X_ME2);
  logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << osi->csi->contribution_order_alpha_s - 1 << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2 << ");" << endl;

  logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;
  osi->VA_X_ME2 = osi->VA_X_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, osi->csi->contribution_order_alpha_s - 1);
  logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

  osi->VA_V_ME2 = osi->VA_V_ME2 + osi->VA_X_ME2;
  // ??? One could simply use the sum as usually later...



  if (osi->switch_H2 || !osi->initial_diag){
    osi->QT_A0 = osi->VA_b_ME2;
    osi->QT_A1 = osi->VA_V_ME2;
    osi->QT_H1_delta = osi->QT_A1 / 2 / (osi->alpha_S * inv2pi * osi->QT_A0);
    osi->QT_A2 = 0.;
    osi->QT_H2_delta = 0.;
    //osi->QT_H2_delta = osi->QT_A2 / 4 / (pow(osi->alpha_S * inv2pi, 2) * osi->QT_A0);
  }
  else {
    calculate_H2_2loop();
  
    //  logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << ", " << osi->QT_A1 / M2L1[0] << endl;
    ///    logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << ", " << osi->QT_A1 / osi->VA_V_ME2 << endl;
    ///    cerr << "polylog   A2_1 = " << setw(15) << setprecision(8) << osi->QT_A2 / osi->QT_A1
    ///	 << "     s_part = " << setw(15) << setprecision(8) << esi->p_parton[0][0].m() << endl;
      /*
    cerr << "polylog   A2_1 = " << setw(15) << setprecision(8) << osi->QT_A2 / osi->QT_A1
	 << "     A1_0 = " << setw(15) << setprecision(8) << osi->QT_A1 / osi->QT_A0
	 << "     A20_11 = " << setw(15) << setprecision(8) << osi->QT_A2 * osi->QT_A0 / osi->QT_A1 / osi->QT_A1 << endl;
    cerr << "A2 = " << setw(23) << setprecision(15) << osi->QT_A2
	 << "     A1 = " << setw(23) << setprecision(15) << osi->QT_A1
	 << "     A0 = " << setw(23) << setprecision(15) << osi->QT_A0 << endl;
    cerr << "ratios (A0, A1) = " << setw(23) << setprecision(15) << osi->QT_A0 / osi->VA_b_ME2 << ", " << setw(23) << setprecision(15) << osi->QT_A1 / osi->VA_V_ME2 << endl;
      */
    // needed because of polylog issue in GiNaC (via VVamp) !!!
    /*
    if (abs(osi->QT_A2 / osi->QT_A1) > 5.){
      osi->QT_A2 = 0.;
      osi->QT_H2_delta = 0.;
      (osi->psi->i_nan)++;
    }
    */
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
