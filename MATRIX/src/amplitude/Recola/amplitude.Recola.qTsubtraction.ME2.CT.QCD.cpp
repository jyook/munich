#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_CT_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_CT_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  osi->ME2 = 0.;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);
  /*
   double P_rec[esi->p_parton[0].size()][4];
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[0][i].x0();
    P_rec[i - 1][1] = esi->p_parton[0][i].x1();
    P_rec[i - 1][2] = esi->p_parton[0][i].x2();
    P_rec[i - 1][3] = esi->p_parton[0][i].x3();
  }
  */

  if (!csi->class_contribution_loopinduced){
    compute_process_rcl(1, P_Recola, "LO");
    get_squared_amplitude_rcl(1,csi->contribution_order_alpha_s - 1, "LO", osi->ME2);
    osi->value_ME2term[0] = osi->ME2;
    if (!(osi->check_vanishing_ME2_end)){check_vanishing_ME2_born();}
    logger << LOG_DEBUG << "ME2_RCL = " << osi->ME2 << endl;
  }
  else {
    compute_process_rcl(1, P_Recola, "NLO");
    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "NLO", osi->ME2);
    osi->value_ME2term[0] = osi->ME2;
    logger << LOG_DEBUG << "ME2_RCL = " << osi->ME2 <<endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
