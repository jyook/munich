#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_ioperator_VA_MIX(){
  Logger logger("amplitude_Recola_set::calculate_ME2_ioperator_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi->VA_ioperator->size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      osi->VA_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
      osi->VA_I_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
    }
    initialization = 0;
  }

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);
  
  // Correct sub-amplitudes need to be selected (only copied from QCD and QEW versions so far...) !!!

  int flag_QCD = 0;
  int flag_QEW = 0;
  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
      if (flag_QCD == 0){
	if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 1){
	  //	  double dummy_b_ME2;
	  //	  ol_evaluate_cc((*(osi->VA_ioperator))[i_a][j_a].process_id, P, &dummy_b_ME2, M2cc, &ewcc);
	  if (osi->switch_VI == 2){
	    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
	    set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
	    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;
	    compute_process_rcl(1, P_Recola, "LO");
	    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_Recola, " << char(34) << "LO" << char(34) << ");" << endl;
	  }
	  compute_all_colour_correlations_rcl(1, P_Recola);
	  logger << LOG_DEBUG_VERBOSE << "compute_all_colour_correlations_rcl(1, P_Recola);" << endl;

	  flag_QCD = 1;
	}
      }
      if (flag_QEW == 0){
	if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 2){
	  ///	  ol_evaluate_tree((*(osi->VA_ioperator))[i_a][j_a].process_id, P, &osi->VA_b_ME2);
	  if (osi->switch_VI == 2){
	    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
	    set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
	    logger << LOG_DEBUG << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;
	    compute_process_rcl(1, P_Recola, "LO");
	    logger << LOG_DEBUG << "compute_process_rcl(1, P_Recola, " << char(34) << "LO" << char(34) << ");" << endl;
	    // needed ???
	    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", osi->VA_b_ME2);
	    logger << LOG_DEBUG << "osi->VA_b_ME2 = " << osi->VA_b_ME2 << endl;
	  }

	  flag_QEW = 1;
	}
      }
    }
  }


  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    if ((*(osi->VA_ioperator))[i_a][0].type_correction() == 1){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	///        osi->VA_ME2_cf[i_a][j_a] = M2cc[(*(osi->VA_ioperator))[i_a][j_a].no_BLHA_entry];
	get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, (*(osi->VA_ioperator))[i_a][j_a].no_emitter(), (*(osi->VA_ioperator))[i_a][j_a].no_spectator(), "NLO", osi->VA_ME2_cf[i_a][j_a]);
	logger << LOG_DEBUG_VERBOSE << "get_colour_correlation_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_emitter() << ", " << (*(osi->VA_ioperator))[i_a][j_a].no_spectator() << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << ");" << endl;
	logger << LOG_DEBUG_VERBOSE << "(*(osi->VA_ioperator))[" << i_a << "][" << j_a << "].colour_factor() = " <<  (*(osi->VA_ioperator))[i_a][j_a].colour_factor() << endl;
	osi->VA_ME2_cf[i_a][j_a] = osi->VA_ME2_cf[i_a][j_a] * (*(osi->VA_ioperator))[i_a][j_a].colour_factor();
	logger << LOG_DEBUG_POINT << "Recola:     VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
      }
    }
    else if ((*(osi->VA_ioperator))[i_a][0].type_correction() == 2){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	///        osi->VA_ME2_cf[i_a][j_a] = (*(osi->VA_ioperator))[i_a][j_a].charge_factor() * osi->VA_b_ME2;
	logger << LOG_DEBUG << "(*(osi->VA_ioperator))[" << i_a << "][" << j_a << "].charge_factor() = " << (*(osi->VA_ioperator))[i_a][j_a].charge_factor() << endl;
	osi->VA_ME2_cf[i_a][j_a] = (*(osi->VA_ioperator))[i_a][j_a].charge_factor() * osi->VA_b_ME2;
	logger << LOG_DEBUG << "osi->VA_ME2_cf[" << i_a << "][" << j_a << "] = " << osi->VA_ME2_cf [i_a][j_a] << endl;
      }
    }
  }

  if (osi->massive_QCD){osi->calculate_ioperator_QCD_CDST();}
  else {osi->calculate_ioperator_QCD_CS();}

  if (osi->massive_QEW){osi->calculate_ioperator_QEW_CDST();}
  else {osi->calculate_ioperator_QEW_CS();}

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi->VA_I_ME2_cf[i_a].begin(), osi->VA_I_ME2_cf[i_a].end(), 0.);
  }
  osi->VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::calculate_ME2_VA_MIX(){
  Logger logger("amplitude_Recola_set::calculate_ME2_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI == 0 || osi->switch_VI == 1){
  }
  else if (osi->switch_VI == 2){
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
