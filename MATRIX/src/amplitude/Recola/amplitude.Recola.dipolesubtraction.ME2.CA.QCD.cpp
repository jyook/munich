#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_CA_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_CA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);
  /*
  double P_rec[esi->p_parton[0].size()][4];
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[0][i].x0();
    P_rec[i - 1][1] = esi->p_parton[0][i].x1();
    P_rec[i - 1][2] = esi->p_parton[0][i].x2();
    P_rec[i - 1][3] = esi->p_parton[0][i].x3();
  }

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }
  */

  if ((csi->type_contribution == "CA" ||
       csi->type_contribution == "RCA") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){
    compute_process_rcl(1, P_Recola, "LO");
    compute_all_colour_correlations_rcl(1, P_Recola);
    for (int i_a = 0; i_a < (*osi->CA_collinear).size(); i_a++){
      for (int j_a = 0; j_a < (*osi->CA_collinear)[i_a].size(); j_a++){
	if (j_a == 0){
	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "LO", osi->CA_ME2_cf[i_a][j_a]);
	}
	else {
	  get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, (*osi->CA_collinear)[i_a][j_a].no_emitter(), (*osi->CA_collinear)[i_a][j_a].no_spectator(), osi->CA_ME2_cf[i_a][j_a]);
	  osi->CA_ME2_cf[i_a][j_a] = osi->CA_ME2_cf[i_a][j_a] * (*osi->CA_collinear)[i_a][j_a].colour_factor();
	}
	logger << LOG_DEBUG_POINT << "Recola:     CA_ME2[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf[i_a][j_a]<< endl;
      }
    }

  }
  else if (csi->type_contribution == "L2CA" ||
	   osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){
    compute_process_rcl(1, P_Recola, "NLO");
    compute_all_colour_correlations_rcl(1, P_Recola, "NLO");

    for (int i_a = 0; i_a < (*osi->CA_collinear).size(); i_a++){
      for (int j_a = 0; j_a < (*osi->CA_collinear)[i_a].size(); j_a++){
	if (j_a == 0){
	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "NLO", osi->CA_ME2_cf[i_a][j_a]);
	}
	else {
	  get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, (*osi->CA_collinear)[i_a][j_a].no_emitter(), (*osi->CA_collinear)[i_a][j_a].no_spectator(), "NLO", osi->CA_ME2_cf[i_a][j_a]);
	  osi->CA_ME2_cf[i_a][j_a] = osi->CA_ME2_cf[i_a][j_a] * (*osi->CA_collinear)[i_a][j_a].colour_factor();
	}
	logger << LOG_DEBUG_POINT << "Recola: (loop)    CA_ME2[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf[i_a][j_a] << endl;
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
