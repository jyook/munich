#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_ioperator_VA_QEW(){
  Logger logger("amplitude_Recola_set::calculate_ME2_ioperator_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi->VA_ioperator->size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      osi->VA_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
      osi->VA_I_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
    }
    initialization = 0;
  }

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);

  if (osi->switch_VI == 2){
    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
    set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
    logger << LOG_DEBUG << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;
    compute_process_rcl(1, P_Recola, "LO");
    logger << LOG_DEBUG << "compute_process_rcl(1, P_Recola, " << char(34) << "LO" << char(34) << ");" << endl;
    // needed ???
    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", osi->VA_b_ME2);
    logger << LOG_DEBUG << "osi->VA_b_ME2 = " << osi->VA_b_ME2 << endl;
  }




  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
      logger << LOG_DEBUG << "(*(osi->VA_ioperator))[" << i_a << "][" << j_a << "].charge_factor() = " << (*(osi->VA_ioperator))[i_a][j_a].charge_factor() << endl;
      osi->VA_ME2_cf[i_a][j_a] = (*(osi->VA_ioperator))[i_a][j_a].charge_factor() * osi->VA_b_ME2;
      logger << LOG_DEBUG << "osi->VA_ME2_cf[" << i_a << "][" << j_a << "] = " << osi->VA_ME2_cf [i_a][j_a] << endl;
    }
  }

  osi->calculate_ioperator_QEW();
  /*
  if (osi->massive_QEW){osi->calculate_ioperator_QEW_CDST();}
  else {osi->calculate_ioperator_QEW_CS();}
  */

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi->VA_I_ME2_cf[i_a].begin(), osi->VA_I_ME2_cf[i_a].end(), 0.);
    logger << LOG_DEBUG << "I_ME2_emitter[" << i_a << "] = " << I_ME2_emitter[i_a] << endl;

  }
  osi->VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);
  logger << LOG_DEBUG << "osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::calculate_ME2_VA_QEW(){
  Logger logger("amplitude_Recola_set::calculate_ME2_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI == 0 || osi->switch_VI == 1){
    double P_Recola[esi->p_parton[0].size()][4];
    set_phasespacepoint(P_Recola);
    if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){
      set_mu_uv_rcl(osi->var_mu_ren);
      set_mu_ir_rcl(osi->var_mu_ren);
    }
    else if (osi->user->double_value[osi->user->double_map["mu_reg"]] == -1.){
      set_mu_uv_rcl(osi->var_mu_ren);
      set_mu_ir_rcl(osi->var_mu_ren);
    }
    else {
      set_mu_uv_rcl(osi->user->double_value[osi->user->double_map["mu_reg"]]);
      set_mu_ir_rcl(osi->user->double_value[osi->user->double_map["mu_reg"]]);
    }

    set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, N_nondecoupled);
    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << osi->var_mu_ren << ", " << N_nondecoupled << ");" << endl;

    compute_process_rcl(1, P_Recola, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_Recola, " << char(34) << "NLO" << char(34) << ");" << endl;

    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", osi->VA_b_ME2);
    /////      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "LO", osi->VA_b_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
    /////      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
    //      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;
    osi->VA_b_ME2 = osi->VA_b_ME2 / osi->var_rel_alpha_S;
    /////      osi->VA_b_ME2 = osi->VA_b_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

    osi->VA_V_ME2 = 0.;
    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->VA_X_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2 << ");" << endl;
    //      logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
    osi->VA_X_ME2 = osi->VA_X_ME2 / osi->var_rel_alpha_S;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

    if (osi->switch_output_comparison){
      double V_ME2_D4 = 0.;
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-D4", V_ME2_D4);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-D4" << char(34) << ", " << setprecision(15) << V_ME2_D4 << ");" << endl;
      V_ME2_D4 = V_ME2_D4 / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_D4 = " << setw(23) << setprecision(15) << V_ME2_D4 << "   (corrected for var_rel_alpha_S)" << endl;

      double V_ME2_R2 = 0.;
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-R2", V_ME2_R2);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-R2" << char(34) << ", " << setprecision(15) << V_ME2_R2 << ");" << endl;
      V_ME2_R2 = V_ME2_R2 / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_R2 = " << setw(23) << setprecision(15) << V_ME2_R2 << "   (corrected for var_rel_alpha_S)" << endl;

      double V_ME2_CT = 0.;
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-CT", V_ME2_CT);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO-CT" << char(34) << ", " << setprecision(15) << V_ME2_CT << ");" << endl;
      V_ME2_CT = V_ME2_CT / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_CT = " << setw(23) << setprecision(15) << V_ME2_CT << "   (corrected for var_rel_alpha_S)" << endl;

      //	logger << LOG_DEBUG << "" << endl;

      // in order to match the OpenLoops output:

      osi->VA_X_ME2 = V_ME2_CT;
      osi->VA_V_ME2 = V_ME2_D4 + V_ME2_R2;

      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;
    }

    osi->VA_I_ME2 = 0.;
    if (osi->switch_VI == 0 || osi->switch_VI == 2){
      calculate_ME2_ioperator_VA_QEW();
      //      logger << LOG_DEBUG << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << endl;
      // don't know why for EW !!! comment out:
      //	osi->VA_I_ME2 = osi->VA_I_ME2 / osi->var_rel_alpha_S;
      /////      osi->VA_I_ME2 = osi->VA_I_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << "   (corrected for var_rel_alpha_S)" << endl;
    }


    if (osi->switch_CV){
      for (int s = 0; s < osi->n_scales_CV; s++){
	set_alphas_rcl(osi->var_alpha_S_CV[s], osi->var_mu_ren_CV[s], N_nondecoupled);
	logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_CV[s] << ", " << setprecision(15) << osi->var_mu_ren_CV[s] << ", " << N_nondecoupled << ");" << endl;

	rescale_process_rcl(1, "NLO");
	logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;

	/*
	  double temp_alphaS = osi->alpha_S * pow(osi->var_rel_alpha_S_CV[s], double(csi->contribution_order_alpha_s - 1) / csi->contribution_order_alpha_s);
	  set_alphas_rcl(temp_alphaS, osi->var_mu_ren_CV[s], N_nondecoupled);
	  compute_process_rcl(1, P_Recola, "NLO");
	  */
	  //	  set_alphas_rcl(osi->var_rel_alpha_S_CV[s], osi->var_mu_ren_CV[s], N_nondecoupled);
	  //	  compute_process_rcl(1, P_Recola, "NLO");

	osi->VA_X_ME2_CV[s] = 0.;
	//	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO-CT", osi->VA_X_ME2_CV[s]);

	get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->VA_X_ME2_CV[s]);
	logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2_CV[s] << ");" << endl;
	osi->VA_X_ME2_CV[s] = osi->VA_X_ME2_CV[s] / osi->var_rel_alpha_S_CV[s];
	logger << LOG_DEBUG_POINT << "Recola:     VA_X_ME2_CV[" << s << "] = " << setw(23) << setprecision(15) << osi->VA_X_ME2_CV[s] << "   (after removing alpha_S factor)" << endl;
      }
    }


    if (osi->switch_TSV){
      for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	  set_alphas_rcl(osi->value_alpha_S_TSV[0][i_v][i_r], osi->value_scale_ren[0][i_v][i_r], N_nondecoupled);
	  logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->value_alpha_S_TSV[0][i_v][i_r] << ", " << setprecision(15) << osi->value_scale_ren[0][i_v][i_r] << ", " << N_nondecoupled << ");" << endl;

	  rescale_process_rcl(1, "NLO");
	  logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;

	  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->value_ME2term_ren[0][i_v][i_r]);
	  logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->value_ME2term_ren[0][i_v][i_r] << ");" << endl;

	  osi->value_ME2term_ren[0][i_v][i_r] = osi->value_ME2term_ren[0][i_v][i_r] / osi->value_relative_factor_alpha_S[0][i_v][i_r];
	  logger << LOG_DEBUG_POINT << "Recola:     ME2_VX_TSV[" << i_v << "][" << i_r << "] = " << setw(23) << setprecision(15) << osi->value_ME2term_ren[0][i_v][i_r] << "   (corrected for var_rel_alpha_S)" << endl;
	  osi->value_ME2term_ren[0][i_v][i_r] += osi->VA_I_ME2;
	}
      }
    }
  }
  else if (osi->switch_VI == 2){
    logger << LOG_DEBUG_VERBOSE << "OL: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
    //  osi->VA_I_ME2 = 0.;
    //  osi->VA_V_ME2 = 0.;
    //  osi->VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi->n_scales_CV; s++){osi->VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi->VA_I_ME2
    calculate_ME2_ioperator_VA_QEW();
    for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
        osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_I_ME2;
      }
    }
    logger << LOG_DEBUG << "SK: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
  }


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
