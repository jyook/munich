#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_CT2_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_CT2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);

  if (!csi->class_contribution_loopinduced){
    // Renormalization and regularization scales are set to Q here:
    double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
    // osi->var_mu_ren -> mu_Q
    set_mu_uv_rcl(mu_Q);
    logger << LOG_DEBUG_VERBOSE << "set_mu_uv_rcl(" << setprecision(15) << mu_Q << ");" << endl;

    set_mu_ir_rcl(mu_Q);
    logger << LOG_DEBUG_VERBOSE << "set_mu_ir_rcl(" << setprecision(15) << mu_Q << ");" << endl;

    double alpha_S_mu_Q = LHAPDF::alphasPDF(mu_Q);
    set_alphas_rcl(alpha_S_mu_Q, mu_Q, N_nondecoupled);
    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi->var_alpha_S_reference << ", " << setprecision(15) << mu_Q << ", " << N_nondecoupled << ");" << endl;

    compute_process_rcl(1, P_Recola, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_Recola, " << char(34) << "NLO" << char(34) << ");" << endl;

    osi->VA_V_ME2 = 0.;

    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 2, "LO", osi->VA_b_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 2 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi->VA_b_ME2 << ");" << endl;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_B = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
    osi->VA_b_ME2 = osi->VA_b_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, csi->contribution_order_alpha_s - 2);
    logger << LOG_DEBUG_POINT << "Recola:     ME2_B = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl;

    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s - 1, "NLO", osi->VA_X_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << csi->contribution_order_alpha_s - 1 << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi->VA_X_ME2 << ");" << endl;

    logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;
    osi->VA_X_ME2 = osi->VA_X_ME2 * pow(osi->alpha_S / alpha_S_mu_Q, csi->contribution_order_alpha_s - 1);
    logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl;



    // Same as in OpenLoops implementation from here on:
    osi->QT_H1_delta = (osi->VA_V_ME2 + osi->VA_X_ME2) / osi->VA_b_ME2 / (osi->alpha_S / pi);
    // same translation for osi->QT_H1_delta as in ppllll24_calculate_H2.
    //  (alpha_S / pi) normalization of H1
    if (osi->switch_polenorm == 1){
      if (osi->initial_gg){osi->QT_H1_delta -= pi2_6 * C_A;}
      else if (osi->initial_qqx){osi->QT_H1_delta -= pi2_6 * C_F;}
      else {logger << LOG_FATAL << "Wrong process specified." << endl; exit(1);}
    }
  }
  else {
    logger << LOG_ERROR << "L2CT2 contribution has not been implemented yet." << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
