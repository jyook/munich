#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_RA_MIX(){
  Logger logger("amplitude_Recola_set::calculate_ME2_RA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (esi->cut_ps[0] > -1){
    double P_Recola[esi->p_parton[0].size()][4];
    set_phasespacepoint(P_Recola);
    /*
    double P_rec[esi->p_parton[0].size()][4];
    for (int i = 1; i < esi->p_parton[0].size(); i++){
      P_rec[i - 1][0] = esi->p_parton[0][i].x0();
      P_rec[i - 1][1] = esi->p_parton[0][i].x1();
      P_rec[i - 1][2] = esi->p_parton[0][i].x2();
      P_rec[i - 1][3] = esi->p_parton[0][i].x3();
    }

    for (int i = 1; i < esi->p_parton[0].size(); i++){
      stringstream temp_ss;
      for (int j = 0; j < 4; j++){
	temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
      }
      logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
    }
    */

    compute_process_rcl(1, P_Recola, "LO");
    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", osi->value_ME2term[0]);
    logger << LOG_DEBUG_POINT << "Recola:        R_ME2 = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;
  }
  else {osi->value_ME2term[0] = 0.;}

  for (int i_a = 1; i_a < osi->n_ps; i_a++){
    if (esi->cut_ps[i_a] >= 0){
      if      (csi->dipole[i_a].type_correction() == 1){
	if (csi->dipole[i_a].massive() == 0){
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
	else {
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	}
      }
      else if (csi->dipole[i_a].type_correction() == 2){
	if (csi->dipole[i_a].massive() == 0){
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
	else {
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
      }
    }
    else {osi->value_ME2term[i_a] = 0.;}
  }
  osi->RA_ME2 = osi->value_ME2term;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
