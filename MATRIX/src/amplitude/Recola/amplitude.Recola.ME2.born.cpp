#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_born(){
  Logger logger("amplitude_Recola_set::calculate_ME2_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  osi->ME2 = 0.;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);
  /*
    double P_rec[esi->p_parton[0].size()][4];
    for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[0][i].x0();
    P_rec[i - 1][1] = esi->p_parton[0][i].x1();
    P_rec[i - 1][2] = esi->p_parton[0][i].x2();
    P_rec[i - 1][3] = esi->p_parton[0][i].x3();
    }
  */

  if (!csi->class_contribution_loopinduced){
    //    compute_process_rcl(1, Pz_Recola, "LO");
    compute_process_rcl(1, P_Recola, "LO");
    get_squared_amplitude_rcl(1,csi->contribution_order_alpha_s, "LO", osi->ME2);
    osi->value_ME2term[0] = osi->ME2;
    if (!(osi->check_vanishing_ME2_end)){check_vanishing_ME2_born();}
    logger << LOG_DEBUG_VERBOSE << "ME2_RCL = " << osi->ME2 << endl;
  }
  else {
    compute_process_rcl(1, P_Recola, "NLO");
    get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->ME2);
    osi->value_ME2term[0] = osi->ME2;
    logger << LOG_DEBUG_VERBOSE << "ME2_RCL = " << osi->ME2 <<endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::check_vanishing_ME2_born(){
  Logger logger("amplitude_Recola_set::check_vanishing_ME2_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Not yet implemented/validated for RECOLA !!!
  if (osi->ME2 == 0.){osi->flag_vanishing_ME2 = 1;}
  else {
    osi->flag_vanishing_ME2 = 0;
    logger << LOG_DEBUG << "Before compute_all_colour_correlations_rcl" << endl;
    double P_Recola[esi->p_parton[0].size()][4];
    set_phasespacepoint(P_Recola);
    /*
      double P_rec[esi->p_parton[0].size()][4];
      for (int i = 1; i < esi->p_parton[0].size(); i++){
      P_rec[i - 1][0] = esi->p_parton[0][i].x0();
      P_rec[i - 1][1] = esi->p_parton[0][i].x1();
      P_rec[i - 1][2] = esi->p_parton[0][i].x2();
      P_rec[i - 1][3] = esi->p_parton[0][i].x3();
      }
    */
    compute_all_colour_correlations_rcl(1, P_Recola);
    //    compute_all_colour_correlations_rcl(1, **P_Recola);
    logger << LOG_DEBUG << "After compute_all_colour_correlations_rcl" << endl;

    logger << LOG_DEBUG_VERBOSE << "ME2 = " << osi->ME2 << endl;
    for (int i_1 = 1; i_1 < csi->n_particle + 3; i_1++){
      for (int i_2 = 1; i_2 < csi->n_particle + 3; i_2++){
	//	    for (int i_2 = i_1 + 1; i_2 < csi->n_particle + 3; i_2++){
	double A2cc;
	if (csi->type_contribution == "born" ||
	    csi->type_contribution == "RT" ||
	    csi->type_contribution == "RJ"){
	  get_colour_correlation_rcl(1, csi->contribution_order_alpha_s, i_1, i_2, A2cc);
	}
	else if (csi->type_contribution == "CT" ||
		 csi->type_contribution == "CJ"){
	  if (csi->type_correction == "QCD"){
	    get_colour_correlation_rcl(1, csi->contribution_order_alpha_s - 1, i_1, i_2, A2cc);
	  }
	  else if (csi->type_correction == "QEW"){
	    get_colour_correlation_rcl(1, csi->contribution_order_alpha_s, i_1, i_2, A2cc);
	  }
	  else {logger << LOG_ERROR << "csi->type_correction = " << csi->type_correction << "   is not defined." << endl;}
	}

	logger << LOG_DEBUG << "Recola:     CA_ME2[" << i_1 << "][" << i_2 << "] = " << A2cc << endl;
	if (abs(A2cc) > 1.e12 * abs(osi->ME2)){
	  logger << LOG_DEBUG << "b_ME2 = 0. due to numerical cancellations!" << endl;
	  osi->flag_vanishing_ME2 = 1;
	  break;
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
