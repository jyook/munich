#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_RA_QCD(){
  Logger logger("amplitude_Recola_set::calculate_ME2_RA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (esi->cut_ps[0] > -1){
    double P_Recola[esi->p_parton[0].size()][4];
    set_phasespacepoint(P_Recola);
    /*
    double P_rec[esi->p_parton[0].size()][4];
    for (int i = 1; i < esi->p_parton[0].size(); i++){
      P_rec[i - 1][0] = esi->p_parton[0][i].x0();
      P_rec[i - 1][1] = esi->p_parton[0][i].x1();
      P_rec[i - 1][2] = esi->p_parton[0][i].x2();
      P_rec[i - 1][3] = esi->p_parton[0][i].x3();
    }

    for (int i = 1; i < esi->p_parton[0].size(); i++){
      stringstream temp_ss;
      for (int j = 0; j < 4; j++){
	temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
      }
      logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
    }
    */

    if ((csi->type_contribution == "RA" ||
	 csi->type_contribution == "RRA") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){
      compute_process_rcl(1, P_Recola, "LO");
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", osi->value_ME2term[0]);
      logger << LOG_DEBUG_POINT << "Recola:     ME2_R  = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;

      if (!(osi->check_vanishing_ME2_end)){
	osi->flag_vanishing_ME2 = 0;
	compute_all_colour_correlations_rcl(1, P_Recola);
	for (int i_1 = 1; i_1 < csi->n_particle + 3; i_1++){
	  for (int i_2 = 1; i_2 < csi->n_particle + 3; i_2++){
	    //	    for (int i_2 = i_1 + 1; i_2 < csi->n_particle + 3; i_2++){
	    double A2cc;
	    get_colour_correlation_rcl(1, csi->contribution_order_alpha_s, i_1, i_2, A2cc);
	    logger << LOG_DEBUG << "Recola:     ccME2[" << i_1 << "][" << i_2 << "] = " << A2cc << endl;
	    if (abs(A2cc) > 1.e12 * abs(osi->value_ME2term[0])){
	      logger << LOG_DEBUG << "osi->value_ME2term[0] = 0. due to numerical cancellations!" << endl;
	      osi->flag_vanishing_ME2 = 1;
	      break;
	    }
	  }
	}
      }
    }
    else if (csi->type_contribution == "L2RA" ||
	       osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){
      compute_process_rcl(1, P_Recola, "NLO");
      get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "NLO", osi->value_ME2term[0]);
      logger << LOG_DEBUG_POINT << "Recola:     ME2_R  = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;

      // This is probably not helpful for loop-induced processes (unstable ME'2 might be set to 0):
      /*
      osi->flag_vanishing_ME2 = 0;
      if (osi->value_ME2term[0] == 0.){
	osi->flag_vanishing_ME2 = 1;
	logger << LOG_DEBUG << "b_ME2 = 0. due to numerical cancellations!" << endl;
      }
      */
    }
    else {
      logger << LOG_FATAL << "Should not happen!" << endl;
    }
  }
  else {osi->value_ME2term[0] = 0.;}

  for (int i_a = 1; i_a < osi->n_ps; i_a++){
    if (esi->cut_ps[i_a] >= 0){
      if (csi->dipole[i_a].massive() == 0){
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	else {cout << "Should not happen!" << endl;}
      }
      else {
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
      }
    }
    else {osi->value_ME2term[i_a] = 0.;}
  }
  osi->RA_ME2 = osi->value_ME2term;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_Recola_set::calculate_dipole_Acc_QCD(int x_a, double & Dfactor){
  static Logger logger("amplitude_Recola_set::calculate_dipole_Acc_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  Dfactor = 0.;

  double P_dipole_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint_dipole(P_dipole_Recola, x_a);
  /*
  double P_rec[esi->p_parton[x_a].size()][4];
  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[x_a][i].x0();
    P_rec[i - 1][1] = esi->p_parton[x_a][i].x1();
    P_rec[i - 1][2] = esi->p_parton[x_a][i].x2();
    P_rec[i - 1][3] = esi->p_parton[x_a][i].x3();
  }

  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }
  */

  if ((csi->type_contribution == "RA" ||
       csi->type_contribution == "RRA") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){
    compute_process_rcl(x_a + 1, P_dipole_Recola, "LO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << char(34) << "LO" << char(34) << ");" << endl;
    compute_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), Dfactor);
    logger << LOG_DEBUG_VERBOSE << "compute_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "Dfactor -> " << Dfactor << ");" << endl;
    Dfactor = Dfactor * csi->dipole[x_a].colour_factor();
    logger << LOG_DEBUG_POINT << "Recola:        Dfactor[" << x_a << "] = " << setw(23) << setprecision(15) << Dfactor << endl;
  }
  else if (csi->type_contribution == "L2RA" ||
	   osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){
    compute_process_rcl(x_a + 1, P_dipole_Recola, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << char(34) << "NLO" << char(34) << ");" << endl;
    compute_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), "NLO", Dfactor);
    logger << LOG_DEBUG_VERBOSE << "compute_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << char(34) << "NLO" << char(34) << "Dfactor -> " << Dfactor << ");" << endl;
    logger << LOG_DEBUG_POINT << "Recola:        Dfactor[" << x_a << "] = " << setw(23) << setprecision(15) << Dfactor << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void amplitude_Recola_set::calculate_dipole_Asc_QCD(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector){
  static Logger logger("amplitude_Recola_set::calculate_dipole_Asc_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double P_dipole_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint_dipole(P_dipole_Recola, x_a);
  /*
  double P_rec[esi->p_parton[x_a].size()][4];
  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[x_a][i].x0();
    P_rec[i - 1][1] = esi->p_parton[x_a][i].x1();
    P_rec[i - 1][2] = esi->p_parton[x_a][i].x2();
    P_rec[i - 1][3] = esi->p_parton[x_a][i].x3();
  }

  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }
  */

  double_complex MOM[4];
  MOM[0] = Vtensor.x0();
  MOM[1] = Vtensor.x1();
  MOM[2] = Vtensor.x2();
  MOM[3] = Vtensor.x3();
  stringstream temp_ss;
  for (int j = 0; j < 4; j++){
    temp_ss << setw(23) << setprecision(15) << MOM[j] << "   ";
  }

  if ((csi->type_contribution == "RA" ||
       csi->type_contribution == "RRA") && osi->user->string_value[osi->user->string_map["model"]] != "Bornloop"){
    compute_process_rcl(x_a + 1, P_dipole_Recola, "LO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << char(34) << "LO" << char(34) << ");" << endl;
    compute_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), ME2_metric);
    logger << LOG_DEBUG_VERBOSE << "compute_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "ME2_metric -> " << ME2_metric << ");" << endl;
    ME2_metric = ME2_metric * csi->dipole[x_a].colour_factor();
    logger << LOG_DEBUG_POINT << "Recola:        ME2_metric[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_metric << endl;

    compute_spin_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), MOM);
    logger << LOG_DEBUG_VERBOSE << "compute_spin_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "MOM);" << endl;
    get_spin_colour_correlation_rcl(x_a + 1, csi->contribution_order_alpha_s - 1, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), ME2_vector);
    logger << LOG_DEBUG_VERBOSE << "get_spin_colour_correlation_rcl(" << x_a + 1 << ", " << csi->contribution_order_alpha_s - 1 << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "ME2_vector -> " << ME2_vector << ");" << endl;
    ME2_vector = ME2_vector * csi->dipole[x_a].colour_factor() / (-Vtensor.m2());
    logger << LOG_DEBUG_POINT << "Recola:        ME2_vector[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_vector << endl;

  }
  else if (csi->type_contribution == "L2RA" ||
	   osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){
    compute_process_rcl(x_a + 1, P_dipole_Recola, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << char(34) << "NLO" << char(34) << ");" << endl;
    compute_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), "NLO", ME2_metric);
    logger << LOG_DEBUG_VERBOSE << "compute_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << char(34) << "NLO" << char(34) << ", " << "ME2_metric -> " << ME2_metric << ");" << endl;
    ME2_metric = ME2_metric * csi->dipole[x_a].colour_factor();
    logger << LOG_DEBUG_POINT << "Recola:        ME2_metric[" << x_a << "] = " << ME2_metric << endl;

    compute_spin_colour_correlation_rcl(x_a + 1, P_dipole_Recola, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), MOM, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_spin_colour_correlation_rcl(" << x_a + 1 << ", " << "P_dipole_Recola" << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "MOM" << ", " << char(34) << "NLO" << char(34) << ");" << endl;
    get_spin_colour_correlation_rcl(x_a + 1, csi->contribution_order_alpha_s - 1, csi->dipole[x_a].no_A_emitter(), csi->dipole[x_a].no_A_spectator(), "NLO", ME2_vector);
    logger << LOG_DEBUG_VERBOSE << "get_spin_colour_correlation_rcl(" << x_a + 1 << ", " << csi->contribution_order_alpha_s - 1 << ", " << csi->dipole[x_a].no_A_emitter() << ", " << csi->dipole[x_a].no_A_spectator() << ", " << "ME2_vector -> " << ME2_vector << ");" << endl;
    ME2_vector = ME2_vector * csi->dipole[x_a].colour_factor() / (-Vtensor.m2());
    logger << LOG_DEBUG_POINT << "Recola:        ME2_vector[" << x_a << "] = " << ME2_vector << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
