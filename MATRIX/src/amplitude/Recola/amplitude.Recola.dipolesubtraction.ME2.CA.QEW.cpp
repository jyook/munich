#include "header.hpp"
#ifdef RECOLA

void amplitude_Recola_set::calculate_ME2_CA_QEW(){
  Logger logger("amplitude_Recola_set::calculate_ME2_CA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double P_Recola[esi->p_parton[0].size()][4];
  set_phasespacepoint(P_Recola);
  /*
  double P_rec[esi->p_parton[0].size()][4];
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_rec[i - 1][0] = esi->p_parton[0][i].x0();
    P_rec[i - 1][1] = esi->p_parton[0][i].x1();
    P_rec[i - 1][2] = esi->p_parton[0][i].x2();
    P_rec[i - 1][3] = esi->p_parton[0][i].x3();
  }

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }
  */

  static double b_ME2;
  compute_process_rcl(1, P_Recola, "LO");
  get_squared_amplitude_rcl(1, csi->contribution_order_alpha_s, "LO", b_ME2);
  for (int i_a = 0; i_a < (*osi->CA_collinear).size(); i_a++){
    for (int j_a = 0; j_a < (*osi->CA_collinear)[i_a].size(); j_a++){
      // better shift charge_factor to pdfs !!! (could be wrong in sums over all (anti-)quarks otherwise) !!!
      osi->CA_ME2_cf[i_a][j_a] = (*osi->CA_collinear)[i_a][j_a].charge_factor() * b_ME2;
      osi->CA_ME2_cf_fi[i_a][j_a] = (*osi->CA_collinear)[i_a][j_a].charge_factor_fi() * b_ME2;
      logger << LOG_DEBUG_POINT << "Recola:     CA_ME2[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf[i_a][j_a] << "    CA_ME2_fi[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf_fi[i_a][j_a] << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
