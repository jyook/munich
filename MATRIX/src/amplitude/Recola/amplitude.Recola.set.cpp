#include "header.hpp"
#ifdef RECOLA

////////////////////
//  constructors  //
////////////////////
amplitude_Recola_set::amplitude_Recola_set(){
  Logger logger("amplitude_Recola_set::amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "amplitude_Recola_set::amplitude_Recola_set called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

amplitude_Recola_set::amplitude_Recola_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi){
  Logger logger("amplitude_Recola_set::amplitude_Recola_set (osi, csi, msi, esi)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  logger << LOG_INFO << "amplitude_Recola_set::amplitude_Recola_set (osi, csi, msi, esi) called..." << endl;
  // ???
  //this->amplitude_set::
  amplitude_set(_osi, _csi, _msi, _esi);
  logger << LOG_INFO << "after  calling  amplitude_set(_osi, _csi, _msi, _esi); ..." << endl;
  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void amplitude_Recola_set::set_phasespacepoint(){
  Logger logger("amplitude_Recola_set::set_phasespacepoint");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  /*
  //  double P_Recola[esi->p_parton[0].size()][4];
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_Recola[i - 1][0] = esi->p_parton[0][i].x0();
    P_Recola[i - 1][1] = esi->p_parton[0][i].x1();
    P_Recola[i - 1][2] = esi->p_parton[0][i].x2();
    P_Recola[i - 1][3] = esi->p_parton[0][i].x3();
  }

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    Px_Recola[4 * (i - 1) + 0] = esi->p_parton[0][i].x0();
    Px_Recola[4 * (i - 1) + 1] = esi->p_parton[0][i].x1();
    Px_Recola[4 * (i - 1) + 2] = esi->p_parton[0][i].x2();
    Px_Recola[4 * (i - 1) + 3] = esi->p_parton[0][i].x3();
  }
  */
  /*
  for (int i = 1; i < esi->p_parton[0].size(); i++){
    *Pz_Recola[i - 1][0] = esi->p_parton[0][i].x0();
    *Pz_Recola[i - 1][1] = esi->p_parton[0][i].x1();
    *Pz_Recola[i - 1][2] = esi->p_parton[0][i].x2();
    *Pz_Recola[i - 1][3] = esi->p_parton[0][i].x3();
    //    logger << LOG_INFO << "Pz_Recola[" << i - 1 << "][0] = " << endl;//Pz_Recola[i - 1][0] << endl;
  }
  *//*

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
    */
/*
void amplitude_Recola_set::set_phasespacepoint_dipole(const int x_a){
  Logger logger("amplitude_Recola_set::set_phasespacepoint_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  /*
  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    P_Recola_dipole[x_a][5 * (i - 1)]     = esi->p_parton[x_a][i].x0();
    P_Recola_dipole[x_a][5 * (i - 1) + 1] = esi->p_parton[x_a][i].x1();
    P_Recola_dipole[x_a][5 * (i - 1) + 2] = esi->p_parton[x_a][i].x2();
    P_Recola_dipole[x_a][5 * (i - 1) + 3] = esi->p_parton[x_a][i].x3();
    P_Recola_dipole[x_a][5 * (i - 1) + 4] = esi->p_parton[x_a][i].m();
  }
*//*
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
  */

void amplitude_Recola_set::set_phasespacepoint(double P_Recola [][4]){
  Logger logger("amplitude_Recola_set::set_phasespacepoint");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_Recola[i - 1][0] = esi->p_parton[0][i].x0();
    P_Recola[i - 1][1] = esi->p_parton[0][i].x1();
    P_Recola[i - 1][2] = esi->p_parton[0][i].x2();
    P_Recola[i - 1][3] = esi->p_parton[0][i].x3();
  }

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_Recola[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_Recola[" << i << "] = " << temp_ss.str() << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void amplitude_Recola_set::set_phasespacepoint_dipole(double P_dipole_Recola [][4], const int x_a){
  Logger logger("amplitude_Recola_set::set_phasespacepoint_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    P_dipole_Recola[i - 1][0] = esi->p_parton[x_a][i].x0();
    P_dipole_Recola[i - 1][1] = esi->p_parton[x_a][i].x1();
    P_dipole_Recola[i - 1][2] = esi->p_parton[x_a][i].x2();
    P_dipole_Recola[i - 1][3] = esi->p_parton[x_a][i].x3();
  }

  for (int i = 1; i < esi->p_parton[x_a].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_dipole_Recola[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_dipole_Recola[" << i << "] = " << temp_ss.str() << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void amplitude_Recola_set::set_phasespacepoint_OLformat(){
  Logger logger("amplitude_Recola_set::set_phasespacepoint_OLformat");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i = 1; i < esi->p_parton[0].size(); i++){
    P_OLformat[5 * (i - 1)]     = esi->p_parton[0][i].x0();
    P_OLformat[5 * (i - 1) + 1] = esi->p_parton[0][i].x1();
    P_OLformat[5 * (i - 1) + 2] = esi->p_parton[0][i].x2();
    P_OLformat[5 * (i - 1) + 3] = esi->p_parton[0][i].x3();
    P_OLformat[5 * (i - 1) + 4] = esi->p_parton[0][i].m();
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



#endif
