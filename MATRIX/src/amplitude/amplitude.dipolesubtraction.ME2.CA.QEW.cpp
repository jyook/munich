#include "header.hpp"

void amplitude_set::calculate_ME2check_CA_QEW(){
  Logger logger("amplitude_set::calculate_ME2check_CA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){
    esi->phasespacepoint();
    if (esi->p_parton[0][0].x0() == 0.){
      generate_testpoint();
      if (esi->p_parton[0][0].x0() != 0.){
	// Add z1 and z2 values for the testpoint
	esi->psi->z_coll[1] = pow(esi->psi->x_pdf[1], 0.4);
	esi->psi->z_coll[2] = pow(esi->psi->x_pdf[2], 0.3);
      }
    }
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){
      esi->cut_ps[0] = 0;
      set_testpoint_scale_CA();
      set_testpoint_scale_TSV(0);
    }
    else {
      // Probably not implemented like this ...
      // 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
      // 2 - Testpoint is calculated at output scale.
      osi->calculate_dynamic_scale(0);
      osi->calculate_dynamic_scale_TSV(0);
    }
    osi->determine_scale();
    osi->determine_scale_CA();
    if (esi->p_parton[0][0].x0() != 0.){
      calculate_ME2_CA_QEW();
      if (osi->massive_QEW){osi->calculate_collinear_QEW_CDST();}
      else {osi->calculate_collinear_QEW_CS();}
      osi->output_testpoint_CA();
    }
  }
  output_parameter("testpoint");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


