#include "header.hpp"

void amplitude_set::calculate_ME2check_VA_QEW(){
  Logger logger("amplitude_set::calculate_ME2check_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){

    esi->phasespacepoint();
    //    gsi->phasespacepoint_psp(esi);

    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}
    if (esi->p_parton[0][0].x0() != 0.){

      ofstream out_comparison;
      logger << LOG_DEBUG_VERBOSE << "osi->filename_comparison = " << osi->filename_comparison << endl;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);

      esi->perform_event_selection();
      //    esi->perform_event_selection(*osi->xmunich->gsi);

      if (esi->cut_ps[0] == -1){
	out_comparison << "Phase-space 0 is cut. -> Default scale is used." << endl;
	for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	    osi->value_scale_ren[0][sd][ss] = 100.;
	  }
	}
	for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	    osi->value_scale_fact[0][sd][ss] = 100.;
	  }
	}
	osi->var_mu_ren = 100.;
	osi->var_mu_fact = 100.;

	esi->cut_ps[0] = 0;
      }
      else {
	// Maybe not valid any longer:
	// 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
	// 2 - Testpoint is calculated at output scale.
	osi->calculate_dynamic_scale(0);
	//    gsi->calculate_dynamic_scale(0, esi, osi);
	osi->calculate_dynamic_scale_TSV(0);
	//    gsi->calculate_dynamic_scale_TSV(0, esi, osi);
	osi->determine_scale();
	/////  }
	logger << LOG_DEBUG_VERBOSE << "back" << endl;
      }


      osi->VA_delta_flag = 1;
      string s_Delta;
      osi->VA_b_ME2 = 0.;
      osi->VA_V_ME2 = 0.;
      osi->VA_X_ME2 = 0.;
      osi->VA_I_ME2 = 0.;
      osi->VA_X_ME2_CV.resize(osi->n_scales_CV, 0.);

      double coefficient_B = 0.;
      double coefficient_VF = 0.;
      double coefficient_V1 = 0.;
      double coefficient_V2 = 0.;

      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->var_alpha_S_reference);}
      /*
	if (osi->switch_output_comparison == 2){
	ol_setparameter_double(OL_alpha_s, osi->var_alpha_S_reference);
	}
      */

      double i_Delta;
      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      osi->VA_DeltaIR2 = 0.;

      logger << LOG_DEBUG << "osi->VA_DeltaUV  = " << osi->VA_DeltaUV << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR1 = " << osi->VA_DeltaIR1 << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR2 = " << osi->VA_DeltaIR2 << endl;

      ///    ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
      ///    if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
      ///    else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}
      //      logger << LOG_DEBUG_VERBOSE << "before switch_OL" << endl;

      // not yet treated correctly  (mu_ren treatment needs to be improved anyway) !!!

      double temp_mu_reg = 0.;
      ///      ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
      //      ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
      if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){
	///	ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
	temp_mu_reg = osi->var_mu_ren;
      }
      else {
	///	ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);
	temp_mu_reg = osi->user->double_value[osi->user->double_map["mu_reg"]];
      }
      /*
	ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);
      */

      set_mu_and_delta_VA();
      output_parameter("testpoint");
      //     OLP_PrintParameter(stch("log/olparameters." + csi->subprocess + ".txt"));

      calculate_ME2_VA_QEW();

      coefficient_B = osi->VA_b_ME2;
      coefficient_VF = osi->VA_V_ME2 + osi->VA_X_ME2;

      out_comparison << "Settings: " << endl << endl;
      out_comparison << setw(12) << "  mu_reg" << " = " << setprecision(15) << setw(23) << temp_mu_reg << "  ( = mu_ren by default)" << endl;
      //      out_comparison << setw(12) << "  mu_reg  = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  ( = mu_ren by default)" << endl;
      out_comparison << setw(12) << "  mu_ren" << " = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  " << "alpha_S(mu_ren) = " << osi->alpha_S << endl;
      out_comparison << setw(12) << "  mu_fact" << " = " << setprecision(15) << setw(23) << osi->var_mu_fact << endl;
      out_comparison << endl;

      out_comparison << "Absolute results: " << endl << endl;
      osi->output_testpoint_VA_result(out_comparison);
      double OL_I_ME2 = osi->VA_I_ME2;
      //      if (osi->switch_VI != 2){
      {
	osi->VA_I_ME2 = 0.;
	// Recola: !!! from QCD case: check if needed here as well !!!
	set_testpoint_alpha_S(osi->var_alpha_S_reference);
	//
	calculate_ME2_ioperator_VA_QEW();
	//      double SK_I_ME2 = osi->VA_I_ME2;
	// Recola: !!! from QCD case: check if needed here as well !!!
	//	set_testpoint_rescale_I_ME2();
	//
	osi->output_testpoint_VA_ioperator(out_comparison);
	osi->VA_I_ME2 = OL_I_ME2;
      }
      /* // not needed, thus output switched off
	 osi->VA_V_ME2 = osi->VA_V_ME2 / osi->VA_b_ME2;
	 osi->VA_X_ME2 = osi->VA_X_ME2 / osi->VA_b_ME2;
	 osi->VA_I_ME2 = osi->VA_I_ME2 / osi->VA_b_ME2;
	 //    osi->VA_b_ME2 = osi->VA_b_ME2;
	 out_comparison << "Relative results (corrections devided by ME2_born, ME2_born divided by coupling constants): " << endl << endl;
	 osi->output_testpoint_VA_result(out_comparison);
	 out_comparison << endl;
      */
      out_comparison << "Particle momenta: " << endl << endl;
      output_momenta(out_comparison, *osi);

      out_comparison << "Numerical check of (UV and IR) finiteness: " << endl << endl;

      if (allowed_Delta_IRneqUV){
	s_Delta = "Delta_UV";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaUV = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_QEW();
	  // use SK I-operator (needed for external photons by now) !!!
	  ///  calculate_ME2_ioperator_VA_QEW();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	osi->VA_DeltaUV = 0.;
	out_comparison << endl;

	s_Delta = "Delta_IR_1";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaIR1 = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_QEW();
	  // use SK I-operator (needed for external photons by now) !!!
	  ///  calculate_ME2_ioperator_VA_QEW();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	coefficient_V1 = osi->VA_V_ME2 + osi->VA_X_ME2 - coefficient_VF;
	osi->VA_DeltaIR1 = 0.;
	out_comparison << endl;
      }

      s_Delta = "Delta_IR_2";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
      	osi->VA_DeltaIR2 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_QEW();
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      coefficient_V2 = osi->VA_V_ME2 + osi->VA_X_ME2 - coefficient_VF;

      osi->VA_DeltaIR2 = 0.;
      out_comparison << endl;
      s_Delta = "Delta_UV = Delta_IR_1";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi->VA_DeltaUV = i_Delta;
	osi->VA_DeltaIR1 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_QEW();
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      if (allowed_Delta_IRneqUV){
	coefficient_V1 = osi->VA_V_ME2 + osi->VA_X_ME2 - coefficient_VF;
      }
      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      out_comparison << endl;
      set_mu_and_delta_VA();
      calculate_ME2_VA_QEW();
      // in order to reset Delta-dependent SK I-operator settings !!!
      ///  calculate_ME2_ioperator_VA_QEW();
      osi->VA_delta_flag = 0;

      out_comparison << endl;
      out_comparison << endl;
      out_comparison << "Coefficients:" << endl;
      out_comparison << endl;
      out_comparison << right << setw(5) << "B" << " = " << setprecision(15) << setw(23) << left << coefficient_B << endl;
      out_comparison << right << setw(5) << "VF" << " = " << setprecision(15) << setw(23) << left << coefficient_VF << endl;
      out_comparison << right << setw(5) << "V1" << " = " << setprecision(15) << setw(23) << left << coefficient_V1 << endl;
      out_comparison << right << setw(5) << "V2" << " = " << setprecision(15) << setw(23) << left << coefficient_V2 << endl;
      out_comparison << endl;
      out_comparison << endl;

      out_comparison << "#" << right
		     << setprecision(6) << setw(11) << "lam"
		     << setprecision(15) << setw(23) << "B"
		     << setprecision(15) << setw(23) << "VF"
		     << setprecision(15) << setw(23) << "V1"
		     << setprecision(15) << setw(23) << "V2"
		     << setprecision(15) << setw(23) << "VF/(B*alpha)"
		     << setprecision(15) << setw(23) << "V1/(B*alpha)"
		     << setprecision(15) << setw(23) << "V2/(B*alpha)"
		     << endl;

      // hard-coded alpha value ???
      out_comparison << right
		     << setprecision(7) << setw(12) << msi->alpha_e / 0.0075552541674291547
		     << setprecision(15) << setw(23) << coefficient_B
		     << setprecision(15) << setw(23) << coefficient_VF
		     << setprecision(15) << setw(23) << coefficient_V1
		     << setprecision(15) << setw(23) << coefficient_V2
		     << setprecision(15) << setw(23) << coefficient_VF / coefficient_B / msi->alpha_e
		     << setprecision(15) << setw(23) << coefficient_V1 / coefficient_B / msi->alpha_e
		     << setprecision(15) << setw(23) << coefficient_V2 / coefficient_B / msi->alpha_e
		     << endl;

      out_comparison.close();

      // Check if needed for QEW corrections
      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->alpha_S);}
    }
    else {
      output_parameter("testpoint");
    }

    osi->switch_output_comparison = 0;
  }
  else {
    output_parameter("testpoint");
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

