#include "header.hpp"

void amplitude_set::calculate_ME2check_born(){
  Logger logger("amplitude_set::calculate_ME2check_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){
    esi->phasespacepoint();
    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){
      esi->cut_ps[0] = 0;
    }
    else {
      // 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
      // 2 - Testpoint is calculated at output scale.
      osi->calculate_dynamic_scale(0);
      osi->calculate_dynamic_scale_TSV(0);
      osi->determine_scale();
    }
    logger << LOG_DEBUG << "After determine_scale:" << endl;
    logger << LOG_DEBUG << "scale_ren = " << osi->scale_ren << "   csi->contribution_order_alpha_s = " << csi->contribution_order_alpha_s << "   alpha_S = " << osi->alpha_S << endl;
    if (esi->p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);
      calculate_ME2_born();
      osi->output_testpoint_born(out_comparison);
      out_comparison << endl << "Particle momenta: " << endl << endl;
      osi->output_momenta(out_comparison);
      int i_a = 0;
      out_comparison << endl;
      for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	  out_comparison << "osi->value_scale_ren[" << i_a << "][" << sd << "][" << ss << "] = " << osi->value_scale_ren[i_a][sd][ss] << endl;
	}
      }
      for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	  out_comparison << "osi->value_scale_fact[" << i_a << "][" << sd << "][" << ss << "] = " << osi->value_scale_fact[i_a][sd][ss] << endl;
	}
      }
      out_comparison.close();
    }
  }
  output_parameter("testpoint");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


