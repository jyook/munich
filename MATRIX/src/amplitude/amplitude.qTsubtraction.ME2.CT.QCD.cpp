#include "header.hpp"

void amplitude_set::calculate_ME2check_CT_QCD(){
  static Logger logger("amplitude_set::calculate_ME2check_CT_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){
    esi->phasespacepoint();
    if (esi->p_parton[0][0].x0() == 0.){
      generate_testpoint();
      if (esi->p_parton[0][0].x0() != 0.){
	// Add z1 and z2 values for the testpoint
	esi->psi->z_coll[1] = pow(esi->psi->x_pdf[1], 0.4);
	esi->psi->z_coll[2] = pow(esi->psi->x_pdf[2], 0.3);
      }
    }
    esi->perform_event_selection();
    if (esi->cut_ps[0] == -1){
      esi->cut_ps[0] = 0;
    }
    else {
      // 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
      // 2 - Testpoint is calculated at output scale.
      osi->calculate_dynamic_scale(0);
      osi->calculate_dynamic_scale_TSV(0);
      osi->determine_scale();
    }
    // from CA contribution:
    osi->determine_scale();

    if (esi->p_parton[0][0].x0() != 0.){
      calculate_ME2_CT_QCD();

      osi->calculate_IS_CX();
      osi->psi->g_z_coll[0] = 1.;
      osi->psi->g_z_coll[1] = 1.;
      osi->psi->g_z_coll[2] = 1.;

      osi->psi->xbp_all = osi->psi->start_xbp_all;
      osi->psi->xbs_all = osi->psi->start_xbs_all;
      osi->psi->xbsqrts_all = osi->psi->start_xbsqrts_all;
      osi->psi->boost = (osi->psi->x_pdf[1] - osi->psi->x_pdf[2]) / (osi->psi->x_pdf[1] + osi->psi->x_pdf[2]);
      osi->psi->xbs_all[0][0] = esi->p_parton[0][0].m2();
      osi->psi->xbsqrts_all[0][0] = esi->p_parton[0][0].m();

      osi->calculate_pdf_LHAPDF_list_CV();
      if (osi->switch_TSV){osi->calculate_pdf_LHAPDF_list_TSV();}
      osi->determine_integrand_CX_ncollinear_CT();
      ofstream out_comparison;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);
      out_comparison << output_amplitude_CT_QCD();
      out_comparison << osi->output_ME2_CT_QCD();
      out_comparison << osi->output_momenta();
      out_comparison << osi->output_scales();
      out_comparison.close();
    }
  }
  output_parameter("testpoint");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#define Fcw left << setw(17)
#define FldXV setprecision(15) << setw(23) << left

string amplitude_set::output_amplitude_CT_QCD(){
  static Logger logger("amplitude_set::output_amplitude_CT_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  stringstream out_comparison;
  out_comparison << "Matrix elements:" << endl;
  out_comparison << endl;
  out_comparison << Fcw << "ME2_born" << " = " << FldXV << osi->ME2 << endl;
  out_comparison << endl;
  out_comparison << endl;

  logger << LOG_DEBUG_VERBOSE << "finished - return (string):" << endl << out_comparison.str() << endl;
  return out_comparison.str();
}
