#include "header.hpp"

void amplitude_set::calculate_ME2check_VA_MIX(){
  Logger logger("amplitude_set::calculate_ME2check_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Not yet updated to RECOLA and scale-dependent testpoint evaluation, etc.

  if (osi->switch_output_comparison){

    esi->phasespacepoint();
    //    gsi->phasespacepoint_psp(osi->esi);

    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}
    if (esi->p_parton[0][0].x0() != 0.){

      ofstream out_comparison;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);

      esi->perform_event_selection();
      //    esi->perform_event_selection(*osi->xmunich->gsi);

      if (esi->cut_ps[0] == -1){
	out_comparison << "Phase-space 0 is cut. -> Default scale is used." << endl;
	for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	    osi->value_scale_ren[0][sd][ss] = 100.;
	  }
	}
	for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	    osi->value_scale_fact[0][sd][ss] = 100.;
	  }
	}
	// imported from QEW case:
	osi->var_mu_ren = 100.;
	osi->var_mu_fact = 100.;

	esi->cut_ps[0] = 0;
      }
      else {
	// Maybe not valid any longer:
	// 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
	// 2 - Testpoint is calculated at output scale.
	osi->calculate_dynamic_scale(0);
	//    gsi->calculate_dynamic_scale(0, esi, osi);
	osi->calculate_dynamic_scale_TSV(0);
	//    gsi->calculate_dynamic_scale_TSV(0, esi, osi);
	osi->determine_scale();
	/////  }
      }

      osi->VA_delta_flag = 1;
      string s_Delta;
      osi->VA_b_ME2 = 0.;
      osi->VA_V_ME2 = 0.;
      osi->VA_X_ME2 = 0.;
      osi->VA_I_ME2 = 0.;
      osi->VA_X_ME2_CV.resize(osi->n_scales_CV, 0.);

      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->var_alpha_S_reference);}

      double i_Delta;
      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      osi->VA_DeltaIR2 = 0.;

      logger << LOG_DEBUG << "osi->VA_DeltaUV  = " << osi->VA_DeltaUV << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR1 = " << osi->VA_DeltaIR1 << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR2 = " << osi->VA_DeltaIR2 << endl;

      set_mu_and_delta_VA();
      output_parameter("testpoint");
      ///      ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
      ///    if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
      ///    else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}

      // temp_mu_reg not used !!!

      // not yet treated correctly  (mu_ren treatment needs to be improved anyway) !!!
      /*
     ///      double temp_mu_reg = 0.;
     ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
     //      ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
     if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){
     ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
     ///	temp_mu_reg = osi->var_mu_ren;
     }
     else {
     ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);
     ///	temp_mu_reg = osi->user->double_value[osi->user->double_map["mu_reg"]];
     }
      */
      /*
	ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);

	OLP_PrintParameter(stch("log/olparameters." + csi->subprocess + ".txt"));
      */

      calculate_ME2_VA_MIX();

      out_comparison << "Settings: " << endl << endl;
      out_comparison << setw(12) << "  mu_reg  = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  ( = mu_ren by default)" << endl;
      out_comparison << setw(12) << "  mu_ren  = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  " << "alpha_S(mu_ren) = " << osi->alpha_S << endl;
      out_comparison << setw(12) << "  mu_fact = " << setprecision(15) << setw(23) << osi->var_mu_fact << endl;
      out_comparison << endl;


      out_comparison << "Absolute results: " << endl << endl;
      osi->output_testpoint_VA_result(out_comparison);
      double OL_I_ME2 = osi->VA_I_ME2;
      if (osi->switch_VI != 2){
	osi->VA_I_ME2 = 0.;
	calculate_ME2_ioperator_VA_MIX();
	//      double SK_I_ME2 = osi->VA_I_ME2;
	osi->output_testpoint_VA_ioperator(out_comparison);
	osi->VA_I_ME2 = OL_I_ME2;
      }
      osi->VA_V_ME2 = osi->VA_V_ME2 / osi->VA_b_ME2;
      osi->VA_X_ME2 = osi->VA_X_ME2 / osi->VA_b_ME2;
      osi->VA_I_ME2 = osi->VA_I_ME2 / osi->VA_b_ME2;
      //    osi->VA_b_ME2 = osi->VA_b_ME2;
      out_comparison << "Relative results (corrections devided by ME2_born, ME2_born divided by coupling constants): " << endl << endl;
      osi->output_testpoint_VA_result(out_comparison);
      out_comparison << endl;
      out_comparison << "Particle momenta: " << endl << endl;
      output_momenta(out_comparison, *osi);
      ///    out_comparison << "On-shell-projected particle momenta: " << endl << endl;
      ///    output_momenta(out_comparison, *osi);
      out_comparison << "Numerical check of (UV and IR) finiteness: " << endl << endl;

      /*
      // Delta_UV =/= Delta_IR not allowed in CutTOols !!!
      int set_OL_stability_mode;
      ol_getparameter_int(stability_mode, &set_OL_stability_mode);
      int temp_OL_stability_mode = set_OL_stability_mode;
      int set_OL_redlib1;
      ol_getparameter_int(redlib1, &set_OL_redlib1);
      int temp_OL_redlib1 = set_OL_redlib1;
      int set_OL_redlib2;
      ol_getparameter_int(redlib2, &set_OL_redlib2);
      int temp_OL_redlib2 = set_OL_redlib2;
      //    char* set_OL_model;
      //    ol_getparameter_string("model", set_OL_model);
      string set_OL_model;
      for (int i_o = 0; i_o < osi->OL_parameter.size(); i_o++){
      if (osi->OL_parameter[i_o] == "model"){set_OL_model = osi->OL_value[i_o];}
      }
      string temp_OL_model = set_OL_model;

      logger << LOG_DEBUG << "set_OL_stability_mode  = " << set_OL_stability_mode << endl;
      logger << LOG_DEBUG << "set_OL_redlib1         = " << set_OL_redlib1 << endl;
      logger << LOG_DEBUG << "set_OL_redlib2         = " << set_OL_redlib2 << endl;
      logger << LOG_DEBUG << "set_OL_model           = " << set_OL_model << endl;
      */
      //      int temp_switch_check_IRneqUV = 1;
      /*
	if (set_OL_model == "heft"){temp_switch_check_IRneqUV = 0;}
	else if (set_OL_stability_mode < 20){temp_switch_check_IRneqUV = 0;}
	else {
	if (set_OL_stability_mode == 23){
	temp_OL_stability_mode = 21;
	if      (set_OL_redlib1 == 5 && set_OL_redlib2 == 1){temp_OL_redlib1 = 7;}
	else if (set_OL_redlib1 == 5 && set_OL_redlib2 == 7){temp_OL_redlib1 = 1;}
	else if (set_OL_redlib2 == 5 && set_OL_redlib1 == 1){temp_OL_redlib2 = 7;}
	else if (set_OL_redlib2 == 5 && set_OL_redlib1 == 7){temp_OL_redlib2 = 1;}

	ol_setparameter_int(stch(stability_mode), temp_OL_stability_mode);
	ol_setparameter_int(stch(redlib1), temp_OL_redlib1);
	ol_setparameter_int(stch(redlib2), temp_OL_redlib2);
	}
	logger << LOG_DEBUG << "temp_OL_stability_mode = " << temp_OL_stability_mode << endl;
	logger << LOG_DEBUG << "temp_OL_redlib1        = " << temp_OL_redlib1 << endl;
	logger << LOG_DEBUG << "temp_OL_redlib2        = " << temp_OL_redlib2 << endl;
	logger << LOG_DEBUG << "temp_OL_model          = " << temp_OL_model << endl;
	}
      */

      //      if (temp_switch_check_IRneqUV){
      if (allowed_Delta_IRneqUV){
	s_Delta = "Delta_UV";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaUV = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_MIX();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	osi->VA_DeltaUV = 0.;
	out_comparison << endl;

	s_Delta = "Delta_IR_1";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaIR1 = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_MIX();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	osi->VA_DeltaIR1 = 0.;
	out_comparison << endl;
      }

      s_Delta = "Delta_IR_2";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi->VA_DeltaIR2 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_MIX();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      osi->VA_DeltaIR2 = 0.;
      out_comparison << endl;
      s_Delta = "Delta_UV = Delta_IR_1";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi->VA_DeltaUV = i_Delta;
	osi->VA_DeltaIR1 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_MIX();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      out_comparison << endl;
      set_mu_and_delta_VA();
      /*
	ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
	if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
	else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}
      */
      calculate_ME2_VA_MIX();

      osi->VA_delta_flag = 0;
      out_comparison.close();

      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->alpha_S);}
    }
    else {
      output_parameter("testpoint");
    }

    osi->switch_output_comparison = 0;
  }
  else {
    output_parameter("testpoint");
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

