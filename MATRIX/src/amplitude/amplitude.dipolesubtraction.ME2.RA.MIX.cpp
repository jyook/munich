#include "header.hpp"

void amplitude_set::calculate_ME2check_RA_MIX(phasespace_set * psi){
  Logger logger("amplitude_set::calculate_ME2check_RA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){

    esi->phasespacepoint();
    //    gsi->phasespacepoint_psp(esi);
    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}

    if (esi->p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);
      for (int i_p = 0; i_p < esi->p_parton[0].size(); i_p++){psi->xbp_all[0][intpow(2, i_p - 1)] = esi->p_parton[0][i_p];}
      psi->determine_dipole_phasespace_RA();

      for (int i_a = 1; i_a < osi->n_ps; i_a++){
	for (int i_p = 0; i_p < esi->p_parton[i_a].size(); i_p++){
	  esi->p_parton[i_a][i_p] = psi->xbp_all[i_a][intpow(2, i_p - 1)];
	}
      }

      esi->perform_event_selection();
      //    esi->perform_event_selection(*gsi);
      for (int i_a = 0; i_a < osi->n_ps; i_a++){
	if (esi->cut_ps[i_a] == -1){
	  out_comparison << "Phase-space " << i_a << " is cut -> Scales are set to fixed value mu_ren = mu_fact = 100 GeV." << endl;
	  for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	    for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	      osi->value_scale_ren[i_a][sd][ss] = 100.;
	    }
	  }
	  for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	    for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	      osi->value_scale_fact[i_a][sd][ss] = 100.;
	    }
	  }
	  esi->cut_ps[i_a] = 0;
	}
	else {
	  osi->calculate_dynamic_scale_RA(i_a);
	  //    gsi->calculate_dynamic_scale_RA(i_a, esi, osi);
	  osi->calculate_dynamic_scale_TSV(i_a);
	  //    gsi->calculate_dynamic_scale_TSV(i_a, esi, osi);
	  osi->determine_scale_RA(i_a);
	}
      }

      calculate_ME2_RA_MIX();

      osi->output_testpoint_RA(out_comparison);

      out_comparison << "Corresponding phase-space points:" << endl << endl;
      for (int i_a = 0; i_a < osi->n_ps; i_a++){
	out_comparison << setw(12) << csi->dipole[i_a].name() << endl << endl;
	vector<vector<int> > xdx_pa = csi->dipole[i_a].dx_pa();
	osi->output_momenta_phasespace(out_comparison, i_a);
      }

      out_comparison.close();
    }
  }

  output_parameter("testpoint");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

