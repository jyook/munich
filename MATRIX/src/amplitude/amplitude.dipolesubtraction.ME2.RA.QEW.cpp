#include "header.hpp"

void amplitude_set::calculate_ME2check_RA_QEW(phasespace_set * psi){
  Logger logger("amplitude_set::calculate_ME2check_RA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){

    esi->phasespacepoint();
    //    gsi->phasespacepoint_psp(esi);
    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}

    if (esi->p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);
      for (int i_p = 0; i_p < esi->p_parton[0].size(); i_p++){psi->xbp_all[0][intpow(2, i_p - 1)] = esi->p_parton[0][i_p];}
      psi->determine_dipole_phasespace_RA();

      for (int i_a = 1; i_a < osi->n_ps; i_a++){
	for (int i_p = 0; i_p < esi->p_parton[i_a].size(); i_p++){
	  esi->p_parton[i_a][i_p] = psi->xbp_all[i_a][intpow(2, i_p - 1)];
	}
      }

      esi->perform_event_selection();
      //    esi->perform_event_selection(*gsi);
      for (int i_a = 0; i_a < osi->n_ps; i_a++){
	if (esi->cut_ps[i_a] == -1){
	  out_comparison << "Phase-space " << i_a << " is cut -> Scales are set to fixed value mu_ren = mu_fact = 100 GeV." << endl;
	  for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	    for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	      osi->value_scale_ren[i_a][sd][ss] = 100.;
	    }
	  }
	  for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	    for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	      osi->value_scale_fact[i_a][sd][ss] = 100.;
	    }
	  }
	  esi->cut_ps[i_a] = 0;
	}
	else {
	  osi->calculate_dynamic_scale_RA(i_a);
	  //    gsi->calculate_dynamic_scale_RA(i_a, esi, osi);
	  osi->calculate_dynamic_scale_TSV(i_a);
	  //    gsi->calculate_dynamic_scale_TSV(i_a, esi, osi);
	  osi->determine_scale_RA(i_a);
	}
      }

      calculate_ME2_RA_QEW();

      osi->output_testpoint_RA(out_comparison);

      out_comparison << "Corresponding phase-space points:" << endl << endl;
      for (int i_a = 0; i_a < osi->n_ps; i_a++){
	out_comparison << setw(12) << csi->dipole[i_a].name() << endl << endl;
	vector<vector<int> > xdx_pa = csi->dipole[i_a].dx_pa();
	osi->output_momenta_phasespace(out_comparison, i_a);
      }

      out_comparison.close();
    }
  }

  output_parameter("testpoint");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#define p_i esi->p_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define p_j esi->p_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define p_k esi->p_parton[0][csi->dipole[x_a].no_R_spectator()]
#define pt_ij esi->p_parton[x_a][csi->dipole[x_a].no_A_emitter()]
#define pt_k esi->p_parton[x_a][csi->dipole[x_a].no_A_spectator()]
double amplitude_set::calculate_dipole_QEW_A_ij_k(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ij_k");
  logger << LOG_DEBUG_VERBOSE << "called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double pi_pj = p_i * p_j;
  double pj_pk = p_j * p_k;
  double pi_pk = p_i * p_k;
  double y_ij_k = pi_pj / (pi_pj + pi_pk + pj_pk);
  double one_minus_y_ij_k = 1. - y_ij_k;
  double z_i = pi_pk / (pi_pk + pj_pk);
  double z_j = pj_pk / (pi_pk + pj_pk);
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = z_i * p_i - z_j * p_j;
    // emitter1 = charged (anti)particle, emitter2 = charged (anti)particle
    factor = -1. / (2 * pi_pj) * 8 * N_c * pi * msi->alpha_e;
    //  T_R / C_A -> N_c ???
    // adapt to paper version... Q_f^2 etc. !!!
    // formula seems ok.
    factor_vector = -2. / pi_pj;
    factor_metric = 1.;

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
  }
  else {
    // always csi->dipole[x_a].type_splitting() == 2; emitter1 = charged (anti)particle, emitter2 = photon
    double Dfactor;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    ME2 = -1. / (2. * pi_pj) * (8 * pi * msi->alpha_e) * (2. / (1. - z_i * one_minus_y_ij_k) - (1. + z_i)) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
    // C_F -> Q_f^2, cancelled against T_ij^2 -> Q_f^2 in the denominator
  }
  return -ME2;
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
#define m2_i osi->mass2_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define m2_j osi->mass2_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define m2_k osi->mass2_parton[0][csi->dipole[x_a].no_R_spectator()]
#define m2_ij osi->mass2_parton[x_a][csi->dipole[x_a].no_A_emitter()]
double amplitude_set::calculate_dipole_QEW_A_ij_k_massive(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ij_k_massive");
  logger << LOG_DEBUG_VERBOSE << "called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double pi_pj = p_i * p_j;
  double pj_pk = p_j * p_k;
  double pi_pk = p_i * p_k;
  double y_ij_k = pi_pj / (pi_pj + pi_pk + pj_pk);
  double one_minus_y_ij_k = 1. - y_ij_k;
  double z_i = pi_pk / (pi_pk + pj_pk);
  double z_j = pj_pk / (pi_pk + pj_pk);
  fourvector Q = pt_ij + pt_k;
  double Q2 = Q.m2();
  double mu2_i = m2_i / Q2;
  double mu2_j = m2_j / Q2;
  double mu2_k = m2_k / Q2;
  double mu2_ij = m2_ij / Q2;
  double one_minus_all_mu = 1. - mu2_i - mu2_j - mu2_k;
  double z_prefactor = (2 * mu2_i + one_minus_all_mu * y_ij_k) / (2 * (mu2_i + mu2_j + one_minus_all_mu * y_ij_k));
  double v_ij_k = sqrt(pow(2 * mu2_k + one_minus_all_mu * one_minus_y_ij_k, 2) - 4 * mu2_k) / (one_minus_all_mu * one_minus_y_ij_k);
  double v_ij_i = sqrt(pow(one_minus_all_mu * y_ij_k, 2) - 4 * mu2_i * mu2_j) / (one_minus_all_mu * y_ij_k + 2 * mu2_i);
  double vt_ij_k = sqrt(lambda(1, mu2_ij, mu2_k)) / (1. - mu2_ij - mu2_k);
  double z_minus = z_prefactor * (1 - v_ij_i * v_ij_k);
  double z_plus = z_prefactor * (1 + v_ij_i * v_ij_k);
  double z_i_m = z_i - .5 * (1. - v_ij_k);
  double z_j_m = z_j - .5 * (1. - v_ij_k);
  double kappa = 2. / 3.;  // must match the I-operator implementation (which uses kappa = 2. / 3.)
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = z_i_m * p_i - z_j_m * p_j;
    // emitter1 = charged particle, emitter2 = charged antiparticle
    factor = -1. / (2 * pi_pj + m2_i + m2_j - m2_ij) * 8 * T_R * pi * msi->alpha_e / v_ij_k;
    factor_vector = -4 / (2 * pi_pj + m2_i + m2_j);
    if (m2_i != m2_j){cout << "m2_i = " << m2_i << " =/= " << m2_j << " = m2_j" << endl; exit(1);}
    factor_metric = 1. - 2 * kappa * (z_plus * z_minus - m2_i / (2 * pi_pj + m2_i + m2_j));

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) / C_A * csi->dipole[x_a].symmetry_factor();
  }
  else {
    // always csi->dipole[x_a].type_splitting() == 2; emitter1/2 = charged (anti-)particle/photon
    double Dfactor = 0.;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    Dfactor = Dfactor / C_F * csi->dipole[x_a].symmetry_factor();

    // emitter1 = charged (anti-)particle, emitter2 = photon
    ME2 = -1. / (2 * pi_pj + m2_i + m2_j - m2_ij) * (8 * C_F * pi * msi->alpha_e) * (2. / (1. - z_i * one_minus_y_ij_k) - (vt_ij_k / v_ij_k) * (1. + z_i + m2_ij / pi_pj)) * Dfactor;
    // C_F -> Q_f^2, cancelled against T_ij^2 -> Q_f^2 in the denominator
    // has not yet been replaced here !!!
  }
  return -ME2 * csi->dipole[x_a].charge_factor();
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
#undef m2_i
#undef m2_j
#undef m2_k
#undef m2_ij

#undef p_i
#undef p_j
#undef p_k
#undef pt_ij
#undef pt_k

#define p_i esi->p_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define p_j esi->p_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define p_a esi->p_parton[0][csi->dipole[x_a].no_R_spectator()]
#define pt_ij esi->p_parton[x_a][csi->dipole[x_a].no_A_emitter()]
#define pt_a esi->p_parton[x_a][csi->dipole[x_a].no_A_spectator()]
double amplitude_set::calculate_dipole_QEW_A_ij_a(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ij_a");
  logger << LOG_DEBUG_VERBOSE << "called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double pi_pj = p_i * p_j;
  double pi_pa = p_i * p_a;
  double pj_pa = p_j * p_a;
  double one_minus_x_ij_a = pi_pj / (pi_pa + pj_pa);
  double x_ij_a = 1. - one_minus_x_ij_a;
  double z_i = pi_pa / (pi_pa + pj_pa);
  double z_j = pj_pa / (pi_pa + pj_pa);
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = z_i * p_i - z_j * p_j;
    // emitter1 = charged particle, emitter2 = charged antiparticle
    factor = -1. / (2 * pi_pj * x_ij_a) * 8 * N_c * pi * msi->alpha_e;
    //  T_R / C_A -> N_c ???
    // adapt to paper version... Q_f^2 etc. !!!
    factor_vector = -2. / pi_pj;
    factor_metric = 1.;

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
  }
  else {
    // always csi->dipole[x_a].type_splitting() == 2; emitter1/2 = charged (anti)particle/photon
    double Dfactor;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    ME2 = -1. / (2 * x_ij_a * pi_pj) * (8 * pi * msi->alpha_e) * (2. / (z_j + one_minus_x_ij_a) - (1. + z_i)) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
    // C_F -> Q_f^2, cancelled against Q_f^2 in the denominator
  }
  return -ME2;
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
#define m2_i osi->mass2_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define m2_j osi->mass2_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define m2_ij osi->mass2_parton[x_a][csi->dipole[x_a].no_A_emitter()]
double amplitude_set::calculate_dipole_QEW_A_ij_a_massive(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ij_a_massive");
  logger << LOG_DEBUG_VERBOSE << "A_ij_a_massive   called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double pi_pj = p_i * p_j;
  double pi_pa = p_i * p_a;
  double pj_pa = p_j * p_a;
  double one_minus_x_ij_a = (pi_pj - .5 * (m2_ij - m2_i - m2_j)) / (pi_pa + pj_pa);
  double x_ij_a = 1. - one_minus_x_ij_a;
  double z_i = pi_pa / (pi_pa + pj_pa);
  double z_j = pj_pa / (pi_pa + pj_pa);
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = z_i * p_i - z_j * p_j;
    // emitter1 = quark, emitter2 = antiquark
    factor = -1. / ((2 * pi_pj + m2_i + m2_j - m2_ij) * x_ij_a) * 8 * T_R * pi * msi->alpha_e;
    factor_vector = -4. / (2 * pi_pj + m2_i + m2_j);
    factor_metric = 1.;

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) / C_A * csi->dipole[x_a].symmetry_factor();
  }
  else {
    // always csi->dipole[x_a].type_splitting() == 2; emitter1/2 = charged (anti)particle/photon
    double Dfactor = 0.;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    Dfactor = Dfactor / C_F * csi->dipole[x_a].symmetry_factor();
    // emitter1 = charged (anti)particle, emitter2 = photon
    if (m2_i != m2_ij){exit(1);}
    ME2 = -1. / ((2 * pi_pj + m2_i + m2_j - m2_ij) * x_ij_a) * (8 * C_F * pi * msi->alpha_e) * (2. / (one_minus_x_ij_a + z_j) - 1. - z_i - m2_ij / pi_pj) * Dfactor;
    // C_F -> Q_f^2, cancelled against T_ij^2 -> Q_f^2 in the denominator
    // has not yet been replaced here !!!
  }
  return -ME2 * csi->dipole[x_a].charge_factor();
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
#undef m2_i
#undef m2_j
#undef m2_ij
#undef p_i
#undef p_j
#undef p_a
#undef pt_ij
#undef pt_a

#define p_a esi->p_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define p_i esi->p_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define p_k esi->p_parton[0][csi->dipole[x_a].no_R_spectator()]
double amplitude_set::calculate_dipole_QEW_A_ai_k(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ai_k");
  logger << LOG_DEBUG_VERBOSE << "called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double pi_pk = p_i * p_k;
  double pi_pa = p_i * p_a;
  double pk_pa = p_k * p_a;
  double one_minus_x_ik_a = pi_pk / (pi_pa + pk_pa);
  double x_ik_a = 1. - one_minus_x_ik_a;
  double u_i = pi_pa / (pi_pa + pk_pa);
  double one_minus_u_i = 1. - u_i;
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = p_i / u_i - p_k / one_minus_u_i;
    // emitter1 = charged (anti)particle, emitter2 = charged (anti)particle -> emitter12 = photon
    factor = -1. / (2 * pi_pa * x_ik_a) * 8 * pi * msi->alpha_e;
    //    factor = -1. / (2 * pi_pa * x_ik_a) * 8 * 0.5 * pi * msi->alpha_e;
    // C_F / C_A -> .5
    // adapt to paper !!! not used so far !!!
    factor_vector = (2 * one_minus_x_ik_a * u_i * one_minus_u_i) / (x_ik_a * pi_pk);
    factor_metric = x_ik_a;

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    //    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor() * 0.;
  }
  else {
    double Dfactor = 0.;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    if      (csi->dipole[x_a].type_splitting() == 2){
      // emitter1 = charged (anti)particle, emitter2 = photon -> emitter12 = charged (anti)particle
      // adapt to paper !!! not used so far !!!
      ME2 = -1. / (2 * x_ik_a * pi_pa) * (8 * pi * msi->alpha_e) * (2. / (one_minus_x_ik_a + u_i) - (1. + x_ik_a)) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
    }
    else if (csi->dipole[x_a].type_splitting() == 3){
      // emitter1 = photon, emitter2 = charged (anti)particle -> emitter12 = charged (anti)particle
      ME2 = -1. / (2 * x_ik_a * pi_pa) * (8 * N_c * pi * msi->alpha_e) * (1. - 2 * x_ik_a * one_minus_x_ik_a) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
      // T_R / C_F -> N_c
    // adapt to paper !!! not used so far !!!
    }
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return -ME2;
}
#undef p_a
#undef p_i
#undef p_k

#define p_a esi->p_parton[0][csi->dipole[x_a].no_R_emitter_1()]
#define p_i esi->p_parton[0][csi->dipole[x_a].no_R_emitter_2()]
#define p_b esi->p_parton[0][csi->dipole[x_a].no_R_spectator()]
double amplitude_set::calculate_dipole_QEW_A_ai_b(int x_a){
  static Logger logger("amplitude_set::calculate_dipole_QEW_A_ai_b");
  logger << LOG_DEBUG_VERBOSE << "called    with dipole no. " << x_a << endl;
  double ME2 = 0.;
  double x_i_ab = 1. - (p_i * (p_a + p_b)) / (p_a * p_b);
  // define some z_i analogously !!!
  if (csi->dipole[x_a].type_splitting() == 1){
    double factor = 0.;
    double ME2_metric = 0.;
    double ME2_vector = 0.;
    double factor_metric = 0.;
    double factor_vector = 0.;
    fourvector Vtensor = p_i - p_b * (p_i * p_a) / (p_b * p_a);
    // emitter1 = charged (anti)particle, emitter2 = charged (anti)particle -> emitter12 = photon
    factor = -1. / (2. * x_i_ab * (p_a * p_i)) * 8. * pi * msi->alpha_e;
    //    factor = -1. / (2. * x_i_ab * (p_a * p_i)) * 8. * 0.5 * pi * msi->alpha_e;
    // C_F / C_A -> .5
    // adapt to paper !!! not used so far !!!
    factor_vector = (2. * (1. - x_i_ab) * (p_a * p_b)) / (x_i_ab * (p_a * p_i) * (p_i * p_b)); // manually introduced factor 0.25!!! -> should be fixed now !!!
    factor_metric = x_i_ab;

    calculate_dipole_Asc_QEW(x_a, Vtensor, ME2_metric, ME2_vector);

    ME2 = factor * (factor_metric * ME2_metric - Vtensor.m2() * factor_vector * ME2_vector) * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
  }
  else {
    double Dfactor = 0.;

    calculate_dipole_Acc_QEW(x_a, Dfactor);

    if      (csi->dipole[x_a].type_splitting() == 2){
      // emitter1 = charged (anti)particle, emitter2 = photon -> emitter12 = charged (anti)particle
      ME2 = -1. / (2 * x_i_ab * (p_a * p_i)) * (8 * pi * msi->alpha_e) * (2. / (1. - x_i_ab) - (1. + x_i_ab)) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
    // adapt to paper !!! not used so far !!!
    }
    else if (csi->dipole[x_a].type_splitting() == 3){
      // emitter1 = photon, emitter2 = charged (anti)particle -> emitter12 = charged (anti)particle
      ME2 = -1. / (2 * x_i_ab * (p_a * p_i)) * (8 * N_c * pi * msi->alpha_e) * (1. - 2 * x_i_ab * (1. - x_i_ab)) * Dfactor * csi->dipole[x_a].symmetry_factor() * csi->dipole[x_a].charge_factor();
      // T_R / C_F -> N_c
    // adapt to paper !!! not used so far !!!
    }
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
  return -ME2;
}
#undef p_a
#undef p_i
#undef p_b
