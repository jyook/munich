#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_RA_MIX(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_RA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (esi->cut_ps[0] > -1){
    set_phasespacepoint();
    ol_evaluate_tree(csi->dipole[0].process_id, P_OpenLoops, &osi->value_ME2term[0]);
    logger << LOG_DEBUG_POINT << "OpenLoops:     R_ME2 = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;
  }
  else {osi->value_ME2term[0] = 0.;}

  for (int i_a = 1; i_a < osi->n_ps; i_a++){
    if (esi->cut_ps[i_a] >= 0){
      if      (csi->dipole[i_a].type_correction() == 1){
	if (csi->dipole[i_a].massive() == 0){
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
	else {
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	}
      }
      else if (csi->dipole[i_a].type_correction() == 2){
	if (csi->dipole[i_a].massive() == 0){
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
	else {
	  if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a_massive(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	  else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	  else {cout << "Should not happen!" << endl;}
	}
      }
    }
    else {osi->value_ME2term[i_a] = 0.;}
  }
  osi->RA_ME2 = osi->value_ME2term;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
