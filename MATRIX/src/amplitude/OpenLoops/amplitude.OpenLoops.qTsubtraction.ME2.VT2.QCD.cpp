#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_VT2_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_VT2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint();

  double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();

  ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
  ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
  ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);

  int polenorm_0 = 0;
  ol_setparameter_int(polenorm, polenorm_0);

  //  ol_setparameter_double(stch("renscale"), mu_Q);
  ol_setparameter_double(OL_mu_ren, mu_Q);
  ol_setparameter_double(OL_mu_reg, mu_Q);
  ol_setparameter_double(fact_uv, one);
  ol_setparameter_double(fact_ir, one);

  //  ol_evaluate_loop2(1, P_OpenLoops, M2L2, &acc);
  ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);

  // previous version !!! check if correct !!!
  //  osi->VA_V_ME2 = M2L1[0];

  ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2);
  osi->VA_V_ME2 = M2L1[0] + osi->VA_X_ME2;
  // ??? One could simply use the sum as usually later...



  if (osi->switch_H2 || !osi->initial_diag){
    osi->QT_A0 = osi->VA_b_ME2;
    osi->QT_A1 = osi->VA_V_ME2;
    osi->QT_H1_delta = osi->QT_A1 / 2 / (osi->alpha_S * inv2pi * osi->QT_A0);
    osi->QT_A2 = 0.;
    osi->QT_H2_delta = 0.;
    //osi->QT_H2_delta = osi->QT_A2 / 4 / (pow(osi->alpha_S * inv2pi, 2) * osi->QT_A0);
  }
  else {
    //    gsi->calculate_H2(osi);
    calculate_H2_2loop();

    //  logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << ", " << osi->QT_A1 / M2L1[0] << endl;
    ///    logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << ", " << osi->QT_A1 / osi->VA_V_ME2 << endl;
    ///    cerr << "polylog   A2_1 = " << setw(15) << setprecision(8) << osi->QT_A2 / osi->QT_A1
    ///	 << "     s_part = " << setw(15) << setprecision(8) << esi->p_parton[0][0].m() << endl;
      /*
    cerr << "polylog   A2_1 = " << setw(15) << setprecision(8) << osi->QT_A2 / osi->QT_A1
	 << "     A1_0 = " << setw(15) << setprecision(8) << osi->QT_A1 / osi->QT_A0
	 << "     A20_11 = " << setw(15) << setprecision(8) << osi->QT_A2 * osi->QT_A0 / osi->QT_A1 / osi->QT_A1 << endl;
    cerr << "A2 = " << setw(23) << setprecision(15) << osi->QT_A2
	 << "     A1 = " << setw(23) << setprecision(15) << osi->QT_A1
	 << "     A0 = " << setw(23) << setprecision(15) << osi->QT_A0 << endl;
    cerr << "ratios (A0, A1) = " << setw(23) << setprecision(15) << osi->QT_A0 / osi->VA_b_ME2 << ", " << setw(23) << setprecision(15) << osi->QT_A1 / osi->VA_V_ME2 << endl;
      */
    // needed because of polylog issue in GiNaC (via VVamp) !!!
    /*
    if (abs(osi->QT_A2 / osi->QT_A1) > 5.){
      osi->QT_A2 = 0.;
      osi->QT_H2_delta = 0.;
      (osi->psi->i_nan)++;
    }
    */
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
