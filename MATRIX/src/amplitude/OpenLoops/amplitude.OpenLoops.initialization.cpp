#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::initialization(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi){
  Logger logger("amplitude_OpenLoops_set::initialization (osi, csi, msi, esi, isi)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  amplitude_set::initialization(_osi, _csi, _msi, _esi, isi);
  ///  initialization_basic(_osi, _csi, _msi, _esi, isi);

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  //  amplitude_set::initialization(_osi, _csi, _msi, _esi);
  //  initialization_basic(_osi, _csi, _msi, _esi);

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  initialization(isi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::initialization(inputparameter_set * isi){
  Logger logger("amplitude_OpenLoops_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization_set_default();

  //  ol_start();
  //  output_parameter("default");


  initialization_input(isi);
  initialization_parameter();
  initialization_process();


  one = 1;

  n_momentum = 5 * (csi->n_particle + 2);
  // colour correlators are also used to check numerical zeros in tree amplitudes:
  n_cc_check = (csi->n_particle + 2) * (csi->n_particle + 1) / 2;
  M2cc_check = new double[n_cc_check];

  P_OpenLoops = new double[n_momentum];

  if (csi->class_contribution_CS_real){
    n_momentum_dipole = 5 * (csi->n_particle + 1);

    P_OL.resize(osi->n_ps);
    //    P_OL[0] = new double[n_momentum];
    for (size_t i_a = 1; i_a < osi->n_ps; i_a++){
      P_OL[i_a] = new double[n_momentum_dipole];
    }

    n_cc = (csi->n_particle + 1) * (csi->n_particle) / 2;
    M2cc = new double[n_cc];

    n_sc = (csi->n_particle + 1);
    M2sc = new double[n_sc];

    P_sc.resize(osi->n_ps);
    for (size_t i_a = 1; i_a < osi->n_ps; i_a++){
      P_sc[i_a] = new double[4];
    }
  }
  else {
    n_cc = (csi->n_particle + 2) * (csi->n_particle + 1) / 2;
    M2cc = new double[n_cc];

    if (csi->type_contribution == "CT2"){
      M2loopcc = new double[n_cc];
    }
  }


  if (csi->class_contribution_loopinduced){
    M2L2 = new double[5];
  }
  else if (csi->class_contribution_virtual){
    M2L1 = new double[3];
    IRL1 = new double[3];
    M2L2 = new double[5];
    IRL2 = new double[5];
  }
  else if (csi->type_contribution == "CT2"){
    M2L1 = new double[3];
    IRL1 = new double[3];
    M2L2 = new double[5];
    IRL2 = new double[5];
  }

  allowed_Delta_IRneqUV = 1;
  string set_OL_model = "";
  for (int i_o = 0; i_o < OL_parameter.size(); i_o++){
    if (OL_parameter[i_o] == "model"){set_OL_model = OL_value[i_o];}
  }
  if (set_OL_model == "heft"){allowed_Delta_IRneqUV = 0;}


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::initialization_set_default(){
  Logger logger("amplitude_OpenLoops_set::initialization_set_default");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  amplitude_set::initialization_set_default();
  //  N_quarks = 5;

  OL_ct_on = stch("ct_on");
  renscale = stch("renscale");
  fact_uv = stch("fact_uv");
  fact_ir = stch("fact_ir");
  OL_mu_ren = stch("muren");
  OL_mu_reg = stch("mureg");
  pole_uv = stch("pole_uv");
  pole_ir1 = stch("pole_ir1");
  pole_ir2 = stch("pole_ir2");
  polenorm = stch("polenorm");
  OL_alpha_s = stch("alpha_s");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::initialization_input(inputparameter_set * isi){
  Logger logger("amplitude_OpenLoops_set::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  amplitude_set::initialization_input(isi);


  // Set the OL parameters separately for each contribution !!!
  
  for (int i_i = 0; i_i < isi->input_amplitude_OpenLoops.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_amplitude_OpenLoops[" << i_i << "] = " << isi->input_amplitude_OpenLoops[i_i] << endl;

    if (isi->input_amplitude_OpenLoops[i_i].variable == ""){}

    else if (isi->input_amplitude_OpenLoops[i_i].variable == "OL"){
      if (isi->input_amplitude_OpenLoops[i_i].specifier != ""){
	OL_parameter.push_back(isi->input_amplitude_OpenLoops[i_i].specifier);
	OL_value.push_back(isi->input_amplitude_OpenLoops[i_i].value);
      }
    }
  }

  //    N_quarks = isi->N_quarks;
    /*
  OL_parameter = isi->OL_parameter;
  OL_value = isi->OL_value;
    */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::initialization_parameter(){
  Logger logger("amplitude_OpenLoops_set::initialization_parameter");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ////////////////////////////////////
  //  N_nondecoupled determination  //
  ////////////////////////////////////

  //  string LHAPDFversion = LHAPDF::version();
#ifdef LHAPDF5
  string LHAPDFversion = "5";
#else
  string LHAPDFversion = "6";
#endif

  logger << LOG_INFO << "LHAPDFversion = " << LHAPDFversion << endl;
  logger << LOG_INFO << "LHAPDFversion.substr(0, 1) = " << LHAPDFversion.substr(0, 1) << endl;

  int LHAPDF_N_nondecoupled = LHAPDF::getNf();

  if (LHAPDFversion.substr(0, 1) == "6"){
    logger << LOG_INFO << "LHAPDF version 6 (" << LHAPDFversion << ") is used." << endl;
    N_nondecoupled = LHAPDF_N_nondecoupled;
    if ((osi->LHAPDFname).substr(0, 10) == "NNPDF21_lo"){N_nondecoupled = 6;}
  }
  else if (LHAPDFversion.substr(0, 1) == "5"){
    logger << LOG_INFO << "LHAPDF version 5 (" << LHAPDFversion << ") is used." << endl;

    logger << LOG_INFO << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
    logger << LOG_INFO << "LHAPDF_N_nondecoupled = " << LHAPDF_N_nondecoupled << endl;

    if ((osi->LHAPDFname).substr(0, 7) == "NNPDF21"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 6;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF5"){N_nondecoupled = 5;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 7) == "NNPDF23"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 6;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF5"){N_nondecoupled = 5;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 6) == "NNPDF3"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 5;
      for (int i_s = (osi->LHAPDFname).size() - 4; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 4);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 4) = " << temp_Nf << endl;
	if (temp_Nf == "nf_3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf_4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf_6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 4) == "CT14"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 5;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 8) == "MMHT2014"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 5;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 9) == "PDF4LHC15"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 5;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if ((osi->LHAPDFname).substr(0, 6) == "LUXqed"){
      logger << LOG_DEBUG << "(osi->LHAPDFname) = " << (osi->LHAPDFname) << endl;
      N_nondecoupled = 5;
      for (int i_s = (osi->LHAPDFname).size() - 3; i_s >=0; i_s--){
	string temp_Nf = (osi->LHAPDFname).substr(i_s, 3);
	logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else {
      N_nondecoupled = LHAPDF_N_nondecoupled;
    }
  }

  if ((osi->LHAPDFname).substr(0, 8) == "MSTW2008" || (osi->LHAPDFname).substr(0, 8) == "MMHT2014"){
    for (int i_s = (osi->LHAPDFname).size() - 6; i_s >=0; i_s--){
      string temp_Nf = (osi->LHAPDFname).substr(i_s, 6);
      logger << LOG_DEBUG << "(osi->LHAPDFname).substr(" << i_s << ", 6) = " << temp_Nf << endl;
      if (temp_Nf == "nf4as5"){N_nondecoupled = 5;}
    }
  }

  if (N_nondecoupled != LHAPDF_N_nondecoupled){
    logger << LOG_WARN << "N_nondecoupled has been changed wrt. LHAPDF_N_nondecoupled in LHAPDF " << LHAPDFversion << endl;
  }

  logger << LOG_INFO << "N_nondecoupled = " << N_nondecoupled << endl;


  // generic version to replace mass/width initialization by particle name
  //  for (int i_p = 1; i_p < msi->M.size(); i_p++){
  for (int i_p = 1; i_p < 26; i_p++){
    if ((i_p > 6 && i_p < 11) || (i_p > 16 && i_p < 21) || i_p == 12 || i_p == 14 || i_p == 16 || i_p == 21 || i_p == 22){continue;}
    stringstream temp_mass_ss;
    temp_mass_ss << "mass(" << i_p << ")";
    //    string temp_mass_s = temp_mass_ss.str();
    //    ol_setparameter_double(stch(temp_mass_ss.str()), msi->M[i_p]);
    logger << LOG_DEBUG << "msi->M[" << i_p << "] = " << msi->M[i_p] << endl;
    munich_ol_setparameter_double(temp_mass_ss.str(), msi->M[i_p], logger);
  }

  //  ol_setparameter_double(stch("yuk(5)"), msi->M[5]);



  //  for (int i_p = 1; i_p < msi->Gamma.size(); i_p++){
  for (int i_p = 1; i_p < 26; i_p++){
    if ((i_p > 6 && i_p < 11) || (i_p > 16 && i_p < 21) || i_p == 12 || i_p == 14 || i_p == 16 || i_p == 21 || i_p == 22){continue;}
    stringstream temp_width_ss;
    temp_width_ss << "width(" << i_p << ")";
    //    string temp_width_s = temp_width_ss.str();
    //    ol_setparameter_double(stch(temp_width_ss.str()), msi->Gamma[i_p]);
    logger << LOG_DEBUG << "msi->Gamma[" << i_p << "] = " << msi->Gamma[i_p] << endl;
    munich_ol_setparameter_double(temp_width_ss.str(), msi->Gamma[i_p], logger);
  }

  //  ol_setparameter_int(stch("n_quarks"), N_quarks);
  munich_ol_setparameter_int("n_quarks", N_quarks, logger);
  /*
  int minnf_alphasrun = p_pdf->info().get_entry_as<int>("NumFlavors");
  logger << LOG_INFO << "manual:    N_nondecoupled = " << N_nondecoupled << endl;
  logger << LOG_INFO << "automatic: minnf_alphasrun = " << minnf_alphasrun << endl;
  assert(N_nondecoupled == minnf_alphasrun);
  */
  logger << LOG_INFO << "N_nondecoupled = " << N_nondecoupled << endl;
  //  ol_setparameter_int(stch("nq_nondecoupled"), N_nondecoupled); // should be calculated from chosen PDF set
  munich_ol_setparameter_int("nq_nondecoupled", N_nondecoupled, logger); // should be calculated from chosen PDF set

  logger << LOG_INFO << "osi->scale_ren = " << osi->scale_ren << endl;
  //  ol_setparameter_double(stch("renscale"), osi->scale_ren);
  munich_ol_setparameter_double("renscale", osi->scale_ren, logger);

  //  ol_setparameter_int(stch("polenorm"), osi->switch_polenorm);
  munich_ol_setparameter_int("polenorm", osi->switch_polenorm, logger);
  logger << LOG_INFO << "osi->switch_polenorm = " << osi->switch_polenorm << endl;
  //  ol_setparameter_int(stch("flavour_mapping"), 0);
  //  ol_setparameter_int(stch("ew_renorm_scheme"), 1);
  //  ol_setparameter_int(stch("verbose"), 2);
  munich_ol_setparameter_int("verbose", 2, logger);

  logger << LOG_INFO << "msi->ew_scheme = " << msi->ew_scheme << endl;
  //  ol_setparameter_int(stch("ew_scheme"), msi->ew_scheme);
  munich_ol_setparameter_int("ew_scheme", msi->ew_scheme, logger);

  /*
  logger << LOG_INFO << "msi->ew_renorm_scheme = " << msi->ew_scheme << " (per default equal to ew_scheme)" << endl;
  ol_setparameter_int(stch("ew_renorm_scheme"), msi->ew_scheme);
  */

  logger << LOG_INFO << "msi->use_cms = " << msi->use_cms << endl;
  //  ol_setparameter_int(stch("use_cms"), msi->use_cms);
  munich_ol_setparameter_int("use_cms", msi->use_cms, logger);

  logger << LOG_INFO << "Gmu = " << msi->G_F << endl;
  munich_ol_setparameter_double("Gmu", msi->G_F, logger);
  //  ol_setparameter_double(stch("Gmu"), msi->G_F);

  logger << LOG_INFO << "osi->alpha_S = " << osi->alpha_S << endl;
  //  ol_setparameter_double(stch("alpha_s"), osi->alpha_S);
  munich_ol_setparameter_double("alpha_s", osi->alpha_S, logger);


  // CKM matrix
  if (msi->CKM_matrix != "trivial"){
    //    ol_setparameter_int(stch("ckmorder"), 1);
    //    ol_setparameter_double(stch("VCKMdu"), msi->V_du);
    //    ol_setparameter_double(stch("VCKMsu"), msi->V_su);
    //    ol_setparameter_double(stch("VCKMbu"), msi->V_bu);
    //    ol_setparameter_double(stch("VCKMdc"), msi->V_dc);
    //    ol_setparameter_double(stch("VCKMsc"), msi->V_sc);
    //    ol_setparameter_double(stch("VCKMbc"), msi->V_bc);
    //    ol_setparameter_double(stch("VCKMdt"), msi->V_dt);
    //    ol_setparameter_double(stch("VCKMst"), msi->V_st);
    //    ol_setparameter_double(stch("VCKMbt"), msi->V_bt);
    munich_ol_setparameter_int("ckmorder", 1, logger);
    munich_ol_setparameter_double("VCKMdu", msi->V_du, logger);
    munich_ol_setparameter_double("VCKMsu", msi->V_su, logger);
    munich_ol_setparameter_double("VCKMbu", msi->V_bu, logger);
    munich_ol_setparameter_double("VCKMdc", msi->V_dc, logger);
    munich_ol_setparameter_double("VCKMsc", msi->V_sc, logger);
    munich_ol_setparameter_double("VCKMbc", msi->V_bc, logger);
    munich_ol_setparameter_double("VCKMdt", msi->V_dt, logger);
    munich_ol_setparameter_double("VCKMst", msi->V_st, logger);
    munich_ol_setparameter_double("VCKMbt", msi->V_bt, logger);
  }
  else {
    //    ol_setparameter_int(stch("ckmorder"), 0);
    munich_ol_setparameter_int("ckmorder", 0, logger);
  }


  int last_switch = 1;
  //  ol_setparameter_int(stch("last_switch"), last_switch);
  munich_ol_setparameter_int("last_switch", last_switch, logger);

  ///  int amp_switch = 1;
  ///  ol_setparameter_int(stch("redlib1"), amp_switch);
  ///  //  cout << "redlib1 is set to " << amp_switch << "." << endl;

  ///  int amp_switch_rescue = 7;
  ///  ol_setparameter_int(stch("redlib2"), amp_switch_rescue);
  ///  //  cout << "redlib2 is set to " << amp_switch_rescue << "." << endl;

  int use_coli_cache = 1;
  //  ol_setparameter_int(stch("use_coli_cache"), use_coli_cache);
  munich_ol_setparameter_int("use_coli_cache", use_coli_cache, logger);

  int out_symmetry = 1;
  //  ol_setparameter_int(stch("out_symmetry"), out_symmetry);
  munich_ol_setparameter_int("out_symmetry", out_symmetry, logger);

  int leading_colour = 0;
  //  ol_setparameter_int(stch("leading_colour"), leading_colour);
  munich_ol_setparameter_int("leading_colour", leading_colour, logger);

  int n_log = 1;
  //  int n_log = n_step;
  //  ol_setparameter_int(stch("stability_log"), n_log);
  munich_ol_setparameter_int("stability_log", n_log, logger);

  ///  int xtest;
  ///  ol_getparameter_int(stch("stability_mode"), &xtest);
  ///  logger << LOG_DEBUG << "default: stability_mode = " << xtest << endl;

  ///  int stability_mode = 23;
  ///  ol_setparameter_int(stch("stability_mode"), stability_mode);


  // ???
  double pole1_UV = 0.;
  //  ol_setparameter_double(stch("pole_uv"), pole1_UV);
  munich_ol_setparameter_double("pole_uv", pole1_UV, logger);

  double pole1_IR = 0.;
  //  ol_setparameter_double(stch("pole_ir1"), pole1_IR);
  munich_ol_setparameter_double("pole_ir1", pole1_IR, logger);

  double pole2_IR = 0.;
  //  ol_setparameter_double(stch("pole_ir2"), pole2_IR);
  munich_ol_setparameter_double("pole_ir2", pole2_IR, logger);


  int CT_on = 0; // modification of ...ME2_VA and OpenLoops calls needed !!!
  //  ol_setparameter_int(stch("ct_on"), CT_on); // modification of ...ME2_VA and OpenLoops calls needed !!!
  munich_ol_setparameter_int("ct_on", CT_on, logger); // modification of ...ME2_VA and OpenLoops calls needed !!!

  int R2_on = 1;
  //  ol_setparameter_int(stch("r2_on"), R2_on);
  munich_ol_setparameter_int("r2_on", R2_on, logger);

  ///  int IR_on = 2; // I-operator added to virtual amplitude
  int IR_on = 1;
  //  ol_setparameter_int(stch("ir_on"), IR_on);
  munich_ol_setparameter_int("ir_on", IR_on, logger);

  // ???
  double fact_uv = 1.;
  //  ol_setparameter_double(stch("fact_uv"), fact_uv);
  munich_ol_setparameter_double("fact_uv", fact_uv, logger);

  double fact_ir = 1.;
  //  ol_setparameter_double(stch("fact_ir"), fact_ir);
  munich_ol_setparameter_double("fact_ir", fact_ir, logger);


  int polecheck = 0;
  //  int polecheck = 2; // sets polecheck = 1 and IR_on = 2
  //  ol_setparameter_int(stch("polecheck"), polecheck);
  munich_ol_setparameter_int("polecheck", polecheck, logger);


  for (int i_ol = 0; i_ol < OL_parameter.size(); i_ol++){
    logger << LOG_DEBUG << "OL_parameter[" << i_ol << "] = " << OL_parameter[i_ol] << " = " << OL_value[i_ol] << endl;

    string temp_parameter = OL_parameter[i_ol];
    string temp_value = OL_value[i_ol];
    char * char_parameter = new char[temp_parameter.size() + 1];
    std::copy(temp_parameter.begin(), temp_parameter.end(), char_parameter);
    char_parameter[temp_parameter.size()] = '\0';
    char * char_value = new char[temp_value.size() + 1];
    std::copy(temp_value.begin(), temp_value.end(), char_value);
    char_value[temp_value.size()] = '\0';
    ol_setparameter_string(char_parameter, char_value);
    delete [] char_parameter;
    delete [] char_value;
    //    ol_setparameter_string(stch(OL_parameter[i_ol]), stch(OL_value[i_ol]));
  }

  if (msi->ew_scheme == -1){
    //    ol_setparameter_double(stch("alpha_QED"), msi->alpha_e_Gmu);
    munich_ol_setparameter_double("alpha_QED", msi->alpha_e_Gmu, logger);
  }
  if (msi->ew_scheme == 0){
    //    ol_setparameter_double(stch("alpha_QED_0"), msi->alpha_e_0);
    //    munich_ol_setparameter_double("alpha_QED_0", msi->alpha_e_0, logger);
  }
  if (msi->ew_scheme == 1){
    //    ol_setparameter_double(stch("alpha_QED"), msi->alpha_e_Gmu);
    munich_ol_setparameter_double("alpha_QED", msi->alpha_e_Gmu, logger);
  }
  if (msi->ew_scheme == 2){
    //    ol_setparameter_double(stch("alpha_QED_MZ"), msi->alpha_e_MZ);
    munich_ol_setparameter_double("alpha_QED_MZ", msi->alpha_e_MZ, logger);
  }
  // Set for all schemes because of possible external identified photons
  munich_ol_setparameter_double("alpha_QED_0", msi->alpha_e_0, logger);

  /*
  ol_setparameter_double(stch("alpha_QED_0"), msi->alpha_e_0);
  ol_setparameter_double(stch("alpha_QED"), msi->alpha_e_Gmu);
  ol_setparameter_double(stch("alpha_QED_MZ"), msi->alpha_e_MZ);
  */

  ol_start();

  double temp_alpha = 0.;
  //  ol_getparameter_double(stch("alpha_QED"), &temp_alpha);
  munich_ol_getparameter_double("alpha_QED", temp_alpha, logger);
  logger << LOG_INFO << "OpenLoops:   msi->alpha_e     = " << setw(23) << setprecision(15) << temp_alpha << "   1 / msi->alpha_e     = " << setw(23) << setprecision(15) << 1. / temp_alpha << endl;
  //  ol_getparameter_double(stch("alpha_QED_0"), &temp_alpha);
  munich_ol_getparameter_double("alpha_QED_0", temp_alpha, logger);
  logger << LOG_INFO << "OpenLoops:   msi->alpha_e_0   = " << setw(23) << setprecision(15) << temp_alpha << "   1 / msi->alpha_e_0   = " << setw(23) << setprecision(15) << 1. / temp_alpha << endl;
  //  ol_getparameter_double(stch("alpha_QED_MZ"), &temp_alpha);
  munich_ol_getparameter_double("alpha_QED_MZ", temp_alpha, logger);
  logger << LOG_INFO << "OpenLoops:   msi->alpha_e_MZ  = " << setw(23) << setprecision(15) << temp_alpha << "   1 / msi->alpha_e_MZ  = " << setw(23) << setprecision(15) << 1. / temp_alpha << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::initialization_process(){
  Logger logger("amplitude_OpenLoops_set::initialization_process");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG << "csi->type_perturbative_order         = " << csi->type_perturbative_order << endl;
  logger << LOG_DEBUG << "csi->type_contribution               = " << csi->type_contribution << endl;
  logger << LOG_DEBUG << "csi->type_correction                 = " << csi->type_correction << endl;
  logger << LOG_DEBUG << "csi->contribution_order_alpha_s      = " << csi->contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "csi->contribution_order_alpha_e      = " << csi->contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "csi->contribution_order_interference = " << csi->contribution_order_interference << endl;

  process_id = -1;
  //  int process_id = -1;
  if (csi->type_contribution == "born" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "RJ"){
    int type_amplitude = 1;
    if (osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){type_amplitude = 12;}
    //  select_OL_born_mode(QCD_order, QEW_order);
    logger << LOG_DEBUG << "csi->contribution_order_interference = " << csi->contribution_order_interference << endl;
    if (csi->contribution_order_interference == 0){
      //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
      munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
      logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;


      output_parameter("testpoint");


      process_id = register_OL_subprocess(0, type_amplitude);
      if (process_id == -1){
	//	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	process_id = register_OL_subprocess(0, type_amplitude);
      }
    }
    else if (csi->contribution_order_interference == 1){
      /*
	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, 11);
	logger << LOG_DEBUG << "process_id (QCD corr. to int)    = " << process_id << endl;
	// usual initialization for QCD processes, where QCD virtual amplitudes are available
	*/
      //      if (process_id == -1){
      //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
      munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
      logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
      process_id = register_OL_subprocess(0, 1);
      logger << LOG_DEBUG << "process_id (bare born)           = " << process_id << endl;

      //      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s + 1); // ????
      munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s + 1, logger); // ????
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s + 1 << " (only to check if ME2 == 0.)" << endl;
      process_id = register_OL_subprocess(0, 11);
      logger << LOG_DEBUG << "process_id (colour correlations) = " << process_id << endl;
      //      }
    }
    else {
      logger << LOG_FATAL << "to be solved " << endl;
      exit(1);
    }
    //    if (process_id == -1 && csi->contribution_order_interference == 1){
  }
  else if (csi->type_contribution == "loop" ||
	   csi->type_contribution == "L2I" ||
	   csi->type_contribution == "L2RT" ||
	   csi->type_contribution == "L2RJ" ||
	   csi->type_contribution == "L2VT" ||
	   csi->type_contribution == "L2VJ" ||
	   csi->type_contribution == "L2VA" ||
	   csi->type_contribution == "L2CT" ||
	   csi->type_contribution == "L2CJ"){
    logger << LOG_DEBUG << "csi->type_contribution = " << csi->type_contribution << endl;
    //  select_OL_born_mode(QCD_order, QEW_order);
    //  ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
    process_id = register_OL_subprocess(0, 12);
    logger << LOG_DEBUG << "process_id = " << process_id << endl;
  }
  else if (csi->type_contribution == "CA" ||
	   csi->type_contribution == "RCA" ||
	   csi->type_contribution == "RCJ"){
    int type_amplitude = 11;
    if (osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){type_amplitude = 12;}
    if (csi->type_correction == "QCD"){
      //   ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      if (csi->contribution_order_interference == 0){
	//	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
      }
      else if (csi->contribution_order_interference == 1){
	//	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	logger << LOG_DEBUG << "process_id (colour correlations) = " << process_id << endl;

	//	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, 1); // no colour correlations needed in EW corrections !!! adapt for loop-induced processes !!!
	logger << LOG_DEBUG << "process_id (bare born)           = " << process_id << endl;
	// if (process_id == 2) different libraries are needed for different Born contributions
      }
      else {
	logger << LOG_FATAL << "to be solved " << endl;
	exit(1);
      }
    }
    else if (csi->type_correction == "QEW"){
      //      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
      //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e - 1);
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
      process_id = register_OL_subprocess(0, 1); // temporary, to force the use of correct colour correlations !!!
    }
    else {logger << LOG_FATAL << "Wrong correction type: " << csi->type_contribution << " - " << csi->type_correction << " does not exist." << endl;}
    //    process_id = register_OL_subprocess(0, 1);
    //    process_id = register_OL_subprocess(0, 2);
    //    process_id = register_OL_subprocess(0, 11); // temporary, to force the use of correct colour correlations !!!
  }

  else if (csi->type_contribution == "L2CA"){
    int type_amplitude = 12;
    if (csi->type_correction == "QCD"){
      //   ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      if (csi->contribution_order_interference == 0){
	//	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
      }
      /*
      else if (csi->contribution_order_interference == 1){
	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	logger << LOG_DEBUG << "process_id (colour correlations) = " << process_id << endl;

	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // no colour correlations needed in EW corrections !!! adapt for loop-induced processes !!!
	logger << LOG_DEBUG << "process_id (bare born)           = " << process_id << endl;
	// if (process_id == 2) different libraries are needed for different Born contributions
      }
      */
      else {
	logger << LOG_FATAL << "to be solved " << endl;
	exit(1);
      }
    }
    /*
    else if (csi->type_correction == "QEW"){
      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
      process_id = register_OL_subprocess(0, 1); // temporary, to force the use of correct colour correlations !!!
    }
    */
    else {logger << LOG_FATAL << "Wrong correction type: " << csi->type_contribution << " - " << csi->type_correction << " does not exist." << endl;}
  }


  else if (csi->type_contribution == "VA" ||
	   csi->type_contribution == "RVA" ||
	   csi->type_contribution == "RVJ"){
    if (csi->type_correction == "QCD"){
      //   ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
      munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
      logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
      process_id = register_OL_subprocess(0, 11);
    }
    else if (csi->type_correction == "QEW"){
      /////      ol_setparameter_int(stch("ew_renorm"), 1); // needed ???
      //      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
      process_id = register_OL_subprocess(0, 11);
    }
    else if (csi->type_correction == "MIX"){
      /////      ol_setparameter_int(stch("ew_renorm"), 1); // needed ???
      //      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
      process_id = register_OL_subprocess(0, 11);
      if (process_id == -1){
	//	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, 11);
      }
      logger << LOG_DEBUG << "Virtual amplitude initialized.   process_id = " << process_id << endl;

      logger << LOG_DEBUG << "osi->VA_ioperator->size() = " << setw(15) << osi->VA_ioperator->size() << endl;

      for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
	logger << LOG_DEBUG << "osi->VA_ioperator[" << i_a << "].size() = " << setw(15) << (osi->VA_ioperator)[i_a].size() << endl;
	for (int j_a = 0; j_a < osi->VA_ioperator[i_a].size(); j_a++){
	  logger << LOG_DEBUG << "osi->VA_ioperator[" << i_a << "][" << j_a << "].name() = " << setw(15) << (*(osi->VA_ioperator))[i_a][j_a].name() << "   type_correction = " << (*(osi->VA_ioperator))[i_a][j_a].type_correction() << "   to be processed..." << endl;
	  if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 1){
	    //	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	    (*(osi->VA_ioperator))[i_a][j_a].process_id = register_OL_subprocess(0, 11);
	    if ((*(osi->VA_ioperator))[i_a][j_a].process_id == -1){
	      //	      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	      munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	      logger << LOG_DEBUG << "order_ew = " << csi->contribution_order_alpha_e << endl;
	      (*(osi->VA_ioperator))[i_a][j_a].process_id = register_OL_subprocess(0, 11);
	    }
	  }
	  if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 2){
	    //	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	    logger << LOG_DEBUG << "order_qcd = " << csi->contribution_order_alpha_s << endl;
	    (*(osi->VA_ioperator))[i_a][j_a].process_id = register_OL_subprocess(0, 1);
	  }

	  logger << LOG_DEBUG << "osi->VA_ioperator[" << i_a << "][" << j_a << "].name() = " << setw(15) << (*(osi->VA_ioperator))[i_a][j_a].name() << "   type_correction = " << (*(osi->VA_ioperator))[i_a][j_a].type_correction() << "   process_id = " << (*(osi->VA_ioperator))[i_a][j_a].process_id << endl;
	}
      }

      //      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      /*
	if (csi->contribution_order_interference == 0){
	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	}
	else if (csi->contribution_order_interference == 1){
	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	}
      */
    }
    else {logger << LOG_FATAL << "Wrong correction type: " << csi->type_contribution << " - " << csi->type_correction << " does not exist." << endl;}
    //    process_id = register_OL_subprocess(0, 11);
  }



  else if (csi->type_contribution == "RA" ||
	   csi->type_contribution == "RRA" ||
	   csi->type_contribution == "RRJ"){
    logger << LOG_DEBUG_VERBOSE << "(R)RA:   csi->dipole.size() = " << csi->dipole.size() << endl;
    int type_amplitude = 1;
    if (osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){type_amplitude = 12;}
    for (int i_a = 0; i_a < csi->dipole.size(); i_a++){
      logger << LOG_DEBUG_VERBOSE << "(R)RA:   i_a = " << i_a << endl;
      if (i_a == 0){
	//	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude);
	logger << LOG_DEBUG << "csi->dipole[0].process_id = " << csi->dipole[0].process_id << endl;
	logger << LOG_DEBUG << "type_amplitude = " << type_amplitude << endl;
	if (csi->dipole[i_a].process_id == -1){
	  //	  ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	  munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	  logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude);
	}
	/*
	  ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 1);
	*/
      }
      else {
	if (csi->type_correction == "QCD"){
	  type_amplitude = 11;
	  if (csi->contribution_order_interference == 1){
	    //	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	  else {
	    //	    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	}
	else if (csi->type_correction == "QEW"){
	  //	  ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	  munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	  logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, only Born needed here !!!
	}
	else if (csi->type_correction == "MIX"){
	  if (csi->dipole[i_a].type_correction() == 1){
	    type_amplitude = 11;
	    //	    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	    //	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 1); // temporary, to force the use of correct colour correlations !!!
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	  else if (csi->dipole[i_a].type_correction() == 2){
	    //	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, only Born needed here !!!
	  }
	  else {logger << LOG_FATAL << "Wrong correction type: " << csi->dipole[i_a].type_correction() << " does not exist." << endl;}
	  //	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 1);
	}
	//	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 11); // temporary, to force the use of correct colour correlations !!!
	//      select_OL_born_mode(QCD_order - 1, QEW_order);
      }
      //      csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 2);
      //      csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 1);
      logger << LOG_DEBUG << "dipole[" << i_a << "].process_id  = " << csi->dipole[i_a].process_id << endl;
      if (csi->dipole[i_a].process_id == -1){logger << LOG_FATAL << "Requested amplitudes are not available." << endl;}
    }
    logger.newLine(LOG_DEBUG);
  }


  else if (csi->type_contribution == "L2RA"){
    logger << LOG_DEBUG_VERBOSE << "(L2RA:   csi->dipole.size() = " << csi->dipole.size() << endl;
    int type_amplitude = 12;
    for (int i_a = 0; i_a < csi->dipole.size(); i_a++){
      logger << LOG_DEBUG_VERBOSE << "L2RA:   i_a = " << i_a << endl;
      if (i_a == 0){
	//	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude);
	logger << LOG_DEBUG << "csi->dipole[0].process_id = " << csi->dipole[0].process_id << endl;
	logger << LOG_DEBUG << "type_amplitude = " << type_amplitude << endl;
	if (csi->dipole[i_a].process_id == -1){
	  //	  ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	  munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	  logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude);
	}
      }
      else {
	if (csi->type_correction == "QCD"){
	  if (csi->contribution_order_interference == 1){
	    //	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    munich_ol_setparameter_int("order_qcd", csi->contribution_order_alpha_s, logger);
	    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	  else {
	    //	    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
	    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	}
	/*
	else if (csi->type_correction == "QEW"){
	  ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	  logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	  csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, only Born needed here !!!
	}
	else if (csi->type_correction == "MIX"){
	  if (csi->dipole[i_a].type_correction() == 1){
	    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	    //	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, 1); // temporary, to force the use of correct colour correlations !!!
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	  }
	  else if (csi->dipole[i_a].type_correction() == 2){
	    ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	    csi->dipole[i_a].process_id = register_OL_subprocess(i_a, type_amplitude); // temporary, only Born needed here !!!
	  }
	  else {logger << LOG_FATAL << "Wrong correction type: " << csi->dipole[i_a].type_correction() << " does not exist." << endl;}
	}
	*/
      }
      logger << LOG_DEBUG << "dipole[" << i_a << "].process_id  = " << csi->dipole[i_a].process_id << endl;
      if (csi->dipole[i_a].process_id == -1){logger << LOG_FATAL << "Requested amplitudes are not available." << endl;}
    }
    logger.newLine(LOG_DEBUG);
  }





  else if (csi->type_contribution == "CT" ||
	   csi->type_contribution == "CJ"){
    int type_amplitude = 1;
    if (osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){type_amplitude = 12;}
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
    process_id = register_OL_subprocess(0, type_amplitude);
  }
  else if (csi->type_contribution == "CT2" ||
	   csi->type_contribution == "CJ2"){
    //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
    process_id = register_OL_subprocess(0, 11);
  }
  else if (csi->type_contribution == "VT" ||
	   csi->type_contribution == "VJ"){
    int type_amplitude = 11;
    if (osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){type_amplitude = 12;}
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, type_amplitude);
  }
  else if (csi->type_contribution == "VT2" ||
	   csi->type_contribution == "VJ2"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
    process_id = register_OL_subprocess(0, 11);
  }

  else if (csi->type_contribution == "NLL_LO"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, 11);
  }
  else if (csi->type_contribution == "NLL_NLO"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, 11);
  }
  else if (csi->type_contribution == "NNLL_LO"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, 11);
  }
  else if (csi->type_contribution == "NNLL_NLO"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, 11);
  }
  else if (csi->type_contribution == "NNLL_NNLO"){
    //  ol_setparameter_int(stch("polenorm"), 1); // polenorm prescribed by H^(1) construction
    //    ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
    munich_ol_setparameter_int("order_ew", csi->contribution_order_alpha_e, logger);
    process_id = register_OL_subprocess(0, 11);
  }

  if (csi->type_contribution != "RA" &&
      csi->type_contribution != "RRA" &&
      csi->type_contribution != "RRJ" &&
      csi->type_contribution != "L2RA"){
    logger << LOG_DEBUG << "process_id = " << process_id << endl;
    if (process_id == -1){logger << LOG_FATAL << "Requested amplitudes are not available." << endl;}
  }

  ol_start();
  //  OLP_PrintParameter("olparameters.txt");
  //  logger << LOG_DEBUG << "1st testpoint" << endl;
  //  OpenLoops_testpoint_pptt_cc();
  //  logger << LOG_DEBUG << "2nd testpoint" << endl;
  //  OpenLoops_testpoint_pptt_cc();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// specific function for OpenLoops amplitudes
int amplitude_OpenLoops_set::register_OL_subprocess(int i_a, int amptype){
  Logger logger("amplitude_OpenLoops_set::register_OL_subprocess");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // modify photons depending on hadron_type_parton content:
  vector<int> this_type_parton_OL = csi->type_parton[i_a];
  // Select IS photons based on collider type (hadronic IS particles):
  for (int i_p = 1; i_p < 3; i_p++){
    if (this_type_parton_OL[i_p] == 22){
      if (abs(csi->type_hadron[i_p]) == 101){this_type_parton_OL[i_p] = -2002;}
      else if (abs(csi->type_hadron[i_p]) == 22){this_type_parton_OL[i_p] = 2002;}
      else {logger << LOG_ERROR << "Should not happen!" << endl; exit(1);}
    }
  }

  // Select identified FS photons:
  int temp_counter_photon_born = 0;
  for (int i_p = 3; i_p < this_type_parton_OL.size(); i_p++){
    if (this_type_parton_OL[i_p] == 22 && temp_counter_photon_born < csi->n_photon_born){
      temp_counter_photon_born++;
      this_type_parton_OL[i_p] = 2002;
    }
  }

  // Select un-identified FS photons (which can untergo gamma->ffx splittings)
  int temp_counter_jet_born = 0;
  for (int i_p = 3; i_p < this_type_parton_OL.size(); i_p++){
    if (abs(this_type_parton_OL[i_p]) < 7){
      // More precisely: Check partons in jets (top, bottom, etc.) !!!
      temp_counter_jet_born++;
    }
  }
  for (int i_p = 3; i_p < this_type_parton_OL.size(); i_p++){
    if (this_type_parton_OL[i_p] == 22 && temp_counter_jet_born < csi->n_jet_born){
      temp_counter_jet_born++;
      this_type_parton_OL[i_p] = -2002;
    }
  }

  // Remaining photons should be unresolved ones.


  stringstream temp_processname_ss;
  for (int i_p = 1; i_p < this_type_parton_OL.size(); i_p++){
    if (this_type_parton_OL[i_p] == 0){temp_processname_ss << 21;}
    // pdf convention:
    // W+ = +24 !!!
    // W- = -24 !!!
    else if (this_type_parton_OL[i_p] == 24){temp_processname_ss << -24;}
    else if (this_type_parton_OL[i_p] == -24){temp_processname_ss << 24;}

    /*
    /// only off-shell Photonen !!! needs to be controlled elsewhere...
    else if (this_type_parton_OL[i_p] == 22){temp_processname_ss << 22;}
    else if (this_type_parton_OL[i_p] == -22){temp_processname_ss << 22;}
    //    else if (this_type_parton_OL[i_p] == 22){temp_processname_ss << -22;}
    ///
    */
    else {temp_processname_ss << this_type_parton_OL[i_p];}
    if (i_p == 2){temp_processname_ss << " -> ";}
    else if (i_p == this_type_parton_OL.size() - 1){}
    else {temp_processname_ss << " ";}
  }
  string temp_processname = temp_processname_ss.str();

  char * char_processname = new char[temp_processname.size() + 1];
  std::copy(temp_processname.begin(), temp_processname.end(), char_processname);
  char_processname[temp_processname.size()] = '\0';

  logger << LOG_INFO << "before registration:   processname = " << setw(20) << char_processname << "   amptype = " << amptype << endl;

  int no_reg = ol_register_process(char_processname, amptype);

  logger << LOG_INFO << " after registration:   processname = " << setw(20) << char_processname << "   amptype = " << amptype << "   id = " << no_reg << endl;

  delete [] char_processname;

  if (no_reg == -1){
   stringstream temp_symm_processname_ss;
   vector<int> symm_type_parton = this_type_parton_OL;
    for (int i_p = 1; i_p < symm_type_parton.size(); i_p++){
      if   (symm_type_parton[i_p] == 1){symm_type_parton[i_p] = 3;}
      else if (symm_type_parton[i_p] == 2){symm_type_parton[i_p] = 4;}
      else if (symm_type_parton[i_p] == 3){symm_type_parton[i_p] = 1;}
      else if (symm_type_parton[i_p] == 4){symm_type_parton[i_p] = 2;}
      else if (symm_type_parton[i_p] == -1){symm_type_parton[i_p] = -3;}
      else if (symm_type_parton[i_p] == -2){symm_type_parton[i_p] = -4;}
      else if (symm_type_parton[i_p] == -3){symm_type_parton[i_p] = -1;}
      else if (symm_type_parton[i_p] == -4){symm_type_parton[i_p] = -2;}
    }
    for (int i_p = 1; i_p < symm_type_parton.size(); i_p++){
      if (symm_type_parton[i_p] == 0){temp_symm_processname_ss << 21;}
      // pdf convention:
      // W+ = +24 !!!
      // W- = -24 !!!
      else if (symm_type_parton[i_p] == 24){temp_symm_processname_ss << -24;}
      else if (symm_type_parton[i_p] == -24){temp_symm_processname_ss << 24;}

      // temporary !!! Needs to distinguish between ax (-22) and a (22) !!!
      else if (symm_type_parton[i_p] == 22){temp_symm_processname_ss << -22;}

      else {temp_symm_processname_ss << symm_type_parton[i_p];}
      if (i_p == 2){temp_symm_processname_ss << " -> ";}
      else if (i_p == symm_type_parton.size() - 1){}
      else {temp_symm_processname_ss << " ";}
    }
    string temp_symm_processname = temp_symm_processname_ss.str();
    char * char_symm_processname = new char[temp_symm_processname.size() + 1];
    std::copy(temp_symm_processname.begin(), temp_symm_processname.end(), char_symm_processname);
    char_symm_processname[temp_symm_processname.size()] = '\0';

    logger << LOG_DEBUG << "before registration:   symm_processname = " << setw(20) << char_symm_processname << "   amptype = " << amptype << endl;

    no_reg = ol_register_process(char_symm_processname, amptype);

    logger << LOG_DEBUG << " after registration:   symm_processname = " << setw(20) << char_symm_processname << "   amptype = " << amptype << "   id = " << no_reg << endl;

    delete [] char_symm_processname;
  }

  logger << LOG_DEBUG_VERBOSE << "finished  no_reg = " << no_reg << endl;
  return no_reg;
}


// specific function for OpenLoops amplitudes
void amplitude_OpenLoops_set::generate_testpoint(){
  Logger logger("amplitude_OpenLoops_set::generate_testpoint");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  logger << LOG_DEBUG_VERBOSE << "esi->psi->sqrtsmin_opt[0][4] = " << esi->psi->sqrtsmin_opt[0][4] << endl;
  //  logger << LOG_DEBUG_VERBOSE << "esi->psi->start_xbsqrts_all[0][" << esi->psi->xb_max - 4 << "] = " << esi->psi->start_xbsqrts_all[0][esi->psi->xb_max - 4]  << endl;
  double testpoint_E_CMS = osi->E_CMS;
  if (esi->psi->start_xbsqrts_all[0][esi->psi->xb_max - 4] != 0.){testpoint_E_CMS = esi->psi->start_xbsqrts_all[0][esi->psi->xb_max - 4];}
  else if (esi->psi->sqrtsmin_opt[0][4] < osi->E_CMS / 2){testpoint_E_CMS = osi->E_CMS / 2;}
  else {testpoint_E_CMS = (osi->E_CMS + esi->psi->sqrtsmin_opt[0][4]) / 2;}
  // fails sometimes at present when calling ol_phase_space_point !!!
  static int n_momentum = 5 * (csi->n_particle + 2);
  //  logger << LOG_DEBUG_VERBOSE << "n_momentum = " << n_momentum << endl;
  double *P;
  P = new double[n_momentum];
  //  logger << LOG_DEBUG_VERBOSE << "osi->E_CMS = " << osi->E_CMS << endl;
  ol_phase_space_point(1, testpoint_E_CMS, P);
  for (int i_p = 1; i_p < esi->p_parton[0].size(); i_p++){esi->p_parton[0][i_p] = fourvector(P[5 * (i_p - 1)], P[5 * (i_p - 1) + 1], P[5 * (i_p - 1) + 2], P[5 * (i_p - 1) + 3]);}
  //  logger << LOG_DEBUG_VERBOSE << "esi->p_parton[0][1].pT2() = " << esi->p_parton[0][1].pT2() << endl;
  //  logger << LOG_DEBUG_VERBOSE << "esi->p_parton[0][2].pT2() = " << esi->p_parton[0][2].pT2() << endl;
  // Rotate phase space point if particles 1 and 2 are not parallel to z axis:
  if (esi->p_parton[0][1].pT2() != 0. || esi->p_parton[0][2].pT2() != 0.){
    fourvector rotate_vector = esi->p_parton[0][1];
    for (int i_p = 1; i_p < esi->p_parton[0].size(); i_p++){esi->p_parton[0][i_p] = esi->p_parton[0][i_p].rotate(rotate_vector);}
    // Avoid numerical noise in incoming momenta:
    for (int i_p = 1; i_p < 3; i_p++){esi->p_parton[0][i_p] = fourvector(esi->p_parton[0][i_p].x0(), 0., 0., esi->p_parton[0][i_p].x3());}
  }
  esi->p_parton[0][0] = esi->p_parton[0][1] + esi->p_parton[0][2];
  esi->psi->x_pdf[0] = pow(testpoint_E_CMS / osi->E_CMS, 2);
  esi->psi->x_pdf[1] = pow(esi->psi->x_pdf[0], 0.6);
  esi->psi->x_pdf[2] = esi->psi->x_pdf[0] / esi->psi->x_pdf[1];
  esi->psi->boost = (esi->psi->x_pdf[1] - esi->psi->x_pdf[2]) / (esi->psi->x_pdf[1] + esi->psi->x_pdf[2]);
  delete [] P;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::output_parameter(string label){
  Logger logger("amplitude_OpenLoops_set::output_parameter");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (label == ""){
    OLP_PrintParameter(stch("log/olparameters." + csi->subprocess + ".txt"));
  }
  else {
    OLP_PrintParameter(stch("log/olparameters." + csi->subprocess + "." + label + ".txt"));
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
