#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_ioperator_VA_MIX(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_ioperator_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi->VA_ioperator->size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      osi->VA_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
      osi->VA_I_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
    }
    initialization = 0;
  }

  set_phasespacepoint();

  int flag_QCD = 0;
  int flag_QEW = 0;
  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
      if (flag_QCD == 0){
	if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 1){
	  double dummy_b_ME2;
	  ol_evaluate_cc((*(osi->VA_ioperator))[i_a][j_a].process_id, P_OpenLoops, &dummy_b_ME2, M2cc, &ewcc);
	  flag_QCD = 1;
	}
      }
      if (flag_QEW == 0){
	if ((*(osi->VA_ioperator))[i_a][j_a].type_correction() == 2){
	  ol_evaluate_tree((*(osi->VA_ioperator))[i_a][j_a].process_id, P_OpenLoops, &osi->VA_b_ME2);
	  flag_QEW = 1;
	}
      }
    }
  }

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    if ((*(osi->VA_ioperator))[i_a][0].type_correction() == 1){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
        osi->VA_ME2_cf[i_a][j_a] = M2cc[(*(osi->VA_ioperator))[i_a][j_a].no_BLHA_entry];
      }
    }
    else if ((*(osi->VA_ioperator))[i_a][0].type_correction() == 2){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
        osi->VA_ME2_cf[i_a][j_a] = (*(osi->VA_ioperator))[i_a][j_a].charge_factor() * osi->VA_b_ME2;
      }
    }
  }

  /*
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
    logger << LOG_DEBUG_VERBOSE << "osi->VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
    }
    }
  */


  if (osi->massive_QCD){osi->calculate_ioperator_QCD_CDST();}
  else {osi->calculate_ioperator_QCD_CS();}

  if (osi->massive_QEW){osi->calculate_ioperator_QEW_CDST();}
  else {osi->calculate_ioperator_QEW_CS();}

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi->VA_I_ME2_cf[i_a].begin(), osi->VA_I_ME2_cf[i_a].end(), 0.);
  }
  osi->VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::calculate_ME2_VA_MIX(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI == 0 || osi->switch_VI == 1){

    set_phasespacepoint();
    ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
    if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
    else if (osi->user->double_value[osi->user->double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
    else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}

    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);

    ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);

    osi->VA_V_ME2 = M2L1[0];
    if (osi->switch_VI == 0){
      if (osi->VA_DeltaIR1 == 0. && osi->VA_DeltaIR2 == 0.){osi->VA_I_ME2 = IRL1[0];}
      else {osi->VA_I_ME2 = IRL1[0] + osi->VA_DeltaIR1 * IRL1[1] + osi->VA_DeltaIR2 * IRL1[2];}
      //  osi->VA_I_ME2 = i_DeltaIR1 * IR1 + osi->VA_DeltaIR2 * IR2; // !!! I-operator switched off !!!
    }

    ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2);
    if (osi->switch_CV){
      for (int s = 0; s < osi->n_scales_CV; s++){
        double inv_factor_CV = osi->var_mu_ren_CV[s] / osi->var_mu_ren;
        ol_setparameter_double(OL_mu_ren, osi->var_mu_ren_CV[s]);
	if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren_CV[s]);}
	else if (osi->user->double_value[osi->user->double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
	else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}

        ol_setparameter_double(fact_uv, inv_factor_CV);
        ol_setparameter_double(fact_ir, inv_factor_CV);
	ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2_CV[s]);
      }
    }
    if (osi->switch_TSV){
      for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	  double inv_factor_TSV = osi->value_scale_ren[0][i_v][i_r] / osi->var_mu_ren;
	  ol_setparameter_double(OL_mu_ren, osi->value_scale_ren[0][i_v][i_r]);
	  if (osi->user->double_value[osi->user->double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi->value_scale_ren[0][i_v][i_r]);}
	  else if (osi->user->double_value[osi->user->double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);}
	  else {ol_setparameter_double(OL_mu_reg, osi->user->double_value[osi->user->double_map["mu_reg"]]);}
	  ol_setparameter_double(fact_uv, inv_factor_TSV);
	  ol_setparameter_double(fact_ir, inv_factor_TSV);
	  ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->value_ME2term_ren[0][i_v][i_r]);
	  osi->value_ME2term_ren[0][i_v][i_r] += osi->VA_V_ME2 + osi->VA_I_ME2;
	}
      }
    }
  }
  else if (osi->switch_VI == 2){
    logger << LOG_DEBUG_VERBOSE << "OL: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
    //  osi->VA_I_ME2 = 0.;
    //  osi->VA_V_ME2 = 0.;
    //  osi->VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi->n_scales_CV; s++){osi->VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi->VA_I_ME2
    calculate_ME2_ioperator_VA_MIX();
    for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
        osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_I_ME2;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "SK: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
