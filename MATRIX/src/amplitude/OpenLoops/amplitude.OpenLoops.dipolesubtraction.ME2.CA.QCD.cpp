#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_CA_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_CA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint();
  if (!csi->class_contribution_loopinduced){
    ol_evaluate_cc(1, P_OpenLoops, &M2L0, M2cc, &ewcc);
  }
  else {
    ol_evaluate_cc2(1, P_OpenLoops, &M2L0, M2cc, &ewcc);
  }

  for (int i_a = 0; i_a < (*osi->CA_collinear).size(); i_a++){
    for (int j_a = 0; j_a < (*osi->CA_collinear)[i_a].size(); j_a++){
      if (j_a == 0){osi->CA_ME2_cf[i_a][j_a] = M2L0;}
      else {osi->CA_ME2_cf[i_a][j_a] = M2cc[(*osi->CA_collinear)[i_a][j_a].no_BLHA_entry];}
      logger << LOG_DEBUG_POINT << "OpenLoops:  CA_ME2[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf[i_a][j_a] << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
