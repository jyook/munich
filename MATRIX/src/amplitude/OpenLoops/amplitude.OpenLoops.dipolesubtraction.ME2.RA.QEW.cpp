#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_RA_QEW(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_RA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (esi->cut_ps[0] > -1){
    set_phasespacepoint();
    ol_evaluate_tree(csi->dipole[0].process_id, P_OpenLoops, &osi->value_ME2term[0]);
    logger << LOG_DEBUG_POINT << "OpenLoops:     R_ME2 = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;
  }
  else {osi->value_ME2term[0] = 0.;}

  for (int i_a = 1; i_a < osi->n_ps; i_a++){
    if (esi->cut_ps[i_a] >= 0){
      if (csi->dipole[i_a].massive() == 0){
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	else {cout << "Should not happen!" << endl;}
      }
      else {
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_k_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ij_a_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QEW_A_ai_b(i_a);}
	else {cout << "Should not happen!" << endl;}
      }
    }
    else {osi->value_ME2term[i_a] = 0.;}
  }
  osi->RA_ME2 = osi->value_ME2term;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::calculate_dipole_Acc_QEW(int x_a, double & Dfactor){
  static Logger logger("amplitude_OpenLoops_set::calculate_dipole_Acc_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint_dipole(x_a);
  ol_evaluate_tree(csi->dipole[x_a].process_id, P_OL[x_a], &Dfactor);
  logger << LOG_DEBUG_POINT << "OpenLoops:     Dfactor[" << x_a << "] = " << setw(23) << setprecision(15) << Dfactor << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::calculate_dipole_Asc_QEW(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector){
  static Logger logger("amplitude_OpenLoops_set::calculate_dipole_Asc_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint_dipole(x_a);
  P_sc[x_a][0] = Vtensor.x0();
  P_sc[x_a][1] = Vtensor.x1();
  P_sc[x_a][2] = Vtensor.x2();
  P_sc[x_a][3] = Vtensor.x3();

  ol_evaluate_tree(csi->dipole[x_a].process_id, P_OL[x_a], &ME2_metric);
  logger << LOG_DEBUG_POINT << "OpenLoops:     ME2_metric[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_metric << endl;
  ol_evaluate_sc(csi->dipole[x_a].process_id, P_OL[x_a], csi->dipole[x_a].no_A_emitter(), P_sc[x_a], M2sc);
  ME2_vector = M2sc[0];
  logger << LOG_DEBUG_POINT << "OpenLoops:     ME2_vector[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_vector << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
