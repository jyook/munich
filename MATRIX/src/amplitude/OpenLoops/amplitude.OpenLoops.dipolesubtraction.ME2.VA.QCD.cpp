#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::set_mu_and_delta_VA(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_ioperator_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
  ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
  ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
  ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
  ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::set_testpoint_alpha_S(const double alpha_S){
  Logger logger("amplitude_OpenLoops_set::set_testpoint_alpha_S");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  ol_setparameter_double(OL_alpha_s, alpha_S);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::set_testpoint_rescale_I_ME2(){
  Logger logger("amplitude_OpenLoops_set::set_testpoint_rescale_I_ME2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void amplitude_OpenLoops_set::calculate_ME2_ioperator_VA_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_ioperator_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi->VA_ioperator->size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      osi->VA_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
      osi->VA_I_ME2_cf[i_a].resize((*(osi->VA_ioperator))[i_a].size());
    }
    initialization = 0;
  }

  set_phasespacepoint();

  if (!csi->class_contribution_loopinduced){
    ol_evaluate_tree(1, P_OpenLoops, &osi->VA_b_ME2);
    ol_evaluate_cc(1, P_OpenLoops, &osi->VA_b_ME2, M2cc, &ewcc);
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	osi->VA_ME2_cf[i_a][j_a] = M2cc[(*(osi->VA_ioperator))[i_a][j_a].no_BLHA_entry];
	logger << LOG_DEBUG_POINT << "OpenLoops:  VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
      }
    }
  }
  else {
    ol_evaluate_cc2(1, P_OpenLoops, &osi->VA_b_ME2, M2cc, &ewcc);
    for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
      for (int j_a = 0; j_a < (*(osi->VA_ioperator))[i_a].size(); j_a++){
	osi->VA_ME2_cf[i_a][j_a] = M2cc[(*(osi->VA_ioperator))[i_a][j_a].no_BLHA_entry];
	logger << LOG_DEBUG_POINT << "OpenLoops:  VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi->VA_ME2_cf[i_a][j_a] << endl;
      }
    }
  }

  // different from QEW !!!
  //  osi->calculate_ioperator_QCD();
  if (osi->massive_QCD){osi->calculate_ioperator_QCD_CDST();}
  else {osi->calculate_ioperator_QCD_CS();}

  for (int i_a = 0; i_a < osi->VA_ioperator->size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi->VA_I_ME2_cf[i_a].begin(), osi->VA_I_ME2_cf[i_a].end(), 0.);
  }
  osi->VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::calculate_ME2_VA_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_VI == 0 || osi->switch_VI == 1){

    set_phasespacepoint();
    if (!csi->class_contribution_loopinduced){
      ol_setparameter_double(OL_mu_ren, osi->var_mu_ren);
      ol_setparameter_double(OL_mu_reg, osi->var_mu_ren);
      ol_setparameter_double(fact_uv, one);
      ol_setparameter_double(fact_ir, one);
      /*
      // Another option:  Set  CT_on = 1 , set  osi->VA_V_ME2 = M2L1[0] - osi->VA_X_ME2;  after CT evaluation (remaining mu_ren-dependent counterterms as before):
      int CT_on = 1;
      ol_setparameter_int(stch("ct_on"), CT_on); // modification of ...ME2_VA and OpenLoops calls needed !!!
      */
      ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
      osi->VA_V_ME2 = M2L1[0];
      ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2);

      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_B  = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;

      /*
	if (osi->switch_output_comparison){
	int R2_on = 0;
	ol_setparameter_int(stch("r2_on"), R2_on);
	double V_ME2_D4 = 0.;
	ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
	V_ME2_D4 = M2L1[0];
	logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_D4 = " << setw(23) << setprecision(15) << V_ME2_D4 << endl;
	double V_ME2_R2 = osi->VA_V_ME2 - V_ME2_D4;
	logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_R2 = " << setw(23) << setprecision(15) << V_ME2_R2 << endl;
	R2_on = 1;
	ol_setparameter_int(stch("r2_on"), R2_on);
	}
      */

      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_V  = " << setw(23) << setprecision(15) << osi->VA_V_ME2 << endl;
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_CT = " << setw(23) << setprecision(15) << osi->VA_X_ME2 << endl;
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;

      if (osi->switch_VI == 0){
	if (osi->VA_DeltaIR1 == 0. && osi->VA_DeltaIR2 == 0.){osi->VA_I_ME2 = IRL1[0];}
	else {osi->VA_I_ME2 = IRL1[0] + osi->VA_DeltaIR1 * IRL1[1] + osi->VA_DeltaIR2 * IRL1[2];}
	//  osi->VA_I_ME2 = i_DeltaIR1 * IR1 + osi->VA_DeltaIR2 * IR2; // !!! I-operator switched off !!!
      }
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_I  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << endl;

      if (osi->switch_CV){
	for (int s = 0; s < osi->n_scales_CV; s++){
	  double inv_factor_CV = osi->var_mu_ren_CV[s] / osi->var_mu_ren;
	  ol_setparameter_double(OL_mu_ren, osi->var_mu_ren_CV[s]);
	  ol_setparameter_double(OL_mu_reg, osi->var_mu_ren_CV[s]);
	  ol_setparameter_double(fact_uv, inv_factor_CV);
	  ol_setparameter_double(fact_ir, inv_factor_CV);
	  ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2_CV[s]);
	  logger << LOG_DEBUG_POINT << "OpenLoops:  VA_X_ME2_CV[" << s << "] = " << setw(23) << setprecision(15) << osi->VA_X_ME2_CV[s] + osi->VA_V_ME2 << endl;
	  //	  logger << LOG_DEBUG_POINT << "OpenLoops:  VA_X_ME2_CV[" << s << "] = " << osi->VA_X_ME2_CV[s] << endl;
	  logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct s = " << s << endl;
	}
      }
      if (osi->switch_TSV){
	for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	  for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	    double inv_factor_TSV = osi->value_scale_ren[0][i_v][i_r] / osi->var_mu_ren;
	    ol_setparameter_double(OL_mu_ren, osi->value_scale_ren[0][i_v][i_r]);
	    ol_setparameter_double(OL_mu_reg, osi->value_scale_ren[0][i_v][i_r]);
	    ol_setparameter_double(fact_uv, inv_factor_TSV);
	    ol_setparameter_double(fact_ir, inv_factor_TSV);
	    ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->value_ME2term_ren[0][i_v][i_r]);
	    //	    logger << LOG_DEBUG_POINT << "OpenLoops:  VA_X_ME2_TSV[" << i_v << "][" << i_r << "] = " << osi->value_ME2term_ren[0][i_v][i_r] << endl;
	    logger << LOG_DEBUG_POINT << "OpenLoops:  VA_VX_ME2_TSV[" << i_v << "][" << i_r << "] = " << osi->value_ME2term_ren[0][i_v][i_r] + osi->VA_V_ME2 << endl;
	    osi->value_ME2term_ren[0][i_v][i_r] += osi->VA_V_ME2 + osi->VA_I_ME2;
	    logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct i_v = " << i_v << "   i_r = " << i_r << endl;
	  }
	}
      }
    }
    else {
      // This loop-induced L2VA contribution is not a general implementation and should be modified, e.g. moved to amp/<process> part !!!
      // Here: Implementation used for gg->WW/ZZ .

      ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
      ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
      ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);
      double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
      ol_setparameter_double(OL_mu_ren, mu_Q);
      ol_setparameter_double(OL_mu_reg, mu_Q);
      ol_setparameter_double(fact_uv, one);
      ol_setparameter_double(fact_ir, one);

      osi->VA_b_ME2 = 0.;
      ol_evaluate_loop2(1, P_OpenLoops, M2L2, &acc);

      logger << LOG_DEBUG_POINT << "M2L2[0] = " << M2L2[0] << endl;

      osi->VA_b_ME2 = M2L2[0];
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_L2I = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;

      if (osi->switch_H1gg){
	osi->QT_A0 = 0.;
	osi->QT_A1 = 0.;
	osi->QT_H1_delta = 0.;
      }
      else {
	//	gsi->calculate_H1gg(osi);
	calculate_H1gg_2loop();
      }

      if (osi->QT_A0 == 0. && osi->QT_A1 == 0. && osi->QT_H1_delta == 0.){
	osi->VA_V_ME2 = 0.;
      }
      else {
	logger << LOG_DEBUG_POINT << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << endl;

	logger << LOG_DEBUG_POINT << "born VVamp = " << setprecision(15) << setw(23) << osi->QT_A0 << " ,   "
	       << "1-loop VVamp = " << setprecision(15) << setw(23) << osi->QT_A1 << endl;
	logger << LOG_DEBUG_POINT << "born OL    = " << setprecision(15) << setw(23) << osi->VA_b_ME2 << " ,   "
	       << "             = " << setprecision(15) << setw(23) << osi->VA_V_ME2 << endl;

	// reweighting 2-loop amplitude with mt-dependence (from osi->VA_b_ME2):
	osi->VA_V_ME2 = osi->QT_A1 * osi->VA_b_ME2 / osi->QT_A0;

	// no mt-dependence (from osi->VA_b_ME2) in 2-loop amplitude:
	//      osi->VA_V_ME2 = osi->QT_A1;

	// osi.QT_A1 is only the two-loop amplitude (times one-loop...),
	// without reweighting with any loop² result
      }

      // temporary solution because mu_reg does not exist as a standard parameter -> use mu_reg = mu_Q = s^ in I-operator !!!
      double save_osi_var_mu_ren = osi->var_mu_ren;
      osi->var_mu_ren = mu_Q;
      calculate_ME2_ioperator_VA_QCD();
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_L2II  = " << setw(23) << setprecision(15) << osi->VA_I_ME2 << "   (corrected for var_rel_alpha_S)" << endl;
      osi->var_mu_ren = save_osi_var_mu_ren;

      // beta0 usually not initialized in CS subtraction !!!
      static double beta0 = (33. - 2 * osi->N_f) / 12;
      // scale variation (from mu_ren = s^ = mu_Q to the usual scales...) !!!
      osi->VA_X_ME2 = -csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->var_mu_ren, 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
      if (osi->switch_CV){
	for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	  osi->VA_X_ME2_CV[i_s] = -csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->var_mu_ren_CV[i_s], 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
	}
      }

      if (osi->switch_TSV){
	for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
	  for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
	    osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_V_ME2 + osi->VA_I_ME2 - csi->order_alpha_s_born * beta0 * log(pow(mu_Q / osi->value_scale_ren[0][i_v][i_r], 2)) * osi->VA_b_ME2 * (osi->alpha_S / pi);
	  }
	}
      }

      logger << LOG_DEBUG_VERBOSE
	     << "   V = " << setprecision(15) << setw(23) << osi->VA_V_ME2
	     << "   X = " << setprecision(15) << setw(23) << osi->VA_X_ME2
	     << "   I = " << setprecision(15) << setw(23) << osi->VA_I_ME2 << endl;

    }
  }
  else if (osi->switch_VI == 2){
    logger << LOG_DEBUG_VERBOSE << "OL: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
    //  osi->VA_I_ME2 = 0.;
    //  osi->VA_V_ME2 = 0.;
    //  osi->VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi->n_scales_CV; s++){osi->VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi->VA_I_ME2
    calculate_ME2_ioperator_VA_QCD();
    for (int i_v = 0; i_v < osi->max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi->n_scale_dyn_ren[i_v]; i_r++){
        osi->value_ME2term_ren[0][i_v][i_r] = osi->VA_I_ME2;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "SK: osi->VA_I_ME2 = " << osi->VA_I_ME2 << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
