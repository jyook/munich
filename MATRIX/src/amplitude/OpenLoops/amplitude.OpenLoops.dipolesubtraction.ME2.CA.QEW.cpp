#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_CA_QEW(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_CA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint();
  ol_evaluate_tree(1, P_OpenLoops, &M2L0);
  for (int i_a = 0; i_a < (*osi->CA_collinear).size(); i_a++){
    for (int j_a = 0; j_a < (*osi->CA_collinear)[i_a].size(); j_a++){
      // better shift charge_factor to pdfs !!! (could be wrong in sums over all (anti-)quarks otherwise) !!!
      osi->CA_ME2_cf[i_a][j_a] = (*osi->CA_collinear)[i_a][j_a].charge_factor() * M2L0;
      //  charge_factor_fi needs to be divided by corresponding CA_Q2f[i_c][...] as this factor is provided with the PDFs

      // check if this is correct (different in RCL version !!!)

      osi->CA_ME2_cf_fi[i_a][j_a] = (*osi->CA_collinear)[i_a][j_a].charge_factor_fi() / osi->CA_Q2f[i_a][0][0] * M2L0;
      logger << LOG_DEBUG_POINT << "OpenLoops:  CA_ME2[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf[i_a][j_a] << "    CA_ME2_fi[" << i_a << "][" << j_a << "] = " << osi->CA_ME2_cf_fi[i_a][j_a] << endl;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
