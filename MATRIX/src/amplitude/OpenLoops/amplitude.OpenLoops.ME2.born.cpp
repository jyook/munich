#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_born(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint();
  if (!csi->class_contribution_loopinduced){
    ol_evaluate_tree(1, P_OpenLoops, &(osi->value_ME2term[0]));
    osi->ME2 = osi->value_ME2term[0];
    if (!(osi->check_vanishing_ME2_end)){check_vanishing_ME2_born();}
    logger << LOG_DEBUG << "ME2_OL  = " << osi->ME2 <<endl;
  }
  else {
    ol_setparameter_double(renscale, osi->var_mu_ren);
    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);
    ol_evaluate_loop2(1, P_OpenLoops, M2L2, &acc);
    osi->ME2 = M2L2[0];
    logger << LOG_DEBUG_VERBOSE << "M2L2[0] = " << M2L2[0] << endl;
    osi->value_ME2term[0] = M2L2[0];
    logger << LOG_DEBUG_VERBOSE << "ME2_OL  = " << osi->ME2 <<endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::check_vanishing_ME2_born(){
  Logger logger("amplitude_OpenLoops_set::check_vanishing_ME2_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->ME2 == 0.){osi->flag_vanishing_ME2 = 1;}
  else {
    osi->flag_vanishing_ME2 = 0;
    ol_evaluate_cc(1, P_OpenLoops, &M2L0, M2cc, &ewcc);
    logger << LOG_DEBUG << "M2L0 = " << M2L0 << endl;
    for (int i_c = 0; i_c < n_cc; i_c++){
      logger << LOG_DEBUG << "M2cc[" << setw(2) << i_c << "] = " << M2cc[i_c] << endl;
      if (abs(M2cc[i_c]) > 1.e12 * abs(M2L0)){
	logger << LOG_DEBUG << "M2L0 = 0. due to numerical cancellations!" << endl;
	osi->flag_vanishing_ME2 = 1;
	break;
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
