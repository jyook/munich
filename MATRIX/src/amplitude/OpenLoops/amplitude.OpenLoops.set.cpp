#include "header.hpp"
#ifdef OPENLOOPS

////////////////////
//  constructors  //
////////////////////
amplitude_OpenLoops_set::amplitude_OpenLoops_set(){
  Logger logger("amplitude_OpenLoops_set::amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "amplitude_OpenLoops_set::amplitude_OpenLoops_set called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


amplitude_OpenLoops_set::amplitude_OpenLoops_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi){
  Logger logger("amplitude_OpenLoops_set::amplitude_OpenLoops_set (osi, csi, msi, esi)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  // ???
  logger << LOG_INFO << "amplitude_OpenLoops_set::amplitude_OpenLoops_set (osi, csi, msi, esi) called..." << endl;
  //this->amplitude_set::
  amplitude_set(_osi, _csi, _msi, _esi);
  logger << LOG_INFO << "after  calling  amplitude_set(_osi, _csi, _msi, _esi); ..." << endl;
  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


amplitude_OpenLoops_set::~amplitude_OpenLoops_set(){
  Logger logger("amplitude_OpenLoops_set::~amplitude_set");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  delete [] renscale;
  delete [] fact_uv;
  delete [] fact_ir;
  delete [] OL_mu_ren;
  delete [] OL_mu_reg;
  delete [] pole_uv;
  delete [] pole_ir1;
  delete [] pole_ir2;
  delete [] OL_alpha_s;

  delete [] M2cc_check;
  delete [] P_OpenLoops;

  if (csi->class_contribution_CS_real){
    for (size_t i_a = 1; i_a < osi->n_ps; i_a++){
      delete [] P_OL[i_a];
      delete [] P_sc[i_a];
    }
    delete [] M2cc;
    delete [] M2sc;
  }

  if (csi->class_contribution_loopinduced){
    delete [] M2L2;
  }
  else if (csi->class_contribution_virtual){
    delete [] M2L1;
    delete [] IRL1;
    delete [] M2L2;
    delete [] IRL2;
  }

   logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::set_phasespacepoint(){
  Logger logger("amplitude_OpenLoops_set::set_phasespacepoint");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i_p = 1; i_p < esi->p_parton[0].size(); i_p++){
    P_OpenLoops[5 * (i_p - 1)]     = esi->p_parton[0][i_p].x0();
    P_OpenLoops[5 * (i_p - 1) + 1] = esi->p_parton[0][i_p].x1();
    P_OpenLoops[5 * (i_p - 1) + 2] = esi->p_parton[0][i_p].x2();
    P_OpenLoops[5 * (i_p - 1) + 3] = esi->p_parton[0][i_p].x3();
    //    P_OpenLoops[5 * (i_p - 1) + 4] = esi->p_parton[0][i_p].m();
    P_OpenLoops[5 * (i_p - 1) + 4] = osi->mass_parton[0][i_p];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::set_phasespacepoint_dipole(const int x_a){
  Logger logger("amplitude_OpenLoops_set::set_phasespacepoint_dipole");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  for (int i_p = 1; i_p < esi->p_parton[x_a].size(); i_p++){
    P_OL[x_a][5 * (i_p - 1)]     = esi->p_parton[x_a][i_p].x0();
    P_OL[x_a][5 * (i_p - 1) + 1] = esi->p_parton[x_a][i_p].x1();
    P_OL[x_a][5 * (i_p - 1) + 2] = esi->p_parton[x_a][i_p].x2();
    P_OL[x_a][5 * (i_p - 1) + 3] = esi->p_parton[x_a][i_p].x3();
    //    P_OL[x_a][5 * (i_p - 1) + 4] = esi->p_parton[x_a][i_p].m();
    P_OL[x_a][5 * (i_p - 1) + 4] = osi->mass_parton[x_a][i_p];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::output_phasespacepoints(){
  Logger logger("amplitude_OpenLoops_set::output_phasespacepoints");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  ofstream out_point_OL;
  string filename_point_OL = "list.phasespacepoints.OL.dat";
  out_point_OL.open(filename_point_OL.c_str(), ofstream::out | ofstream::app);
  out_point_OL << "no_psp= " << right << setw(7) << osi->psi->i_acc << endl;
  ///  out_point_OL << "n_gen= " << right << setw(7) << osi->psi->i_gen << endl;
  for (int i_p = 1; i_p < esi->p_parton[0].size(); i_p++){
    out_point_OL << "p="
		 << setw(23) << showpoint << setprecision(17) << esi->p_parton[0][i_p].x0() << "   "
		 << setw(23) << showpoint << setprecision(17) << esi->p_parton[0][i_p].x1() << "   "
		 << setw(23) << showpoint << setprecision(17) << esi->p_parton[0][i_p].x2() << "   "
		 << setw(23) << showpoint << setprecision(17) << esi->p_parton[0][i_p].x3() << endl;
  }
  int x_s = osi->no_reference_TSV;
  int x_r = osi->no_scale_ren_reference_TSV;
  int x_f = osi->no_scale_fact_reference_TSV;

  logger << LOG_INFO << "x_s = " << x_s << endl;
  logger << LOG_INFO << "x_r = " << x_r << endl;
  logger << LOG_INFO << "x_f = " << x_f << endl;
  logger << LOG_INFO << "*(osi->pointer_scale_ren).size() = " << (osi->pointer_scale_ren).size() << endl;
  logger << LOG_INFO << "*(osi->pointer_scale_ren)[0].size() = " << (osi->pointer_scale_ren)[0].size() << endl;
  logger << LOG_INFO << "*(osi->pointer_scale_ren)[0][x_s].size() = " << (osi->pointer_scale_ren)[0][x_s].size() << endl;

  out_point_OL << showpoint << "mu= " << setw(21) <<  *(osi->pointer_scale_ren)[0][x_s][x_r] << endl;
  out_point_OL << showpoint << "as= " << setw(21) <<  *(osi->pointer_alpha_S_TSV)[0][x_s][x_r] << endl;
  ///  out_point_OL << "perm=" << right << setw(12) << "5" << setw(12) << "6" << setw(12) << "2" << setw(12) << "1" << setw(12) << "4" << setw(12) << "3" << setw(12) << "7" << endl;
  out_point_OL << showpoint << "me= " << setw(26) << osi->rescaling_factor_alpha_e  * *(osi->pointer_ME2term)[0][0][x_s][x_r][x_f] * *(osi->pointer_relative_factor_alpha_S)[0][x_s][x_r] << endl;
  out_point_OL << showpoint << "pdf= " << setw(25) << *(osi->pointer_pdf_factor)[0][0][x_s][x_f][0] << endl;
  out_point_OL << showpoint << "psw= " << setw(25) << osi->psi->ps_factor << endl;
  out_point_OL << showpoint << "tot= " << setw(25) << osi->integrand_TSV[x_s][x_r][x_f] << endl;
  //    out_point_OL << showpoint << "=" << setw(22) <<  << endl;
  out_point_OL << endl;
  out_point_OL.close();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
