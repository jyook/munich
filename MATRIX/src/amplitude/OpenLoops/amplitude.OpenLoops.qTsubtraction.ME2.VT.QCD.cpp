#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_VT_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_VT_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint();

  if (!csi->class_contribution_loopinduced){

    ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
    ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
    ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);

    double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
    ol_setparameter_double(OL_mu_ren, mu_Q);
    ol_setparameter_double(OL_mu_reg, mu_Q);
    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);

    logger << LOG_DEBUG_VERBOSE << "ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);" << endl;
    ol_evaluate_full(1, P_OpenLoops, &osi->VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
    osi->VA_V_ME2 = M2L1[0];
    logger << LOG_DEBUG_VERBOSE << "ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2);" << endl;
    ol_evaluate_ct(1, P_OpenLoops, &M2L0, &osi->VA_X_ME2);
    logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_B = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;
    logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_VX = " << setw(23) << setprecision(15) << osi->VA_V_ME2 + osi->VA_X_ME2 << endl;

      // Check if this if statement changes the "rest" contribution !!!
    if (osi->initial_diag || osi->initial_pdf_gq){

      //  osi->VA_V_ME2 = 2 Re < M0 | M1 >  etc.
      osi->QT_H1_delta = (osi->VA_V_ME2 + osi->VA_X_ME2) / osi->VA_b_ME2 / (osi->alpha_S / pi);
      // same translation for osi->QT_H1_delta as in ppllll24_calculate_H2.
      //  (alpha_S / pi) normalization of H1
      if (osi->switch_polenorm == 1){
	if (osi->initial_gg){osi->QT_H1_delta -= pi2_6 * C_A;}
	else if (osi->initial_qqx){osi->QT_H1_delta -= pi2_6 * C_F;}
	else {logger << LOG_FATAL << "Wrong process specified." << endl; exit(1);}
      }

    }
    else {
      osi->QT_H1_delta = 0.;
    }
  }
  else {
    //  else if (csi->type_contribution == "L2VT" ||
    //	   osi->user->string_value[osi->user->string_map["model"]] == "Bornloop"){

    ol_setparameter_double(pole_uv, osi->VA_DeltaUV);
    ol_setparameter_double(pole_ir1, osi->VA_DeltaIR1);
    ol_setparameter_double(pole_ir2, osi->VA_DeltaIR2);

    double mu_Q = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
    ol_setparameter_double(OL_mu_ren, mu_Q);
    ol_setparameter_double(OL_mu_reg, mu_Q);
    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);

    osi->VA_b_ME2 = 0.;

    //    double var_mu_ren = (esi->p_parton[0][1] + esi->p_parton[0][2]).m();
    //    ol_setparameter_double(renscale, var_mu_ren);
    ol_evaluate_loop2(1, P_OpenLoops, M2L2, &acc);
    osi->VA_b_ME2 = M2L2[0];
    logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_L2I = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << endl;

    if (osi->initial_diag){
      if (osi->switch_H1gg){
	osi->QT_A0 = osi->VA_b_ME2;
	osi->QT_A1 = 0.;
	osi->QT_H1_delta = 0.;
      }
      else {
	//	gsi->calculate_H1gg(osi);
	calculate_H1gg_2loop();
	// osi->QT_A0 determined in calculate_H1gg does not contain massive-quark loops.
	// The number of quark flavours in the loop is set by osi.N_f .

	//  logger << LOG_DEBUG_VERBOSE << "ratios = " << osi->QT_A0 / osi->VA_b_ME2 << ", " << osi->QT_A1 / M2L1[0] << endl;
	logger << LOG_DEBUG_POINT << "ratio:   born(VVamp) / b_ME2(OL) = " << setw(23) << setprecision(15) << osi->QT_A0 / osi->VA_b_ME2 << endl;

	// A0 and b_ME2 should be identical if same flavour schemes are used.
	// To avoid mismatch between L2VT and L2VA, set:
	// reweighting 2-loop amplitude with mt-dependence (by commenting the following line):
	// osi->QT_H1_delta = osi->QT_H1_delta / osi->VA_b_ME2 * osi->QT_A0;
	// osi->QT_A0 = osi->VA_b_ME2;
	// Changed to OpenLoops result in order to check if the CS-QT difference can be explained from this !!!

	logger << LOG_DEBUG_VERBOSE << "born(VVamp) = " << setw(23) << setprecision(15) << osi->QT_A0 << "   born(OL) = " << setw(23) << setprecision(15) << osi->VA_b_ME2 << ", " << "1-loop VVamp = " << setw(23) << setprecision(15) << osi->QT_A1 << endl;
      }
    }
    else {
      osi->QT_A0 = osi->VA_b_ME2;
      osi->QT_A1 = 0.;
      osi->QT_H1_delta = 0.;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
