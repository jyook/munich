#include "header.hpp"
#ifdef OPENLOOPS

void amplitude_OpenLoops_set::calculate_ME2_RA_QCD(){
  Logger logger("amplitude_OpenLoops_set::calculate_ME2_RA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (esi->cut_ps[0] > -1){
    set_phasespacepoint();
    if (!csi->class_contribution_loopinduced){
      ol_evaluate_tree(csi->dipole[0].process_id, P_OpenLoops, &osi->value_ME2term[0]);
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_R  = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;
      if (!(osi->check_vanishing_ME2_end)){
	osi->flag_vanishing_ME2 = 0;
	ol_evaluate_cc(1, P_OpenLoops, &M2tree, M2cc_check, &ewcc);
	logger << LOG_DEBUG_VERBOSE << "M2tree = " << M2tree << endl;
	for (int i_c = 0; i_c < n_cc_check; i_c++){
	  logger << LOG_DEBUG_VERBOSE << "M2cc_check[" << setw(2) << i_c << "] = " << M2cc_check[i_c] << endl;
	  if (abs(M2cc_check[i_c]) > 1.e12 * abs(M2tree)){
	    logger << LOG_DEBUG << "M2tree = 0. due to numerical cancellations!" << endl;
	    osi->flag_vanishing_ME2 = 1;
	    break;
	  }
	}
      }
    }
    else {
      ol_setparameter_double(renscale, osi->var_mu_ren);
      ol_setparameter_double(fact_uv, one);
      ol_setparameter_double(fact_ir, one);
      logger << LOG_DEBUG_VERBOSE << "csi->dipole[0].process_id = " << csi->dipole[0].process_id << endl;
      ol_evaluate_loop2(csi->dipole[0].process_id, P_OpenLoops, M2L2, &acc);
      osi->value_ME2term[0] = M2L2[0];
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_R  = " << setw(23) << setprecision(15) << osi->value_ME2term[0] << endl;
      // No "flag_vanishing_ME2" check for loop-induced processes (unstable ME'2 might be set to 0)!
    }
  }
  else {osi->value_ME2term[0] = 0.;}

  for (int i_a = 1; i_a < osi->n_ps; i_a++){
    if (esi->cut_ps[i_a] >= 0){
      if (csi->dipole[i_a].massive() == 0){
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
	else {cout << "Should not happen!" << endl;}
      }
      else {
	if      (csi->dipole[i_a].type_dipole() == 1){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_k_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 2){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ij_a_massive(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 3){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_k(i_a);}
	else if (csi->dipole[i_a].type_dipole() == 5){osi->value_ME2term[i_a] = calculate_dipole_QCD_A_ai_b(i_a);}
      }
    }
    else {osi->value_ME2term[i_a] = 0.;}
  }
  osi->RA_ME2 = osi->value_ME2term;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_OpenLoops_set::calculate_dipole_Acc_QCD(int x_a, double & Dfactor){
  static Logger logger("amplitude_OpenLoops_set::calculate_dipole_Acc_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint_dipole(x_a);
  logger << LOG_DEBUG_VERBOSE << "csi->dipole[x_a].process_id = " << csi->dipole[x_a].process_id << endl;
  for (int i = 0; i < n_momentum_dipole; i++){logger << LOG_DEBUG_VERBOSE << "P_OL[" << x_a << "][" << i << "] = " << P_OL[x_a][i] << endl;}

  if (csi->class_contribution_loopinduced){
    ol_evaluate_cc2(csi->dipole[x_a].process_id, P_OL[x_a], &M2tree, M2cc, &ewcc);
  }
  else {
    ol_evaluate_cc(csi->dipole[x_a].process_id, P_OL[x_a], &M2tree, M2cc, &ewcc);
  }
  for (int i = 0; i < n_cc; i++){logger << LOG_DEBUG_VERBOSE << "M2cc[" << i << "] = " << M2cc[i] << endl;}
  Dfactor = M2cc[csi->dipole[x_a].no_BLHA_entry];
  logger << LOG_DEBUG_POINT << "OpenLoops:     Dfactor[" << x_a << "] = " << setw(23) << setprecision(15) << Dfactor << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->dipole[x_a].no_BLHA_entry = " << csi->dipole[x_a].no_BLHA_entry << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void amplitude_OpenLoops_set::calculate_dipole_Asc_QCD(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector){
  static Logger logger("amplitude_OpenLoops_set::calculate_dipole_Asc_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  set_phasespacepoint_dipole(x_a);
  P_sc[x_a][0] = Vtensor.x0();
  P_sc[x_a][1] = Vtensor.x1();
  P_sc[x_a][2] = Vtensor.x2();
  P_sc[x_a][3] = Vtensor.x3();

  if (csi->class_contribution_loopinduced){
    ol_evaluate_cc2(csi->dipole[x_a].process_id, P_OL[x_a], &M2tree, M2cc, &ewcc);
  }
  else {
    ol_evaluate_cc(csi->dipole[x_a].process_id, P_OL[x_a], &M2tree, M2cc, &ewcc);
  }
  ME2_metric = M2cc[csi->dipole[x_a].no_BLHA_entry];
  logger << LOG_DEBUG_POINT << "OpenLoops:     ME2_metric[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_metric << endl;

  if (csi->class_contribution_loopinduced){
    ol_evaluate_sc2(csi->dipole[x_a].process_id, P_OL[x_a], csi->dipole[x_a].no_A_emitter(), P_sc[x_a], M2sc);
  }
  else {
    ol_evaluate_sc(csi->dipole[x_a].process_id, P_OL[x_a], csi->dipole[x_a].no_A_emitter(), P_sc[x_a], M2sc);
  }
  ME2_vector = M2sc[csi->dipole[x_a].no_A_spectator() - 1];
  logger << LOG_DEBUG_POINT << "OpenLoops:     ME2_vector[" << x_a << "] = " << setw(23) << setprecision(15) << ME2_vector << endl;

  logger << LOG_DEBUG_VERBOSE << "Vtensor = " << Vtensor << "   M2sc[" << csi->dipole[x_a].no_A_spectator() - 1 << "] = " << ME2_vector << endl;
  for (int i = 0; i < n_sc; i++){logger << LOG_DEBUG_VERBOSE << "M2sc[" << i << "] = " << M2sc[i] << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


#endif
