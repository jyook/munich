#include "header.hpp"

void amplitude_set::set_testpoint_scale_CA(){
  static Logger logger("ppemxnmnex04_calculate_dynamic_scale");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int sd = 1; sd < osi->value_mu_ren_rel.size(); sd++){
    if (osi->value_mu_ren_rel[sd].size() != 0){
      double temp_mu_central = osi->scale_ren;
      for (int ss = 0; ss < osi->value_mu_ren_rel[sd].size(); ss++){
        osi->value_mu_ren[sd][ss] = temp_mu_central * osi->value_mu_ren_rel[sd][ss];
        osi->value_alpha_S[sd][ss] = LHAPDF::alphasPDF(osi->value_mu_ren[sd][ss]);
        osi->value_factor_alpha_S[sd][ss] = pow(osi->value_alpha_S[sd][ss] / osi->alpha_S, osi->csi->contribution_order_alpha_s);
      }
    }
  }
  if (osi->id_scales == 1){
    osi->value_mu_fact = osi->value_mu_ren;
  }
  else {
    for (int sd = 1; sd < osi->value_mu_fact_rel.size(); sd++){
      if (osi->value_mu_fact_rel[sd].size() != 0){
	double temp_mu_central = osi->scale_fact;
        for (int ss = 0; ss < osi->value_mu_fact_rel[sd].size(); ss++){
          osi->value_mu_fact[sd][ss] = temp_mu_central * osi->value_mu_fact_rel[sd][ss];
        }
      }
    }
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void amplitude_set::set_testpoint_scale_TSV(int i_a){
  static Logger logger("ppemxnmnex04_calculate_dynamic_scale_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
    double temp_mu_central = osi->scale_ren;
    for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
      osi->value_scale_ren[i_a][sd][ss] = temp_mu_central * osi->value_relative_scale_ren[sd][ss];
      if (osi->needed_scale2_ren){osi->value_scale2_ren[i_a][sd][ss] = pow(osi->value_scale_ren[i_a][sd][ss], 2);}
      osi->value_alpha_S_TSV[i_a][sd][ss] = LHAPDF::alphasPDF(osi->value_scale_ren[i_a][sd][ss]);
      osi->value_relative_factor_alpha_S[i_a][sd][ss] = pow(osi->value_alpha_S_TSV[i_a][sd][ss] / osi->alpha_S, osi->csi->contribution_order_alpha_s);
    }
  }
  for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
    double temp_mu_central = osi->scale_fact;
    osi->value_central_scale_fact[sd] = temp_mu_central;
    if (osi->needed_scale2_fact){osi->value_central_logscale2_fact[sd] = 2 * log(temp_mu_central);}
    for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
      osi->value_scale_fact[i_a][sd][ss] = osi->value_central_scale_fact[sd] * osi->value_relative_scale_fact[sd][ss];
      if (osi->needed_scale2_fact){osi->value_scale2_fact[i_a][sd][ss] = pow(osi->value_scale_fact[i_a][sd][ss], 2);}
    }
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
