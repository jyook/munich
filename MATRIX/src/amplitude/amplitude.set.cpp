#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
amplitude_set::amplitude_set(){
  Logger logger("amplitude_set::amplitude_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "amplitude_set::amplitude_set called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


amplitude_set::amplitude_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi){
  Logger logger("amplitude_set::amplitude_set (osi, csi, msi, esi)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "amplitude_set::amplitude_set (osi, csi, msi, esi) called..." << endl;
  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void amplitude_set::initialization_basic(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi){
  Logger logger("amplitude_set::initialization_basic");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

/*
amplitude_set::amplitude_set(observable_set & _osi, contribution_set * _csi, model_set * _msi, event_set * _esi) : observable_set () {
  Logger logger("amplitude_set::amplitude_set (csi, msi, esi) : osi");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  //  this = &_osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

/*
void amplitude_set::initialization(){
  Logger logger("amplitude_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

