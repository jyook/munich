#include "header.hpp"

void amplitude_set::calculate_ME2check_VA_QCD(){
  Logger logger("amplitude_set::calculate_ME2check_VA_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi->switch_output_comparison){

    esi->phasespacepoint();
    //    gsi->phasespacepoint_psp(esi);

    if (esi->p_parton[0][0].x0() == 0.){generate_testpoint();}
    if (esi->p_parton[0][0].x0() != 0.){

      ofstream out_comparison;
      logger << LOG_DEBUG_VERBOSE << "osi->filename_comparison = " << osi->filename_comparison << endl;
      out_comparison.open(osi->filename_comparison.c_str(), ofstream::out | ofstream::app);

      esi->perform_event_selection();
      //    esi->perform_event_selection(*gsi);

      if (esi->cut_ps[0] == -1){
	out_comparison << "Phase-space 0 is cut. -> Default scale is used." << endl;
	for (int sd = 1; sd < osi->max_dyn_ren + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_ren[sd]; ss++){
	    osi->value_scale_ren[0][sd][ss] = 100.;
	  }
	}
	for (int sd = 1; sd < osi->max_dyn_fact + 1; sd++){
	  for (int ss = 0; ss < osi->n_scale_dyn_fact[sd]; ss++){
	    osi->value_scale_fact[0][sd][ss] = 100.;
	  }
	}
	// imported from QEW case:
	osi->var_mu_ren = 100.;
	osi->var_mu_fact = 100.;

	esi->cut_ps[0] = 0;
      }
      else {
	// Maybe not valid any longer:
	// 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
	// 2 - Testpoint is calculated at output scale.
	osi->calculate_dynamic_scale(0);
	//    gsi->calculate_dynamic_scale(0, esi, osi);
	osi->calculate_dynamic_scale_TSV(0);
	//    gsi->calculate_dynamic_scale_TSV(0, esi, osi);
	osi->determine_scale();
	/////  }
      }

      osi->VA_delta_flag = 1;
      string s_Delta;
      osi->VA_b_ME2 = 0.;
      osi->VA_V_ME2 = 0.;
      osi->VA_X_ME2 = 0.;
      osi->VA_I_ME2 = 0.;
      osi->VA_X_ME2_CV.resize(osi->n_scales_CV, 0.);

      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->var_alpha_S_reference);}
      // With OpenLoops, MUNICH does not reset the value of alpha_S for different scales, but adds the corresponding relative factors.
      // For the test-point output, the value of alpha_S is set in OpenLoops here:
      //      osi->alpha_S = osi->alpha_S * osi->var_rel_alpha_S;
      //      osi->alpha_S = osi->alpha_S * pow(osi->var_rel_alpha_S, double(csi->contribution_order_alpha_s - 1) / csi->contribution_order_alpha_s);
      //      osi->alpha_S = osi->var_alpha_S_reference;
      //      ol_setparameter_double(OL_alpha_s, osi->alpha_S);

      double i_Delta;
      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      osi->VA_DeltaIR2 = 0.;

      logger << LOG_DEBUG << "osi->VA_DeltaUV  = " << osi->VA_DeltaUV << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR1 = " << osi->VA_DeltaIR1 << endl;
      logger << LOG_DEBUG << "osi->VA_DeltaIR2 = " << osi->VA_DeltaIR2 << endl;

      set_mu_and_delta_VA();
      output_parameter("testpoint");
      calculate_ME2_VA_QCD();

      out_comparison << "Settings: " << endl << endl;
      out_comparison << setw(12) << "  mu_reg  = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  ( = mu_ren by default)" << endl;
      out_comparison << setw(12) << "  mu_ren  = " << setprecision(15) << setw(23) << osi->var_mu_ren << "  " << "alpha_S(mu_ren) = " << osi->alpha_S << endl;
      out_comparison << setw(12) << "  mu_fact = " << setprecision(15) << setw(23) << osi->var_mu_fact << endl;
      out_comparison << endl;

      out_comparison << "Absolute results: " << endl << endl;
      osi->output_testpoint_VA_result(out_comparison);
      double OL_I_ME2 = osi->VA_I_ME2;
      if (osi->switch_VI != 2){
	osi->VA_I_ME2 = 0.;
	// Recola: !!!
	set_testpoint_alpha_S(osi->var_alpha_S_reference);
	/*
	set_alphas_rcl(osi->var_alpha_S_reference, osi->var_mu_ren, osi->N_nondecoupled);
	rescale_process_rcl(1, "LO");
	*/
	calculate_ME2_ioperator_VA_QCD();
	// Recola: !!!
	set_testpoint_rescale_I_ME2();
	/*
	osi->VA_I_ME2 = osi->VA_I_ME2 * (osi->var_alpha_S_reference / osi->alpha_S) / osi->var_rel_alpha_S;
	*/
	osi->output_testpoint_VA_ioperator(out_comparison);
	osi->VA_I_ME2 = OL_I_ME2;
	//    double SK_I_ME2 = osi->VA_I_ME2;
	//    osi->output_testpoint_VA_ioperator(out_comparison);
      }
      osi->VA_V_ME2 = osi->VA_V_ME2 / osi->VA_b_ME2;
      osi->VA_X_ME2 = osi->VA_X_ME2 / osi->VA_b_ME2;
      osi->VA_I_ME2 = osi->VA_I_ME2 / osi->VA_b_ME2;
      //    osi->VA_b_ME2 = osi->VA_b_ME2;
      out_comparison << "Relative results (corrections devided by ME2_born, ME2_born divided by coupling constants): " << endl << endl;
      osi->output_testpoint_VA_result(out_comparison);
      out_comparison << endl;
      out_comparison << "Particle momenta: " << endl << endl;
      output_momenta(out_comparison, *osi);

      out_comparison << "Numerical check of (UV and IR) finiteness: " << endl << endl;

      if (allowed_Delta_IRneqUV){
	s_Delta = "Delta_UV";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaUV = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_QCD();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	osi->VA_DeltaUV = 0.;
	out_comparison << endl;

	s_Delta = "Delta_IR_1";
	for (int i = 0; i < 3; i++){
	  i_Delta = (double(i) - 1.) * 1.;
	  osi->VA_DeltaIR1 = i_Delta;
	  set_mu_and_delta_VA();
	  calculate_ME2_VA_QCD();
	  osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
	}
	osi->VA_DeltaIR1 = 0.;
	out_comparison << endl;
      }

      s_Delta = "Delta_IR_2";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi->VA_DeltaIR2 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_QCD();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      osi->VA_DeltaIR2 = 0.;
      out_comparison << endl;

      s_Delta = "Delta_UV = Delta_IR_1";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi->VA_DeltaUV = i_Delta;
	osi->VA_DeltaIR1 = i_Delta;
	set_mu_and_delta_VA();
	calculate_ME2_VA_QCD();
	osi->output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }

      osi->VA_DeltaUV = 0.;
      osi->VA_DeltaIR1 = 0.;
      out_comparison << endl;
      set_mu_and_delta_VA();
      calculate_ME2_VA_QCD();

      osi->VA_delta_flag = 0;
      out_comparison.close();

      if (osi->switch_output_comparison == 2){set_testpoint_alpha_S(osi->alpha_S);}
      // With OpenLoops, MUNICH does not reset the value of alpha_S for different scales, but adds the corresponding relative factors.
      // For the test-point output, the value of alpha_S is set in OpenLoops and reset to its standard value here:
      //      osi->alpha_S = osi->alpha_S / osi->var_rel_alpha_S;
      ///      osi->alpha_S = osi->alpha_S / pow(osi->var_rel_alpha_S, double(csi->contribution_order_alpha_s - 1) / csi->contribution_order_alpha_s);
      // Check if comments still make sense !!!

    }
    else {
      output_parameter("testpoint");
    }

    osi->switch_output_comparison = 0;
  }
  else {
    output_parameter("testpoint");
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

