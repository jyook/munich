#include "header.hpp"

void amplitude_set::initialization(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi){
  Logger logger("amplitude_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  initialization_set_default();
  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  initialization_input(isi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_set::initialization_basic(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi){
  Logger logger("amplitude_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  initialization_set_default();
  initialization_input(isi);

  logger << LOG_INFO << "_osi->switch_OL = " << _osi->switch_OL << endl;
  logger << LOG_INFO << "_osi->switch_RCL = " << _osi->switch_RCL << endl;

  osi = _osi;
  csi = _csi;
  msi = _msi;
  esi = _esi;

  logger << LOG_INFO << "osi->switch_OL = " << osi->switch_OL << endl;
  logger << LOG_INFO << "osi->switch_RCL = " << osi->switch_RCL << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_set::initialization_set_default(){
  Logger logger("amplitude_set::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  N_quarks = 6;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void amplitude_set::initialization_input(inputparameter_set * isi){
  Logger logger("amplitude_set::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_i = 0; i_i < isi->input_amplitude.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_amplitude[" << i_i << "] = " << isi->input_amplitude[i_i] << endl;

    if (isi->input_amplitude[i_i].variable == ""){}

    else if (isi->input_amplitude[i_i].variable == "N_quarks"){N_quarks = atoi(isi->input_amplitude[i_i].value.c_str());}

  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
