#include "header.hpp"

summary_distribution::summary_distribution(){}

summary_distribution::summary_distribution(int _i_d, int _i_s, int _x_q, summary_subprocess & _ysubprocess){
  static Logger logger("summary_distribution::summary_distribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  i_d = _i_d;
  i_s = _i_s;
  x_q = _x_q;
  ysubprocess = &_ysubprocess;
  ycontribution = ysubprocess->ycontribution;
  ygeneric = ysubprocess->ygeneric;
  osi = ygeneric->osi;

  initialization();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_distribution::initialization(){
  static Logger logger("summary_distribution::cleanup");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  name = ysubprocess->name + " " + osi->extended_distribution[i_d].xdistribution_name;
  fullname = ycontribution->infix_order_contribution + "___" +ysubprocess->name + "___" + osi->extended_distribution[i_d].xdistribution_name;

  group_run_N_binwise_TSV.resize(ycontribution->extended_directory.size());
  group_run_result_TSV.resize(ycontribution->extended_directory.size());
  group_run_deviation_TSV.resize(ycontribution->extended_directory.size());
  /*
  distribution_group_N_TSV.resize(ycontribution->extended_directory.size(), 0);
  distribution_group_run_N_TSV.resize(ycontribution->extended_directory.size());
  distribution_group_run_removal_run_qTcut_TSV.resize(ycontribution->extended_directory.size());
  */
  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    group_run_N_binwise_TSV[i_m].resize(ycontribution->extended_directory[i_m].size(), vector<long long> (osi->extended_distribution[i_d].n_bins));
    group_run_result_TSV[i_m].resize(ycontribution->extended_directory[i_m].size(), vector<vector<vector<double> > > (osi->extended_distribution[i_d].n_bins, vector<vector<double> > (osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.))));
    group_run_deviation_TSV[i_m].resize(ycontribution->extended_directory[i_m].size(), vector<vector<vector<double> > > (osi->extended_distribution[i_d].n_bins, vector<vector<double> > (osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.))));
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_distribution::readin(int i_m, int i_z, vector<vector<string> > & readin_data){
  static Logger logger("summary_distribution::readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
      for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	if (ycontribution->infix_order_contribution == "NNLO.QT-CS.24.CT2.QCD" || ycontribution->infix_order_contribution == "NNLO.QT-CS.24.VT2.QCD"){
	  //	  logger << LOG_DEBUG << "i_b = " << i_b << " + osi->extended_distribution[i_d = " << i_d << "].n_bins * (i_r = " << i_r << " * osi->n_scale_fact_TSV[i_s = " << i_s << "] + i_f = " << i_f << ") = " << i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f) << "   readin_data.size() = " << readin_data.size() << endl;
	  logger << LOG_DEBUG << "readin_data[" << i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f) << "].size() = " << readin_data[i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f)].size() << endl;
	}
	// Should be sufficient once !!!s
	group_run_N_binwise_TSV[i_m][i_z][i_b] = atol(readin_data[i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f)][0].c_str());
	if (ycontribution->infix_order_contribution == "NNLO.QT-CS.24.CT2.QCD" || ycontribution->infix_order_contribution == "NNLO.QT-CS.24.VT2.QCD"){logger << LOG_DEBUG << "after   group_run_N_binwise_TSV[" << i_m << "][" << i_z << "][" << i_b << "]" << endl;}
	group_run_result_TSV[i_m][i_z][i_b][i_r][i_f] = atof(readin_data[i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f)][1].c_str());
	if (ycontribution->infix_order_contribution == "NNLO.QT-CS.24.CT2.QCD" || ycontribution->infix_order_contribution == "NNLO.QT-CS.24.VT2.QCD"){logger << LOG_DEBUG << "after   group_run_result_TSV[" << i_m << "][" << i_z << "][" << i_b << "][" << i_r << "][" << i_f << "]" << endl;}
	group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f] = atof(readin_data[i_b + osi->extended_distribution[i_d].n_bins * (i_r * osi->n_scale_fact_TSV[i_s] + i_f)][2].c_str());
 	if (ycontribution->infix_order_contribution == "NNLO.QT-CS.24.CT2.QCD" || ycontribution->infix_order_contribution == "NNLO.QT-CS.24.VT2.QCD"){logger << LOG_DEBUG << "after   group_run_deviation_TSV[" << i_m << "][" << i_z << "][" << i_b << "][" << i_r << "][" << i_f << "]" << endl;}

	logger << LOG_DEBUG_VERBOSE << "" << osi->extended_distribution[i_d].xdistribution_name << "[i_d = " << i_d << "][i_s = " << i_s << "][x_q = " << x_q << "][i_m = " << i_m << "][i_z = " << i_z << "][i_b = " << i_b << "][" << i_r << "][" << i_f << "]   " << setw(15) << group_run_N_binwise_TSV[i_m][i_z][i_b] << "   " << setprecision(15) << setw(23) << group_run_result_TSV[i_m][i_z][i_b][i_r][i_f] << " +- " << setprecision(15) << setw(23) << group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f] << endl;
     }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_distribution::cleanup(){
  static Logger logger("summary_distribution::cleanup");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pid_t pid = getpid();
  stringstream temp_pid;
  //  temp_pid << "cat /proc/" << pid << "/status | grep VmSize";
  temp_pid << "cat /proc/" << pid << "/status | grep VmRSS";
  string temp_s_pid = temp_pid.str();
  int temp_i = system(temp_s_pid.c_str());
  logger << LOG_INFO << "... before cleanup in " << fullname << " :   temp_i = " << temp_i << endl;

  group_run_N_binwise_TSV.clear();
  group_run_result_TSV.clear();
  group_run_deviation_TSV.clear();

  temp_i = system(temp_s_pid.c_str());
  logger << LOG_INFO << "... after cleanup in " << fullname << " :   temp_i = " << temp_i << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
