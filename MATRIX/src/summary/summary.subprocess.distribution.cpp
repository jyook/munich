#include "header.hpp"

void summary_subprocess::initialization_distribution_TSV(){
  Logger logger("summary_subprocess::initialization_distribution_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  distribution_result_TSV.resize(osi->extended_distribution.size());
  distribution_deviation_TSV.resize(osi->extended_distribution.size());
  distribution_chi2_TSV.resize(osi->extended_distribution.size());

  distribution_N_total_TSV.resize(osi->extended_distribution.size());
  distribution_N_total_binwise_TSV.resize(osi->extended_distribution.size());

  logger << LOG_DEBUG << ycontribution->type_contribution << "." << ycontribution->type_correction << "   " << name << "   ycontribution->selection_n_qTcut = " << ycontribution->selection_n_qTcut << endl;

  vector<vector<long long> > temp_vvll_db(osi->extended_distribution.size());
  vector<vector<vector<long long> > > temp_vvvll_dbq(osi->extended_distribution.size());
  vector<vector<vector<int> > > temp_vvvi_dbq(osi->extended_distribution.size());

  for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
    if (!ygeneric->switch_output_distribution[i_d]){continue;}
    temp_vvll_db[i_d].resize(osi->extended_distribution[i_d].n_bins);
    temp_vvvll_dbq[i_d].resize(osi->extended_distribution[i_d].n_bins);
    temp_vvvi_dbq[i_d].resize(osi->extended_distribution[i_d].n_bins);

    distribution_result_TSV[i_d].resize(osi->extended_distribution[i_d].n_bins);
    distribution_deviation_TSV[i_d].resize(osi->extended_distribution[i_d].n_bins);
    distribution_chi2_TSV[i_d].resize(osi->extended_distribution[i_d].n_bins);

    // total is also binwise, but after removal of runs...
    distribution_N_total_TSV[i_d].resize(osi->extended_distribution[i_d].n_bins, vector<long long> (ycontribution->selection_n_qTcut, 0));
    distribution_N_total_binwise_TSV[i_d].resize(osi->extended_distribution[i_d].n_bins, vector<long long> (ycontribution->selection_n_qTcut, 0));

    for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
      temp_vvvll_dbq[i_d][i_b].resize(ycontribution->selection_n_qTcut, 0);
      temp_vvvi_dbq[i_d][i_b].resize(ycontribution->selection_n_qTcut, 0);

      distribution_result_TSV[i_d][i_b].resize(ycontribution->selection_n_qTcut);
      distribution_deviation_TSV[i_d][i_b].resize(ycontribution->selection_n_qTcut);
      distribution_chi2_TSV[i_d][i_b].resize(ycontribution->selection_n_qTcut);
      for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){

	distribution_result_TSV[i_d][i_b][x_q].resize(osi->n_extended_set_TSV);
	distribution_deviation_TSV[i_d][i_b][x_q].resize(osi->n_extended_set_TSV);
	distribution_chi2_TSV[i_d][i_b][x_q].resize(osi->n_extended_set_TSV);
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (!osi->switch_distribution_TSV[i_s]){continue;}

	  distribution_result_TSV[i_d][i_b][x_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  distribution_deviation_TSV[i_d][i_b][x_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  distribution_chi2_TSV[i_d][i_b][x_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	}
      }
    }
  }


  distribution_group_N_TSV.resize(ycontribution->extended_directory.size(), 0);
  distribution_group_run_N_TSV.resize(ycontribution->extended_directory.size());
  distribution_group_run_removal_run_qTcut_TSV.resize(ycontribution->extended_directory.size());
  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    distribution_group_run_N_TSV[i_m].resize(ycontribution->extended_directory[i_m].size(), 0);
    distribution_group_run_removal_run_qTcut_TSV[i_m].resize(ycontribution->extended_directory[i_m].size(), temp_vvvi_dbq);
  }

  distribution_group_result_TSV.resize(ycontribution->extended_directory.size(), distribution_result_TSV);
  distribution_group_deviation_TSV.resize(ycontribution->extended_directory.size(), distribution_deviation_TSV);
  distribution_group_chi2_TSV.resize(ycontribution->extended_directory.size(), distribution_chi2_TSV);
  distribution_group_N_binwise_TSV.resize(ycontribution->extended_directory.size(), temp_vvvll_dbq);
  distribution_group_N_total_TSV.resize(ycontribution->extended_directory.size(), temp_vvvll_dbq);
  distribution_group_N_total_binwise_TSV.resize(ycontribution->extended_directory.size(), temp_vvvll_dbq);
  distribution_group_counter_removal_run_qTcut_TSV.resize(ycontribution->extended_directory.size(), temp_vvvi_dbq);
  distribution_group_counter_nonzero_run_qTcut_TSV.resize(ycontribution->extended_directory.size(), temp_vvvi_dbq);

  distribution_group_counter_run_TSV.resize(ycontribution->extended_directory.size(), 0);



  // distribution readin definitions:
  logger << LOG_DEBUG_VERBOSE << "distribution readin definitions" << endl;

  logger << LOG_INFO << "Determination of non-vanishing contributions for chosen process : " << endl;

  index_contributing_run.resize(osi->n_extended_set_TSV, vector<vector<int> > (ycontribution->extended_directory.size()));
  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
	string distribution_file;
	distribution_file = "distribution_" + name + ".txt";
	string filepath = "../" + ycontribution->extended_directory[i_m][i_z] + "/distribution/" + osi->name_extended_set_TSV[i_s] + "/" + distribution_file;
	ifstream in_result(filepath.c_str());
	if (in_result.peek() == ifstream::traits_type::eof()){}
	else {index_contributing_run[i_s][i_m].push_back(i_z);}
      }
      if (i_s == osi->no_reference_TSV){distribution_group_counter_run_TSV[i_m] = index_contributing_run[i_s][i_m].size();}
    }
  }
  logger << LOG_DEBUG_VERBOSE << "distribution readin definitions   distribution_group_counter_run_TSV.size() = " << distribution_group_counter_run_TSV.size() << endl;

  in_result_vector.resize(osi->n_extended_set_TSV, vector<vector<ifstream*> > (ycontribution->extended_directory.size()));
  pos_line.resize(osi->n_extended_set_TSV, vector<vector<vector<streampos> > > (ycontribution->extended_directory.size()));
  time_stamp.resize(osi->n_extended_set_TSV, vector<vector<double> > (ycontribution->extended_directory.size()));
  filepath_time.resize(osi->n_extended_set_TSV, vector<vector<string> > (ycontribution->extended_directory.size()));
  filepath_distribution.resize(osi->n_extended_set_TSV, vector<vector<string> > (ycontribution->extended_directory.size()));

  logger << LOG_DEBUG_VERBOSE << "distribution readin definitions   paths resized." << endl;
  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      if (index_contributing_run[i_s][i_m].size() == 0){continue;}

      pos_line[i_s][i_m].resize(index_contributing_run[i_s][i_m].size(), vector<streampos> (1, 0));
      in_result_vector[i_s][i_m].resize(index_contributing_run[i_s][i_m].size());
      time_stamp[i_s][i_m].resize(index_contributing_run[i_s][i_m].size(), 0.);
      filepath_time[i_s][i_m].resize(index_contributing_run[i_s][i_m].size(), "");
      filepath_distribution[i_s][i_m].resize(index_contributing_run[i_s][i_m].size(), "");
      for (int x_z = 0; x_z < index_contributing_run[i_s][i_m].size(); x_z++){
	filepath_time[i_s][i_m][x_z] = "../" + ycontribution->extended_directory[i_m][index_contributing_run[i_s][i_m][x_z]] + "/time/" + "time_" + name + ".dat";
	logger << LOG_DEBUG_VERBOSE << "filepath_time[" << i_s << "][" << i_m << "][" << x_z << "] = " << filepath_time[i_s][i_m][x_z] << endl;
	/*
	string distribution_file;
 	distribution_file = "distribution_" + name + ".txt";
	string filepath = "../" + ycontribution->extended_directory[i_m][index_contributing_run[i_s][i_m][x_z]] + "/distribution/" + osi->name_extended_set_TSV[i_s] + "/" + distribution_file;
	*/
	filepath_distribution[i_s][i_m][x_z] = "../" + ycontribution->extended_directory[i_m][index_contributing_run[i_s][i_m][x_z]] + "/distribution/" + osi->name_extended_set_TSV[i_s] + "/" + "distribution_" + name + ".txt";
      }
    }
  }
  logger << LOG_DEBUG_VERBOSE << "distribution readin definitions   paths resized." << endl;

  no_line_distribution.resize(osi->n_extended_set_TSV, vector<vector<vector<int> > > (osi->extended_distribution.size(), vector<vector<int> > (ycontribution->selection_n_qTcut, vector<int> (2, 0))));
  pos_qTcut_distribution.resize(osi->n_extended_set_TSV, vector<vector<vector<vector<streampos> > > > (ycontribution->extended_directory.size()));

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      logger << LOG_DEBUG_VERBOSE << "index_contributing_run[" << i_s << "][" << i_m << "].size() = " << index_contributing_run[i_s][i_m].size() << endl;
      pos_qTcut_distribution[i_s][i_m].resize(index_contributing_run[i_s][i_m].size(), vector<vector<streampos> > (ycontribution->selection_n_qTcut, vector<streampos> (osi->extended_distribution.size())));
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::readin_distribution_TSV(){
  Logger logger("summary_subprocess::readin_distribution_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  output_memory_consumption(ycontribution->infix_order_contribution + "   " + name + "   " + "readin_distribution_TSV: start");

  initialization_distribution_TSV();

  output_memory_consumption(ycontribution->infix_order_contribution + "   " + name + "   " + "readin_distribution_TSV: after initialization");

  int x_s = osi->no_reference_TSV;
  int x_r = (osi->n_scale_ren_TSV[x_s] - 1) / 2;
  int x_f = (osi->n_scale_fact_TSV[x_s] - 1) / 2;

  // New implementation to read in distribution-wise:

  int check_contribution = 0;
  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      logger << LOG_DEBUG << ycontribution->infix_order_contribution << "   " << name << "   index_contributing_run[" << i_s << "][" << i_m << "].size() = " << index_contributing_run[i_s][i_m].size() << endl;
      if (index_contributing_run[i_s][i_m].size() > 0){check_contribution = 1; break;}
    }
    if (check_contribution){break;}
  }
  if (!check_contribution){
    logger << LOG_INFO << "No contributing run:    " << ycontribution->infix_contribution << " -- " << ycontribution->infix_order_contribution <<  " -- " << ycontribution->infix_path_contribution << " --- " << name << endl;
    cleanup_file_distribution();
    return;
  }

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      if (index_contributing_run[i_s][i_m].size() == 0){continue;}
      for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	time_stamp[i_s][i_m][i_z] = get_time_stamp(i_s, i_m, i_z);
	logger << LOG_DEBUG << ycontribution->infix_order_contribution << "   " << name << "   time_stamp[" << i_s << "][" << i_m << "][" << i_z << "] = " << time_stamp[i_s][i_m][i_z] << endl;
	//	time_stamp[i_s][i_m][i_z] = get_time_stamp(filepath_time[i_s][i_m][i_z]);
	logger << LOG_INFO << "Register filepath_distribution[" << i_s << "][" << i_m << "][" << i_z << "] = " << filepath_distribution[i_s][i_m][i_z] << "   " << "time_stamp = " << time_stamp[i_s][i_m][i_z] << endl;

	in_result_vector[i_s][i_m][i_z] = new std::ifstream(filepath_distribution[i_s][i_m][i_z].c_str());
	// Extract end-of-line information into  pos_line[i_s][i_m][i_z] :
	char c;
	while (in_result_vector[i_s][i_m][i_z]->get(c)){
	  if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result_vector[i_s][i_m][i_z]->tellg());}
	  //	  if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result->tellg());}
	}
	in_result_vector[i_s][i_m][i_z]->clear();
	in_result_vector[i_s][i_m][i_z]->close();
	//	logger << LOG_INFO << "in_result_vector[i_s][i_m][i_z]->is_open() = " << in_result_vector[i_s][i_m][i_z]->is_open() << endl;

	// Extract information on where to start readin for specified qTcut and distribution into  pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d] :
	int temp_counter = 2; // 1 without 'group counter' (old version)
	for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
	  temp_counter++;
	  for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	    no_line_distribution[i_s][i_d][x_q][0] = temp_counter;
	    temp_counter += (osi->extended_distribution[i_d].n_bins + 1) * osi->n_scale_ren_TSV[i_s] * osi->n_scale_fact_TSV[i_s];
	    pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d] = pos_line[i_s][i_m][i_z][no_line_distribution[i_s][i_d][x_q][0]];
	    temp_counter++;
	  }
	}
      }
    }

    output_memory_consumption(ycontribution->infix_order_contribution + "   " + name + "   readin_distribution_TSV: after setting pos_qTcut_distribution (" + osi->name_set_TSV[i_s] + ")");
    // clear pos_line only later !!!

    /*
    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
      if (!osi->switch_distribution_TSV[i_s]){continue;}
      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	  for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
	    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	      string temp_s;
	      in_result_vector[i_s][i_m][i_z]->seekg(pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d]);//, is.beg);
	      getline(*in_result_vector[i_s][i_m][i_z], temp_s);
	    }
	  }
	  in_result_vector[i_s][i_m][i_z]->clear();
	}
      }
    }
    */

    logger << LOG_INFO << "Readin data:   " << ycontribution->infix_order_contribution << "   " << name << "   " << osi->name_set_TSV[i_s] << endl;

    logger << LOG_DEBUG << "Readin distribution-independent data:   " << ycontribution->infix_order_contribution << "   " << name << "   " << osi->name_set_TSV[i_s] << endl;

    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      distribution_group_N_TSV[i_m] = 0;
      for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	check_reset_file_distribution(i_s, i_m, i_z);
	//	in_result_vector[i_s][i_m][i_z]->close();
	in_result_vector[i_s][i_m][i_z]->open(filepath_distribution[i_s][i_m][i_z].c_str());
	in_result_vector[i_s][i_m][i_z]->seekg(0);
	string readin_line;
	getline(*in_result_vector[i_s][i_m][i_z], readin_line);
	distribution_group_run_N_TSV[i_m][i_z] = atoll(readin_line.c_str());
	distribution_group_N_TSV[i_m] += distribution_group_run_N_TSV[i_m][i_z];
	getline(*in_result_vector[i_s][i_m][i_z], readin_line);
	if (readin_line[0] == '#'){n_ps = 1; logger << LOG_INFO << "Old file format: n_ps not set." << endl;}
	else {n_ps = atoll(readin_line.c_str());}
	in_result_vector[i_s][i_m][i_z]->close();
      }
    }

    logger << LOG_DEBUG << "Readin distribution-dependent data:   " << ycontribution->infix_order_contribution << "   " << name << "   " << osi->name_set_TSV[i_s] << endl;

    for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
      // This way, it should depend on x_q, which looks more reasonable in the first place !!!
      vector<long long> temp_events_counted_completely(ycontribution->extended_directory.size(), 0);
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	logger << LOG_DEBUG << "Readin distribution-dependent data:   x_q = " << x_q << "   i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	summary_distribution * distribution = new summary_distribution(i_d, i_s, x_q, *this);
	for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	  for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	    logger << LOG_DEBUG << "Readin distribution-dependent data [" << i_s << "][" << i_m << "][" << i_z << "] : " << filepath_distribution[i_s][i_m][i_z] << endl;
	    in_result_vector[i_s][i_m][i_z]->open(filepath_distribution[i_s][i_m][i_z].c_str());
	    check_reset_file_distribution(i_s, i_m, i_z);
	    logger << LOG_DEBUG << "in_result_vector[" << i_s << "][" << i_m << "][" << i_z << "] = " << in_result_vector[i_s][i_m][i_z] << endl;
	    in_result_vector[i_s][i_m][i_z]->seekg(pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d]);//, is.beg);
	    vector<vector<string> > readin_data;
	    while (readin_data.size() < osi->extended_distribution[i_d].n_bins * osi->n_scale_ren_TSV[i_s] * osi->n_scale_fact_TSV[i_s]){
	      string readin_line;
	      getline(*in_result_vector[i_s][i_m][i_z], readin_line);
	      vector<string> temp_s(1);
	      if (readin_line.size() == 0){break;}
	      int temp_comment = 0;
	      for (int i_sc = 0; i_sc < readin_line.size(); i_sc++){
		if (readin_line[0] == '#'){temp_comment = 1; break;}
		else if (readin_line[i_sc] == ' ' || readin_line[i_sc] == char(9)){
		  if (temp_s[temp_s.size() - 1].size() == 0){}
		  else{temp_s.push_back("");}
		}
		else {temp_s[temp_s.size() - 1].push_back(readin_line[i_sc]);}
	      }
	      if (temp_comment == 0){
		if (temp_s[temp_s.size() - 1] == ""){temp_s.erase(temp_s.end());}
		readin_data.push_back(temp_s);
	      }
      	    }
	    if (readin_data.size() == 0){
	      logger << LOG_INFO << "Readin ERROR" << "   " << ycontribution->infix_order_contribution << "   " << name << "   " << osi->name_set_TSV[i_s] << "   in_result_vector[" << i_s << "][" << i_m << "][" << i_z << "] = " << in_result_vector[i_s][i_m][i_z] << "   readin_data.size() = " << readin_data.size() << endl;
	    }
	    else {
	      // Check default size ???
	      distribution->readin(i_m, i_z, readin_data);
	    }
	    in_result_vector[i_s][i_m][i_z]->close();
	  }
	}

	if (i_d == 0 && osi->extended_distribution[i_d].xdistribution_type == "XS"){
	  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	    for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	      if (distribution_group_run_N_TSV[i_m][i_z] > 0){
		temp_events_counted_completely[i_m] += distribution->group_run_N_binwise_TSV[i_m][i_z][0];
	      }
	    }
	  }
	}

	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  ///      for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
	  double tolerance_factor = ygeneric->deviation_tolerance_factor;
	  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	    distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q] = 0;
	    vector<double> deviation_measure(index_contributing_run[i_s][i_m].size(), 0.);
	    vector<double> deviation_measure_sorted;
	    double min_deviation_measure = 1.e99;
	    double max_deviation_measure = 0.;

	    // reset this counter for each scaleset:
	    distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] = 0;
	    distribution_group_counter_removal_run_qTcut_TSV[i_m][i_d][i_b][x_q] = 0;

	    for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	      distribution_group_run_removal_run_qTcut_TSV[i_m][i_z][i_d][i_b][x_q] = 0;

	      distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] += distribution->group_run_N_binwise_TSV[i_m][i_z][i_b];
	      if (distribution_group_run_N_TSV[i_m][i_z] > 0){
		deviation_measure[i_z] = sqrt(distribution->group_run_deviation_TSV[i_m][i_z][i_b][x_r][x_f] - pow(distribution->group_run_result_TSV[i_m][i_z][i_b][x_r][x_f], 2) / distribution_group_run_N_TSV[i_m][i_z]) / (distribution_group_run_N_TSV[i_m][i_z] - 1) * sqrt(distribution_group_run_N_TSV[i_m][i_z]);
		if (deviation_measure[i_z] > 0.){
		  distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q]++;
		  if (deviation_measure[i_z] < min_deviation_measure){min_deviation_measure = deviation_measure[i_z];}
		  if (deviation_measure[i_z] > max_deviation_measure){max_deviation_measure = deviation_measure[i_z];}
		  deviation_measure_sorted.push_back(deviation_measure[i_z]);
		}
	      }
	    }

	    if (deviation_measure_sorted.size() > 0){
	      sort(deviation_measure_sorted.begin(), deviation_measure_sorted.end());
	      // in order to ignore runs with too low error estimates:
	      min_deviation_measure = deviation_measure_sorted[deviation_measure_sorted.size() / 10];
	      double temp_fraction = double(distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q]) / (distribution_group_N_TSV[i_m] * n_ps);
	      tolerance_factor = ygeneric->deviation_tolerance_factor * (1. - log10(temp_fraction));
	      // reset for each scaleset to avoid wrong counting:
	      distribution_group_N_total_TSV[i_m][i_d][i_b][x_q] = 0;

	      for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
		///		logger << LOG_INFO << setw(20) << name << "   deviation_measure[" << setw(2) << i_z << "] = " << deviation_measure[i_z] << endl;
		///		logger << LOG_INFO << setw(20) << name << "   condition 2:   " << deviation_measure[i_z] << " > " << tolerance_factor * min_deviation_measure << "   ->   " << (deviation_measure[i_z] > tolerance_factor * min_deviation_measure) << endl;
		//		temp_events_counted_completely[i_m] -> number of events counted in total (extracted from total_rate distribution)
		//		if (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] > 25 * temp_events_counted_completely[i_m] / distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q] && deviation_measure[i_z] > tolerance_factor * min_deviation_measure){
		//		if (deviation_measure[i_z] > tolerance_factor * min_deviation_measure){
		if ((deviation_measure[i_z] > tolerance_factor * min_deviation_measure) &&
		    (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] > 25 * distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q])){
		  logger << LOG_DEBUG << "ERASED   " << name << "   " << ycontribution->infix_order_contribution << "   " << osi->extended_distribution[i_d].xdistribution_name << "   " << osi->extended_distribution[i_d].bin_edge[i_b] << " :   " << deviation_measure[i_z] << " > " << tolerance_factor * min_deviation_measure << endl;
		  distribution_group_run_removal_run_qTcut_TSV[i_m][i_z][i_d][i_b][x_q] = 1;
		  distribution_group_counter_removal_run_qTcut_TSV[i_m][i_d][i_b][x_q]++;
		}
		else {
		  if ((deviation_measure[i_z] > tolerance_factor * min_deviation_measure) &&
		      (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] < 25 * distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q])){
		    logger << LOG_DEBUG << "TOO LOW STATISTICS   " << name << "   " << ycontribution->infix_order_contribution << "   " << osi->extended_distribution[i_d].xdistribution_name << "   " << osi->extended_distribution[i_d].bin_edge[i_b] << endl;
		  }
		  distribution_group_N_total_TSV[i_m][i_d][i_b][x_q] += distribution_group_run_N_TSV[i_m][i_z];
		  distribution_group_N_total_binwise_TSV[i_m][i_d][i_b][x_q] += distribution->group_run_N_binwise_TSV[i_m][i_z][i_b];
		}
	      }
	    }
	  }

	  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	    distribution_N_total_TSV[i_d][i_b][x_q] += distribution_group_N_total_TSV[i_m][i_d][i_b][x_q];
	    distribution_N_total_binwise_TSV[i_d][i_b][x_q] += distribution_group_N_total_binwise_TSV[i_m][i_d][i_b][x_q];
	  }
	  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	      if (ycontribution->average_factor == 1){
		combine_distribution_conservative_TSV(x_q, i_d, i_b, i_s, i_r, i_f, distribution);
	      }
	      else {
		/////		  combine_distribution_hybrid_TSV(x_q, i_d, i_b, i_s, i_r, i_f);
	      }

	      double result_subprocess = 0.;
	      double deviation_subprocess = 0.;
	      vector<double> deltasigma(ycontribution->extended_directory.size(), 0.);
	      vector<double> singleweight(ycontribution->extended_directory.size(), 0.);
	      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
		if (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] > 0 &&
		    distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] != 0. &&
		    distribution_group_N_total_binwise_TSV[i_m][i_d][i_b][x_q] > distribution_N_total_binwise_TSV[i_d][i_b][x_q] / 100){
		  singleweight[i_m] = 1. / pow(distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f], 2);
		  result_subprocess += singleweight[i_m] * distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f];
		  deviation_subprocess += singleweight[i_m];
		}
	      }
	      if (deviation_subprocess != 0.){
		distribution_result_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = result_subprocess / deviation_subprocess;
		distribution_deviation_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = 1. / sqrt(deviation_subprocess);
	      }
	      else {
		distribution_result_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
		distribution_deviation_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
	      }

	      //  chi2 calculation from [i_m] combination:
	      distribution_chi2_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
	      int temp_counter = 0;
	      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
		if (distribution_group_N_TSV[i_m] > 0){
		  if (!(distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] == 0. && distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] == 0.)){
		    temp_counter++;
		    distribution_chi2_TSV[i_d][i_b][x_q][i_s][i_r][i_f] += pow((distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] - distribution_result_TSV[i_d][i_b][x_q][i_s][i_r][i_f]) / distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f], 2.);
		  }
		}
	      }
	      if (temp_counter > 1){
		distribution_chi2_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = distribution_chi2_TSV[i_d][i_b][x_q][i_s][i_r][i_f] / temp_counter;
	      }
	      else {
		//  chi2 cannot be reasonably defined for only one run.
		distribution_chi2_TSV[i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
	      }
	      /////	      }
	    }
	  }
	}
	//	delete new_distribution;
	delete distribution;
      }
    }

    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
      for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	//	in_result_vector[i_s][i_m][i_z]->close();
	//	delete in_result_vector[i_s][i_m][i_z];
      }
    }
  }


  logger << LOG_INFO << "New implemantation done.   " << ycontribution->infix_order_contribution << "   " << name << endl;

  output_memory_consumption(ycontribution->infix_order_contribution + "   " + name + "   readin_distribution_TSV: before swapping");

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
     for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
       //       in_result_vector[i_s][i_m][i_z]->close();
       delete in_result_vector[i_s][i_m][i_z];
      }
    }
  }

  cleanup_file_distribution();

  output_memory_consumption(ycontribution->infix_order_contribution + "   " + name + "   readin_distribution_TSV: after cleanup_file_distribution");

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::combine_distribution_conservative_TSV(int x_q, int i_d, int i_b, int i_s, int i_r, int i_f, summary_distribution * distribution){
  Logger logger("summary_subprocess::combine_distribution_conservative_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    double temp_result_subprocess = 0.;
    double temp_deviation2_subprocess = 0.;
    for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
      logger << LOG_DEBUG_VERBOSE << "distribution_group_run_removal_run_qTcut_TSV[" << i_m << "][" << i_z << "][" << i_d << "][" << i_b << "][" << x_q << "] = " << distribution_group_run_removal_run_qTcut_TSV[i_m][i_z][i_d][i_b][x_q] << "   distribution_group_run_N_binwise_TSV = " << distribution->group_run_N_binwise_TSV[i_m][i_z][i_b] << endl;
      if (!distribution_group_run_removal_run_qTcut_TSV[i_m][i_z][i_d][i_b][x_q] && distribution->group_run_N_binwise_TSV[i_m][i_z][i_b] > 0){
	///      if (!distribution_group_run_removal_run_qTcut_TSV[i_m][i_z][i_d][i_b][x_q] && distribution->group_run_N_binwise_TSV[i_m][i_z][i_b][i_s][i_r][i_f] > 0){
	// better: use only 'reference' version thereof ???
	// repeated for all i_s, i_r, i_f !!! (should be counted only once - if run is not removed ??? ) -> comment out !!!
	//	distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] += distribution->group_run_N_binwise_TSV[i_m][i_z][i_b];
	///	distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] += distribution->group_run_N_binwise_TSV[i_m][i_z][i_b][i_s][i_r][i_f];
	temp_result_subprocess += distribution->group_run_result_TSV[i_m][i_z][i_b][i_r][i_f];
	  // should be named sum_result...
	temp_deviation2_subprocess += distribution->group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f];
	  // should be named sum_deviation²...
      }
    }
    // difference to CV??? Maybe because of errors in CV counter !!! ???
    if (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q] != 0){
      ///    if (distribution_group_N_binwise_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] != 0){
      distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = temp_result_subprocess / distribution_group_N_total_TSV[i_m][i_d][i_b][x_q];
      // distribution_group_N_total_TSV[i_m][i_d][i_b][x_q]  instead of  distribution_group_N_TSV[i_m]  since the latter contains also results from removed runs
      //  distribution_group_deviation2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = (distribution_group_N_TSV[i_m] * temp_deviation2_subprocess - pow(temp_result_subprocess, 2)) / distribution_group_N_TSV[i_m] / pow(distribution_group_N_TSV[i_m] - 1, 2); // possible replacement - check if correct !!!
      distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = sqrt((distribution_group_N_total_TSV[i_m][i_d][i_b][x_q] * temp_deviation2_subprocess - pow(temp_result_subprocess, 2)) / distribution_group_N_total_TSV[i_m][i_d][i_b][x_q]) / (distribution_group_N_total_TSV[i_m][i_d][i_b][x_q] - 1);
    }
    else {
      distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
      distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
    }




    //  chi2 calculation:
    distribution_group_chi2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
    logger << LOG_DEBUG_VERBOSE << "chi2 evaluation in " << ycontribution->resultdirectory << " -- " << ycontribution->infix_contribution << " -- " << ycontribution->infix_order_contribution <<  " -- " << ycontribution->infix_path_contribution << " --- " << name << endl;
    logger << LOG_DEBUG_VERBOSE << "combined result = " << setw(15) << setprecision(9) << distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] << " +- " << setw(15) << setprecision(9) << distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] << endl;

    if (distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q] > 1){
      for (int i_z = 0; i_z < index_contributing_run[i_s][i_m].size(); i_z++){
	if (!(distribution->group_run_result_TSV[i_m][i_z][i_b][i_r][i_f] == 0. && distribution->group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f] == 0.)){
	  // expectation value -> temp_run_result
	  // sum (result) ->distribution->group_run_result_TSV[i_m][i_z][i_b][i_r][i_f]
	  double temp_run_result = distribution->group_run_result_TSV[i_m][i_z][i_b][i_r][i_f] / distribution_group_run_N_TSV[i_m][i_z];
	  // standard deviation -> temp_run_deviation
	  // sum (result²) ->distribution->group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f]
	  double temp_run_deviation = sqrt(distribution->group_run_deviation_TSV[i_m][i_z][i_b][i_r][i_f] - pow(distribution->group_run_result_TSV[i_m][i_z][i_b][i_r][i_f], 2) / distribution_group_run_N_TSV[i_m][i_z]) / (distribution_group_run_N_TSV[i_m][i_z] - 1);
	  distribution_group_chi2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] += pow((temp_run_result - distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f]) / temp_run_deviation, 2.);
	}
      }
      distribution_group_chi2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = distribution_group_chi2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] / distribution_group_counter_nonzero_run_qTcut_TSV[i_m][i_d][i_b][x_q];
    }
    else {
      // chi2 cannot be reasonably defined for only one run.
      distribution_group_chi2_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] = 0.;
    }

    logger << LOG_DEBUG_VERBOSE << "total_rate combined [" << i_m << "][" << i_d << "][" << i_b << "][" << x_q << "][i_s = " << i_s << "][" << i_r << "][" << i_f << "]   " << setprecision(15) << setw(23) << distribution_group_result_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] << " +- " << distribution_group_deviation_TSV[i_m][i_d][i_b][x_q][i_s][i_r][i_f] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}





void summary_subprocess::initialization_distribution_CV(){
  Logger logger("summary_subprocess::initialization_distribution_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  distribution_result_CV.resize(osi->extended_distribution.size());
  distribution_deviation_CV.resize(osi->extended_distribution.size());
  distribution_chi2_CV.resize(osi->extended_distribution.size());

  distribution_N_total_CV.resize(osi->extended_distribution.size());
  distribution_N_total_binwise_CV.resize(osi->extended_distribution.size());

  // in analogy to TSV:
  vector<vector<long long> > temp_vvll_db(osi->extended_distribution.size());
  vector<vector<int> > temp_vvi_db(osi->extended_distribution.size());

  for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
    if (!ygeneric->switch_output_distribution[i_d]){continue;}
    temp_vvll_db[i_d].resize(osi->extended_distribution[i_d].n_bins, 0);
    temp_vvi_db[i_d].resize(osi->extended_distribution[i_d].n_bins, 0);

    distribution_result_CV[i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
    distribution_deviation_CV[i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
    distribution_chi2_CV[i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));

    // total is also binwise, but after removal of runs...
    distribution_N_total_CV[i_d].resize(osi->extended_distribution[i_d].n_bins, 0);
    distribution_N_total_binwise_CV[i_d].resize(osi->extended_distribution[i_d].n_bins, 0);
  }

  distribution_group_N_CV.resize(ycontribution->extended_directory.size(), 0);
  distribution_group_run_result_CV.resize(ycontribution->extended_directory.size());
  distribution_group_run_deviation_CV.resize(ycontribution->extended_directory.size());
  distribution_group_run_N_CV.resize(ycontribution->extended_directory.size());
  distribution_group_run_N_binwise_CV.resize(ycontribution->extended_directory.size());
  distribution_group_run_removal_run_CV.resize(ycontribution->extended_directory.size());

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    distribution_group_run_result_CV[i_m].resize(ycontribution->extended_directory[i_m].size(), distribution_result_CV);
    distribution_group_run_deviation_CV[i_m].resize(ycontribution->extended_directory[i_m].size(), distribution_deviation_CV);
    distribution_group_run_N_binwise_CV[i_m].resize(ycontribution->extended_directory[i_m].size(), temp_vvll_db);
    distribution_group_run_N_CV[i_m].resize(ycontribution->extended_directory[i_m].size(), 0);
    distribution_group_run_removal_run_CV[i_m].resize(ycontribution->extended_directory[i_m].size(), temp_vvi_db);
  }

  distribution_group_result_CV.resize(ycontribution->extended_directory.size(), distribution_result_CV);
  distribution_group_deviation_CV.resize(ycontribution->extended_directory.size(), distribution_deviation_CV);
  distribution_group_chi2_CV.resize(ycontribution->extended_directory.size(), distribution_chi2_CV);
  distribution_group_N_binwise_CV.resize(ycontribution->extended_directory.size(), temp_vvll_db);
  distribution_group_N_total_CV.resize(ycontribution->extended_directory.size(), temp_vvll_db);
  distribution_group_N_total_binwise_CV.resize(ycontribution->extended_directory.size(), temp_vvll_db);
  distribution_group_counter_removal_run_CV.resize(ycontribution->extended_directory.size(), temp_vvi_db);
  distribution_group_counter_nonzero_run_CV.resize(ycontribution->extended_directory.size(), temp_vvi_db);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::readin_distribution_CV(){
  Logger logger("summary_subprocess::readin_distribution_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

 initialization_distribution_CV();


  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	  string filename = "../" + ycontribution->extended_directory[i_m][i_z] + "/distribution/CV/" + osi->directory_name_scale_CV[i_s] + "/" + osi->extended_distribution[i_d].xdistribution_name + "_" + name + ".dat";
	  vector<string> readin;
	  char LineBuffer[128];
	  ifstream in_result(filename.c_str());
	  while (in_result.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
	  in_result.close();
	  logger << LOG_DEBUG_VERBOSE << filename << "   number of lines: " << setw(5) << readin.size() << endl;
	  if (readin.size() != 0){
	    // old version: to be adapted !!!
	    // should be identical for each distribution, thus independent of [i_d] (is overwritten for each i_d):
	    distribution_group_run_N_CV[i_m][i_z] = atoll(readin[0].c_str());
	    for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	      distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] = atoll(readin[1 + i_b * 3].c_str());

	      // temporary (because N is doubled):
	      distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] = distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] / 2;

	      //  N[i_z][i_p][i_d][i_b] = N[i_z][i_p][i_d][i_b] / (osi->n_scales_CV + 1); // !!! ???
	      distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s] = atof(readin[2 + i_b * 3].c_str());
	      distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s] = atof(readin[3 + i_b * 3].c_str());

	      // only to deal with incompletely filled distribution files (N->0)
	      if (distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] == 0 && distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s] != 0.){
		logger << LOG_INFO << name << "   distribution_group_run_N_binwise_CV corrected." << endl;
		distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] = 1;
	      }
	      // by here.
	    }

	  }
	  else {
	    distribution_group_run_N_CV[i_m][i_z] = 0;
	    for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	      distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] = 0;
	      distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s] = 0.;
	      distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s] = 0.;
	    }
	  }
	}
      }

    }
  }

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
      distribution_group_N_CV[i_m] += distribution_group_run_N_CV[i_m][i_z];
      // with no runs removed at [i_d][i_b], one should later get: distribution_group_run_N_CV[i_m] == distribution_group_N_total_CV[i_m][i_d][i_b] !!!
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::combination_distribution_CV(){
  Logger logger("summary_subprocess::combination_distribution_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
    if (!ygeneric->switch_output_distribution[i_d]){continue;}
    logger << LOG_DEBUG << left << setw(20) << name << "  osi->extended_distribution[" << i_d << "].xdistribution_name = " << osi->extended_distribution[i_d].xdistribution_name << endl;
    for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){

      double tolerance_factor = ygeneric->deviation_tolerance_factor;

      // replace by reference value ???
      int x_s = (osi->n_scales_CV - 1) / 2;

      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	distribution_group_counter_nonzero_run_CV[i_m][i_d][i_b] = 0;
	vector<double> deviation_measure(ycontribution->extended_directory[i_m].size(), 0.);
	vector<double> deviation_measure_sorted;
	double min_deviation_measure = 1.e99;
	double max_deviation_measure = 0.;

	for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
	  distribution_group_N_binwise_CV[i_m][i_d][i_b] += distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b];
	  if (distribution_group_run_N_CV[i_m][i_z] > 0){
	    deviation_measure[i_z] = sqrt(distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][x_s] - pow(distribution_group_run_result_CV[i_m][i_z][i_d][i_b][x_s], 2) / distribution_group_run_N_CV[i_m][i_z]) / (distribution_group_run_N_CV[i_m][i_z] - 1) * sqrt(distribution_group_run_N_CV[i_m][i_z]);

	    if (deviation_measure[i_z] > 0.){
	      distribution_group_counter_nonzero_run_CV[i_m][i_d][i_b]++;
	      if (deviation_measure[i_z] < min_deviation_measure){min_deviation_measure = deviation_measure[i_z];}
	      if (deviation_measure[i_z] > max_deviation_measure){max_deviation_measure = deviation_measure[i_z];}
	      deviation_measure_sorted.push_back(deviation_measure[i_z]);
	    }
	    //	      else {logger << LOG_WARN << "deviation_measure[" << i_z << "] = " << deviation_measure[i_z] << endl;}
	  }
	}

	if (deviation_measure_sorted.size() > 0){
	  sort(deviation_measure_sorted.begin(), deviation_measure_sorted.end());
	  // in order to ignore runs with too low error estimates:
	  min_deviation_measure = deviation_measure_sorted[deviation_measure_sorted.size() / 10];


	  logger << LOG_DEBUG_VERBOSE << "distribution_group_N_binwise_CV[" << i_m << "][" << i_d << "][" << i_b << "] = " << distribution_group_N_binwise_CV[i_m][i_d][i_b] << endl;

	  double temp_fraction = double(distribution_group_N_binwise_CV[i_m][i_d][i_b]) / distribution_group_N_CV[i_m];
	  tolerance_factor = ygeneric->deviation_tolerance_factor * (1. - log10(temp_fraction));
	  if (tolerance_factor < ygeneric->deviation_tolerance_factor){tolerance_factor = ygeneric->deviation_tolerance_factor;}

	  for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
	    if (deviation_measure[i_z] > tolerance_factor * min_deviation_measure &&
		distribution_group_N_binwise_CV[i_m][i_d][i_b] > 1000){
	      distribution_group_run_removal_run_CV[i_m][i_z][i_d][i_b] = 1;
	      distribution_group_counter_removal_run_CV[i_m][i_d][i_b]++;
	    }
	    else {
	      distribution_group_N_total_CV[i_m][i_d][i_b] += distribution_group_run_N_CV[i_m][i_z];
	      distribution_group_N_total_binwise_CV[i_m][i_d][i_b] += distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b];
	      if (distribution_group_N_binwise_CV[i_m][i_d][i_b] <= 1000){
		logger << LOG_DEBUG << setw(20) << ycontribution->infix_order_contribution << "   " << setw(20) << name << "TFE [" << i_m << "] -> " << setw(6) << ygeneric->phasespace_optimization[i_m] << ": [" << i_d << "] = " << setw(25) << osi->extended_distribution[i_d].xdistribution_name << "   [" << setw(3) << i_b << "] = " << osi->extended_distribution[i_d].bin_edge[i_b] << "   " << endl;

	      }
	    }
	  }
	}
      }


      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	distribution_N_total_CV[i_d][i_b] += distribution_group_N_total_CV[i_m][i_d][i_b];
	distribution_N_total_binwise_CV[i_d][i_b] += distribution_group_N_total_binwise_CV[i_m][i_d][i_b];
      }



      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	if (ycontribution->average_factor == 1){
	  combine_distribution_conservative_CV(i_d, i_b, i_s);
	}
	else {
	  //	  combine_distribution_hybrid_CV(i_d, i_b, i_s);
	}
      }

      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
      //  weighted combination of results from different extended_directory contributions:
      //  should receive a mechanism to prevent underestimated errors from low-statistics contributions to get much weight !!!
      double result_subprocess = 0.;
      double deviation_subprocess = 0.;
      vector<double> deltasigma(ycontribution->extended_directory.size(), 0.);
      vector<double> singleweight(ycontribution->extended_directory.size(), 0.);
      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	logger << LOG_DEBUG_VERBOSE << "distribution_group_result_CV[" << setw(3) << i_m << "][" << setw(3) << i_d << "][" << setw(3) << i_b << "][" << setw(3) << i_s << "] = " << setw(15) << setprecision(8) << distribution_group_result_CV[i_m][i_d][i_b][i_s] << " +- " << setw(15) << setprecision(9) << distribution_group_deviation_CV[i_m][i_d][i_b][i_s] << endl;

	//		if (distribution_group_N_binwise_CV[i_m][i_d][i_b][i_s] > 0){
	//	if (distribution_group_N_binwise_CV[i_m][i_d][i_b] > 0){
	if (distribution_group_N_binwise_CV[i_m][i_d][i_b] > 0 &&
	    distribution_group_deviation_CV[i_m][i_d][i_b][i_s] != 0. &&
	    distribution_group_N_total_binwise_CV[i_m][i_d][i_b] > distribution_N_total_binwise_CV[i_d][i_b] / 100){
	  ///	    distribution_group_N_total_CV[i_m][i_d][i_b] > distribution_N_total_CV[i_d][i_b] / 100){
	  //		singleweight[i_m] = 1. / distribution_group_deviation2_CV[i_m][i_d][i_b][i_s];
	  singleweight[i_m] = 1. / pow(distribution_group_deviation_CV[i_m][i_d][i_b][i_s], 2);
	  result_subprocess += singleweight[i_m] * distribution_group_result_CV[i_m][i_d][i_b][i_s];
	  deviation_subprocess += singleweight[i_m];
	}
	else if (distribution_group_N_total_binwise_CV[i_m][i_d][i_b] < distribution_N_total_binwise_CV[i_d][i_b] / 100){
	  ///	else if (distribution_group_N_total_CV[i_m][i_d][i_b] <= distribution_N_total_CV[i_d][i_b] / 100){
	  logger << LOG_DEBUG << setw(20) << ycontribution->infix_order_contribution << "   " << setw(20) << name << "OCU [" << i_m << "] -> " << setw(6) << ygeneric->phasespace_optimization[i_m] << ": [" << i_d << "] = " << setw(25) << osi->extended_distribution[i_d].xdistribution_name << "   [" << setw(3) << i_b << "] = " << osi->extended_distribution[i_d].bin_edge[i_b] << "   " << endl;

	}
      }
      if (deviation_subprocess != 0.){
	distribution_result_CV[i_d][i_b][i_s] = result_subprocess / deviation_subprocess;
	/// simplified version that does not take into account the difference of central values in determination of new standard deviation...
	distribution_deviation_CV[i_d][i_b][i_s] = 1. / sqrt(deviation_subprocess);
	/*
	  double final_deviation = 0.;
	  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	  final_deviation += (singleweight[i_m] + 4 * pow(singleweight[i_m] * (distribution_group_result_CV[i_m][i_d][i_b][i_s] - distribution_result_CV[i_d][i_b][i_s]), 2)) / pow(deviation_subprocess, 2);
	  }
	  distribution_deviation_CV[i_d][i_b][i_s] = sqrt(final_deviation);
	*/
      }
      else {
	distribution_result_CV[i_d][i_b][i_s] = 0.;
	distribution_deviation_CV[i_d][i_b][i_s] = 0.;
      }


      //	if (ycontribution->type_contribution == "RRA" && name == "du~_emmumepvm~gg"){
      if (i_d == 0){
	logger << LOG_DEBUG
	       << setw(5) << ycontribution->type_contribution
	       << "   "
	       << setw(20) << name
	       << "   [i_d][i_b][i_s]"
	       << "[" << setw(3) << i_d << "]"
	       << "[" << setw(3) << i_d << "]"
	       << "[" << setw(3) << i_b << "]"
	       << "[" << setw(3) << i_s << "]"
	       << "   "
	       << setw(23) << setprecision(15) << distribution_result_CV[i_d][i_b][i_s]
	       << " +- "
	       << setw(23) << setprecision(15) << distribution_deviation_CV[i_d][i_b][i_s]
	       << endl;
      }


      logger << LOG_DEBUG_VERBOSE << "           distribution_result_CV[" << setw(3) << i_d << "][" << setw(3) << i_b << "][" << setw(3) << i_s << "] = " << setw(15) << setprecision(8) << distribution_result_CV[i_d][i_b][i_s] << " +- " << setw(15) << setprecision(9) << distribution_deviation_CV[i_d][i_b][i_s] << endl;

      //  chi2 calculation from [i_m] combination:
      distribution_chi2_CV[i_d][i_b][i_s] = 0.;
      logger << LOG_DEBUG_VERBOSE << "chi2 evaluation in " << ycontribution->resultdirectory << " -- " << ycontribution->infix_contribution << " -- " << ycontribution->infix_order_contribution <<  " -- " << ycontribution->infix_path_contribution << " --- " << name << endl;
      logger << LOG_DEBUG_VERBOSE << "combined result = " << setw(15) << setprecision(9) << distribution_result_CV[i_d][i_b][i_s] << " +- " << setw(15) << setprecision(8) << distribution_deviation_CV[i_d][i_b][i_s] << endl;

      int temp_counter = 0;
      for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
	if (distribution_group_N_CV[i_m] > 0){
	  if (!(distribution_group_result_CV[i_m][i_d][i_b][i_s] == 0. && distribution_group_deviation_CV[i_m][i_d][i_b][i_s] == 0.)){
	    temp_counter++;
	    distribution_chi2_CV[i_d][i_b][i_s] += pow((distribution_group_result_CV[i_m][i_d][i_b][i_s] - distribution_result_CV[i_d][i_b][i_s]) / distribution_group_deviation_CV[i_m][i_d][i_b][i_s], 2.);
	  }
	}
      }
      if (temp_counter > 1){
	distribution_chi2_CV[i_d][i_b][i_s] = distribution_chi2_CV[i_d][i_b][i_s] / temp_counter;
      }
      else {
	//  chi2 cannot be reasonably defined for only one run.
	distribution_chi2_CV[i_d][i_b][i_s] = 0.;
      }
      logger << LOG_DEBUG_VERBOSE << "distribution_chi2_CV[" << setw(3) << i_d << "][" << setw(3) << i_b << "][" << setw(3) << i_s << "] = " << distribution_chi2_CV[i_d][i_b][i_s] << " (" << temp_counter << ")" << endl;
      }

    }
  }

  vector<vector<long long> > ().swap(distribution_group_run_N_CV);
  vector<vector<vector<vector<int> > > > ().swap(distribution_group_run_removal_run_CV);
  vector<vector<vector<vector<long long> > > > ().swap(distribution_group_run_N_binwise_CV);
  vector<vector<vector<vector<vector<double> > > > > ().swap(distribution_group_run_result_CV);
  vector<vector<vector<vector<vector<double> > > > > ().swap(distribution_group_run_deviation_CV);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void summary_subprocess::combine_distribution_conservative_CV(int i_d, int i_b, int i_s){
  Logger logger("summary_subprocess::combine_distribution_conservative_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // !!! just for now: x_z is not needed as a parameter...

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    double temp_result_subprocess = 0.;
    double temp_deviation2_subprocess = 0.;
    for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
      if (!distribution_group_run_removal_run_CV[i_m][i_z][i_d][i_b] && distribution_group_run_N_binwise_CV[i_m][i_z][i_d][i_b] > 0){
	temp_result_subprocess += distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s];
	  // should be named sum_result...
	temp_deviation2_subprocess += distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s];
	  // should be named sum_deviation²...
      }
    }
    if (distribution_group_N_total_CV[i_m][i_d][i_b] != 0){
      //    if (distribution_group_N_binwise_CV[i_m][i_d][i_b] != 0){
      ///    if (distribution_group_N_binwise_CV[i_m][i_d][i_b][i_s] != 0){
      distribution_group_result_CV[i_m][i_d][i_b][i_s] = temp_result_subprocess / distribution_group_N_total_CV[i_m][i_d][i_b];
      // distribution_group_N_total_CV[i_m][i_d][i_b]  instead of  distribution_group_N_CV[i_m]  since the latter contains also results from removed runs
      //  distribution_group_deviation2_CV[i_m][i_d][i_b][i_s] = (distribution_group_N_CV[i_m] * temp_deviation2_subprocess - pow(temp_result_subprocess, 2)) / distribution_group_N_CV[i_m] / pow(distribution_group_N_CV[i_m] - 1, 2); // possible replacement - check if correct !!!
      distribution_group_deviation_CV[i_m][i_d][i_b][i_s] = sqrt((distribution_group_N_total_CV[i_m][i_d][i_b] * temp_deviation2_subprocess - pow(temp_result_subprocess, 2)) / distribution_group_N_total_CV[i_m][i_d][i_b]) / (distribution_group_N_total_CV[i_m][i_d][i_b] - 1);
    }
    else {
      distribution_group_result_CV[i_m][i_d][i_b][i_s] = 0.;
      distribution_group_deviation_CV[i_m][i_d][i_b][i_s] = 0.;
    }

    if (i_d == 0){
      logger << LOG_DEBUG
	     << setw(5) << ycontribution->type_contribution
	     << "   "
	     << setw(20) << name
	     << "   group"
	     << "[" << setw(3) << i_m << "]"
	     << "[" << setw(3) << i_d << "]"
	     << "[" << setw(3) << i_b << "]"
	     << "[" << setw(3) << i_s << "]"
	     << "   "
	     << setw(23) << setprecision(15) << distribution_group_result_CV[i_m][i_d][i_b][i_s]
	     << " +- "
	     << setw(23) << setprecision(15) << distribution_group_deviation_CV[i_m][i_d][i_b][i_s]
	     << endl;
    }


    //  chi2 calculation:
    distribution_group_chi2_CV[i_m][i_d][i_b][i_s] = 0.;
    logger << LOG_DEBUG_VERBOSE << "chi2 evaluation in " << ycontribution->resultdirectory << " -- " << ycontribution->infix_contribution << " -- " << ycontribution->infix_order_contribution <<  " -- " << ycontribution->infix_path_contribution << " --- " << name << endl;
    logger << LOG_DEBUG_VERBOSE << "combined result = " << setw(15) << setprecision(9) << distribution_group_result_CV[i_m][i_d][i_b][i_s] << " +- " << setw(15) << setprecision(9) << distribution_group_deviation_CV[i_m][i_d][i_b][i_s] << endl;

    if (distribution_group_counter_nonzero_run_CV[i_m][i_d][i_b] > 1){
      for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
	if (!(distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s] == 0. && distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s] == 0.)){
	  // expectation value -> temp_run_result
	  // sum (result) ->distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s]
	  double temp_run_result = distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s] / distribution_group_run_N_CV[i_m][i_z];
	  // standard deviation -> temp_run_deviation
	  // sum (result²) ->distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s]
	  double temp_run_deviation = sqrt(distribution_group_run_deviation_CV[i_m][i_z][i_d][i_b][i_s] - pow(distribution_group_run_result_CV[i_m][i_z][i_d][i_b][i_s], 2) / distribution_group_run_N_CV[i_m][i_z]) / (distribution_group_run_N_CV[i_m][i_z] - 1);
	  distribution_group_chi2_CV[i_m][i_d][i_b][i_s] += pow((temp_run_result - distribution_group_result_CV[i_m][i_d][i_b][i_s]) / temp_run_deviation, 2.);
	}
      }
      distribution_group_chi2_CV[i_m][i_d][i_b][i_s] = distribution_group_chi2_CV[i_m][i_d][i_b][i_s] / distribution_group_counter_nonzero_run_CV[i_m][i_d][i_b];
    }
    else {
      // chi2 cannot be reasonably defined for only one run.
      distribution_group_chi2_CV[i_m][i_d][i_b][i_s] = 0.;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


