#include "header.hpp"

void summary_generic::determination_runtime_result(){
  Logger logger("summary_generic::determination_runtime_result");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "switch_reference = " << osi->switch_reference << endl;

  //  int x_m = 0;

  //  int x_q = 0;
  int x_q = osi->no_qTcut_reference_TSV;
  int x_s = osi->no_reference_TSV;
  int x_r = osi->no_scale_ren_reference_TSV;
  int x_f = osi->no_scale_fact_reference_TSV;

  logger << LOG_DEBUG << "default output from TSV (in summary routine):" << endl;
  logger << LOG_DEBUG << "name_reference_TSV          = " << osi->name_reference_TSV << endl;
  logger << LOG_DEBUG << "no_reference_TSV            = " << osi->no_reference_TSV << endl;
  logger << LOG_DEBUG << "no_scale_ren_reference_TSV  = " << osi->no_scale_ren_reference_TSV << endl;
  logger << LOG_DEBUG << "no_scale_fact_reference_TSV = " << osi->no_scale_fact_reference_TSV << endl;
  logger << LOG_DEBUG << "no_qTcut_reference_TSV      = " << osi->no_qTcut_reference_TSV << endl;

  // runtime extrapolation determination:

  for (int i_o = 0; i_o < yorder.size(); i_o++){
    //    logger << LOG_INFO << "yorder[" << i_o << "]->resultdirectory = " << setw(15) << yorder[i_o]->resultdirectory << endl;
    yorder[i_o]->error2_time = 0.;
    for (int i_l = 0; i_l < yorder[i_o]->contribution_file.size(); i_l++){
      int x_l = mapping_contribution_file[yorder[i_o]->contribution_file[i_l]];
      //      logger << LOG_INFO << "yorder[" << i_o << "]->resultdirectory = " << "order_contribution_file[" << i_o << "][" << i_l << "] = " << yorder[i_o]->contribution_file[i_l] << endl;
      //      logger << LOG_INFO << "x_l = mapping_contribution_file[order_contribution_file[" << i_o << "][" << i_l << "]] = " << mapping_contribution_file[yorder[i_o]->contribution_file[i_l]] << endl;
      logger << LOG_INFO << "yorder[" << i_o << "]->resultdirectory = " << setw(15) << yorder[i_o]-> resultdirectory << "   xlist[" << x_l << "]: error2_time = " << xlist[x_l]->xcontribution[0]->xsubprocess[0]->error2_time << endl;
      yorder[i_o]->error2_time += xlist[x_l]->xcontribution[0]->xsubprocess[0]->error2_time;
    }
    logger << LOG_INFO << "yorder[" << i_o << "]->resultdirectory = " << setw(15) << yorder[i_o]-> resultdirectory << " :           error2_time = " << yorder[i_o]->error2_time << endl;
  }

  //  logger << LOG_INFO << "osi->switch_reference = " << osi->switch_reference << endl;

  vector<double> order_cross_section(yorder.size());
  vector<double> order_relative_error(yorder.size());
  for (int i_o = 0; i_o < order_cross_section.size(); i_o++){
    //    logger << LOG_INFO << "yorder[" << i_o << "].accuracy_no_normalization = " << yorder[i_o]->accuracy_no_normalization << endl;

    //    logger << LOG_INFO << "osi->switch_reference = " << osi->switch_reference << endl;
    if (osi->switch_reference == "TSV"){
      logger << LOG_INFO << "yorder[" << i_o << "]:   [0][0][" << x_s << "][" << x_r << "][" << x_f << "]:   result_TSV = " << setw(15) << setprecision(8) << yorder[yorder[i_o]->accuracy_no_normalization]->result_TSV[0][0][x_s][x_r][x_f] << " +- " << setw(15) << setprecision(8) << yorder[yorder[i_o]->accuracy_no_normalization]->deviation_TSV[0][0][x_s][x_r][x_f] << "   acc_rel = " << yorder[i_o]->accuracy_relative << endl;
      order_cross_section[i_o] = yorder[yorder[i_o]->accuracy_no_normalization]->result_TSV[0][0][x_s][x_r][x_f];
    }
    else if (osi->switch_reference == "CV"){
      logger << LOG_INFO << "yorder[yorder[" << i_o << "].accuracy_no_normalization = " << yorder[i_o]->accuracy_no_normalization << "].result_CV[0][0][" << osi->no_central_scale_CV << "] = " << yorder[yorder[i_o]->accuracy_no_normalization]->result_CV[0][0][osi->no_central_scale_CV] << endl;

      order_cross_section[i_o] = yorder[yorder[i_o]->accuracy_no_normalization]->result_CV[0][0][osi->no_central_scale_CV];
      ///      order_cross_section[i_o] = yorder[yorder[i_o]->accuracy_no_normalization]->result_CV[0][0][0][osi->no_central_scale_CV];
    }
    else {logger << LOG_FATAL << "Should not happen! No reference scale selected." << endl; exit(1);}
    order_relative_error[i_o] = yorder[i_o]->accuracy_relative;

    //    logger << LOG_INFO << "order_cross_section[" << i_o << "] = " << order_cross_section[i_o] << "   order_relative_error[" << i_o << "] = " << order_relative_error[i_o] << endl;

  }



  //  Doesn't seem to work in general !!!
  //  First calculate extrapolated runtime from each order (later to be generalied to distributions, tail-enhancement runs, etc.)

  //  ERT[i_l][j_o][i_c][i_p] -> xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime  based on  order_cross_section[x_o = CiO[j_o]]
  vector<vector<vector<vector<double> > > > ERT(xlist.size());

  vector<vector<int> > CiO(xlist.size());
  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int i_o = 0; i_o < yorder.size(); i_o++){
      for (int j_l = 0; j_l < yorder[i_o]->contribution_file.size(); j_l++){
	if (i_l == mapping_contribution_file[yorder[i_o]->contribution_file[j_l]]){
	 CiO[i_l].push_back(i_o);
	 break;
	}
      }
    }

    ERT[i_l].resize(CiO[i_l].size());
    for (int j_o = 0; j_o < CiO[i_l].size(); j_o++){
      // probably not needed here:
      //      int x_o = CiO[i_l][j_o];
      ERT[i_l][j_o].resize(xlist[i_l]->xcontribution.size());
      ERT[i_l][j_o][0].resize(1, 0.);
      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	ERT[i_l][j_o][i_c].resize(xlist[i_l]->xcontribution[i_c]->xsubprocess.size(), 0.);
      }
    }
  }

  //  ESD[i_l][j_o][i_c][i_p] -> xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation  based on  order_cross_section[x_o = CiO[j_o]]
  vector<vector<vector<vector<double> > > > ESD = ERT;
  //  ESND[i_l][j_o][i_c][i_p] -> xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation  based on  order_cross_section[x_o = CiO[j_o]]
  vector<vector<vector<vector<double> > > > ESND = ERT;
  //  ESN[i_l][j_o][i_c][i_p] -> xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization  based on  order_cross_section[x_o = CiO[j_o]]
  vector<vector<vector<vector<double> > > > ESN = ERT;
  //  ESE[i_l][j_o][i_c][i_p] -> xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event  based on  order_cross_section[x_o = CiO[j_o]]
  vector<vector<vector<vector<double> > > > ENE = ERT; // should finally be 'long long' or so...


  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int j_o = 0; j_o < CiO[i_l].size(); j_o++){
      int x_o = CiO[i_l][j_o];
      /*
      ERT[i_l][j_o][0][0] = 0.;
      ESD[i_l][j_o][0][0] = 0.;
      ESND[i_l][j_o][0][0] = 0.;
      */ // 1 -> 0
      for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	ERT[i_l][j_o][i_c][0] = 0.;
	ESD[i_l][j_o][i_c][0] = 0.;
	ESND[i_l][j_o][i_c][0] = 0.;
 	if (i_c == 0){continue;}

	for (int i_p = 1; i_p < xlist[i_l]->xcontribution[i_c]->xsubprocess.size(); i_p++){

	  int switch_zero_contribution = 0;
	  if (osi->switch_reference == "TSV"){
	    logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_TSV[0][0] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	    if (xlist[i_l]->xcontribution[i_c]->active_qTcut && no_qTcut_runtime_estimate){x_q = no_qTcut_runtime_estimate;}
	    if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] == 0.){switch_zero_contribution = 1;}
	    logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_TSV[0][" << x_q << "][" << x_s << "][" << x_r << "][" << x_f << "] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	  }
	  else if (osi->switch_reference == "CV"){
	    // check why zero !!!
	    if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_CV[0][0] == 0.){switch_zero_contribution = 1;}
	  }

	  if (switch_zero_contribution){
	    ERT[i_l][j_o][i_c][i_p] = 0.;
	    ENE[i_l][j_o][i_c][i_p] = 0.;
	    ESD[i_l][j_o][i_c][i_p] = 0.;
	    ESN[i_l][j_o][i_c][i_p] = 0.;
	    ESND[i_l][j_o][i_c][i_p] = 0.;
	  }
	  else {
	    //  error2_time  seems to actually contain the sqrt of how it is named, i.e.  error * sqrt(time) !!!
	    ERT[i_l][j_o][i_c][i_p] = yorder[x_o]->error2_time * xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->error2_time / pow(order_relative_error[x_o] * order_cross_section[x_o], 2);

	    // ERT[i_l][j_o][i_c][0]  contains the sum of the extrapolated runtimes of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	    ERT[i_l][j_o][i_c][0] += ERT[i_l][j_o][i_c][i_p];

	    // ENE[i_l][j_o][i_c][i_p]  converts  extrapolated_runtime  for each subprocess into  extrapolated_n_event  by using respective  time_per_event :
	    ENE[i_l][j_o][i_c][i_p] = ERT[i_l][j_o][i_c][i_p] / xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->time_per_event;

	    // ESD[i_l][j_o][i_c][i_p]  states final absolute error of each subprocess :
	    ESD[i_l][j_o][i_c][i_p] = xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->error2_time / sqrt(ERT[i_l][j_o][i_c][i_p]);

	    // ESD[i_l][j_o][i_c][0]  states (temporarily) square of final absolute error of complete contribution  xlist[i_l]->xcontribution[i_c] :
	    ESD[i_l][j_o][i_c][0] += pow(ESD[i_l][j_o][i_c][i_p], 2);

	    //  ESN[i_l][j_o][i_c][i_p]  contains reference cross section  order_cross_section[x_o]  (defined by order  x_o ):
	    ESN[i_l][j_o][i_c][i_p] = order_cross_section[x_o];

	    //  ESN[i_l][j_o][i_c][i_p]_deviation  contains square of relative error of each subprocess  wrt.  order_cross_section[x_o] :
	    ESND[i_l][j_o][i_c][i_p] = ESD[i_l][j_o][i_c][i_p] / order_cross_section[x_o];
	    //  ESND[i_l][j_o][i_c][0]  contains sum of squares of relative errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	    ESND[i_l][j_o][i_c][0] += pow(ESND[i_l][j_o][i_c][i_p], 2);
	  }
	}

	//  ERT[i_l][j_o][0][0]  contatins sum of runtimes of all contributions in  xlist[i_l] :
	ERT[i_l][j_o][0][0] += ERT[i_l][j_o][i_c][0];

	//  ESN[i_l][j_o][0][0]  contains  order_cross_section[x_o]  (defined by order  x_o ):
	ESN[i_l][j_o][0][0] = order_cross_section[x_o];
	//  ESN[i_l][j_o][i_c][0]  contains  order_cross_section[x_o]  (defined by order  x_o ):
	ESN[i_l][j_o][i_c][0] = order_cross_section[x_o];

	//  ESD[i_l][j_o][0][0]  contains (temporarily) sum of  squares of absolute errors of all contributions in  xlist[i_l] :
	ESD[i_l][j_o][0][0] += ESD[i_l][j_o][i_c][0];

	//  ESD[i_l][j_o][i_c][0]  contains sqrt of sum of absolute errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	ESD[i_l][j_o][i_c][0] = sqrt(ESD[i_l][j_o][i_c][0]);

	//  ESND[i_l][j_o][0][0]  contains (temporarily) sum of  squares of relative errors of all contributions in  xlist[i_l] :
	ESND[i_l][j_o][0][0] += ESND[i_l][j_o][i_c][0];

	//  ESND[i_l][j_o][i_c][0]  contains sqrt of sum of relative errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	ESND[i_l][j_o][i_c][0] = sqrt(ESND[i_l][j_o][i_c][0]);

	// ???
	ENE[i_l][j_o][0][0] = 0.;
      }
      // again ???
      ESN[i_l][j_o][0][0] = order_cross_section[x_o];

      //  ESD[i_l][j_o][0][0]  contains sqrt of sum of squares of absolute errors of all contributions in  xlist[i_l] :
      ESD[i_l][j_o][0][0] = sqrt(ESD[i_l][j_o][0][0]);
      //  ESND[i_l][j_o][0][0]  contains sqrt of sum of squares of relative errors of all contributions in  xlist[i_l] :
      ESND[i_l][j_o][0][0] = sqrt(ESND[i_l][j_o][0][0]);

    }
  }



  // Output for extrapolated runtimes based on all possible reference cross sections:

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int j_o = 0; j_o < CiO[i_l].size(); j_o++){
      int x_o = CiO[i_l][j_o];
      logger.newLine(LOG_INFO);
      logger << LOG_INFO << left << "Output based on reference cross section  order_cross_section[" << x_o << " -> " << yorder[x_o]->resultdirectory << "] = " << order_cross_section[x_o] << endl;
    logger.newLine(LOG_INFO);
      for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	stringstream temp;
	temp << left << setw(25) << xlist[i_l]->xcontribution[i_c]->infix_path_contribution;
	temp << left << setw(20) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->name;
	temp << time_hms_from_double(ERT[i_l][j_o][i_c][0]) << "   ";
	temp << left << setw(16) << "";
	temp << right << setw(16) << setprecision(8) << ESND[i_l][j_o][i_c][0];
	temp << right << setw(16) << setprecision(8) << ESN[i_l][j_o][i_c][0];
	temp << right << setw(16) << setprecision(8) << ESD[i_l][j_o][i_c][0];
	logger << LOG_INFO << temp.str() << endl;
	if (i_c == 0){stringstream temp_line; for (int i_s = 0; i_s < 129; i_s++){temp_line << "-";} logger << LOG_INFO << temp_line.str() << endl;}
      }
      logger.newLine(LOG_INFO);
    }
  }



  //  Now use  ESD[i_l][j_o][0][0]  to distringuish which order j_o [x_o]  determines the actual runtime in  xlist[i_l]:

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    int y_o = -1;
    double min_ESD_selection = 1.e99;
    for (int j_o = 0; j_o < CiO[i_l].size(); j_o++){
      //      int x_o = CiO[i_l][j_o];
      if (ESD[i_l][j_o][0][0] < min_ESD_selection){y_o = j_o; min_ESD_selection = ESD[i_l][j_o][0][0];}
    }
    if (y_o == -1){logger << LOG_FATAL << "No reference cross section found! for  xlist[" << i_l << "] = " << xlist[i_l]->resultdirectory << endl; exit(1);}

    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      for (int i_p = 0; i_p < xlist[i_l]->xcontribution[i_c]->xsubprocess.size(); i_p++){
	xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime = ERT[i_l][y_o][i_c][i_p];
	xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event = ENE[i_l][y_o][i_c][i_p];
	xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation = ESD[i_l][y_o][i_c][i_p];
	xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization = ESN[i_l][y_o][i_c][i_p];
	xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation = ESND[i_l][y_o][i_c][i_p];

      }
    }

    int x_o = CiO[i_l][y_o];
    logger.newLine(LOG_INFO);
    logger << LOG_INFO << left << "Output based on *selected* reference cross section  order_cross_section[" << x_o << " -> " << yorder[x_o]->resultdirectory << "] = " << order_cross_section[x_o] << endl;
    logger.newLine(LOG_INFO);
    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      stringstream temp;
      temp << left << setw(25) << xlist[i_l]->xcontribution[i_c]->infix_path_contribution;
      temp << left << setw(20) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->name;
      temp << time_hms_from_double(ERT[i_l][y_o][i_c][0]) << "   ";
      temp << left << setw(16) << "";
      temp << right << setw(16) << setprecision(8) << ESND[i_l][y_o][i_c][0];
      temp << right << setw(16) << setprecision(8) << ESN[i_l][y_o][i_c][0];
      temp << right << setw(16) << setprecision(8) << ESD[i_l][y_o][i_c][0];

      //      logger << LOG_INFO << left << setw(25) << "folder" << setw(20) << "subprocess" << setw(21) << right << "runtime" << "   " << setw(12) << "n_event" << "   " << setw(16) << "deviation" << setw(16) << "norm" << setw(16) << "norm_err" << endl;

      logger << LOG_INFO << temp.str() << endl;
      if (i_c == 0){stringstream temp_line; for (int i_s = 0; i_s < 129; i_s++){temp_line << "-";} logger << LOG_INFO << temp_line.str() << endl;}
    }
    logger.newLine(LOG_INFO);

  }



  /*
  //  Old implementation:
  for (int i_l = 0; i_l < xlist.size(); i_l++){
    //    logger << LOG_INFO << "xlist[" << i_l << "]: " << xlist[i_l]->xcontribution[0]->infix_order_contribution << endl;
    vector<int> contribution_in_order;
    for (int i_o = 0; i_o < yorder.size(); i_o++){
      for (int j_l = 0; j_l < yorder[i_o]->contribution_file.size(); j_l++){
	if (i_l == mapping_contribution_file[yorder[i_o]->contribution_file[j_l]]){
	  contribution_in_order.push_back(i_o);
	  break;
	}
      }
    }
    for (int i_o = 0; i_o < contribution_in_order.size(); i_o++){logger << LOG_INFO << "contribution_in_order[" << i_o << "] = " << contribution_in_order[i_o] << endl;}


    int x_o = 0;

    double min_order_selection = 1.e99;
    for (int i_o = 0; i_o < contribution_in_order.size(); i_o++){
      double temp_order_selection = order_cross_section[contribution_in_order[i_o]] * order_relative_error[contribution_in_order[i_o]];
      logger << LOG_INFO << "order_selection[contribution_in_order[" << i_o << "] = " << contribution_in_order[i_o] << "] = " << setprecision(8) << setw(16) << temp_order_selection << "   o_c_s = " << setprecision(8) << setw(16) << order_cross_section[contribution_in_order[i_o]] << "   o_r_e = " << setprecision(8) << setw(16) << order_relative_error[contribution_in_order[i_o]] << endl;
      if (temp_order_selection < min_order_selection){
	min_order_selection = temp_order_selection;
	x_o = contribution_in_order[i_o];
      }
    }
    logger << LOG_INFO << "min_order_selection = " << min_order_selection << " @ x_o = " << x_o << endl;

    // reset to zero for xcontribution[0]->xsubprocess[0] component !!!
    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime = 0.;

    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation = 0.;
    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation = 0.;
    for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime = 0.;
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation = 0.;
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation = 0.;

      logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].type_contribution.type_correction = " << xlist[i_l]->xcontribution[i_c]->type_contribution << "." << xlist[i_l]->xcontribution[i_c]->type_correction << endl;

      for (int i_p = 1; i_p < xlist[i_l]->xcontribution[i_c]->xsubprocess.size(); i_p++){
	/*
	  cout << "i_c = " << i_c << "   i_p = " << i_p << endl;
	  cout << "xlist[" << i_l << "].extrapolated_runtime.size() = " << xlist[i_l]->extrapolated_runtime.size() << endl;
	  cout << "xlist[" << i_l << "].extrapolated_runtime[" << i_c << "].size() = " << xlist[i_l]->extrapolated_runtime[i_c].size() << endl;
	  cout << "xlist[" << i_l << "].error2_time.size() = " << xlist[i_l]->error2_time.size() << endl;
	  cout << "xlist[" << i_l << "].error2_time[" << i_c << "].size() = " << xlist[i_l]->error2_time[i_c].size() << endl;
	  cout << "xlist[" << i_l << "].extrapolated_n_event.size() = " << xlist[i_l]->extrapolated_n_event.size() << endl;
	  cout << "xlist[" << i_l << "].extrapolated_n_event[" << i_c << "].size() = " << xlist[i_l]->extrapolated_n_event[i_c].size() << endl;
	  cout << "xlist[" << i_l << "].extrapolated_sigma_deviation.size() = " << xlist[i_l]->extrapolated_sigma_deviation.size() << endl;
	  cout << "xlist[" << i_l << "].extrapolated_sigma_deviation[" << i_c << "].size() = " << xlist[i_l]->extrapolated_sigma_deviation[i_c].size() << endl;
  *//*
	int switch_zero_contribution = 0;


	if (osi->switch_reference == "TSV"){
	  logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_TSV[0][0] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	  //	  logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_CV[0][0] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	  if (xlist[i_l]->xcontribution[i_c]->active_qTcut && no_qTcut_runtime_estimate){
	    x_q = no_qTcut_runtime_estimate;
	  }
	  if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] == 0.){switch_zero_contribution = 1;}
	  logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_TSV[0][" << x_q << "][" << x_s << "][" << x_r << "][" << x_f << "] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	}
	else if (osi->switch_reference == "CV"){
	  // check why zero !!!
	  if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_CV[0][0] == 0.){switch_zero_contribution = 1;}
	}
	/*
	  if (osi->switch_CV){if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_CV[0][0] == 0.){switch_zero_contribution = 1;}}
	  if (osi->switch_TSV){
	  if (xlist[i_l]->xcontribution[i_c]->active_qTcut && no_qTcut_runtime_estimate){
	  x_q = no_qTcut_runtime_estimate;
	  }
	  if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] == 0.){switch_zero_contribution = 1;}
	  logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_TSV[0][" << x_q << "][" << x_s << "][" << x_r << "][" << x_f << "] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][x_q][x_s][x_r][x_f] << endl;
	  }
    *//*

	//	if (osi->switch_TSV){if (xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_TSV[0][0][0][0][0] == 0.){switch_zero_contribution = 1;}}

	if (osi->switch_CV){logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].result_CV[0][0] = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_CV[0][0] << "   switch_zero_contribution = " << switch_zero_contribution << endl;}

	logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "]:   switch_zero_contribution = " << switch_zero_contribution << endl;

	if (switch_zero_contribution){
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime = 0.;
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event = 0.;
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation = 0.;
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization = 0.;
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation = 0.;


	}
	else {
	  //  error2_time  seems to actually contain the sqrt of how it is named, i.e.  error * sqrt(time) !!!
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime = yorder[x_o]->error2_time * xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->error2_time / pow(order_relative_error[x_o] * order_cross_section[x_o], 2);

	  logger << LOG_DEBUG << "yorder[" << x_o << "].error2_time = " << yorder[x_o]->error2_time << endl;
	  logger << LOG_DEBUG << "order_cross_section[" << x_o << "] = " << order_cross_section[x_o] << endl;
	  logger << LOG_DEBUG << "order_relative_error[" << x_o << "] = " << order_relative_error[x_o] << endl;

	  logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].error2_time = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->error2_time << endl;

	  logger << LOG_DEBUG << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[" << i_p << "].extrapolated_runtime = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime << endl;

	  // xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime  contains the sum of the extrapolated runtimes of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime += xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime;

	  // xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event  converts  extrapolated_runtime  for each subprocess into  extrapolated_n_event  by using respective  time_per_event :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event = xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime / xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->time_per_event;

	  // xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation  states final absolute error of each subprocess :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation = xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->error2_time / sqrt(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime);

	  // xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation  states (temporarily) square of final absolute error of complete contribution  xlist[i_l]->xcontribution[i_c] :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation += pow(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation, 2);

	  //////

	  //  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization  contains reference cross section  order_cross_section[x_o]  (defined by order  x_o ):
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization = order_cross_section[x_o];

	  //  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation  contains square of relative error of each subprocess  wrt.  order_cross_section[x_o] :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation = xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation / order_cross_section[x_o];
	  //  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation  contains sum of squares of relative errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
	  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation += pow(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation, 2);
	}
      }

      //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime  contatins sum of runtimes of all contributions in  xlist[i_l] :
      xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime += xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime;

      //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization  contains  order_cross_section[x_o]  (defined by order  x_o ):
      xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization = order_cross_section[x_o];
      //  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization  contains  order_cross_section[x_o]  (defined by order  x_o ):
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization = order_cross_section[x_o];

      //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation  contains (temporarily) sum of  squares of absolute errors of all contributions in  xlist[i_l] :
      xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation += xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation;

      //  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation  contains sqrt of sum of absolute errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation = sqrt(xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation);

      //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation  contains (temporarily) sum of  squares of relative errors of all contributions in  xlist[i_l] :
      xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation += xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation;

      //  xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation  contains sqrt of sum of relative errors of all subprocesses in  xlist[i_l]->xcontribution[i_c] :
      xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation = sqrt(xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation);

      // ???
      xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_n_event = 0.;
    }
    // again ???
    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization = order_cross_section[x_o];

    //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation  contains sqrt of sum of squares of absolute errors of all contributions in  xlist[i_l] :
    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation = sqrt(xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_deviation);
    //  xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation  contains sqrt of sum of squares of relative errors of all contributions in  xlist[i_l] :
    xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation = sqrt(xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_sigma_normalization_deviation);

  }
      */

  //////////////////////////////////////////////////////
  //  Output information about extrapolated runtimes  //
  //////////////////////////////////////////////////////

  output_runtime_extrapolated();

  ////////////////////////////////////////////////////////////////////////////////
  //  Output information about extrapolated runtimes - input for MATRIX script  //
  ////////////////////////////////////////////////////////////////////////////////

  output_runtime_MATRIX();

  //////////////////////////////////////////////
  //  Output information about used runtimes  //
  //////////////////////////////////////////////

  output_runtime_used();
  output_runtime_used_new();

  /////////////////////////////////////////////////////////////////////
  //  Output for new run directories based on extrapolated runtimes  //
  /////////////////////////////////////////////////////////////////////

  if (switch_generate_rundirectories){generate_rundirectories();}

  logger << LOG_DEBUG << "finished" << endl;
}




void summary_generic::output_runtime_used(){
  Logger logger("summary_generic::output_runtime_used");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  string filename;
  double runtime_used_complete = 0.;
  for (int i_l = 0; i_l < xlist.size(); i_l++){runtime_used_complete += xlist[i_l]->xcontribution[0]->xsubprocess[0]->used_runtime;}

  ofstream outfile_used;
  filename = final_resultdirectory + "/info.used.runtime.txt";
  outfile_used.open(filename.c_str(), ofstream::out | ofstream::trunc);
  outfile_used << "Overview over contributions   " << setprecision(8) << setw(16) << runtime_used_complete / 24 / 60 / 60 << " d" << endl;
  for (int i_s = 0; i_s < 27; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;
  outfile_used << left << setw(25) << "folder" << setw(20) << "" << setw(21) << right << "runtime" << endl;
  outfile_used << endl;

  outfile_used << left << setw(25) << "complete";
  outfile_used << left << setw(20) << "";
  outfile_used << time_hms_from_double(runtime_used_complete) << "   ";
  outfile_used << endl;
  for (int i_s = 0; i_s < 66; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      stringstream temp_ext_runtime;
      outfile_used << left << setw(25) << xlist[i_l]->xcontribution[i_c]->infix_path_contribution;
      outfile_used << left << setw(20) << "";//xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->name;
      outfile_used << time_hms_from_double(xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->used_runtime) << "   ";
      outfile_used << endl;
      if (i_c == 0){for (int i_s = 0; i_s < 66; i_s++){outfile_used << "-";} outfile_used << endl;}
    }
    outfile_used << endl;
  }
  outfile_used << endl;
  outfile_used << "Overview over subprocesses" << endl;
  for (int i_s = 0; i_s < 26; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;
  outfile_used << left << setw(25) << "folder" << setw(20) << "subprocess" << setw(16) << right << "runtime" << "   " << setw(12) << "n_event" << "   " << setw(16) << "deviation" << setw(16) << "norm" << setw(16) << "norm_err" << endl;
  outfile_used << endl;
  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->output_used_runtime(outfile_used);}
  outfile_used.close();

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_runtime_used_new(){
  Logger logger("summary_generic::output_runtime_used_new");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  string filename;
  double runtime_used_complete = 0.;
  for (int i_l = 0; i_l < xlist.size(); i_l++){runtime_used_complete += xlist[i_l]->xcontribution[0]->xsubprocess[0]->used_runtime;}

  ofstream outfile_used;
  filename = final_resultdirectory + "/info.used.runtime.new.txt";
  outfile_used.open(filename.c_str(), ofstream::out | ofstream::trunc);
  outfile_used << "Overview over contributions   " << setprecision(8) << setw(16) << runtime_used_complete / 24 / 60 / 60 << " d" << endl;
  for (int i_s = 0; i_s < 27; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;
  outfile_used << left << setw(25) << "folder" << setw(20) << "" << setw(21) << right << "runtime" << endl;
  outfile_used << endl;

  outfile_used << left << setw(25) << "complete";
  outfile_used << left << setw(20) << "";
  outfile_used << time_hms_from_double(runtime_used_complete) << "   ";
  outfile_used << endl;
  for (int i_s = 0; i_s < 66; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      stringstream temp_ext_runtime;
      outfile_used << left << setw(25) << xlist[i_l]->xcontribution[i_c]->infix_path_contribution;
      outfile_used << left << setw(20) << "";//xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->name;
      outfile_used << time_hms_from_double(xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->used_runtime) << "   ";
      outfile_used << endl;
      if (i_c == 0){for (int i_s = 0; i_s < 66; i_s++){outfile_used << "-";} outfile_used << endl;}
    }
    outfile_used << endl;
  }
  outfile_used << endl;
  outfile_used << "Overview over subprocesses" << endl;
  for (int i_s = 0; i_s < 26; i_s++){outfile_used << "=";} outfile_used << endl;
  outfile_used << endl;
  outfile_used << left << setw(25) << "folder" << setw(20) << "subprocess" << setw(16) << right << "runtime" << "   " << setw(12) << "n_event" << "   " << setw(16) << "deviation" << setw(16) << "norm" << setw(16) << "norm_err" << endl;
  outfile_used << endl;
  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->output_used_runtime_new(outfile_used);}
  outfile_used.close();

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_runtime_extrapolated(){
  Logger logger("summary_generic::output_runtime_extrapolated");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  string filename;
  double runtime_extrapolated_complete = 0.;
  for (int i_l = 0; i_l < xlist.size(); i_l++){
    logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[0]->xsubprocess[0]->extrapolated_runtime = " << xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime << endl;
  }
  for (int i_l = 0; i_l < xlist.size(); i_l++){runtime_extrapolated_complete += xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime;}
  // check double-counting (of runtimes) around here !!!
  for (int i_l = 0; i_l < xlist.size(); i_l++){
    logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[0]->xsubprocess[0]->extrapolated_runtime = " << xlist[i_l]->xcontribution[0]->xsubprocess[0]->extrapolated_runtime << endl;
  }
  logger << LOG_INFO << "runtime_extrapolated_complete = " << runtime_extrapolated_complete << endl;

  ofstream outfile_extrapolated;
  filename = final_resultdirectory + "/info.extrapolated.runtime.txt";
  outfile_extrapolated.open(filename.c_str(), ofstream::out | ofstream::trunc);
  outfile_extrapolated << "Overview over contributions   " << setprecision(8) << setw(16) << runtime_extrapolated_complete / 24 / 60 / 60 << " d" << endl;
  for (int i_s = 0; i_s < 27; i_s++){outfile_extrapolated << "=";} outfile_extrapolated << endl;
  outfile_extrapolated << endl;
  outfile_extrapolated << left << setw(25) << "folder" << setw(20) << "" << setw(21) << right << "runtime" << "   " << setw(12) << "" << "   " << setw(15) << "deviation" << setw(15) << "norm" << setw(15) << "norm_err" << endl;
  outfile_extrapolated << endl;

  outfile_extrapolated << left << setw(25) << "complete";
  outfile_extrapolated << left << setw(20) << "";
  outfile_extrapolated << time_hms_from_double(runtime_extrapolated_complete) << "   ";

  outfile_extrapolated << endl;
  for (int i_s = 0; i_s < 129; i_s++){outfile_extrapolated << "=";} outfile_extrapolated << endl;
  outfile_extrapolated << endl;

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      //      logger << LOG_INFO << "xlist[" << i_l << "].xcontribution[" << i_c << "].xsubprocess[0]->extrapolated_runtime = " << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime << endl;
      outfile_extrapolated << left << setw(25) << xlist[i_l]->xcontribution[i_c]->infix_path_contribution;
      outfile_extrapolated << left << setw(20) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->name;
      outfile_extrapolated << time_hms_from_double(xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_runtime) << "   ";
      outfile_extrapolated << left << setw(12) << "" << "   ";
      outfile_extrapolated << right << setw(15) << setprecision(8) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization_deviation;
      outfile_extrapolated << right << setw(15) << setprecision(8) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_normalization;
      outfile_extrapolated << right << setw(15) << setprecision(8) << xlist[i_l]->xcontribution[i_c]->xsubprocess[0]->extrapolated_sigma_deviation;

      //      outfile_extrapolated << left << setw(25) << "folder" << setw(20) << "subprocess" << setw(21) << right << "runtime" << "   " << setw(12) << "n_event" << "   " << setw(16) << "deviation" << setw(16) << "norm" << setw(16) << "norm_err" << endl;

      outfile_extrapolated << endl;
      if (i_c == 0){for (int i_s = 0; i_s < 129; i_s++){outfile_extrapolated << "-";} outfile_extrapolated << endl;}
    }
    outfile_extrapolated << endl;
  }
  outfile_extrapolated << endl;
  outfile_extrapolated << "Overview over subprocesses" << endl;
  for (int i_s = 0; i_s < 26; i_s++){outfile_extrapolated << "=";} outfile_extrapolated << endl;
  outfile_extrapolated << endl;
  outfile_extrapolated << left << setw(25) << "folder" << setw(20) << "subprocess" << setw(21) << right << "runtime" << "   " << setw(12) << "n_event" << "   " << setw(15) << "deviation" << setw(15) << "norm" << setw(15) << "norm_err" << endl;
  outfile_extrapolated << endl;
  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->output_extrapolated_runtime(outfile_extrapolated);}
  outfile_extrapolated.close();

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::generate_rundirectories(){
  Logger logger("summary_generic::generate_rundirectories");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  int n_run_phasespace_optimization = phasespace_optimization.size();
  if (phasespace_optimization.size() > 1){n_run_phasespace_optimization = phasespace_optimization.size() - 1;}

  stringstream list_path;
  list_path << final_resultdirectory + "/newrundir";
  system_execute(logger, "mkdir " + list_path.str());

  string path_newrundir_result;
  path_newrundir_result =  final_resultdirectory + "/newrundir/result";
  system_execute(logger, "mkdir " + path_newrundir_result);

  list_path << "/start";
  for (int i_m = 0; i_m < n_run_phasespace_optimization; i_m++){
    system_execute(logger, "mkdir " + list_path.str() + phasespace_optimization[i_m]);
  }

  string path_newrundir_result_infile_runs;
  path_newrundir_result_infile_runs = final_resultdirectory + "/newrundir/result/infile.runs";
  for (int i_m = 0; i_m < phasespace_optimization.size(); i_m++){
    system_execute(logger, "mkdir " + path_newrundir_result_infile_runs + phasespace_optimization[i_m]);
  }


  vector<int> counter_complete_process(n_run_phasespace_optimization, 0);
  vector<vector<int> > counter_list_process(n_run_phasespace_optimization, vector<int> (xlist.size() , 0));
  vector<vector<vector<int> > > counter_contribution_process(n_run_phasespace_optimization, vector<vector<int> > (xlist.size()));

  for (int i_m = 0; i_m < n_run_phasespace_optimization; i_m++){
    ofstream out_runscript_complete;
    string runscript_complete_name = final_resultdirectory + "/newrundir" + "/MUNICH.complete" + phasespace_optimization[i_m] + ".start.sh";

    logger << LOG_DEBUG << "runscript_complete_name = " << runscript_complete_name << endl;
    out_runscript_complete.open(runscript_complete_name.c_str(), ofstream::out | ofstream::trunc);

    ofstream out_processlist_complete;
    string runlist_complete_name = final_resultdirectory + "/newrundir" + "/list.run.subprocesses.complete" + phasespace_optimization[i_m] + ".txt";
    out_processlist_complete.open(runlist_complete_name.c_str(), ofstream::out | ofstream::trunc);


    //      int counter_complete_process = 0;
    for (int i_l = 0; i_l < xlist.size(); i_l++){
      ofstream out_processlist_list;
      string name_processscript_list = final_resultdirectory + "/newrundir" + "/start" + phasespace_optimization[i_m] + "/list.run.subprocesses." + xlist[i_l]->xcontribution[0]->infix_order_contribution + ".txt";
      logger << LOG_DEBUG << "name_processscript_list = " << name_processscript_list << endl;
      out_processlist_list.open(name_processscript_list.c_str(), ofstream::out | ofstream::trunc);

      ofstream out_runscript_list;
      string name_runscript_list = final_resultdirectory + "/newrundir/start" + phasespace_optimization[i_m] + "/MUNICH." + xlist[i_l]->xcontribution[0]->infix_order_contribution + ".start.sh";
      out_runscript_list.open(name_runscript_list.c_str(), ofstream::out | ofstream::trunc);
      logger << LOG_DEBUG << "name_runscript_list = " << name_runscript_list << endl;

      //	int counter_list_process = 0;
      //	vector<int> counter_list_process(n_run_phasespace_optimization, 0);
      counter_contribution_process[i_m][i_l].resize(xlist[i_l]->xcontribution.size(), 0);
      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	//	  vector<int> counter_contribution_process(n_run_phasespace_optimization, 0);
	//	  int counter_contribution_process = 0;
	xlist[i_l]->xcontribution[i_c]->output_extrapolated_runtime_directories(i_m, counter_contribution_process[i_m][i_l][i_c]);
	//out_runscript_list, out_processlist_list
	out_processlist_list << right << setw(8) << "" << setw(30) << left << xlist[i_l]->xcontribution[i_c]->infix_order_contribution + phasespace_optimization[i_m] << "     " << right << setw(5) << counter_contribution_process[i_m][i_l][i_c] << " ( " << setw(5) << xlist[i_l]->xcontribution[i_c]->max_number_of_jobs_for_one_channel << " )" << endl;
	out_runscript_list << "./start" + phasespace_optimization[i_m] + "/MUNICH." + xlist[i_l]->xcontribution[i_c]->infix_order_contribution + ".start.sh" << endl;
	counter_list_process[i_m][i_l] += counter_contribution_process[i_m][i_l][i_c];
      }
      out_processlist_list << right << setw(8) << "========" << setw(30) << "==============================" << "=====" << "=====" << "===" << "=====" << "==" << endl;
      out_processlist_list << right << setw(8) << "" << setw(30) << left << xlist[i_l]->xcontribution[0]->infix_order_contribution + phasespace_optimization[i_m] << "     " << right << setw(5) << counter_list_process[i_m][i_l] << endl;
      out_runscript_list.close();
      system_execute(logger, "chmod 744 " + name_runscript_list);
      out_processlist_list.close();

      out_runscript_complete << "./start" + phasespace_optimization[i_m] + "/MUNICH." << xlist[i_l]->xcontribution[0]->infix_order_contribution << ".start.sh" << endl;
      out_processlist_complete << right << setw(8) << "" << setw(30) << left << xlist[i_l]->xcontribution[0]->infix_order_contribution + phasespace_optimization[i_m] << "     " << right << setw(5) << counter_list_process[i_m][i_l] << endl;
      counter_complete_process[i_m] += counter_list_process[i_m][i_l];
    }

    out_runscript_complete.close();
    system_execute(logger, "chmod 744 " + runscript_complete_name);
    out_processlist_complete << right << setw(8) << "========" << setw(30) << "==============================" << "=====" << "=====" << endl;
    out_processlist_complete << right << setw(8) << "" << setw(30) << left << "complete" << "     " << right << setw(5) << counter_complete_process[i_m] << endl;
    out_processlist_complete.close();
  }


  for (int i_m = 0; i_m < phasespace_optimization.size(); i_m++){
    for (int i_l = 0; i_l < xlist.size(); i_l++){
      ofstream out_infile_result_sub;
      string infile_result_sub_name = path_newrundir_result_infile_runs + phasespace_optimization[i_m] + "/" + xlist[i_l]->xcontribution[0]->infix_order_contribution + ".dat";
      out_infile_result_sub.open(infile_result_sub_name.c_str(), ofstream::out | ofstream::trunc);
      write_infile_string(out_infile_result_sub, "processname", xlist[i_l]->processname);
      out_infile_result_sub << endl;
      write_infile_string(out_infile_result_sub, "resultdirectory", xlist[i_l]->xcontribution[0]->infix_order_contribution);
      write_infile_string(out_infile_result_sub, "type_perturbative_order", xlist[i_l]->type_perturbative_order);
      if (xlist[i_l]->type_subtraction_method == ""){write_infile_string(out_infile_result_sub, "subtraction_method", "---");}
      else {write_infile_string(out_infile_result_sub, "subtraction_method", xlist[i_l]->type_subtraction_method);}
      //	  write_infile_string(out_infile_result_sub, "contribution_order", generic_parameter.advanced_directory[j_co][i_sm][j_o]);
      //  if (generic_parameter.advanced_directory[j_co][i_sm][j_o].substr(0, 1) == "a"){
      write_infile_int(out_infile_result_sub, "contribution_order_alpha_s", xlist[i_l]->in_contribution_order_alpha_s);
      write_infile_int(out_infile_result_sub, "contribution_order_alpha_e", xlist[i_l]->in_contribution_order_alpha_e);
      write_infile_int(out_infile_result_sub, "photon_induced", xlist[i_l]->photon_induced);
      out_infile_result_sub << endl;

      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	write_infile_string(out_infile_result_sub, "type_contribution", xlist[i_l]->xcontribution[i_c]->type_contribution);
	if (xlist[i_l]->xcontribution[i_c]->type_correction == ""){write_infile_string(out_infile_result_sub, "type_correction", "---");}
	else {write_infile_string(out_infile_result_sub, "type_correction", xlist[i_l]->xcontribution[i_c]->type_correction);}
	write_infile_int(out_infile_result_sub, "interference", xlist[i_l]->xcontribution[i_c]->interference);

	if (phasespace_optimization[i_m] == ".combined"){
	  for (int j_z = 0; j_z < phasespace_optimization.size() - 1; j_z++){
	    stringstream temp_ss;
	    temp_ss << "start" << phasespace_optimization[j_z] << "/runlist." << xlist[i_l]->xcontribution[i_c]->infix_order_contribution << ".dat";
	    string temp_listname = "directory_list";
	    if (phasespace_optimization[j_z] != ""){
	      temp_listname = temp_listname + " " + phasespace_optimization[j_z].substr(1, phasespace_optimization[j_z].size() - 1);
	    }
	    write_infile_string(out_infile_result_sub, temp_listname, temp_ss.str());

	  }
	}
	else {
	  stringstream temp_ss;
	  temp_ss << "start" << phasespace_optimization[i_m] << "/runlist." << xlist[i_l]->xcontribution[i_c]->infix_order_contribution << ".dat";
	  string temp_listname = "directory_list";
	  if (phasespace_optimization[i_m] != ""){
	    temp_listname = temp_listname + " " + phasespace_optimization[i_m].substr(1, phasespace_optimization[i_m].size() - 1);
	  }
	  write_infile_string(out_infile_result_sub, temp_listname, temp_ss.str());
	}

	out_infile_result_sub << endl;
      }

      out_infile_result_sub.close();
    }

  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_runtime_MATRIX(){
  Logger logger("summary_generic::output_runtime_MATRIX");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  string filename;
  ofstream outfile_readin_extrapolated;
  filename = "./runtime.dat";
  outfile_readin_extrapolated.open(filename.c_str(), ofstream::out | ofstream::trunc);
  outfile_readin_extrapolated << left << setw(25) << "# folder" << setw(20) << "subprocess" << setw(12) << right << "runtime(s)" << "   " << setw(12) << "n_event" << "   " << setw(15) << "deviation" << setw(15) << "norm" << setw(15) << "norm_err" << endl;
  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->output_readin_extrapolated_runtime(outfile_readin_extrapolated);}
  outfile_readin_extrapolated.close();

  logger << LOG_DEBUG << "finished" << endl;
}

