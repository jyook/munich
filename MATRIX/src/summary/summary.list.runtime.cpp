#include "header.hpp"

void summary_list::calculate_runtime(){
  Logger logger("summary_list::calculate_runtime");
  logger << LOG_DEBUG << "called" << endl;

  //  logger << LOG_INFO << "xcontribution[0]->xsubprocess.size() = " << xcontribution[0]->xsubprocess.size() << endl;
  //  logger << LOG_INFO << "xcontribution[0]->xsubprocess[0]->error2_time = " << xcontribution[0]->xsubprocess[0]->error2_time << "   --- should be 0 here !!!" << endl;

  // reset to zero for xcontribution[0]->xsubprocess[0] component !!!
  xcontribution[0]->xsubprocess[0]->used_runtime = 0.;
  xcontribution[0]->xsubprocess[0]->used_n_event = 0;
  xcontribution[0]->xsubprocess[0]->error2_time = 0.;

  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    xcontribution[0]->xsubprocess[0]->error2_time += xcontribution[i_c]->xsubprocess[0]->error2_time;

    //    logger << LOG_INFO << "SUMRUNTIME   contribution   " << setw(15) << xcontribution[0]->xsubprocess[0]->name << "   error2_time = " << xcontribution[0]->xsubprocess[0]->error2_time << endl;

    //    logger << LOG_INFO << "xcontribution[" << i_c << "].infix_order_contribution = " << xcontribution[i_c]->infix_order_contribution << endl;
    /*
    for (int i_p = 0; i_p < xcontribution[i_c]->subprocess.size(); i_p++){
      logger << LOG_INFO << "convergence[" << i_c << "][" << i_p << "] = " << setw(20) << xcontribution[i_c]->subprocess[i_p] << "   list_error2_time = " << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->error2_time << "   list_time_per_event = " <<  xcontribution[i_c]->xsubprocess[i_p]->time_per_event << endl;
    }
    */
  }
  /*
  for (int i_c = 0; i_c < xcontribution.size(); i_c++){
    logger << LOG_INFO << setw(10) << xcontribution[i_c]->infix_order_contribution << ": " << "   xcontribution[" << i_c << "].xsubprocess[0]->error2_time = " << setw(20) << xcontribution[i_c]->xsubprocess[0]->error2_time << endl;
  }
  */
  /*
  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    for (int i_p = 0; i_p < xcontribution[i_c]->xsubprocess.size(); i_p++){
      logger << LOG_DEBUG << setw(20) << xcontribution[i_c]->infix_order_contribution << "[" << i_c << "][" << setw(2) << i_p << "] = " << setw(20) << xcontribution[i_c]->subprocess[i_p] << "   used_runtime = " << setprecision(3) << setw(10) << xcontribution[i_c]->xsubprocess[i_p]->used_runtime / 3600 << " h" << endl;
    }
    logger << LOG_DEBUG << endl;
  }
  */
  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    xcontribution[0]->xsubprocess[0]->used_runtime += xcontribution[i_c]->xsubprocess[0]->used_runtime;
    xcontribution[0]->xsubprocess[0]->used_n_event += xcontribution[i_c]->xsubprocess[0]->used_n_event;
  }

  for (int i_c = 0; i_c < xcontribution.size(); i_c++){
    logger << LOG_INFO << setw(20) << xcontribution[i_c]->infix_order_contribution << " - xcontribution[" << i_c << "]:   used_runtime = " << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[0]->used_runtime << "   used_n_event = " << setw(15) << xcontribution[i_c]->xsubprocess[0]->used_n_event << "   error2_time = " << setw(20) << xcontribution[i_c]->xsubprocess[0]->error2_time << endl;
  }

  /*
  logger << LOG_DEBUG << "      used_runtime = " << setprecision(3) << setw(10) << xcontribution[0]->xsubprocess[0]->used_runtime / 3600 << " h" << endl;
  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    logger << LOG_DEBUG << "[" << i_c << "]   used_runtime = " << setprecision(3) << setw(10) << xcontribution[i_c]->xsubprocess[0]->used_runtime / 3600 << " h" << endl;
  }
  */

  logger << LOG_DEBUG << "finished" << endl;
}





void summary_list::output_readin_extrapolated_runtime(ofstream & outfile_readin_extrapolated){
  Logger logger("summary_list::output_readin_extrapolated_runtime");
  logger << LOG_DEBUG << "called" << endl;

  /////////////////////////////////////////////////////////////
  //  extrapolated runtime - readin.extrapolated.runtime.dat //
  /////////////////////////////////////////////////////////////


  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    for (int i_p = 1; i_p < xcontribution[i_c]->xsubprocess.size(); i_p++){
      for (int i_m = 0; i_m < ygeneric->phasespace_optimization.size(); i_m++){
	if (ygeneric->phasespace_optimization[i_m] == ".combined"){continue;}
 	string temp_directory = xcontribution[i_c]->infix_path_contribution + ygeneric->phasespace_optimization[i_m];
	outfile_readin_extrapolated << left << setw(25) << temp_directory;
	outfile_readin_extrapolated << left << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->name;
	outfile_readin_extrapolated << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime) << "   ";
	outfile_readin_extrapolated << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event) << "   ";
	outfile_readin_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation;
	outfile_readin_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization;
	outfile_readin_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation;
	outfile_readin_extrapolated << endl;
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_list::output_extrapolated_runtime(ofstream & outfile_extrapolated){
  Logger logger("summary_list::output_extrapolated_runtime");
  logger << LOG_DEBUG << "called" << endl;

  ///////////////////////////////////////////////////////////
  //  extrapolated runtime - info.extrapolated.runtime.txt //
  ///////////////////////////////////////////////////////////

  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    for (int i_p = 0; i_p < xcontribution[i_c]->xsubprocess.size(); i_p++){
      outfile_extrapolated << left << setw(25) << xcontribution[i_c]->infix_path_contribution;
      outfile_extrapolated << left << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->name;
      logger << LOG_WARN << "time_hms_from_double(xcontribution[" << i_c << "] = " << xcontribution[i_c]->infix_contribution << "->xsubprocess[" << i_p << "] = " << xcontribution[i_c]->xsubprocess[i_p]->name << "->extrapolated_runtime) = " << time_hms_from_double(xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime) << endl;
      outfile_extrapolated << time_hms_from_double(xcontribution[i_c]->xsubprocess[i_p]->extrapolated_runtime) << "   ";
      if (i_p == 0){
	outfile_extrapolated << endl;
	for (int i_s = 0; i_s < 129; i_s++){outfile_extrapolated << "-";} outfile_extrapolated << endl;
      }
      else {
	outfile_extrapolated << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->extrapolated_n_event) << "   ";
	outfile_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_deviation;
	outfile_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization;
	outfile_extrapolated << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_deviation;
	outfile_extrapolated << endl;
      }
    }
    outfile_extrapolated << endl;
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_list::output_used_runtime(ofstream & outfile_used){
  Logger logger("summary_list::output_used_runtime");
  logger << LOG_DEBUG << "called" << endl;

  ///////////////////////////////////////////
  //  used runtime - info.used.runtime.txt //
  ///////////////////////////////////////////

  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    for (int i_p = 0; i_p < xcontribution[i_c]->xsubprocess.size(); i_p++){
      outfile_used << left << setw(25) << xcontribution[i_c]->infix_path_contribution;
      outfile_used << left << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->name;
      outfile_used << time_hms_from_double(xcontribution[i_c]->xsubprocess[i_p]->used_runtime) << "   ";
      if (i_p == 0){
	outfile_used << endl;
	for (int i_s = 0; i_s < 129; i_s++){outfile_used << "-";} outfile_used << endl;
      }
      else {
	outfile_used << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->used_n_event) << "   ";
	outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_normalization_deviation;
	outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization;
	outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_deviation;
	outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->error2_time;
	outfile_used << endl;
      }
    }
    outfile_used << endl;
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_list::output_used_runtime_new(ofstream & outfile_used){
  Logger logger("summary_list::output_used_runtime");
  logger << LOG_DEBUG << "called" << endl;

  ///////////////////////////////////////////
  //  used runtime - info.used.runtime.txt //
  ///////////////////////////////////////////

  for (int i_c = 1; i_c < xcontribution.size(); i_c++){
    bool switch_mapping = false;
    if (xcontribution[i_c]->extended_directory.size() > 1){switch_mapping = true;}
    for (int i_p = 0; i_p < xcontribution[i_c]->xsubprocess.size(); i_p++){
      outfile_used << left << setw(25) << xcontribution[i_c]->infix_path_contribution;
      outfile_used << left << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->name;
      if (switch_mapping){outfile_used << left << setw(10) << "combined";}

      outfile_used << time_hms_from_double(xcontribution[i_c]->xsubprocess[i_p]->used_runtime) << "   ";
      if (i_p == 0 || switch_mapping){
	outfile_used << endl;
	for (int i_s = 0; i_s < 129; i_s++){outfile_used << "-";} outfile_used << endl;
      }
      else {
	outfile_used << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->used_n_event) << "   ";
	outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_normalization_deviation;
	outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization;
	outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_deviation;
	outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->error2_time;
	outfile_used << endl;
      }
      if (switch_mapping){
	for (int i_m = 0; i_m < xcontribution[i_c]->extended_directory.size(); i_m++){
	  /*
	    outfile_used << "xcontribution[" << i_c << "]->extended_directory.size() = " << xcontribution[i_c]->extended_directory.size() << endl;
	    outfile_used << "xcontribution[" << i_c << "]->extended_directory_name.size() = " << xcontribution[i_c]->extended_directory_name.size() << endl;
	    outfile_used << "xcontribution[" << i_c << "]->xsubprocess[" << i_p << "]->used_n_event_group.size() = " << xcontribution[i_c]->xsubprocess[i_p]->used_n_event_group.size() << endl;
	    outfile_used << "xcontribution[" << i_c << "]->xsubprocess[" << i_p << "]->error2_time_group.size() = " << xcontribution[i_c]->xsubprocess[i_p]->error2_time_group.size() << endl;
	  */
	  outfile_used << left << setw(25) << xcontribution[i_c]->infix_path_contribution;
	  outfile_used << left << setw(20) << xcontribution[i_c]->xsubprocess[i_p]->name;
	  outfile_used << left << setw(10) << xcontribution[i_c]->extended_directory_name[i_m];
	  outfile_used << time_hms_from_double(xcontribution[i_c]->xsubprocess[i_p]->used_runtime_group[i_m]) << "   ";
	  if (i_p == 0){
	    outfile_used << endl;
	    if (i_m == xcontribution[i_c]->extended_directory.size() - 1){for (int i_s = 0; i_s < 129; i_s++){outfile_used << "=";} outfile_used << endl;}
	  }
	  else {
	    outfile_used << right << setw(12) << (long long)(xcontribution[i_c]->xsubprocess[i_p]->used_n_event_group[i_m]) << "   ";
	    outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_normalization_deviation;
	    outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization;
	    /////	  outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->extrapolated_sigma_normalization_group[i_m];
	    outfile_used << right << setw(15) << setprecision(8) << "";//xcontribution[i_c]->xsubprocess[i_p]->used_sigma_deviation;
	    outfile_used << right << setw(15) << setprecision(8) << xcontribution[i_c]->xsubprocess[i_p]->error2_time_group[i_m];
	    outfile_used << endl;
	  }
	  if (i_m == xcontribution[i_c]->extended_directory.size() - 1){outfile_used << endl;}
	}
      }
    }
    if (switch_mapping){for (int i_s = 0; i_s < 129; i_s++){outfile_used << "=";} outfile_used << endl;}
    outfile_used << endl;
  }

  logger << LOG_DEBUG << "finished" << endl;
}
