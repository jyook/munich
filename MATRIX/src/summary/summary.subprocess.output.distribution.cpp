#include "header.hpp"

void summary_subprocess::output_distribution_qTcut_TSV(){
  Logger logger("summary_subprocess::output_distribution_qTcut_TSV");
  logger << LOG_DEBUG << "called" << endl;

  //  if (!active_qTcut){return;}
  int local_n_qTcut = 1;
  if (ycontribution->active_qTcut){local_n_qTcut = ycontribution->selection_n_qTcut;}

  int plot_mode = 0;
  string name_result = ycontribution->infix_order_contribution + "." + name;

  for (int x_q = 0; x_q < local_n_qTcut; x_q++){
    string subdirectory = "";
    if (ycontribution->active_qTcut){
      stringstream qTcut_ss;
      qTcut_ss << "qTcut-" << ygeneric->osi->value_qTcut_distribution[x_q];
      subdirectory = "/" + ycontribution->ylist->resultdirectory + "/" + ycontribution->infix_contribution + "/" + name + "/" + qTcut_ss.str();
    }
    else {subdirectory = "/" + ycontribution->ylist->resultdirectory + "/" + ycontribution->infix_contribution + "/" + name;}

    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
      if (!ygeneric->osi->switch_distribution_TSV[i_s]){continue;}
      for (int i_r = 0; i_r < ygeneric->osi->n_scale_ren_TSV[i_s]; i_r++){
	for (int i_f = 0; i_f < ygeneric->osi->n_scale_fact_TSV[i_s]; i_f++){
	  string directory_distribution_plot = ygeneric->scalename_TSV[i_s][i_r][i_f] + "/" + subdirectory;
	  //	  string directory_distribution_plot = ygeneric->scalename_TSV[i_s][i_r][i_f] + "/" + qTcut_ss.str();
	  logger << LOG_INFO << "mkdir " << directory_distribution_plot << endl;
	  system_execute(logger, "mkdir " + directory_distribution_plot);
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < ygeneric->osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	string identifier = "";
	identifier = "norm";
	ygeneric->output_sddistribution_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "plot";
	ygeneric->output_sddistribution_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < ygeneric->osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	if (i_d < ygeneric->osi->dat.size()){continue;}
	string identifier = "";
	identifier = "norm";
	ygeneric->output_dddistribution_reconstruct_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_reconstruct_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "plot";
	ygeneric->output_dddistribution_reconstruct_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_reconstruct_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < ygeneric->osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	if (i_d < ygeneric->osi->dat.size()){continue;}
	string identifier = "";
	identifier = "norm.norm";
	ygeneric->output_dddistribution_split_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_split_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "norm.plot";
	ygeneric->output_dddistribution_split_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_split_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "plot.norm";
	ygeneric->output_dddistribution_split_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_split_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "plot.plot";
	ygeneric->output_dddistribution_split_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_split_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < ygeneric->osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	if (i_d < ygeneric->osi->dat.size()){continue;}
	string identifier = "";
	identifier = "norm";
	ygeneric->output_dddistribution_ge_lt_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_ge_lt_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	identifier = "plot";
	ygeneric->output_dddistribution_ge_lt_in_first_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
	ygeneric->output_dddistribution_ge_lt_in_second_sdd_TSV(identifier, distribution_result_qTcut_TSV[x_q][i_g][i_d], distribution_deviation_qTcut_TSV[x_q][i_g][i_d], i_d, name_result, subdirectory, plot_mode);
      }
    }

  }

  logger << LOG_DEBUG << "finished" << endl;
}





