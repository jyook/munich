#include "header.hpp"

summary_contribution::summary_contribution(){}

summary_contribution::summary_contribution(string _type_contribution, summary_list * _list){
  Logger logger("summary_contribution::summary_contribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization(_type_contribution, _list);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_contribution::initialization(string _type_contribution, summary_list * _list){
  Logger logger("summary_contribution::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ylist = _list;
  ygeneric = ylist->ygeneric;
  osi = ygeneric->osi;

  logger << LOG_INFO << "ylist->resultdirectory = " << ylist->resultdirectory << endl;

  type_contribution = _type_contribution;

  // ???
  subprocess.resize(1);
  xsubprocess.resize(1);
  subgroup_no_member.resize(1);
  //

  in_contribution_order_alpha_s = 0;
  in_contribution_order_alpha_e = 0;
  interference = 0;
  photon_induced = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_contribution::readin_contribution_remove_run(){
  Logger logger("summary_contribution::readin_contribution_remove_run");
  //  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_seed = directory.size();
  int default_size_CV = 1 + 2;
  if (active_qTcut){default_size_CV += 2 * osi->n_qTcut * osi->n_scales_CV;}
  else {default_size_CV += 2 * osi->n_scales_CV;}

  xsubprocess.resize(subprocess.size());
  for (int i_p = 0; i_p < xsubprocess.size(); i_p++){xsubprocess[i_p]->initialization(subprocess[i_p], n_seed, default_size_CV, this);}

  //  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


