#include "header.hpp"

void summary_contribution::readin_result_contribution_CV(string result_moment, int x_m){
  Logger logger("summary_contribution::readin_result_contribution_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger.newLine(LOG_INFO);
  logger << LOG_DEBUG << "readin_result_contribution_CV started:" << endl;
  logger << LOG_DEBUG << "ylist->resultdirectory = " << ylist->resultdirectory << endl;
  logger << LOG_DEBUG << "type_perturbative_order = " << type_perturbative_order << endl;
  logger << LOG_DEBUG << "type_subtraction_method = " << type_subtraction_method << endl;
  logger << LOG_DEBUG << "in_contribution_order_alpha_s = " << in_contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "in_contribution_order_alpha_e = " << in_contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "type_contribution = " << type_contribution << endl;
  logger << LOG_DEBUG << "type_correction = " << type_correction << endl;
  logger << LOG_DEBUG << "interference = " << interference << endl;
  logger << LOG_DEBUG << "photon_induced = " << photon_induced << endl;
  logger << LOG_DEBUG << "infix_contribution = " << infix_contribution << endl;

  logger << LOG_INFO << "infix_order_contribution = " << infix_order_contribution << endl;

  for (int i_d = 0; i_d < directory.size(); i_d++){logger << LOG_DEBUG << "directory[" << setw(3) << "] = " << directory[i_d] << endl;}
  logger << LOG_DEBUG << "xsubprocess.size() = " << xsubprocess.size() << endl;
  for (int i_p = 1; i_p < xsubprocess.size(); i_p++){logger << LOG_DEBUG << "xsubprocess[" << setw(3) << "] = " << xsubprocess[i_p]->name << endl;}
  logger << LOG_DEBUG << "ygeneric->subgroup.size() = " << ygeneric->subgroup.size() << endl;
  for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG << "ygeneric->subgroup[" << setw(3) << "] = " << ygeneric->subgroup[i_g] << endl;}
  logger << LOG_DEBUG << "subgroup_no_member = " << subgroup_no_member.size() << endl;
  logger << LOG_DEBUG << "ygeneric->subgroup.size() = " << ygeneric->subgroup.size() << endl;
  logger << LOG_DEBUG << "result_moment = " << result_moment << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->n_qTcut        = " << osi->n_qTcut << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->min_qTcut     = " << osi->min_qTcut << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->value_qTcut.size()     = " << osi->value_qTcut.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->n_scales_CV    = " << osi->n_scales_CV << endl;
  //osi->value_qTcut


  int default_size_CV =  1 + 2;
  if (active_qTcut){default_size_CV += 2 * osi->n_qTcut * osi->n_scales_CV;}
  else {default_size_CV += 2 * osi->n_scales_CV;}

  logger << LOG_DEBUG_VERBOSE << "default_size_CV = " << default_size_CV << endl;



  result_CV.resize(osi->n_moments, vector<vector<vector<double> > > (ygeneric->subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));
  deviation_CV.resize(osi->n_moments, vector<vector<vector<double> > > (ygeneric->subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));


  for (int i_p = 1; i_p < xsubprocess.size(); i_p++){xsubprocess[i_p]->readin_result_CV(result_moment);}

  ///////////////////////////////////////////////////////////////
  //  combination of different-seed runs for all subprocesses  //
  ///////////////////////////////////////////////////////////////

  for (int i_p = 1; i_p < xsubprocess.size(); i_p++){
    logger << LOG_DEBUG_VERBOSE << left << xsubprocess[i_p]->name << ":" << endl;
    // ???
    xsubprocess[i_p]->combine_result_alternative();
    //    xsubprocess[i_p]->combine_result_original(oset);
  }

  /////////////////////////////////////////////////////////////////////////////////
  //  combination of different-seed runs for all subprocesses with CV variation  //
  /////////////////////////////////////////////////////////////////////////////////

  for (int i_p = 1; i_p < xsubprocess.size(); i_p++){
    logger << LOG_DEBUG_VERBOSE << left << xsubprocess[i_p]->name << ":" << endl;
    xsubprocess[i_p]->combination_result_CV(result_moment);
  }

  for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
    for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
      xsubprocess[0]->result_CV[i_q][i_s] = 0.;
      double dev = 0.;
      for (int i_p = 1; i_p < xsubprocess.size(); i_p++){
	xsubprocess[0]->result_CV[i_q][i_s] += xsubprocess[i_p]->result_CV[i_q][i_s];
	dev += pow(xsubprocess[i_p]->deviation_CV[i_q][i_s], 2);
      }
      xsubprocess[0]->deviation_CV[i_q][i_s] = sqrt(dev);
    }
  }

  ///////////////////////////////////////////////////////
  //  calculation of subgroup results in CV variation  //
  ///////////////////////////////////////////////////////

  for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
    for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
      for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
	double dev = 0.;
	for (int i_m = 0; i_m < subgroup_no_member[i_g].size(); i_m++){
	  result_CV[x_m][i_g][i_q][i_s] += xsubprocess[subgroup_no_member[i_g][i_m]]->result_CV[i_q][i_s];
	  dev += pow(xsubprocess[subgroup_no_member[i_g][i_m]]->deviation_CV[i_q][i_s], 2);
	}
	deviation_CV[x_m][i_g][i_q][i_s] = sqrt(dev);
      }
    }
  }

  
  ////////////////////
  //  output begin  // ???
  ////////////////////

  if ((x_m == 0 || x_m == osi->n_moments)){// || (infix_order_contribution == "LO")){
    if (ygeneric->switch_output_subprocess > 1){
      string filename;
      ofstream outfile_moment;

      if (x_m == osi->n_moments){filename = "" + ygeneric->final_resultdirectory + "/CV/" + ygeneric->name_variation_CV + "/" + ylist->resultdirectory + "/" + infix_order_contribution + "/group/plot.qTcut." + infix_order_contribution + ".dat";}
      else {filename = "" + ygeneric->final_resultdirectory + "/CV/" + ygeneric->name_variation_CV + "/" + ylist->resultdirectory + "/" + infix_order_contribution + "/group/plot." + result_moment + ".qTcut." + infix_order_contribution + ".dat";}
      logger << LOG_DEBUG << "Output : xfilename = " << filename << endl;
      outfile_moment.open(filename.c_str(), ofstream::out | ofstream::trunc);
      outfile_moment << showpoint;
      for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	outfile_moment << noshowpoint << setw(10) << setprecision(4) << osi->value_qTcut[i_q];
	for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
	  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	    //	    outfile_moment << setw(15) << setprecision(8) << result_CV[x_m][i_g][i_q][i_s] << setw(15) << setprecision(8) << deviation_CV[x_m][i_g][i_q][i_s];
	    outfile_moment << setw(15) << setprecision(8) << osi->unit_factor_result * result_CV[x_m][i_g][i_q][i_s] << setw(15) << setprecision(8) << osi->unit_factor_result * deviation_CV[x_m][i_g][i_q][i_s];
	  }
	}
	outfile_moment << endl;
      }
      outfile_moment.close();
      logger << LOG_DEBUG << filename << " finished." << endl;
    }
  }


  logger << LOG_DEBUG << "output osi->min_qTcut variation finished" << endl;

  //  if ((x_m == 0 || x_m == osi->n_moments)){
  if (ygeneric->switch_output_overview > 2){output_result_overview_qTcut_CV();}
  if (ygeneric->switch_output_result > 2){output_result_qTcut_CV();}
  if (ygeneric->switch_output_plot > 2){output_result_plot_qTcut_CV();}
  if (ygeneric->switch_output_plot > 2){output_result_plot_CV();}

  if (ygeneric->switch_output_plot > 3){output_subprocesses_result_plot_qTcut_CV();}
  if (ygeneric->switch_output_plot > 3){output_subprocesses_result_plot_CV();}
  //output_result_check_CV(result_moment, x_m);
  //  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_contribution::readin_result_contribution_TSV(string result_moment, int x_m){
  Logger logger("summary_contribution::readin_result_contribution_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "infix_order_contribution = " << infix_order_contribution << endl;

  // only debug output -> temporary !!!
  logger.newLine(LOG_DEBUG);
  logger << LOG_DEBUG << "readin_result_contribution_TSV started:" << endl;
  logger << LOG_DEBUG << "type_perturbative_order = " << type_perturbative_order << endl;
  logger << LOG_DEBUG << "type_subtraction_method = " << type_subtraction_method << endl;
  logger << LOG_DEBUG << "in_contribution_order_alpha_s = " << in_contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "in_contribution_order_alpha_e = " << in_contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "type_contribution = " << type_contribution << endl;
  logger << LOG_DEBUG << "type_correction = " << type_correction << endl;
  logger << LOG_DEBUG << "interference = " << interference << endl;
  logger << LOG_DEBUG << "photon_induced = " << photon_induced << endl;
  for (int i_d = 0; i_d < directory.size(); i_d++){logger << LOG_DEBUG << "directory[" << setw(3) << "] = " << directory[i_d] << endl;}
  logger << LOG_DEBUG << "subprocess.size() = " << subprocess.size() << endl;
  for (int i_p = 1; i_p < subprocess.size(); i_p++){logger << LOG_DEBUG << "subprocess[" << setw(3) << "] = " << subprocess[i_p] << endl;}
  logger << LOG_DEBUG << "ygeneric->subgroup.size() = " << ygeneric->subgroup.size() << endl;
  for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG << "ygeneric->subgroup[" << setw(3) << "] = " << ygeneric->subgroup[i_g] << endl;}
  logger << LOG_DEBUG << "subgroup_no_member = " << subgroup_no_member.size() << endl;
  logger << LOG_DEBUG << "ygeneric->subgroup.size() = " << ygeneric->subgroup.size() << endl;
  logger << LOG_DEBUG << "result_moment = " << result_moment << endl;

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
    for (int i_v = 0; i_v < ygeneric->name_scale_variation_TSV.size(); i_v++){
      // only if filled ???
      system_execute(logger, "mkdir " + ygeneric->final_resultdirectory + "/" + osi->name_extended_set_TSV[i_s] + "/" + ygeneric->name_scale_variation_TSV[i_v] + "/" + ylist->resultdirectory);
      // only if filled ???
      system_execute(logger, "mkdir " + ygeneric->final_resultdirectory + "/" + osi->name_extended_set_TSV[i_s] + "/" + ygeneric->name_scale_variation_TSV[i_v] + "/" + ylist->resultdirectory + "/" + infix_contribution);
    }
  }

  result_TSV.resize(osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<double> > > > (osi->n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV))));
  deviation_TSV.resize(osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<double> > > > (osi->n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV))));

  for (int i_m = 0; i_m < osi->n_moments; i_m++){
    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      /////      for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  result_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  deviation_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	}
      }
    }
  }

  for (int i_p = 1; i_p < xsubprocess.size(); i_p++){
    xsubprocess[i_p]->readin_result_TSV(result_moment);
    xsubprocess[i_p]->combination_result_TSV(result_moment);
    vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(xsubprocess[i_p]->result_run_TSV);
    vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(xsubprocess[i_p]->deviation_run_TSV);
  }

  ////////////////////////////////////////////////////////////
  //  calculate result summed over individual subprocesses  //
  ////////////////////////////////////////////////////////////

  logger << LOG_DEBUG_VERBOSE << "calculate result summed over individual subprocesses" << endl;

  for (int i_m = 0; i_m < osi->n_moments; i_m++){
    for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	    xsubprocess[0]->result_TSV[i_m][i_q][i_s][i_r][i_f] = 0.;
	    double dev = 0.;
	    for (int i_p = 1; i_p < subprocess.size(); i_p++){
	      xsubprocess[0]->result_TSV[i_m][i_q][i_s][i_r][i_f] += xsubprocess[i_p]->result_TSV[i_m][i_q][i_s][i_r][i_f];
	      dev += pow(xsubprocess[i_p]->deviation_TSV[i_m][i_q][i_s][i_r][i_f] , 2);
	    }
	    xsubprocess[0]->deviation_TSV[i_m][i_q][i_s][i_r][i_f] = sqrt(dev);
	  }
	}
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  //  calculation of cross sections for subprocess groups (i_g = 0 -> all subprocesses)  //
  /////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////
  //  calculation of subgroup results in TSV variation  //
  ////////////////////////////////////////////////////////

  if (osi->switch_TSV){
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	      for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
		double dev = 0.;
		logger << LOG_DEBUG_VERBOSE << "subgroup_no_member[" << i_g << "].size() = " << subgroup_no_member[i_g].size() << endl;
		for (int i_gm = 0; i_gm < subgroup_no_member[i_g].size(); i_gm++){
		  logger << LOG_DEBUG_VERBOSE << "   i_q = " << i_q << "   i_s = " << i_s << "   i_r = " << i_r << "   i_f = " << i_f << "   i_g = " << i_g << "   i_gm = " << i_gm << "   i_m = " << i_m << endl;
		  logger << LOG_DEBUG_VERBOSE << "subgroup_no_member[i_g][i_gm] = " << subgroup_no_member[i_g][i_gm] << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV.size() = " << result_TSV.size() << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV[i_m].size() = " << result_TSV[i_m].size() << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV[i_m][i_g].size() = " << result_TSV[i_m][i_g].size() << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV[i_m][i_g][i_q].size() = " << result_TSV[i_m][i_g][i_q].size() << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV[i_m][i_g][i_q][i_s].size() = " << result_TSV[i_m][i_g][i_q].size() << endl;
		  logger << LOG_DEBUG_VERBOSE << "result_TSV[i_m][i_g][i_q][i_s][i_r].size() = " << result_TSV[i_m][i_g][i_q][i_s].size() << endl;
		  result_TSV[i_m][i_g][i_q][i_s][i_r][i_f] += xsubprocess[subgroup_no_member[i_g][i_gm]]->result_TSV[i_m][i_q][i_s][i_r][i_f];
		  dev += pow(xsubprocess[subgroup_no_member[i_g][i_gm]]->deviation_TSV[i_m][i_q][i_s][i_r][i_f], 2);
		}
		deviation_TSV[i_m][i_g][i_q][i_s][i_r][i_f] = sqrt(dev);
		stringstream temp_res;
		temp_res << setw(23) << setprecision(15) << result_TSV[i_m][i_g][i_q][i_s][i_r][i_f];
		stringstream temp_dev;
		temp_dev << setw(23) << setprecision(15) << deviation_TSV[i_m][i_g][i_q][i_s][i_r][i_f];
		logger << LOG_DEBUG_VERBOSE << "XXX   result_TSV[" << i_m << "][" << i_g << "][" << i_q << "][" << i_s << "][" << i_r << "][" << i_f << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	      }
	    }
	  }
	}
      }
    }
  }

  if (ygeneric->switch_output_overview > 2){output_result_overview_qTcut_TSV();}
  if (ygeneric->switch_output_result > 2){output_result_qTcut_TSV();}
  if (ygeneric->switch_output_plot > 2){output_result_plot_qTcut_TSV();}

  if (ygeneric->switch_output_plot > 3){output_subprocesses_result_plot_qTcut_TSV();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

