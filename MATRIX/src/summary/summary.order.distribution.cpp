#include "header.hpp"

void summary_order::collect_distribution_TSV(){
  Logger logger("summary_order::collect_distribution_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ////////////////////////////////////////////////
  //  resize distribution_result/deviation_TSV  //
  ////////////////////////////////////////////////

  logger << LOG_DEBUG_VERBOSE << "initialization of   distribution_result_TSV   started" << endl;

  distribution_result_TSV.resize(ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size()));
  distribution_deviation_TSV.resize(ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size()));
  for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      if (!ygeneric->switch_output_distribution[i_d]){continue;}
      distribution_result_TSV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
      distribution_deviation_TSV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
      for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	distribution_result_TSV[i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	distribution_deviation_TSV[i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (!osi->switch_distribution_TSV[i_s]){continue;}
	  distribution_result_TSV[i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  distribution_deviation_TSV[i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "initialization of   distribution_result_TSV   finished" << endl;

  //////////////////////////////////////////////////////
  //  resize distribution_result/deviation_qTcut_TSV  //
  //////////////////////////////////////////////////////

  logger << LOG_DEBUG_VERBOSE << "initialization of   distribution_result_qTcut_TSV   started" << endl;

  distribution_result_qTcut_TSV.resize(selection_n_qTcut, vector<vector<vector<vector<vector<vector<double> > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size())));
  distribution_deviation_qTcut_TSV.resize(selection_n_qTcut, vector<vector<vector<vector<vector<vector<double> > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size())));
  for (int i_q = 0; i_q < selection_n_qTcut; i_q++){
    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	distribution_result_qTcut_TSV[i_q][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	distribution_deviation_qTcut_TSV[i_q][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	  distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (!osi->switch_distribution_TSV[i_s]){continue;}
	    distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  }
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "initialization of   distribution_result_qTcut_TSV   finished" << endl;

  ///////////////////////////////////////////////////
  //  calculate distribution_result/deviation_TSV  //
  ///////////////////////////////////////////////////

  if (combination.size() == 1){
    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-independent/extrapolated part) started" << endl;

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (!osi->switch_distribution_TSV[i_s]){continue;}
	    distribution_result_TSV[i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    distribution_deviation_TSV[i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){logger << LOG_DEBUG_VERBOSE << "i_r = " << i_r << endl;
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){logger << LOG_DEBUG_VERBOSE << "i_f = " << i_f << endl;
		double dev2 = 0.;
		for (int i_l = 0; i_l < xlist.size(); i_l++){logger << LOG_DEBUG_VERBOSE << "i_l = " << i_l << endl;
		  distribution_result_TSV[i_g][i_d][i_b][i_s][i_r][i_f] += xlist[i_l]->distribution_result_TSV[i_g][i_d][i_b][i_s][i_r][i_f];
		  dev2 += pow(xlist[i_l]->distribution_deviation_TSV[i_g][i_d][i_b][i_s][i_r][i_f], 2);
		}
		distribution_deviation_TSV[i_g][i_d][i_b][i_s][i_r][i_f] = sqrt(dev2);
	      }
	    }
	  }
	}
      }
    }
    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-independent/extrapolated part) finished" << endl;
  }
  else if (combination.size() > 1){
    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-independent/extrapolated part) started" << endl;

    vector<vector<vector<vector<vector<vector<vector<double> > > > > > > factor_order_result_TSV(combination.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size())));
    vector<vector<vector<vector<vector<vector<vector<double> > > > > > > factor_order_deviation_TSV(combination.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<double> > > > > (osi->extended_distribution.size())));
    for (int i_c = 0; i_c < combination.size(); i_c++){
      for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
	for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	  if (!ygeneric->switch_output_distribution[i_d]){continue;}
	  factor_order_result_TSV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	  factor_order_deviation_TSV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    factor_order_result_TSV[i_c][i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	    factor_order_deviation_TSV[i_c][i_g][i_d][i_b].resize(osi->n_extended_set_TSV);
	    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	      if (!osi->switch_distribution_TSV[i_s]){continue;}
	      factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	      factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    }
	  }
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (!osi->switch_distribution_TSV[i_s]){continue;}
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){logger << LOG_DEBUG_VERBOSE << "i_r = " << i_r << endl;
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){logger << LOG_DEBUG_VERBOSE << "i_f = " << i_f << endl;
		for (int i_c = 0; i_c < combination.size(); i_c++){logger << LOG_DEBUG_VERBOSE << "i_c = " << i_c << endl;
		  double result = 0.;
		  double dev2 = 0.;
		  for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		    int i_l = combination[i_c][j_c];
		    result += xlist[i_l]->distribution_result_TSV[i_g][i_d][i_b][i_s][i_r][i_f];
		    dev2 += pow(xlist[i_l]->distribution_deviation_TSV[i_g][i_d][i_b][i_s][i_r][i_f], 2);
		  }
		  factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f] = result;
		  factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f] = sqrt(dev2);
		}
	      }
	    }
	  }
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (!osi->switch_distribution_TSV[i_s]){continue;}
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){logger << LOG_DEBUG_VERBOSE << "i_r = " << i_r << endl;
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){logger << LOG_DEBUG_VERBOSE << "i_f = " << i_f << endl;
		double finalized_result = 0.;
		double finalized_deviation2 = 0.;
		double index_LO = 0;
		double temp_result = 0.;
		double temp_deviation2 = 0.;
		for (int i_c = 0; i_c < combination.size(); i_c++){
		  if (combination_type[i_c] == 0){
		    if (i_c != 0){
		      finalized_result += temp_result;
		      temp_result = 0.;
		      finalized_deviation2 += temp_deviation2;
		      temp_deviation2 = 0.;
		    }
		    index_LO = i_c;
		    temp_result = factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f];
		    temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f], 2);
		  }
		  else if (combination_type[i_c] == 1){
		    if (factor_order_result_TSV[index_LO][i_g][i_d][i_b][i_s][i_r][i_f] == 0.){
		      temp_result += factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f];
		      // simplification in case of multiplicative combination:
		      temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f], 2);
		    }
		    else {
		      temp_result *= (1. + factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f] / factor_order_result_TSV[index_LO][i_g][i_d][i_b][i_s][i_r][i_f]);
		      // simplification in case of multiplicative combination:
		      temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f], 2);
		    }
		  }
		  else if (combination_type[i_c] == 2){
		    temp_result -= factor_order_result_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f];
		    temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_s][i_r][i_f], 2);
		  }

		}
		distribution_result_TSV[i_g][i_d][i_b][i_s][i_r][i_f] = finalized_result + temp_result;
		distribution_deviation_TSV[i_g][i_d][i_b][i_s][i_r][i_f] = sqrt(finalized_deviation2 + temp_deviation2);
	      }
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-independent/extrapolated part) finished" << endl;
  }

  /////////////////////////////////////////////////////////
  //  calculate distribution_result/deviation_qTcut_TSV  //
  /////////////////////////////////////////////////////////

  if (combination.size() == 1){
    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent part) started" << endl;

   for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_q = 0; i_q < selection_n_qTcut; i_q++){logger << LOG_DEBUG_VERBOSE << "i_q = " << i_q << endl;
	    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	      if (!osi->switch_distribution_TSV[i_s]){continue;}
	      distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	      distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	      for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){logger << LOG_DEBUG_VERBOSE << "i_r = " << i_r << endl;
		for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){logger << LOG_DEBUG_VERBOSE << "i_f = " << i_f << endl;
		  double dev = 0.;
		  for (int i_l = 0; i_l < xlist.size(); i_l++){logger << LOG_DEBUG_VERBOSE << "i_l = " << i_l << endl;
		    int y_q = 0;
		    if (xlist[i_l]->xcontribution[0]->active_qTcut){y_q = i_q; logger << LOG_DEBUG_VERBOSE << "y_q = " << y_q << endl;}
		    logger << LOG_DEBUG_VERBOSE << "xlist[" << i_l << "]->xcontribution[0]->distribution_result_TSV[" << i_g << "][" << i_d << "][" << i_b << "].size() = " << xlist[i_l]->xcontribution[0]->distribution_result_TSV[i_g][i_d][i_b].size() << endl;
		    logger << LOG_DEBUG_VERBOSE << "xlist[" << i_l << "]->xcontribution[0]->distribution_deviation_TSV[" << i_g << "][" << i_d << "][" << i_b << "].size() = " << xlist[i_l]->xcontribution[0]->distribution_deviation_TSV[i_g][i_d][i_b].size() << endl;
		    distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f] += xlist[i_l]->xcontribution[0]->distribution_result_TSV[i_g][i_d][i_b][y_q][i_s][i_r][i_f];
		    dev += pow(xlist[i_l]->xcontribution[0]->distribution_deviation_TSV[i_g][i_d][i_b][y_q][i_s][i_r][i_f], 2);
		  }
		  distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f] = sqrt(dev);
		  stringstream temp_res;
		  temp_res << setw(23) << setprecision(15) << distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f];
		  stringstream temp_dev;
		  temp_dev << setw(23) << setprecision(15) << distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f];
		  logger << LOG_DEBUG_VERBOSE << "distribution_result_qTcut_TSV[" << i_q << "][" << i_g << "][" << i_d << "][" << i_b << "][" << i_s << "][" << i_r << "][" << i_f << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
		}
	      }
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent part) finished" << endl;
  }
  else if (combination.size() > 1){
    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent part) started" << endl;

    logger << LOG_DEBUG_VERBOSE << "combination_type.size() = " << combination_type.size() << endl;
    for (int i_c = 0; i_c < combination_type.size(); i_c++){
      logger << LOG_DEBUG_VERBOSE << "combination_type[" << i_c << "] = " << combination_type[i_c] << endl;
    }

    logger << LOG_DEBUG_VERBOSE << "combination.size() = " << combination.size() << endl;
    for (int i_c = 0; i_c < combination.size(); i_c++){
      logger << LOG_DEBUG_VERBOSE << "combination[" << i_c << "].size() = " << combination[i_c].size() << endl;
      for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
	logger << LOG_DEBUG_VERBOSE << "combination[" << i_c << "][" << j_c << "] = " << combination[i_c][j_c] << endl;
      }
    }

    vector<vector<vector<vector<vector<vector<vector<vector<double> > > > > > > > factor_order_result_TSV(combination.size(), vector<vector<vector<vector<vector<vector<vector<double> > > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->extended_distribution.size())));
    vector<vector<vector<vector<vector<vector<vector<vector<double> > > > > > > > factor_order_deviation_TSV(combination.size(), vector<vector<vector<vector<vector<vector<vector<double> > > > > > > (ygeneric->subgroup.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->extended_distribution.size())));
    for (int i_c = 0; i_c < combination.size(); i_c++){
      factor_order_result_TSV[i_c].resize(ygeneric->subgroup.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->extended_distribution.size()));
      factor_order_deviation_TSV[i_c].resize(ygeneric->subgroup.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->extended_distribution.size()));
      for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
	for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	  if (!ygeneric->switch_output_distribution[i_d]){continue;}
	  factor_order_result_TSV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	  factor_order_deviation_TSV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins);
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    factor_order_result_TSV[i_c][i_g][i_d][i_b].resize(selection_n_qTcut);
	    factor_order_deviation_TSV[i_c][i_g][i_d][i_b].resize(selection_n_qTcut);
	    for (int i_q = 0; i_q < selection_n_qTcut; i_q++){
	      factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q].resize(osi->n_extended_set_TSV);
	      factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q].resize(osi->n_extended_set_TSV);
	      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
		if (!ygeneric->switch_output_scaleset[i_s]){continue;}
		if (!osi->switch_distribution_TSV[i_s]){continue;}
		factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
		factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	      }
	    }
	  }
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_q = 0; i_q < selection_n_qTcut; i_q++){logger << LOG_DEBUG_VERBOSE << "i_q = " << i_q << endl;
	    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	      if (!osi->switch_distribution_TSV[i_s]){continue;}
	      for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
		for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
		  for (int i_c = 0; i_c < combination.size(); i_c++){
		    double result = 0.;
		    double dev2 = 0.;
		    for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		      int i_l = combination[i_c][j_c];
		      int y_q = 0;
		      if (xlist[i_l]->xcontribution[0]->active_qTcut){y_q = i_q;}
		      result += xlist[i_l]->xcontribution[0]->distribution_result_TSV[i_g][i_d][i_b][y_q][i_s][i_r][i_f];
		      dev2 += pow(xlist[i_l]->xcontribution[0]->distribution_deviation_TSV[i_g][i_d][i_b][y_q][i_s][i_r][i_f], 2);
		    }
		    factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f] = result;
		    factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f] = sqrt(dev2);
		  }
		}
	      }
	    }
	  }
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_q = 0; i_q < selection_n_qTcut; i_q++){logger << LOG_DEBUG_VERBOSE << "i_q = " << i_q << endl;
	    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){logger << LOG_DEBUG_VERBOSE << "i_s = " << i_s << endl;
	      if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	      if (!osi->switch_distribution_TSV[i_s]){continue;}
	      for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
		for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
		  double finalized_result = 0.;
		  double finalized_deviation2 = 0.;
		  double index_LO = 0;
		  double temp_result = 0.;
		  double temp_deviation2 = 0.;
		  for (int i_c = 0; i_c < combination.size(); i_c++){
		    if (combination_type[i_c] == 0){
		      if (i_c != 0){
			// + combination (combination_type[i_c] == 0) may start a new multiplicative part
			finalized_result += temp_result;
			temp_result = 0.;
			finalized_deviation2 += temp_deviation2;
			temp_deviation2 = 0.;
		      }
		      index_LO = i_c;
		      temp_result = factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f];
		      // Why += ???
		      temp_deviation2 += factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f];
		    }
		    else if (combination_type[i_c] == 1){
		      if (factor_order_result_TSV[index_LO][i_g][i_d][i_b][i_q][i_s][i_r][i_f] == 0.){
			temp_result += factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f];
		        // simplification in case of multiplicative combination:
		        temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f], 2);
		      }
		      else {
			temp_result *= (1. + factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f] / factor_order_result_TSV[index_LO][i_g][i_d][i_b][i_q][i_s][i_r][i_f]);
			// simplification in case of multiplicative combination:
			temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f], 2);
		      }
		    }
		    else if (combination_type[i_c] == 2){
		      temp_result -= factor_order_result_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f];
		      temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_g][i_d][i_b][i_q][i_s][i_r][i_f], 2);
		    }
		  }
		  distribution_result_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f] = finalized_result + temp_result;
		  distribution_deviation_qTcut_TSV[i_q][i_g][i_d][i_b][i_s][i_r][i_f] = sqrt(finalized_deviation2 + temp_deviation2);
		}
	      }
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent part) finished" << endl;
  }

  if (ygeneric->switch_output_order){
    if (ygeneric->switch_output_overview > 0){output_distribution_overview_TSV();}
    if (ygeneric->switch_output_plot > 0 && ygeneric->switch_output_qTcut > 0){output_distribution_qTcut_TSV();}
    if (ygeneric->switch_output_plot > 0){output_distribution_TSV();}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_order::collect_distribution_CV(){
  Logger logger("summary_order::collect_distribution_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  distribution_result_CV.resize(ygeneric->subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size()));
  distribution_deviation_CV.resize(ygeneric->subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size()));
  for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      if (!ygeneric->switch_output_distribution[i_d]){continue;}
      distribution_result_CV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
      distribution_deviation_CV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
    }
  }

  if (combination.size() == 1){
    logger << LOG_DEBUG_VERBOSE << "additive combination (only qTcut-independent part at value minqTcut available) started" << endl;

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	    double dev = 0.;
	    for (int i_l = 0; i_l < xlist.size(); i_l++){logger << LOG_DEBUG_VERBOSE << "i_l = " << i_l << endl;
	      ///	    for (int i_l = 0; i_l < contribution_file.size(); i_l++){
	      ///	      int x_l = mapping_contribution_file[contribution_file[i_l]];
	      distribution_result_CV[i_g][i_d][i_b][i_s] += xlist[i_l]->xcontribution[0]->distribution_result_CV[i_g][i_d][i_b][i_s];
	      dev += pow(xlist[i_l]->xcontribution[0]->distribution_deviation_CV[i_g][i_d][i_b][i_s], 2);
	    }
	    distribution_deviation_CV[i_g][i_d][i_b][i_s] = sqrt(dev);

	    if (osi->extended_distribution[i_d].xdistribution_name == "total_rate"){
	      stringstream temp_res;
	      temp_res << setw(23) << setprecision(15) << distribution_result_CV[i_g][i_d][i_b][i_s];
	      stringstream temp_dev;
	      temp_dev << setw(23) << setprecision(15) << distribution_deviation_CV[i_g][i_d][i_b][i_s];
	      //	      if (i_b == 0 && i_s == 0){
		logger << LOG_INFO << resultdirectory << "   result_CV[" << i_g << "][" << i_d << "][" << i_b << "][" << i_s << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
		//	      }
	      logger << LOG_DEBUG_VERBOSE << "XXX   result_CV[" << i_g << "][" << i_d << "][" << i_b << "][" << i_s << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "additive combination (only qTcut-independent part at value minqTcut available) finished" << endl;
  }
  else if (combination.size() > 1){
    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (only qTcut-independent part at value minqTcut available) started" << endl;

    vector<vector<vector<vector<vector<double> > > > > factor_order_result_CV(combination.size(), vector<vector<vector<vector<double> > > > (ygeneric->subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size())));
    vector<vector<vector<vector<vector<double> > > > > factor_order_deviation_CV(combination.size(), vector<vector<vector<vector<double> > > > (ygeneric->subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size())));
    for (int i_c = 0; i_c < combination.size(); i_c++){
      for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
	for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	  if (!ygeneric->switch_output_distribution[i_d]){continue;}
	  factor_order_result_CV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
	  factor_order_deviation_CV[i_c][i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	    for (int i_c = 0; i_c < combination.size(); i_c++){logger << LOG_DEBUG_VERBOSE << "i_c = " << i_c << endl;
	      double result = 0.;
	      double dev2 = 0.;
	      for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		int i_l = combination[i_c][j_c];
		result += xlist[i_l]->xcontribution[0]->distribution_result_CV[i_g][i_d][i_b][i_s];
		dev2 += pow(xlist[i_l]->xcontribution[0]->distribution_deviation_CV[i_g][i_d][i_b][i_s], 2);
	      }
	      factor_order_result_CV[i_c][i_g][i_d][i_b][i_s] = result;
	      factor_order_deviation_CV[i_c][i_g][i_d][i_b][i_s] = sqrt(dev2);
	    }
	  }
	}
      }
    }

    for (int i_g = 0; i_g < ygeneric->subgroup.size(); i_g++){logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){logger << LOG_DEBUG_VERBOSE << "i_d = " << i_d << endl;
	if (!ygeneric->switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << endl;
	  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	    double finalized_result = 0.;
	    double finalized_deviation2 = 0.;
	    double index_LO = 0;
	    double temp_result = 0.;
	    double temp_deviation2 = 0.;
	    for (int i_c = 0; i_c < combination.size(); i_c++){
	      if (combination_type[i_c] == 0){
		if (i_c != 0){
		  finalized_result += temp_result;
		  temp_result = 0.;
		  finalized_deviation2 += temp_deviation2;
		  temp_deviation2 = 0.;
		}
		index_LO = i_c;
		temp_result = factor_order_result_CV[i_c][i_g][i_d][i_b][i_s];
		temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_g][i_d][i_b][i_s], 2);
	      }
	      else if (combination_type[i_c] == 1){
		if (factor_order_result_CV[index_LO][i_g][i_d][i_b][i_s] == 0.){
		  temp_result += factor_order_result_CV[i_c][i_g][i_d][i_b][i_s];
		  // simplification in case of multiplicative combination:
		  temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_g][i_d][i_b][i_s], 2);
		}
		else {
		  temp_result *= (1. + factor_order_result_CV[i_c][i_g][i_d][i_b][i_s] / factor_order_result_CV[index_LO][i_g][i_d][i_b][i_s]);
		  // simplification in case of multiplicative combination:
		  temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_g][i_d][i_b][i_s], 2);
		}
	      }
	      else if (combination_type[i_c] == 2){
		temp_result = -factor_order_result_CV[i_c][i_g][i_d][i_b][i_s];
		temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_g][i_d][i_b][i_s], 2);
	      }

	    }
	    distribution_result_CV[i_g][i_d][i_b][i_s] = finalized_result + temp_result;
	    distribution_deviation_CV[i_g][i_d][i_b][i_s] = sqrt(finalized_deviation2 + temp_deviation2);
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (only qTcut-independent part at value minqTcut available) finished" << endl;
  }

  if (ygeneric->switch_output_order){
    if (ygeneric->switch_output_overview > 0){output_distribution_overview_CV();}
    if (ygeneric->switch_output_plot > 0){output_distribution_CV();}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

