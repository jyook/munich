#include "header.hpp"

void summary_generic::output_distribution_table_order(){
  Logger logger("summary_generic::output_distribution_table_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

    // create table output here (without K-factors etc.) !!!

  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
      // only copy&paste: more elegant soultion for directory_qTcut !!!
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution_table[i_d]){continue;}

	vector<string> name_plot;
	string temp_sdd = "." + osi->extended_distribution[i_d].xdistribution_name;

	name_plot.push_back("table.plot" + temp_sdd + ".dat");
	name_plot.push_back("table.norm" + temp_sdd + ".dat");

	if (i_d >= osi->dat.size()){
	  int i_ddd = i_d - osi->dat.size();

	  // recombined distributions (essentially for validation)
	  stringstream name_rec;
	  name_rec << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name;
	  //	  string s0;
	  name_plot.push_back("table.plot" + name_rec.str() + ".tex");
	  name_plot.push_back("table.norm" + name_rec.str()  + ".tex");

	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    stringstream name_split_bin;
	    name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	    stringstream name_split_lt;
	    name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	    stringstream name_split_ge;
	    name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	    name_plot.push_back("table.norm.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.norm.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.plot.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.plot.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");

	    name_plot.push_back("table.norm.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("table.norm.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	    name_plot.push_back("table.plot.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("table.plot.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	  }
	}




	vector<string> output_order(4);
	output_order[0] = "LO";
	output_order[1] = "NLO.QCD";
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";

	vector<int> no_output_order(output_order.size(), -1);
	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  for (int i_o = 0; i_o < yorder.size(); i_o++){
	    if (output_order[j_o] == yorder[i_o]->resultdirectory){no_output_order[j_o] = i_o; break;}
	  }
	}

	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  logger << LOG_INFO << left
		 << setw(4) << j_o
		 << setw(20) << output_order[j_o]
		 << " -> "
		 << setw(4) << no_output_order[j_o] << endl;
	}

	int n_output_version = name_plot.size();
	for (int i_x = 0; i_x < n_output_version; i_x++){
	  logger << LOG_INFO << "i_x = " << i_x << endl;

	  string outfilename_latex_table = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];

	  logger << LOG_INFO << "outfilename_latex_table = " <<  outfilename_latex_table << endl;

	  ofstream outf;
	  outf.open(outfilename_latex_table.c_str(), ofstream::out | ofstream::trunc);

	  outf << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	  outf << char(92) << "begin{table}" << endl;
	  outf << char(92) << "begin{center}" << endl;
	  outf << char(92) << "begin{tabular}{|c|";
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}
	    outf << "c|";
	  }
	  outf << "}" << endl;
	  outf << char(92) << "hline" << endl;
	  outf << "$" << char(92) << "sqrt{s}" << char(92) << ", (" << char(92) << "mathrm{TeV})$ &" << endl;
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}

	    if (output_order[j_o] == "LO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}";}
	    else if (output_order[j_o] == "nLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}";}
	    else if (output_order[j_o] == "nnLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}";}
	    else if (output_order[j_o] == "NLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NLO}}";}
	    else if (output_order[j_o] == "nNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}";}
	    else if (output_order[j_o] == "nNLO.QCD+gg"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}";}
	    else if (output_order[j_o] == "NNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}";}
	    else {outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_order[j_o] << "}}$";}
	    outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";

	    if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << endl;}
	    else {outf << " &" << endl;}
	  }
	  outf << char(92) << "hline" << endl;

	  // better: variable... -> doubled lines for plotting reasons etc.
	  /*
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    outf << osi->extended_distribution[i_d].bin_edge[i_b] << " &" << endl;
	  */
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "].size() = " << scaleband_variable[i_sb].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " << scaleband_variable[i_sb][i_d].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " << scaleband_variable[i_sb][i_d][i_x].size() << endl;

	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
	    //	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size(); i_b++){
	    outf << osi->E_CMS / 1000 << " " << char(92) << "nameobservable{" << scaleband_variable[i_sb][i_d][i_x][i_b] << "} &" << endl;

	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      int i_o = no_output_order[j_o];
	      if (i_o == -1){continue;}

	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "].size() = " << scaleband_central_deviation[i_sb].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "].size() = " << scaleband_central_deviation[i_sb][i_d].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "][" << i_x << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o][i_x].size() << endl;

	      outf << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 1)
		   << "^{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}"
		   << "_{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}$";
	      /*
	      outf << setw(16) << setprecision(8) << scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] << "("
		   << setw(16) << setprecision(8) << scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b] << ")"
		   << "^{" << (scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b] / scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] - 1.) * 100. << char(92) << "%}"
		   << "_{" << (scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b] / scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] - 1.) * 100. << char(92) << "%}";
	      */
	      if (j_o == output_order.size() - 1){
		outf << " " << char(92) << char(92) << endl;
		outf << char(92) << "hline" << endl;
	      }
	      else {outf << " &" << endl;}
	      //  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	    }
	  }
	  outf << char(92) << "end{tabular}" << endl;
	  outf << char(92) << "end{center}" << endl;
	  outf << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	  outf << char(92) << "end{table}" << endl;
	  outf.close();
	}
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_distribution_table_Kfactor(){
  Logger logger("summary_generic::output_distribution_table_Kfactor");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    // create K-factor table output here !!!


    for (int i_y = 0; i_y < 2; i_y++){
      string output_reference;
      string output_reference_latex;
      vector<string> output_order;

      if (i_y == 0){
	output_reference = "NLO.QCD";
	output_reference_latex = "NLO";

	output_order.resize(5);
	output_order[0] = "LO";
	output_order[1] = "NLO.QCD";
	output_order[2] = "nNLO.QCD";
	output_order[3] = "nNLO.QCD+gg";
	output_order[4] = "NNLO.QCD";
      }

      else if (i_y == 1){
	output_reference = "nNLO.QCD";
	output_reference_latex = "nNLO";

	output_order.resize(4);
	output_order[0] = "nnLO";
	output_order[1] = "nNLO.QCD";
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";
      }



    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
      // only copy&paste: more elegant soultion for directory_qTcut !!!
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution_table[i_d]){continue;}

	vector<string> name_plot;
	string temp_sdd = "." + osi->extended_distribution[i_d].xdistribution_name;

	name_plot.push_back("Kfactor" + output_reference_latex + ".plot" + temp_sdd + ".dat");
	name_plot.push_back("Kfactor" + output_reference_latex + ".norm" + temp_sdd + ".dat");

	if (i_d >= osi->dat.size()){
	  int i_ddd = i_d - osi->dat.size();

	// recombined distributions (essentially for validation)
	  stringstream name_rec;
	  name_rec << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name;
	  //	  string s0;
	  name_plot.push_back("Kfactor" + output_reference_latex + ".plot" + name_rec.str() + ".tex");
	  name_plot.push_back("Kfactor" + output_reference_latex + ".norm" + name_rec.str()  + ".tex");

	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    stringstream name_split_bin;
	    name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	    stringstream name_split_lt;
	    name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	    stringstream name_split_ge;
	    name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	    name_plot.push_back("Kfactor" + output_reference_latex + ".norm.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".norm.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".plot.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".plot.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");

	    name_plot.push_back("Kfactor" + output_reference_latex + ".norm.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".norm.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".plot.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("Kfactor" + output_reference_latex + ".plot.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	  }
	}



	int no_ref = -1;
	vector<int> no_output_order(output_order.size(), -1);
	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  for (int i_o = 0; i_o < yorder.size(); i_o++){
	    if (output_order[j_o] == yorder[i_o]->resultdirectory){no_output_order[j_o] = i_o; break;}
	  }
	  if (output_reference == output_order[j_o]){no_ref = no_output_order[j_o];}
	}

	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  logger << LOG_INFO << left
		 << setw(4) << j_o
		 << setw(20) << output_order[j_o]
		 << " -> "
		 << setw(4) << no_output_order[j_o] << endl;
	}

	int n_output_version = name_plot.size();
	for (int i_x = 0; i_x < n_output_version; i_x++){
	  logger << LOG_INFO << "i_x = " << i_x << endl;

	  string outfilename_latex_Kfactor = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];

	  logger << LOG_INFO << "outfilename_latex_Kfactor = " <<  outfilename_latex_Kfactor << endl;

	  ofstream outf;
	  outf.open(outfilename_latex_Kfactor.c_str(), ofstream::out | ofstream::trunc);

	  outf << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	  outf << char(92) << "begin{table}" << endl;
	  outf << char(92) << "begin{center}" << endl;
	  outf << char(92) << "begin{tabular}{|c|";
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}
	    if (output_order[j_o] != output_reference){outf << "c|";}
	  }
	  outf << "}" << endl;
	  outf << char(92) << "hline" << endl;
	  outf << "$" << char(92) << "sqrt{s}" << char(92) << ", (" << char(92) << "mathrm{TeV})$ &" << endl;
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}

	    if (output_order[j_o] == output_reference){
	      if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << endl;}

	    }
	    else {
	      if (output_order[j_o] == "LO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nnLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "NLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD+gg"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "NNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else {outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_order[j_o] << "}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      //	      outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";
	      outf << "$";
	      if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << endl;}
	      else {outf << " &" << endl;}
	    }

	  }
	  outf << char(92) << "hline" << endl;

	  // better: variable... -> doubled lines for plotting reasons etc.
	  /*
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    outf << osi->extended_distribution[i_d].bin_edge[i_b] << " &" << endl;
	  */
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "].size() = " << scaleband_variable[i_sb].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " << scaleband_variable[i_sb][i_d].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " << scaleband_variable[i_sb][i_d][i_x].size() << endl;

	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
	    //	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size(); i_b++){
	    outf << osi->E_CMS / 1000 << " " << char(92) << "nameobservable{" << scaleband_variable[i_sb][i_d][i_x][i_b] << "} &" << endl;

	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      int i_o = no_output_order[j_o];
	      if (i_o == -1){continue;}

	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "].size() = " << scaleband_central_deviation[i_sb].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "].size() = " << scaleband_central_deviation[i_sb][i_d].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "][" << i_x << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o][i_x].size() << endl;

	      if (output_order[j_o] == output_reference){
		//		outf << "$0$";
		if (j_o == output_order.size() - 1){
		  outf << " " << char(92) << char(92) << endl;
		  outf << char(92) << "hline" << endl;
		}
	      }
	      else {
		//		double temp_double = scaleband_central_result[i_sb][i_d][no_ref][i_x][i_b] + scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
		outf << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][no_ref][i_x][i_b], scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], 1) << "$";

		if (j_o == output_order.size() - 1){
		  outf << " " << char(92) << char(92) << endl;
		  outf << char(92) << "hline" << endl;
		}
		else {outf << " &" << endl;}
		//  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	      }
	    }
	  }
	  outf << char(92) << "end{tabular}" << endl;
	  outf << char(92) << "end{center}" << endl;
	  outf << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	  outf << char(92) << "end{table}" << endl;
	  outf.close();
	}
      }
    }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_distribution_table_crosssection_Kfactor(){
  Logger logger("summary_generic::output_distribution_table_crosssection_Kfactor");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    // create K-factor table output here !!!


    for (int i_y = 0; i_y < 2; i_y++){
      string output_reference;
      string output_reference2;
      string output_reference_latex;
      vector<string> output_order;

      if (i_y == 0){
	output_reference = "NLO.QCD";
	output_reference2 = "LO";
	output_reference_latex = "NLO";

	output_order.resize(4);
	//	output_order.resize(5);
	output_order[0] = "LO";
	output_order[1] = "NLO.QCD";
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";
	//	output_order[2] = "nNLO.QCD";
	//	output_order[3] = "nNLO.QCD+gg";
	//	output_order[4] = "NNLO.QCD";
      }

      else if (i_y == 1){
	output_reference = "nNLO.QCD";
	output_reference2 = "nnLO";
	output_reference_latex = "nNLO";

	output_order.resize(4);
	output_order[0] = "nnLO";
	output_order[1] = "nNLO.QCD";
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";
      }


    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
      // only copy&paste: more elegant soultion for directory_qTcut !!!
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution_table[i_d]){continue;}

	vector<string> name_plot;
	string temp_sdd = "." + osi->extended_distribution[i_d].xdistribution_name;

	name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot" + temp_sdd + ".dat");
	name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm" + temp_sdd + ".dat");

	if (i_d >= osi->dat.size()){
	  int i_ddd = i_d - osi->dat.size();

	// recombined distributions (essentially for validation)
	  stringstream name_rec;
	  name_rec << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name;
	  //	  string s0;
	  name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot" + name_rec.str() + ".tex");
	  name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm" + name_rec.str()  + ".tex");

	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    stringstream name_split_bin;
	    name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	    stringstream name_split_lt;
	    name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	    stringstream name_split_ge;
	    name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");

	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".norm.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("Xsection.Kfactor" + output_reference_latex + ".plot.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	  }
	}



	int no_ref = -1;
	int no_ref2 = -1;
	vector<int> no_output_order(output_order.size(), -1);
	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  for (int i_o = 0; i_o < yorder.size(); i_o++){
	    if (output_order[j_o] == yorder[i_o]->resultdirectory){no_output_order[j_o] = i_o; break;}
	  }
	  if (output_reference == output_order[j_o]){no_ref = no_output_order[j_o];}
	  if (output_reference2 == output_order[j_o]){no_ref2 = no_output_order[j_o];}
	}

	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  logger << LOG_INFO << left
		 << setw(4) << j_o
		 << setw(20) << output_order[j_o]
		 << " -> "
		 << setw(4) << no_output_order[j_o] << endl;
	}

	int n_output_version = name_plot.size();
	for (int i_x = 0; i_x < n_output_version; i_x++){
	  logger << LOG_INFO << "i_x = " << i_x << endl;

	  string outfilename_latex_Kfactor = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];

	  logger << LOG_INFO << "outfilename_latex_Kfactor = " <<  outfilename_latex_Kfactor << endl;

	  ofstream outf;
	  outf.open(outfilename_latex_Kfactor.c_str(), ofstream::out | ofstream::trunc);

	  outf << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	  outf << char(92) << "begin{table}" << endl;
	  outf << char(92) << "begin{center}" << endl;
	  outf << char(92) << "begin{tabular}{|c|";
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}
	    //	    if (output_order[j_o] != output_reference)
	    else {outf << "c|";}
	  }
	  outf << "}" << endl;
	  outf << char(92) << "hline" << endl;
	  outf << "$" << char(92) << "sqrt{s}" << char(92) << ", (" << char(92) << "mathrm{TeV})$ &" << endl;

	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}

	    if (output_order[j_o] == "LO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}";}
	    else if (output_order[j_o] == "nLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}";}
	    else if (output_order[j_o] == "nnLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}";}
	    else if (output_order[j_o] == "NLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NLO}}";}
	    else if (output_order[j_o] == "nNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}";}
	    else if (output_order[j_o] == "nNLO.QCD+gg"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}";}
	    else if (output_order[j_o] == "NNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}";}
	    else {outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_order[j_o] << "}}$";}
	    outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";

	    if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << endl;}
	    else {outf << " &" << endl;}
	  }

	  outf << " &" << endl;
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}

    	    if (output_order[j_o] == output_reference){
	      outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference2 << "}}}-1";
	      outf << "$";
	      if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << "[1ex]" << endl;}
	      else {outf << " &" << endl;}
	    }
	    else {
	      if (output_order[j_o] == "LO"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "nLO"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "nnLO"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "NLO.QCD"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{NLO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD+gg"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else if (output_order[j_o] == "NNLO.QCD"){outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      else {outf << "$" << char(92) << "frac{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_order[j_o] << "}}}{" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}}-1";}
	      //	      outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";
	      /*
	      if (output_order[j_o] == "LO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nnLO"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "NLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "nNLO.QCD+gg"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else if (output_order[j_o] == "NNLO.QCD"){outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      else {outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_order[j_o] << "}}/" << char(92) << "sigma_{" << char(92) << "mathrm{" << output_reference_latex << "}}-1";}
	      //	      outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";
	      */
	      outf << "$";
	      if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << "[1ex]" << endl;}
	      else {outf << " &" << endl;}
	    }

	  }
	  outf << char(92) << "hline" << endl;

	  // better: variable... -> doubled lines for plotting reasons etc.
	  /*
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    outf << osi->extended_distribution[i_d].bin_edge[i_b] << " &" << endl;
	  */
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "].size() = " << scaleband_variable[i_sb].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " << scaleband_variable[i_sb][i_d].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " << scaleband_variable[i_sb][i_d][i_x].size() << endl;

	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
	    //	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size(); i_b++){
	    outf << osi->E_CMS / 1000 << " " << char(92) << "nameobservable{" << scaleband_variable[i_sb][i_d][i_x][i_b] << "} &" << endl;


	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      int i_o = no_output_order[j_o];
	      if (i_o == -1){continue;}


	      outf << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 1)
		   << "^{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}"
		   << "_{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}$";
	      /*
	      outf << setw(16) << setprecision(8) << scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] << "("
		   << setw(16) << setprecision(8) << scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b] << ")"
		   << "^{" << (scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b] / scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] - 1.) * 100. << char(92) << "%}"
		   << "_{" << (scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b] / scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] - 1.) * 100. << char(92) << "%}";
	      */
	      if (j_o == output_order.size() - 1){
		outf << " " << char(92) << char(92) << endl;
		//		outf << char(92) << "hline" << endl;
	      }
	      else {outf << " &" << endl;}
	      //  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	    }

	    outf << " &" << endl;
    	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      int i_o = no_output_order[j_o];
	      if (i_o == -1){continue;}

	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "].size() = " << scaleband_central_deviation[i_sb].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "].size() = " << scaleband_central_deviation[i_sb][i_d].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "][" << i_x << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o][i_x].size() << endl;

	      if (output_order[j_o] == output_reference){
		//		outf << "$0$";
		outf << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][no_ref2][i_x][i_b], scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], 1) << "$";
		if (j_o == output_order.size() - 1){
		  outf << " " << char(92) << char(92) << endl;
		  outf << char(92) << "hline" << endl;
		}
		else {outf << " &" << endl;}
	      }
	      else {
		//		double temp_double = scaleband_central_result[i_sb][i_d][no_ref][i_x][i_b] + scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
		outf << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][no_ref][i_x][i_b], scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], 1) << "$";

		if (j_o == output_order.size() - 1){
		  outf << " " << char(92) << char(92) << endl;
		  outf << char(92) << "hline" << endl;
		}
		else {outf << " &" << endl;}
		//  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	      }
	    }
	  }
	  outf << char(92) << "end{tabular}" << endl;
	  outf << char(92) << "end{center}" << endl;
	  outf << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	  outf << char(92) << "end{table}" << endl;
	  outf.close();
	}
      }
    }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_distribution_table_IS_splitting(){
  Logger logger("summary_generic::output_distribution_table_IS_splitting");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    // create table output here (composition of NNLO corrections according to PDF channels) !!!

    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
      // only copy&paste: more elegant soultion for directory_qTcut !!!
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution_table[i_d]){continue;}

	vector<string> name_plot;
	string temp_sdd = "." + osi->extended_distribution[i_d].xdistribution_name;

	name_plot.push_back("composition.plot" + temp_sdd + ".dat");
	name_plot.push_back("composition.norm" + temp_sdd + ".dat");

	if (i_d >= osi->dat.size()){
	  int i_ddd = i_d - osi->dat.size();

	  // recombined distributions (essentially for validation)
	  stringstream name_rec;
	  name_rec << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name;
	  //	  string s0;
	  name_plot.push_back("composition.plot" + name_rec.str() + ".tex");
	  name_plot.push_back("composition.norm" + name_rec.str()  + ".tex");

	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    stringstream name_split_bin;
	    name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	    stringstream name_split_lt;
	    name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	    stringstream name_split_ge;
	    name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	    name_plot.push_back("composition.norm.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("composition.norm.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("composition.plot.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("composition.plot.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");

	    name_plot.push_back("composition.norm.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("composition.norm.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	    name_plot.push_back("composition.plot.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("composition.plot.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	  }
	}




	vector<string> output_order(6);
	output_order[0] = "dNNLO.QCD";
	output_order[1] = "dNNLO.QCD.qqxD";
	output_order[2] = "dNNLO.QCD.qqxND";
	output_order[3] = "dNNLO.QCD.qq";
	output_order[4] = "dNNLO.QCD.gq";
	output_order[5] = "dNNLO.QCD.gg";

	vector<int> no_output_order(output_order.size(), -1);
	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  for (int i_o = 0; i_o < yorder.size(); i_o++){
	    if (output_order[j_o] == yorder[i_o]->resultdirectory){no_output_order[j_o] = i_o; break;}
	  }
	}

	for (int j_o = 0; j_o < output_order.size(); j_o++){
	  logger << LOG_INFO << left
		 << setw(4) << j_o
		 << setw(20) << output_order[j_o]
		 << " -> "
		 << setw(4) << no_output_order[j_o] << endl;
	}

	int n_output_version = name_plot.size();
	for (int i_x = 0; i_x < n_output_version; i_x++){
	  logger << LOG_INFO << "i_x = " << i_x << endl;

	  string outfilename_latex_table = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];

	  logger << LOG_INFO << "outfilename_latex_table = " <<  outfilename_latex_table << endl;

	  ofstream outf;
	  outf.open(outfilename_latex_table.c_str(), ofstream::out | ofstream::trunc);

	  outf << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	  outf << char(92) << "begin{table}" << endl;
	  outf << char(92) << "begin{center}" << endl;
	  outf << char(92) << "begin{tabular}{|c|";
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}
	    outf << "c|";
	    /*
	    if (j_o == 0){outf << "c|";}
	    else {outf << "cc|";}
	    */
	  }
	  outf << "c|"; //validation !!!
	  outf << "}" << endl;
	  outf << char(92) << "hline" << endl;
	  outf << "$" << char(92) << "sqrt{s}" << char(92) << ", (" << char(92) << "mathrm{TeV})$ &" << endl;
	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}

	    if (output_order[j_o] == "dNNLO.QCD"){outf << "$d" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}}" << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";}
	    /*
	    else if (output_order[j_o] == "dNNLO.QCD.qq"){outf << "$d" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}_{qq}}";}
	    else if (output_order[j_o] == "dNNLO.QCD.gq"){outf << "$d" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}_{gq}}";}
	    else if (output_order[j_o] == "dNNLO.QCD.gg"){outf << "$d" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO}_{gg}}";}
	    */
	    else if (output_order[j_o] == "dNNLO.QCD.qqxD"){outf << "$d" << char(92) << "sigma^{q" << char(92) << "bar{q}}_{D}/d" << char(92) << "sigma$";}
	    else if (output_order[j_o] == "dNNLO.QCD.qqxND"){outf << "$d" << char(92) << "sigma^{q" << char(92) << "bar{q}}_{ND}/d" << char(92) << "sigma$";}
	    else if (output_order[j_o] == "dNNLO.QCD.qq"){outf << "$d" << char(92) << "sigma^{qq}/d" << char(92) << "sigma$";}
	    else if (output_order[j_o] == "dNNLO.QCD.gq"){outf << "$d" << char(92) << "sigma^{gq}/d" << char(92) << "sigma$";}
	    else if (output_order[j_o] == "dNNLO.QCD.gg"){outf << "$d" << char(92) << "sigma^{gg}/d" << char(92) << "sigma$";}
	    else {outf << "$" << char(92) << "sigma_{" << char(92) << "mathrm{output_order[j_o]}}$";}
	    /*
	    outf << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";
	    if (output_order[j_o] == "dNNLO.QCD.qq" || output_order[j_o] == "dNNLO.QCD.gq" || output_order[j_o] == "dNNLO.QCD.gg"){outf << " & $(" << char(92) << "%)$";}
	    */

	    if (j_o == output_order.size() - 1){outf << " & " << char(92) << char(92) << endl;} // validation !!!
	    //	    if (j_o == output_order.size() - 1){outf << " " << char(92) << char(92) << endl;}
	    else {outf << " &" << endl;}
	  }
	  outf << char(92) << "hline" << endl;

	  // better: variable... -> doubled lines for plotting reasons etc.
	  /*
	  for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	    outf << osi->extended_distribution[i_d].bin_edge[i_b] << " &" << endl;
	  */
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "].size() = " << scaleband_variable[i_sb].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " << scaleband_variable[i_sb][i_d].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " << scaleband_variable[i_sb][i_d][i_x].size() << endl;

	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
	    //	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size(); i_b++){
	    outf << osi->E_CMS / 1000 << " " << char(92) << "nameobservable{" << scaleband_variable[i_sb][i_d][i_x][i_b] << "} &" << endl;

	    double checksum = 0.;

	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      int i_o = no_output_order[j_o];
	      if (i_o == -1){continue;}

	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "].size() = " << scaleband_central_deviation[i_sb].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "].size() = " << scaleband_central_deviation[i_sb][i_d].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o].size() << endl;
	      logger << LOG_DEBUG_VERBOSE << "scaleband_central_deviation[" << i_sb << "][" << i_d << "][" << i_o << "][" << i_x << "].size() = " << scaleband_central_deviation[i_sb][i_d][i_o][i_x].size() << endl;

	      if (j_o == 0){
		outf << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 1) << "$";
	      }
	      else if (j_o > 0){
		/*
		outf << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 1) << "$";
		*/
		checksum += scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
		double temp_double = scaleband_central_result[i_sb][i_d][no_output_order[0]][i_x][i_b] + scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
		/*
		outf << " & ";
		*/
		outf << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][no_output_order[0]][i_x][i_b], temp_double, 1) << "$";
	      }
	      /*
	      outf << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 1)
		   << "^{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}"
		   << "_{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b], 1) << "}$";
	      */

	      if (j_o == output_order.size() - 1){

		outf << " & " << output_latex_percent(scaleband_central_result[i_sb][i_d][no_output_order[0]][i_x][i_b], checksum, 1) << " " << char(92) << char(92) << endl; // validation !!!
		//		outf << " " << char(92) << char(92) << "   % checksum: " << output_latex_percent(scaleband_central_result[i_sb][i_d][no_output_order[0]][i_x][i_b], checksum, 1) << endl;
		outf << char(92) << "hline" << endl;
	      }
	      else {outf << " &" << endl;}
	      //  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	    }
	  }
	  outf << char(92) << "end{tabular}" << endl;
	  outf << char(92) << "end{center}" << endl;
	  outf << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	  outf << char(92) << "end{table}" << endl;
	  outf.close();
	}
      }
    }

  }

  logger << LOG_DEBUG << "finished" << endl;
}

string latex_name_order(string & name){
  Logger logger("latex_name_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  stringstream temp_ss;
  if (name == "LO"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{LO}}";}
  //  else if (name == "nLO"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{nLO}}";}
  //  else if (name == "nnLO"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{nnLO}}";}
  else if (name == "NLO.QCD"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NLO" << char(92) << ",QCD}}";}
  //  else if (name == "nNLO.QCD"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO}}";}
  //  else if (name == "nNLO.QCD+gg"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{nNLO+gg}}";}
  else if (name == "NNLO.QCD"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO" << char(92) << ",QCD}}";}
  else if (name == "NLO.EW"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NLO" << char(92) << ",EW}}";}
  else if (name == "NNLO.QCD+NLO.EW"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO" << char(92) << ",QCD+EW}}";}
  else if (name == "NNLO.QCDxNLO.EW"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO" << char(92) << ",QCD" << char(92) << "times EW}}";}
  else if (name == "NNLO.QCDxNLO.EW+a"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO" << char(92) << ",QCD" << char(92) << "times EW_{qq}}}";}
  //  else if (name == "NNLO.QCDxNLO.EW+a"){temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{NNLO" << char(92) << ",QCD" << char(92) << "times EW+" << char(92) << "gamma-ind.}}";}
  else {temp_ss << "" << char(92) << "sigma_{" << char(92) << "mathrm{" << name << "}}";}
  //  temp_ss << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})";

  return temp_ss.str();

  logger << LOG_DEBUG << "finished" << endl;
}

void summary_generic::output_distribution_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW(){
  Logger logger("summary_generic::output_distribution_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  vector<xdistribution> temporary_distribution = osi->extended_distribution;


  // define a combined distribution, based on different binnings in the same observable:
  /*
  int x1_d = -1;
  int x2_d = -1;
  for (int i_d = 0; i_d < temporary_distribution.size(); i_d++){
    if (temporary_distribution[i_d].xdistribution_name == "pT_Zlead" ||
	temporary_distribution[i_d].xdistribution_name == "pT_Wlead" ||
	temporary_distribution[i_d].xdistribution_name == "pT_Vlead"){x1_d = i_d; break;}
  }
  for (int i_d = 0; i_d < temporary_distribution.size(); i_d++){
    if (temporary_distribution[i_d].xdistribution_name == "logpT_Zlead" ||
	temporary_distribution[i_d].xdistribution_name == "logpT_Wlead" ||
	temporary_distribution[i_d].xdistribution_name == "logpT_Vlead"){x2_d = i_d; break;}
  }
  if (x1_d > -1 && x2_d > -1){
    temporary_distribution.push_back(osi->extended_distribution[x1_d]);
    int xl_d = temporary_distribution.size() - 1;
    temporary_distribution[xl_d].xdistribution_name = temporary_distribution[xl_d].xdistribution_name + "_combined";

    //    logger << LOG_INFO << "TEMPORARY" << endl;
    //    logger << LOG_INFO << "temporary_distribution[" << x1_d << "]].xdistribution_name = " << temporary_distribution[x1_d].xdistribution_name << endl;
    //    logger << LOG_INFO << "temporary_distribution[" << x2_d << "]].xdistribution_name = " << temporary_distribution[x2_d].xdistribution_name << endl;

    for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
      scaleband_variable[i_sb].push_back(scaleband_variable[i_sb][x1_d]);
      scaleband_central_result[i_sb].push_back(scaleband_central_result[i_sb][x1_d]);
      scaleband_central_deviation[i_sb].push_back(scaleband_central_deviation[i_sb][x1_d]);
      scaleband_maximum_result[i_sb].push_back(scaleband_maximum_result[i_sb][x1_d]);
      scaleband_minimum_result[i_sb].push_back(scaleband_minimum_result[i_sb][x1_d]);
      for (int i_b = 0; i_b < scaleband_variable[i_sb][x1_d][0].size(); i_b++){
	logger << LOG_INFO << "scaleband_variable[" << setw(3) << i_sb << "][" << setw(3) << x1_d << "][0][" << setw(3) << i_b << "] = " << scaleband_variable[i_sb][x1_d][0][i_b] << endl;
      }
      int switch_uncombined = 0;
      for (int i_b = 0; i_b < scaleband_variable[i_sb][x2_d][0].size() - 1; i_b++){
	if (scaleband_variable[i_sb][x2_d][0][i_b] > scaleband_variable[i_sb][x1_d][0][scaleband_variable[i_sb][x1_d][0].size() - 2]){
	  if (switch_uncombined){
	    logger << LOG_INFO << "scaleband_variable[" << setw(3) << i_sb << "][" << setw(3) << x2_d << "][0][" << setw(3) << i_b << "] = " << scaleband_variable[i_sb][x2_d][0][i_b] << endl;
	    for (int i_o = 0; i_o < scaleband_central_result[i_sb][xl_d].size(); i_o++){
	      for (int i_x = 0; i_x < scaleband_central_result[i_sb][xl_d][i_o].size(); i_x++){
		if (i_o == 0){scaleband_variable[i_sb][xl_d][i_x].push_back(scaleband_variable[i_sb][x2_d][i_x][i_b]);}
		scaleband_central_result[i_sb][xl_d][i_o][i_x].push_back(scaleband_central_result[i_sb][x2_d][i_o][i_x][i_b]);
		scaleband_central_deviation[i_sb][xl_d][i_o][i_x].push_back(scaleband_central_deviation[i_sb][x2_d][i_o][i_x][i_b]);
		scaleband_maximum_result[i_sb][xl_d][i_o][i_x].push_back(scaleband_maximum_result[i_sb][x2_d][i_o][i_x][i_b]);
		scaleband_minimum_result[i_sb][xl_d][i_o][i_x].push_back(scaleband_minimum_result[i_sb][x2_d][i_o][i_x][i_b]);
	      }
	    }
	  }
	  else {
	    logger << LOG_INFO << "INTERBIN scaleband_variable[" << setw(3) << i_sb << "][" << setw(3) << x2_d << "][0][" << setw(3) << i_b << "] = " << scaleband_variable[i_sb][x2_d][0][i_b] << endl;
	    logger << LOG_INFO << "NEWINTERBIN scaleband_variable[" << setw(3) << i_sb << "][" << setw(3) << x1_d << "][0][" << setw(3) << scaleband_variable[i_sb][x1_d][0].size() - 1 << "] = " << scaleband_variable[i_sb][x1_d][0][scaleband_variable[i_sb][x1_d][0].size() - 1] << endl;

	    for (int i_o = 0; i_o < scaleband_central_result[i_sb][xl_d].size(); i_o++){
	      for (int i_x = 0; i_x < scaleband_central_result[i_sb][xl_d][i_o].size(); i_x++){
		if (i_o == 0){scaleband_variable[i_sb][xl_d][i_x].push_back(scaleband_variable[i_sb][x1_d][i_x][scaleband_variable[i_sb][x1_d][i_x].size() - 1]);}
		double temp_result;
		temp_result = (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x1_d][i_x][scaleband_variable[i_sb][x1_d][i_x].size() - 1])
		  / (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x2_d][i_x][i_b]) * scaleband_central_result[i_sb][x2_d][i_o][i_x][i_b];
		scaleband_central_result[i_sb][xl_d][i_o][i_x].push_back(temp_result);

		scaleband_central_deviation[i_sb][xl_d][i_o][i_x].push_back(scaleband_central_deviation[i_sb][x2_d][i_o][i_x][i_b]); // unchanged -> extra uncertainty from approximation
		temp_result = (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x1_d][i_x][scaleband_variable[i_sb][x1_d][i_x].size() - 1])
		  / (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x2_d][i_x][i_b]) * scaleband_maximum_result[i_sb][x2_d][i_o][i_x][i_b];
		scaleband_maximum_result[i_sb][xl_d][i_o][i_x].push_back(temp_result);

		temp_result = (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x1_d][i_x][scaleband_variable[i_sb][x1_d][i_x].size() - 1])
		  / (scaleband_variable[i_sb][x2_d][i_x][i_b + 1] - scaleband_variable[i_sb][x2_d][i_x][i_b]) * scaleband_minimum_result[i_sb][x2_d][i_o][i_x][i_b];
		scaleband_minimum_result[i_sb][xl_d][i_o][i_x].push_back(temp_result);
	      }
	    }

	    switch_uncombined = 1;
	  }
	}
      }
    }
  }
*/




  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    // create K-factor table output here !!!

    for (int i_y = 0; i_y < 1; i_y++){
      vector<string> output_order;
      vector<string> output_order_reference;

      if (i_y == 0){
	output_order.push_back("LO");
	output_order_reference.push_back("");

	output_order.push_back("NLO.EW");
	output_order_reference.push_back("");

	output_order.push_back("NLO.QCD");
	output_order_reference.push_back("");

	output_order.push_back("NNLO.QCD");
	output_order_reference.push_back("");

	output_order.push_back("NNLO.QCD+NLO.EW");
	output_order_reference.push_back("");

	output_order.push_back("NNLO.QCDxNLO.EW");
	output_order_reference.push_back("");

	output_order.push_back("NNLO.QCDxNLO.EW+a");
	output_order_reference.push_back("");

	output_order.push_back("NLO.EW");
	output_order_reference.push_back("LO");

	output_order.push_back("NLO.QCD");
	output_order_reference.push_back("LO");

	output_order.push_back("NNLO.QCD");
	output_order_reference.push_back("NLO.QCD");

	output_order.push_back("NNLO.QCD+NLO.EW");
	output_order_reference.push_back("NNLO.QCD");

	output_order.push_back("NNLO.QCDxNLO.EW");
	output_order_reference.push_back("NNLO.QCD");

	output_order.push_back("NNLO.QCDxNLO.EW+a");
	output_order_reference.push_back("NNLO.QCD");


	//	for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
	for (int x_q = osi->value_qTcut_distribution.size(); x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
	  // only copy&paste: more elegant solution for directory_qTcut !!!
	  string directory_qTcut;
	  string in_directory_qTcut;
	  string out_directory_qTcut;
	  if (x_q == osi->value_qTcut_distribution.size()){
	    directory_qTcut = "";
	    in_directory_qTcut = "";
	    out_directory_qTcut = "";
	  }
	  else {
	    stringstream qTcut_ss;
	    qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	    directory_qTcut = "/" + qTcut_ss.str();
	    in_directory_qTcut = "";
	    out_directory_qTcut = directory_qTcut;
	  }

	  logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
	  for (int i_d = 0; i_d < temporary_distribution.size(); i_d++){
	    if (!switch_output_distribution_table[i_d]){continue;}

	    vector<string> result_name(0);
	    vector<string> result_filename(0);
	    vector<string> result_type(0);
	    vector<double> result_value(0);
	    vector<string> result_unit(0);

	    if (temporary_distribution[i_d].xdistribution_name == "total_rate"){
	      result_name.push_back("baseline cuts");
	      result_filename.push_back("complete");
	      result_type.push_back("value");
	      result_value.push_back(0.);
	      result_unit.push_back("fb");
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "logpT_Zlead" ||
		     temporary_distribution[i_d].xdistribution_name == "logpT_Wlead" ||
		     temporary_distribution[i_d].xdistribution_name == "logpT_Vlead"){
	      stringstream pTVlead_I;
	      pTVlead_I << "$p_{" << char(92) << "mathrm{T,V_1}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTVlead_I.str());
	      result_filename.push_back("pTV1_I");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1.);
	      result_unit.push_back("fb");
	      stringstream pTVlead_C;
	      pTVlead_C << "$p_{" << char(92) << "mathrm{T,V_1}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTVlead_C.str());
	      result_filename.push_back("pTV1_C");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTVlead_M;
	      pTVlead_M << "$p_{" << char(92) << "mathrm{T,V_1}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTVlead_M.str());
	      result_filename.push_back("pTV1_M");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	      /*
	      stringstream pTVlead_MM;
	      pTVlead_MM << "$p_{" << char(92) << "mathrm{T,V_2}}>2" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTVlead_MM.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1995.);
	      result_unit.push_back("ab");
	      */
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "logpT_Zsub" ||
		     temporary_distribution[i_d].xdistribution_name == "logpT_Wsub" ||
		     temporary_distribution[i_d].xdistribution_name == "logpT_Vsub"){
	      stringstream pTVsub_I;
	      pTVsub_I << "$p_{" << char(92) << "mathrm{T,V_2}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTVsub_I.str());
	      result_filename.push_back("pTV2_I");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(0.);
	      result_unit.push_back("fb");
	      stringstream pTVsub_C;
	      pTVsub_C << "$p_{" << char(92) << "mathrm{T,V_2}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTVsub_C.str());
	      result_filename.push_back("pTV2_C");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTVsub_M;
	      pTVsub_M << "$p_{" << char(92) << "mathrm{T,V_2}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTVsub_M.str());
	      result_filename.push_back("pTV2_M");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	      /*
	      stringstream pTVsub_MM;
	      pTVsub_MM << "$p_{" << char(92) << "mathrm{T,V_2}}>2" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTVsub_MM.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1995.);
	      result_unit.push_back("ab");
	      */
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "logm_ZZ" ||
		     temporary_distribution[i_d].xdistribution_name == "logm_WW" ||
		     temporary_distribution[i_d].xdistribution_name == "logm_WZ"){
	      stringstream mVV_I;
	      mVV_I << "$m_{" << char(92) << "mathrm{VV}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(mVV_I.str());
	      result_filename.push_back("mVV_I");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1.);
	      result_unit.push_back("fb");
	      stringstream mVV_C;
	      mVV_C << "$m_{" << char(92) << "mathrm{VV}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(mVV_C.str());
	      result_filename.push_back("mVV_C");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream mVV_M;
	      mVV_M << "$m_{" << char(92) << "mathrm{VV}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(mVV_M.str());
	      result_filename.push_back("mVV_M");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	      stringstream mVV_CC;
	      mVV_CC << "$m_{" << char(92) << "mathrm{VV}}" << char(92) << "gtrsim 200" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(mVV_CC.str());
	      result_filename.push_back("mVV_approxCC");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(199.);
	      result_unit.push_back("fb");
	      stringstream mVV_MM;
	      mVV_MM << "$m_{" << char(92) << "mathrm{VV}}" << char(92) << "gtrsim 2000" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(mVV_MM.str());
	      result_filename.push_back("mVV_approxMM");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1990.);
	      result_unit.push_back("ab");
	      stringstream mVV_MMMM;
	      mVV_MMMM << "$m_{" << char(92) << "mathrm{VV}}" << char(92) << "gtrsim 4000" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(mVV_MMMM.str());
	      result_filename.push_back("mVV_approxMMMM");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(3980.);
	      result_unit.push_back("zb");
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "logpT_lep1"){
	      stringstream pTlep_I;
	      pTlep_I << "$p_{" << char(92) << "mathrm{T," << char(92) << "ell_1}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTlep_I.str());
	      result_filename.push_back("pTlep1_I");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1.);
	      result_unit.push_back("fb");
	      stringstream pTlep_C;
	      pTlep_C << "$p_{" << char(92) << "mathrm{T," << char(92) << "ell_1}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTlep_C.str());
	      result_filename.push_back("pTlep1_C");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTlep_M;
	      pTlep_M << "$p_{" << char(92) << "mathrm{T," << char(92) << "ell_1}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTlep_M.str());
	      result_filename.push_back("pTlep1_M");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "logpT_miss"){
	      stringstream pTmiss_I;
	      pTmiss_I << "$p_{" << char(92) << "mathrm{T,miss}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTmiss_I.str());
	      result_filename.push_back("pTmiss_I");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTmiss_C;
	      pTmiss_C << "$p_{" << char(92) << "mathrm{T,miss}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTmiss_C.str());
	      result_filename.push_back("pTmiss_C");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTmiss_M;
	      pTmiss_M << "$p_{" << char(92) << "mathrm{T,miss}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTmiss_M.str());
	      result_filename.push_back("pTmiss_M");
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	    }
	    /*
	    else if (temporary_distribution[i_d].xdistribution_name == "pT_Zlead" ||
		     temporary_distribution[i_d].xdistribution_name == "pT_Wlead" ||
		     temporary_distribution[i_d].xdistribution_name == "pT_Vlead"){
	      stringstream pTV_I;
	      pTV_I << "$p_{" << char(92) << "mathrm{T,V_1}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_I.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(0.);
	      result_unit.push_back("fb");
	      stringstream pTV_C;
	      pTV_C << "$p_{" << char(92) << "mathrm{T,V_1}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_C.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTV_D;
	      pTV_D << "$p_{" << char(92) << "mathrm{T,V_1}}>500" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_D.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(500.);
	      result_unit.push_back("ab");
	      stringstream pTV_M;
	      pTV_M << "$p_{" << char(92) << "mathrm{T,V_1}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTV_M.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	    }
	    else if (temporary_distribution[i_d].xdistribution_name == "pT_Zlead_combined" ||
		     temporary_distribution[i_d].xdistribution_name == "pT_Wlead_combined" ||
		     temporary_distribution[i_d].xdistribution_name == "pT_Vlead_combined"){
	      stringstream pTV_I;
	      pTV_I << "$p_{" << char(92) << "mathrm{T,V_1}}>0" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_I.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(0.);
	      result_unit.push_back("fb");
	      stringstream pTV_C;
	      pTV_C << "$p_{" << char(92) << "mathrm{T,V_1}}>100" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_C.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(100.);
	      result_unit.push_back("fb");
	      stringstream pTV_D;
	      pTV_D << "$p_{" << char(92) << "mathrm{T,V_1}}>500" << char(92) << "," << char(92) << "mathrm{GeV}$";
	      result_name.push_back(pTV_D.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(500.);
	      result_unit.push_back("ab");
	      stringstream pTV_M;
	      pTV_M << "$p_{" << char(92) << "mathrm{T,V_1}}>1" << char(92) << "," << char(92) << "mathrm{TeV}$";
	      result_name.push_back(pTV_M.str());
	      result_type.push_back("cumulative_above");
	      result_value.push_back(1000.);
	      result_unit.push_back("ab");
	    }
	    */
	    else {continue;}

	    vector<string> name_plot;
	    string temp_sdd = "." + temporary_distribution[i_d].xdistribution_name;

	    name_plot.push_back("Xsection.Kfactor.plot" + temp_sdd);
	    name_plot.push_back("Xsection.Kfactor.norm" + temp_sdd);

	    // no_output_order contains information on where the respective selected orders are stored.
	    vector<int> no_output_order(output_order.size(), -1);
	    vector<int> no_output_order_reference(output_order_reference.size(), -1);
	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      for (int i_o = 0; i_o < yorder.size(); i_o++){
		if (output_order[j_o] == yorder[i_o]->resultdirectory){no_output_order[j_o] = i_o;}
		if (output_order_reference[j_o] == yorder[i_o]->resultdirectory){no_output_order_reference[j_o] = i_o;}
	      }
	    }

	    for (int j_o = 0; j_o < output_order.size(); j_o++){
	      logger << LOG_INFO << left << setw(4) << j_o << setw(20) << output_order[j_o] << " -> " << setw(4) << no_output_order[j_o] << setw(4) << "   reference: " << setw(20) << output_order_reference[j_o] << " -> " << setw(4) << no_output_order_reference[j_o] << endl;
	    }

	    int n_output_version = name_plot.size();
	    for (int i_x = 0; i_x < n_output_version; i_x++){
	      if (name_plot[i_x].substr(0, 21) != "Xsection.Kfactor.norm"){continue;}

	      //	      logger << LOG_INFO << "name_plot[" << i_x << "].substr(0, 21) = " << name_plot[i_x].substr(0, 21) << endl;

	      string outfilename_latex_Kfactor = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];



	      logger << LOG_INFO << "outfilename_latex_Kfactor = " <<  outfilename_latex_Kfactor << endl;

	      ofstream out_table;
	      out_table.open(outfilename_latex_Kfactor.c_str(), ofstream::out | ofstream::trunc);

	      ofstream out_column;
	      ofstream out_column_label;
	      string outfilename_latex_Kfactor_label = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x] + ".label.tex";
	      ///	      string outfilename_latex_Kfactor_label = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + "Xsection.Kfactor.norm.label.tex";
	      out_column.open(outfilename_latex_Kfactor_label.c_str(), ofstream::out | ofstream::trunc);
	      //	      out_column << " & ";
    	      out_column.close();

	      vector<string> outfilename_latex_Kfactor_column(result_name.size());
	      vector<string> outfilename_latex_Kfactor_column_label(result_name.size());
	      for (int j_r = 0; j_r < result_name.size(); j_r++){
		outfilename_latex_Kfactor_column[j_r] = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x] + "." + result_filename[j_r] + ".tex";
		outfilename_latex_Kfactor_column_label[j_r] = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x] + "." + result_filename[j_r] + ".label.tex";
		/*
		stringstream temp_ss;
		temp_ss << "column_" << j_r + 1;
		outfilename_latex_Kfactor_column[j_r] = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x] + "." + temp_ss.str() + ".tex";
		///		outfilename_latex_Kfactor_column[j_r] = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + "Xsection.Kfactor.norm" + temp_sdd + "." + temp_ss.str() + ".tex";
		outfilename_latex_Kfactor_column_label[j_r] = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x] + ".label." + temp_ss.str() + ".tex";
		*/
	      }
	      for (int j_r = 0; j_r < result_name.size(); j_r++){
		out_column.open(outfilename_latex_Kfactor_column[j_r].c_str(), ofstream::out | ofstream::trunc);
		out_column.close();
		out_column_label.open(outfilename_latex_Kfactor_column_label[j_r].c_str(), ofstream::out | ofstream::trunc);
		out_column_label.close();
	      }

	      out_table << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	      out_table << char(92) << "begin{table}" << endl;
	      out_table << char(92) << "begin{center}" << endl;
	      out_table << char(92) << "begin{tabular}{|c|";
	      for (int j_r = 0; j_r < result_name.size(); j_r++){out_table << "c";}
	      out_table << "|}" << endl;


	      out_column.open(outfilename_latex_Kfactor_label.c_str(), ofstream::out | ofstream::trunc);
	      out_column << endl;
	      out_column << char(92) << "hline" << endl;
   	      out_column.close();

	      out_table << char(92) << " & ";
	      for (int j_r = 0; j_r < result_name.size(); j_r++){

		out_table << result_name[j_r];
		if (j_r + 1 < result_name.size()){out_table << " & ";}
		else {out_table << " " << char(92) << char(92) << endl;}

		out_column.open(outfilename_latex_Kfactor_column[j_r].c_str(), ofstream::out | ofstream::app);
		out_column << char(92) << "multicolumn{1}{c|}{" << result_name[j_r] << "}" << endl;
		out_column << endl;
		out_column.close();

		out_column_label.open(outfilename_latex_Kfactor_column_label[j_r].c_str(), ofstream::out | ofstream::app);
		out_column_label << char(92) << "multicolumn{1}{c|}{" << result_name[j_r] << "}" << endl;
		out_column_label << endl;
		out_column_label.close();
	      }
	      out_table << char(92) << "hline" << endl;


	      for (int j_o = 0; j_o < output_order.size(); j_o++){
		int i_o = no_output_order[j_o];
		int i_or = no_output_order_reference[j_o];
		if (i_o == -1){continue;}

		if (output_order_reference[j_o] == ""){
		  stringstream temp_contribution_ss;
		  temp_contribution_ss << "$" << latex_name_order(output_order[j_o]) << "$";
		  //		  temp_contribution_ss << char(92) << ",$(" << char(92) << "mathrm{" << result_unit[j_r] << "})$";
		  out_table << setw(50) << temp_contribution_ss.str() << " & ";

		  out_column.open(outfilename_latex_Kfactor_label.c_str(), ofstream::out | ofstream::app);
		  out_column << temp_contribution_ss.str() << endl;
		  //",$(" << char(92) << "mathrm{" << result_unit[j_r] << "})$" <<
		  out_column.close();

		  for (int j_r = 0; j_r < result_name.size(); j_r++){
		    double temp_unit_factor = osi->determine_unit_factor(result_unit[j_r]) / osi->determine_unit_factor(osi->unit_distribution);

		    out_column_label.open(outfilename_latex_Kfactor_column_label[j_r].c_str(), ofstream::out | ofstream::app);
		    stringstream temp_contributionunit_ss;
		    temp_contributionunit_ss << "$" << latex_name_order(output_order[j_o]) << "$";
		    temp_contributionunit_ss << char(92) << ",$[" << char(92) << "mathrm{" << result_unit[j_r] << "}]$";
		    out_column_label << setw(80) << temp_contributionunit_ss.str() << endl;
		    out_column_label.close();


		    int x_b = -1;
		    for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
		      if (scaleband_variable[i_sb][i_d][i_x][i_b] >= result_value[j_r]){x_b = i_b; break;}
		    }
		    if (x_b != -1){

		      stringstream temp_result_ss;
		      if (result_type[j_r] == "value"){
			temp_result_ss << "$" << output_result_deviation(temp_unit_factor * scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], temp_unit_factor * scaleband_central_deviation[i_sb][i_d][i_o][i_x][x_b], 1)
				       << "^{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], scaleband_maximum_result[i_sb][i_d][i_o][i_x][x_b], 1) << "}"
				       << "_{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], scaleband_minimum_result[i_sb][i_d][i_o][i_x][x_b], 1) << "}$";
		      }
		      else if (result_type[j_r] == "cumulative_above"){
			double result_central_order = 0.;
			double deviation_central_order = 0.;
			double result_maximum_order = 0.;
			double result_minimum_order = 0.;
			for (int i_b = x_b; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){

			  result_central_order += scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
			  deviation_central_order += pow(scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b], 2);
			  result_maximum_order += scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b];
			  result_minimum_order += scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b];

			  //			  logger << LOG_INFO << setw(50) << result_name[j_r] << "   " << setw(15) << scaleband_variable[i_sb][i_d][i_x][i_b] << "   "  << setw(15) << scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] << "   "  << setw(15) << result_central_order << endl;
			  /*
			  // strange !!!
			  result_central_order += scaleband_central_result[i_sb][i_d][i_o][i_x][x_b];
			  deviation_central_order += pow(scaleband_central_deviation[i_sb][i_d][i_o][i_x][x_b], 2);
			  result_maximum_order += scaleband_maximum_result[i_sb][i_d][i_o][i_x][x_b];
			  result_minimum_order += scaleband_minimum_result[i_sb][i_d][i_o][i_x][x_b];
			  */
			}
			deviation_central_order = sqrt(deviation_central_order);
			temp_result_ss << "$" << output_result_deviation(temp_unit_factor * result_central_order, temp_unit_factor * deviation_central_order, 1)
				       << "^{" << output_latex_percent(result_central_order, result_maximum_order, 1) << "}"
				       << "_{" << output_latex_percent(result_central_order, result_minimum_order, 1) << "}$";
		      }
		      stringstream temp_result_unit_ss;
		      temp_result_unit_ss << char(92) << ",$" << char(92) << "mathrm{" << result_unit[j_r] << "}$";

		      out_table << setw(50) << temp_result_ss.str() << setw(15) << temp_result_unit_ss.str();

		      if (j_r + 1 < result_name.size()){out_table << " & ";}
		      else {out_table << " " << char(92) << char(92) << endl;}

		      out_column.open(outfilename_latex_Kfactor_column[j_r].c_str(), ofstream::out | ofstream::app);
		      out_column << setw(50) << temp_result_ss.str() << endl;
		      out_column.close();
		    }
		  }
		}
		else {
		  if (i_or == -1){continue;}
		  stringstream temp_contribution_ss;
		  temp_contribution_ss << "$" << char(92) << "frac{" << latex_name_order(output_order[j_o]) << "}{" << latex_name_order(output_order_reference[j_o]) << "}-1" << "$";
		  out_table << setw(50) << temp_contribution_ss.str() << " & ";

		  out_column.open(outfilename_latex_Kfactor_label.c_str(), ofstream::out | ofstream::app);
		  out_column << temp_contribution_ss.str() << endl;
		  out_column.close();

		  for (int j_r = 0; j_r < result_name.size(); j_r++){

		    out_column_label.open(outfilename_latex_Kfactor_column_label[j_r].c_str(), ofstream::out | ofstream::app);
		    //		    stringstream temp_contributionunit_ss;
		    //		    temp_contributionunit_ss << "$" << latex_name_order(output_order[j_o]) << "$";
		    //		    temp_contributionunit_ss << char(92) << ",$(" << char(92) << "mathrm{" << result_unit[j_r] << "})$"
		    out_column_label << setw(80) << temp_contribution_ss.str() << endl;
		    out_column_label.close();


		    int x_b = -1;
		    for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
		      if (scaleband_variable[i_sb][i_d][i_x][i_b] >= result_value[j_r]){x_b = i_b; break;}
		    }
		    if (x_b != -1){

		      stringstream temp_result_ss;
		      if (result_type[j_r] == "value"){
			temp_result_ss << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_or][i_x][x_b], scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], 1) << "$";
		      }
		      else if (result_type[j_r] == "cumulative_above"){
			double result_central_order = 0.;
			//			double deviation_central_order = 0.;
			double result_reference = 0.;
			for (int i_b = x_b; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
			  result_central_order += scaleband_central_result[i_sb][i_d][i_o][i_x][i_b];
			  //			  deviation_central_order += pow(scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], 2);
			  result_reference += scaleband_central_result[i_sb][i_d][i_or][i_x][i_b];
			  /*
			  // looks wrong !!!
			  result_central_order += scaleband_central_result[i_sb][i_d][i_o][i_x][x_b];
			  //			  deviation_central_order += pow(scaleband_central_result[i_sb][i_d][i_o][i_x][x_b], 2);
			  result_reference += scaleband_central_result[i_sb][i_d][i_or][i_x][x_b];
			  */
			}
			temp_result_ss << "$" << output_latex_percent(result_reference, result_central_order, 1) << "$";
		      }
		      out_table << setw(50) << temp_result_ss.str();

		      if (j_r + 1 < result_name.size()){out_table << " & ";}
		      else {out_table << " " << char(92) << char(92) << endl;}

		      out_column.open(outfilename_latex_Kfactor_column[j_r].c_str(), ofstream::out | ofstream::app);
		      out_column << setw(60) << temp_result_ss.str() << endl;
		      out_column.close();
		    }
		  }
		}
	      }

	      out_table << char(92) << "hline" << endl;

	      out_table << char(92) << "end{tabular}" << endl;
	      out_table << char(92) << "end{center}" << endl;
	      out_table << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	      out_table << char(92) << "end{table}" << endl;
	      out_table.close();
	    }
	  }
	}
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_distribution_table_order_nNNLOQCDxEW(){
  Logger logger("summary_generic::output_distribution_table_order_nNNLOQCDxEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

    // create table output here (without K-factors etc.) !!!
  //  logger << LOG_INFO << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " <<  scaleband_variable[i_sb][i_d][i_x].size() << endl;

  logger << LOG_INFO << "scaleband_variable.size() = " <<  scaleband_variable.size() << endl;
  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    logger << LOG_INFO << "scaleband_variable[" << i_sb << "].size() = " <<  scaleband_variable[i_sb].size() << endl;
    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
     // only copy&paste: more elegant soultion for directory_qTcut !!!
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_INFO << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution_table[i_d]){continue;}
	logger << LOG_INFO << "osi->extended_distribution[" << i_d << "].xdistribution_name = " <<  osi->extended_distribution[i_d].xdistribution_name << endl;
	logger << LOG_INFO << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " <<  scaleband_variable[i_sb][i_d].size() << endl;

	vector<string> name_plot;
	string temp_sdd = "." + osi->extended_distribution[i_d].xdistribution_name;

	for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
	  if (i_d < osi->dat.size()){
	    name_plot.push_back("table." + output_distribution_format[i_d][i_f] + "." + osi->extended_distribution[i_d].xdistribution_name + ".dat");
	  }
	  else {
	    int i_ddd = i_d - osi->dat.size();
	    if (output_distribution_format[i_d][i_f] == "norm.plot.split"){
	      for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
		stringstream name_split_bin;
		name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
		name_plot.push_back("table.norm.two." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	      }
	    }
	  }
	}

	/*
	name_plot.push_back("table.plot" + temp_sdd + ".dat");
	name_plot.push_back("table.norm" + temp_sdd + ".dat");

	if (i_d >= osi->dat.size()){
	  int i_ddd = i_d - osi->dat.size();

	  // recombined distributions (essentially for validation)
	  stringstream name_rec;
	  name_rec << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name;
	  //	  string s0;
	  name_plot.push_back("table.plot" + name_rec.str() + ".tex");
	  name_plot.push_back("table.norm" + name_rec.str()  + ".tex");

	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    stringstream name_split_bin;
	    name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	    stringstream name_split_lt;
	    name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	    stringstream name_split_ge;
	    name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	    name_plot.push_back("table.norm.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.norm.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.plot.norm.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");
	    name_plot.push_back("table.plot.plot.split." + osi->dddat[i_ddd].name + name_split_bin.str() + ".tex");

	    name_plot.push_back("table.norm.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("table.norm.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	    name_plot.push_back("table.plot.two." + osi->dddat[i_ddd].name + name_split_lt.str() + ".tex");
	    name_plot.push_back("table.plot.two." + osi->dddat[i_ddd].name + name_split_ge.str() + ".tex");
	  }
	}

*/


	vector<vector<string> > output_order(4);
	vector<string> output_norm(4);

	output_order[0].push_back("LO");
	output_order[0].push_back("NLO.QCD");
	output_order[0].push_back("qqNNLO.QCD");
	output_norm[0] = "NLO.QCD";

	output_order[1].push_back("ggLO");
	output_order[1].push_back("ggNLO.QCD.gg");
	output_order[1].push_back("ggNLO.QCD");
	output_norm[1] = "ggLO";

	output_order[2].push_back("NNLO.QCD");
	output_order[2].push_back("nNNLO.QCD");
	output_norm[2] = "NLO.QCD";

	output_order[3].push_back("nNNLO.QCD+NLO.EW");
	output_order[3].push_back("nNNLO.QCDxNLO.EWqq");
	output_order[3].push_back("nNNLO.QCDxNLO.EW");
	output_norm[3] = "nNNLO.QCD";

	/*
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";

	vector<string> output_order(4);
	output_order[0] = "LO";
	output_order[1] = "NLO.QCD";
	output_order[2] = "nNLO.QCD+gg";
	output_order[3] = "NNLO.QCD";
	*/

	//	vector<int> no_output_order(output_order.size(), -1);
	vector<vector<int> > no_output_order(output_order.size());
	vector<int> no_output_norm(output_order.size());
	for (int i_b = 0; i_b < output_order.size(); i_b++){
	  no_output_order[i_b].resize(output_order[i_b].size(), -1);
	  for (int i_o = 0; i_o < yorder.size(); i_o++){
	    for (int j_o = 0; j_o < output_order[i_b].size(); j_o++){
	      if (output_order[i_b][j_o] == yorder[i_o]->resultdirectory){no_output_order[i_b][j_o] = i_o;}// break;}
	    }
	    if (output_norm[i_b] == yorder[i_o]->resultdirectory){no_output_norm[i_b] = i_o;}
	  }
	}

	for (int i_b = 0; i_b < output_order.size(); i_b++){
	  for (int j_o = 0; j_o < output_order[i_b].size(); j_o++){
	    logger << LOG_INFO << left
		   << setw(4) << i_b
		   << setw(4) << j_o
		   << setw(20) << output_order[i_b][j_o]
		   << " -> "
		   << setw(4) << no_output_order[i_b][j_o] << endl;
	  }
	}

	int n_output_version = name_plot.size();
	for (int i_x = 0; i_x < n_output_version; i_x++){
	  logger << LOG_INFO << "i_x = " << i_x << endl;
	  logger << LOG_INFO << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " <<  scaleband_variable[i_sb][i_d][i_x].size() << endl;

	  string outfilename_latex_table = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + name_plot[i_x];

	  logger << LOG_INFO << "outfilename_latex_table = " <<  outfilename_latex_table << endl;

	  ofstream outf;
	  outf.open(outfilename_latex_table.c_str(), ofstream::out | ofstream::trunc);

	  outf << char(92) << "renewcommand" << char(92) << "arraystretch{1.5}" << endl;
	  outf << char(92) << "begin{table}" << endl;
	  outf << char(92) << "begin{center}" << endl;
	  outf << char(92) << "begin{tabular}{|c|c|c|}";
	  /*	  for (int j_o = 0; j_o < output_order.size(); j_o++){
	    int i_o = no_output_order[j_o];
	    if (i_o == -1){continue;}
	    outf << "c|";
	  }
	  outf << "}" << endl;
	  */
	  outf << char(92) << "hline" << endl;
	  outf << "$" << char(92) << "sqrt{s}" << char(92) << ", (" << char(92) << "mathrm{TeV})$ & & " << char(92) << char(92) << endl;

	  for (int i_b = 0; i_b < scaleband_variable[i_sb][i_d][i_x].size() - 1; i_b++){
	    outf << char(92) << "nameobservable{" << scaleband_variable[i_sb][i_d][i_x][i_b] << "} & & " << char(92) << char(92) << endl;

	    for (int i_u = 0; i_u < output_order.size(); i_u++){
	      //   	      outf << " << scaleband_variable[i_sb][i_d][i_x][i_b] << "} &" << endl;
	      stringstream temp_result_name;
	      temp_result_name << "$" << char(92) << "sigma" << char(92) << ", (" << char(92) << "mathrm{" << osi->unit_distribution << "})$";
	      stringstream temp_Kfactor_name;
	      temp_Kfactor_name << "$" << char(92) << "sigma" << char(92) << ";/" << latex_name_order(output_norm[i_u]) << "-1$";
	      outf << setw(50) << scaleband_variable[i_sb][i_d][i_x][i_b] << " & " << setw(50) << temp_result_name.str() << " & " << setw(50) << temp_Kfactor_name.str() << " " << char(92) << char(92) << endl;

	      for (int j_o = 0; j_o < output_order[i_u].size(); j_o++){
		int i_o = no_output_order[i_u][j_o];
		int i_n = no_output_norm[i_u];
		if (i_o == -1){continue;}

		stringstream temp_result;
		temp_result << "$" << output_result_deviation(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] * 2, scaleband_central_deviation[i_sb][i_d][i_o][i_x][i_b] * 2, 1)
			    << "^{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] * 2, scaleband_maximum_result[i_sb][i_d][i_o][i_x][i_b] * 2, 1) << "}"
			    << "_{" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_o][i_x][i_b] * 2, scaleband_minimum_result[i_sb][i_d][i_o][i_x][i_b] * 2, 1) << "}$";

		stringstream temp_Kfactor;
		temp_Kfactor << "$" << output_latex_percent(scaleband_central_result[i_sb][i_d][i_n][i_x][i_b], scaleband_central_result[i_sb][i_d][i_o][i_x][i_b], 1) << "$";

		outf << setw(50) << latex_name_order(output_order[i_u][j_o]) << " & " << setw(50) << temp_result.str() << " & " << setw(50) << temp_Kfactor.str() << " " << char(92) << char(92) << endl;

	      }
	      outf << char(92) << "hline" << endl;
	    }
	    outf << char(92) << "hline" << endl;
	  }

	  outf << char(92) << "end{tabular}" << endl;
	  outf << char(92) << "end{center}" << endl;
	  outf << char(92) << "caption{" << char(92) << "captiontext}" << endl;
	  outf << char(92) << "end{table}" << endl;
	  outf.close();
	}
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



