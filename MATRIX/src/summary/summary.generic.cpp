#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
summary_generic::summary_generic(){}

summary_generic::summary_generic(munich * xmunich){
  Logger logger("summary_generic::summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG << "finished" << endl;
}


void summary_generic::get_summary(){
  Logger logger("summary_generic::get_summary");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "csi->type_perturbative_order = " << csi->type_perturbative_order << endl;
  logger << LOG_INFO << "csi->type_contribution = " << csi->type_contribution << endl;
  logger << LOG_INFO << "csi->type_correction = " << csi->type_correction << endl;
  
  // default values:
  run_min_n_step = 10;
  run_factor_max = 2;
  run_min_n_event = 50000;
  run_max_time_per_job = 2 * 60 * 60;
  run_min_number_of_jobs_for_one_channel = 3;
  average_factor = 0;
  switch_generate_rundirectories = 0;
  no_qTcut_runtime_estimate = 0;
  deviation_tolerance_factor = 5.;

  switch_extrapolation_result = 2;
  switch_extrapolation_distribution = 2;

  min_qTcut_extrapolation = 0.05;
  max_qTcut_extrapolation = 0.5;
  error_extrapolation_range_chi2 = 4.;

  min_max_value_extrapolation_range = 0.25;
  min_n_qTcut_extrapolation_range = 10;

  switch_output_plot = 1;
  switch_output_qTcut = 1;
  switch_output_result = 2;
  switch_output_overview = 3;
  // 0 - none
  // 1 - order
  // 2 - order + list
  // 3 - order + list + contribution
  // 4 - order + list + contribution + subprocess


  switch_output_subprocess = 0;
  // 0 - none
  // 1 - overview
  // 2 - overview + plot
  // 3 - overview + plot + result
  switch_output_contribution = 0;
  switch_output_list = 0;
  switch_output_order = 1;


  switch_output_table_order = 0;
  switch_output_table_Kfactor = 0;
  switch_output_table_crosssection_Kfactor = 0;
  switch_output_table_IS_splitting = 0;
  switch_output_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW = 0;

  if (!osi->switch_moment){osi->n_moments = 1;}

  infix_name_moment.resize(osi->n_moments);
  int start_i_m = 0; // temporary !!!
  for (int i_m = start_i_m; i_m < osi->n_moments; i_m++){
    stringstream temp_ss;
    if (i_m == 0){}
    else {temp_ss << ".moment_" << i_m;}
    infix_name_moment[i_m] = temp_ss.str();
  }
  x_m = 0;

  // readin from specified input file:
  readin_combination_infile();

  system_execute(logger, "mkdir " + final_resultdirectory);

  for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){logger << LOG_INFO << "list_contribution_file[" << i_l << "] = " << list_contribution_file[i_l] << endl;}

  resultdirectory.resize(list_contribution_file.size());
  osi->initialization_result();
  // shifted to initialization_scaleset_CV():  osi->initialization_CV();

  if (osi->switch_TSV){initialization_scaleset_TSV();}
  // Check if "if (osi->switch_CV){" can cause trouble here !!!
  initialization_scaleset_CV();

  subgroup.push_back(csi->process_class); // ???

  // Initialization of xlist from 'list_contribution_file':
  initialization_summary_list();


  // qTcut information must come from the individual list -> investigate in initialization_summary_list !!!

  for (int i_o = 0; i_o < yorder.size(); i_o++){
    yorder[i_o]->active_qTcut = 0;
    yorder[i_o]->output_n_qTcut = 1;
    yorder[i_o]->selection_n_qTcut = 1;
    for (int i_c = 0; i_c < yorder[i_o]->combination.size(); i_c++){
      for (int i_l = 0; i_l < yorder[i_o]->contribution_file.size(); i_l++){
	int x_l = mapping_contribution_file[yorder[i_o]->contribution_file[i_l]];
	if (xlist[x_l]->active_qTcut){
	  yorder[i_o]->active_qTcut = xlist[x_l]->active_qTcut;
	  yorder[i_o]->output_n_qTcut = xlist[x_l]->output_n_qTcut;
	  yorder[i_o]->selection_n_qTcut = xlist[x_l]->selection_n_qTcut;
	}
      }
    }
  }

  
  for (int i_l = 0; i_l < xlist.size(); i_l++){
    logger << LOG_INFO << "xlist[" << i_l << "]->resultdirectory = " << xlist[i_l]->resultdirectory << "   active_qTcut = " << xlist[i_l]->active_qTcut << "   output_n_qTcut = " << setw(3) << xlist[i_l]->output_n_qTcut << "   selection_n_qTcut = " << xlist[i_l]->selection_n_qTcut << endl;
    logger << LOG_INFO << "list   " << "   active_qTcut = " << xlist[i_l]->active_qTcut << "   output_n_qTcut = " << setw(3) << xlist[i_l]->output_n_qTcut << "   selection_n_qTcut = " << xlist[i_l]->selection_n_qTcut << endl;
    for (int i_c = 0; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      logger << LOG_INFO << "i_c = " << i_c << "   active_qTcut = " << xlist[i_l]->xcontribution[i_c]->active_qTcut << "   output_n_qTcut = " << setw(3) << xlist[i_l]->xcontribution[i_c]->output_n_qTcut << "   selection_n_qTcut = " << xlist[i_l]->xcontribution[i_c]->selection_n_qTcut << endl;
    }
  }


  for (int i_o = 0; i_o < yorder.size(); i_o++){
    logger << LOG_INFO << "yorder[" << i_o << "].resultdirectory = " << setw(16) << yorder[i_o]->resultdirectory << "   active_qTcut = " << yorder[i_o]->active_qTcut << "   output_n_qTcut = " << setw(3) << yorder[i_o]->output_n_qTcut << "   selection_n_qTcut = " << yorder[i_o]->selection_n_qTcut << endl;
  }
  

  if (order == "result"){
    for (int i_l = 0; i_l < xlist.size(); i_l++){
      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){xlist[i_l]->xcontribution[i_c]->readin_contribution_remove_run();}
    }

    for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){
      logger << LOG_DEBUG_VERBOSE << "collect_contribution:   list_contribution_file[" << i_l << "] = " << list_contribution_file[i_l] << endl;

      if (osi->switch_CV){xlist[i_l]->collect_contribution_result_CV();}
      if (osi->switch_TSV){xlist[i_l]->collect_contribution_result_TSV();}
    }

    for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){
      logger << LOG_DEBUG_VERBOSE << "readin_runtime:   list_contribution_file[" << i_l << "] = " << list_contribution_file[i_l] << endl;
      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){xlist[i_l]->xcontribution[i_c]->readin_runtime_contribution();}
      xlist[i_l]->calculate_runtime();
    }

    for (int i_l = 0; i_l < xlist.size(); i_l++){
      for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
	for (int i_p = 1; i_p < xlist[i_l]->xcontribution[i_c]->xsubprocess.size(); i_p++){
	  if (osi->switch_CV){
	    vector<vector<vector<double> > > ().swap(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_run_CV);
	    vector<vector<vector<double> > > ().swap(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->deviation_run_CV);
	  }
	  if (osi->switch_TSV){
	    vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->result_run_TSV);
	    vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->deviation_run_TSV);
	  }
	}
      }
    }

    for (int i_o = 0; i_o < yorder.size(); i_o++){
      for (int i_l = 0; i_l < yorder[i_o]->contribution_file.size(); i_l++){
	int x_l = mapping_contribution_file[yorder[i_o]->contribution_file[i_l]];
	yorder[i_o]->xlist.push_back(xlist[x_l]);
      }
    }

    if (osi->switch_CV){collect_order_result_CV();} // partially contains runtime evaluation !!!
    if (osi->switch_TSV){collect_order_result_TSV();}

    determination_runtime_result();

    // add scaleband output for cross sections ???

  }



  if (order == "distribution"){
    initialization_distribution();

    if (osi->switch_CV){for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){xlist[i_l]->collect_contribution_distribution_CV();}}
    if (osi->switch_TSV){for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){xlist[i_l]->collect_contribution_distribution_TSV();}}

    for (int i_o = 0; i_o < yorder.size(); i_o++){
      logger << LOG_DEBUG << "yorder[i_o = " << i_o << "].contribution_file.size() = " << yorder[i_o]->contribution_file.size() << endl;
      for (int i_l = 0; i_l < yorder[i_o]->contribution_file.size(); i_l++){
	int x_l = mapping_contribution_file[yorder[i_o]->contribution_file[i_l]];
	yorder[i_o]->xlist.push_back(xlist[x_l]);
      }
    }
    if (osi->switch_CV){collect_order_distribution_CV();}
    if (osi->switch_TSV){collect_order_distribution_TSV();}

    logger << LOG_INFO << "infilename_scaleband = " << infilename_scaleband << endl;

    logger << LOG_DEBUG << "clear xlist since not needed any longer !!!" << endl;
    for (int i_l = 0; i_l < xlist.size(); i_l++){delete xlist[i_l];}
    xlist.clear();
    logger << LOG_DEBUG << "clear xlist done." << endl;

    if (infilename_scaleband != ""){
      logger << LOG_INFO << "Determination of scaleband is started - final_resultdirectory = " << final_resultdirectory << " ." << endl;
      determine_scaleband();
    }

    // add runtime estimate based on a selected distribution / a number of selected distributions !!!

  }
  if (order == "scaleband"){
    initialization_distribution();

    if (infilename_scaleband != ""){
      logger << LOG_INFO << "Determination of scaleband is started - final_resultdirectory = " << final_resultdirectory << " ." << endl;
      determine_scaleband();
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void summary_generic::determine_scaleband(){
  Logger logger("summary_generic::determine_scaleband");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<string> subgroup;
  //  int plotmode = 0;

  logger << LOG_DEBUG << "final_resultdirectory = " << final_resultdirectory << endl;

  readin_infile_scaleband();

  logger << LOG_DEBUG_VERBOSE << "osi->value_qTcut_distribution.size() = " << osi->value_qTcut_distribution.size() << endl;

  scaleband_variable.resize(outpath_scaleband.size(), vector<vector<vector<double> > > (osi->extended_distribution.size(), vector<vector<double> > ()));
  scaleband_central_result.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));
  scaleband_central_deviation.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));
  scaleband_minimum_result.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));
  scaleband_minimum_deviation.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));
  scaleband_maximum_result.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));
  scaleband_maximum_deviation.resize(outpath_scaleband.size(), vector<vector<vector<vector<double> > > > (osi->extended_distribution.size(), vector<vector<vector<double> > > (yorder.size(), vector<vector<double> > ())));

  for (int i_sb = 0; i_sb < outpath_scaleband.size(); i_sb++){
    logger << LOG_INFO << "Determination of scaleband for " << outpath_scaleband[i_sb] << endl;
    int x_v = -1;
    for (int i_v = 0; i_v < inpath_scalevariation[i_sb].size(); i_v++){
      if (inpath_scalecentral[i_sb] == inpath_scalevariation[i_sb][i_v]){x_v = i_v; break;}
    }
    if (x_v == -1){
      x_v = inpath_scalevariation[i_sb].size();
      inpath_scalevariation[i_sb].push_back(inpath_scalecentral[i_sb]);
    }

    system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scaleband[i_sb]);
    system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalegeneva[i_sb]);
    system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalecentral[i_sb]);
    system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalemax[i_sb]);
    system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalemin[i_sb]);

    for (int x_q = 0; x_q < osi->value_qTcut_distribution.size() + 1; x_q++){
      string directory_qTcut;
      string in_directory_qTcut;
      string out_directory_qTcut;
      if (x_q == osi->value_qTcut_distribution.size()){
	directory_qTcut = "";
	in_directory_qTcut = "";
	out_directory_qTcut = "";
      }
      else {
	stringstream qTcut_ss;
	qTcut_ss << "qTcut-" << osi->value_qTcut_distribution[x_q];
	directory_qTcut = "/" + qTcut_ss.str();
	in_directory_qTcut = "";
	out_directory_qTcut = directory_qTcut;
      }

      logger << LOG_DEBUG << "x_q = " << x_q << "   directory_qTcut = " << directory_qTcut << endl;

      if (x_q != osi->value_qTcut_distribution.size()){
	system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut);
	system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalegeneva[i_sb] + directory_qTcut);
	system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalecentral[i_sb] + directory_qTcut);
	system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalemax[i_sb] + directory_qTcut);
	system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath_scalemin[i_sb] + directory_qTcut);
      }



      string infilename;

      char LineBuffer[1024];
      string s0;
      vector<string> vs0, vs1(1);
      logger << LOG_DEBUG << "osi->extended_distribution.size() = " << osi->extended_distribution.size() << endl;
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	logger << LOG_INFO << "Determination of scaleband for " << outpath_scaleband[i_sb] << " : " << osi->extended_distribution[i_d].xdistribution_name << endl;

	logger << LOG_DEBUG << "switch_output_distribution[" << i_d << "] = " << switch_output_distribution[i_d] << endl;
	if (!switch_output_distribution[i_d]){continue;}
	logger << LOG_DEBUG << "osi->extended_distribution[" << i_d << "].xdistribution_name = " << osi->extended_distribution[i_d].xdistribution_name << endl;

	for (int i_o = 0; i_o < yorder.size(); i_o++){
	  logger << LOG_DEBUG << "(!yorder[i_o]->active_qTcut && x_q < osi->value_qTcut_distribution.size()) = " << (!yorder[i_o]->active_qTcut && x_q < osi->value_qTcut_distribution.size()) << endl;
	  logger << LOG_DEBUG << "osi->value_qTcut_distribution.size() = " << osi->value_qTcut_distribution.size() << endl;
	  logger << LOG_DEBUG << "yorder[" << i_o << "].active_qTcut = " << yorder[i_o]->active_qTcut << endl;

	  if (!yorder[i_o]->active_qTcut && x_q < osi->value_qTcut_distribution.size()){continue;}

	  vector<string> format_plot;
	  vector<string> name_plot;

	  string temp_sdd = osi->extended_distribution[i_d].xdistribution_name + ".." + yorder[i_o]->resultdirectory;

	  format_plot.push_back("plot");
	  name_plot.push_back(temp_sdd + ".dat");

	  format_plot.push_back("norm");
	  name_plot.push_back(temp_sdd + ".dat");

	  if (i_d >= osi->dat.size()){
	    int i_ddd = i_d - osi->dat.size();

	    // recombined distributions (essentially for validation)
	    stringstream name_rec;
	    name_rec << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name << ".." << yorder[i_o]->resultdirectory;

	    format_plot.push_back("plot.rec");
	    name_plot.push_back(name_rec.str() + ".dat");

	    format_plot.push_back("norm.rec");
	    name_plot.push_back(name_rec.str() + ".dat");

	    for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	      stringstream name_split_bin;
	      name_split_bin << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1];
	      stringstream name_split_lt;
	      name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];
	      stringstream name_split_ge;
	      name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1];

	      format_plot.push_back("norm.norm.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.plot.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.norm.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.plot.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.norm.split_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.plot.split_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.norm.split_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.plot.split_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.ge_lt_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.ge_lt_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.ge_lt_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.ge_lt_in_first");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	    }
	    for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	      stringstream name_split_bin;
	      name_split_bin << "_" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2] << "-" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2 + 1];
	      stringstream name_split_lt;
	      name_split_lt << "_lt" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2];
	      stringstream name_split_ge;
	      name_split_ge << "_ge" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2];

	      format_plot.push_back("norm.norm.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.plot.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.norm.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.plot.split");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.norm.split_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.plot.split_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.norm.split_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.plot.split_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_bin.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.two");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	      format_plot.push_back("norm.ge_lt_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("norm.ge_lt_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.ge_lt_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_lt.str() + ".." + yorder[i_o]->resultdirectory + ".dat");

	      format_plot.push_back("plot.ge_lt_in_second");
	      name_plot.push_back(osi->dddat[i_ddd].name + name_split_ge.str() + ".." + yorder[i_o]->resultdirectory + ".dat");


	    }
	  }

	  int n_max_output_version = name_plot.size();
	  logger << LOG_DEBUG << "n_max_output_version = " << n_max_output_version << endl;

	  vector<string> selection_format_plot;
	  vector<string> selection_name_plot;

	  for (size_t i_x = 0; i_x < format_plot.size(); i_x++){
	    for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
	      if (format_plot[i_x] == output_distribution_format[i_d][i_f]){
		if      (format_plot[i_x] == "plot.plot.split_in_first" || format_plot[i_x] == "plot.plot.split_in_second"){selection_format_plot.push_back("plot.plot.split");}
		else if (format_plot[i_x] == "plot.norm.split_in_first" || format_plot[i_x] == "plot.norm.split_in_second"){selection_format_plot.push_back("plot.norm.split");}
		else if (format_plot[i_x] == "norm.plot.split_in_first" || format_plot[i_x] == "norm.plot.split_in_second"){selection_format_plot.push_back("norm.plot.split");}
		else if (format_plot[i_x] == "norm.norm.split_in_first" || format_plot[i_x] == "norm.norm.split_in_second"){selection_format_plot.push_back("norm.norm.split");}
		else if (format_plot[i_x] == "plot.ge_lt_in_first" || format_plot[i_x] == "plot.ge_lt_in_second"){selection_format_plot.push_back("plot.two");}
		else if (format_plot[i_x] == "norm.ge_lt_in_first" || format_plot[i_x] == "norm.ge_lt_in_second"){selection_format_plot.push_back("norm.two");}
		else {selection_format_plot.push_back(format_plot[i_x]);}
		selection_name_plot.push_back(name_plot[i_x]);
	      }
	    }
	  }


	  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
	    logger << LOG_INFO << "output_distribution_format[" << setw(3) << i_d << "][" << setw(3) << i_f << "] = " << setw(20) << output_distribution_format[i_d][i_f] << "   " << "selection_format_plot[" << setw(3) << i_f << "] = " << setw(20) << selection_format_plot[i_f] << "   " << "selection_name_plot[" << setw(3) << i_f << "] = " << setw(20) << selection_name_plot[i_f] << endl;
	  }


	  stringstream temp;
	  temp << "Output selection: " << osi->extended_distribution[i_d].xdistribution_name  << " : ";
	  for (size_t i_x = 0; i_x < selection_format_plot.size(); i_x++){
	    temp << selection_format_plot[i_x] << "." << selection_name_plot[i_x] << endl;
	  }
	  logger << LOG_INFO << temp.str() << endl;
	  int n_output_version = selection_format_plot.size();


	  scaleband_variable[i_sb][i_d].resize(n_output_version);

	  logger << LOG_DEBUG << "scaleband_variable[" << i_sb << "].size() = " << scaleband_variable[i_sb].size() << endl;
	  logger << LOG_DEBUG << "scaleband_variable[" << i_sb << "][" << i_d << "].size() = " << scaleband_variable[i_sb][i_d].size() << endl;

	  scaleband_central_result[i_sb][i_d][i_o].resize(n_output_version);
	  scaleband_central_deviation[i_sb][i_d][i_o].resize(n_output_version);
	  scaleband_minimum_result[i_sb][i_d][i_o].resize(n_output_version);
	  scaleband_minimum_deviation[i_sb][i_d][i_o].resize(n_output_version);
	  scaleband_maximum_result[i_sb][i_d][i_o].resize(n_output_version);
	  scaleband_maximum_deviation[i_sb][i_d][i_o].resize(n_output_version);

	  for (int i_x = 0; i_x < n_output_version; i_x++){

	    logger << LOG_INFO << "selection_format_plot[" << setw(3) << i_x << "] = " << setw(20) << selection_format_plot[i_x] << "   selection_name_plot[" << setw(3) << i_x << "] = " << setw(20) << selection_name_plot[i_x] << endl;

	    int switch_exist = 0;

	    vector<vector<vector<double> > > xresult, xdeviation, xdeviation2;
	    vector<double> variable;
	    vector<double> centralresult;
	    vector<double> centraldeviation;
	    for (int i_v = 0; i_v < inpath_scalevariation[i_sb].size(); i_v++){
	      vector<string> readin;
	      vector<vector<string> > readin2;
	      infilename = final_resultdirectory + "/" + inpath_scalevariation[i_sb][i_v] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];

	      logger << LOG_INFO << "i_d = " << setw(2) << i_d << "   i_v = " << setw(2) << i_v << "   i_o = " << setw(2) << i_o << "   infilename = " << infilename << endl;

	      ifstream inf(infilename.c_str());
	      readin = vs0;
	      while (inf.getline(LineBuffer, 1024)){readin.push_back(LineBuffer);}
	      logger << LOG_DEBUG << infilename << "   readin.size() = " << readin.size() << endl;
	      if (readin.size() == 0){continue;}
	      else {switch_exist = 1;}
	      for (int i_b = 0; i_b < readin.size(); i_b++){
		if (readin[i_b][0] == char(35)){}
		else {
		  readin2.push_back(vs0);
		  int start = 0;
		  for (int j = 0; j < readin[i_b].size(); j++){
		    if (readin[i_b][j] != ' '){
		      if (start == 0){start = 1; readin2[readin2.size() - 1].push_back(s0); readin2[readin2.size() - 1][readin2[readin2.size() - 1].size() - 1] = readin[i_b][j];}
		       else {readin2[readin2.size() - 1][readin2[readin2.size() - 1].size() - 1] = readin2[readin2.size() - 1][readin2[readin2.size() - 1].size() - 1] + readin[i_b][j];}
		    }
		    else if ((readin[i_b][j] == ' ') && (start == 1)){start = 0;}
		  }
		}
		if (i_b == 0 || i_b == readin.size() - 1){logger << LOG_DEBUG_VERBOSE << "i_b = " << setw(2) << i_b << "   (" << readin.size() << " - " << readin2.size() << ")" << endl;}
	      }

	      if (i_v == 0){variable.resize(readin2.size());}
	      vector<vector<double> > data(readin2.size());
	      vector<vector<double> > result(readin2.size()), deviation(readin2.size()), deviation2(readin2.size());


	      logger << LOG_INFO << "i_v = " << i_v << "   readin2.size() = " << readin2.size() << endl;

	      for (int i_b = 0; i_b < readin2.size(); i_b++){
		data[i_b].resize(readin2[i_b].size());
		result[i_b].resize((readin2[i_b].size() - 1) / 2);
		deviation[i_b].resize((readin2[i_b].size() - 1) / 2);
		deviation2[i_b].resize((readin2[i_b].size() - 1) / 2);
	      }

	      for (int i_b = 0; i_b < readin2.size(); i_b++){
		for (int j = 0; j < readin2[i_b].size(); j++){
		  data[i_b][j] = atof(readin2[i_b][j].c_str());
		}
	      }
	      for (int i_b = 0; i_b < result.size(); i_b++){
		if (i_v == 0){variable[i_b] = data[i_b][0];}
		for (int j = 0; j < result[i_b].size(); j++){
		  result[i_b][j] = data[i_b][j * 2 + 1];
		  deviation[i_b][j] = data[i_b][j * 2 + 2];
		  deviation2[i_b][j] = pow(deviation[i_b][j], 2);
		}
	      }

	      if (x_v == i_v){
		centralresult.resize(result.size());
		centraldeviation.resize(deviation.size());
		for (int i_b = 0; i_b < result.size(); i_b++){
		  centralresult[i_b] = result[i_b][0];
		  centraldeviation[i_b] = deviation[i_b][0];
		}
	      }
	      xresult.push_back(result);
	      xdeviation.push_back(deviation);
	      xdeviation2.push_back(deviation2);

	      logger << LOG_INFO << "i_d = " << setw(2) << i_d << "   i_v = " << setw(2) << i_v << "   i_o = " << setw(2) << i_o << "   infilename = " << infilename << "   " << readin.size() << " " << readin2.size() << endl;

	    }

	    if (!switch_exist){continue;}





	    vector<double> minresult(xresult[0].size(), 0.);
	    vector<double> maxresult(xresult[0].size(), 0.);
	    vector<double> mindeviation(xdeviation[0].size(), 0.);
	    vector<double> maxdeviation(xdeviation[0].size(), 0.);
	    for (int i_v = 0; i_v < xresult[0].size(); i_v++){
	      logger << LOG_DEBUG_VERBOSE << "i_v = " << i_v << "   xresult.size() = " << xresult.size() << endl;

	      minresult[i_v] = 1.e99;
	      maxresult[i_v] = -1.e99;
	      for (int i_c = 0; i_c < xresult.size(); i_c++){
		if (xresult[i_c][i_v][0] < minresult[i_v]){
		  minresult[i_v] = xresult[i_c][i_v][0];
		  mindeviation[i_v] = xdeviation[i_c][i_v][0];
		}
		if (xresult[i_c][i_v][0] > maxresult[i_v]){
		  maxresult[i_v] = xresult[i_c][i_v][0];
		  maxdeviation[i_v] = xdeviation[i_c][i_v][0];
		}
	      }
	      //	if (plotmode == 1){if (minresult[i_b] < 0.){minresult[i_b] = 0.;}}
	      logger << LOG_DEBUG_VERBOSE << "i_v = " << i_v << "   xresult.size() = " << xresult.size() << " done." << endl;
	    }


	    scaleband_variable[i_sb][i_d][i_x] = variable;
	    scaleband_central_result[i_sb][i_d][i_o][i_x] = centralresult;
	    scaleband_central_deviation[i_sb][i_d][i_o][i_x] = centraldeviation;
	    scaleband_minimum_result[i_sb][i_d][i_o][i_x] = minresult;
	    scaleband_minimum_deviation[i_sb][i_d][i_o][i_x] = mindeviation;
	    scaleband_maximum_result[i_sb][i_d][i_o][i_x] = maxresult;
	    scaleband_maximum_deviation[i_sb][i_d][i_o][i_x] = maxdeviation;

	    logger << LOG_INFO << "scaleband_variable[" << i_sb << "][" << i_d << "][" << i_x << "].size() = " <<  scaleband_variable[i_sb][i_d][i_x].size() << endl;

	    ofstream outf;

	    string outfilename_scaleband_matrix = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scaleband = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/fill" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scaleband_ref = final_resultdirectory + "/" + outpath_scaleband[i_sb] + directory_qTcut + "/reffill" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scaleband_geneva = final_resultdirectory + "/" + outpath_scalegeneva[i_sb] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scalecentral = final_resultdirectory + "/" + outpath_scalecentral[i_sb] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scalemax = final_resultdirectory + "/" + outpath_scalemax[i_sb] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];
	    string outfilename_scalemin = final_resultdirectory + "/" + outpath_scalemin[i_sb] + directory_qTcut + "/" + selection_format_plot[i_x] + "." + selection_name_plot[i_x];

	    logger << LOG_DEBUG << "outfilename_scaleband_matrix = " << outfilename_scaleband_matrix << endl;
	    logger << LOG_DEBUG << "outfilename_scaleband = " << outfilename_scaleband << endl;
	    logger << LOG_DEBUG << "outfilename_scaleband_geneva = " << outfilename_scaleband_geneva << endl;
	    logger << LOG_DEBUG << "outfilename_scalecentral = " << outfilename_scalecentral << endl;
	    logger << LOG_DEBUG << "outfilename_scalemax = " << outfilename_scalemax << endl;
	    logger << LOG_DEBUG << "outfilename_scalemin = " << outfilename_scalemin << endl;

	    /////////////////
	    //  scaleband  //
	    /////////////////
	    outf.open(outfilename_scaleband_matrix.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < centralresult.size(); i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << "   "
		   << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b]
		   << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b]
		   << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b]
		   << endl;
	    }
	    outf.close();

	    // outfilename_scaleband_matrix contains all the output needed for using matrix plot scripts !!!

	    outf.open(outfilename_scaleband.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < minresult.size(); i_b++){
	      /*
	      if (centralresult[i_b] < 0.){centralresult[i_b] = 0.;}
	      if (minresult[i_b] < 0.){minresult[i_b] = 0.;}
	      if (maxresult[i_b] < 0.){maxresult[i_b] = 0.;}
	      */
	      outf << setw(16) << setprecision(8) << variable[i_b] << "   "
		   << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b]
		   << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b]
		   << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b]
		   << endl;
	      //	       outf << setw(16) << setprecision(8) << variable[i_b] << "   " << setw(16) << minresult[i_b] << setw(16) << maxresult[i_b] << endl;

	      if (i_b < minresult.size() - 1){
		outf << setw(16) << setprecision(8) << variable[i_b + 1] << "   "
		     << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b]
		     << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b]
		     << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b]
		     << endl;
		//		 outf << setw(16) << setprecision(8) << variable[i_b + 1] << "   " << setw(16) << minresult[i_b] << setw(16) << maxresult[i_b] << endl;
	      }
	    }
	    outf.close();

	    /////////////////////
	    //  scaleband-ref  //
	    /////////////////////
	    outf.open(outfilename_scaleband_ref.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < centralresult.size(); i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << setw(16) << setprecision(8) << centralresult[i_b] << endl;
	      if (i_b < centralresult.size() - 1){
		outf << setw(16) << setprecision(8) << variable[i_b + 1] << setw(16) << setprecision(8) << centralresult[i_b] << endl;
	      }
	    }
	    outf.close();

	    ///////////////////
	    //  scalegeneva  //
	    ///////////////////
	    if (xresult.size() == 3){
	      outf.open(outfilename_scaleband_geneva.c_str(), ofstream::out | ofstream::trunc);
	      if (osi->extended_distribution[i_d].type_binning == "logarithmic"){
		for (int i_b = 0; i_b < variable.size() - 1; i_b++){
		  double factor = osi->extended_distribution[i_d].bin_width[i_b] / (log10(osi->extended_distribution[i_d].bin_edge[i_b + 1]) - log10(osi->extended_distribution[i_d].bin_edge[i_b]));
		  outf << setw(16) << setprecision(8) << log10(variable[i_b]) << "   "
		       << setw(16) << setprecision(8) << log10(variable[i_b + 1]) << "   "
		       << setw(16) << setprecision(8) << xresult[1][i_b][0] * factor << "   " << setw(16) << setprecision(8) << xdeviation[1][i_b][0] * factor
		       << setw(16) << setprecision(8) << xresult[2][i_b][0] * factor << "   " << setw(16) << setprecision(8) << xdeviation[2][i_b][0] * factor
		       << setw(16) << setprecision(8) << xresult[0][i_b][0] * factor << "   " << setw(16) << setprecision(8) << xdeviation[0][i_b][0] * factor
		       << endl;
		}
	      }
	      else {
		for (int i_b = 0; i_b < variable.size() - 1; i_b++){
		  outf << setw(16) << setprecision(8) << variable[i_b] << "   "
		       << setw(16) << setprecision(8) << variable[i_b + 1] << "   "
		       << setw(16) << setprecision(8) << xresult[1][i_b][0] << "   " << setw(16) << setprecision(8) << xdeviation[1][i_b][0]
		       << setw(16) << setprecision(8) << xresult[2][i_b][0] << "   " << setw(16) << setprecision(8) << xdeviation[2][i_b][0]
		       << setw(16) << setprecision(8) << xresult[0][i_b][0] << "   " << setw(16) << setprecision(8) << xdeviation[0][i_b][0]
		       << endl;
		}
	      }
	      outf.close();
	    }
	    
	    ////////////////////
	    //  scalecentral  //
	    ////////////////////
	    /*
	    outf.open(outfilename_scalecentral.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < centralresult.size(); i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b] << endl;
	      //  if (i < centralresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b] << endl;}
	    }
	    outf.close();
	    */
	    outf.open(outfilename_scalecentral.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < centralresult.size() - 1; i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << "   "
		   << setw(16) << setprecision(8) << variable[i_b + 1] << "   "
		   << setw(16) << setprecision(8) << centralresult[i_b] << "   " << setw(16) << setprecision(8) << centraldeviation[i_b]
		   << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b]
		   << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b]
		   << endl;
	    }
	    outf.close();

	    ////////////////
	    //  scalemax  //
	    ////////////////
	    outf.open(outfilename_scalemax.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < maxresult.size(); i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b] << endl;
	      //  if (i < maxresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << maxresult[i_b] << "   " << setw(16) << setprecision(8) << maxdeviation[i_b] << endl;}
	    }
	    outf.close();

	    ////////////////
	    //  scalemin  //
	    ////////////////
	    outf.open(outfilename_scalemin.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < minresult.size(); i_b++){
	      outf << setw(16) << setprecision(8) << variable[i_b] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;
	      //  if (i < minresult.size() - 1){outf << setw(16) << setprecision(8) << variable[i + 1] << setw(16) << setprecision(8) << minresult[i_b] << "   " << setw(16) << setprecision(8) << mindeviation[i_b] << endl;}
	    }
	    outf.close();
	  }
	}
      }
    }
  }

  //  if (switch_output_table_order){output_distribution_table_order();}
  if (switch_output_table_order){output_distribution_table_order_nNNLOQCDxEW();}
  if (switch_output_table_Kfactor){output_distribution_table_Kfactor();}
  if (switch_output_table_crosssection_Kfactor){output_distribution_table_crosssection_Kfactor();}
  if (switch_output_table_IS_splitting){output_distribution_table_IS_splitting();}
  if (switch_output_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW){output_distribution_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW();}


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_generic::collect_order_result_TSV(){
  Logger logger("summary_generic::collect_order_result_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  system_execute(logger, "mkdir " + final_resultdirectory);

  for (int i_o = 0; i_o < yorder.size(); i_o++){yorder[i_o]->collect_result_TSV(subgroup);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_generic::collect_order_result_CV(){
  Logger logger("summary_generic::collect_order_result_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_o = 0; i_o < yorder.size(); i_o++){yorder[i_o]->collect_result_CV(subgroup);}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_generic::collect_order_distribution_TSV(){
  Logger logger("summary_generic::collect_order_distribution_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_o = 0; i_o < yorder.size(); i_o++){yorder[i_o]->collect_distribution_TSV();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_generic::collect_order_distribution_CV(){
  Logger logger("summary_generic::collect_order_distribution_CV");
  logger << LOG_DEBUG_VERBOSE  << "called" << endl;

  for (int i_o = 0; i_o < yorder.size(); i_o++){yorder[i_o]->collect_distribution_CV();}

  // old asymmetry implementation deactivated for now !!!

  /*
  for (int i_o = 0; i_o < yorder.size(); i_o++){
    yorder[i_o]->distribution_result_CV.resize(subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size()));
    yorder[i_o]->distribution_deviation_CV.resize(subgroup.size(), vector<vector<vector<double> > > (osi->extended_distribution.size()));
    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution[i_d]){continue;}
	yorder[i_o]->distribution_result_CV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
	yorder[i_o]->distribution_deviation_CV[i_g][i_d].resize(osi->extended_distribution[i_d].n_bins, vector<double> (osi->n_scales_CV, 0.));
      }
    }

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution[i_d]){continue;}
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	    double dev = 0.;
	    for (int i_l = 0; i_l < yorder[i_o]->contribution_file.size(); i_l++){
	      int x_l = mapping_contribution_file[yorder[i_o]->contribution_file[i_l]];
	      yorder[i_o]->distribution_result_CV[i_g][i_d][i_b][i_s] += xlist[x_l]->xcontribution[0]->distribution_result_CV[i_g][i_d][i_b][i_s];
	      dev += pow(xlist[x_l]->xcontribution[0]->distribution_deviation_CV[i_g][i_d][i_b][i_s], 2);
	    }
	    yorder[i_o]->distribution_deviation_CV[i_g][i_d][i_b][i_s] = sqrt(dev);
	    stringstream temp_res;
	    temp_res << setw(23) << setprecision(15) << yorder[i_o]->distribution_result_CV[i_g][i_d][i_b][i_s];
	    stringstream temp_dev;
	    temp_dev << setw(23) << setprecision(15) << yorder[i_o]->distribution_deviation_CV[i_g][i_d][i_b][i_s];
	    logger << LOG_DEBUG_VERBOSE << "XXX   result_CV[" << i_o << "][" << i_g << "][" << i_d << "][" << i_b << "][" << i_s << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	  }
	}
      }
    }
  */
  for (int i_o = 0; i_o < 0; i_o++){
    exit(1);
    // to switch off this loop...
    //  for (int i_o = 0; i_o < yorder.size(); i_o++){

    if (switch_output_overview > 0){yorder[i_o]->output_distribution_overview_CV();}

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
	if (!switch_output_distribution[i_d]){continue;}
	for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	  if ((osi->extended_distribution[i_d].xdistribution_name).substr(0, 4) == "asym"){
	  /*
	  double result_fw, result_bw, deviation_fw, deviation_bw;
	  ofstream out_asymfile;
	  string filename_asym = final_resultdirectory + "/CV/" + osi->directory_name_scale_CV[i_s] + "/" + osi->extended_distribution[i_d].xdistribution_name + "." + infix_order_contribution[0] + ".dat";
	  logger << LOG_DEBUG_VERBOSE << "filename_asym = " << filename_asym << endl;
	  out_asymfile.open(filename_asym.c_str(), ofstream::out | ofstream::trunc);
	  if (osi->extended_distribution[i_d].xdistribution_type == "asym1" || osi->extended_distribution[i_d].xdistribution_type == "asym2"){
	    ofstream out_plotfile;
	    string filename_plot = final_resultdirectory + "/CV/" + osi->directory_name_scale_CV[i_s] + "/plot." + osi->extended_distribution[i_d].xdistribution_name + "." + infix_order_contributiony[0] + ".dat";
	    logger << LOG_DEBUG_VERBOSE << "filename_plot = " << filename_plot << endl;
	    out_plotfile.open(filename_plot.c_str(), ofstream::out | ofstream::trunc);
	    // too slow // checksum << setw(16) << "interval" << " " << setw(16) << "backward cs." << setw(16) << "forward cs." << setw(16) << "cross section" << endl;
	    for (int i_b = 0; i_b < n_bin_distribution[i_d]; i_b++){
	      if (osi->extended_distribution[i_d].xdistribution_type == "asym1"){
		result_fw = yorder[i_o]->distribution_result_CV[i_g][i_d][n_bin_distribution[i_d] + i_b][i_s] * osi->fakeasymfactor[i_d];
		result_bw = yorder[i_o]->distribution_result_CV[i_g][i_d][i_b][i_s] * osi->fakeasymfactor[i_d];
		deviation_fw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][n_bin_distribution[i_d] + i_b][i_s] * osi->fakeasymfactor[i_d];
		deviation_bw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][i_b][i_s] * osi->fakeasymfactor[i_d];
	      }
	      else if (osi->extended_distribution[i_d].xdistribution_type == "asym2"){
		result_fw = yorder[i_o]->distribution_result_CV[i_g][i_d][2 * i_b + 1][i_s] * osi->fakeasymfactor[i_d];
		result_bw = yorder[i_o]->distribution_result_CV[i_g][i_d][2 * i_b + 0][i_s] * osi->fakeasymfactor[i_d];
		deviation_fw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][2 * i_b + 1][i_s] * osi->fakeasymfactor[i_d];
		deviation_bw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][2 * i_b + 0][i_s] * osi->fakeasymfactor[i_d];
	      }
	      out_asymfile << setw(25) << "backw. cross section: " << "   [" << setw(5) << setprecision(4) << bin_edge[i_d][i_b] << ";" << setw(5) << setprecision(4) << bin_edge[i_d][i_b + 1] << "]   " << setw(16) << setprecision(8) << result_bw * osi->extended_distribution[i_d].step << setw(16) << setprecision(8) << deviation_bw * osi->extended_distribution[i_d].step << endl;
	      out_asymfile << setw(25) << "forw. cross section: " << "   [" << setw(5) << setprecision(4) << bin_edge[i_d][i_b] << ";" << setw(5) << setprecision(4) << bin_edge[i_d][i_b + 1] << "]   " << setw(16) << setprecision(8) << result_fw * osi->extended_distribution[i_d].step << setw(16) << setprecision(8) << deviation_fw * osi->extended_distribution[i_d].step << endl;
	      out_asymfile << setw(25) << "forw.-backw. asymmetry: " << "   [" << setw(5) << setprecision(4) << bin_edge[i_d][i_b] << ";" << setw(5) << setprecision(4) << bin_edge[i_d][i_b + 1] << "]   " << setw(16) << setprecision(8) << (result_fw - result_bw) / (result_fw + result_bw) << setw(16) << 2. * (deviation_fw * result_bw + deviation_bw * result_fw) / pow(result_bw + result_fw, 2) << endl;
	      out_plotfile << setw(5) << setprecision(4) << bin_edge[i_d][i_b] << setw(16) << setprecision(8) << (result_fw - result_bw) / (result_fw + result_bw) << setw(16) << 2. * (deviation_fw * result_bw + deviation_bw * result_fw) / pow(result_bw + result_fw, 2) << endl;
	      double sum_result_fw = 0., sum_result_bw = 0.;
	      for (int c = i_b; c < n_bin_distribution[i_d]; c++){
		if (osi->extended_distribution[i_d].xdistribution_type == "asym1"){
		  sum_result_fw += yorder[i_o]->distribution_result_CV[i_g][i_d][n_bin_distribution[i_d] + c][i_s] * osi->fakeasymfactor[i_d];
		  sum_result_bw += yorder[i_o]->distribution_result_CV[i_g][i_d][c][i_s] * osi->fakeasymfactor[i_d];
		}
		else if (osi->extended_distribution[i_d].xdistribution_type == "asym2"){
		  sum_result_fw += yorder[i_o]->distribution_result_CV[i_g][i_d][2 * c + 1][i_s] * osi->fakeasymfactor[i_d];
		  sum_result_bw += yorder[i_o]->distribution_result_CV[i_g][i_d][2 * c + 0][i_s] * osi->fakeasymfactor[i_d];
		}
	      }
	    }
	    out_plotfile << setw(5) << setprecision(4) << osi->extended_distribution[i_d].end << setw(16) << setprecision(8) << (result_fw - result_bw) / (result_fw + result_bw) << setw(16) << 2. * (deviation_fw * result_bw + deviation_bw * result_fw) / pow(result_bw + result_fw, 2) << endl;
	    out_asymfile.close();
	    out_plotfile.close();
	  }
	  else {
	    result_fw = yorder[i_o]->distribution_result_CV[i_g][i_d][1][i_s] * osi->fakeasymfactor[i_d];
	    result_bw = yorder[i_o]->distribution_result_CV[i_g][i_d][0][i_s] * osi->fakeasymfactor[i_d];
	    deviation_fw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][1][i_s] * osi->fakeasymfactor[i_d];
	    deviation_bw = yorder[i_o]->distribution_deviation_CV[i_g][i_d][0][i_s] * osi->fakeasymfactor[i_d];
	    out_asymfile << setw(25) << "backw. cross section: " << setw(16) << setprecision(8) << result_bw << setw(16) << setprecision(8) << deviation_bw << endl;
	    out_asymfile << setw(25) << "forw. cross section: " << setw(16) << setprecision(8) << result_fw << setw(16) << setprecision(8) << deviation_fw << endl;
	    out_asymfile << setw(25) << "forw.-backw. asymmetry: " << setw(16) << setprecision(8) << (result_fw - result_bw) / (result_fw + result_bw) << setw(16) << 2. * (deviation_fw * result_bw + deviation_bw * result_fw) / pow(result_bw + result_fw, 2) << endl;
	    out_asymfile.close();
	  }
	  */
	  }
	  else {
	    ofstream out_plotfile;
	    string filename_plot = final_resultdirectory + "/CV/" + osi->directory_name_scale_CV[i_s] + "/plot." + osi->extended_distribution[i_d].xdistribution_name + ".." + yorder[i_o]->resultdirectory + ".dat";
	    logger << LOG_DEBUG_VERBOSE << "filename_plot = " << filename_plot << endl;
	    out_plotfile.open(filename_plot.c_str(), ofstream::out | ofstream::trunc);
	    for (int i_b = 0; i_b < yorder[i_o]->distribution_result_CV[i_g][i_d].size(); i_b++){

	      out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[i_b] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_result_CV[i_g][i_d][i_b][i_s] / osi->extended_distribution[i_d].bin_width[i_b] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_deviation_CV[i_g][i_d][i_b][i_s] / osi->extended_distribution[i_d].bin_width[i_b] << endl;
	      // norm ->	    out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[i_b] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_result_CV[i_g][i_d][i_b][i_s] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_deviation_CV[i_g][i_d][i_b][i_s] << endl;
	      double sum_result = 0.;
	      for (int j_b = i_b; j_b < osi->extended_distribution[i_d].n_bins; j_b++){sum_result += yorder[i_o]->distribution_result_CV[i_g][i_d][j_b][i_s];}
	      //	      for (int j_b = i_b; j_b < osi->extended_distribution[i_d].n_bins; j_b++){sum_result += yorder[i_o]->distribution_result_CV[i_g][i_d][j_b][i_s] / osi->extended_distribution[i_d].bin_width[i_b];}
	    }
	    out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[osi->extended_distribution[i_d].n_bins] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_result_CV[i_g][i_d][yorder[i_o]->distribution_result_CV[i_g][i_d].size() - 1][i_s] / osi->extended_distribution[i_d].bin_width[osi->extended_distribution[i_d].n_bins - 1] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_deviation_CV[i_g][i_d][yorder[i_o]->distribution_deviation_CV[i_g][i_d].size() - 1][i_s] / osi->extended_distribution[i_d].bin_width[osi->extended_distribution[i_d].n_bins - 1] << endl; // endpoint
	    // norm ->	    out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[osi->extended_distribution[i_d].n_bins] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_result_CV[i_g][i_d][yorder[i_o]->distribution_result_CV[i_g][i_d].size() - 1][i_s] << setw(16) << setprecision(8) << osi->unit_factor_distribution * yorder[i_o]->distribution_deviation_CV[i_g][i_d][yorder[i_o]->distribution_deviation_CV[i_g][i_d].size() - 1][i_s] << endl; // endpoint

	    out_plotfile.close();
	  }
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE  << "finished" << endl;
}



void summary_generic::output_filename_TSV(string & filename_begin, string & filename_end){
  Logger logger("summary_generic::output_filename_TSV");
  logger << LOG_DEBUG_VERBOSE  << "called" << endl;

  filename_complete = filename_begin + "/complete/" + filename_end;
  filename_ren = filename_begin + "/ren/" + filename_end;
  filename_fact = filename_begin + "/fact/" + filename_end;
  filename_equal = filename_begin + "/equal/" + filename_end;
  filename_antipodal = filename_begin + "/antipodal/" + filename_end;

  logger << LOG_DEBUG_VERBOSE  << "finished" << endl;
}

