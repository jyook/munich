#include "header.hpp"

// Make "identifier" a vector of string's to avoid the repeated call for different output formats !!!

void summary_generic::output_sddistribution_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_sddistribution_TSV");
  logger << LOG_DEBUG << "called" << endl;

  /////////////////////////////////////////
  //  singly-differential distributions  //
  /////////////////////////////////////////

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier){flag = 1; break;}
  }
  if (!flag){return;}

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	string filename_distribution_plot = identifier + "." + osi->extended_distribution[i_d].xdistribution_name + "." + name_result + ".dat";
	// subdirectory should contain the complete path between scalename_TSV[i_s][i_r][i_f]  and  "/" + filename_distribution_plot;
	string filepath_distribution_plot = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot;
	logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot = " << filepath_distribution_plot << endl;
	ofstream out_plotfile;
	out_plotfile.open(filepath_distribution_plot.c_str(), ofstream::out | ofstream::trunc);
	double temp_result = 0;
	double temp_deviation = 0;
	for (int i_b = 0; i_b < osi->extended_distribution[i_d].n_bins; i_b++){
	  if (identifier == "norm"){
	    temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f];
	    temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f];
	  }
	  else if (identifier == "plot"){
	    temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->extended_distribution[i_d].bin_width[i_b];
	    temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->extended_distribution[i_d].bin_width[i_b];
	  }
	  if (plotmode == 1){if (temp_result < 0.){temp_result = 0.;}}

	  out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[i_b]
		       << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
		       << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	}
	out_plotfile << setw(10) << osi->extended_distribution[i_d].bin_edge[osi->extended_distribution[i_d].n_bins]
		     << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
		     << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	out_plotfile.close();
	logger << LOG_DEBUG_VERBOSE << "singly-differential output finished" << endl;
      }
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::output_dddistribution_reconstruct_first_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_reconstruct_first_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier){flag = 1; break;}
  }
  if (!flag){return;}

  /////////////////////////////////////////
  //  doubly-differential distributions  //
  /////////////////////////////////////////
  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	/*
	  if (osi->dddat[i_ddd].distribution_2.xdistribution_type == "pTratio" ||
	  osi->dddat[i_ddd].distribution_2.xdistribution_type == "multiplicity" ||
	  osi->dddat[i_ddd].distribution_2.xdistribution_type == "abseta"){
	*/
	if (1 > 0){
	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	  //  sum over re-normalized pTratio-distibution to reconstruct the singly-differential distribution  //
	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	  stringstream filename_distribution_plot_ss;
	  filename_distribution_plot_ss << identifier << ".rec." << osi->dddat[i_ddd].distribution_1.xdistribution_name << ".from." << osi->dddat[i_ddd].name << ".." << name_result << ".dat";
	  string filename_distribution_plot = filename_distribution_plot_ss.str();
	  string filepath_distribution_plot = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot              = " << filepath_distribution_plot << endl;
	  ofstream out_plotfile;
	  out_plotfile.open(filepath_distribution_plot.c_str(), ofstream::out | ofstream::trunc);
	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    double temp_result = 0;
	    double temp_deviation = 0;
	    for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	      int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;
	      if (identifier == "norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];// / osi->dddat[i_ddd].distribution_1.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);// / osi->dddat[i_ddd].distribution_1.bin_width[i_b2], 2);
	      }
	      else if (identifier == "plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1], 2);
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	  }
	  out_plotfile.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// Corresponds to old output_dddistribution_reconstruct_sdd_TSV:
void summary_generic::output_dddistribution_reconstruct_second_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_reconstruct_second_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier){flag = 1; break;}
  }
  if (!flag){return;}

  /////////////////////////////////////////
  //  doubly-differential distributions  //
  /////////////////////////////////////////
  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	/*
	  if (osi->dddat[i_ddd].distribution_1.xdistribution_type == "pTratio" ||
	  osi->dddat[i_ddd].distribution_1.xdistribution_type == "multiplicity" ||
	  osi->dddat[i_ddd].distribution_1.xdistribution_type == "abseta"){
	*/
	if (1 > 0){
	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	  //  sum over re-normalized pTratio-distibution to reconstruct the singly-differential distribution  //
	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	  stringstream filename_distribution_plot_ss;
	  filename_distribution_plot_ss << identifier << ".rec." << osi->dddat[i_ddd].distribution_2.xdistribution_name << ".from." << osi->dddat[i_ddd].name << ".." << name_result << ".dat";
	  string filename_distribution_plot = filename_distribution_plot_ss.str();
	  string filepath_distribution_plot = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot              = " << filepath_distribution_plot << endl;
	  ofstream out_plotfile;
	  out_plotfile.open(filepath_distribution_plot.c_str(), ofstream::out | ofstream::trunc);
	  for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	    double temp_result = 0;
	    double temp_deviation = 0;
	    for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	      int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;
	      if (identifier == "norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];// / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);// / osi->dddat[i_ddd].distribution_2.bin_width[i_b2], 2);
	      }
	      else if (identifier == "plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2], 2);
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	  }
	  out_plotfile.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// original version:
void summary_generic::output_dddistribution_split_in_first_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_split_in_first_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier + ".split" ||
	output_distribution_format[i_d][i_f] == identifier + ".split_in_first"){flag = 1; break;}
  }
  if (!flag){return;}

  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){

	/////////////////////////////////////////////////////////////
	//  state re-normalized distibutions for each pTratio-bin  //
	//  state re-normalized distibutions for each abseta-bin   //
	/////////////////////////////////////////////////////////////

	for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	  stringstream filename_distribution_plot_ss;
	  filename_distribution_plot_ss << identifier << ".split." << osi->dddat[i_ddd].name << "_" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << "-" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1] << ".." << name_result << ".dat";
	  string filename_distribution_plot = filename_distribution_plot_ss.str();
	  string filepath_distribution_plot = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot              = " << filepath_distribution_plot << endl;
	  ofstream out_plotfile;
	  out_plotfile.open(filepath_distribution_plot.c_str(), ofstream::out | ofstream::trunc);
	  double temp_result = 0;
	  double temp_deviation = 0;
	  for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	    int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;

	    if (identifier == "norm.norm"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f];
	    }
	    else if (identifier == "norm.plot"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	    }
	    else if (identifier == "plot.norm"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	    }
	    else if (identifier == "plot.plot"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	    }
	    out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation
			 << endl;
	  }
	  out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[osi->dddat[i_ddd].distribution_2.n_bins]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation
			 << endl;
	  out_plotfile.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void summary_generic::output_dddistribution_split_in_second_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_split_in_first_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier + ".split" ||
	output_distribution_format[i_d][i_f] == identifier + ".split_in_second"){flag = 1; break;}
  }
  if (!flag){return;}

  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){

	/////////////////////////////////////////////////////////////
	//  state re-normalized distibutions for each pTratio-bin  //
	//  state re-normalized distibutions for each abseta-bin   //
	/////////////////////////////////////////////////////////////

	for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	  stringstream filename_distribution_plot_ss;
	  filename_distribution_plot_ss << identifier << ".split." << osi->dddat[i_ddd].name << "_" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2] << "-" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2 + 1] << ".." << name_result << ".dat";
	  string filename_distribution_plot = filename_distribution_plot_ss.str();
	  string filepath_distribution_plot = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot              = " << filepath_distribution_plot << endl;
	  ofstream out_plotfile;
	  out_plotfile.open(filepath_distribution_plot.c_str(), ofstream::out | ofstream::trunc);
	  double temp_result = 0;
	  double temp_deviation = 0;
	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;

	    if (identifier == "norm.norm"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f];
	    }
	    // Check if normalization is correct here: !!!
	    else if (identifier == "norm.plot"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	    }
	    else if (identifier == "plot.norm"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	    }
	    else if (identifier == "plot.plot"){
	      temp_result = this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      temp_deviation = this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	    }
	    out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation
			 << endl;
	  }
	  out_plotfile << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[osi->dddat[i_ddd].distribution_1.n_bins]
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			 << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation
			 << endl;
	  out_plotfile.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



// original version:
void summary_generic::output_dddistribution_ge_lt_in_first_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_ge_lt_in_first_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier + ".two" ||
	output_distribution_format[i_d][i_f] == identifier + ".ge_lt" ||
	output_distribution_format[i_d][i_f] == identifier + ".ge_lt_in_first"){flag = 1; break;}
  }
  if (!flag){return;}

  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){

	////////////////////////////////////////////////////////////////////
	//  state re-normalized distibutions for all pTratio <>-binnings  //
	////////////////////////////////////////////////////////////////////

	for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	  stringstream filename_distribution_plot_lt_ss;
	  stringstream filename_distribution_plot_ge_ss;
	  filename_distribution_plot_lt_ss << identifier << ".two." << osi->dddat[i_ddd].name << "_lt" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << ".." << name_result << ".dat";
	  filename_distribution_plot_ge_ss << identifier << ".two." << osi->dddat[i_ddd].name << "_ge" << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1] << ".." << name_result << ".dat";
	  string filename_distribution_plot_lt = filename_distribution_plot_lt_ss.str();
	  string filename_distribution_plot_ge = filename_distribution_plot_ge_ss.str();
	  string filepath_distribution_plot_lt = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot_lt;
	  string filepath_distribution_plot_ge = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot_ge;

	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot_lt              = " << filepath_distribution_plot_lt << endl;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot_ge              = " << filepath_distribution_plot_ge << endl;

	  ////////////////////////////////////////////////////////////////////
	  //  state re-normalized distibutions for all pTratio <>-binnings  //
	  ////////////////////////////////////////////////////////////////////

	  //  for (int i_b1 = 1; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	  ofstream out_plotfile_lt;
	  ofstream out_plotfile_ge;
	  out_plotfile_lt.open(filepath_distribution_plot_lt.c_str(), ofstream::out | ofstream::trunc);
	  out_plotfile_ge.open(filepath_distribution_plot_ge.c_str(), ofstream::out | ofstream::trunc);
	  for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	    double temp_result = 0;
	    double temp_deviation = 0;
	    for (int j_b1 = 0; j_b1 < i_b1; j_b1++){
	      //      cout << "lt: osi->dddat[" << i_ddd << "].distribution_1.bin_edge[" << j_b1 << "] = " << osi->dddat[i_ddd].distribution_1.bin_edge[j_b1] << "   osi->dddat[" << i_ddd << "].distribution_1.bin_width[" << j_b1 << "] = " << osi->dddat[i_ddd].distribution_1.bin_width[j_b1] << endl;
	      int i_b = j_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;
	      if (identifier == "norm" || identifier == "norm.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);
	      }
	      else if (identifier == "plot" || identifier == "norm.plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2], 2);
	      }
	      else if (identifier == "plot.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      }
	      else if (identifier == "plot.plot"){
		temp_result = +this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation = +this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile_lt << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2]
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	    if (i_b2 == osi->dddat[i_ddd].distribution_2.n_bins - 1){
	      out_plotfile_lt << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2 + 1]
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;}

	    temp_result = 0;
	    temp_deviation = 0;
	    for (int j_b1 = i_b1; j_b1 < osi->dddat[i_ddd].distribution_1.n_bins; j_b1++){
	      //      cout << "ge: osi->dddat[" << i_ddd << "].distribution_1.bin_edge[" << j_b1 << "] = " << osi->dddat[i_ddd].distribution_1.bin_edge[j_b1] << "   osi->dddat[" << i_ddd << "].distribution_1.bin_width[" << j_b1 << "] = " << osi->dddat[i_ddd].distribution_1.bin_width[j_b1] << endl;
	      int i_b = j_b1 * osi->dddat[i_ddd].distribution_2.n_bins + i_b2;
	      if (identifier == "norm" || identifier == "norm.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];// / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);// / osi->dddat[i_ddd].distribution_2.bin_width[i_b2], 2);
	      }
	      else if (identifier == "plot" || identifier == "norm.plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2], 2);
	      }
	      else if (identifier == "plot.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      }
	      else if (identifier == "plot.plot"){
		temp_result = +this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation = +this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile_ge << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2]
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	    if (i_b2 == osi->dddat[i_ddd].distribution_2.n_bins - 1){
	      out_plotfile_ge << setw(10) << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2 + 1]
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;}
	  }
	  out_plotfile_lt.close();
	  out_plotfile_ge.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void summary_generic::output_dddistribution_ge_lt_in_second_sdd_TSV(string & identifier, vector<vector<vector<vector<double> > > > & this_distribution_result_TSV, vector<vector<vector<vector<double> > > > & this_distribution_deviation_TSV, int i_d, string & name_result, string & subdirectory, int plotmode){
  Logger logger("summary_generic::output_dddistribution_ge_lt_in_first_sdd_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int flag = 0;
  for (size_t i_f = 0; i_f < output_distribution_format[i_d].size(); i_f++){
    if (output_distribution_format[i_d][i_f] == identifier + ".two" ||
	output_distribution_format[i_d][i_f] == identifier + ".ge_lt" ||
	output_distribution_format[i_d][i_f] == identifier + ".ge_lt_in_second"){flag = 1; break;}
  }
  if (!flag){return;}

  int i_ddd = i_d - osi->dat.size();

  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    if (!switch_output_scaleset[i_s]){continue;}
    if (!osi->switch_distribution_TSV[i_s]){continue;}
    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){

	////////////////////////////////////////////////////////////////////
	//  state re-normalized distibutions for all pTratio <>-binnings  //
	////////////////////////////////////////////////////////////////////

	for (int i_b2 = 0; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	  stringstream filename_distribution_plot_lt_ss;
	  stringstream filename_distribution_plot_ge_ss;
	  filename_distribution_plot_lt_ss << identifier << ".two." << osi->dddat[i_ddd].name << "_lt" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2] << ".." << name_result << ".dat";
	  filename_distribution_plot_ge_ss << identifier << ".two." << osi->dddat[i_ddd].name << "_ge" << osi->dddat[i_ddd].distribution_2.bin_edge[i_b2] << ".." << name_result << ".dat";
	  string filename_distribution_plot_lt = filename_distribution_plot_lt_ss.str();
	  string filename_distribution_plot_ge = filename_distribution_plot_ge_ss.str();
	  string filepath_distribution_plot_lt = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot_lt;
	  string filepath_distribution_plot_ge = scalename_TSV[i_s][i_r][i_f] + subdirectory + "/" + filename_distribution_plot_ge;

	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot_lt              = " << filepath_distribution_plot_lt << endl;
	  logger << LOG_DEBUG_VERBOSE << "filepath_distribution_plot_ge              = " << filepath_distribution_plot_ge << endl;

	  ////////////////////////////////////////////////////////////////////
	  //  state re-normalized distibutions for all pTratio <>-binnings  //
	  ////////////////////////////////////////////////////////////////////

	  //  for (int i_b2 = 1; i_b2 < osi->dddat[i_ddd].distribution_2.n_bins; i_b2++){
	  ofstream out_plotfile_lt;
	  ofstream out_plotfile_ge;
	  out_plotfile_lt.open(filepath_distribution_plot_lt.c_str(), ofstream::out | ofstream::trunc);
	  out_plotfile_ge.open(filepath_distribution_plot_ge.c_str(), ofstream::out | ofstream::trunc);
	  for (int i_b1 = 0; i_b1 < osi->dddat[i_ddd].distribution_1.n_bins; i_b1++){
	    double temp_result = 0;
	    double temp_deviation = 0;
	    for (int j_b2 = 0; j_b2 < i_b2; j_b2++){
	      //      cout << "lt: osi->dddat[" << i_ddd << "].distribution_2.bin_edge[" << j_b2 << "] = " << osi->dddat[i_ddd].distribution_2.bin_edge[j_b2] << "   osi->dddat[" << i_ddd << "].distribution_2.bin_width[" << j_b2 << "] = " << osi->dddat[i_ddd].distribution_2.bin_width[j_b2] << endl;
	      int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + j_b2;
	      if (identifier == "norm" || identifier == "norm.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);
	      }
	    // Check if normalization is correct here: !!!
	      else if (identifier == "norm.plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      }
	      else if (identifier == "plot" || identifier == "plot.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1], 2);
	      }
	      else if (identifier == "plot.plot"){
		temp_result = +this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation = +this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile_lt << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1]
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	    if (i_b1 == osi->dddat[i_ddd].distribution_1.n_bins - 1){
	      out_plotfile_lt << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1]
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;}

	    temp_result = 0;
	    temp_deviation = 0;
	    for (int j_b2 = i_b2; j_b2 < osi->dddat[i_ddd].distribution_2.n_bins; j_b2++){
	      //      cout << "ge: osi->dddat[" << i_ddd << "].distribution_2.bin_edge[" << j_b2 << "] = " << osi->dddat[i_ddd].distribution_2.bin_edge[j_b2] << "   osi->dddat[" << i_ddd << "].distribution_2.bin_width[" << j_b2 << "] = " << osi->dddat[i_ddd].distribution_2.bin_width[j_b2] << endl;
	      int i_b = i_b1 * osi->dddat[i_ddd].distribution_2.n_bins + j_b2;
	      if (identifier == "norm" || identifier == "norm.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f];// / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f], 2);// / osi->dddat[i_ddd].distribution_1.bin_width[i_b1], 2);
	      }
	    // Check if normalization is correct here: !!!
	      else if (identifier == "norm.plot"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation += this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      }
	      else if (identifier == "plot" || identifier == "plot.norm"){
		temp_result += this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1];
		temp_deviation += pow(this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1], 2);
	      }
	      else if (identifier == "plot.plot"){
		temp_result = +this_distribution_result_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
		temp_deviation = +this_distribution_deviation_TSV[i_b][i_s][i_r][i_f] / osi->dddat[i_ddd].distribution_1.bin_width[i_b1] / osi->dddat[i_ddd].distribution_2.bin_width[i_b2];
	      }
	    }
	    temp_deviation = sqrt(temp_deviation);
	    out_plotfile_ge << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1]
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			    << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;
	    if (i_b1 == osi->dddat[i_ddd].distribution_1.n_bins - 1){
	      out_plotfile_ge << setw(10) << osi->dddat[i_ddd].distribution_1.bin_edge[i_b1 + 1]
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_result
			      << setw(16) << setprecision(8) << osi->unit_factor_distribution * temp_deviation << endl;}
	  }
	  out_plotfile_lt.close();
	  out_plotfile_ge.close();
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



