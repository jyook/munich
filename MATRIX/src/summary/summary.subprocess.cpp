#include "header.hpp"

summary_subprocess::summary_subprocess(){
  Logger logger("summary_subprocess::summary_subprocess ()");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  name = "";
  error2_time = 0.;
  time_per_event = 0.;
  used_runtime = 0.;
  used_n_event = 0;
  extrapolated_runtime = 0.;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


summary_subprocess::summary_subprocess(summary_generic * _ygeneric){
  Logger logger("summary_subprocess::summary_subprocess (ygeneric)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ygeneric = _ygeneric;
  osi = ygeneric->osi;
  name = "";

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


summary_subprocess::summary_subprocess(string _name, summary_contribution * _contribution){
  Logger logger("summary_subprocess::summary_subprocess (name, contribution)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ycontribution = _contribution;
  ygeneric = ycontribution->ygeneric;
  osi = ygeneric->osi;

  name = _name;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


summary_subprocess::summary_subprocess(string _name, int _n_seed, int _default_size_CV, summary_contribution * _contribution){
  Logger logger("summary_subprocess::summary_subprocess (name, n_seed, default_size_CV, contribution)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization(_name, _n_seed, _default_size_CV, _contribution);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::initialization(string _name, int _n_seed, int _default_size_CV, summary_contribution * _contribution){
  Logger logger("summary_subprocess::summary_subprocess (name, n_seed, default_size_CV, contribution)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ycontribution = _contribution;
  ygeneric = ycontribution->ygeneric;
  osi = ygeneric->osi;

  name = _name;
  n_seed = _n_seed;
  default_size_CV = _default_size_CV;

  no_results = 0;
  N_all = 0;

  result = 0.;
  deviation = 0.;
  chi2 = 0.;

  logger << LOG_INFO << "initialization:  " << setw(20) << name << "  in  " << setw(15) << ycontribution->type_contribution << endl;

  remove_run_qTcut_CV.resize(osi->n_qTcut, vector<int> (n_seed, 0));

  // Check if needed beforehand !!!

  N.resize(n_seed, 0);
  result_run.resize(n_seed, 0.);
  deviation_run.resize(n_seed, 0.);
  // might be needed for CV !!!
  used_n_event_run.resize(n_seed, 0);

  N_event_run_CV.resize(n_seed, 0);
  N_event_CV = 0;
  Nd_event_CV = 0.;
  N_valid_event_CV.resize(osi->n_qTcut, 0);

  n_parallel_runs_reference_CV.resize(osi->n_qTcut, 0);
  n_valid_parallel_runs_reference_CV.resize(osi->n_qTcut, 0);

  result_CV.resize(osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.));
  deviation_CV.resize(osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.));
  chi2_CV.resize(osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.));
  n_parallel_runs_CV.resize(osi->n_qTcut, vector<int> (osi->n_scales_CV, 0));
  n_valid_parallel_runs_CV.resize(osi->n_qTcut, vector<int> (osi->n_scales_CV, 0));

  result_run_CV.resize(ycontribution->output_n_qTcut, vector<vector<double> > (osi->n_scales_CV, vector<double> (n_seed, 0.)));
  deviation_run_CV.resize(ycontribution->output_n_qTcut, vector<vector<double> > (osi->n_scales_CV, vector<double> (n_seed, 0.)));

  if (osi->switch_TSV){
    N_event_run_TSV.resize(n_seed, 0);
    N_event_TSV = 0;
    Nd_event_TSV = 0.;
    N_valid_event_TSV.resize(osi->n_qTcut, 0);

    n_parallel_runs_reference_TSV.resize(osi->n_qTcut, 0);
    n_valid_parallel_runs_reference_TSV.resize(osi->n_qTcut, 0);

    chi2_TSV.resize(osi->n_moments, vector<vector<vector<vector<double> > > > (osi->n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV)));
    result_TSV.resize(osi->n_moments, vector<vector<vector<vector<double> > > > (osi->n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV)));
    deviation_TSV.resize(osi->n_moments, vector<vector<vector<vector<double> > > > (osi->n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV)));
    n_parallel_runs_TSV.resize(osi->n_moments, vector<vector<vector<vector<int> > > > (osi->n_qTcut, vector<vector<vector<int> > > (osi->n_extended_set_TSV)));
    n_valid_parallel_runs_TSV.resize(osi->n_moments, vector<vector<vector<vector<int> > > > (osi->n_qTcut, vector<vector<vector<int> > > (osi->n_extended_set_TSV)));

    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  result_TSV[i_m][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  deviation_TSV[i_m][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  chi2_TSV[i_m][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  n_parallel_runs_TSV[i_m][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<int> (osi->n_scale_fact_TSV[i_s], 0));
	  n_valid_parallel_runs_TSV[i_m][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<int> (osi->n_scale_fact_TSV[i_s], 0));
	}
      }
    }

    logger << LOG_DEBUG << "ycontribution->output_n_qTcut < osi->n_qTcut = " << ycontribution->output_n_qTcut << " < " << osi->n_qTcut << endl;

    remove_run_qTcut_TSV.resize(ycontribution->output_n_qTcut, vector<int> (n_seed, 0));
    //    remove_run_qTcut_TSV.resize(osi->n_qTcut, vector<int> (n_seed, 0));
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// What are these two functions needed for ???
void summary_subprocess::combine_result_original(){
  Logger logger("summary_subprocess::combine_result_original");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  N_all = 0;
  no_results = 0;
  result = 0.;
  deviation = 0.;
  for (int i_z = 0; i_z < n_seed; i_z++){
    if (!(result_run[i_z] == 0. && deviation_run[i_z] == 0.)){
      N_all += N[i_z];
      result += N[i_z] * result_run[i_z];
      deviation += pow((N[i_z] - 1) * deviation_run[i_z], 2) + N[i_z] * pow(result_run[i_z], 2);
      no_results++;
    }
  }
  deviation = sqrt((N_all * deviation - pow(result, 2)) / N_all) / (N_all - 1);
  result = result / N_all;
  for (int i_z = 0; i_z < n_seed; i_z++){
    if (!(result_run[i_z] == 0. && deviation_run[i_z] == 0.)){
      chi2 += pow((result_run[i_z] - result) / deviation_run[i_z], 2.);
    }
  }
  chi2 = chi2 / no_results;
  for (int i_z = 0; i_z < n_seed; i_z++){
    if (N[i_z] != 0 && deviation_run[i_z] != 0. && result_run[i_z] != 0.){
      logger << LOG_DEBUG << "run " << right << setw(2) << i_z << ":   " << showpoint << setw(13) << right << setprecision(6) << result_run[i_z] << " +- " << setw(13) << setprecision(6) << deviation_run[i_z] << "   " << setw(11) << right << setprecision(4) << (result_run[i_z] - result) / deviation_run[i_z] << " sigma   " << setw(15) << N[i_z] << endl;
    }
  }
  logger << LOG_DEBUG << endl << setw(10) << left << "old av:   " << setw(13) << right << setprecision(6) << result << " +- " << setw(13) << setprecision(6) << deviation << endl << endl;
  //    logger << LOG_DEBUG << "old version: " << i_p << "   " << result << " +- " << deviation << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// Check what this is used for !!!
void summary_subprocess::combine_result_alternative(){
  Logger logger("summary_subprocess::combine_result_alternative");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ///////////////////////////////////////////////////////////////
  //  combination of different-seed runs for all subprocesses  //
  ///////////////////////////////////////////////////////////////
  double temp_singleweight;

  N_all = 0;
  result = 0.;
  deviation = 0.;
  no_results = 0;
  for (int i_z = 0; i_z < n_seed; i_z++){
    if (N[i_z] != 0 && deviation_run[i_z] != 0. && result_run[i_z] != 0.){
      temp_singleweight = 1. / pow(deviation_run[i_z], 2);
      result += temp_singleweight * result_run[i_z];
      deviation += temp_singleweight;
      N_all += N[i_z];
      no_results++;
    }
  }
  if (N_all != 0){
    result = result / deviation;
    deviation = 1. / sqrt(deviation);
    if (result != result){
      logger << LOG_DEBUG_VERBOSE << N_all << endl;
      for (int i_z = 0; i_z < n_seed; i_z++){logger << LOG_DEBUG_VERBOSE << N[i_z] << endl;}
    }
  }
  else{
    result = 0.;
    deviation = 0.;
  }
  for (int i_z = 0; i_z < n_seed; i_z++){
    if (N[i_z] != 0 && deviation_run[i_z] != 0. && result_run[i_z] != 0.){
      logger << LOG_DEBUG << "run " << right << setw(2) << i_z << ":   " << showpoint << setw(13) << right << setprecision(6) << result_run[i_z] << " +- " << setw(13) << setprecision(6) << deviation_run[i_z] << "   " << setw(11) << right << setprecision(4) << (result_run[i_z] - result) / deviation_run[i_z] << " sigma   " << setw(15) << N[i_z] << " EVENTS" << endl;
    }
  }
  logger << LOG_DEBUG << endl << setw(10) << left << "average:  " << setw(13) << right << setprecision(6) << result << " +- " << setw(13) << setprecision(6) << deviation << endl << endl;
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



double summary_subprocess::get_time_stamp(int i_s, int i_m, int i_z){
  Logger logger("summary_subprocess::get_time_stamp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ifstream in_time_file(filepath_time[i_s][i_m][i_z].c_str());
  string temp_time_stamp;
  in_time_file.seekg(0);//, is.beg);
  getline(in_time_file, temp_time_stamp);
  in_time_file.close();
  double time_stamp = atof(temp_time_stamp.c_str());

  logger << LOG_DEBUG_VERBOSE << "finished - time_stamp = " << time_stamp << endl;
  return time_stamp;
}


void summary_subprocess::set_file_distribution(int i_s, int i_m, int i_z){
  Logger logger("summary_subprocess::set_file_distribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /*
  ifstream * in_result = new std::ifstream(filepath_distribution[i_s][i_m][i_z].c_str());
  //	ifstream in_result(filepath.c_str());
  in_result_vector[i_s][i_m][i_z] = in_result;
  // why not ???
  */
  in_result_vector[i_s][i_m][i_z] = new std::ifstream(filepath_distribution[i_s][i_m][i_z].c_str());

  char c;
  while (in_result_vector[i_s][i_m][i_z]->get(c)){
    //    if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result->tellg());}
    if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result_vector[i_s][i_m][i_z]->tellg());}
  }
  in_result_vector[i_s][i_m][i_z]->clear();



  int temp_counter = 2; // 1 without 'group counter' (old version)
  for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
    temp_counter++;
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      logger << LOG_DEBUG << "set pos_qTcut_distribution:   i_s = " << i_s << "   x_q = " << x_q << "   i_d = " << i_d << endl;
      no_line_distribution[i_s][i_d][x_q][0] = temp_counter;
      temp_counter += (osi->extended_distribution[i_d].n_bins + 1) * osi->n_scale_ren_TSV[i_s] * osi->n_scale_fact_TSV[i_s];

      pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d] = pos_line[i_s][i_m][i_z][no_line_distribution[i_s][i_d][x_q][0]];

      temp_counter++;
    }
  }

  // pos_line[i_s][i_m][i_z] not needed any longer

  // Probably only for checks (no function ???):
  for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      string temp_s;
      in_result_vector[i_s][i_m][i_z]->seekg(pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d]);//, is.beg);
      getline(*in_result_vector[i_s][i_m][i_z], temp_s);
    }
  }
  in_result_vector[i_s][i_m][i_z]->clear();
  // ... until here.


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::cleanup_file_distribution(){
  Logger logger("summary_subprocess::cleanup_file_distribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<vector<vector<int> > > ().swap(index_contributing_run);
  vector<vector<vector<ifstream * > > > ().swap(in_result_vector);
  vector<vector<vector<vector<streampos> > > > ().swap(pos_line);
  vector<vector<vector<string> > > ().swap(filepath_time);
  vector<vector<vector<string> > > ().swap(filepath_distribution);
  vector<vector<vector<double> > > ().swap(time_stamp);
  vector<vector<vector<vector<int> > > > ().swap(no_line_distribution);
  vector<vector<vector<vector<vector<streampos> > > > > ().swap(pos_qTcut_distribution);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::check_reset_file_distribution(int i_s, int i_m, int i_z){
  Logger logger("summary_subprocess::check_reset_file_distribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "Open file  " << filepath_distribution[i_s][i_m][i_z] << endl;

  double check_time_stamp = get_time_stamp(i_s, i_m, i_z);
  //  double check_time_stamp = get_time_stamp(filepath_time[i_s][i_m][i_z]);
  if (check_time_stamp == time_stamp[i_s][i_m][i_z]){
    //  logger << LOG_INFO << "time_stamp[" << i_s << "][" << i_m << "][" << i_z << "] = " << time_stamp[i_s][i_m][i_z] << endl;
    logger << LOG_DEBUG << "Open " << name << " at [" << i_s << "][" << i_m << "][" << i_z << "]  " << filepath_distribution[i_s][i_m][i_z] << "   " << "time_stamp = " << setprecision(15) << setw(23) << time_stamp[i_s][i_m][i_z] << "   successful -> return;" << endl;
    return;
  }

  logger << LOG_INFO << "File has changed : filepath_distribution[" << i_s << "][" << i_m << "][" << i_z << "] = " << filepath_distribution[i_s][i_m][i_z] << "   " << "time_stamp = " << time_stamp[i_s][i_m][i_z] << " =/= " << check_time_stamp << " = check_time_stamp - > needs to be reset!" << endl;

  time_stamp[i_s][i_m][i_z] = check_time_stamp;
  logger << LOG_DEBUG_VERBOSE << "Before in_result_vector[" << i_s << "][" << i_m << "][" << i_z << "]->close();" << endl;
  in_result_vector[i_s][i_m][i_z]->close();
  logger << LOG_DEBUG_VERBOSE << "After in_result_vector[" << i_s << "][" << i_m << "][" << i_z << "]->close();" << endl;

  pos_line[i_s][i_m][i_z].clear();

  // Set in_result_vector:
  /*
  string distribution_file;
  distribution_file = "distribution_" + name + ".txt";
  string filepath = "../" + ycontribution->extended_directory[i_m][index_contributing_run[i_s][i_m][i_z]] + "/distribution/" + osi->name_extended_set_TSV[i_s] + "/" + distribution_file;

  logger << LOG_INFO << filepath << "   needs to be reloaded." << endl;
  logger << LOG_DEBUG << "readin pos_line: " << filepath << endl;
  */
  //  ifstream* in_result = new std::ifstream(filepath.c_str());//, std::ios::in); // create in free store
  /*
  ifstream* in_result = new std::ifstream(filepath_distribution[i_s][i_m][i_z].c_str());
  //	ifstream in_result(filepath.c_str());
  in_result_vector[i_s][i_m][i_z] = in_result;
  */

  // Why is it done that way ???

  delete in_result_vector[i_s][i_m][i_z];
  in_result_vector[i_s][i_m][i_z] = new std::ifstream(filepath_distribution[i_s][i_m][i_z].c_str());
  char c;
  while (in_result_vector[i_s][i_m][i_z]->get(c)){
    //    if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result->tellg());}
    if (c == '\n'){pos_line[i_s][i_m][i_z].push_back(in_result_vector[i_s][i_m][i_z]->tellg());}
  }
  in_result_vector[i_s][i_m][i_z]->clear();
  //  in_result_vector[i_s][i_m][i_z]->clear();

  logger << LOG_INFO << "pos_line[" << i_s << "][" << i_m << "][" << i_z << "].size() = " << pos_line[i_s][i_m][i_z].size() << endl;

  int temp_counter = 2; // 1 without 'group counter' (old version)
  for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
    temp_counter++;
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      logger << LOG_INFO << "set pos_qTcut_distribution:   i_s = " << i_s << "   x_q = " << x_q << "   i_d = " << i_d << endl;
      no_line_distribution[i_s][i_d][x_q][0] = temp_counter;
      temp_counter += (osi->extended_distribution[i_d].n_bins + 1) * osi->n_scale_ren_TSV[i_s] * osi->n_scale_fact_TSV[i_s];

      pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d] = pos_line[i_s][i_m][i_z][no_line_distribution[i_s][i_d][x_q][0]];

      temp_counter++;
    }
  }

  // pos_line[i_s][i_m][i_z] not needed any longer

  // Probably only for checks (no function ???):
  for (int x_q = 0; x_q < ycontribution->selection_n_qTcut; x_q++){
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      string temp_s;
      logger << LOG_INFO << "set in_result_vector:   i_s = " << i_s << "   x_q = " << x_q << "   i_d = " << i_d << endl;
      in_result_vector[i_s][i_m][i_z]->seekg(pos_qTcut_distribution[i_s][i_m][i_z][x_q][i_d]);//, is.beg);
      getline(*in_result_vector[i_s][i_m][i_z], temp_s);
    }
  }
  in_result_vector[i_s][i_m][i_z]->clear();
  // ... until here.



  //  logger << LOG_INFO << "pos_line[" << i_s << "][" << i_m << "][" << i_z << "].size() = " << pos_line[i_s][i_m][i_z].size() <
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::cleanup(){
  Logger logger("summary_subprocess::cleanup");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(distribution_result_TSV);
  vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(distribution_deviation_TSV);
  vector<vector<vector<vector<vector<vector<double> > > > > > ().swap(distribution_chi2_TSV);

  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > ().swap(distribution_group_result_TSV);
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > ().swap(distribution_group_deviation_TSV);
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > ().swap(distribution_group_chi2_TSV);

  vector<vector<vector<vector<int> > > > ().swap(distribution_group_counter_removal_run_qTcut_TSV);
  vector<vector<vector<vector<int> > > > ().swap(distribution_group_counter_nonzero_run_qTcut_TSV);

  vector<vector<vector<vector<long long> > > > ().swap(distribution_group_N_binwise_TSV);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

