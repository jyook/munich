#include "header.hpp"

void summary_generic::initialization_distribution(){
  Logger logger("summary_generic::initialization_distribution");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  osi->determine_extended_distribution();

  logger << LOG_INFO << "Before adapting  output_selection_distribution:" << endl;
  logger << LOG_INFO << "output_selection_distribution.size() = " << output_selection_distribution.size() << endl;
  for (size_t i_s = 0; i_s < output_selection_distribution.size(); i_s++){
    stringstream temp;
    temp << "output_selection_distribution[" << setw(3) << i_s << "] = " << setw(30) << output_selection_distribution[i_s] << "   ";
    if (output_selection_distribution_format[i_s].size() > 0){
      size_t indent = (temp.str()).size();
      for (size_t i_f = 0; i_f < output_selection_distribution_format[i_s].size(); i_f++){
	if (i_f > 0){
	  temp << setw(indent) << "";
	}
	temp << output_selection_distribution_format[i_s][i_f] << endl;
      }
    }
    else {temp << endl;}
    logger << LOG_INFO << temp.str();
//    logger << LOG_INFO << "output_selection_distribution[" << i_s << "] = " << output_selection_distribution[i_s] << endl;
  }


  // Possibly move this part into initialization_distribution ???
  // Only needed for distributions !!! (separate distribution input file ???)
  // Change initialization_distribution accordingly
  if (output_selection_distribution.size() == 0){
    // default: output for all calculated distributions
    output_selection_distribution.resize(osi->extended_distribution.size(), "");
    output_selection_distribution_format.resize(osi->extended_distribution.size(), vector<string> ());
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      output_selection_distribution[i_d] = osi->extended_distribution[i_d].xdistribution_name;
    }
  }

  // Fill empty output_selection_distribution_format  (with subsequent ones):
  vector<size_t> n_output_selection_distribution_format(output_selection_distribution_format.size());
  size_t n_format_max = 0;
  for (size_t i_d = 0; i_d < output_selection_distribution_format.size(); i_d++){
    n_output_selection_distribution_format[i_d] = output_selection_distribution_format[i_d].size();
    if (n_output_selection_distribution_format[i_d] > n_format_max){n_format_max = n_output_selection_distribution_format[i_d];}
  }
  if (n_format_max == 0 && output_selection_distribution.size() > 0){
    if (output_format_default.size() == 0){output_format_default.push_back("plot");}
    output_selection_distribution_format[output_selection_distribution_format.size() - 1] = output_format_default;
    n_output_selection_distribution_format[n_output_selection_distribution_format.size() - 1] = output_format_default.size();
  }

  for (size_t i_d = 0; i_d < output_selection_distribution_format.size(); i_d++){
    if (output_selection_distribution_format[i_d].size() == 0){
      for (size_t j_d = i_d + 1; j_d < output_selection_distribution_format.size(); j_d++){
	if (n_output_selection_distribution_format[j_d] > 0){
	  output_selection_distribution_format[i_d] = output_selection_distribution_format[j_d];
	  n_output_selection_distribution_format[i_d] = n_output_selection_distribution_format[j_d];
	  break;
	}
      }
    }
  }

  logger << LOG_INFO << "After adapting  output_selection_distribution:" << endl;
  logger << LOG_INFO << "output_selection_distribution.size() = " << output_selection_distribution.size() << endl;
  for (size_t i_s = 0; i_s < output_selection_distribution.size(); i_s++){
    stringstream temp;
    temp << "output_selection_distribution[" << setw(3) << i_s << "] = " << setw(30) << output_selection_distribution[i_s] << "   ";
    if (output_selection_distribution_format[i_s].size() > 0){
      size_t indent = (temp.str()).size();
      for (size_t i_f = 0; i_f < output_selection_distribution_format[i_s].size(); i_f++){
	if (i_f > 0){
	  temp << setw(indent) << "";
	}
	temp << output_selection_distribution_format[i_s][i_f] << endl;
      }
    }
    else {temp << endl;}
    logger << LOG_INFO << temp.str();
//    logger << LOG_INFO << "output_selection_distribution[" << i_s << "] = " << output_selection_distribution[i_s] << endl;
  }






  logger << LOG_INFO << "switch_output_distribution.size() = " << switch_output_distribution.size() << endl;

  if (output_selection_distribution.size() == 0){
    switch_output_distribution.resize(osi->extended_distribution.size(), 1);
  }
  else {
    switch_output_distribution.resize(osi->extended_distribution.size(), 0);
    output_distribution_format.resize(osi->extended_distribution.size(), vector<string> ());

    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      logger << LOG_INFO << "osi->extended_distribution[" << i_d << "].xdistribution_name = " << osi->extended_distribution[i_d].xdistribution_name << endl;
      //	if (!switch_output_distribution[i_d]){continue;}
      for (int i_s = 0; i_s < output_selection_distribution.size(); i_s++){
	if (osi->extended_distribution[i_d].xdistribution_name == output_selection_distribution[i_s]){
	  switch_output_distribution[i_d] = 1;
	  output_distribution_format[i_d] = output_selection_distribution_format[i_s];
	  break;
	}
      }
      if (switch_output_distribution[i_d] == 1){
	logger << LOG_INFO << "switch_output_distribution[" << setw(2) << i_d << "] = " << switch_output_distribution[i_d] << endl;
	//	output_distribution_format[i_d] = output_selection_distribution_format[i_s];
      }
    }
  }

  logger << LOG_DEBUG << "osi->extended_distribution.size() = " << osi->extended_distribution.size() << endl;
  logger << LOG_DEBUG << "output_selection_distribution.size() = " << output_selection_distribution.size() << endl;
  for (int i_s = 0; i_s < output_selection_distribution.size(); i_s++){
    logger << LOG_DEBUG << "output_selection_distribution[" << i_s << "] = " << output_selection_distribution[i_s] << endl;

  }
  logger << LOG_DEBUG << "switch_output_distribution.size() = " << switch_output_distribution.size() << endl;
  for (int i_s = 0; i_s < switch_output_distribution.size(); i_s++){
    logger << LOG_DEBUG << "switch_output_distribution[" << i_s << "] = " << switch_output_distribution[i_s] << endl;
  }




  logger << LOG_INFO << "output_selection_distribution_table.size() = " << output_selection_distribution_table.size() << endl;
  for (int i_s = 0; i_s < output_selection_distribution_table.size(); i_s++){
    logger << LOG_INFO << "output_selection_distribution_table[" << i_s << "] = " << output_selection_distribution_table[i_s] << endl;

  }
  logger << LOG_INFO << "switch_output_distribution_table.size() = " << switch_output_distribution_table.size() << endl;

  if (output_selection_distribution_table.size() == 0){
    switch_output_distribution_table.resize(osi->extended_distribution.size(), 1);
  }
  else {
    switch_output_distribution_table.resize(osi->extended_distribution.size(), 0);
    for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
      logger << LOG_INFO << "osi->extended_distribution[" << i_d << "].xdistribution_name = " << osi->extended_distribution[i_d].xdistribution_name << endl;
      //	if (!switch_output_distribution_table[i_d]){continue;}
      for (int i_s = 0; i_s < output_selection_distribution_table.size(); i_s++){
	if (osi->extended_distribution[i_d].xdistribution_name == output_selection_distribution_table[i_s]){switch_output_distribution_table[i_d] = 1; break;}
      }
      if (switch_output_distribution_table[i_d] == 1){
	logger << LOG_INFO << "switch_output_distribution_table[" << setw(2) << i_d << "] = " << switch_output_distribution_table[i_d] << endl;
      }
    }
  }

  logger << LOG_DEBUG << "osi->extended_distribution.size() = " << osi->extended_distribution.size() << endl;
  logger << LOG_DEBUG << "output_selection_distribution_table.size() = " << output_selection_distribution_table.size() << endl;
  for (int i_s = 0; i_s < output_selection_distribution_table.size(); i_s++){
    logger << LOG_DEBUG << "output_selection_distribution_table[" << i_s << "] = " << output_selection_distribution_table[i_s] << endl;

  }




  // only used for asymmetries: could most likely be removed later...
  bin_edge.resize(osi->extended_distribution.size());
  for (int i_d = 0; i_d < osi->extended_distribution.size(); i_d++){
    if (!switch_output_distribution[i_d]){continue;}
    bin_edge[i_d] = osi->extended_distribution[i_d].bin_edge;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void summary_generic::initialization_scaleset_TSV(){
  Logger logger("summary_generic::initialization_scaleset_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  name_scale_variation_TSV.resize(7);
  name_scale_variation_TSV[0] = "complete";
  name_scale_variation_TSV[1] = "equal";
  name_scale_variation_TSV[2] = "ren";
  name_scale_variation_TSV[3] = "fact";
  name_scale_variation_TSV[4] = "antipodal";
  name_scale_variation_TSV[5] = "7-point";
  name_scale_variation_TSV[6] = "9-point";

  logger << LOG_INFO << "output_selection_scaleset.size() = " << output_selection_scaleset.size() << endl;
  for (int j_s = 0; j_s < output_selection_scaleset.size(); j_s++){
    logger << LOG_INFO << "output_selection_scaleset[" << j_s << "] = " << output_selection_scaleset[j_s] << endl;
  }
  logger << LOG_INFO << "switch_output_scaleset.size() = " << switch_output_scaleset.size() << endl;

  if (output_selection_scaleset.size() == 0){
    switch_output_scaleset.resize(osi->n_extended_set_TSV, 1);
  }
  else {
    switch_output_scaleset.resize(osi->n_extended_set_TSV, 0);
    for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
      logger << LOG_INFO << "osi->name_extended_set_TSV[" << i_s << "] = " << osi->name_extended_set_TSV[i_s] << endl;
      //	if (!switch_output_scaleset[i_s]){continue;}
      for (int j_s = 0; j_s < output_selection_scaleset.size(); j_s++){
	if (osi->name_extended_set_TSV[i_s] == output_selection_scaleset[j_s]){switch_output_scaleset[i_s] = 1; break;}
      }
      if (switch_output_scaleset[i_s] == 1){
	logger << LOG_INFO << "switch_output_scaleset[" << setw(2) << i_s << "] = " << switch_output_scaleset[i_s] << endl;
      }
    }
  }

  if (switch_output_scaleset[osi->no_reference_TSV] == 0){
    switch_output_scaleset[osi->no_reference_TSV] = 1;
    logger << LOG_INFO << "reference scale must be included in output: switch_output_scaleset[" << setw(2) << osi->no_reference_TSV << "] = " << switch_output_scaleset[osi->no_reference_TSV] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "osi->n_extended_set_TSV = " << osi->n_extended_set_TSV << endl;

  /*
  // only used for asymmetries: could most likely be removed later...
  bin_edge.resize(osi->n_extended_set_TSV);
  for (int i_d = 0; i_d < osi->n_extended_set_TSV; i_d++){
    if (!switch_output_scaleset[i_d]){continue;}
    bin_edge[i_d] = osi->extended_scaleset[i_d].bin_edge;
  }
  */


  scalename_TSV.resize(osi->n_extended_set_TSV);
  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
    scalename_TSV[i_s].resize(osi->n_scale_ren_TSV[i_s], vector<string> (osi->n_scale_fact_TSV[i_s]));
    if (osi->switch_distribution_TSV[i_s] != 0){
      system_execute(logger, "mkdir " + final_resultdirectory + "/" + osi->name_extended_set_TSV[i_s]);
      for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	  stringstream scalepart;
	  scalepart << "scale." << i_r << "." << i_f;
	  scalename_TSV[i_s][i_r][i_f] = final_resultdirectory + "/" + osi->name_extended_set_TSV[i_s] + "/" + scalepart.str();
	  system_execute(logger, "mkdir " + scalename_TSV[i_s][i_r][i_f]);
	  logger << LOG_DEBUG_VERBOSE << "mkdir " << scalename_TSV[i_s][i_r][i_f] << endl;
	}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void summary_generic::initialization_scaleset_CV(){
  Logger logger("summary_generic::initialization_scaleset_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  osi->initialization_CV();

  if (osi->switch_CV){
    system_execute(logger, "mkdir " + final_resultdirectory + "/CV");
  }

  if      (osi->switch_CV == 1){name_variation_CV = "equal";}
  else if (osi->switch_CV == 2){name_variation_CV = "ren";}
  else if (osi->switch_CV == 3){name_variation_CV = "fac";}
  else if (osi->switch_CV == 4){name_variation_CV = "antipodal";}
  else if (osi->switch_CV == 5){name_variation_CV = "7-point";}
  else if (osi->switch_CV == 6){name_variation_CV = "9-point";}

  scalename_CV.resize(osi->n_scales_CV);
  for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
    stringstream temp;
    temp << "scale." << i_s;// << "/";
    scalename_CV[i_s] = temp.str();
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_generic::initialization_summary_list(){
  Logger logger("summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){logger << LOG_INFO << "list_contribution_file[" << i_l << "] = " << list_contribution_file[i_l] << endl;}

  initialization_summary_list(list_contribution_file.size());

  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->initialization(list_contribution_file[i_l], this);}
  for (int i_l = 0; i_l < xlist.size(); i_l++){xlist[i_l]->output_info();}

  for (int i_l = 0; i_l < xlist.size(); i_l++){
    for (int i_c = 1; i_c < xlist[i_l]->xcontribution.size(); i_c++){
      xlist[i_l]->xcontribution[i_c]->ylist = xlist[i_l];
      for (int i_p = 1; i_p < xlist[i_l]->xcontribution[i_c]->xsubprocess.size(); i_p++){xlist[i_l]->xcontribution[i_c]->xsubprocess[i_p]->ycontribution = xlist[i_l]->xcontribution[i_c];}
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// Update the readin format !!!

void summary_generic::readin_combination_infile(){
  Logger logger("summary_generic::readin_combination_infile");
  logger << LOG_DEBUG << "called" << endl;
  vector<string> readin;
  char LineBuffer[128];
  logger << LOG_INFO << "infilename = " << infilename << endl;

  ifstream in_file(infilename.c_str());
  readin.clear();
  while (in_file.getline(LineBuffer, 1024)){readin.push_back(LineBuffer);}

  vector<string> user_variable;
  vector<string> user_value;
  int user_counter = -1;
  for (int i = 0; i < readin.size(); i++){
    if (readin[i].size() != 0){
      if (readin[i][0] != '/' && readin[i][0] != '#' && readin[i][0] != '%'){
	int start = 0;
	user_counter++;
	user_variable.push_back("");
	user_value.push_back("");
	for (int j = 0; j < readin[i].size(); j++){
	  if (start == 0 || start == 1){
	    if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 0){}
	    else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	      user_variable[user_counter].push_back(readin[i][j]);
	      if (start != 1){start = 1;}
	    }
	    else {start++;}
	  }
	  else if (start == 2){
	    if (readin[i][j] == '='){start++;}
	    else if ((readin[i][j] == ' ') || (readin[i][j] == char(9))){}
	    else {cout << readin[i][j] << "    Incorrect input in line " << i + 1 << endl; exit(1);}
	  }
	  else if (start == 3 || start == 4){
	    if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 3){}
	    else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	      user_value[user_counter].push_back(readin[i][j]);
	      if (start != 4){start = 4;}
	    }
	    else {start++;}
	  }
	  else {break;}
	}
	if (start == 0){
	  user_counter--;
	  user_variable.erase(user_variable.end() - 1, user_variable.end());
	  user_value.erase(user_value.end() - 1, user_value.end());
	}
      }
    }
  }

  int counter_directory = -1;
  int counter_combination = -1;
  int counter_selection_distribution = -1;


  // First determine and declare process-depenendent summary_order sets:

  logger << LOG_DEBUG << "list_order.size() = " << list_order.size() << endl;
  list_order.clear();
  for (int i = 0; i < user_variable.size(); i++){
    if (user_variable[i] == "resultdirectory"){
      list_order.push_back(user_value[i]);
    }
  }
  logger << LOG_DEBUG << "list_order.size() = " << list_order.size() << endl;
  for (int i_o = 0; i_o < list_order.size(); i_o++){
    logger << LOG_DEBUG << "list_order[" << setw(2) << i_o << "] = " << list_order[i_o] << endl;
  }
  logger << LOG_DEBUG_VERBOSE << "yorder.size() = " << yorder.size() << endl;
  initialization_summary_order(list_order.size());
  logger << LOG_DEBUG << "list_order.size() = " << list_order.size() << endl;
  logger << LOG_DEBUG_VERBOSE << "yorder.size() = " << yorder.size() << endl;

  phasespace_optimization.push_back("");


  vector<string> accuracy_normalization;

  for (int i = 0; i < user_variable.size(); i++){
    logger << LOG_DEBUG << "i = " << setw(3) << i << "   " << user_variable[i] << endl;
    if (user_variable[i] == "final_resultdirectory"){
      final_resultdirectory = user_value[i];
    }
    else if (user_variable[i] == "switch_generate_rundirectories"){
      switch_generate_rundirectories = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "run_min_n_step"){
      run_min_n_step = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "run_factor_max"){
      run_factor_max = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "run_min_n_event"){
      run_min_n_event = atoll(user_value[i].c_str());
    }
    else if (user_variable[i] == "run_max_time_per_job"){
      run_max_time_per_job = 3600 * atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "run_min_number_of_jobs_for_one_channel"){
      run_min_number_of_jobs_for_one_channel = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "no_qTcut_runtime_estimate"){
      no_qTcut_runtime_estimate = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "directory_runtime_estimate"){
      directory_runtime_estimate = user_value[i].c_str();
    }
    else if (user_variable[i] == "deviation_tolerance_factor"){
      deviation_tolerance_factor = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "min_qTcut_extrapolation"){
      min_qTcut_extrapolation = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "switch_extrapolation_result"){
      switch_extrapolation_result = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "switch_extrapolation_distribution"){
      switch_extrapolation_distribution = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "max_qTcut_extrapolation"){
      max_qTcut_extrapolation = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "min_max_value_extrapolation_range"){
      min_max_value_extrapolation_range = atof(user_value[i].c_str());
    }
    else if (user_variable[i] == "min_n_qTcut_extrapolation_range"){
      min_n_qTcut_extrapolation_range = atoi(user_value[i].c_str());
    }
    else if (user_variable[i] == "error_extrapolation_range_chi2"){
      error_extrapolation_range_chi2 = atof(user_value[i].c_str());
    }

    else if (user_variable[i] == "average_factor"){
      average_factor = atoi(user_value[i].c_str());
      // average_factor decides, how the results are combined:
      // average_factor = 1: conservative
      // average_factor = 0: alternative
      // average_factor = n: hybrid
    }

    else if (user_variable[i] == "phasespace_optimization"){
      phasespace_optimization.push_back("." + user_value[i]);
    }

    else if (user_variable[i] == "resultdirectory"){
      counter_directory++;
      logger << LOG_DEBUG_VERBOSE << "counter_directory = " << counter_directory << endl;
      logger << LOG_DEBUG_VERBOSE << "yorder.size() = " << yorder.size() << endl;
      logger << LOG_DEBUG_VERBOSE << "yorder[counter_directory]->initialization " << endl;
      yorder[counter_directory]->initialization(user_value[i], this);
      logger << LOG_DEBUG_VERBOSE << "after yorder[counter_directory]->initialization " << endl;
      //      yorder.push_back(summary_order(user_value[i], *this));
      counter_combination = 0;
    }

    else if (user_variable[i] == "contribution_file"){
      yorder[counter_directory]->contribution_file.push_back(user_value[i]);
      yorder[counter_directory]->combination[counter_combination].push_back(yorder[counter_directory]->contribution_file.size() - 1);
    }

    else if (user_variable[i] == "combination"){
      // to be made more sophisticated...
      yorder[counter_directory]->combination.push_back(vector<int> ());
      counter_combination++;
      if (user_value[i] == "+"){yorder[counter_directory]->combination_type.push_back(0);}
      else if (user_value[i] == "x"){yorder[counter_directory]->combination_type.push_back(1);}
      else if (user_value[i] == "-"){yorder[counter_directory]->combination_type.push_back(2);}
    }

    else if (user_variable[i] == "accuracy_relative"){
      yorder[counter_directory]->accuracy_relative = atof(user_value[i].c_str());
    }

    else if (user_variable[i] == "output_selection_distribution"){
      output_selection_distribution.push_back(user_value[i]);
      output_selection_distribution_format.push_back(vector<string> ());
      counter_selection_distribution++;
    }
    else if (user_variable[i] == "output_selection_distribution_format"){
      if (counter_selection_distribution == -1){
	output_format_default.push_back(user_value[i]);
      }
      else {
	output_selection_distribution_format[counter_selection_distribution].push_back(user_value[i]);
      }
    }

    else if (user_variable[i] == "output_selection_scaleset"){
      output_selection_scaleset.push_back(user_value[i]);
    }

    else if (user_variable[i] == "switch_output_subprocess"){
      switch_output_subprocess = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_contribution"){
      switch_output_contribution = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_list"){
      switch_output_list = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_order"){
      switch_output_order = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_plot"){
      switch_output_plot = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_qTcut"){
      switch_output_qTcut = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_result"){
      switch_output_result = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_overview"){
      switch_output_overview = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_table_order"){
      switch_output_table_order = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_table_Kfactor"){
      switch_output_table_Kfactor = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_table_crosssection_Kfactor"){
      switch_output_table_crosssection_Kfactor = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW"){
      switch_output_table_crosssection_Kfactor_combination_NNLOQCD_NLOEW = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "switch_output_table_IS_splitting"){
      switch_output_table_IS_splitting = atoi(user_value[i].c_str());
    }

    else if (user_variable[i] == "output_selection_distribution_table"){
      output_selection_distribution_table.push_back(user_value[i]);
    }


    else if (user_variable[i] == "accuracy_normalization"){
      yorder[counter_directory]->accuracy_normalization = user_value[i];
    }
  }


  
  if (phasespace_optimization.size() > 1){
    phasespace_optimization.push_back(".combined");
  }
  for (int i_z = 0; i_z < phasespace_optimization.size(); i_z++){
    logger << LOG_INFO << "phasespace_optimization[" << setw(2) << i_z << "] = " << phasespace_optimization[i_z] << endl;
  }

  for (int i_c = 0; i_c < yorder.size(); i_c++){
    if (yorder[i_c]->accuracy_normalization == ""){yorder[i_c]->accuracy_no_normalization = i_c;}
    else {
      for (int j_c = 0; j_c < yorder.size(); j_c++){
	if (yorder[i_c]->accuracy_normalization == yorder[j_c]->resultdirectory){yorder[i_c]->accuracy_no_normalization = j_c; break;}
      }
    }
  }
  for (int i_c = 0; i_c < yorder.size(); i_c++){
    if (yorder[i_c]->accuracy_relative == 0.){yorder[i_c]->accuracy_relative = 0.001;}
  }



  for (int i_c = 0; i_c < yorder.size(); i_c++){
    for (int j_c = 0; j_c < yorder[i_c]->contribution_file.size(); j_c++){
      int flag = list_contribution_file.size();
      for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){
	if (yorder[i_c]->contribution_file[j_c] == list_contribution_file[i_l]){flag = i_l; break;}
      }
      if (flag == list_contribution_file.size()){
	list_contribution_file.push_back(yorder[i_c]->contribution_file[j_c]);
	mapping_contribution_file[yorder[i_c]->contribution_file[j_c]] = flag;
      }
    }
  }



  logger << LOG_DEBUG << "final_resultdirectory = " << final_resultdirectory << endl;
  for (int i_l = 0; i_l < list_contribution_file.size(); i_l++){logger << LOG_DEBUG << "list_contribution_file[" << i_l << "] = " << list_contribution_file[i_l] << endl;}

  logger << LOG_DEBUG << "finished" << endl;
}



void summary_generic::readin_infile_scaleband(){
  Logger logger("summary_generic::readin_infile_scaleband");
  logger << LOG_DEBUG << "called" << endl;

  string vs;
  string s0;
  vector<string> vs0;
  vector<int> vi0;

  string filename;
  string rundirectory;

  vector<string> readin;
  char LineBuffer[128];

  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;
  ifstream in_file(infilename_scaleband.c_str());
  readin.clear();
  while (in_file.getline(LineBuffer, 1024)){readin.push_back(LineBuffer);}
  logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;
  vector<string> user_variable;
  vector<string> user_value;
  int user_counter = -1;
  for (int i = 0; i < readin.size(); i++){
    logger << LOG_DEBUG << "readin[" << setw(3) << i << "] = " << readin[i] << endl;

    if (readin[i].size() != 0){
      if (readin[i][0] != '/' && readin[i][0] != '#' && readin[i][0] != '%'){
	int start = 0;
	user_counter++;
	user_variable.push_back(s0);
	user_value.push_back(s0);
	for (int j = 0; j < readin[i].size(); j++){
	  if (start == 0 || start == 1){
	    if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 0){}
	    else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	      user_variable[user_counter].push_back(readin[i][j]);
	      if (start != 1){start = 1;}
	    }
	    else {start++;}
	  }
	  else if (start == 2){
	    if (readin[i][j] == '='){start++;}
	    else if ((readin[i][j] == ' ') || (readin[i][j] == char(9))){}
	    else {logger << LOG_FATAL << readin[i][j] << "    Incorrect input in line " << i + 1 << endl; exit(1);}
	  }
	  else if (start == 3 || start == 4){
	    if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 3){}
	    else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	      user_value[user_counter].push_back(readin[i][j]);
	      if (start != 4){start = 4;}
	    }
	    else {start++;}
	  }
	  else {break;}
	}
	if (start == 0){
	  user_counter--;
	  user_variable.erase(user_variable.end() - 1, user_variable.end());
	  user_value.erase(user_value.end() - 1, user_value.end());
	}
      }
    }
  }

  logger << LOG_DEBUG << "user_variable.size() = " << user_variable.size() << endl;
  logger << LOG_DEBUG << "user_value.size() = " << user_value.size() << endl;

  string outpath;
  int counter_contribution = -1;
  for (int i = 0; i < user_variable.size(); i++){
    logger << LOG_DEBUG << "user_variable[" << i << "] = " << user_variable[i] << endl;
    logger << LOG_DEBUG << "user_value[" << i << "] = " << user_value[i] << endl;

    if (user_variable[i] == "outpath"){
      counter_contribution++;
      outpath = user_value[i];
      //      system_execute(logger, "mkdir " + final_resultdirectory + "/" + outpath);
      inpath_scalecentral.push_back("");
      inpath_scalevariation.push_back(vector<string> (0));
      outpath_scalegeneva.push_back(outpath + "/scale.geneva");
      outpath_scalecentral.push_back(outpath + "/scale.central");
      outpath_scaleband.push_back(outpath + "/scale.band");
      outpath_scalemax.push_back(outpath + "/scale.max");
      outpath_scalemin.push_back(outpath + "/scale.min");
      outpath_scaleplusmax.push_back(outpath + "/scale.plusmax");
      outpath_scaleplusmin.push_back(outpath + "/scale.plusmin");
      outpath_scaleminusmax.push_back(outpath + "/scale.minusmax");
      outpath_scaleminusmin.push_back(outpath + "/scale.minusmin");
    }
    else if (user_variable[i] == "inpath_scalecentral"){inpath_scalecentral[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "inpath_scalevariation"){inpath_scalevariation[counter_contribution].push_back(user_value[i]);}
    else if (user_variable[i] == "outpath_scalegeneva"){outpath_scalegeneva[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scalecentral"){outpath_scalecentral[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scaleband"){outpath_scaleband[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scalemax"){outpath_scalemax[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scalemin"){outpath_scalemin[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scaleplusmax"){outpath_scaleplusmax[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scaleplusmin"){outpath_scaleplusmin[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scaleminusmax"){outpath_scaleminusmax[counter_contribution] = user_value[i];}
    else if (user_variable[i] == "outpath_scaleminusmin"){outpath_scaleminusmin[counter_contribution] = user_value[i];}
    else {logger << LOG_WARN << "Input is not known." << endl;}
  }

  logger << LOG_DEBUG << "finished" << endl;
}


