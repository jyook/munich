#include "header.hpp"

void summary_subprocess::initialization_runtime(){
  Logger logger("summary_subprocess::readin_runtime");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Determines for each subprocess (for the selected scales, qTcut values, etc.):
  // error2_time, time_per_event, used_runtime, used_n_event

  error2_time = 0.;
  time_per_event = 0.;
  used_runtime = 0.;
  used_n_event = 0;
  extrapolated_runtime = 0.;

  N.resize(n_seed, 0);
  result_run.resize(n_seed, 0.);
  deviation_run.resize(n_seed, 0.);

  error2_time_run.resize(n_seed, 0.);
  time_per_event_run.resize(n_seed, 0.);
  used_runtime_run.resize(n_seed, 0.);
  used_n_event_run.resize(n_seed, 0);


  error2_time_group.resize(ycontribution->extended_directory.size(), 0.);
  time_per_event_group.resize(ycontribution->extended_directory.size(), 0.);
  used_runtime_group.resize(ycontribution->extended_directory.size(), 0.);
  used_n_event_group.resize(ycontribution->extended_directory.size(), 0);

  extrapolated_runtime_group.resize(ycontribution->extended_directory.size(), 0.);
  extrapolated_n_event_group.resize(ycontribution->extended_directory.size(), 0.);
  extrapolated_sigma_deviation_group.resize(ycontribution->extended_directory.size(), 0.);
  extrapolated_sigma_normalization_group.resize(ycontribution->extended_directory.size(), 0.);
  extrapolated_sigma_normalization_deviation_group.resize(ycontribution->extended_directory.size(), 0.);

  error2_time_group_run.resize(ycontribution->extended_directory.size());
  time_per_event_group_run.resize(ycontribution->extended_directory.size());
  used_runtime_group_run.resize(ycontribution->extended_directory.size());
  used_n_event_group_run.resize(ycontribution->extended_directory.size());

  result_group_run.resize(ycontribution->extended_directory.size());
  deviation_group_run.resize(ycontribution->extended_directory.size());
  N_group.resize(ycontribution->extended_directory.size());

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    logger << LOG_WARN << "ycontribution->extended_directory[" << i_m << "].size() = " << ycontribution->extended_directory[i_m].size() << endl;
    error2_time_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
    time_per_event_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
    used_runtime_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
    used_n_event_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);

    result_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
    deviation_group_run[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
    N_group[i_m].resize(ycontribution->extended_directory[i_m].size(), 0.);
  }

  N_group_max.resize(ycontribution->extended_directory.size(), 0);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_subprocess::readin_runtime(string result_moment){
  Logger logger("summary_subprocess::readin_runtime");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization_runtime();

  double used_n_event_time = 0.;
  double used_runtime_time = 0.;

  if (osi->switch_TSV){remove_run_qTcut = remove_run_qTcut_TSV;}
  else if (osi->switch_CV){remove_run_qTcut = remove_run_qTcut_CV;}
  else {logger << LOG_ERROR << "Should not happen! Run-time determination is not correctly selected." << endl; exit(1);}

  logger << LOG_DEBUG << "ygeneric->directory_runtime_estimate = " << ygeneric->directory_runtime_estimate << endl;
  if (ygeneric->directory_runtime_estimate != ""){
    string result_file = result_moment + "_" + name + ".dat";

    logger << LOG_DEBUG << "ycontribution->directory[0] = " << ycontribution->directory[0] << endl;
    string addtopath = "";
    for (int i_s = ycontribution->directory[0].size() - 1; i_s >= 0; i_s--){
      if (ycontribution->directory[0][i_s] == '/'){
	addtopath = ycontribution->directory[0].substr(0, i_s + 1);
	logger << LOG_DEBUG << "addtopath = " << addtopath << endl;
	break;
      }
    }

    string filename = "../" + addtopath + ygeneric->directory_runtime_estimate + "/result/" + result_file;
    logger << LOG_DEBUG << "filename (time) = " << filename << endl;

    vector<string> readin;
    char LineBuffer[128];
    ifstream in_result(filename.c_str());
    while (in_result.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    in_result.close();

    if (readin.size() > 5){
      //      error2_time_time = atof(readin[readin.size() - 2].c_str());
      //      time_per_event_time = atof(readin[readin.size() - 3].c_str());
      used_runtime_time = atof(readin[readin.size() - 4].c_str());
      used_n_event_time = atoll(readin[0].c_str());
      //      N_time = atoll(readin[0].c_str());
    }
    logger << LOG_INFO << name << ": used_runtime_time = " << setw(15) << setprecision(8) << used_runtime_time << "   used_n_event_time = " << setw(15) << used_n_event_time << endl;
  }



  // New implementation - monitor runtime of standard and extra mappings separately:
  // also needed for "time" part !!!

  logger << LOG_WARN << "ycontribution->extended_directory.size() = " << ycontribution->extended_directory.size() << endl;

  for (int i_m = 0; i_m < ycontribution->extended_directory.size(); i_m++){
    logger << LOG_WARN << "ycontribution->extended_directory[" << i_m << "].size() = " << ycontribution->extended_directory[i_m].size() << endl;
    logger << LOG_WARN << "n_seed = " << n_seed << endl;
    for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
      string result_file = result_moment + "_" + name + ".dat";
      logger << LOG_DEBUG_VERBOSE << "result_file = " << result_file << endl;
      //    string filename = "../" + ycontribution->directory[i_z] + "/result/" + result_file;
      string filename = "../" + ycontribution->extended_directory[i_m][i_z] + "/result/" + result_file;
      logger << LOG_WARN << "NEW filename[" << i_m << "][" << i_z << "] = " << filename << endl;

      vector<string> readin;
      char LineBuffer[128];
      ifstream in_result(filename.c_str());
      while (in_result.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
      in_result.close();

      if (readin.size() > 5){
	error2_time_group_run[i_m][i_z] = atof(readin[readin.size() - 2].c_str());
	time_per_event_group_run[i_m][i_z] = atof(readin[readin.size() - 3].c_str());
	used_runtime_group_run[i_m][i_z] = atof(readin[readin.size() - 4].c_str());

	used_n_event_group_run[i_m][i_z] = atoll(readin[0].c_str());
	N_group[i_m][i_z] = atoll(readin[0].c_str());

	logger << LOG_DEBUG << setw(15) << name << "[" << setw(4) << i_z << "]: time(s) = " << setw(6) << used_runtime_group_run[i_m][i_z] << "   n_event = " << setw(15) << used_n_event_group_run[i_m][i_z] << "   time_per_event = " << setw(15) << setprecision(8) << time_per_event_group_run[i_m][i_z] << "   error2/time = " << setw(15) << setprecision(8) << error2_time_group_run[i_m][i_z] << endl;

	//  runtime correction from directory_runtime_estimate (assume same time/event as in time run): !!!
	used_runtime_time = 0.;
	if (used_runtime_time != 0.){// Should this really change the runtime output ???
	  double new_used_runtime = used_n_event_group_run[i_m][i_z] / used_n_event_time * used_runtime_time;
	  time_per_event_group_run[i_m][i_z] = time_per_event_group_run[i_m][i_z] * (new_used_runtime / used_runtime_group_run[i_m][i_z]);
	  error2_time_group_run[i_m][i_z] = error2_time_group_run[i_m][i_z] * (new_used_runtime / used_runtime_group_run[i_m][i_z]);
	  used_runtime_group_run[i_m][i_z] = new_used_runtime;

	  logger << LOG_INFO << name << ": (rescaled) new_used_runtime = " << setw(15) << new_used_runtime << endl;
	}
	logger << LOG_INFO << name
	       << ": used_runtime_group_run[" << i_m << "][" << i_z << "] = " << setw(10) << used_runtime_group_run[i_m][i_z]
	       << ": used_n_event_group_run = " << setw(10) << used_n_event_group_run[i_m][i_z]
	       << ": time_per_event_group_run = " << setw(10) << time_per_event_group_run[i_m][i_z]
	       << ": error2_time_group_run = " << setw(10) << error2_time_group_run[i_m][i_z] << endl;

	double default_deviation = atof(readin[2].c_str());

	if (osi->switch_TSV && osi->name_reference_TSV != ""){
	  logger << LOG_DEBUG << "default_deviation = " << default_deviation << endl;
	  if (error2_time_group_run[i_m][i_z] != 0. && default_deviation != 0.){error2_time_group_run[i_m][i_z] = error2_time_group_run[i_m][i_z] * pow(deviation_group_run[i_m][i_z] / default_deviation, 2);}
	}

	// later: else if ...
	else if (osi->switch_CV && ycontribution->active_qTcut && ygeneric->no_qTcut_runtime_estimate){
	  int temp_position = 3;
	  if (osi->switch_CV){
	    temp_position += osi->n_scales_CV * 2 * ygeneric->no_qTcut_runtime_estimate + ((osi->n_scales_CV - 1) / 2) * 2;
	  }
	  result_group_run[i_m][i_z] = atof(readin[temp_position].c_str());
	  deviation_group_run[i_m][i_z] = atof(readin[temp_position + 1].c_str());

	  if (error2_time_group_run[i_m][i_z] != 0. && default_deviation != 0.){error2_time_group_run[i_m][i_z] = error2_time_group_run[i_m][i_z] * pow(deviation_group_run[i_m][i_z] / default_deviation, 2);}
	}
	else if (osi->switch_CV){
	  int temp_position = 3 + ((osi->n_scales_CV - 1) / 2) * 2;
	  result_group_run[i_m][i_z] = atof(readin[temp_position].c_str());
	  deviation_group_run[i_m][i_z] = atof(readin[temp_position + 1].c_str());
	  if (error2_time_group_run[i_m][i_z] != 0. && default_deviation != 0.){error2_time_group_run[i_m][i_z] = error2_time_group_run[i_m][i_z] * pow(deviation_group_run[i_m][i_z] / default_deviation, 2);}
	}
	else {
	  logger << LOG_ERROR << "Should not happen! Run-time determination is not correctly selected." << endl;
	}
      }
      else {
	if (readin.size() != 0){logger << LOG_ERROR << result_file << "   error" << endl;}
	else {logger << LOG_DEBUG << result_file << "   empty" << endl;}
	used_n_event_group_run[i_m][i_z] = 0;
	N_group[i_m][i_z] = 0;
	result_group_run[i_m][i_z] = 0.;
	deviation_group_run[i_m][i_z] = 0.;
	error2_time_group_run[i_m][i_z] = 0.;
	time_per_event_group_run[i_m][i_z] = 0.;
	used_runtime_group_run[i_m][i_z] = 0.;
      }

      string result_file_parameter = "file_parameter." + name + ".dat";
      logger << LOG_DEBUG_VERBOSE << "result_file_parameter = " << result_file_parameter << endl;
      filename = "../" + ycontribution->extended_directory[i_m][i_z] + "/log/" + result_file_parameter;
      logger << LOG_WARN << "NEW filename[" << i_m << "][" << i_z << "] = " << filename << endl;
      // temp_isi actually required ???
      inputparameter_set temp_isi;
      readin.clear();
      temp_isi.parameter_readin_file(filename, readin, false, 1);
      temp_isi.get_userinput_from_readin(readin, temp_isi.input_complete);
      for (int i = temp_isi.input_complete.size() - 1; i >= 0; i--){
	if (temp_isi.input_complete[i].variable == "n_events_max"){
	  long long temp_N = atoll((temp_isi.input_complete[i].value).c_str());
	  if (N_group_max[i_m] < temp_N){N_group_max[i_m] = temp_N;}
	  break;
	}
      }
      
      //  logger << LOG_INFO << "Averaging of convergence estimators" << endl;
      int counter_e2_t = 0;
      int counter_t_n = 0;
      error2_time_group[i_m] = 0.;
      time_per_event_group[i_m] = 0.;
      used_runtime_group[i_m] = 0.;
      used_n_event_group[i_m] = 0;

      int this_no_qTcut_runtime_estimate = 0;
      if (ycontribution->active_qTcut){this_no_qTcut_runtime_estimate = ygeneric->no_qTcut_runtime_estimate;}

      // Simply average over single runs that are not excluded from combination (error2_time, used_n_event):
      for (int i_z = 0; i_z < ycontribution->extended_directory[i_m].size(); i_z++){
	if (!remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z]){
	  if (error2_time_group_run[i_m][i_z] != 0.){
	    error2_time_group[i_m] += error2_time_group_run[i_m][i_z];
	    counter_e2_t++;
	  }
	  if (time_per_event_group_run[i_m][i_z] != 0.){
	    time_per_event_group[i_m] += time_per_event_group_run[i_m][i_z];
	    counter_t_n++;
	  }
	}
	else {
	  logger << LOG_INFO << ycontribution->type_contribution << "   " << setw(15) << name << "   run " << i_z << "   removed in time calculation:   remove_run_qTcut[" << this_no_qTcut_runtime_estimate << "][" << i_z << "] = " << remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z] << endl;
	}
	// still count the runtimes and event if run is removed.
	used_runtime_group[i_m] += used_runtime_group_run[i_m][i_z];
	used_n_event_group[i_m] += used_n_event_group_run[i_m][i_z];
      }
      if (counter_e2_t > 0){error2_time_group[i_m] = sqrt(error2_time_group[i_m] / counter_e2_t);}
      if (counter_t_n > 0){time_per_event_group[i_m] = time_per_event_group[i_m] / counter_t_n;}

    }
    logger << LOG_INFO << name
	   << ": used_runtime_group[" << i_m << "] = " << setw(10) << used_runtime_group[i_m]
	   << ": used_n_event_group[" << i_m << "] = " << setw(10) << used_n_event_group[i_m]
	   << ": time_per_event_group[" << i_m << "] = " << setw(10) << time_per_event_group[i_m]
	   << ": error2_time_group[" << i_m << "] = " << setw(10) << error2_time_group[i_m] << endl;
  }












  for (int i_z = 0; i_z < n_seed; i_z++){
    string result_file = result_moment + "_" + name + ".dat";
    logger << LOG_DEBUG_VERBOSE << "result_file = " << result_file << endl;
    string filename = "../" + ycontribution->directory[i_z] + "/result/" + result_file;
    logger << LOG_DEBUG << "filename (i_z = " << setw(4) << i_z << ") = " << filename << endl;
    vector<string> readin;
    char LineBuffer[128];
    ifstream in_result(filename.c_str());
    while (in_result.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    in_result.close();
    logger << LOG_DEBUG << "default_size_CV  = " << default_size_CV << endl;
    logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;
    logger << LOG_DEBUG << "osi->n_scales_CV = " << osi->n_scales_CV << endl;
    logger << LOG_DEBUG << "osi->n_qTcut = " << osi->n_qTcut << endl;
    //    logger << LOG_DEBUG << "osi->active_qTcut = " << osi->active_qTcut << endl;
    logger << LOG_DEBUG << "ygeneric->no_qTcut_runtime_estimate = " << ygeneric->no_qTcut_runtime_estimate << endl;
    logger << LOG_DEBUG << "ycontribution->selection_n_qTcut = " << ycontribution->selection_n_qTcut << endl;
    logger << LOG_DEBUG << "ycontribution->output_n_qTcut = " << ycontribution->output_n_qTcut << endl;
    logger << LOG_DEBUG << "ycontribution->active_qTcut = " << ycontribution->active_qTcut << endl;


    if (readin.size() > 5){
      error2_time_run[i_z] = atof(readin[readin.size() - 2].c_str());
      time_per_event_run[i_z] = atof(readin[readin.size() - 3].c_str());
      used_runtime_run[i_z] = atof(readin[readin.size() - 4].c_str());

      used_n_event_run[i_z] = atoll(readin[0].c_str());
      N[i_z] = atoll(readin[0].c_str());

      logger << LOG_DEBUG << setw(15) << name << "[" << setw(4) << i_z << "]: time(s) = " << setw(6) << used_runtime_run[i_z] << "   n_event = " << setw(15) << used_n_event_run[i_z] << "   time_per_event = " << setw(15) << setprecision(8) << time_per_event_run[i_z] << "   error2/time = " << setw(15) << setprecision(8) << error2_time_run[i_z] << endl;

      //  runtime correction from directory_runtime_estimate (assume same time/event as in time run):

      if (used_runtime_time != 0.){
	double new_used_runtime = used_n_event_run[i_z] / used_n_event_time * used_runtime_time;
	time_per_event_run[i_z] = time_per_event_run[i_z] * (new_used_runtime / used_runtime_run[i_z]);
	error2_time_run[i_z] = error2_time_run[i_z] * (new_used_runtime / used_runtime_run[i_z]);
	used_runtime_run[i_z] = new_used_runtime;

	logger << LOG_INFO << name << ": (rescaled) new_used_runtime = " << setw(15) << new_used_runtime << endl;
      }

      double default_deviation = atof(readin[2].c_str());

      logger << LOG_DEBUG << "osi->switch_TSV = " << osi->switch_TSV << endl;
      logger << LOG_DEBUG << "osi->switch_CV = " << osi->switch_CV << endl;
      logger << LOG_DEBUG << "osi->name_reference_TSV = " << osi->name_reference_TSV << endl;
      logger << LOG_DEBUG << "ycontribution->active_qTcut = " << ycontribution->active_qTcut << endl;
      logger << LOG_DEBUG << "ygeneric->no_qTcut_runtime_estimate = " << ygeneric->no_qTcut_runtime_estimate << endl;


     if (osi->switch_TSV && osi->name_reference_TSV != ""){
       //	double default_result = atof(readin[1].c_str());
       ///	double default_deviation = atof(readin[2].c_str());
	//	logger << LOG_DEBUG << "default_result = " << default_result << endl;
	logger << LOG_DEBUG << "default_deviation = " << default_deviation << endl;
	/*///
	logger << LOG_DEBUG << "osi->name_reference_TSV = " << osi->name_reference_TSV << endl;

	int x_m = 0;
	// 0 replaced by reference value:
	int x_s = osi->no_reference_TSV;
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  logger << LOG_DEBUG << "osi->name_extended_set_TSV[" << i_s << "] = " << osi->name_extended_set_TSV[i_s] << endl;
	  if (osi->name_reference_TSV == osi->name_extended_set_TSV[i_s]){x_s = i_s; break;}
	}
	int x_r = osi->no_scale_ren_reference_TSV;
	int x_f = osi->no_scale_fact_reference_TSV;
	int x_q = 0;
	//osi->no_qTcut_reference_TSV;
	if (ycontribution->active_qTcut && ygeneric->no_qTcut_runtime_estimate){
	  x_q = ygeneric->no_qTcut_runtime_estimate;
	}
	logger << LOG_DEBUG << "x_m = " << x_m << endl;
	logger << LOG_DEBUG << "x_q = " << x_q << endl;
	logger << LOG_DEBUG << "x_s = " << x_s << endl;
	logger << LOG_DEBUG << "x_r = " << x_r << endl;
	logger << LOG_DEBUG << "x_f = " << x_f << endl;

	result_run[i_z] = result_run_TSV[i_z][x_m][x_q][x_s][x_r][x_f];
	deviation_run[i_z] = deviation_run_TSV[i_z][x_m][x_q][x_s][x_r][x_f];
*/

	logger << LOG_DEBUG_VERBOSE << "TIME RESULT FROM TSV  [" << setw(3) << i_z << "] = " << setw(23) << setprecision(15) << result_run[i_z] << " +- " << setw(23) << setprecision(15) << deviation_run[i_z] << endl;

	//	logger << LOG_DEBUG << "default_result = " << default_result << endl;
	logger << LOG_DEBUG << "default_deviation = " << default_deviation << endl;
	logger << LOG_DEBUG << "deviation_run[" << i_z << "] = " << deviation_run[i_z] << endl;

	if (error2_time_run[i_z] != 0. && default_deviation != 0.){error2_time_run[i_z] = error2_time_run[i_z] * pow(deviation_run[i_z] / default_deviation, 2);}
     }

      // later: else if ...
      else if (osi->switch_CV && ycontribution->active_qTcut && ygeneric->no_qTcut_runtime_estimate){

	///	if (ycontribution->active_qTcut && ygeneric->no_qTcut_runtime_estimate){
	int temp_position = 3;
	if (osi->switch_CV){
	  temp_position += osi->n_scales_CV * 2 * ygeneric->no_qTcut_runtime_estimate + ((osi->n_scales_CV - 1) / 2) * 2;
	}
	logger << LOG_DEBUG << "temp_position = " << temp_position << endl;
	logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;
	//	double default_result = atof(readin[1].c_str());
	///	double default_deviation = atof(readin[2].c_str());

	result_run[i_z] = atof(readin[temp_position].c_str());
	deviation_run[i_z] = atof(readin[temp_position + 1].c_str());

	logger << LOG_INFO << "TIME RESULT FROM CV   [" << setw(3) << i_z << "] = " << setw(23) << setprecision(15) << result_run[i_z] << " +- " << setw(23) << setprecision(15) << deviation_run[i_z] << endl;

	//	logger << LOG_DEBUG << "default_result = " << default_result << endl;
	logger << LOG_DEBUG << "default_deviation = " << default_deviation << endl;
	logger << LOG_DEBUG << "deviation_run[" << i_z << "] = " << deviation_run[i_z] << endl;


	if (error2_time_run[i_z] != 0. && default_deviation != 0.){error2_time_run[i_z] = error2_time_run[i_z] * pow(deviation_run[i_z] / default_deviation, 2);}

      }
      else if (osi->switch_CV){
	//	double default_result = atof(readin[1].c_str());
	///	double default_deviation = atof(readin[2].c_str());
	int temp_position = 3 + ((osi->n_scales_CV - 1) / 2) * 2;
	result_run[i_z] = atof(readin[temp_position].c_str());
	deviation_run[i_z] = atof(readin[temp_position + 1].c_str());
	if (error2_time_run[i_z] != 0. && default_deviation != 0.){error2_time_run[i_z] = error2_time_run[i_z] * pow(deviation_run[i_z] / default_deviation, 2);}
      }
      else {
	logger << LOG_ERROR << "Should not happen! Run-time determination is not correctly selected." << endl;
      }
    }
    else {
      if (readin.size() != 0){logger << LOG_ERROR << result_file << "   error" << endl;}
      else {logger << LOG_DEBUG << result_file << "   empty" << endl;}
      used_n_event_run[i_z] = 0;
      N[i_z] = 0;
      result_run[i_z] = 0.;
      deviation_run[i_z] = 0.;
      error2_time_run[i_z] = 0.;
      time_per_event_run[i_z] = 0.;
      used_runtime_run[i_z] = 0.;
    }
  }


  //  logger << LOG_INFO << "Averaging of convergence estimators" << endl;
  int counter_e2_t = 0;
  int counter_t_n = 0;
  error2_time = 0.;
  time_per_event = 0.;
  used_runtime = 0.;
  used_n_event = 0;

  int this_no_qTcut_runtime_estimate = 0;
  if (ycontribution->active_qTcut){this_no_qTcut_runtime_estimate = ygeneric->no_qTcut_runtime_estimate;}

  // Simply average over single runs that are not excluded from combination (error2_time, used_n_event):
  for (int i_z = 0; i_z < n_seed; i_z++){
    //    logger << LOG_INFO << remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z] << " remove_run_qTcut[" << this_no_qTcut_runtime_estimate << "][" << i_z << "] = " << remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z] << endl;
    if (!remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z]){
      if (error2_time_run[i_z] != 0.){
	error2_time += error2_time_run[i_z];
	counter_e2_t++;
      }
      if (time_per_event_run[i_z] != 0.){
	time_per_event += time_per_event_run[i_z];
	counter_t_n++;
      }
    }
    else {
      logger << LOG_INFO << ycontribution->type_contribution << "   " << setw(15) << name << "   run " << i_z << "   removed in time calculation:   remove_run_qTcut[" << this_no_qTcut_runtime_estimate << "][" << i_z << "] = " << remove_run_qTcut[this_no_qTcut_runtime_estimate][i_z] << endl;
    }
    // still count the runtimes and event if run is removed.
    used_runtime += used_runtime_run[i_z];
    used_n_event += used_n_event_run[i_z];
    //      cout << "used_runtime_run[" << i_p << "][" << i_z << "] = " << used_runtime_run[i_z] << endl;
  }
  if (counter_e2_t > 0){error2_time = sqrt(error2_time / counter_e2_t);}
  if (counter_t_n > 0){time_per_event = time_per_event / counter_t_n;}


  logger << LOG_INFO << setw(15) << name << " (av) : " << "time(s) = " << setw(6) << used_runtime << "   n_event = " << setw(15) << used_n_event << "   time_per_event = " << setw(15) << setprecision(8) << time_per_event << "   sqrt(error2/time) = " << setw(15) << setprecision(8) << error2_time << endl;

  // error2_time is actually sqrt(error2_time) !!!

  logger << LOG_DEBUG << "Averaging of convergence estimators done!" << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


