#include "header.hpp"

void summary_order::collect_result_TSV(vector<string> & subgroup){
  Logger logger("summary_order::collect_result_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  result_TSV.resize(osi->n_moments, vector<vector<vector<vector<double> > > > (subgroup.size(), vector<vector<vector<double> > > (osi->n_extended_set_TSV)));
  deviation_TSV.resize(osi->n_moments, vector<vector<vector<vector<double> > > > (subgroup.size(), vector<vector<vector<double> > > (osi->n_extended_set_TSV)));

  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	result_TSV[i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	deviation_TSV[i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
      }
    }
  }

  result_qTcut_TSV.resize(osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (subgroup.size(), vector<vector<vector<vector<double> > > > (output_n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV))));
  deviation_qTcut_TSV.resize(osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (subgroup.size(), vector<vector<vector<vector<double> > > > (output_n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV))));

  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  result_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  deviation_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	}
      }
    }
  }


  logger << LOG_DEBUG_VERBOSE << "additive/multiplicative decision: combination.size() = " << combination.size() << endl;

  if (combination.size() == 1){
    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent and qTcut-independent/extrapolated part) started" << endl;

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	      double dev = 0.;
	      for (int i_l = 0; i_l < xlist.size(); i_l++){
		result_TSV[i_m][i_g][i_s][i_r][i_f] += xlist[i_l]->result_TSV[i_m][i_g][i_s][i_r][i_f];
		dev += pow(xlist[i_l]->deviation_TSV[i_m][i_g][i_s][i_r][i_f], 2);
	      }
	      deviation_TSV[i_m][i_g][i_s][i_r][i_f] = sqrt(dev);
	      stringstream temp_res;
	      temp_res << setw(23) << setprecision(15) << result_TSV[i_m][i_g][i_s][i_r][i_f];
	      stringstream temp_dev;
	      temp_dev << setw(23) << setprecision(15) << deviation_TSV[i_m][i_g][i_s][i_r][i_f];
	      logger << LOG_DEBUG_VERBOSE << "result_TSV[" << i_m << "][" << i_g << "][" << i_s << "][" << i_r << "][" << i_f << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	    }
	  }
	}

	for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	    //	    result_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    //	    deviation_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
		double dev2 = 0.;
		for (int i_l = 0; i_l < xlist.size(); i_l++){
		  int y_q = 0;
		  if (xlist[i_l]->xcontribution[0]->active_qTcut){y_q = i_q;}

		  result_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f] += xlist[i_l]->xcontribution[0]->result_TSV[i_m][i_g][y_q][i_s][i_r][i_f];
		  dev2 += pow(xlist[i_l]->xcontribution[0]->deviation_TSV[i_m][i_g][y_q][i_s][i_r][i_f], 2);
		}
		deviation_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f] = sqrt(dev2);
		stringstream temp_res;
		temp_res << setw(23) << setprecision(15) << result_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f];
		stringstream temp_dev;
		temp_dev << setw(23) << setprecision(15) << deviation_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f];
		logger << LOG_DEBUG_VERBOSE << "result_qTcut_TSV[" << i_m << "][" << i_g << "][" << i_q << "][" << i_s << "][" << i_r << "][" << i_f << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	      }
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent and qTcut-independent/extrapolated part) finished" << endl;
  }
  else if (combination.size() > 1){
    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent and qTcut-independent/extrapolated part) started" << endl;

    // qTcut-independent / extrapolated results: declaration and initialization (resize) of factors
    vector<vector<vector<vector<vector<vector<double> > > > > > factor_order_result_TSV(combination.size(), vector<vector<vector<vector<vector<double> > > > > (osi->n_moments, vector<vector<vector<vector<double> > > > (subgroup.size(), vector<vector<vector<double> > > (osi->n_extended_set_TSV))));
    vector<vector<vector<vector<vector<vector<double> > > > > > factor_order_deviation_TSV(combination.size(), vector<vector<vector<vector<vector<double> > > > > (osi->n_moments, vector<vector<vector<vector<double> > > > (subgroup.size(), vector<vector<vector<double> > > (osi->n_extended_set_TSV))));

    /*
    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  result_TSV[i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  deviation_TSV[i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	}
      }
    }
    */
    for (int i_c = 0; i_c < combination.size(); i_c++){
      for (int i_g = 0; i_g < subgroup.size(); i_g++){
	for (int i_m = 0; i_m < osi->n_moments; i_m++){
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	    factor_order_result_TSV[i_c][i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    factor_order_deviation_TSV[i_c][i_m][i_g][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	  }
	}
      }
    }

    // qTcut-independent / extrapolated results: calculation of factors

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	      for (int i_c = 0; i_c < combination.size(); i_c++){
		double result = 0.;
		double dev2 = 0.;
		for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		  int i_l = combination[i_c][j_c];
		  result += xlist[i_l]->result_TSV[i_m][i_g][i_s][i_r][i_f];
		  dev2 += pow(xlist[i_l]->deviation_TSV[i_m][i_g][i_s][i_r][i_f], 2);
		}
		factor_order_result_TSV[i_c][i_m][i_g][i_s][i_r][i_f] = result;
		factor_order_deviation_TSV[i_c][i_m][i_g][i_s][i_r][i_f] = sqrt(dev2);
	      }
	    }
	  }
	}
      }
    }

    // qTcut-independent / extrapolated results: calculation of final results

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
  	  for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	      double finalized_result = 0.;
	      double finalized_deviation2 = 0.;
	      double index_LO = 0;
	      double temp_result = 0.;
	      double temp_deviation2 = 0.;
	      for (int i_c = 0; i_c < combination.size(); i_c++){
		if (combination_type[i_c] == 0){
		  if (i_c != 0){
		    // + combination (combination_type[i_c] == 0) may start a new multiplicative part
		    finalized_result += temp_result;
		    temp_result = 0.;
		    finalized_deviation2 += temp_deviation2;
		    temp_deviation2 = 0.;
		  }
		  index_LO = i_c;
		  temp_result = factor_order_result_TSV[i_c][i_m][i_g][i_s][i_r][i_f];
		  // Why += ??? (copied from distribution routines)
		  temp_deviation2 += factor_order_deviation_TSV[i_c][i_m][i_g][i_s][i_r][i_f];
		}
		else if (combination_type[i_c] == 1){
		  temp_result *= (1. + factor_order_result_TSV[i_c][i_m][i_g][i_s][i_r][i_f] / factor_order_result_TSV[index_LO][i_m][i_g][i_s][i_r][i_f]);
		  // simplification in case of multiplicative combination:
		  temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_m][i_g][i_s][i_r][i_f], 2);
		}
		else if (combination_type[i_c] == 2){
		  // refinemant might be needed for "subtractive combination"
		  temp_result -= factor_order_result_TSV[i_c][i_m][i_g][i_s][i_r][i_f];
		  temp_deviation2 += pow(factor_order_deviation_TSV[i_c][i_m][i_g][i_s][i_r][i_f], 2);
		}
	      }
result_TSV[i_m][i_g][i_s][i_r][i_f] = finalized_result + temp_result;
	      deviation_TSV[i_m][i_g][i_s][i_r][i_f] = sqrt(finalized_deviation2 + temp_deviation2);

	    }
	  }
	}
      }
    }


    // qTcut dependent results: declaration and initialization (resize) of factors

    vector<vector<vector<vector<vector<vector<vector<double> > > > > > > factor_order_result_qTcut_TSV(combination.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (subgroup.size(), vector<vector<vector<vector<double> > > > (output_n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV)))));
    vector<vector<vector<vector<vector<vector<vector<double> > > > > > > factor_order_deviation_qTcut_TSV(combination.size(), vector<vector<vector<vector<vector<vector<double> > > > > > (osi->n_moments, vector<vector<vector<vector<vector<double> > > > > (subgroup.size(), vector<vector<vector<vector<double> > > > (output_n_qTcut, vector<vector<vector<double> > > (osi->n_extended_set_TSV)))));

    /*
    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	    result_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s]));
	    deviation_qTcut_TSV[i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s]));
	  }
	}
      }
    }
    */

    for (int i_c = 0; i_c < combination.size(); i_c++){
      for (int i_g = 0; i_g < subgroup.size(); i_g++){
	for (int i_m = 0; i_m < osi->n_moments; i_m++){
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	      factor_order_result_qTcut_TSV[i_c][i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	      factor_order_deviation_qTcut_TSV[i_c][i_m][i_g][i_q][i_s].resize(osi->n_scale_ren_TSV[i_s], vector<double> (osi->n_scale_fact_TSV[i_s], 0.));
	    }
	  }
	}
      }
    }

    // qTcut dependent results: calculation of factors

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	  if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	  if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	  for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
		for (int i_c = 0; i_c < combination.size(); i_c++){
		  double result = 0.;
		  double dev2 = 0.;
		  for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		    int i_l = combination[i_c][j_c];
		    logger << LOG_DEBUG_VERBOSE << "list_result_TSV[" << i_l << "][" << i_m << "][" << i_g << "][" << i_q << "][" << i_s << "][" << i_r << "][" << i_f << "] = " << xlist[i_l]->xcontribution[0]->result_TSV[i_m][i_g][i_q][i_s][i_r][i_f] << endl;

		    /*
		    // something similar might be needed here if not al qTcut values are involved in result !!!
		    int y_q = 0;
		    if (xlist[i_l]->xcontribution[0]->active_qTcut){y_q = i_q;}
		    */
		    result += xlist[i_l]->xcontribution[0]->result_TSV[i_m][i_g][i_q][i_s][i_r][i_f];
		    dev2 += pow(xlist[i_l]->xcontribution[0]->deviation_TSV[i_m][i_g][i_q][i_s][i_r][i_f], 2);
		  }
		  factor_order_result_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f] = result;
		  factor_order_deviation_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f] = sqrt(dev2);
		}
	      }
	    }
	  }
	}
      }
    }


    // qTcut-dependent results: calculation of final results

    for (int i_g = 0; i_g < subgroup.size(); i_g++){
      for (int i_m = 0; i_m < osi->n_moments; i_m++){
	for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	  for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	    if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	    if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	    for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
		double finalized_result = 0.;
		double finalized_deviation2 = 0.;
		double index_LO = 0;
		double temp_result = 0.;
		double temp_deviation2 = 0.;

		for (int i_c = 0; i_c < combination.size(); i_c++){
		  if (combination_type[i_c] == 0){
		    if (i_c != 0){
		      // + combination (combination_type[i_c] == 0) may start a new multiplicative part
		      finalized_result += temp_result;
		      temp_result = 0.;
		      finalized_deviation2 += temp_deviation2;
		      temp_deviation2 = 0.;
		    }
		    index_LO = i_c;
		    temp_result = factor_order_result_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f];
		    // Why += ??? (copied from distribution routines)
		    temp_deviation2 += factor_order_deviation_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f];
		  }
		  else if (combination_type[i_c] == 1){
		    temp_result *= (1. + factor_order_result_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f] / factor_order_result_qTcut_TSV[index_LO][i_m][i_g][i_q][i_s][i_r][i_f]);
		    // simplification in case of multiplicative combination (correct error propagation needs to be implemented...)
		    temp_deviation2 += pow(factor_order_deviation_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f], 2);
		  }
		  else if (combination_type[i_c] == 2){
		    // refinement might be needed for "subtractive combination"
		    temp_result -= factor_order_result_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f];
		    temp_deviation2 += pow(factor_order_deviation_qTcut_TSV[i_c][i_m][i_g][i_q][i_s][i_r][i_f], 2);
		  }
		}
		result_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f] = finalized_result + temp_result;
		deviation_qTcut_TSV[i_m][i_g][i_q][i_s][i_r][i_f] = sqrt(finalized_deviation2 + temp_deviation2);

	      }
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent and qTcut-independent/extrapolated part) finished" << endl;
  }

  vector<vector<vector<vector<vector<double> > > > > moment_result_TSV = result_TSV;
  vector<vector<vector<vector<vector<double> > > > > moment_deviation_TSV = deviation_TSV;

  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 1; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	    // normalization: divede by cross section to get moment:
	    result_TSV[i_m][i_g][i_s][i_r][i_f] = result_TSV[i_m][i_g][i_s][i_r][i_f] / result_TSV[0][i_g][i_s][i_r][i_f];
	    deviation_TSV[i_m][i_g][i_s][i_r][i_f] = deviation_TSV[i_m][i_g][i_s][i_r][i_f] / result_TSV[0][i_g][i_s][i_r][i_f];
	  }
	}
      }
    }
  }

  vector<vector<vector<vector<vector<double> > > > > save_result_TSV = result_TSV;
  vector<vector<vector<vector<vector<double> > > > > save_deviation_TSV = deviation_TSV;

  if (ygeneric->switch_output_result > 0){output_moment_TSV("moment");}

  if (ygeneric->switch_output_result > 0){output_moment_TSV("ln_muR");}

  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 1; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	    // normalization: divede by cross section to get moment:
	    result_TSV[i_m][i_g][i_s][i_r][i_f] = exp(result_TSV[i_m][i_g][i_s][i_r][i_f]);
	    deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	  }
	}
      }
    }
  }

  if (ygeneric->switch_output_result > 0){output_moment_TSV("exp_ln_muR");}
  result_TSV = save_result_TSV;

  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 1; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	    if (i_m == 1){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] = result_TSV[i_m][i_g][i_s][i_r][i_f] - result_TSV[1][i_g][i_s][i_r][i_f];
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	    if (i_m == 2){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] =
		result_TSV[i_m][i_g][i_s][i_r][i_f]
		- 2 * result_TSV[i_m - 1][i_g][i_s][i_r][i_f] * result_TSV[1][i_g][i_s][i_r][i_f]
		+ pow(result_TSV[1][i_g][i_s][i_r][i_f], 2);
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	    if (i_m == 3){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] =
		result_TSV[i_m][i_g][i_s][i_r][i_f]
		- 3 * result_TSV[i_m - 1][i_g][i_s][i_r][i_f] * result_TSV[1][i_g][i_s][i_r][i_f]
		+ 3 * result_TSV[i_m - 2][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 2)
		- pow(result_TSV[1][i_g][i_s][i_r][i_f], 3);
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	    if (i_m == 4){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] =
		result_TSV[i_m][i_g][i_s][i_r][i_f]
		- 4 * result_TSV[i_m - 1][i_g][i_s][i_r][i_f] * result_TSV[1][i_g][i_s][i_r][i_f]
		+ 6 * result_TSV[i_m - 2][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 2)
		- 4 * result_TSV[i_m - 3][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 3)
		+ pow(result_TSV[1][i_g][i_s][i_r][i_f], 4);
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	    if (i_m == 5){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] =
		result_TSV[i_m][i_g][i_s][i_r][i_f]
		- 5 * result_TSV[i_m - 1][i_g][i_s][i_r][i_f] * result_TSV[1][i_g][i_s][i_r][i_f]
		+ 10 * result_TSV[i_m - 2][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 2)
		- 10 * result_TSV[i_m - 3][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 3)
		+ 5 * result_TSV[i_m - 4][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 4)
		- pow(result_TSV[1][i_g][i_s][i_r][i_f], 5);
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	    if (i_m == 6){
	      moment_result_TSV[i_m][i_g][i_s][i_r][i_f] =
		result_TSV[i_m][i_g][i_s][i_r][i_f]
		- 6 * result_TSV[i_m - 1][i_g][i_s][i_r][i_f] * result_TSV[1][i_g][i_s][i_r][i_f]
		+ 15 * result_TSV[i_m - 2][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 2)
		- 20 * result_TSV[i_m - 3][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 3)
		+ 15 * result_TSV[i_m - 4][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 4)
		- 6 * result_TSV[i_m - 5][i_g][i_s][i_r][i_f] * pow(result_TSV[1][i_g][i_s][i_r][i_f], 5)
		+ pow(result_TSV[1][i_g][i_s][i_r][i_f], 6);
	      moment_deviation_TSV[i_m][i_g][i_s][i_r][i_f] = 0.;
	    }
	  }
	}
      }
    }
  }
  result_TSV = moment_result_TSV;
  if (ygeneric->switch_output_result > 0){output_moment_TSV("f_N");}
  result_TSV = save_result_TSV;


  for (int i_g = 0; i_g < subgroup.size(); i_g++){
    for (int i_m = 1; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_extended_set_TSV; i_s++){
	if (!ygeneric->switch_output_scaleset[i_s]){continue;}
	if (i_m > 0 && !osi->switch_moment_TSV[i_s]){continue;}
	for (int i_r = 0; i_r < osi->n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < osi->n_scale_fact_TSV[i_s]; i_f++){
	    moment_result_TSV[i_m][i_g][i_s][i_r][i_f] = moment_result_TSV[i_m][i_g][i_s][i_r][i_f] / pow(log(result_TSV[1][i_g][i_s][i_r][i_f]) - log(0.218), i_m);
	  }
	}
      }
    }
  }

  result_TSV = moment_result_TSV;
  if (ygeneric->switch_output_result > 0){output_moment_TSV("g_N");}
  result_TSV = save_result_TSV;


  if (ygeneric->switch_output_order){
    if (ygeneric->switch_output_overview > 0){output_result_overview_TSV();}
    if (ygeneric->switch_output_result > 0){output_result_TSV();}
    if (ygeneric->switch_output_plot > 0){output_result_plot_TSV();}
    if (ygeneric->switch_output_plot > 0){output_result_plot_qTcut_TSV();}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void summary_order::collect_result_CV(vector<string> & subgroup){
  Logger logger("summary_order::collect_result_CV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // extrapolated result
  result_CV.resize(osi->n_moments, vector<vector<double> > (subgroup.size(), vector<double> (osi->n_scales_CV, 0.)));
  deviation_CV.resize(osi->n_moments, vector<vector<double> > (subgroup.size(), vector<double> (osi->n_scales_CV, 0.)));
  // qTcut result
  result_qTcut_CV.resize(osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));
  deviation_qTcut_CV.resize(osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));

  if (combination.size() == 1){
    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent and qTcut-independent/extrapolated part) started" << endl;

    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_g = 0; i_g < subgroup.size(); i_g++){
	  double dev = 0.;
	  for (int i_l = 0; i_l < xlist.size(); i_l++){
	    result_CV[i_m][i_g][i_s] += xlist[i_l]->result_CV[i_m][i_g][i_s];
	    dev += pow(xlist[i_l]->deviation_CV[i_m][i_g][i_s], 2);
	  }
	  deviation_CV[i_m][i_g][i_s] = sqrt(dev);
	  stringstream temp_res;
	  temp_res << setw(23) << setprecision(15) << result_CV[i_m][i_g][i_s];
	  stringstream temp_dev;
	  temp_dev << setw(23) << setprecision(15) << deviation_CV[i_m][i_g][i_s];
	  logger << LOG_DEBUG_VERBOSE << "result_CV[" << i_m << "][" << i_g << "][" << i_s << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	}
      }
    }

    // qTcut result
    /*
    result_qTcut_CV.resize(osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));
    deviation_qTcut_CV.resize(osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.))));
    */
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	  for (int i_g = 0; i_g < subgroup.size(); i_g++){
	    double dev = 0.;
	    for (int i_l = 0; i_l < xlist.size(); i_l++){
	      result_qTcut_CV[i_m][i_g][i_q][i_s] += xlist[i_l]->xcontribution[0]->result_CV[i_m][i_g][i_q][i_s];
	      dev += pow(xlist[i_l]->xcontribution[0]->deviation_CV[i_m][i_g][i_q][i_s], 2);
	    }
	    deviation_qTcut_CV[i_m][i_g][i_q][i_s] = sqrt(dev);
	    stringstream temp_res;
	    temp_res << setw(23) << setprecision(15) << result_qTcut_CV[i_m][i_g][i_q][i_s];
	    stringstream temp_dev;
	    temp_dev << setw(23) << setprecision(15) << deviation_qTcut_CV[i_m][i_g][i_q][i_s];
	    logger << LOG_DEBUG_VERBOSE << "result_qTcut_CV[" << i_m << "][" << i_g << "][" << i_q << "][" << i_s << "] = " << temp_res.str() << " +- " << temp_dev.str() << endl;
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "additive combination (qTcut-dependent and qTcut-independent/extrapolated part) finished" << endl;
  }
  else if (combination.size() > 1){
    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent and qTcut-independent/extrapolated part) started" << endl;

    // extrapolated result
    vector<vector<vector<vector<double> > > > factor_order_result_CV(combination.size(), vector<vector<vector<double> > > (osi->n_moments, vector<vector<double> > (subgroup.size(), vector<double> (osi->n_scales_CV, 0.))));
    vector<vector<vector<vector<double> > > > factor_order_deviation_CV(combination.size(), vector<vector<vector<double> > > (osi->n_moments, vector<vector<double> > (subgroup.size(), vector<double> (osi->n_scales_CV, 0.))));

    // qTcut-independent / extrapolated results: calculation of factors
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	  for (int i_g = 0; i_g < subgroup.size(); i_g++){
	    for (int i_c = 0; i_c < combination.size(); i_c++){
	      double result = 0.;
	      double dev2 = 0.;
	      for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		int i_l = combination[i_c][j_c];
		result += xlist[i_l]->result_CV[i_m][i_g][i_s];
		dev2 += pow(xlist[i_l]->deviation_CV[i_m][i_g][i_s], 2);
	      }
	      factor_order_result_CV[i_c][i_m][i_g][i_s] = result;
	      factor_order_deviation_CV[i_c][i_m][i_g][i_s] = sqrt(dev2);
	    }
	  }
	}
      }
    }

    // qTcut-independent / extrapolated results: calculation of final results
    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	  for (int i_g = 0; i_g < subgroup.size(); i_g++){
	    double finalized_result = 0.;
	    double finalized_deviation2 = 0.;
	    double index_LO = 0;
	    double temp_result = 0.;
	    double temp_deviation2 = 0.;
	    for (int i_c = 0; i_c < combination.size(); i_c++){
	      if (combination_type[i_c] == 0){
		if (i_c != 0){
		  // + combination (combination_type[i_c] == 0) may start a new multiplicative part
		  finalized_result += temp_result;
		  temp_result = 0.;
		  finalized_deviation2 += temp_deviation2;
		  temp_deviation2 = 0.;
		}
		index_LO = i_c;
		temp_result = factor_order_result_CV[i_c][i_m][i_g][i_s];
		// Why += ??? (copied from distribution routines)
		temp_deviation2 += factor_order_deviation_CV[i_c][i_m][i_g][i_s];
	      }
	      else if (combination_type[i_c] == 1){
		temp_result *= (1. + factor_order_result_CV[i_c][i_m][i_g][i_s] / factor_order_result_CV[index_LO][i_m][i_g][i_s]);
		// simplification in case of multiplicative combination:
		temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_m][i_g][i_s], 2);
	      }
	      else if (combination_type[i_c] == 2){
		// refinement might be needed for "subtractive combination"
		temp_result -= factor_order_result_CV[i_c][i_m][i_g][i_s];
		temp_deviation2 += pow(factor_order_deviation_CV[i_c][i_m][i_g][i_s], 2);
	      }
	    }
	    result_CV[i_m][i_g][i_s] = finalized_result + temp_result;
	    deviation_CV[i_m][i_g][i_s] = sqrt(finalized_deviation2 + temp_deviation2);
	  }
	}
      }
    }

    // qTcut dependent results: declaration and initialization (resize) of factors

    vector<vector<vector<vector<vector<double> > > > > factor_order_result_qTcut_CV(combination.size(), vector<vector<vector<vector<double> > > > (osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.)))));
    vector<vector<vector<vector<vector<double> > > > > factor_order_deviation_qTcut_CV(combination.size(), vector<vector<vector<vector<double> > > > (osi->n_moments, vector<vector<vector<double> > > (subgroup.size(), vector<vector<double> > (osi->n_qTcut, vector<double> (osi->n_scales_CV, 0.)))));

    // qTcut-dependent results: calculation of factors

    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	  for (int i_g = 0; i_g < subgroup.size(); i_g++){
	    for (int i_c = 0; i_c < combination.size(); i_c++){
	      double result = 0.;
	      double dev2 = 0.;
	      for (int j_c = 0; j_c < combination[i_c].size(); j_c++){
		int i_l = combination[i_c][j_c];
		logger << LOG_DEBUG_VERBOSE << "list_result_CV[" << i_l << "][" << i_m << "][" << i_g << "][" << i_q << "][" << i_s << "] = " << xlist[i_l]->xcontribution[0]->result_CV[i_m][i_g][i_q][i_s] << endl;
		/*
		// something similar might be needed here if not all qTcut values are involved in result !!!
		int y_q = 0;
		if (xlist[i_l]->xcontribution[0]->active_qTcut){y_q = i_q;}
		*/
		result += xlist[i_l]->xcontribution[0]->result_CV[i_m][i_g][i_q][i_s];
		dev2 += pow(xlist[i_l]->xcontribution[0]->deviation_CV[i_m][i_g][i_q][i_s], 2);
	      }
	      factor_order_result_qTcut_CV[i_c][i_m][i_g][i_q][i_s] = result;
	      factor_order_deviation_qTcut_CV[i_c][i_m][i_g][i_q][i_s] = sqrt(dev2);
	    }
	  }
	}
      }
    }

    // qTcut-dependent results: calculation of final results

    for (int i_m = 0; i_m < osi->n_moments; i_m++){
      for (int i_s = 0; i_s < osi->n_scales_CV; i_s++){
	for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	  for (int i_g = 0; i_g < subgroup.size(); i_g++){
	    for (int i_c = 0; i_c < combination.size(); i_c++){
	      double finalized_result = 0.;
	      double finalized_deviation2 = 0.;
	      double index_LO = 0;
	      double temp_result = 0.;
	      double temp_deviation2 = 0.;

	      for (int i_c = 0; i_c < combination.size(); i_c++){
		if (combination_type[i_c] == 0){
		  if (i_c != 0){
		    // + combination (combination_type[i_c] == 0) may start a new multiplicative part
		    finalized_result += temp_result;
		    temp_result = 0.;
		    finalized_deviation2 += temp_deviation2;
		    temp_deviation2 = 0.;
		  }
		  index_LO = i_c;
		  temp_result = factor_order_result_qTcut_CV[i_c][i_m][i_g][i_q][i_s];
		  // Why += ??? (copied from distribution routines)
		  temp_deviation2 += factor_order_deviation_qTcut_CV[i_c][i_m][i_g][i_q][i_s];
		}
		else if (combination_type[i_c] == 1){
		  temp_result *= (1. + factor_order_result_qTcut_CV[i_c][i_m][i_g][i_q][i_s] / factor_order_result_qTcut_CV[index_LO][i_m][i_g][i_q][i_s]);
		  // simplification in case of multiplicative combination (correct error propagation needs to be implemented...)
		  temp_deviation2 += pow(factor_order_deviation_qTcut_CV[i_c][i_m][i_g][i_q][i_s], 2);
		}
		else if (combination_type[i_c] == 2){
		  // refinement might be needed for "subtractive combination"
		  temp_result -= factor_order_result_qTcut_CV[i_c][i_m][i_g][i_q][i_s];
		  temp_deviation2 += pow(factor_order_deviation_qTcut_CV[i_c][i_m][i_g][i_q][i_s], 2);
		}
	      }
	      result_qTcut_CV[i_m][i_g][i_q][i_s] = finalized_result + temp_result;
	      deviation_qTcut_CV[i_m][i_g][i_q][i_s] = sqrt(finalized_deviation2 + temp_deviation2);
	    }
	  }
	}
      }
    }

    logger << LOG_DEBUG_VERBOSE << "multiplicative combination (qTcut-dependent and qTcut-independent/extrapolated part) finished" << endl;
  }

  if (ygeneric->switch_output_order){
    if (ygeneric->switch_output_overview > 0){output_result_overview_CV();}
    if (ygeneric->switch_output_result > 0){output_result_CV();}
    if (ygeneric->switch_output_plot > 0){output_result_plot_CV();}
    if (ygeneric->switch_output_plot > 0){output_result_plot_qTcut_CV();}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

