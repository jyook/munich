#include "header.hpp"

summary_order::summary_order(){}

summary_order::summary_order(string _resultdirectory, summary_generic * _generic){
  Logger logger("summary_order::summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  initialization(_resultdirectory, _generic);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void summary_order::initialization(string _resultdirectory, summary_generic * _generic){
  Logger logger("summary_order::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ygeneric = _generic;
  osi = ygeneric->osi;

  resultdirectory = _resultdirectory;
  contribution_file = vector<string> ();
  combination.resize(1);
  combination_type.resize(1, 0);
  accuracy_relative = 0.;
  accuracy_normalization = "";

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



