#include "header.hpp"

void model_set::initialization(inputparameter_set * isi, user_defined * _user){
  Logger logger("model_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  user = _user;

  present_type_perturbative_order = isi->present_type_perturbative_order;
  n_perturbative_order = isi->collection_type_perturbative_order.size();
  contribution_LHAPDFname = isi->contribution_LHAPDFname;
  contribution_LHAPDFsubset = isi->contribution_LHAPDFsubset;

  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


  void model_set::initialization_set_default(){
  static  Logger logger("model_set::initialization_set_default");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  /*
  empty_double = -1.e100;
  empty_double_complex = double_complex(-1.e100, -1.e100);
  empty_int = -32000;
  empty_string = "empty";
  */

  V_du = empty_double;
  V_su = empty_double;
  V_dc = empty_double;
  V_sc = empty_double;
  V_bu = empty_double;
  V_bc = empty_double;
  V_dt = empty_double;
  V_st = empty_double;
  V_bt = empty_double;

  Q_u = empty_double;
  Q_d = empty_double;
  Q_v = empty_double;
  Q_l = empty_double;
  Iw_u = empty_double;
  Iw_d = empty_double;
  Iw_n = empty_double;
  Iw_l = empty_double;

  ew_scheme = empty_int;
  use_cms = empty_int;
  use_adapted_ew_coupling = empty_int;

  G_F = empty_double;
  theta_c = empty_double;

  M_W = empty_double;
  M_Z = empty_double;
  M_t = empty_double;
  M_b = empty_double;
  M_c = empty_double;
  M_s = empty_double;
  M_u = empty_double;
  M_d = empty_double;
  M_e = empty_double;
  M_mu = empty_double;
  M_tau = empty_double;
  M_ve = empty_double;
  M_vm = empty_double;
  M_vt = empty_double;
  M_H = empty_double;

  reg_Gamma_Z = empty_double;
  reg_Gamma_W = empty_double;
  reg_Gamma_H = empty_double;
  reg_Gamma_t = empty_double;

  map_Gamma_Z = empty_double;
  map_Gamma_W = empty_double;
  map_Gamma_H = empty_double;
  map_Gamma_t = empty_double;

  alpha_e = empty_double;
  e = empty_double;

  alpha_e_0 = empty_double;
  alpha_e_MZ = empty_double;
  alpha_e_Gmu = empty_double;

  cos_w = empty_double;
  cos2_w = empty_double;
  sin2_w = empty_double;
  sin_w = empty_double;

  Cplus_Zuu = empty_double;
  Cplus_Zdd = empty_double;
  Cminus_Zuu = empty_double;
  Cminus_Zdd = empty_double;
  Cplus_Znn = empty_double;
  Cplus_Zee = empty_double;
  Cminus_Znn = empty_double;
  Cminus_Zee = empty_double;
  C_ZWminusWplus = empty_double;

  Cplus_Auu = empty_double;
  Cplus_Add = empty_double;
  Cminus_Auu = empty_double;
  Cminus_Add = empty_double;
  Cplus_Ann = empty_double;
  Cplus_Aee = empty_double;
  Cminus_Ann = empty_double;
  Cminus_Aee = empty_double;
  C_AWminusWplus = empty_double;
  Cminus_W = empty_double;

  ccos_w = empty_double_complex;
  ccos2_w = empty_double_complex;
  csin2_w = empty_double_complex;
  csin_w = empty_double_complex;
  cCplus_Zuu = empty_double_complex;
  cCplus_Zdd = empty_double_complex;
  cCminus_Zuu = empty_double_complex;
  cCminus_Zdd = empty_double_complex;
  cCplus_Znn = empty_double_complex;
  cCplus_Zee = empty_double_complex;
  cCminus_Znn = empty_double_complex;
  cCminus_Zee = empty_double_complex;

  cC_ZWminusWplus = empty_double_complex;

  //  cCplus_Auu = empty_double_complex;
  //  cCplus_Add = empty_double_complex;
  //  cCminus_Auu = empty_double_complex;
  //  cCminus_Add = empty_double_complex;
  //  cCplus_Ann = empty_double_complex;
  //  cCplus_Aee = empty_double_complex;
  //  cCminus_Ann = empty_double_complex;
  //  cCminus_Aee = empty_double_complex;
  //  cC_AWminusWplus = empty_double_complex;

  cCminus_W = empty_double_complex;

  CKM_matrix = empty_string;

  conversion_to_pole_mass = 0;

  vGamma_W_order.resize(n_perturbative_order, empty_int);
  valpha_S_W_order.resize(n_perturbative_order, empty_int);
  valpha_S_W.resize(n_perturbative_order, empty_double);
  valpha_S_W_scale.resize(n_perturbative_order, empty_double);
  vGamma_W.resize(n_perturbative_order, empty_double);
  vGamma_Wud.resize(n_perturbative_order, empty_double);
  vGamma_Wlv.resize(n_perturbative_order, empty_double);
  vBR_Wud.resize(n_perturbative_order, empty_double);
  vBR_Wlv.resize(n_perturbative_order, empty_double);
  //  double alpha_S_W;

  vGamma_Z_order.resize(n_perturbative_order, empty_int);
  valpha_S_Z_order.resize(n_perturbative_order, empty_int);
  valpha_S_Z.resize(n_perturbative_order, empty_double);
  valpha_S_Z_scale.resize(n_perturbative_order, empty_double);
  vGamma_Z.resize(n_perturbative_order, empty_double);
  vGamma_Zuu.resize(n_perturbative_order, empty_double);
  vGamma_Zdd.resize(n_perturbative_order, empty_double);
  vGamma_Zvv.resize(n_perturbative_order, empty_double);
  vGamma_Zll.resize(n_perturbative_order, empty_double);
  vBR_Zuu.resize(n_perturbative_order, empty_double);
  vBR_Zdd.resize(n_perturbative_order, empty_double);
  vBR_Zvv.resize(n_perturbative_order, empty_double);
  vBR_Zll.resize(n_perturbative_order, empty_double);
  //  double alpha_S_Z;

  vGamma_H.resize(n_perturbative_order, empty_double);

  vGamma_t_order.resize(n_perturbative_order, empty_int);
  valpha_S_t_order.resize(n_perturbative_order, empty_int);
  valpha_S_t.resize(n_perturbative_order, empty_double);
  valpha_S_t_scale.resize(n_perturbative_order, empty_double);
  vGamma_t.resize(n_perturbative_order, empty_double);
  vGamma_tWb.resize(n_perturbative_order, empty_double);
  vBR_tWb.resize(n_perturbative_order, empty_double);
  //  double alpha_S_t;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void model_set::initialization_input(inputparameter_set * isi){
  static  Logger logger("model_set::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int QCD_order = -1;

  for (int i_i = 0; i_i < isi->input_model.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_model[" << i_i << "] = " << isi->input_model[i_i] << endl;

    if (isi->input_model[i_i].variable == ""){}

    else if (isi->input_model[i_i].variable == "ew_scheme"){ew_scheme = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "use_cms"){use_cms = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "use_adapted_ew_coupling"){use_adapted_ew_coupling = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "G_F"){
      G_F = atof(isi->input_model[i_i].value.c_str());
      det_EW_coupling.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "sin_w"){
      sin_w = atof(isi->input_model[i_i].value.c_str());
      det_theta_w.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "sin2_w"){
      sin2_w = atof(isi->input_model[i_i].value.c_str());
      det_theta_w.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "cos2_w"){
      cos2_w = atof(isi->input_model[i_i].value.c_str());
      det_theta_w.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "cos_w"){
      cos_w = atof(isi->input_model[i_i].value.c_str());
      det_theta_w.push_back(isi->input_model[i_i].variable);
    }
    // alpha_e  is not allowed as a direct input parameter: use
    // - alpha_e_0 (with ew_scheme = 0),
    // - alpha_e_Gmu (with ew_scheme = 1),
    // - alpha_e_MZ (with ew_scheme = 2).
    //    else if (isi->input_model[i_i].variable == "alpha_e"){alpha_e = atof(isi->input_model[i_i].value.c_str());
    //      det_EW_coupling.push_back(isi->input_model[i_i].variable);}
    //    else if (isi->input_model[i_i].variable == "e"){alpha_e_0 = pow(atof(isi->input_model[i_i].value.c_str()), 2) / (4. * pi);}
    //    else if (isi->input_model[i_i].variable == "e"){e = atof(isi->input_model[i_i].value.c_str());
    //      det_EW_coupling.push_back(isi->input_model[i_i].variable);}
    else if (isi->input_model[i_i].variable == "M_W"){
      M_W = atof(isi->input_model[i_i].value.c_str());
      det_M_gauge.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "M_Z"){
      M_Z = atof(isi->input_model[i_i].value.c_str());
      det_M_gauge.push_back(isi->input_model[i_i].variable);
    }
    else if (isi->input_model[i_i].variable == "alpha_e_0"){alpha_e_0 = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "1/alpha_e_0"){alpha_e_0 = 1. / atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "e_0"){alpha_e_0 = pow(atof(isi->input_model[i_i].value.c_str()), 2) / (4. * pi);}
    else if (isi->input_model[i_i].variable == "alpha_e_MZ"){alpha_e_MZ = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "1/alpha_e_MZ"){alpha_e_MZ = 1. / atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "e_MZ"){alpha_e_MZ = pow(atof(isi->input_model[i_i].value.c_str()), 2) / (4. * pi);}
    else if (isi->input_model[i_i].variable == "alpha_e_Gmu"){alpha_e_Gmu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "1/alpha_e_Gmu"){alpha_e_Gmu = 1. / atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "e_Gmu"){alpha_e_Gmu = pow(atof(isi->input_model[i_i].value.c_str()), 2) / (4. * pi);}

    else if (isi->input_model[i_i].variable == "M_H"){M_H = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_t"){M_t = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_b"){M_b = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_c"){M_c = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_s"){M_s = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_u"){M_u = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_d"){M_d = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_e"){M_e = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_mu"){M_mu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_tau"){M_tau = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_ve"){M_ve = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_vm"){M_vm = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "M_vt"){M_vt = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "reg_Gamma_Z"){reg_Gamma_Z = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "reg_Gamma_W"){reg_Gamma_W = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "reg_Gamma_H"){reg_Gamma_H = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "reg_Gamma_t"){reg_Gamma_t = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "map_Gamma_Z"){map_Gamma_Z = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "map_Gamma_W"){map_Gamma_W = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "map_Gamma_H"){map_Gamma_H = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "map_Gamma_t"){map_Gamma_t = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "CKM_matrix"){CKM_matrix = isi->input_model[i_i].value;}
    else if (isi->input_model[i_i].variable == "theta_c"){theta_c = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "V_du"){V_du = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_su"){V_su = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_bu"){V_bu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_dc"){V_dc = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_sc"){V_sc = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_bc"){V_bc = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_dt"){V_dt = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_st"){V_st = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "V_bt"){V_bt = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Q_u"){Q_u = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Q_d"){Q_d = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Q_v"){Q_v = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Q_l"){Q_l = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Iw_u"){Iw_u = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Iw_d"){Iw_d = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Iw_n"){Iw_n = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Iw_l"){Iw_l = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Cplus_Zuu"){Cplus_Zuu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Zdd"){Cplus_Zdd = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Zuu"){Cminus_Zuu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Zdd"){Cminus_Zdd = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Znn"){Cplus_Znn = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Zee"){Cplus_Zee = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Znn"){Cminus_Znn = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Zee"){Cminus_Zee = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "C_ZWminusWplus"){C_ZWminusWplus = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Cplus_Auu"){Cplus_Auu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Add"){Cplus_Add = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Auu"){Cminus_Auu = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Add"){Cminus_Add = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Ann"){Cplus_Ann = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cplus_Aee"){Cplus_Aee = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Ann"){Cminus_Ann = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_Aee"){Cminus_Aee = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "C_AWminusWplus"){C_AWminusWplus = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Cminus_W"){Cminus_W = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "conversion_to_pole_mass"){conversion_to_pole_mass = atoi(isi->input_model[i_i].value.c_str());}



    else if (isi->input_model[i_i].variable == "contribution"){
      QCD_order = -1;
      if      (isi->input_model[i_i].value == "LO"){QCD_order = 0;}
      else if (isi->input_model[i_i].value == "NLO"){QCD_order = 1;}
      else if (isi->input_model[i_i].value == "NNLO"){QCD_order = 2;}
      else if (isi->input_model[i_i].value == "NNNLO"){QCD_order = 3;}
      else {logger << LOG_FATAL << "QCD_order = " << QCD_order << "  has not been implemented." << endl;}
    }

    else if (isi->input_model[i_i].variable == "Gamma_W"){vGamma_W[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Wlv"){vGamma_Wlv[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Wud"){vGamma_Wud[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Wlv"){vBR_Wlv[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Wud"){vBR_Wud[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_W_order"){vGamma_W_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_W_order"){valpha_S_W_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_W_scale"){valpha_S_W_scale[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_W"){valpha_S_W[QCD_order] = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Gamma_Z"){vGamma_Z[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Zll"){vGamma_Zll[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Zvv"){vGamma_Zvv[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Zdd"){vGamma_Zdd[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Zuu"){vGamma_Zuu[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Zll"){vBR_Zll[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Zvv"){vBR_Zvv[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Zdd"){vBR_Zdd[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_Zuu"){vBR_Zuu[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_Z_order"){vGamma_Z_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_Z_order"){valpha_S_Z_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_Z_scale"){valpha_S_Z_scale[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_Z"){valpha_S_Z[QCD_order] = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Gamma_H"){vGamma_H[QCD_order] = atof(isi->input_model[i_i].value.c_str());}

    else if (isi->input_model[i_i].variable == "Gamma_t"){
      vGamma_t[QCD_order] = atof(isi->input_model[i_i].value.c_str());
      logger << LOG_DEBUG_VERBOSE << "vGamma_t[" << QCD_order << "] = " << vGamma_t[QCD_order] << endl;
    }
    else if (isi->input_model[i_i].variable == "Gamma_tWb"){vGamma_tWb[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "BR_tWb"){vBR_tWb[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "Gamma_t_order"){vGamma_t_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_t_order"){valpha_S_t_order[QCD_order] = atoi(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_t_scale"){valpha_S_t_scale[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
    else if (isi->input_model[i_i].variable == "alpha_S_t"){valpha_S_t[QCD_order] = atof(isi->input_model[i_i].value.c_str());}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void model_set::initialization_after_input(){
  static  Logger logger("model_set::initialization_after_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<string> manipulate_variable;
  vector<string> manipulate_variable_counter;
  vector<string> manipulate_value;

  logger << LOG_DEBUG << "manipulate_variable.size() = " << manipulate_variable.size() << endl;
  logger << LOG_DEBUG << "manipulate_value.size() = " << manipulate_value.size() << endl;


  logger << LOG_DEBUG << "The following SM parameters and masses were not specified and thus set to the following standard values:" << endl;

  if (M_W == empty_double){M_W = 80.399;}
  if (M_Z == empty_double){M_Z = 91.1876;}
  if (M_H == empty_double){M_H = 126.;}
  if (M_t == empty_double){M_t = 172.0E+00;}
  if (M_b == empty_double){M_b = 4.7E+00;}
  if (M_c == empty_double){M_c = 0.;}
  if (M_s == empty_double){M_s = 0.;}
  if (M_u == empty_double){M_u = 0.;}
  if (M_d == empty_double){M_d = 0.;}
  if (M_e == empty_double){M_e = 0.;}
  if (M_mu == empty_double){M_mu = 0.;}
  if (M_tau == empty_double){M_tau = 0.;}
  if (M_ve == empty_double){M_ve = 0.;}
  if (M_vm == empty_double){M_vm = 0.;}
  if (M_vt == empty_double){M_vt = 0.;}

  
  // default values:
  if (ew_scheme == empty_int){ew_scheme = 1;}
  if (use_cms == empty_int){use_cms = 1;}
  if (use_adapted_ew_coupling == empty_int){use_adapted_ew_coupling = -1;}

  // Here one could reactivate the input parameter  alpha_e / 1/alpha_e !

  determine_sintheta_real(det_M_gauge, det_theta_w);
  determine_EWcouplings_real();

  // Calculation of Gmu-scheme couplings and of Gamma_W/Z based on them:
  int save_use_cms = use_cms;
  if (use_cms == 1 || use_cms == 3){use_cms = 2;} // otherwise Gamma_W/Z and alpha_e_Gmu evaluation becomes circular

  double alpha_rescaling_exp = user->double_value[user->double_map["alpha_rescaling_exp"]];

  logger << LOG_INFO << "First call of determine_alpha_e_Gmu:" << endl;
  logger << LOG_INFO << setw(25) << "before first call: G_F" << " = " << G_F << endl;
  logger << LOG_INFO << setw(25) << "before first call: alpha_e_Gmu" << " = " << alpha_e_Gmu << endl;

  determine_alpha_e_Gmu(det_EW_coupling, alpha_rescaling_exp);
  calculate_Gamma_W(present_type_perturbative_order);
  calculate_Gamma_Z(present_type_perturbative_order);
  use_cms = save_use_cms;
  logger << LOG_INFO << setw(25) << "after first call: G_F" << " = " << G_F << endl;
  logger << LOG_INFO << setw(25) << "after first call: alpha_e_Gmu" << " = " << alpha_e_Gmu << endl;


  
  if (use_cms == 1 || use_cms == 3){logger << LOG_INFO << "Second call of determine_alpha_e_Gmu:" << endl;}
  if (conversion_to_pole_mass){
    logger << LOG_INFO << "Z and W parameters are convereted to pole parameters." << endl;
    logger << LOG_INFO << "Before:" << endl;
    logger << LOG_INFO << setw(25) << "M_W" << " = " << M_W << endl;
    logger << LOG_INFO << setw(25) << "Gamma_W" << " = " << Gamma_W << endl;
    logger << LOG_INFO << setw(25) << "M_Z" << " = " << M_Z << endl;
    logger << LOG_INFO << setw(25) << "Gamma_Z" << " = " << Gamma_Z << endl;
    double M_W_pole = M_W / sqrt(1. + pow(Gamma_W / M_W, 2)); 
    double Gamma_W_pole = Gamma_W / sqrt(1. + pow(Gamma_W / M_W, 2)); 
    double M_Z_pole = M_Z / sqrt(1. + pow(Gamma_Z / M_Z, 2)); 
    double Gamma_Z_pole = Gamma_Z / sqrt(1. + pow(Gamma_Z / M_Z, 2)); 
    M_W = M_W_pole;
    Gamma_W = Gamma_W_pole;
    M_Z = M_Z_pole;
    Gamma_Z = Gamma_Z_pole;
    logger << LOG_INFO << "After:" << endl;
    logger << LOG_INFO << setw(25) << "M_W" << " = " << M_W << endl;
    logger << LOG_INFO << setw(25) << "Gamma_W" << " = " << Gamma_W << endl;
    logger << LOG_INFO << setw(25) << "M_Z" << " = " << M_Z << endl;
    logger << LOG_INFO << setw(25) << "Gamma_Z" << " = " << Gamma_Z << endl;
  }
  
  logger << LOG_INFO << setw(25) << "G_F" << " = " << G_F << endl;
  logger << LOG_INFO << setw(25) << "before second call: G_F" << " = " << G_F << endl;
  logger << LOG_INFO << setw(25) << "before second call: alpha_e_Gmu" << " = " << alpha_e_Gmu << endl;
  fill_particle_mass_vector();

  if (use_cms == 1 || use_cms == 3){determine_alpha_e_Gmu(det_EW_coupling, alpha_rescaling_exp);}
  logger << LOG_INFO << setw(25) << "after second call: G_F" << " = " << G_F << endl;
  logger << LOG_INFO << setw(25) << "after second call: alpha_e_Gmu" << " = " << alpha_e_Gmu << endl;
  calculate_Gamma_H(present_type_perturbative_order);
  calculate_Gamma_t(present_type_perturbative_order);
  fill_particle_mass_vector();

  if (ew_scheme == 0){
    if (alpha_e_0 == empty_double){alpha_e_0 = 1. / 137.03599907399999;}
    alpha_e = alpha_e_0;
    logger << LOG_DEBUG << "alpha_e  is set to chosen value of  alpha_e_0 = " << setprecision(15) << setw(23) << alpha_e_0 << " = 1 / " << setprecision(15) << setw(23) <<  alpha_e_0 << " ." << endl;
  }
  else if (ew_scheme == 1 || ew_scheme == -1){
    alpha_e = alpha_e_Gmu;
    if (use_cms == 3){use_cms = 2;}
    logger << LOG_DEBUG << "alpha_e  is set to chosen value of  alpha_e_Gmu = " << setprecision(15) << setw(23) << alpha_e_Gmu << " = 1 / " << setprecision(15) << setw(23) <<  alpha_e_Gmu << " ." << endl;
  }
  /*
  else if (ew_scheme == -1){
    alpha_e = alpha_e_Gmu;
    logger << LOG_DEBUG << "alpha_e  is set to chosen value of  alpha_e_Gmu = " << setprecision(15) << setw(23) << alpha_e_Gmu << " = 1 / " << setprecision(15) << setw(23) <<  alpha_e_Gmu << " ." << endl;
  }
  */
  else if (ew_scheme == 2){
    if (alpha_e_MZ == empty_double){alpha_e_MZ = 1. / 128.;}
    alpha_e = alpha_e_MZ;
    logger << LOG_DEBUG << "alpha_e  is set to chosen value of  alpha_e_MZ = " << setprecision(15) << setw(23) << alpha_e_MZ << " = 1 / " << setprecision(15) << setw(23) << alpha_e_MZ << " ." << endl;
  }
  else {logger << LOG_ERROR << "Invalid value:  ew_scheme = " << ew_scheme << endl; exit(1);}

  e = sqrt(4 * pi * alpha_e);

  // temporary !!! This should be controlled by use_cms !!!
  int switch_costheta_real = user->switch_value[user->switch_map["switch_costheta_real"]];

  determine_EWcouplings_complex(switch_costheta_real);

  logger << LOG_INFO << "ew_scheme = " << ew_scheme << endl;
  logger << LOG_INFO << "use_adapted_ew_coupling = " << use_adapted_ew_coupling << endl;
  if (ew_scheme == use_adapted_ew_coupling){use_adapted_ew_coupling = -1;}  // No modification of alpha_e factor required
  logger << LOG_INFO << "use_adapted_ew_coupling = " << use_adapted_ew_coupling << endl;

  /*
  logger << LOG_DEBUG_VERBOSE << "alpha_e     = " << alpha_e << endl;
  logger << LOG_DEBUG_VERBOSE << "e           = " << e << endl;
  logger << LOG_DEBUG_VERBOSE << "G_F         = " << G_F << endl;
  logger << LOG_DEBUG_VERBOSE << "det_EW_coupling.size() = " << det_EW_coupling.size() << endl;

  determine_alpha_e_Gmu(det_EW_coupling, alpha_rescaling_exp);
  */

  // At present, OpenLoops does support non-trivial CKM matrices only for selected amplitudes.
  // CKM_matrix must be set to "trivial" for the remaining ones.
  determine_CKM_matrix();

  e_pow.resize(11);
  for (int i = 0; i < e_pow.size(); i++){e_pow[i] = pow(e, i);}

  /*
  logger << LOG_INFO << "alpha_e = " << setprecision(15) << alpha_e << endl;
  if (use_cms == 1){alpha_e = abs(sqrt2 * pow(csin_w, 2) * pow(cM_W, 2) * G_F / pi);}
  logger << LOG_INFO << "use_cms = " << use_cms << endl;
  logger << LOG_INFO << "alpha_e = " << setprecision(15) << alpha_e << endl;
  */

  output_model_file(switch_costheta_real);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
