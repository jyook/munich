#include "header.hpp"

////////////////////
//  constructors  //
////////////////////

inputparameter_set::inputparameter_set(){
  Logger logger("inputparameter_set::inputparameter_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


inputparameter_set::inputparameter_set(string basic_process_class, string subprocess){
  Logger logger("inputparameter_set::inputparameter_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  Log::setLogThreshold(LOG_DEBUG_VERBOSE);
  input_complete.push_back(inputline("basic_process_class", basic_process_class));
  input_complete.push_back(inputline("subprocess", subprocess));

  initialization(subprocess);
  initialization();
  initialization_input_modification();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::initialization(string subprocess){
  Logger logger("inputparameter_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  Log::setLogThreshold(LOG_INFO);
  path_MUNICH = get_path();

  vector<string> readin_parameter;
  vector<string> readin_model;
  vector<string> readin_distribution;

  parameter_readin(subprocess, readin_parameter);
  model_readin(subprocess, readin_model);
  distribution_readin(subprocess, readin_distribution);

  get_userinput_from_readin(readin_parameter, input_complete);
  get_userinput_from_readin(readin_model, input_model);
  get_userinput_from_readin(readin_distribution, input_distribution);

  if (subprocess == ""){
    input_complete.push_back(inputline("type_perturbative_order", "LO"));
    input_complete.push_back(inputline("type_contribution", ""));
    input_complete.push_back(inputline("type_correction", "---"));

    input_complete.push_back(inputline("path_to_main", "../"));
    input_complete.push_back(inputline("switch_output_testpoint", "0"));
    input_complete.push_back(inputline("switch_result", "0"));
    input_complete.push_back(inputline("switch_distribution", "0"));
    input_complete.push_back(inputline("switch_moment", "0"));
    input_complete.push_back(inputline("switch_output_execution", "0"));
    input_complete.push_back(inputline("switch_output_integration", "0"));
    input_complete.push_back(inputline("switch_output_time", "0"));
    input_complete.push_back(inputline("switch_output_maxevent", "0"));
    input_complete.push_back(inputline("switch_output_comparison", "0"));
    input_complete.push_back(inputline("switch_output_gnuplot", "0"));
    input_complete.push_back(inputline("switch_output_proceeding", "0"));
    input_complete.push_back(inputline("switch_output_weights", "0"));
    input_complete.push_back(inputline("switch_output_result", "0"));
    input_complete.push_back(inputline("switch_output_distribution", "0"));
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::initialization(){
  Logger logger("inputparameter_set::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  select_event_input_from_userinput();
  select_phasespace_input_from_userinput();
  select_input_input_from_userinput();
  select_observable_input_from_userinput();
  select_contribution_input_from_userinput();
  select_amplitude_input_from_userinput();
  select_amplitude_OpenLoops_input_from_userinput();
  select_amplitude_Recola_input_from_userinput();
  select_userdefined_input_from_userinput();
  select_leftover_input_from_userinput();

  select_model_input_from_userinput();
  select_distribution_input_from_userinput();
  select_dddistribution_input_from_userinput();

  for (size_t i_i = 0; i_i < input_complete.size(); i_i++){
    //    logger << LOG_DEBUG_VERBOSE << "input_complete[" << i_i << "].variable = " << input_complete[i_i].variable << "   input_complete.size() = " << input_complete.size() << endl;
    int assigned = 0;
    if (assigned_to_input[input_complete[i_i].variable]){input_input.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_contribution[input_complete[i_i].variable]){input_contribution.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_userdefined[input_complete[i_i].variable]){input_userdefined.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_event[input_complete[i_i].variable]){input_event.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_phasespace[input_complete[i_i].variable]){input_phasespace.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_observable[input_complete[i_i].variable]){input_observable.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_model[input_complete[i_i].variable]){input_model.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_amplitude[input_complete[i_i].variable]){input_amplitude.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_amplitude_OpenLoops[input_complete[i_i].variable]){input_amplitude_OpenLoops.push_back(input_complete[i_i]); assigned++;}
    if (assigned_to_amplitude_Recola[input_complete[i_i].variable]){input_amplitude_Recola.push_back(input_complete[i_i]); assigned++;}
    //    logger << LOG_DEBUG_VERBOSE << "assigned = " << assigned << "   i_i = " << i_i << "   input_complete.size() = " << input_complete.size() << endl;

    if (!assigned){input_leftover.push_back(input_complete[i_i]);}
  }
  logger << LOG_DEBUG << "input_complete.size() = " << input_complete.size() << endl;
  logger << LOG_DEBUG << "input_input.size() = " << input_input.size() << endl;
  logger << LOG_DEBUG << "input_userdefined.size() = " << input_userdefined.size() << endl;
  logger << LOG_DEBUG << "input_event.size() = " << input_event.size() << endl;
  logger << LOG_DEBUG << "input_phasespace.size() = " << input_phasespace.size() << endl;
  logger << LOG_DEBUG << "input_observable.size() = " << input_observable.size() << endl;
  logger << LOG_DEBUG << "input_amplitude.size() = " << input_amplitude.size() << endl;
  logger << LOG_DEBUG << "input_amplitude_OpenLoops.size() = " << input_amplitude_OpenLoops.size() << endl;
  logger << LOG_DEBUG << "input_amplitude_Recola.size() = " << input_amplitude_Recola.size() << endl;
  logger << LOG_DEBUG << "input_leftover.size() = " << input_leftover.size() << endl;

  for (size_t i_i = 0; i_i < input_leftover.size(); i_i++){
    if (!assigned_to_model[input_leftover[i_i].variable]){input_model.push_back(input_leftover[i_i]);}
  }
  logger << LOG_DEBUG << "input_model.size() = " << input_model.size() << endl;

  for (size_t i_i = 0; i_i < input_leftover.size(); i_i++){
    if (!assigned_to_distribution[input_leftover[i_i].variable]){input_distribution.push_back(input_leftover[i_i]);}
  }
  logger << LOG_DEBUG << "input_distribution.size() = " << input_distribution.size() << endl;


  max_perturbative_QCD_order = 0;
  for (size_t i_i = 0; i_i < input_input.size(); i_i++){
    if (input_input[i_i].variable == "type_perturbative_order"){
      if      (input_input[i_i].value == "LO"){if (max_perturbative_QCD_order < 0){max_perturbative_QCD_order = 0;}}
      else if (input_input[i_i].value == "NLO"){if (max_perturbative_QCD_order < 1){max_perturbative_QCD_order = 1;}}
      else if (input_input[i_i].value == "NNLO"){if (max_perturbative_QCD_order < 2){max_perturbative_QCD_order = 2;}}
      else if (input_input[i_i].value == "NNNLO"){if (max_perturbative_QCD_order < 3){max_perturbative_QCD_order = 3;}}
    }
    else if (input_input[i_i].variable == "output_level"){
      output_level = input_input[i_i].value.c_str();
      if (output_level == "DEBUG_VERBOSE"){Log::setLogThreshold(LOG_DEBUG_VERBOSE);}
      if (output_level == "DEBUG_POINT"){Log::setLogThreshold(LOG_DEBUG_POINT);}
      if (output_level == "DEBUG"){Log::setLogThreshold(LOG_DEBUG);}
      if (output_level == "INFO"){Log::setLogThreshold(LOG_INFO);}
      if (output_level == "WARN"){Log::setLogThreshold(LOG_WARN);}
      if (output_level == "ERROR"){Log::setLogThreshold(LOG_ERROR);}
      if (output_level == "FATAL"){Log::setLogThreshold(LOG_FATAL);}
      logger << LOG_DEBUG_VERBOSE << "output_level = " << output_level << endl;
    }
  }


  ////////////////////////////////////////////////////////////////////////////
  //  determination of particular type_perturbative_order to be calculated  //
  ////////////////////////////////////////////////////////////////////////////

  present_type_perturbative_order = -1;
  type_perturbative_order_counter = 0;
  collection_type_perturbative_order.resize(1, "all");
  for (size_t i_i = 0; i_i < input_input.size(); i_i++){
    if (input_input[i_i].variable == "type_perturbative_order"){
      int flag = -1;
      for (int is = 0; is < type_perturbative_order_counter + 1; is++){if (input_input[i_i].value == collection_type_perturbative_order[is]){flag = is; break;}}
      //      logger << LOG_DEBUG_VERBOSE << "i_i = " << setw(4) << i_i << "   ---" << input_input[i_i].value << "---  flag = " << flag << endl;
      if (flag == -1){
	type_perturbative_order_counter++;
	collection_type_perturbative_order.push_back(input_input[i_i].value);
	present_type_perturbative_order = type_perturbative_order_counter;
      }
      else {present_type_perturbative_order = flag;}
    }
  }
  logger << LOG_INFO << "number of type_perturbative_order's: " << collection_type_perturbative_order.size() << endl;
  for (int is = 0; is < type_perturbative_order_counter + 1; is++){logger << LOG_INFO << "collection_type_perturbative_order[" << setw(2) << is << "] = " << collection_type_perturbative_order[is] << endl;}
  logger << LOG_INFO << "present_type_perturbative_order:     " << present_type_perturbative_order << endl;
  logger << LOG_INFO << "type_perturbative_order = collection_type_perturbative_order[present_type_perturbative_order = " << present_type_perturbative_order << "]:             " << collection_type_perturbative_order[present_type_perturbative_order] << endl;

  logger.newLine(LOG_INFO);

  //////////////////////////////////////////////////////////////////////
  //  determination of particular type_contribution to be calculated  //
  //////////////////////////////////////////////////////////////////////

  present_type_contribution = -1;
  type_contribution_counter = 0;
  collection_type_contribution.resize(1, "all");
  for (size_t i_i = 0; i_i < input_input.size(); i_i++){
    if (input_input[i_i].variable == "type_contribution"){
      int flag = -1;
      for (int is = 0; is < type_contribution_counter + 1; is++){if (input_input[i_i].value == collection_type_contribution[is]){flag = is; break;}}
      if (flag == -1){
	type_contribution_counter++;
	collection_type_contribution.push_back(input_input[i_i].value);
	present_type_contribution = type_contribution_counter;
      }
      else {present_type_contribution = flag;}
    }
  }
  logger << LOG_INFO << "number of type_contributions: " << collection_type_contribution.size() << endl;
  for (int is = 0; is < type_contribution_counter + 1; is++){logger << LOG_INFO << "collection_type_contribution[" << setw(2) << is << "] = " << collection_type_contribution[is] << endl;}
  logger << LOG_INFO << "present_type_contribution:    " << present_type_contribution << endl;
  logger << LOG_INFO << "type_contribution = collection_type_contribution[present_type_contribution = " << present_type_contribution << "]:             " << collection_type_contribution[present_type_contribution] << endl;

  logger.newLine(LOG_INFO);

  ////////////////////////////////////////////////////////////////////
  //  determination of particular type_correction to be calculated  //
  ////////////////////////////////////////////////////////////////////

  present_type_correction = -1;
  type_correction_counter = 0;
  collection_type_correction.resize(1, "all");
  for (size_t i_i = 0; i_i < input_input.size(); i_i++){
    if (input_input[i_i].variable == "type_correction"){
      int flag = -1;
      for (int is = 0; is < type_correction_counter + 1; is++){if (input_input[i_i].value == collection_type_correction[is]){flag = is; break;}}
      if (flag == -1){
	type_correction_counter++;
	collection_type_correction.push_back(input_input[i_i].value);
	present_type_correction = type_correction_counter;
      }
      else {present_type_correction = flag;}
    }
  }
  logger << LOG_INFO << "number of type_correction's: " << collection_type_correction.size() << endl;
  for (int is = 0; is < type_correction_counter + 1; is++){logger << LOG_INFO << "collection_type_correction[" << setw(2) << is << "] = " << collection_type_correction[is] << endl;}
  logger << LOG_INFO << "present_type_correction:     " << present_type_correction << endl;
  logger << LOG_INFO << "type_correction = collection_type_correction[present_type_correction = " << present_type_correction << "]:             " << collection_type_correction[present_type_correction] << endl;
  logger.newLine(LOG_INFO);
  logger.newLine(LOG_INFO);

  for (int i_o = 0; i_o < collection_type_perturbative_order.size(); i_o++){logger << LOG_INFO << "collection_type_perturbative_order[" << i_o << "] = " << collection_type_perturbative_order[i_o] << endl;}
  for (int i_o = 0; i_o < collection_type_contribution.size(); i_o++){logger << LOG_INFO << "collection_type_contribution[" << i_o << "] = " << collection_type_contribution[i_o] << endl;}
  for (int i_o = 0; i_o < collection_type_correction.size(); i_o++){logger << LOG_INFO << "collection_type_correction[" << i_o << "] = " << collection_type_correction[i_o] << endl;}
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << left << setw(31) << "present_type_perturbative_order" << " = " << present_type_perturbative_order << "   " << setw(34) << "collection_type_perturbative_order" << "[" << setw(2) << present_type_perturbative_order << "] = " << setw(10) << collection_type_perturbative_order[present_type_perturbative_order] << endl;
  logger << LOG_INFO << setw(31) << "present_type_contribution" << " = " << present_type_contribution << "   " << setw(34) << "collection_type_contribution" << "[" << setw(2) << present_type_contribution << "] = " << setw(10) << collection_type_contribution[present_type_contribution] << endl;
  logger << LOG_INFO << setw(31) << "present_type_correction" << " = " << present_type_correction << "   " << setw(34) << "collection_type_correction" << "[" << setw(2) << present_type_correction << "] = " << setw(10) << collection_type_correction[present_type_correction] << endl;

  logger.newLine(LOG_INFO);
  
  logger << LOG_INFO << " After removing 'all' entry:" << endl;
  logger.newLine(LOG_INFO);
  collection_type_perturbative_order.erase(collection_type_perturbative_order.begin());
  present_type_perturbative_order--;
  collection_type_contribution.erase(collection_type_contribution.begin());
  present_type_contribution--;
  collection_type_correction.erase(collection_type_correction.begin());
  present_type_correction--;
  
  logger << LOG_INFO << left << setw(31) << "present_type_perturbative_order" << " = " << present_type_perturbative_order << endl;
  logger << LOG_INFO << setw(31) << "present_type_contribution" << " = " << present_type_contribution << endl;
  logger << LOG_INFO << setw(31) << "present_type_correction" << " = " << present_type_correction << endl;

  logger.newLine(LOG_INFO);
  for (int i_o = 0; i_o < collection_type_perturbative_order.size(); i_o++){logger << LOG_INFO << "collection_type_perturbative_order[" << i_o << "] = " << collection_type_perturbative_order[i_o] << endl;}
  for (int i_o = 0; i_o < collection_type_contribution.size(); i_o++){logger << LOG_INFO << "collection_type_contribution[" << i_o << "] = " << collection_type_contribution[i_o] << endl;}
  for (int i_o = 0; i_o < collection_type_correction.size(); i_o++){logger << LOG_INFO << "collection_type_correction[" << i_o << "] = " << collection_type_correction[i_o] << endl;}
  logger.newLine(LOG_INFO);

  logger << LOG_INFO << left << setw(31) << "present_type_perturbative_order" << " = " << present_type_perturbative_order << "   " << setw(34) << "collection_type_perturbative_order" << "[" << setw(2) << present_type_perturbative_order << "] = " << setw(10) << collection_type_perturbative_order[present_type_perturbative_order] << endl;
  logger << LOG_INFO << setw(31) << "present_type_contribution" << " = " << present_type_contribution << "   " << setw(34) << "collection_type_contribution" << "[" << setw(2) << present_type_contribution << "] = " << setw(10) << collection_type_contribution[present_type_contribution] << endl;
  logger << LOG_INFO << setw(31) << "present_type_correction" << " = " << present_type_correction << "   " << setw(34) << "collection_type_correction" << "[" << setw(2) << present_type_correction << "] = " << setw(10) << collection_type_correction[present_type_correction] << endl;
  logger.newLine(LOG_INFO);



  int temp_type_perturbative_order = -1;
  //  int temp_type_contribution = -1;
  //  int temp_type_correction = -1;

  ///////////////////////
  //  beam parameters  //
  ///////////////////////
  // ???
  contribution_LHAPDFname.resize(collection_type_perturbative_order.size());
  contribution_LHAPDFsubset.resize(collection_type_perturbative_order.size());

  for (size_t i_i = 0; i_i < input_input.size(); i_i++){
    if (input_input[i_i].variable == ""){}

    else if (input_input[i_i].variable == "type_perturbative_order"){
      /*
      if (input_input[i_i].value == "all" || input_input[i_i].value == collection_type_perturbative_order[present_type_perturbative_order]){temp_type_perturbative_order = present_type_perturbative_order;}
      else {temp_type_perturbative_order = -1;}
      */
      // In this context, all selected orders need to be filled !!!
      temp_type_perturbative_order = -1;
      for (int i_to = 0; i_to < collection_type_perturbative_order.size(); i_to++){
	//	if (input_input[i_i].value == "all"){temp_type_perturbative_order = present_type_perturbative_order; break;}
	if (input_input[i_i].value == "all"){temp_type_perturbative_order = -2; break;}
	else if (input_input[i_i].value == collection_type_perturbative_order[i_to]){temp_type_perturbative_order = i_to; break;}
      }

      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << input_input[i_i].variable << " = " << setw(32) << input_input[i_i].value << setw(32) << "temp_type_perturbative_order = " << temp_type_perturbative_order << endl;
    }
    else if (input_input[i_i].variable == "LHAPDFname"){
      if (temp_type_perturbative_order != -1){
	if (temp_type_perturbative_order == -2){
	  for (int i_o = 0; i_o < collection_type_perturbative_order.size(); i_o++){contribution_LHAPDFname[i_o] = input_input[i_i].value;}
	}
	else {contribution_LHAPDFname[temp_type_perturbative_order] = input_input[i_i].value;}
	//	logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << setw(32) << left << input_input[i_i].variable << " = " << setw(32) << input_input[i_i].value << setw(32) << "contribution_LHAPDFname[temp_type_perturbative_order = " << temp_type_perturbative_order << "] = " << contribution_LHAPDFname[temp_type_perturbative_order] << endl;
      }
    }
    else if (input_input[i_i].variable == "LHAPDFsubset"){
      if (temp_type_perturbative_order != -1){
	if (temp_type_perturbative_order == -2){
	  for (int i_o = 0; i_o < collection_type_perturbative_order.size(); i_o++){contribution_LHAPDFsubset[i_o] = atoi(input_input[i_i].value.c_str());}
	}
	else {contribution_LHAPDFsubset[temp_type_perturbative_order] = atoi(input_input[i_i].value.c_str());}
	//	logger << LOG_DEBUG_VERBOSE << "user_variable[" << i_i << "] = " << setw(32) << left << input_input[i_i].variable << " = " << setw(32) << input_input[i_i].value << setw(32) << "contribution_LHAPDFsubset[temp_type_perturbative_order = " << temp_type_perturbative_order << "] = " << contribution_LHAPDFsubset[temp_type_perturbative_order] << endl;
      }
    }
  }

  logger.newLine(LOG_INFO);
  for (int i_o = 0; i_o < contribution_LHAPDFname.size(); i_o++){
    logger << LOG_INFO << "contribution_LHAPDFname[" << i_o << "] = " << setw(30) << contribution_LHAPDFname[i_o] << "   " << "contribution_LHAPDFsubset[" << i_o << "] = " << contribution_LHAPDFsubset[i_o] << endl;
  }
  logger.newLine(LOG_INFO);



  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void inputparameter_set::initialization_input_modification(){
  Logger logger("inputparameter_set::initialization_input_modification");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (present_type_perturbative_order > -1){
    LHAPDFname = contribution_LHAPDFname[present_type_perturbative_order];
    LHAPDFsubset = contribution_LHAPDFsubset[present_type_perturbative_order];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void inputparameter_set::parameter_readin_file(string filename, vector<string> & readin, bool essential, int retry = 1) {
  Logger logger("inputparameter_set::parameter_readin_file");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  char LineBuffer[256];
  errno = 0;

  for (int attempts=0; attempts<retry; attempts++) {
    ifstream in_new_parameter(filename.c_str());
    if (!in_new_parameter) {
      if (essential){
	logger << LOG_WARN << filename << " could not be opened" << endl;
	logger << LOG_WARN << "Error code: " << errno << " (" << strerror(errno) << ")" << endl;
      }
    }
    else {
      size_t temp_start = readin.size();
      while (in_new_parameter.getline(LineBuffer, 256)){readin.push_back(LineBuffer);}
      logger << LOG_DEBUG << "filename = " << filename << "   " << readin.size() << endl;
      for (int i = temp_start; i < readin.size(); i++){logger << LOG_DEBUG_VERBOSE << "readin[" << setw(4) << "] = " << readin[i] << endl;}
      in_new_parameter.close();
      break;
    }
    if (attempts<retry-1) {
      // workaround for NFS issues..
      logger << LOG_WARN << "retrying.." << endl;
      usleep(1000000);
    }
  }
  // assert out if the file_parameter.dat to be read in was essential
  assert(!essential || errno==0);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void inputparameter_set::parameter_readin(string & subprocess, vector<string> & readin){
  Logger logger("inputparameter_set::parameter_readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << "subprocess = " << subprocess << endl;

  if (subprocess == ""){
    //    char LineBuffer[256];
    // default parameter file
    string filename;
    filename = "../setup/file_parameter.dat";
    parameter_readin_file(filename, readin, true, 3);
    /*
    logger << LOG_DEBUG_VERBOSE << "filename = " << filename << endl;
    ifstream in_new_parameter_default(filename.c_str());
    while (in_new_parameter_default.getline(LineBuffer, 256)){readin.push_back(LineBuffer);}
    in_new_parameter_default.close();
    for (int i = 0; i < readin.size(); i++){logger << LOG_DEBUG_VERBOSE << "default:    readin[" << setw(3) << "] = " << readin[i] << endl;}
    */
    // general parameter file
    filename = "../file_parameter.dat";
    parameter_readin_file(filename, readin, true, 3);
    /*
    logger << LOG_DEBUG_VERBOSE << "filename = " << filename << endl;
    ifstream in_new_parameter_gen(filename.c_str());
    while (in_new_parameter_gen.getline(LineBuffer, 256)){readin.push_back(LineBuffer);}
    in_new_parameter_gen.close();
    for (int i = 0; i < readin.size(); i++){logger << LOG_DEBUG_VERBOSE << "general:    readin[" << setw(3) << "] = " << readin[i] << endl;}
    */
    // summary parameter file
    filename = "file_parameter.dat";
    parameter_readin_file(filename, readin, false, 1);
    /*
    logger << LOG_DEBUG_VERBOSE << "filename = " << filename << endl;
    ifstream in_new_parameter_con(filename.c_str());
    while (in_new_parameter_con.getline(LineBuffer, 256)){readin.push_back(LineBuffer);}
    for (int i = 0; i < readin.size(); i++){logger << LOG_DEBUG_VERBOSE << "summary:    readin[" << setw(3) << "] = " << readin[i] << endl;}
    in_new_parameter_con.close();
    */
  }
  else {
    string filename;
    // default parameter file
    filename = "../../../../setup/file_parameter.dat";
    parameter_readin_file(filename, readin, true, 3);

    // general parameter file
    filename = "../../../../file_parameter.dat";
    parameter_readin_file(filename, readin, true, 3);

    // perturbative_order.subtraction_method parameter file
    filename = "../../../file_parameter.dat";
    parameter_readin_file(filename, readin, false, 1);

    // coupling_order parameter file
    filename = "../../file_parameter.dat";
    parameter_readin_file(filename, readin, false, 1);

    // contribution.correction parameter file
    filename = "../file_parameter.dat";
    parameter_readin_file(filename, readin, false, 1);

    // individual rundirectory parameter file
    filename = "file_parameter.dat";
    parameter_readin_file(filename, readin, true, 3);

    // individual rundirectory subprocess parameter file
    filename = "log/file_parameter." + subprocess + ".dat";
    parameter_readin_file(filename, readin, false, 1);
  }
  logger << LOG_DEBUG_VERBOSE << "parameter readin: readin.size() = " << readin.size() << endl;

  if (readin.size() == 0) {
    logger << LOG_FATAL << "file_parameter.dat  is empty." << endl;
    assert(readin.size() > 0);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::model_readin(string & subprocess, vector<string> & readin){
  Logger logger("inputparameter_set::model_readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (subprocess == ""){
    string filename;
    filename = "../setup/file_model.dat";
    parameter_readin_file(filename, readin, false, 1);

    // general parameter file
    filename = "../file_model.dat";
    parameter_readin_file(filename, readin, true, 1);

    // individual rundirectory paramete1);
  }
  else {
    string filename;
    filename = "../../../../setup/file_model.dat";
    parameter_readin_file(filename, readin, false, 1);

    // general parameter file
    filename = "../../../../file_model.dat";
    parameter_readin_file(filename, readin, true, 1);

    // perturbative_order.subtraction_method parameter file
    filename = "../../../file_model.dat";
    parameter_readin_file(filename, readin, false, 1);

    // coupling_order parameter file
    filename = "../../file_model.dat";
    parameter_readin_file(filename, readin, false, 1);

    // contribution.correction parameter file
    filename = "../file_model.dat";
    parameter_readin_file(filename, readin, false, 1);

    // individual rundirectory parameter file
    filename = "file_model.dat";
    parameter_readin_file(filename, readin, false, 1);
  }

  if (readin.size() == 0) {
    logger << LOG_ERROR << "file_model.dat  is empty." << endl;
    assert(readin.size() > 0);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::distribution_readin(string & subprocess, vector<string> & readin){
  Logger logger("inputparameter_set::distribution_readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (subprocess == ""){
    string filename;
    filename = "../setup/file_distribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    filename = "../setup/file_dddistribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    // general parameter file
    filename = "../file_distribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    filename = "../file_dddistribution.dat";
    parameter_readin_file(filename, readin, false, 1);
  }
  else {
    string filename;
    filename = "../../../../setup/file_distribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    filename = "../../../../setup/file_dddistribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    // general parameter file
    filename = "../../../../file_distribution.dat";
    parameter_readin_file(filename, readin, false, 1);

    filename = "../../../../file_dddistribution.dat";
    parameter_readin_file(filename, readin, false, 1);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::get_userinput_from_readin(vector<string> & readin, vector<inputline> & input){
  Logger logger("inputparameter_set::get_userinput_from_readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  string readindata;
  //  for (int i = 0; i < readin->size(); i++){
  for (int i = 0; i < readin.size(); i++){
    //    logger << LOG_DEBUG << "readin[" << setw(4) << i << "].size() = " << readin->at(i).size() << endl;
    //    logger << LOG_DEBUG << "readin[" << setw(4) << i << "].size() = " << readin[i].size() << endl;
    //    readindata = readin[i][0];
    //    if (readin->at(i)[0] != "/" && readin->at(i)[0] != "#" && readin->at(i)[0] != "%"){
    if (readin[i][0] != '/' && readin[i][0] != '#' && readin[i][0] != '%'){
      //    if (readin[i][0] != "/" && readin[i][0] != "#" && readin[i][0] != "%"){
      //      inputline temp_inputline = inputline(readin->at(i));
      inputline temp_inputline = inputline(readin[i]);
      if (temp_inputline.valid){input.push_back(temp_inputline);}
    }
  }


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


ostream & operator << (ostream & s_isi, const inputparameter_set & isi){
  // Needs to be filled if required:
  return s_isi;
}



// summary_list still uses this function !!!

void inputparameter_set::get_userinput_from_readin(vector<string> & user_variable, vector<string> & user_variable_additional, vector<string> & user_value, vector<string> & readin){
  Logger logger("get_userinput_from_readin");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  string readindata;
  for (int i = 0; i < readin.size(); i++){
    logger << LOG_DEBUG << "readin[" << setw(4) << i << "].size() = " << readin[i].size() << endl;
    readindata = readin[i][0];
    if (readindata != "/" && readindata != "#" && readindata != "%"){
      int start = 0;
      user_variable.push_back("");
      user_variable_additional.push_back("");
      user_value.push_back("");
      for (int j = 0; j < readin[i].size(); j++){
	if (start == 0 || start == 1){
	  if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 0){}
	  else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	    user_variable[user_variable.size() - 1].push_back(readin[i][j]);
	    if (start != 1){start = 1;}
	  }
	  else {start++;}
	}
	else if (start == 2){
	  if (readin[i][j] == '='){start = 5;}
	  else if ((readin[i][j] == ' ') || (readin[i][j] == char(9))){}
	  else if (start == 2){
	    start++;
	    j--;
	  }
	  else {
	    logger << LOG_ERROR << "Incorrect input in line " << i << endl;
	    user_variable.erase(user_variable.end(), user_variable.end());
	    user_variable_additional.erase(user_variable_additional.end(), user_variable_additional.end());
	    user_value.erase(user_value.end(), user_value.end());
	    break;
	  }
	}
	else if (start == 3){
	  if ((readin[i][j] != '=')){
	    user_variable_additional[user_variable_additional.size() - 1].push_back(readin[i][j]);
	  }
	  else {start++; j--;} // should be the same as shifting (start == 4) here !!!
	}
	else if (start == 4){
	  // additional: should be allowed to contain ' ' !!!
	  if (readin[i][j] == '='){
	    start = 5;
	    logger << LOG_DEBUG << "before: ---" << user_variable_additional[user_variable_additional.size() - 1] << "---" << endl;
	    for (int i_s = user_variable_additional[user_variable_additional.size() - 1].size() - 1; i_s > 0; i_s--){
	      if (user_variable_additional[user_variable_additional.size() - 1][i_s] == ' ' ||
		  user_variable_additional[user_variable_additional.size() - 1][i_s] == char(9)){
		user_variable_additional[user_variable_additional.size() - 1].erase(user_variable_additional[user_variable_additional.size() - 1].end() - 1, user_variable_additional[user_variable_additional.size() - 1].end());
	      }
	      else {break;}
	    }
	    logger << LOG_DEBUG << "after:  ---" << user_variable_additional[user_variable_additional.size() - 1] << "---" << endl;
	  }
	  //	  else if ((readin[i][j] == ' ') || (readin[i][j] == char(9))){}
	  else {
	    logger << LOG_ERROR << "Incorrect input in line " << i << endl;
	    user_variable.erase(user_variable.end(), user_variable.end());
	    user_variable_additional.erase(user_variable_additional.end(), user_variable_additional.end());
	    user_value.erase(user_value.end(), user_value.end());
	    break;
	  }
	}
	else if (start == 5 || start == 6){
	  if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 5){}
	  else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	    user_value[user_value.size() - 1].push_back(readin[i][j]);
	    if (start != 6){start = 6;}
	  }
	  else {start++;}
	}

	else if (start == 5 || start == 6 || start == 7){
	  // start == 5: before beginning of user_value
	  // start == 6: user_value has started
	  // start == 7:
	  logger << LOG_DEBUG_VERBOSE << "readin[" << i << "][" << j << "] = " << readin[i][j] << endl;
	  if (readin[i][j] == '%'){break;}
	  if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 5){}
	  else if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start > 5){start = 7;}
	  else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	    if (start == 7){user_value[user_value.size() - 1].push_back(' ');}
	    user_value[user_value.size() - 1].push_back(readin[i][j]);
	    start = 6;
	  }
	  else {start++;}
	}

	/*
	else if (start == 5 || start == 6){
	  if (((readin[i][j] == ' ') || (readin[i][j] == char(9))) && start == 5){}
	  else if ((readin[i][j] != ' ') && (readin[i][j] != char(9))){
	    user_value[user_value.size() - 1].push_back(readin[i][j]);
	    if (start != 6){start = 6;}
	  }
	  else {start++;}
	}
	*/

	else {break;}
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


