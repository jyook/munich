#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
runresumption_set::runresumption_set(){
  size_proc_generic = 0;
  size_proceeding.resize(6);
}
runresumption_set::runresumption_set(observable_set & _osi, phasespace_set & _psi, observable_set & _save_osi, phasespace_set & _save_psi){

  // Can probably be removed !!!
  size_proc_generic = 0;
  size_proceeding.resize(6);

  osi = &_osi;
  psi = &_psi;

  resumption_osi = &_save_osi;
  resumption_psi = &_save_psi;

  //  (*resumption_psi).random_manager.proceeding_save();
}

void runresumption_set::perform_proceeding_step(){
  Logger logger("runresumption_set::perform_proceeding_step");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  /*
  logger << LOG_INFO << "Before:" << endl;
  logger << LOG_INFO << "&psi            = " << &psi << endl;
  logger << LOG_INFO << "&resumption_psi = " << &resumption_psi << endl;
  logger << LOG_INFO << "psi->msi            = " << psi->msi << endl;
  logger << LOG_INFO << "resumption_psi->msi = " << resumption_psi->msi << endl;
  logger << LOG_INFO << "&(psi->random_manager)            = " << &(psi->random_manager) << endl;
  logger << LOG_INFO << "&(resumption_psi->random_manager) = " << &(resumption_psi->random_manager) << endl;
  */
  /*
  for (int i_r = 0; i_r < psi->random_manager.random_psp.size(); i_r++){
    //    random_psp[i_r].proceeding_save();
    for (int i_b = 0; i_b < psi->random_manager.random_psp[i_r].n_bins; i_b++){
      logger << LOG_INFO << "psi->random_manager.random_psp[i_r].alpha[" << setw(4) << i_b << "] = " << setprecision(15) << setw(23) << psi->random_manager.random_psp[i_r].alpha[i_b] << "   resumption_psi->random_manager.random_psp[i_r].alpha[" << setw(4) << i_b << "] = " << setprecision(15) << setw(23) << resumption_psi->random_manager.random_psp[i_r].alpha[i_b] << endl;
      //      out_proceeding << double2hexastr(save_alpha[i_b]) << endl;
    }
  }
  */
  /*
  logger << LOG_INFO << "psi->rng.output_random_generator_set()            = " << psi->rng.output_random_generator_set() << endl;
  logger << LOG_INFO << "resumption_psi->rng.output_random_generator_set() = " << resumption_psi->rng.output_random_generator_set() << endl;
  logger << LOG_INFO << "psi->random_manager.random_psp[0].alpha[" << setw(4) << 0 << "]            = " << setprecision(15) << setw(23) << psi->random_manager.random_psp[0].alpha[0] << endl;
  logger << LOG_INFO << "resumption_psi->random_manager.random_psp[0].alpha[" << setw(4) << 0 << "] = " << setprecision(15) << setw(23) << resumption_psi->random_manager.random_psp[0].alpha[0] << endl;
  */


  *resumption_osi = *osi;
  *resumption_psi = *psi;
  /*
  logger << LOG_INFO << "After:" << endl;
  logger << LOG_INFO << "psi->random_manager.random_psp[0].alpha[" << setw(4) << 0 << "]            = " << setprecision(15) << setw(23) << psi->random_manager.random_psp[0].alpha[0] << endl;
  logger << LOG_INFO << "resumption_psi->random_manager.random_psp[0].alpha[" << setw(4) << 0 << "] = " << setprecision(15) << setw(23) << resumption_psi->random_manager.random_psp[0].alpha[0] << endl;
  logger << LOG_INFO << "psi->rng.output_random_generator_set()            = " << psi->rng.output_random_generator_set() << endl;
  logger << LOG_INFO << "resumption_psi->rng.output_random_generator_set() = " << resumption_psi->rng.output_random_generator_set() << endl;
  logger << LOG_INFO << "&psi            = " << &psi << endl;
  logger << LOG_INFO << "&resumption_psi = " << &resumption_psi << endl;
  logger << LOG_INFO << "&(psi->random_manager)            = " << &(psi->random_manager) << endl;
  logger << LOG_INFO << "&(resumption_psi->random_manager) = " << &(resumption_psi->random_manager) << endl;
  */
  /*
  for (int i_r = 0; i_r < psi->random_manager.random_psp.size(); i_r++){
    //    random_psp[i_r].proceeding_save();
    for (int i_b = 0; i_b < psi->random_manager.random_psp[i_r].n_bins; i_b++){
      logger << LOG_INFO << "psi->random_manager.random_psp[i_r].alpha[" << setw(4) << i_b << "] = " << setprecision(15) << setw(23) << psi->random_manager.random_psp[i_r].alpha[i_b] << "   resumption_psi->random_manager.random_psp[i_r].alpha[" << setw(4) << i_b << "] = " << setprecision(15) << setw(23) << resumption_psi->random_manager.random_psp[i_r].alpha[i_b] << endl;
      //      out_proceeding << double2hexastr(save_alpha[i_b]) << endl;
    }
  }
  */
/*
  (*resumption_psi).random_manager.proceeding_save();
  (*resumption_psi).rng.proceeding_save();
												       */
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void runresumption_set::perform_proceeding_in(){
  Logger logger("runresumption_set::perform_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  // psi dependenpende should be treated via the respective pointer in resumption_osi !!!

  ///  (*osi).perform_proceeding_in(*psi);
  (*osi).perform_proceeding_in();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void runresumption_set::perform_proceeding_out(){
  Logger logger("runresumption_set::perform_proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  // Should depend on value of switch_proceeding when used !!!
  // Check addresses of osi, psi and resumption_osi, resumption_psi !!!
  ///  (*resumption_osi).perform_proceeding_out(*resumption_psi);
  (*resumption_osi).perform_proceeding_out();
  //  (*osi).perform_proceeding_out(*psi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;

}
void runresumption_set::perform_proceeding_check(){
  Logger logger("runresumption_set::perform_proceeding_check");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  (*osi).perform_proceeding_check();
  ///  (*osi).perform_proceeding_check(*psi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void runresumption_set::perform_iteration_step(){
  Logger logger("runresumption_set::perform_iteration_step");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  ///  static int last_step_mode = 0;
  logger << LOG_DEBUG_VERBOSE << "*psi->i_step_mode = " << setw(12) << *psi->i_step_mode << "   "
	 << setw(12) << psi->i_gen << " [" << setw(3) << psi->i_nan << "] " << " "
	 << setw(10) << psi->i_acc << " (" << setw(7) << psi->i_tec << ") " << " "
	 << setw(10) << psi->i_rej << " "
	 << endl;
  logger << LOG_DEBUG_VERBOSE << "psi->last_step_mode = " << psi->last_step_mode << "   psi->n_step = " << psi->n_step << endl;
  logger << LOG_DEBUG_VERBOSE << "(*psi->i_step_mode > psi->last_step_mode) = " << (*psi->i_step_mode > psi->last_step_mode) << endl;
  logger << LOG_DEBUG_VERBOSE << "(*psi->i_step_mode % psi->n_step == 0) = " << (*psi->i_step_mode % psi->n_step == 0) << endl;
  if (*psi->i_step_mode > psi->last_step_mode && *psi->i_step_mode % psi->n_step == 0){
    /*
    Log::setLogThreshold(LOG_DEBUG_VERBOSE);
 logger << LOG_DEBUG_VERBOSE << "*psi->i_step_mode = " << setw(12) << *psi->i_step_mode << "   "
	 << setw(12) << psi->i_gen << " [" << setw(3) << psi->i_nan << "] " << " "
	 << setw(10) << psi->i_acc << " (" << setw(7) << psi->i_tec << ") " << " "
	 << setw(10) << psi->i_rej << " "
	 << endl;
    */

    if (osi->switch_output_proceeding == 2){perform_proceeding_out();}
    osi->calculate_intermediate_result();
    ///    osi->calculate_intermediate_result(*psi);

    logger << LOG_DEBUG_VERBOSE << "osi->perform_integration_step_complete" << endl;
    osi->perform_integration_step_complete();
    if (osi->switch_output_distribution){osi->output_distribution_complete();}

    if (osi->csi->type_contribution == "RVA" ||
	osi->csi->type_contribution == "L2RT" ||
	osi->csi->type_contribution == "L2RJ"){
      logger << LOG_INFO << "Information about killed phase-space points at " << psi->i_acc << " events:" << endl;
      for (int i_q = 0; i_q < osi->n_qTcut; i_q++){
	if (osi->counter_acc_qTcut[i_q] > 0){
	  logger << LOG_INFO << "counter_killed_qTcut[" << setw(3) << i_q << "] = " << setw(8) << osi->counter_killed_qTcut[i_q] << "   counter_acc_qTcut[" << setw(3) << i_q << "] = " << setw(8) << osi->counter_acc_qTcut[i_q] << "   ratio = " << double(osi->counter_killed_qTcut[i_q]) / double(osi->counter_acc_qTcut[i_q]) << endl;
	}
      }
    }

    // introduce global 'active_optimization' variable to avoid weight output in normal (non-grid) runs !!!

    psi->step_optimization_complete();
    // to be implemented !!! better distinction for normal (non-grid) runs !!!
    // maybe psi->end_optimization = 0, 1, 2 ???

    psi->result_optimization_complete();

    logger << LOG_DEBUG_VERBOSE << "psi->MC_phasespace.end_optimization = " << psi->MC_phasespace.end_optimization << endl;

    output_weight_optimization(*psi, *osi, size_proc_generic, size_proceeding);

    logger << LOG_DEBUG_VERBOSE << "psi->MC_phasespace.end_optimization = " << psi->MC_phasespace.end_optimization << endl;

    // shifted:    psi->output_optimization_complete();


    /*
    if (!psi->end_optimization){psi->result_optimization_complete();}
    if (psi->end_optimization){psi->output_optimization_complete();}
    */
  /*
  psi->step_MCweight_optimization();
  ///  psi->random_manager.do_optimization_step(psi->i_acc);
  ///  psi->random_manager.do_optimization_step(psi->i_gen);
  psi->random_manager.do_optimization_step(*psi->i_step_mode);
  psi->result_MCweight_optimization();
  psi->step_tau_weight_optimization();
  psi->step_x1x2_weight_optimization();
  if (osi->csi->type_contribution == "CA" ||
      osi->csi->type_contribution == "RCA" ||
      osi->csi->type_contribution == "L2CA"){
    psi->step_z1z2_weight_optimization();
    // for CT, CT2 this is handled differently.
  }
  */

    ///    output_weight_optimization(*psi, *osi, size_proc_generic, size_proceeding);
    if (osi->switch_output_proceeding){perform_proceeding_step();}
    if (osi->switch_output_proceeding == 1){perform_proceeding_out();}

    psi->output_optimization_complete();

    psi->last_step_mode = *psi->i_step_mode;
  }

  if (*psi->i_step_mode == psi->n_events_max){osi->int_end = 1;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
