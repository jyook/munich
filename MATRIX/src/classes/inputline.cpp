#include "header.hpp"

inputline::inputline(){
  Logger logger("inputline::inputline ()");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  variable = "";
  specifier = "";
  value = "";
  valid = false;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


inputline::inputline(string _variable, string _value){
  Logger logger("inputline::inputline (variable, value)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  variable = _variable;
  specifier = "";
  value = _value;
  valid = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


inputline::inputline(string _variable, string _specifier, string _value){
  Logger logger("inputline::inputline (variable, specifier, value)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  variable = _variable;
  specifier = _specifier;
  value = _value;
  valid = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


inputline::inputline(string readin_line){
  Logger logger("inputline::inputline (readin_line)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int phase = 0;
  int start_variable = -1;
  int end_variable = -1;
  int start_value = -1;
  int end_value = -1;
  for (size_t i_i = 0; i_i < readin_line.size(); i_i++){
    if (phase == 0 && readin_line[i_i] != ' ' && readin_line[i_i] != char(9)){phase = 1; start_variable = i_i;}
    else if (phase == 1 && readin_line[i_i] == '='){phase = 2;}
    else if (phase == 2 && readin_line[i_i] != ' ' && readin_line[i_i] != char(9)){start_value = i_i; break;}
  }

  phase = 0;
  for (size_t i_i = 0; i_i < readin_line.size(); i_i++){
    size_t j_i = readin_line.size() - 1 - i_i;
    if (phase == 0 && readin_line[j_i] != ' ' && readin_line[j_i] != char(9) && readin_line[j_i] != '%'){phase = 1; end_value = j_i;}
    else if (phase == 1 && readin_line[j_i] == '%'){phase = 0; end_value = j_i;}
    else if (phase == 1 && readin_line[j_i] == '='){phase = 2;}
    else if (phase == 2 && readin_line[j_i] != ' ' && readin_line[j_i] != char(9)){end_variable = j_i; break;}
  }

  int start_specifier = -1;
  int end_specifier = -1;
  for (size_t i_i = start_variable; i_i < end_variable; i_i++){
    if (readin_line[i_i] == ' ' || readin_line[i_i] == char(9)){
      end_specifier = end_variable;
      end_variable = i_i - 1;
      for (size_t j_i = i_i + 1; j_i < end_specifier + 1; j_i++){
	if (readin_line[j_i] != ' ' && readin_line[j_i] != char(9)){start_specifier = j_i; break;}
      }
    }
  }

  if (start_variable != -1 && start_value != -1 && end_variable != -1 && end_value != -1){
    variable = readin_line.substr(start_variable, end_variable - start_variable + 1);
    value = readin_line.substr(start_value, end_value - start_value + 1);
    if (start_specifier != -1 && start_specifier != -1){specifier = readin_line.substr(start_specifier, end_specifier - start_specifier + 1);}
    else {specifier = "";}
    valid = true;
  }
  else {valid = false;}

  logger << LOG_DEBUG << left << setw(30) << variable << "   " << setw(15) << specifier << " = " << setw(30) << value << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

ostream & operator << (ostream &s, const inputline & input){
  s << left << setw(30) << input.variable << setw(20) << input.specifier <<  " = " << setw(50) << input.value;
  return s;
}

