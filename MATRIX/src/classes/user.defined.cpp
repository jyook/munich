#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
user_defined::user_defined(){
  switch_name.resize(1);
  switch_value.resize(1);
  //  switch_map;

  cut_name.resize(1);
  cut_value.resize(1);
  //  cut_map;

  int_name.resize(1);
  int_value.resize(1);
  //  int_map;

  double_name.resize(1);
  double_value.resize(1);
  //  double_map;

  string_name.resize(1);
  string_value.resize(1);
  //  string_map;

  //  particle_name.resize(1);
  //  particle_value.resize(1);
}


void user_defined::initialization(inputparameter_set * isi){
  static  Logger logger("user_defined::initialization");
  logger << LOG_DEBUG << "called" << endl;

  for (int i_i = 0; i_i < isi->input_userdefined.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_userdefined[" << i_i << "] = " << isi->input_userdefined[i_i] << endl;

    if (isi->input_userdefined[i_i].variable == ""){}

    ///////////////////////////////
    //  user-defined parameters  //
    ///////////////////////////////

    else if (isi->input_userdefined[i_i].variable == "user_switch"){
      if (switch_map[isi->input_userdefined[i_i].specifier] == 0){
	switch_name.push_back(isi->input_userdefined[i_i].specifier);
	switch_map[isi->input_userdefined[i_i].specifier] = switch_name.size() - 1;
	switch_value.push_back(atoi(isi->input_userdefined[i_i].value.c_str()));
      }
      else {
	switch_value[switch_map[isi->input_userdefined[i_i].specifier]] = atoi(isi->input_userdefined[i_i].value.c_str());
      }
    }

    else if (isi->input_userdefined[i_i].variable == "user_cut"){
      if (cut_map[isi->input_userdefined[i_i].specifier] == 0){
	cut_name.push_back(isi->input_userdefined[i_i].specifier);
	cut_map[isi->input_userdefined[i_i].specifier] = cut_name.size() - 1;
	cut_value.push_back(atof(isi->input_userdefined[i_i].value.c_str()));
      }
      else {
	cut_value[cut_map[isi->input_userdefined[i_i].specifier]] = atof(isi->input_userdefined[i_i].value.c_str());
      }
    }

    else if (isi->input_userdefined[i_i].variable == "user_int"){
      if (int_map[isi->input_userdefined[i_i].specifier] == 0){
	int_name.push_back(isi->input_userdefined[i_i].specifier);
	int_map[isi->input_userdefined[i_i].specifier] = int_name.size() - 1;
	int_value.push_back(atoi(isi->input_userdefined[i_i].value.c_str()));
      }
      else {
	int_value[int_map[isi->input_userdefined[i_i].specifier]] = atoi(isi->input_userdefined[i_i].value.c_str());
      }
    }

    else if (isi->input_userdefined[i_i].variable == "user_double"){
      if (double_map[isi->input_userdefined[i_i].specifier] == 0){
	double_name.push_back(isi->input_userdefined[i_i].specifier);
	double_map[isi->input_userdefined[i_i].specifier] = double_name.size() - 1;
	double_value.push_back(atof(isi->input_userdefined[i_i].value.c_str()));
      }
      else {
	double_value[double_map[isi->input_userdefined[i_i].specifier]] = atof(isi->input_userdefined[i_i].value.c_str());
      }
    }

    else if (isi->input_userdefined[i_i].variable == "user_string"){
      if (string_map[isi->input_userdefined[i_i].specifier] == 0){
	string_name.push_back(isi->input_userdefined[i_i].specifier);
	string_map[isi->input_userdefined[i_i].specifier] = string_name.size() - 1;
	string_value.push_back(isi->input_userdefined[i_i].value);
      }
      else {
	string_value[string_map[isi->input_userdefined[i_i].specifier]] = atof(isi->input_userdefined[i_i].value.c_str());
      }
    }

    if (isi->input_userdefined[i_i].variable == "user_particle"){
      if (particle_map[isi->input_userdefined[i_i].specifier] == 0){
	particle_name.push_back(isi->input_userdefined[i_i].specifier);
	particle_map[isi->input_userdefined[i_i].specifier] = particle_name.size() - 1;
	particle_value.push_back(isi->input_userdefined[i_i].value);
      }
      else {
	particle_value[particle_map[isi->input_userdefined[i_i].specifier]] = isi->input_userdefined[i_i].value;
      }
    }

  }

  logger << LOG_INFO << "finished" << endl;
}

