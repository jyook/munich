#include "header.hpp"

void inputparameter_set::select_input_input_from_userinput(){
  Logger logger("inputparameter_set::select_observable_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_input["output_level"] = true;
  assigned_to_input["type_perturbative_order"] = true;
  assigned_to_input["type_contribution"] = true;
  assigned_to_input["type_correction"] = true;
  // temporarily !!!
  assigned_to_input["LHAPDFname"] = true;
  assigned_to_input["LHAPDFsubset"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_event_input_from_userinput(){
  Logger logger("inputparameter_set::select_event_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_event["switch_output_cutinfo"] = true;
  // needed ???
  assigned_to_event["switch_testcut"] = true;

  ////////////////////////////////////////////////////
  //  basic event-selection criteria for particles  //
  ////////////////////////////////////////////////////

  assigned_to_event["n_observed_min"] = true;
  assigned_to_event["n_observed_max"] = true;
  assigned_to_event["define_pT"] = true;
  assigned_to_event["define_ET"] = true;
  assigned_to_event["define_eta"] = true;
  assigned_to_event["define_y"] = true;
  assigned_to_event["fiducial_cut"] = true;

  ///////////////////////////////////////
  //  photon-recombination parameters  //
  ///////////////////////////////////////

  assigned_to_event["photon_recombination"] = true;
  assigned_to_event["photon_R_definition"] = true;
  assigned_to_event["photon_R"] = true;
  assigned_to_event["photon_E_threshold_ratio"] = true;
  assigned_to_event["photon_jet_algorithm"] = true;
  assigned_to_event["photon_recombination_selection"] = true;
  assigned_to_event["photon_recombination_disable"] = true;
  assigned_to_event["photon_photon_recombination"] = true;
  assigned_to_event["photon_photon_recombination_R"] = true;

  ///////////////////////////////////
  //  photon-isolation parameters  //
  ///////////////////////////////////

  assigned_to_event["frixione_isolation"] = true;
  assigned_to_event["frixione_n"] = true;
  assigned_to_event["frixione_epsilon"] = true;
  assigned_to_event["frixione_fixed_ET_max"] = true;
  assigned_to_event["frixione_delta_0"] = true;
  assigned_to_event["frixione_jet_removal"] = true;

  ////////////////////////////////////////
  //  jet and jet-algorithm parameters  //
  ////////////////////////////////////////

  assigned_to_event["jet_algorithm"] = true;
  assigned_to_event["jet_R_definition"] = true;
  assigned_to_event["jet_R"] = true;
  assigned_to_event["parton_y_max"] = true;
  assigned_to_event["parton_eta_max"] = true;
  assigned_to_event["jet_algorithm_selection"] = true;
  assigned_to_event["jet_algorithm_disable"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_phasespace_input_from_userinput(){
  Logger logger("inputparameter_set::select_phasespace_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Required wherever contribution-dependent output needs to be selected !!!
  assigned_to_phasespace["type_perturbative_order"] = true;
  assigned_to_phasespace["type_contribution"] = true;
  assigned_to_phasespace["type_correction"] = true;

  assigned_to_phasespace["coll_choice"] = true;
  assigned_to_phasespace["E"] = true;

  assigned_to_phasespace["switch_off_random_generator"] = true;
  assigned_to_phasespace["switch_IO_generator"] = true;
  assigned_to_phasespace["zwahl"] = true;

  //////////////////////////////////////////
  //  new weight-optimization parameters  //
  //////////////////////////////////////////

  assigned_to_phasespace["MC"] = true;
  assigned_to_phasespace["IS"] = true;
  
  ///////////////////////////////////////////////
  //  selection of run_mode (grid, time, run)  //
  ///////////////////////////////////////////////

  assigned_to_phasespace["run_mode"] = true;

  // needed ???
  assigned_to_phasespace["switch_n_events_opt"] = true;

  //////////////////////////////////////
  //  weight-optimization parameters  //
  //////////////////////////////////////

  // needed ???
  assigned_to_phasespace["switch_use_alpha_after_IS"] = true;

  assigned_to_phasespace["switch_step_mode_grid"] = true;
  assigned_to_phasespace["switch_IS_mode_phasespace"] = true;
  assigned_to_phasespace["switch_output_weights"] = true;

  // needed ???
  assigned_to_phasespace["MCweight_in_directory"] = true;

  //////////////////////////////////////////////////////////////////////////////////////
  //  technical switches for selected phase space parametrization in RS contribution  //
  //////////////////////////////////////////////////////////////////////////////////////

  // for compatibility with old name of switch !!!
  assigned_to_phasespace["switch_RS_mapping"] = true;
  assigned_to_phasespace["switch_off_RS_mapping"] = true;
  assigned_to_phasespace["switch_off_RS_mapping_ij_k"] = true;
  assigned_to_phasespace["switch_off_RS_mapping_ij_a"] = true;
  assigned_to_phasespace["switch_off_RS_mapping_ai_k"] = true;
  assigned_to_phasespace["switch_off_RS_mapping_ai_b"] = true;

  /////////////////////////////////////////
  //  phase-space generation parameters  //
  /////////////////////////////////////////

  assigned_to_phasespace["nuxs"] = true;
  assigned_to_phasespace["nuxt"] = true;
  assigned_to_phasespace["exp_pdf"] = true;
  assigned_to_phasespace["exp_ij_k_y"] = true;
  assigned_to_phasespace["exp_ij_k_z"] = true;
  assigned_to_phasespace["exp_ij_a_x"] = true;
  assigned_to_phasespace["exp_ij_a_z"] = true;
  assigned_to_phasespace["exp_ai_k_x"] = true;
  assigned_to_phasespace["exp_ai_k_u"] = true;
  assigned_to_phasespace["exp_ai_b_x"] = true;
  assigned_to_phasespace["exp_ai_b_v"] = true;

  ////////////////////////////////////////
  //  technical integration parameters  //
  ////////////////////////////////////////

  assigned_to_phasespace["mass0"] = true;
  assigned_to_phasespace["map_technical_r"] = true;
  assigned_to_phasespace["map_technical_s"] = true;
  assigned_to_phasespace["map_technical_t"] = true;
  assigned_to_phasespace["map_technical_x"] = true;
  assigned_to_phasespace["cut_technical"] = true;

  /////////////////////////////////
  //  qT-resummation parameters  //
  /////////////////////////////////
  // not only in psi ???
  assigned_to_phasespace["switch_resummation"] = true;
  assigned_to_phasespace["switch_dynamic_Qres"] = true;
  assigned_to_phasespace["Qres"] = true;
  assigned_to_phasespace["Qres_prefactor"] = true;

  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////
  // not only in psi ???
  assigned_to_phasespace["switch_qTcut"] = true;
  assigned_to_phasespace["min_qTcut"] = true;

  //////////////////////////////
  //  integration parameters  //
  //////////////////////////////
  // not only in psi ???
  assigned_to_phasespace["n_events_max"] = true;
  assigned_to_phasespace["n_events_min"] = true;
  assigned_to_phasespace["n_step"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_contribution_input_from_userinput(){
  Logger logger("inputparameter_set::select_contribution_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_contribution["basic_process_class"] = true;
  assigned_to_contribution["subprocess"] = true;
  assigned_to_contribution["type_perturbative_order"] = true;
  assigned_to_contribution["type_contribution"] = true;
  assigned_to_contribution["type_correction"] = true;
  assigned_to_contribution["process_class"] = true;
  assigned_to_contribution["decay"] = true;
  assigned_to_contribution["contribution_order_alpha_s"] = true;
  assigned_to_contribution["contribution_order_alpha_e"] = true;
  assigned_to_contribution["contribution_order_interference"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_observable_input_from_userinput(){
  Logger logger("inputparameter_set::select_observable_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_observable["path_to_main"] = true;
  assigned_to_observable["ckm_choice"] = true;
  assigned_to_observable["n_moments"] = true;
  assigned_to_observable["test_output"] = true;

  ////////////////////////////////////////////////
  //  switches to steer calculation of results  //
  ////////////////////////////////////////////////

  assigned_to_observable["switch_distribution"] = true;
  assigned_to_observable["switch_moment"] = true;
  assigned_to_observable["switch_result"] = true;

  ///////////////////////////////////////////////
  //  switches to steer output of calculation  //
  ///////////////////////////////////////////////

  assigned_to_observable["switch_output_execution"] = true;
  assigned_to_observable["switch_output_integration"] = true;
  assigned_to_observable["switch_output_maxevent"] = true;
  assigned_to_observable["switch_output_cancellation_check"] = true;
  assigned_to_observable["switch_output_comparison"] = true;
  assigned_to_observable["switch_output_gnuplot"] = true;
  assigned_to_observable["switch_output_proceeding"] = true;
  // also in observable ???
  //  assigned_to_observable["switch_output_weights"] = true;
  assigned_to_observable["switch_output_result"] = true;
  assigned_to_observable["switch_output_moment"] = true;
  assigned_to_observable["switch_output_time"] = true;
  assigned_to_observable["switch_output_distribution"] = true;
  assigned_to_observable["switch_output_testpoint"] = true;

  assigned_to_observable["switch_console_output_runtime"] = true;
  assigned_to_observable["switch_console_output_tau_0"] = true;
  assigned_to_observable["switch_console_output_techcut_RA"] = true;
  assigned_to_observable["switch_console_output_phasespace_issue"] = true;
  assigned_to_observable["switch_console_output_ME2_issue"] = true;

  ////////////////////////////////////////////////////////////////////////
  //  unit of calculation output / result output / distribution output  //
  ////////////////////////////////////////////////////////////////////////

  assigned_to_observable["unit_calculation"] = true;
  assigned_to_observable["unit_result"] = true;
  assigned_to_observable["unit_distribution"] = true;

  ///////////////////////
  //  beam parameters  //
  ///////////////////////

  // maybe not only in psi !!!
  assigned_to_observable["E"] = true;
  // maybe not only in psi !!!
  assigned_to_observable["coll_choice"] = true;

  ///////////
  //  PDF  //
  ///////////

  assigned_to_observable["N_f_active"] = true;
  assigned_to_observable["pdf_selection"] = true;
  assigned_to_observable["pdf_disable"] = true;

  ////////////////////
  //  PDF - LHAPDF  //
  ////////////////////

  assigned_to_observable["LHAPDFname"] = true;
  assigned_to_observable["LHAPDFsubset"] = true;

  ////////////////////////////////////////////////////////
  //  input that still needs to be reasonably assigned  //
  ////////////////////////////////////////////////////////

  assigned_to_observable["N_f"] = true;
  // !! actually set by the Standard Model
  assigned_to_observable["N_quarks"] = true;

  // !! actually internal parameter
  assigned_to_observable["N_nondecoupled"] = true;


  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////

  // needed elsewhere ???
  assigned_to_observable["switch_qTcut"] = true;
  assigned_to_observable["n_qTcut"] = true;
  // needed elsewhere ???
  assigned_to_observable["min_qTcut"] = true;
  assigned_to_observable["step_qTcut"] = true;
  assigned_to_observable["max_qTcut"] = true;
  assigned_to_observable["binning_qTcut"] = true;
  assigned_to_observable["selection_qTcut"] = true;
  assigned_to_observable["selection_qTcut_distribution"] = true;
  assigned_to_observable["selection_no_qTcut_distribution"] = true;
  assigned_to_observable["selection_qTcut_result"] = true;
  assigned_to_observable["selection_no_qTcut_result"] = true;
  assigned_to_observable["selection_qTcut_integration"] = true;
  assigned_to_observable["selection_no_qTcut_integration"] = true;

  //////////////////////////////////////////
  //  N-jettiness-subtraction parameters  //
  //////////////////////////////////////////

  assigned_to_observable["switch_NJcut"] = true;
  assigned_to_observable["switch_NJcut_axes"] = true;
  assigned_to_observable["switch_NJcut_axes_energy"] = true;
  assigned_to_observable["switch_NJcut_measure"] = true;




  //////////////////////////////
  //  integration parameters  //
  //////////////////////////////

  assigned_to_observable["sigma_normalization"] = true;
  // to be removed...
  assigned_to_observable["sigma_LO"] = true;


  ///////////////////////////////
  //  scale-choice parameters  //
  ///////////////////////////////

  assigned_to_observable["scale_fact"] = true;
  assigned_to_observable["scale_ren"] = true;
  assigned_to_observable["dynamic_scale"] = true;
  assigned_to_observable["prefactor_reference"] = true;

  ///////////////////////////////////////
  //  scale variation parameters - CV  //
  ///////////////////////////////////////

  assigned_to_observable["switch_CV"] = true;
  assigned_to_observable["variation_CV"] = true;
  assigned_to_observable["n_scales_CV"] = true;
  assigned_to_observable["dynamic_scale_CV"] = true;
  assigned_to_observable["variation_mu_ren_CV"] = true;
  // ???

  assigned_to_observable["variation_mu_fact_CV"] = true;
  // ???

  assigned_to_observable["variation_factor_CV"] = true;
  assigned_to_observable["central_scale_CV"] = true;
  // ???

  assigned_to_observable["prefactor_CV"] = true;

  ////////////////////////////////////////
  //  scale variation parameters - TSV  //
  ////////////////////////////////////////

  assigned_to_observable["switch_TSV"] = true;
  assigned_to_observable["name_set_TSV"] = true;
  assigned_to_observable["scaleset"] = true;
  assigned_to_observable["central_scale_TSV"] = true;
  assigned_to_observable["central_scale_ren_TSV"] = true;
  assigned_to_observable["central_scale_fact_TSV"] = true;
  assigned_to_observable["relative_central_scale_TSV"] = true;
  assigned_to_observable["relative_central_scale_ren_TSV"] = true;
  assigned_to_observable["relative_central_scale_fact_TSV"] = true;
  assigned_to_observable["n_scale_TSV"] = true;
  assigned_to_observable["n_scale_ren_TSV"] = true;
  assigned_to_observable["n_scale_fact_TSV"] = true;
  assigned_to_observable["factor_scale_TSV"] = true;
  assigned_to_observable["factor_scale_ren_TSV"] = true;
  assigned_to_observable["factor_scale_fact_TSV"] = true;
  assigned_to_observable["dynamic_scale_TSV"] = true;
  assigned_to_observable["dynamic_scale_ren_TSV"] = true;
  assigned_to_observable["dynamic_scale_fact_TSV"] = true;
  assigned_to_observable["min_qTcut_TSV"] = true;
  assigned_to_observable["max_qTcut_TSV"] = true;
  assigned_to_observable["switch_distribution_TSV"] = true;
  assigned_to_observable["min_qTcut_distribution_TSV"] = true;
  assigned_to_observable["max_qTcut_distribution_TSV"] = true;
  assigned_to_observable["switch_moment_TSV"] = true;
  assigned_to_observable["name_diff_set_TSV"] = true;
  assigned_to_observable["name_diff_set_plus_TSV"] = true;
  assigned_to_observable["name_diff_set_minus_TSV"] = true;

  assigned_to_observable["switch_reference"] = true;
  assigned_to_observable["name_reference_TSV"] = true;
  assigned_to_observable["no_reference_TSV"] = true;
  assigned_to_observable["no_scale_ren_reference_TSV"] = true;
  assigned_to_observable["no_scale_fact_reference_TSV"] = true;
  assigned_to_observable["no_qTcut_reference_TSV"] = true;




  ////////////////////////////////////////////////////
  //  technical switches for selected contributions //
  ////////////////////////////////////////////////////

  assigned_to_observable["switch_polenorm"] = true;
  assigned_to_observable["switch_old_qT_version"] = true;


  //////////////////////////////////////////////////////////////
  //  selection of process and contribution to be calculated  //
  //////////////////////////////////////////////////////////////

  assigned_to_observable["type_perturbative_order"] = true;
  assigned_to_observable["type_contribution"] = true;
  assigned_to_observable["type_correction"] = true;
  assigned_to_observable["switch_KP"] = true;
  assigned_to_observable["switch_VI"] = true;
  assigned_to_observable["switch_VI_bosonic_fermionic"] = true;
  assigned_to_observable["switch_H1gg"] = true;
  assigned_to_observable["switch_H2"] = true;

  assigned_to_observable["switch_CM"] = true;
  assigned_to_observable["switch_OL"] = true;
  assigned_to_observable["switch_RCL"] = true;
  assigned_to_observable["switch_RS"] = true;

  /////////////////////////////////
  //  qT-resummation parameters  //
  /////////////////////////////////

  // All qT-resummation parameters: not only in psi ???
  assigned_to_observable["switch_resummation"] = true;
  assigned_to_observable["switch_dynamic_Qres"] = true;
  assigned_to_observable["Qres"] = true;
  assigned_to_observable["Qres_prefactor"] = true;


  assigned_to_observable["switch_yuk"] = true;
  assigned_to_observable["order_y"] = true;

  //////////////////////////////
  //  integration parameters  //
  //////////////////////////////

  assigned_to_observable["sigma_normalization_deviation"] = true;
  assigned_to_observable["sigma_LO_deviation"] = true;
  // next three not only in psi ???
  assigned_to_observable["n_events_max"] = true;
  assigned_to_observable["n_events_min"] = true;
  assigned_to_observable["n_step"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_amplitude_input_from_userinput(){
  Logger logger("inputparameter_set::select_amplitude_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_amplitude["N_quarks"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_amplitude_OpenLoops_input_from_userinput(){
  Logger logger("inputparameter_set::select_amplitude_OpenLoops_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_amplitude_OpenLoops["OL"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_amplitude_Recola_input_from_userinput(){
  Logger logger("inputparameter_set::select_amplitude_Recola_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_amplitude_Recola["RCL"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_userdefined_input_from_userinput(){
  Logger logger("inputparameter_set::select_userdefined_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ///////////////////////////////
  //  user-defined parameters  //
  ///////////////////////////////

  assigned_to_userdefined["user_switch"] = true;
  assigned_to_userdefined["user_cut"] = true;
  assigned_to_userdefined["user_int"] = true;
  assigned_to_userdefined["user_double"] = true;
  assigned_to_userdefined["user_string"] = true;
  assigned_to_userdefined["user_particle"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_leftover_input_from_userinput(){
  Logger logger("inputparameter_set::select_leftover_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_model_input_from_userinput(){
  Logger logger("inputparameter_set::select_model_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  assigned_to_model["contribution"] = true;
  //  assigned_to_model["N_quarks"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_distribution_input_from_userinput(){
  Logger logger("inputparameter_set::select_distribution_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  assigned_to_distribution["N_quarks"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void inputparameter_set::select_dddistribution_input_from_userinput(){
  Logger logger("inputparameter_set::select_dddistribution_input_from_userinput");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  assigned_to_dddistribution["N_quarks"] = true;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


