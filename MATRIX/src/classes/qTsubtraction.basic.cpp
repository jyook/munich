#include "header.hpp"
//#include "definitions.phasespace.set.cxx"

////////////////////
//  constructors  //
////////////////////

qTsubtraction_basic::qTsubtraction_basic(){}

/*
qTsubtraction_basic::qTsubtraction_basic(observable_set & osi){
  switch_qTcut = osi.switch_qTcut;
  n_qTcut = osi.n_qTcut;
  output_n_qTcut = osi.output_n_qTcut;
  min_qTcut = osi. min_qTcut;
  step_qTcut = osi.step_qTcut;
  max_qTcut = osi.max_qTcut;
  binning_qTcut = osi.binning_qTcut;
  selection_qTcut = osi.selection_qTcut;

  value_qTcut = osi.value_qTcut;

  selection_qTcut_distribution = osi.selection_qTcut_distribution;
  selection_no_qTcut_distribution = osi.selection_no_qTcut_distribution;
  no_qTcut_distribution = osi.no_qTcut_distribution;
  value_qTcut_distribution = osi.value_qTcut_distribution;

  selection_qTcut_result = osi.selection_qTcut_result;
  selection_no_qTcut_result = osi.selection_no_qTcut_result;
  no_qTcut_result = osi.no_qTcut_result;
  value_qTcut_result = osi.value_qTcut_result;

  selection_qTcut_integration = osi.selection_qTcut_integration;
  selection_no_qTcut_integration = osi.selection_no_qTcut_integration;
  no_qTcut_integration = osi.no_qTcut_integration;
  value_qTcut_integration = osi.value_qTcut_integration;

  counter_killed_qTcut = osi.counter_killed_qTcut;
  counter_acc_qTcut = osi.counter_acc_qTcut;

  QT_Q = nullvector;
  QT_QT = 0.;
  QT_sqrtQ2 = 0.;

}
*/



qTsubtraction_basic::qTsubtraction_basic(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi){
  static Logger logger("qTsubtraction_basic::initialization_qTcut (isi, csi, esi, psi)");
  logger << LOG_DEBUG << "called" << endl;

  initialization(isi, _csi, _esi, _psi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void qTsubtraction_basic::initialization(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi){
  static Logger logger("qTsubtraction_basic::initialization");
  logger << LOG_DEBUG << "called" << endl;

  csi = _csi;
  esi = _esi;
  psi = _psi;

  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void qTsubtraction_basic::initialization_set_default(){
  static  Logger logger("qTsubtraction_basic::initialization_set_default");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////

  switch_qTcut = 0;
  n_qTcut = 0;
  min_qTcut = 0.;
  step_qTcut = 0.;
  max_qTcut = 0.;
  binning_qTcut = "linear";
  selection_qTcut = "";

  selection_qTcut_distribution = "";
  selection_no_qTcut_distribution = "";
  selection_qTcut_result = "";
  selection_no_qTcut_result = "";
  selection_qTcut_integration = "";
  selection_no_qTcut_integration = "";
  //  value_qTcut;
  // !!! no input parameter !!!
  //  no_qTcut_distribution;
  // !!! no input parameter !!!
  //  value_qTcut_distribution;
  // !!! no input parameter !!!


  QT_Q = nullvector;
  QT_QT = 0.;
  QT_sqrtQ2 = 0.;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void qTsubtraction_basic::initialization_input(inputparameter_set * isi){
  static  Logger logger("qTsubtraction_basic::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_observable[" << i_i << "] = " << isi->input_observable[i_i] << endl;

    if (isi->input_observable[i_i].variable == ""){}

   /////////////////////////////////
    //  qT-subtraction parameters  //
    /////////////////////////////////

    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "switch_qTcut"){switch_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_qTcut"){n_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "min_qTcut"){min_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "step_qTcut"){step_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "max_qTcut"){max_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "binning_qTcut"){binning_qTcut = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "selection_qTcut"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut = "";}
      else {selection_qTcut = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_distribution = "";}
      else {selection_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_distribution = "";}
      else {selection_no_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_result = "";}
      else {selection_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_result = "";}
      else {selection_no_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_integration = "";}
      else {selection_qTcut_integration = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_integration = "";}
      else {selection_no_qTcut_integration = isi->input_observable[i_i].value;}
    }
  }


  /*
    switch_qTcut = isi.switch_qTcut;
  n_qTcut = isi.n_qTcut;
  min_qTcut = isi.min_qTcut;
  max_qTcut = isi.max_qTcut;
  step_qTcut = isi.step_qTcut;
  binning_qTcut = isi.binning_qTcut;
  selection_qTcut = isi.selection_qTcut;
  selection_qTcut_distribution = isi.selection_qTcut_distribution;
  selection_no_qTcut_distribution = isi.selection_no_qTcut_distribution;
  selection_qTcut_result = isi.selection_qTcut_result;
  selection_no_qTcut_result = isi.selection_no_qTcut_result;
  selection_qTcut_integration = isi.selection_qTcut_integration;
  selection_no_qTcut_integration = isi.selection_no_qTcut_integration;
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void qTsubtraction_basic::initialization_after_input(){
  static  Logger logger("qTsubtraction_basic::initialization_after_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << setw(40) << "switch_qTcut" << "     =     " << setw(20) << switch_qTcut << endl;

  if (!switch_qTcut){
    n_qTcut = 1;
    min_qTcut = 0;
    step_qTcut = 0;
    max_qTcut = 0;
    value_qTcut = vector<double> (1, 0.);
    logger << LOG_INFO << "No qTcut variation applied!" << endl;
    logger << LOG_INFO << setw(40) << "switch_qTcut" << "     =     " << setw(20) << switch_qTcut << endl;
    logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
    logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
    logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
    logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
    logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
    logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;

  }
  else {
    if (switch_qTcut && !n_qTcut){exit(1);}
    //  value_qTcut = isi.value_qTcut;

    if (binning_qTcut == "linear"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, step_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	double temp_max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
	if (max_qTcut != temp_max_qTcut){logger << LOG_FATAL << "Inconsistently over-defined qTcut input!" << endl; exit(1);}
      }
      // min_qTcut, n_qTcut, step_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0.){
	max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
      }
      // min_qTcut, n_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = (max_qTcut - min_qTcut) / (n_qTcut - 1);
      }
      // min_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && step_qTcut != 0. && max_qTcut != 0.){
	n_qTcut = (max_qTcut - min_qTcut) / step_qTcut + 1;
      }
      // n_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	min_qTcut = max_qTcut - (n_qTcut - 1) * step_qTcut;
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for linear binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut + i_q * step_qTcut);
      }
    }

    else if (binning_qTcut == "logarithmic"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = 0.; // no constant step width in logarithmic binning!
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut * exp10(log10(max_qTcut / min_qTcut) * double(i_q) / (n_qTcut - 1)));
      }
      value_qTcut[n_qTcut - 1] = max_qTcut;
    }

    else if (binning_qTcut == "irregular"){
      // possible ways to set input parameters:

      // selection_qTcut
      if (selection_qTcut != ""){
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(selection_qTcut[i_b]);}
	}
	n_qTcut = vs_selection.size();
	value_qTcut.resize(n_qTcut);
	for (int i_b = 0; i_b < value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	}
	min_qTcut = value_qTcut[0];
	max_qTcut = value_qTcut[n_qTcut - 1];
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
    }
  }

  logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
  logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
  logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
  logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
  logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
  logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;

  logger << LOG_INFO << setw(40) << "value_qTcut.size()" << "     =     " << setw(20) << value_qTcut.size() << endl;
  for (int i_b = 0; i_b < value_qTcut.size(); i_b++){logger << LOG_DEBUG << "value_qTcut[" << setw(3) << i_b << "] = " << value_qTcut[i_b] << endl;}


  logger << LOG_INFO << "csi->type_contribution = " << csi->type_contribution << endl;

  if (csi->type_contribution == "" ||
      csi->type_contribution == "all" ||
      csi->type_contribution == "CT" ||
      csi->type_contribution == "CJ" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "CJ2" ||
      csi->type_contribution == "RVA" ||
      csi->type_contribution == "RCA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2CT"){
    active_qTcut = 1;
    output_n_qTcut = n_qTcut;
  }
  else {
    active_qTcut = 0;
    output_n_qTcut = 1;
  }

  logger << LOG_INFO << "output_n_qTcut = " << output_n_qTcut << endl;
  logger << LOG_INFO << "active_qTcut = " << active_qTcut << endl;
  logger << LOG_INFO << "selection_qTcut_distribution =    " << selection_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_no_qTcut_distribution = " << selection_no_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_qTcut_result =    " << selection_qTcut_result << endl;
  logger << LOG_INFO << "selection_no_qTcut_result = " << selection_no_qTcut_result << endl;
  logger << LOG_INFO << "selection_qTcut_integration =    " << selection_qTcut_integration << endl;
  logger << LOG_INFO << "selection_no_qTcut_integration = " << selection_no_qTcut_integration << endl;

  for (int i_m = 0; i_m < 3; i_m++){
    string temp_name;
    string temp_selection_qTcut;
    string temp_selection_no_qTcut;
    vector<int> temp_no_qTcut;
    vector<double> temp_value_qTcut;
    if (i_m == 0){temp_selection_qTcut = selection_qTcut_distribution; temp_selection_no_qTcut = selection_no_qTcut_distribution; temp_name = "distribution";}
    else if (i_m == 1){temp_selection_qTcut = selection_qTcut_result; temp_selection_no_qTcut = selection_no_qTcut_result; temp_name = "result";}
    else if (i_m == 2){temp_selection_qTcut = selection_qTcut_integration; temp_selection_no_qTcut = selection_no_qTcut_integration; temp_name = "integration";}
    else {logger << LOG_ERROR << "Wrong entry in selection_qTcut!" << endl;}

    logger << LOG_DEBUG << "i_m = " << i_m << "   temp_selection_qTcut = " << temp_selection_qTcut << endl;
    logger << LOG_DEBUG << "i_m = " << i_m << "   temp_selection_no_qTcut = " << temp_selection_no_qTcut << endl;

    if (active_qTcut){
      if (temp_selection_qTcut == "" && temp_selection_no_qTcut == ""){
	temp_value_qTcut.resize(1, 0.);
	temp_no_qTcut.resize(1, 0);
	logger << LOG_INFO << temp_name << " output generated for lowest qTcut value." << endl;
      }
      else if (temp_selection_qTcut == "all" || temp_selection_no_qTcut == "all"){
	temp_value_qTcut = value_qTcut;
	temp_no_qTcut.resize(n_qTcut);
	for (int i_q = 0; i_q < n_qTcut; i_q++){temp_no_qTcut[i_q] = i_q;}
	logger << LOG_INFO << temp_name << " output generated for all qTcut values." << endl;
      }
      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut == ""){
	logger << LOG_INFO << temp_name << " output generated for selected qTcut values." << endl;

	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	  int flag = n_qTcut;
	  for (int i_q = 0; i_q < n_qTcut; i_q++){
	    if (abs(temp_value_qTcut[i_b] - value_qTcut[i_q]) < 1.e-12 * value_qTcut[i_q]){flag = i_q; break;}
	  }
	  if (flag == n_qTcut){temp_value_qTcut.erase(temp_value_qTcut.begin() + i_b); i_b--;}
	  else {temp_no_qTcut[i_b] = flag;}
	}
      }

      else if (temp_selection_qTcut == "" && temp_selection_no_qTcut != ""){
	logger << LOG_INFO << temp_name << " output generated for selected no_qTcut values." << endl;
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_no_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_no_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_no_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_no_qTcut[i_b] = atoi(vs_selection[i_b].c_str());
	  temp_value_qTcut[i_b] = value_qTcut[temp_no_qTcut[i_b]];
	}
      }

      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut != ""){
	logger << LOG_FATAL << "Inconsistent qTcut input for " << temp_name << "!" << endl;
	exit(1);
      }

      logger << LOG_DEBUG << temp_name << "   temp_value_qTcut.size() = " << temp_value_qTcut.size() << endl;
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
    }

    else {
      temp_value_qTcut.resize(1, 0.);
      temp_no_qTcut.resize(1, 0);
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
    }
    if (i_m == 0){no_qTcut_distribution = temp_no_qTcut; value_qTcut_distribution = temp_value_qTcut;}
    else if (i_m == 1){no_qTcut_result = temp_no_qTcut; value_qTcut_result = temp_value_qTcut;}
    else if (i_m == 2){no_qTcut_integration = temp_no_qTcut; value_qTcut_integration = temp_value_qTcut;}

    //    logger << LOG_INFO << "temp_name = " << temp_name << endl;
    //    logger << LOG_INFO << "temp_selection_qTcut = " << temp_selection_qTcut << endl;
  }

  if (csi->type_contribution == "RVA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2RJ"){
    counter_killed_qTcut.resize(n_qTcut, 0);
    counter_acc_qTcut.resize(n_qTcut, 0);
  }

  logger << LOG_DEBUG << "finished" << endl;
}








void qTsubtraction_basic::output(){
  static Logger logger("qTsubtraction_basic::output");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "&csi - csi = " << csi << endl;
  logger << LOG_DEBUG << "csi->n_particle_born = " << csi->n_particle_born << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
