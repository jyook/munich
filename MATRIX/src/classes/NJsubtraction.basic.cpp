#include "header.hpp"
//#include "definitions.phasespace.set.cxx"

////////////////////
//  constructors  //
////////////////////


NJsubtraction_basic::NJsubtraction_basic(){}

NJsubtraction_basic::NJsubtraction_basic(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi){
  static Logger logger("NJsubtraction_basic::initialization_NJcut (isi, csi, esi, psi)");
  logger << LOG_DEBUG << "called" << endl;

  initialization(isi, _csi, _esi, _psi);

  logger << LOG_DEBUG << "finished" << endl;
}

void NJsubtraction_basic::initialization(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi){
  static Logger logger("NJsubtraction_basic::initialization_NJcut (isi, csi)");
  logger << LOG_DEBUG << "called" << endl;

  csi = _csi;
  esi = _esi;
  psi = _psi;

  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void NJsubtraction_basic::initialization_set_default(){
  static  Logger logger("NJsubtraction_basic::initialization_set_default");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////

  switch_qTcut = 0;
  n_qTcut = 0;
  min_qTcut = 0.;
  step_qTcut = 0.;
  max_qTcut = 0.;
  binning_qTcut = "linear";
  selection_qTcut = "";

  selection_qTcut_distribution = "";
  selection_no_qTcut_distribution = "";
  selection_qTcut_result = "";
  selection_no_qTcut_result = "";
  selection_qTcut_integration = "";
  selection_no_qTcut_integration = "";
  //  value_qTcut;
  // !!! no input parameter !!!
  //  no_qTcut_distribution;
  // !!! no input parameter !!!
  //  value_qTcut_distribution;
  // !!! no input parameter !!!

  /*
  QT_Q = nullvector;
  QT_QT = 0.;
  QT_sqrtQ2 = 0.;
  */
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// Need to find a reasonable solution for these parameters (if NJ subtraction is ever added...)
void NJsubtraction_basic::initialization_input(inputparameter_set * isi){
  static  Logger logger("NJsubtraction_basic::initialization_input");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_observable[" << i_i << "] = " << isi->input_observable[i_i] << endl;

    if (isi->input_observable[i_i].variable == ""){}

   /////////////////////////////////
    //  NJ-subtraction parameters  //
    /////////////////////////////////

    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "switch_qTcut"){switch_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_qTcut"){n_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "min_qTcut"){min_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "step_qTcut"){step_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "max_qTcut"){max_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "binning_qTcut"){binning_qTcut = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "selection_qTcut"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut = "";}
      else {selection_qTcut = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_distribution = "";}
      else {selection_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_distribution = "";}
      else {selection_no_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_result = "";}
      else {selection_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_result = "";}
      else {selection_no_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_integration = "";}
      else {selection_qTcut_integration = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_integration = "";}
      else {selection_no_qTcut_integration = isi->input_observable[i_i].value;}
    }
  }


  /*
    switch_qTcut = isi.switch_qTcut;
  n_qTcut = isi.n_qTcut;
  min_qTcut = isi.min_qTcut;
  max_qTcut = isi.max_qTcut;
  step_qTcut = isi.step_qTcut;
  binning_qTcut = isi.binning_qTcut;
  selection_qTcut = isi.selection_qTcut;
  selection_qTcut_distribution = isi.selection_qTcut_distribution;
  selection_no_qTcut_distribution = isi.selection_no_qTcut_distribution;
  selection_qTcut_result = isi.selection_qTcut_result;
  selection_no_qTcut_result = isi.selection_no_qTcut_result;
  selection_qTcut_integration = isi.selection_qTcut_integration;
  selection_no_qTcut_integration = isi.selection_no_qTcut_integration;
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
NJsubtraction_basic::NJsubtraction_basic(observable_set & osi){

  csi = osi.csi;


  switch_qTcut = osi.switch_qTcut;
  n_qTcut = osi.n_qTcut;
  min_qTcut = osi. min_qTcut;
  step_qTcut = osi.step_qTcut;
  max_qTcut = osi.max_qTcut;
  binning_qTcut = osi.binning_qTcut;
  selection_qTcut = osi.selection_qTcut;

  value_qTcut = osi.value_qTcut;

  selection_qTcut_distribution = osi.selection_qTcut_distribution;
  selection_no_qTcut_distribution = osi.selection_no_qTcut_distribution;
  no_qTcut_distribution = osi.no_qTcut_distribution;
  value_qTcut_distribution = osi.value_qTcut_distribution;

  selection_qTcut_result = osi.selection_qTcut_result;
  selection_no_qTcut_result = osi.selection_no_qTcut_result;
  no_qTcut_result = osi.no_qTcut_result;
  value_qTcut_result = osi.value_qTcut_result;

  selection_qTcut_integration = osi.selection_qTcut_integration;
  selection_no_qTcut_integration = osi.selection_no_qTcut_integration;
  no_qTcut_integration = osi.no_qTcut_integration;
  value_qTcut_integration = osi.value_qTcut_integration;

  counter_killed_qTcut = osi.counter_killed_qTcut;
  counter_acc_qTcut = osi.counter_acc_qTcut;

  /*
  QT_Q = nullvector;
  QT_QT = 0.;
  QT_sqrtQ2 = 0.;
*//*



}

*/

void NJsubtraction_basic::initialization_after_input(){
  static  Logger logger("NJsubtraction_basic::initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_INFO << setw(40) << "switch_qTcut" << "     =     " << setw(20) << switch_qTcut << endl;

  if (!switch_qTcut){
    n_qTcut = 1;
    min_qTcut = 0;
    step_qTcut = 0;
    max_qTcut = 0;
    value_qTcut = vector<double> (1, 0.);
    logger << LOG_INFO << "No qTcut variation applied!" << endl;
    logger << LOG_INFO << setw(40) << "switch_qTcut" << "     =     " << setw(20) << switch_qTcut << endl;
    logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
    logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
    logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
    logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
    logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
    logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;

  }
  else {
    if (switch_qTcut && !n_qTcut){exit(1);}
    //  value_qTcut = isi.value_qTcut;

    if (binning_qTcut == "linear"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, step_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	double temp_max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
	if (max_qTcut != temp_max_qTcut){logger << LOG_FATAL << "Inconsistently over-defined qTcut input!" << endl; exit(1);}
      }
      // min_qTcut, n_qTcut, step_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0.){
	max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
      }
      // min_qTcut, n_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = (max_qTcut - min_qTcut) / (n_qTcut - 1);
      }
      // min_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && step_qTcut != 0. && max_qTcut != 0.){
	n_qTcut = (max_qTcut - min_qTcut) / step_qTcut + 1;
      }
      // n_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	min_qTcut = max_qTcut - (n_qTcut - 1) * step_qTcut;
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for linear binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut + i_q * step_qTcut);
      }
    }

    else if (binning_qTcut == "logarithmic"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = 0.; // no constant step width in logarithmic binning!
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut * exp10(log10(max_qTcut / min_qTcut) * double(i_q) / (n_qTcut - 1)));
      }
      value_qTcut[n_qTcut - 1] = max_qTcut;
    }

    else if (binning_qTcut == "irregular"){
      // possible ways to set input parameters:

      // selection_qTcut
      if (selection_qTcut != ""){
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(selection_qTcut[i_b]);}
	}
	n_qTcut = vs_selection.size();
	value_qTcut.resize(n_qTcut);
	for (int i_b = 0; i_b < value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	}
	min_qTcut = value_qTcut[0];
	max_qTcut = value_qTcut[n_qTcut - 1];
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
    }
  }

  logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
  logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
  logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
  logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
  logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
  logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;
  for (int i_b = 0; i_b < value_qTcut.size(); i_b++){
    logger << LOG_INFO << "value_qTcut[" << setw(3) << i_b << "] = " << value_qTcut[i_b] << endl;
  }


  logger << LOG_INFO << "csi->type_contribution = " << csi->type_contribution << endl;

  if (csi->type_contribution == "" ||
      csi->type_contribution == "all" ||
      csi->type_contribution == "CT" ||
      csi->type_contribution == "CJ" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "CJ2" ||
      csi->type_contribution == "RVA" ||
      csi->type_contribution == "RCA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2CT"){
    active_qTcut = 1;
    output_n_qTcut = n_qTcut;
  }
  else {
    active_qTcut = 0;
    output_n_qTcut = 1;
  }

  logger << LOG_INFO << "output_n_qTcut = " << output_n_qTcut << endl;
  logger << LOG_INFO << "active_qTcut = " << active_qTcut << endl;
  logger << LOG_INFO << "selection_qTcut_distribution = " << selection_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_no_qTcut_distribution = " << selection_no_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_qTcut_result = " << selection_qTcut_result << endl;
  logger << LOG_INFO << "selection_no_qTcut_result = " << selection_no_qTcut_result << endl;
  logger << LOG_INFO << "selection_qTcut_integration = " << selection_qTcut_integration << endl;
  logger << LOG_INFO << "selection_no_qTcut_integration = " << selection_no_qTcut_integration << endl;

  for (int i_m = 0; i_m < 3; i_m++){
    string temp_name;
    string temp_selection_qTcut;
    string temp_selection_no_qTcut;
    vector<int> temp_no_qTcut;
    vector<double> temp_value_qTcut;
    if (i_m == 0){temp_selection_qTcut = selection_qTcut_distribution; temp_selection_no_qTcut = selection_no_qTcut_distribution; temp_name = "distribution";}
    else if (i_m == 1){temp_selection_qTcut = selection_qTcut_result; temp_selection_no_qTcut = selection_no_qTcut_result; temp_name = "result";}
    else if (i_m == 2){temp_selection_qTcut = selection_qTcut_integration; temp_selection_no_qTcut = selection_no_qTcut_integration; temp_name = "integration";}
    else {logger << LOG_ERROR << "Wrong entry in selection_qTcut!" << endl;}

    logger << LOG_INFO << "i_m = " << i_m << "   temp_selection_qTcut = " << temp_selection_qTcut << "   temp_selection_no_qTcut = " << temp_selection_no_qTcut << endl;

    if (active_qTcut){
      if (temp_selection_qTcut == "" && temp_selection_no_qTcut == ""){
	temp_value_qTcut.resize(1, 0.);
	temp_no_qTcut.resize(1, 0);
	logger << LOG_INFO << temp_name << " output generated for lowest qTcut value." << endl;
      }
      else if (temp_selection_qTcut == "all" || temp_selection_no_qTcut == "all"){
	temp_value_qTcut = value_qTcut;
	temp_no_qTcut.resize(n_qTcut);
	for (int i_q = 0; i_q < n_qTcut; i_q++){temp_no_qTcut[i_q] = i_q;}
	logger << LOG_INFO << temp_name << " output generated for all qTcut values." << endl;
      }
      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut == ""){
	logger << LOG_INFO << temp_name << " output generated for selected qTcut values." << endl;

	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	  int flag = n_qTcut;
	  for (int i_q = 0; i_q < n_qTcut; i_q++){
	    if (abs(temp_value_qTcut[i_b] - value_qTcut[i_q]) < 1.e-12 * value_qTcut[i_q]){flag = i_q; break;}
	  }
	  if (flag == n_qTcut){temp_value_qTcut.erase(temp_value_qTcut.begin() + i_b); i_b--;}
	  else {temp_no_qTcut[i_b] = flag;}
	}
      }

      else if (temp_selection_qTcut == "" && temp_selection_no_qTcut != ""){
	logger << LOG_INFO << temp_name << " output generated for selected no_qTcut values." << endl;
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_no_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_no_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_no_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_no_qTcut[i_b] = atoi(vs_selection[i_b].c_str());
	  temp_value_qTcut[i_b] = value_qTcut[temp_no_qTcut[i_b]];
	}
      }

      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut != ""){
	logger << LOG_FATAL << "Inconsistent qTcut input for " << temp_name << "!" << endl;
	exit(1);
      }

      logger << LOG_DEBUG << temp_name << "   temp_value_qTcut.size() = " << temp_value_qTcut.size() << endl;
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
    }

    else {
      temp_value_qTcut.resize(1, 0.);
      temp_no_qTcut.resize(1, 0);
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
    }
    if (i_m == 0){no_qTcut_distribution = temp_no_qTcut; value_qTcut_distribution = temp_value_qTcut;}
    else if (i_m == 1){no_qTcut_result = temp_no_qTcut; value_qTcut_result = temp_value_qTcut;}
    else if (i_m == 2){no_qTcut_integration = temp_no_qTcut; value_qTcut_integration = temp_value_qTcut;}

    //    logger << LOG_INFO << "temp_name = " << temp_name << endl;
    //    logger << LOG_INFO << "temp_selection_qTcut = " << temp_selection_qTcut << endl;
  }

  if (csi->type_contribution == "RVA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2RJ"){
    counter_killed_qTcut.resize(n_qTcut, 0);
    counter_acc_qTcut.resize(n_qTcut, 0);
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void NJsubtraction_basic::output(){
  static Logger logger("NJsubtraction_basic::output");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "&csi - csi = " << csi << endl;
  logger << LOG_DEBUG << "csi->n_particle_born = " << csi->n_particle_born << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void NJsubtraction_basic::Njettiness_calculate_NJ_axes(int i_a){
  static Logger logger("NJsubtraction_basic::Njettiness_calculate_NJ_axes");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // NJ_q_axes are always defined in the hadronic frame with three options:
  // switch_NJcut_axes == 0:   axes defined via jet algorithm (according to input)
  // switch_NJcut_axes == 1:   axes defined via partitioning using 1-jettiness

  NJ_q_axes[1] = fourvector(psi->x_pdf[1] * psi->E, 0., 0., psi->x_pdf[1] * psi->E);
  NJ_q_axes[2] = fourvector(psi->x_pdf[2] * psi->E, 0., 0., -psi->x_pdf[2] * psi->E);

  NJ_n_axes[1] = fourvector(1., 0., 0., 1.);
  NJ_n_axes[2] = fourvector(1., 0., 0., -1.);

  NJ_Ei[1] = psi->x_pdf[1] * psi->E;
  NJ_Ei[2] = psi->x_pdf[2] * psi->E;


  if (switch_NJcut_axes == 0){
    for (int i_j = 0; i_j < csi->n_jet_born; i_j++){
      // use first n_jet_born results from the applied jet algorithm here for now...
      // esi->particle_event -> hadronic centre-of-mass system
      fourvector temp_jet = esi->particle_event[esi->access_object["jet"]][i_a][i_j].momentum;
      NJ_n_axes[3 + i_j] = (1. / temp_jet.r()) * fourvector(temp_jet.r(), temp_jet.x1(), temp_jet.x2(), temp_jet.x3());
      // use different normalizations of (light-like) momentum (Eq. (2.6)):
      NJ_Ei[3 + i_j] = Njettiness_calculate_NJ_axes_assigned_energy(temp_jet);
      NJ_q_axes[3 + i_j] = NJ_Ei[3 + i_j] * NJ_n_axes[3 + i_j];
    }
  }
  else if (switch_NJcut_axes == 1){
   //
    int temp_order = esi->ps_runtime_jet_algorithm[i_a].size() - csi->n_jet_born;
    logger << LOG_DEBUG << "temp_order = " << temp_order << endl;
    if (temp_order == 1){// NLO
      if (csi->n_jet_born == 1){// only for 1-jettiness
	vector<double> tau_1_NLO(3, 0.);
	vector<fourvector> parton(esi->ps_runtime_jet_algorithm[i_a].size());
	for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
	  parton[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum;
	}
	tau_1_NLO[0] = parton[0].r() - abs(parton[0].x3());
	tau_1_NLO[1] = parton[1].r() - abs(parton[1].x3());
	tau_1_NLO[2] = parton[0].r() + parton[1].r() - (parton[0] + parton[1]).r();
	for (int i = 0; i < 3; i++){
	  logger << LOG_DEBUG << "tau_1_NLO[" << i << "] = " << tau_1_NLO[i] << endl;
	}

	int min_index = -1;
	double min = 1.e99;
	for (int i_x = 0; i_x < tau_1_NLO.size(); i_x++){
	  if (tau_1_NLO[i_x] < min){min = tau_1_NLO[i_x]; min_index = i_x;}
	}

	for (int i_j = 0; i_j < csi->n_jet_born; i_j++){
	  fourvector temp_jet;
	  if (min_index == 0 || min_index == 1){temp_jet = parton[min_index];}
	  else {temp_jet = parton[0] + parton[1];}
	  NJ_n_axes[3 + i_j] = (1. / temp_jet.r()) * fourvector(temp_jet.r(), temp_jet.x1(), temp_jet.x2(), temp_jet.x3());
	  // use different normalizations of (light-like) momentum (Eq. (2.6)):
	  NJ_Ei[3 + i_j] = Njettiness_calculate_NJ_axes_assigned_energy(temp_jet);
	  NJ_q_axes[3 + i_j] = NJ_Ei[3 + i_j] * NJ_n_axes[3 + i_j];
	  /*
	  // use different normalizations of (light-like) momentum (Eq. (2.6)):
	  double normalization_E = Njettiness_calculate_NJ_axes_assigned_energy(temp_jet);
	  */
	  //	NJ_q_axes[3] = normalization_E * fourvector(temp_jet.r(), temp_jet.x1(), temp_jet.x2(), temp_jet.x3());
	}
      }
      else {
	// generic case could be implemented...
      }
    }
    else if (temp_order == 2){// NNLO

    }
  }
  else {
    logger << LOG_FATAL << "switch_NJcut_axes not defined!" << endl; exit(1);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



double NJsubtraction_basic::Njettiness_calculate_NJ_axes_assigned_energy(fourvector & temp_jet){
  static Logger logger("NJsubtraction_basic::Njettiness_calculate_NJ_axes_assigned_energy");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // NJ_Ei are always defined in the hadronic frame with three options:
  // switch_NJcut_axes_energy == 0:   E_i = p_i^0
  // switch_NJcut_axes_energy == 1:   E_i = |p_i|
  // switch_NJcut_axes_energy == 2:   E_i = (p_i^0 + |p_i|) / 2

  if (switch_NJcut_axes_energy == 0){return temp_jet.x0();}
  else if (switch_NJcut_axes_energy == 1){return temp_jet.r();}
  else if (switch_NJcut_axes_energy == 2){return .5 * (temp_jet.x0() + temp_jet.r());}
  else {logger << LOG_FATAL << "switch_NJcut_axes_energy is not properly set!" << endl; exit(1);}
}




void NJsubtraction_basic::Njettiness_calculate_NJ_axes_frame(int i_a){
  Logger logger("NJsubtraction_basic::Njettiness_calculate_NJ_axes_frame");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // NJ_measures are typically defined in special reference frames (exception: 0):
  // switch_NJcut_measure == 0:   invariant-mass measure - Qi = Q
  // switch_NJcut_measure == 1:   geometric measure: hadronic frame (no boost)
  // switch_NJcut_measure == 2:   geometric measure: Born frame (Y)
  // switch_NJcut_measure == 3:   geometric measure: 1-jettiness-axis frame (Y_1)
  // switch_NJcut_measure == 4:   geometric measure: leptonic frame (Y_L)

  NJ_p_parton_frame.resize(esi->ps_runtime_jet_algorithm[i_a].size());

  if (switch_NJcut_measure == 0){
    // hadronic frame
    double temp_Q = sqrt(psi->x_pdf[1] * psi->x_pdf[2] * pow(2 * psi->E, 2));
    for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
      NJ_q_axes_frame[i_j] = NJ_q_axes[i_j];
      NJ_n_axes_frame[i_j] = NJ_n_axes[i_j];
      NJ_Qi[i_j] = temp_Q;
    }
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
      NJ_p_parton_frame[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum;
    }
  }
  else if (switch_NJcut_measure == 1){
    // hadronic frame
    for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
      NJ_q_axes_frame[i_j] = NJ_q_axes[i_j];
      NJ_n_axes_frame[i_j] = NJ_n_axes[i_j];
      NJ_Qi[i_j] = 2 * NJ_Ei[i_j];
    }
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
      NJ_p_parton_frame[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum;
    }
  }
  else if (switch_NJcut_measure == 2){
    // Born frame (Y)
    double boost_Born = (psi->x_pdf[1] - psi->x_pdf[2]) / (psi->x_pdf[1] + psi->x_pdf[2]);
    if (psi->boost != boost_Born){logger << LOG_DEBUG << "psi->boost = " << psi->boost << " != " << boost_Born << " = boost_Born" << endl;}

    logger.newLine(LOG_DEBUG);
    for (int i_p = 1; i_p < esi->p_parton[i_a].size(); i_p++){
      logger << LOG_DEBUG << "p(Y frame)[" << i_p << "] = " << esi->particle_event[0][i_a][i_p].momentum.zboost(boost_Born) << endl;
    }

    for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
      NJ_q_axes_frame[i_j] = NJ_q_axes[i_j].zboost(boost_Born);
      NJ_n_axes_frame[i_j] = NJ_n_axes[i_j].zboost(boost_Born);
      NJ_Qi[i_j] = 2 * NJ_q_axes_frame[i_j].x0();
    }
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
      NJ_p_parton_frame[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum.zboost(boost_Born);
    }
    /*
    esi->p_parton[i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]] == NJ_p_parton_frame[i_p]
    double temp_Y = .5 * log(psi->x_pdf[1] / psi->x_pdf[2]);
    2 * NJ_q_axes_frame[1].x0() == 2 * NJ_q_axes[1].x0() * exp(-temp_Y)
    2 * NJ_q_axes_frame[2].x0() == 2 * NJ_q_axes[2].x0() * exp(+temp_Y)
    double temp_Q2 = psi->x_pdf[1] * psi->x_pdf[2] * pow(2 * psi->E, 2);
    double temp_Q = sqrt(temp_Q2);
    */
  }
  else if (switch_NJcut_measure == 3){
    // 1-jettiness-axis frame (Y_1)
    // only defined here for 1-jettiness case
    fourvector p_1 = NJ_q_axes[3];
    double boost_1 = p_1.x3() / p_1.x0();

    logger.newLine(LOG_DEBUG);
    for (int i_p = 1; i_p < esi->p_parton[i_a].size(); i_p++){
      logger << LOG_DEBUG << "p(Y_1 frame)[" << i_p << "] = " << esi->particle_event[0][i_a][i_p].momentum.zboost(boost_1) << endl;
    }

    for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
      NJ_q_axes_frame[i_j] = NJ_q_axes[i_j].zboost(boost_1);
      NJ_n_axes_frame[i_j] = NJ_n_axes[i_j].zboost(boost_1);
      NJ_Qi[i_j] = 2 * NJ_q_axes_frame[i_j].x0();
    }
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
      NJ_p_parton_frame[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum.zboost(boost_1);
    }
  }
  else if (switch_NJcut_measure == 4){
    // leptonic frame (Y_L)
    fourvector p_L(0., 0., 0., 0.);
    for (int i_p = 3; i_p < 3 + csi->n_particle_born - csi->n_jet_born; i_p++){
      p_L = p_L + esi->particle_event[0][i_a][i_p].momentum;
    }
    double boost_L = p_L.x3() / p_L.x0();

    logger.newLine(LOG_DEBUG);
    for (int i_p = 1; i_p < esi->p_parton[i_a].size(); i_p++){
      logger << LOG_DEBUG << "p(Y_L frame)[" << i_p << "] = " << esi->particle_event[0][i_a][i_p].momentum.zboost(boost_L) << endl;
    }

    for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
      NJ_q_axes_frame[i_j] = NJ_q_axes[i_j].zboost(boost_L);
      NJ_n_axes_frame[i_j] = NJ_n_axes[i_j].zboost(boost_L);
      NJ_Qi[i_j] = 2 * NJ_q_axes_frame[i_j].x0();
    }
    for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
      NJ_p_parton_frame[i_p] = esi->particle_event[0][i_a][esi->ps_runtime_jet_algorithm[i_a][i_p]].momentum.zboost(boost_L);
    }
  }

  logger.newLine(LOG_DEBUG);
  for (int i_q = 1; i_q < NJ_q_axes.size(); i_q++){
    logger << LOG_DEBUG << "NJ_q_axes_frame[" << i_q << "] = " << NJ_q_axes_frame[i_q] << endl;
  }
  logger.newLine(LOG_DEBUG);
  for (int i_j = 1; i_j < NJ_Qi.size(); i_j++){
    logger << LOG_DEBUG << "NJ_Qi[" << i_j << "] = " << NJ_Qi[i_j] << endl;
  }
  logger.newLine(LOG_DEBUG);
  for (int i_q = 1; i_q < NJ_q_axes.size(); i_q++){
    logger << LOG_DEBUG << "NJ_n_axes_frame[" << i_q << "] = " << NJ_n_axes_frame[i_q] << endl;
  }
  logger.newLine(LOG_DEBUG);
  for (int i_p = 0; i_p < esi->ps_runtime_jet_algorithm[i_a].size(); i_p++){
    logger << LOG_DEBUG << "NJ_p_parton_frame[" << i_p << "] = " << NJ_p_parton_frame[i_p] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




