#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
observable_set::observable_set(){
  int_end = 0;
  max_dyn_ren = 0;
  max_dyn_fact = 0;
}



observable_set::observable_set(int _n_set_TSV){
  n_set_TSV = _n_set_TSV;
  int_end = 0;
  max_dyn_ren = 0;
  max_dyn_fact = 0;
}



observable_set::observable_set(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, event_set * _esi, phasespace_set * _psi, user_defined * _user){
  Logger logger("observable_set::observable_set");
  logger << LOG_DEBUG << "called" << endl;

  initialization(isi, _csi, _msi, _esi, _psi, _user);
  /*  csi = _csi;
  esi = _esi;
  user = _user;
  psi = _psi;

  initialization_input_predefinition_TSV(isi);
  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();
  */

  logger << LOG_DEBUG << "finished" << endl;
}

/*
  logger << LOG_INFO << setw(35) << "isi.switch_TSV" << " = " << isi.switch_TSV << endl;
  int_end = 0;
  max_dyn_ren = 0;
  max_dyn_fact = 0;
  */
  //  run_mode = isi.run_mode;

  //  switch_reference = isi.switch_reference;

  // fake parameters which can be removed later:
  //  int pdf_set = 0;

  /////////////////////////////////////////////////////////
  //  set observable_set values from inputparameter_set  //
  /////////////////////////////////////////////////////////


  ////////////////////////////
  //  contribution_set csi  //
  ////////////////////////////

  //  csi = isi.csi;

  /////////////////////////
  //  beam / CMS energy  //
  /////////////////////////
  /*
  E_beam = isi.E;
  E_CMS = 2 * E_beam;
  */
  //////////////////////////////////////////////////////////////
  //  selection of process and contribution to be calculated  //
  //////////////////////////////////////////////////////////////

  /*
  process_class = isi.csi.process_class;
  subprocess = isi.csi.subprocess;
  type_perturbative_order = isi.csi.type_perturbative_order;
  type_contribution = isi.csi.type_contribution;
  type_correction = isi.csi.type_correction;
  contribution_order_alpha_s = isi.csi.contribution_order_alpha_s;
  contribution_order_alpha_e = isi.csi.contribution_order_alpha_e;
  contribution_order_interference = isi.csi.contribution_order_interference;

  order_alphas_born = csi->order_alpha_s_born;
  // only the csi version should be used !!!
  */

  //  order_alphas_born is probably not very useful for the N-jettiness subtraction.
  //  Replace (or add?) parameter based on number of Born-level jets:

  /*
  NJ_q_axes.resize(3 + csi->n_jet_born);
  NJ_n_axes.resize(3 + csi->n_jet_born);
  NJ_Ei.resize(3 + csi->n_jet_born);

  NJ_q_axes_frame.resize(3 + csi->n_jet_born);
  NJ_n_axes_frame.resize(3 + csi->n_jet_born);
  NJ_Qi.resize(3 + csi->n_jet_born);
  */
  /*
  int n_jet_born = 0;
  for (int i_s = 0; i_s < csi->process_class.size(); i_s++){
    if (csi->process_class[i_s] == 'j'){n_jet_born++;}
  }
  */
  /*
  NJ_q_axes.resize(3 + csi->n_jet_born);
  NJ_q_axes[1] = fourvector(1., 0., 0., 1.);
  NJ_q_axes[2] = fourvector(1., 0., 0., -1.);
  */
  // only if needed ???
  /*
  logger << LOG_INFO << "csi->order_alpha_s_born = " << setw(23) << setprecision(15) << csi->order_alpha_s_born << endl;
  logger << LOG_INFO << "order_alphas_born = " << order_alphas_born << endl;
  logger << LOG_INFO << "csi->n_jet_born (no of Born-level jets) = " << csi->n_jet_born << endl;
  */


  // should finally not be needed any more...
  // replaces parts of "initialization_contribution();"
  // still needed: n_particle, type_parton

  /////////////////////
  //  event_set esi  //
  /////////////////////

  /* // shifted to esi !!!
  esi = isi.esi;
  */
  /////////////////////
  //  user_set user  //
  /////////////////////

  //  user = isi.user;

  //  initialization_path(isi);
  //  initialization_unit(isi);


  // ->|  exported to inputparameter_set until here !!!


// **************************************************************************
// *                                                                        *
// *  determination of perturbative QCD order of present contribution       *
// *                                                                        *
// **************************************************************************

//  initialization_LHAPDF_parameters(isi);
//  initialization_LHAPDF();

//  logger << LOG_DEBUG << "LHAPDF has been initialized." << endl;
/*
  int model_perturbative_order = 0;
  vector<string> model_contribution_LHAPDFname(isi.max_perturbative_QCD_order + 1);
  vector<int> model_contribution_LHAPDFsubset(isi.max_perturbative_QCD_order + 1);
  for (int i_s = 0; i_s < isi.type_perturbative_order_counter + 1; i_s++){
    if (isi.collection_type_perturbative_order[i_s] == "LO"){
      model_perturbative_order = 0;
      model_contribution_LHAPDFname[0] = isi.contribution_LHAPDFname[i_s];
      model_contribution_LHAPDFsubset[0] = isi.contribution_LHAPDFsubset[i_s];
    }
    if (isi.collection_type_perturbative_order[i_s] == "NLO"){
      model_perturbative_order = 1;
      model_contribution_LHAPDFname[1] = isi.contribution_LHAPDFname[i_s];
      model_contribution_LHAPDFsubset[1] = isi.contribution_LHAPDFsubset[i_s];
    }
    if (isi.collection_type_perturbative_order[i_s] == "NNLO"){
      model_perturbative_order = 2;
      model_contribution_LHAPDFname[2] = isi.contribution_LHAPDFname[i_s];
      model_contribution_LHAPDFsubset[2] = isi.contribution_LHAPDFsubset[i_s];
    }
  }
  for (int i_s = 0; i_s < model_contribution_LHAPDFname.size(); i_s++){
    if (LHAPDFname == model_contribution_LHAPDFname[i_s]){model_perturbative_order = i_s; break;}
  }
  logger << LOG_DEBUG << "model_perturbative_order = " << model_perturbative_order << endl;
  for (int i_s = 0; i_s < model_contribution_LHAPDFname.size(); i_s++){
    logger << LOG_DEBUG << "model_contribution_LHAPDFname[" << i_s << " = " << setw(25) << model_contribution_LHAPDFname[i_s] << "   " << model_contribution_LHAPDFsubset[i_s] << endl;
  }
  // Only needed for model ???




  int switch_alpha_CMS = 0;
  logger << LOG_DEBUG << "switch_alpha_CMS = " << switch_alpha_CMS << endl;
  logger << LOG_DEBUG << "user.switch_map[switch_alpha_CMS] = " << user.switch_map["switch_alpha_CMS"] << endl;
  logger << LOG_DEBUG << "user.switch_value.size() = " << user.switch_value.size() << endl;
  logger << LOG_DEBUG << "user.switch_value[user.switch_map[switch_alpha_CMS]] = " << user.switch_value[user.switch_map["switch_alpha_CMS"]] << endl;

  switch_alpha_CMS = user.switch_value[user.switch_map["switch_alpha_CMS"]];

  int switch_cosw_real = user.switch_value[user.switch_map["switch_cosw_real"]];



  //  model_set msi;
  vector<string> file_input;
  //isi.max_perturbative_QCD_order

  logger << LOG_DEBUG_VERBOSE << "isi.present_type_perturbative_order = " << isi.present_type_perturbative_order << endl;

  // Remove dependencies: msi = model_set(isi.collection_type_perturbative_order, model_contribution_LHAPDFname, model_contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);

  msi = model_set(file_input, N_f, N_f_active, scale_ren, model_perturbative_order, isi.collection_type_perturbative_order, model_contribution_LHAPDFname, model_contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);

  //  msi = model_set(file_input, N_f, N_f_active, scale_ren, isi.present_type_perturbative_order, isi.collection_type_perturbative_order, isi.contribution_LHAPDFname, isi.contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);
  //  msi = model_set(file_input, N_f, N_f_active, isi.max_perturbative_QCD_order, scale_ren, isi.present_type_perturbative_order, isi.collection_type_perturbative_order, isi.contribution_LHAPDFname, isi.contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);

  //  model = readin_model(N_f, N_f_active, model_perturbative_order, isi.max_perturbative_QCD_order, model_contribution_LHAPDFname, model_contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);

  //  parameter_model pmod = readin_model(N_f, N_f_active, model_perturbative_order, isi.max_perturbative_QCD_order, model_contribution_LHAPDFname, model_contribution_LHAPDFsubset, switch_alpha_CMS, switch_cosw_real);


  // alpha_e rescaling factor because of use_adapted_ew_coupling:
  logger << LOG_INFO << "msi.ew_scheme = " << msi.ew_scheme << endl;
  logger << LOG_INFO << "msi.use_cms = " << msi.use_cms << endl;
  logger << LOG_INFO << "msi.use_adapted_ew_coupling = " << msi.use_adapted_ew_coupling << endl;
  logger << LOG_INFO << "n_photon_born = " << csi->n_photon_born << endl;
  logger << LOG_INFO << "csi->contribution_order_alpha_e = " << csi->contribution_order_alpha_e << endl;
  logger << LOG_INFO << "msi.alpha_e = " << setw(23) << setprecision(15) << msi.alpha_e << endl;
  logger << LOG_INFO << "msi.alpha_e_0 = " << setw(23) << setprecision(15) << msi.alpha_e_0 << endl;
  logger << LOG_INFO << "msi.alpha_e_Gmu = " << setw(23) << setprecision(15) << msi.alpha_e_Gmu << endl;
  logger << LOG_INFO << "msi.alpha_e_MZ = " << setw(23) << setprecision(15) << msi.alpha_e_MZ << endl;
 */

//  rescaling_factor_alpha_e = 1.;

  // Rescaling is automatically done in OpenLoops (might need to be added for RECOLA) !!!
  /*
  if (msi.use_adapted_ew_coupling == -1){
    rescaling_factor_alpha_e = 1.;
  }
  else if (msi.use_adapted_ew_coupling == 0){
    // i.e. ew_scheme = 1 or 2:
    logger << LOG_INFO << "msi.alpha_e = " << setw(23) << setprecision(15) << msi.alpha_e << endl;
    logger << LOG_INFO << "msi.alpha_e_0 = " << setw(23) << setprecision(15) << msi.alpha_e_0 << endl;
    logger << LOG_INFO << "csi->n_photon_born = " << csi->n_photon_born << endl;
    rescaling_factor_alpha_e = pow(msi.alpha_e_0 / msi.alpha_e, csi->n_photon_born);
  }
  else if (msi.use_adapted_ew_coupling == 1 || msi.use_adapted_ew_coupling == 2){
      // check if always correct for "MIX" contributions !!!
    logger << LOG_INFO << "msi.alpha_e = " << setw(23) << setprecision(15) << msi.alpha_e << endl;
    if (msi.use_adapted_ew_coupling == 1){logger << LOG_INFO << setw(23) << setprecision(15) << "msi.alpha_e_Gmu = " << msi.alpha_e_Gmu << endl;}
    else if (msi.use_adapted_ew_coupling == 2){logger << LOG_INFO << "msi.alpha_e_MZ = " << setw(23) << setprecision(15) << msi.alpha_e_MZ << endl;}
    logger << LOG_INFO << "csi->n_photon_born = " << csi->n_photon_born << endl;
    if (csi->type_correction == "QEW" || csi->type_correction == "MIX"){
      logger << LOG_INFO << "csi->contribution_order_alpha_e(" << csi->contribution_order_alpha_e << ") - 1 - csi->n_photon_born(" << csi->n_photon_born << ") = " << csi->contribution_order_alpha_e - 1 - csi->n_photon_born << endl;
      if (msi.use_adapted_ew_coupling == 1){rescaling_factor_alpha_e = pow(msi.alpha_e_Gmu / msi.alpha_e, csi->contribution_order_alpha_e - 1 - csi->n_photon_born);}
      else if (msi.use_adapted_ew_coupling == 2){rescaling_factor_alpha_e = pow(msi.alpha_e_MZ / msi.alpha_e, csi->contribution_order_alpha_e - 1 - csi->n_photon_born);}
    }
    else {
      logger << LOG_INFO << "csi->contribution_order_alpha_e(" << csi->contribution_order_alpha_e << ") - csi->n_photon_born(" << csi->n_photon_born << ") = " << csi->contribution_order_alpha_e - csi->n_photon_born << endl;
      if (msi.use_adapted_ew_coupling == 1){rescaling_factor_alpha_e = pow(msi.alpha_e_Gmu / msi.alpha_e, csi->contribution_order_alpha_e - csi->n_photon_born);}
      else if (msi.use_adapted_ew_coupling == 2){rescaling_factor_alpha_e = pow(msi.alpha_e_MZ / msi.alpha_e, csi->contribution_order_alpha_e - csi->n_photon_born);}
    }
  }
  */
  // rescaling_factor_alpha_e needs to be introduced in  NJ  contributions as well !!!
  /*
  logger << LOG_INFO << "rescaling_factor_alpha_e = " << setw(23) << setprecision(15) << rescaling_factor_alpha_e << endl;



  logger << LOG_INFO << "alpha_S at various scales for " << LHAPDFname << ":" << endl;
  logger << LOG_INFO << "alpha_S(M_W = " << setprecision(5) << setw(7) << msi.M_W << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi.M_W) << endl;
  logger << LOG_INFO << "alpha_S(M_Z = " << setprecision(6) << setw(7) << msi.M_Z << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi.M_Z) << endl;
  logger << LOG_INFO << "alpha_S(M_t = " << setprecision(4) << setw(7) << msi.M_t << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi.M_t) << endl;
  if (msi.M_b != 0.){logger << LOG_INFO << "alpha_S(M_b = " << setprecision(3) << setw(7) << msi.M_b << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi.M_b) << endl;}
  logger << LOG_INFO << "alpha_S(1 TeV)   = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(1000.) << endl;
  */

  // process-dependent part (type_parton, process_type)
  // process_type = 0: no process
  // process_type = 1: particles decay
  // process_type = 2: collision of 2 particles
  //  vector<vector<int> > type_parton(1);
  /*
  logger << LOG_DEBUG << "process_class = " << process_class << endl;
  logger << LOG_DEBUG << "isi.csi.subprocess = " << isi.csi.subprocess << endl;
  */
  // replace by csi->... !!!
  //  type_parton = isi.csi.type_parton;
  //  n_particle = isi.csi.n_particle;
  //  process_type = isi.csi.process_type;
  /*
  logger << LOG_DEBUG << "after" << endl;
  for (int i_x = 0; i_x < csi->type_parton.size(); i_x++){
    for (int i_p = 0; i_p < csi->type_parton[i_x].size(); i_p++){
      logger << LOG_DEBUG << "csi->type_parton[" << i_x << "][" << i_p << "] = " << csi->type_parton[i_x][i_p] << endl;
    }
  }
  */
  /* // shifted to esi !!!
  initialization_object_event_selection(isi);
  //  fills (in particular):
  //  photon_recombination_list -> contains PDG labels of partons that will enter a jet algorithm
  //  jet_algorithm_list        -> contains PDG labels of partonsthat might be 'dressed' with photons
  //  (photon_recombination_selection, photon_recombination_disable, jet_algorithm_selection, jet_algorithm_disable   are NOT used any longer afterwards.)
  //  sets all basic parameters for jet algorithm, photon recombination and Frixione isolation.
  //  nothing process-specific so far.

  determine_object_definition();
  //  initialization_object_process();
  //  determine_object_definition();
  //  define_pT/ET/eta/y and n_observed_min/max are adapted between different objects (like lep and e/mu/tau etc.).
  //  check if this works correctly !!!

  esi.determine_n_partonlevel(csi->type_parton);
  //  fill n_partonlevel: uses process information !!!
  //  very trivial right now, no use of jet_algorithm_list, etc., no splitting between different phasespaces.
  // is this actually needed here ??? More sophisticated determination happens later !!!
  //  initialization_object_process();


  isi.esi = esi;  // !!! Should be done the other way round !!!
  */ //
  /*
  // really needed ???
    initialization_masses(msi.M, msi.M2);



  logger << LOG_DEBUG << "switch_distribution = " << isi.switch_distribution << endl;

  // this is needed because initialization_TSV is (in the routines functions) called before initialization_CV, but seems to be responsible for computing the central alphaS value. if we want this at the scales multiplied by the prefactor, initialization_generic needs to know about it..
  //  prefactor_CV = isi.prefactor_CV;
  // shifted elsewhere...

  initialization_switches(isi);
  //  initialization_resummation(isi);
  initialization_qTcut(isi);
  initialization_integration_parameters(isi);
  initialization_basic_CV(isi);
  initialization_basic_TSV(isi);
  initialization_filename();

#ifdef OPENLOOPS
  initialization_OpenLoops_input(isi);
#endif
#ifdef RECOLA
  initialization_Recola_input(isi);
#endif
*/
  /*
    logger << LOG_DEBUG << "finished" << endl;
}
*/




void observable_set::calculate_intermediate_result(){
  Logger logger("observable_set::calculate_intermediate_result");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  calculate_Xsection(psi->i_gen, Xsection, Xsection_delta, full_sum_weight, full_sum_weight2, step_sum_weight, step_sum_weight2);

  logger << LOG_DEBUG << "full_sum_weight = " << full_sum_weight << endl;

  if (switch_CV){
    for (int i_c = 0; i_c < n_qTcut; i_c++){
      for (int i_s = 0; i_s < n_scales_CV; i_s++){
	calculate_Xsection(psi->i_gen, Xsection_CV[i_c][i_s], Xsection_delta_CV[i_c][i_s], full_sum_weight_CV[i_c][i_s], full_sum_weight2_CV[i_c][i_s], step_sum_weight_CV[i_c][i_s], step_sum_weight2_CV[i_c][i_s]);
      }
    }
  }

  if (switch_TSV){
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	  for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	    if (active_qTcut){
	      logger << LOG_DEBUG_VERBOSE << "    sum_weight_qTcut_TSV[" << i_s << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << sum_weight_qTcut_TSV[i_s][i_q][i_r][i_f] << endl;
	      calculate_Xsection(psi->i_gen, Xsection_TSV[i_s][i_q][i_r][i_f], Xsection_delta_TSV[i_s][i_q][i_r][i_f], fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f], fullsum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f], sum_weight_qTcut_TSV[i_s][i_q][i_r][i_f], sum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f]);
	      logger << LOG_DEBUG_VERBOSE << "fullsum_weight_qTcut_TSV[" << i_s << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f] << endl;
	      logger << LOG_DEBUG_VERBOSE << "      Xsection_TSV[" << i_s << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << Xsection_TSV[i_s][i_q][i_r][i_f] << endl;
	    }
	    else {
	      logger << LOG_DEBUG_VERBOSE << "    sum_weight_TSV[" << i_s << "][" << i_r << "][" << i_f << "] = " << sum_weight_TSV[i_s][i_r][i_f] << endl;
	      calculate_Xsection(psi->i_gen, Xsection_TSV[i_s][i_q][i_r][i_f], Xsection_delta_TSV[i_s][i_q][i_r][i_f], fullsum_weight_TSV[i_s][i_r][i_f], fullsum_weight2_TSV[i_s][i_r][i_f], sum_weight_TSV[i_s][i_r][i_f], sum_weight2_TSV[i_s][i_r][i_f]);
	      logger << LOG_DEBUG_VERBOSE << "fullsum_weight_TSV[" << i_s << "][" << i_r << "][" << i_f << "] = " << fullsum_weight_TSV[i_s][i_r][i_f] << endl;
	      logger << LOG_DEBUG_VERBOSE << "      Xsection_TSV[" << i_s << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << Xsection_TSV[i_s][i_q][i_r][i_f] << endl;
	    }
	  }
	  /*
	  // Should be identical !!!
	  if (active_qTcut){
	    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	      calculate_Xsection(psi->i_gen, Xsection_TSV[i_s][i_q][i_r][i_f], Xsection_delta_TSV[i_s][i_q][i_r][i_f], fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f], fullsum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f], sum_weight_qTcut_TSV[i_s][i_q][i_r][i_f], sum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f]);
	    }
	  }
	  else {
	    calculate_Xsection(psi->i_gen, Xsection_TSV[i_s][0][i_r][i_f], Xsection_delta_TSV[i_s][0][i_r][i_f], fullsum_weight_TSV[i_s][i_r][i_f], fullsum_weight2_TSV[i_s][i_r][i_f], sum_weight_TSV[i_s][i_r][i_f], sum_weight2_TSV[i_s][i_r][i_f]);
	  }
	  */
	}
      }
    }
  }

  if (switch_moment){
     for (int i_m = 0; i_m < n_moments; i_m++){
       calculate_Xsection(psi->i_gen, Xsection_moment[i_m], Xsection_moment_delta[i_m], full_sum_moment[i_m], full_sum_moment2[i_m], step_sum_moment[i_m], step_sum_moment2[i_m]);

       logger << LOG_DEBUG << "full_sum_moment[i_m] = " << full_sum_moment[i_m] << endl;

       if (switch_CV){
	 for (int i_c = 0; i_c < n_qTcut; i_c++){
	   for (int i_s = 0; i_s < n_scales_CV; i_s++){
	     calculate_Xsection(psi->i_gen, Xsection_moment_CV[i_m][i_c][i_s], Xsection_moment_delta_CV[i_m][i_c][i_s], full_sum_moment_CV[i_m][i_c][i_s], full_sum_moment2_CV[i_m][i_c][i_s], step_sum_moment_CV[i_m][i_c][i_s], step_sum_moment2_CV[i_m][i_c][i_s]);
	   }
	 }
       }

       if (switch_TSV){
	 for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	   if (!switch_moment_TSV[i_s]){continue;}
	   for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	     for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	       for (int i_q = 0; i_q < output_n_qTcut; i_q++){
		 if (active_qTcut){
		   logger << LOG_DEBUG_VERBOSE << "    sum_moment_qTcut_TSV[" << i_s << "][" << i_m << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << sum_moment_qTcut_TSV[i_s][i_m][i_q][i_r][i_f] << endl;
		   calculate_Xsection(psi->i_gen, Xsection_moment_TSV[i_s][i_m][i_q][i_r][i_f], Xsection_moment_delta_TSV[i_s][i_m][i_q][i_r][i_f], fullsum_moment_qTcut_TSV[i_s][i_m][i_q][i_r][i_f], fullsum_moment2_qTcut_TSV[i_s][i_m][i_q][i_r][i_f], sum_moment_qTcut_TSV[i_s][i_m][i_q][i_r][i_f], sum_moment2_qTcut_TSV[i_s][i_m][i_q][i_r][i_f]);
		   logger << LOG_DEBUG_VERBOSE << "fullsum_moment_qTcut_TSV[" << i_s << "][" << i_m << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << fullsum_moment_qTcut_TSV[i_s][i_m][i_q][i_r][i_f] << endl;
		   logger << LOG_DEBUG_VERBOSE << "      Xsection_moment_TSV[" << i_s << "][" << i_m << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << Xsection_moment_TSV[i_s][i_m][i_q][i_r][i_f] << endl;
		 }
		 else {
		   logger << LOG_DEBUG_VERBOSE << "    sum_moment_TSV[" << i_s << "][" << i_m << "][" << i_r << "][" << i_f << "] = " << sum_moment_TSV[i_s][i_m][i_r][i_f] << endl;
		   calculate_Xsection(psi->i_gen, Xsection_moment_TSV[i_s][i_m][i_q][i_r][i_f], Xsection_moment_delta_TSV[i_s][i_m][i_q][i_r][i_f], fullsum_moment_TSV[i_s][i_m][i_r][i_f], fullsum_moment2_TSV[i_s][i_m][i_r][i_f], sum_moment_TSV[i_s][i_m][i_r][i_f], sum_moment2_TSV[i_s][i_m][i_r][i_f]);
		   logger << LOG_DEBUG_VERBOSE << "fullsum_moment_TSV[" << i_s << "][" << i_m << "][" << i_r << "][" << i_f << "] = " << fullsum_moment_TSV[i_s][i_m][i_r][i_f] << endl;
		   logger << LOG_DEBUG_VERBOSE << "      Xsection_moment_TSV[" << i_s << "][" << i_m << "][" << i_q << "][" << i_r << "][" << i_f << "] = " << Xsection_moment_TSV[i_s][i_m][i_q][i_r][i_f] << endl;
		 }
	       }
	     }
	   }
	 }
       }
     }
  }

  if ((psi->i_acc == psi->n_events_max) || (psi->i_acc >= psi->n_events_min && abs(Xsection_delta / sigma_normalization) < sigma_normalization_deviation)){int_end = 1;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void observable_set::calculate_Xsection(long long i_gen, double & this_Xsection, double & this_Xsection_delta, double & this_sum_weights, double & this_sum_weights2, double & this_temp_sum_weights, double & this_temp_sum_weights2){
  Logger logger("observable_set::calculate_Xsection");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  this_sum_weights += this_temp_sum_weights;
  this_sum_weights2 += this_temp_sum_weights2;
  this_temp_sum_weights = 0.;
  this_temp_sum_weights2 = 0.;
  this_Xsection_delta = sqrt((this_sum_weights2 - pow(this_sum_weights, 2) / i_gen)) / (i_gen - 1);
  this_Xsection = this_sum_weights / i_gen;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


/*
void observable_set::determine_p_parton(phasespace_set & psi){
  Logger logger("observable_set::determine_p_parton");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  p_parton = start_p_parton;
  for (int xi = 0; xi <= csi->n_particle + 2; xi++){p_parton[0][psi->o_map[0][xi]] = psi->xbp_all[0][intpow(2, xi - 1)];}
  x_pdf = psi->x_pdf;
  boost = psi->boost;
  /*
  logger << LOG_DEBUG_VERBOSE << "type_contribution = " << type_contribution << endl;
  if (type_contribution == "CA" || type_contribution == "RCA"){z_coll = psi->z_coll;}
*//*
  for (int xi = 0; xi <= csi->n_particle + 2; xi++){logger << LOG_DEBUG_VERBOSE << "p_parton[0][" << psi->o_map[0][xi] << "] = " << p_parton[0][psi->o_map[0][xi]] << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

void observable_set::initialization_runtime(){
  Logger logger("observable_set::initialization_runtime");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  start = clock();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::determine_runtime(){
  Logger logger("observable_set::determine_runtime");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (start > clock()){start = clock(); time_counter = 0;}
  if (start < 0 && clock() > 0){start = clock(); time_counter = 0;}

  //  int seconds_passed = clock()/CLOCKS_PER_SEC - start/CLOCKS_PER_SEC;
  int seconds_passed = clock()/CLOCKS_PER_SEC - start/CLOCKS_PER_SEC + sec_import;

  int old_h = h;
  int old_min = min;

  if (seconds_passed > 3600) {
    h = int(seconds_passed / 3600);
    min = 0;
    seconds_passed -= h * 3600;
  }
  if (seconds_passed > 60) {
    min = int(seconds_passed / 60);
    seconds_passed -= min * 60;
  }
  sec = seconds_passed;
  //  sec = seconds_passed + sec_import;

  /*
  static long long i_acc_old = 0;
  static long long i_gen_old = 0;
  double efficiency = 0;

  if (min > old_min || h > old_h){
    //  if ((min > old_min && h == 0) || h > old_h){
    if (i_gen_old != psi->i_gen) {
      efficiency = double(psi->i_acc-i_acc_old)/(psi->i_gen-i_gen_old);

      i_acc_old = psi->i_acc;
      i_gen_old = psi->i_gen;
    }
  }
  */

  if ((switch_console_output_runtime == 1 && ((min > old_min && h == 0) || h > old_h)) ||
      (switch_console_output_runtime == 2 && (min > old_min || h > old_h))){
    // complete efficiency, not only that one of the last interval.
    double efficiency = double(psi->i_acc) / psi->i_gen;

    logger << LOG_INFO << setw(3) << right << h << " hours" << setw(3) << right << min << " minutes needed for " << psi->i_acc << " accepted (" << psi->i_gen << " generated) events (efficiency = " << efficiency << ")." << endl;
  }
  /*
  if (h > old_h) {
    logger << LOG_INFO << setw(3) << right << h << " hours" << setw(3) << right << min << " minutes needed for " << psi->i_acc << " accepted (" << psi->i_gen << " generated) events (efficiency = " << efficiency << ")." << endl;
  }
  */

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void observable_set::determine_techcut_RA(){
  Logger logger("observable_set::determine_techcut_RA");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  psi->RA_techcut = 0;
  static int num_printouts = 0;
  const int max_printouts = 1000;
  static double severity_threshold = 1.e-12;
  static double impact_threshold = 1.e-3;

  // check singular regions (p_i * p_j / s_hat) < cut_technical
  for (int sr = 0; sr < csi->singular_region_list.size(); sr++){
    int x1 = csi->singular_region_list[sr][0];
    int x2 = csi->singular_region_list[sr][1];
    csi->singular_region[x1][x2] = esi->p_parton[0][x1] * esi->p_parton[0][x2] / psi->xbs_all[0][0];

    if (csi->singular_region[x1][x2] < psi->cut_technical){
      int ccount = 0;
      for (int i_a = 1; i_a < esi->cut_ps.size(); i_a++){if (esi->cut_ps[i_a] == -1){ccount++;}}

      double severity = abs(1. + accumulate(RA_ME2.begin() + 1, RA_ME2.end(), 0.) / RA_ME2[0]);
      //      double severity = 1.;

      // suppress warning if point is (relatively) harmless and output_level is low
      //      if (csi->type_contribution != "L2RA" &&
      //	  (((severity > severity_threshold || Log::getLogThreshold() <= LOG_DEBUG) &&
      if ((((severity > severity_threshold || Log::getLogThreshold() <= LOG_DEBUG) &&
	    (abs(integrand / sigma_normalization) > impact_threshold || sigma_normalization == 1.) &&
	    num_printouts < max_printouts) ||
	   Log::getLogThreshold() <= LOG_DEBUG_VERBOSE)){
        logger << LOG_DEBUG << endl;
        logger << LOG_DEBUG << "applying technical cut near dipole singularity" << endl;
        logger << LOG_DEBUG << "cut_technical = " << psi->cut_technical << endl;
        for (int ib = 0; ib < esi->p_parton[0].size(); ib++){logger << LOG_DEBUG << "esi->p_parton[0][" << ib << "] = " << esi->p_parton[0][ib] << "   " << esi->p_parton[0][ib].m2() << "   " << sqrt(abs(esi->p_parton[0][ib].m2())) << endl;}
        logger << LOG_DEBUG << "contributing dipoles:   " << endl;
        for (int i_a = 0; i_a < esi->cut_ps.size(); i_a++){
          if (esi->cut_ps[i_a] != -1){logger << LOG_DEBUG << setw(8) << csi->dipole[i_a].name() << "   " << "RA_ME2[" << i_a << "] = " << RA_ME2[i_a] << endl;}
        }

	if (switch_console_output_techcut_RA){
	  num_printouts++;
	  stringstream temp_ss;
	  temp_ss << "  "
		  << right << setw(12) << psi->i_gen
		  << right << setw(10) << psi->i_acc
		  << "  ("
		  << right << setw(2) << esi->cut_ps[0] << "/"
		  << right << setw(2) << ccount
		  << "  "
		  << csi->singular_region_name[x1][x2] << "/^s = "
		  << right << setw(11) << setprecision(4) << showpoint << csi->singular_region[x1][x2]
		  << ")  int/norm = "
		  << right << setw(15) << setprecision(8) << showpoint << integrand / sigma_normalization;
	  if (accumulate(RA_ME2.begin() + 1, RA_ME2.end(), 0.) != 0. && RA_ME2[0] != 0.){
	    temp_ss << "  A/R = "
		    << setw(23) << setprecision(15) << -accumulate(RA_ME2.begin() + 1, RA_ME2.end(), 0.) / RA_ME2[0]
		    << "  A/R-1 = "
		    << right << scientific << setw(9) << setprecision(2) << -accumulate(RA_ME2.begin() + 1, RA_ME2.end(), 0.) / RA_ME2[0] - 1.
		    << defaultfloat << left;
	  }
	  logger << LOG_INFO << temp_ss.str() << endl;
	}
      }

      /*
      // RA_techcut_integrand could be used to check the dependence on the parameter cut_technical !!!
      if (csi->singular_region[x1][x2] > psi->cut_technical / 10.){RA_techcut_integrand = integrand;}
      else {RA_techcut_integrand = 0.;}
      */
      /*
      if (abs(integrand / sigma_normalization) > 1. || csi->singular_region[x1][x2] < psi->cut_technical / 100.) {
	psi->RA_techcut = 1;
	psi->i_tec++;
	integrand = 0.;
      }
      */
      // new:
      // All points with (p_i * p_j / s_hat) < cut_technical are technically cut.
      // This might require an adaptation of the corresponding values...
      psi->RA_techcut = 1;
      psi->i_tec++;
      integrand = 0.;
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



///////////////////////
//  access elements  //
///////////////////////

ostream & operator << (ostream & s, const observable_set & osi){
  s << "observable_set independent stuff:" << endl;
  s << setw(38) << right << "switch_qTcut" << setw(5) << "" << "=" << setw(5) << "" << osi.switch_qTcut << endl;
  s << setw(38) << right << "n_qTcut" << setw(5) << "" << "=" << setw(5) << "" << osi.n_qTcut << endl;
  s << setw(38) << right << "min_qTcut" << setw(5) << "" << "=" << setw(5) << "" << osi.min_qTcut << endl;
  s << setw(38) << right << "step_qTcut" << setw(5) << "" << "=" << setw(5) << "" << osi.step_qTcut << endl;
  s << setw(38) << right << "switch_distribution_at_all_TSV" << setw(5) << "" << "=" << setw(5) << "" << osi.switch_distribution_at_all_TSV << endl;

    s << "observable_set output:   n_set_TSV = " << osi.n_set_TSV << endl << endl;

  if (osi.switch_TSV == 0){
    s << "observable_set output:   TSV has been switched off." << endl << endl;
  }
  else {
    s << "observable_set output:   n_set_TSV = " << osi.n_set_TSV << endl << endl;
    for (int i_s = 0; i_s < osi.n_set_TSV; i_s++){
      s << setw(35) << right << "name_set_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.name_set_TSV[i_s] << endl;
      s << setw(35) << right << "dynamic_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.dynamic_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "central_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.central_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "relative_central_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.relative_central_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "n_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.n_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "factor_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.factor_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "dynamic_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.dynamic_scale_fact_TSV[i_s] << endl;
      s << setw(35) << right << "central_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.central_scale_fact_TSV[i_s] << endl;
      s << setw(35) << right << "relative_central_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.relative_central_scale_fact_TSV[i_s] << endl;
      s << setw(35) << right << "n_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.n_scale_fact_TSV[i_s] << endl;
      s << setw(35) << right << "factor_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.factor_scale_fact_TSV[i_s] << endl;
      s << setw(35) << right << "min_qTcut_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.min_qTcut_TSV[i_s] << endl;
      s << setw(35) << right << "max_qTcut_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.max_qTcut_TSV[i_s] << endl;
      s << setw(35) << right << "switch_distribution_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.switch_distribution_TSV[i_s] << endl;
      s << setw(35) << right << "max_n_integrand_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.max_n_integrand_TSV[i_s] << endl;
      s << setw(35) << right << "min_qTcut_distribution_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.min_qTcut_distribution_TSV[i_s] << endl;
      s << setw(35) << right << "max_qTcut_distribution_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.max_qTcut_distribution_TSV[i_s] << endl;
      s << setw(35) << right << "switch_moment_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.switch_moment_TSV[i_s] << endl;
      s << setw(35) << right << "filename_integration_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.filename_integration_TSV[i_s] << endl;
      s << setw(35) << right << "filename_result_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.filename_result_TSV[i_s] << endl;
      s << setw(35) << right << "filename_distribution_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.filename_distribution_TSV[i_s] << endl;
      s << setw(35) << right << "filename_moment_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.filename_moment_TSV[i_s] << endl;
      s << endl;
    }
    for (int i_s = 0; i_s < osi.n_extended_set_TSV; i_s++){
      s << setw(35) << right << "name_extended_set_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.name_extended_set_TSV[i_s] << endl;
      s << setw(35) << right << "n_scale_ren_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.n_scale_ren_TSV[i_s] << endl;
      s << setw(35) << right << "n_scale_fact_TSV[" << setw(2) << i_s << "]" << setw(5) << "" << "=" << setw(5) << "" << osi.n_scale_fact_TSV[i_s] << endl;
    }

    s << "observable_set output:   n_set_TSV = " << osi.n_set_TSV << endl << endl;
  }
  return s;
}


