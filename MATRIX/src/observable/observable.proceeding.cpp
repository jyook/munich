#include "header.hpp"

int check_proceeding_in(vector<string> & readin, int temp_check_size, int counter, int & int_end){
  Logger logger("check_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  if (temp_check_size > readin.size()){
    int_end = 2;
    logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()  @ counter = " << counter << endl;
  }
  else {
    logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished --- int_end = " << int_end << endl;
  return int_end;
}


void observable_set::perform_proceeding_in(){
  Logger logger("observable_set::perform_proceeding_in");
  logger << LOG_DEBUG << "started" << endl;

  ifstream in_proceeding(filename_proceeding.c_str());
  char LineBuffer[128];
  vector<string> readin;
  while (in_proceeding.getline(LineBuffer, 128)) {
    readin.push_back(LineBuffer);
  }
  in_proceeding.close();
  for (int i = readin.size() - 1; i >=0; i--){
    if (readin[i][0] == '#'){
      logger << LOG_DEBUG << readin[i] << endl;
      readin.erase(readin.begin() + i);
    }
  }
  logger << LOG_DEBUG << "proceeding_in size: " << readin.size() << endl;



  logger << LOG_DEBUG << "Start checking expected content of proceeding_in" << endl;

  int counter = 0;
  int temp_check_size = 0;

  psi->rng.check_proceeding_in(int_end, temp_check_size, readin);

  temp_check_size += 3 + 5 + 2;
  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}

  logger << LOG_DEBUG << "# standard end: temp_check_size = " << temp_check_size << endl;
  logger << LOG_DEBUG << "# switch_CV = " << switch_CV << " (before) = " << temp_check_size << endl;
  if (switch_CV){
    temp_check_size += 2 * output_n_qTcut * n_scales_CV;
    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    logger << LOG_DEBUG << "# CV end: temp_check_size = " << temp_check_size << endl;
  }

  logger << LOG_DEBUG << "# switch_moment = " << switch_moment << " (before) = " << temp_check_size << endl;
  if (switch_moment){
    for (int i_m = 0; i_m < n_moments; i_m++){
      temp_check_size += 2;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      if (switch_CV){
	temp_check_size += 2 * n_qTcut * n_scales_CV;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      }
    }
    logger << LOG_DEBUG << "# n_moments end: temp_check_size = " << temp_check_size << endl;
  }
  logger << LOG_DEBUG << "# switch_distribution = " << switch_distribution << " (before) = " << temp_check_size << endl;
  if (switch_distribution){
    int dist_numbers = 3;
    if (switch_CV){dist_numbers += 2 * n_scales_CV;}
    for (int i_d = 0; i_d < dat.size(); i_d++){
      temp_check_size += dist_numbers * dat[i_d].n_bins;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    for (int i_ddd = 0; i_ddd < dddat.size(); i_ddd++){
      temp_check_size += dist_numbers * dddat[i_ddd].n_bins;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    logger << LOG_DEBUG << "# distribution end: temp_check_size = " << temp_check_size << endl;
  }

  logger << LOG_DEBUG << "# switch_TSV = " << switch_TSV << " (before) = " << temp_check_size << endl;
  if (switch_TSV){
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      temp_check_size += 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      if (active_qTcut){
	temp_check_size += 2 * n_qTcut * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      }
    }
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      if (!switch_moment_TSV[i_s]){continue;}
       temp_check_size += 2 * n_moments * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    int next_no_qTcut = 0;
    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      if (next_no_qTcut == no_qTcut_distribution.size()){continue;}
      if (i_q != no_qTcut_distribution[next_no_qTcut]){continue;}
      for (int i_d = 0; i_d < dat.size(); i_d++){
	for (int i_b = 0; i_b < dat[i_d].n_bins; i_b++){
	  temp_check_size += 1;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    temp_check_size += 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
	    if (temp_check_size > readin.size()){int_end = 2; logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()  @ counter = " << counter << endl; return;}
	    else {logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;}
	  }
	}
      }
      for (int i_d = 0; i_d < dddat.size(); i_d++){
	// not needed here !!!	int i_ddd = dat.size() + i_d;
	for (int i_b = 0; i_b < dddat[i_d].n_bins; i_b++){
	  temp_check_size += 1;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    temp_check_size += 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
	    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  }
	}
      }
      next_no_qTcut++;
    }
    logger << LOG_DEBUG << "# TSV size end: temp_check_size = " << temp_check_size << endl;
  }

  logger << LOG_DEBUG << "# before temp_psi_MC_opt_end   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;

  temp_check_size += 1;
  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}

  int temp_psi_end_optimization = atoi(readin[temp_check_size - 1].c_str());
  logger << LOG_DEBUG << "# temp_psi_end_optimization = " << temp_psi_end_optimization << endl;

  if (temp_psi_end_optimization != 2){
    logger << LOG_DEBUG << "# MC_phasespace   TEST   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;

    // -> psi->MC_phasespace.end_optimization
    logger << LOG_DEBUG << "# MC_phasespace   temp_check_size (before) = " << temp_check_size << endl;
    if (psi->MC_phasespace.active_optimization != -1){
      temp_check_size += 1;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      int temp_psi_MC_phasespace_end_optimization = atoi(readin[temp_check_size - 1].c_str());
      logger << LOG_DEBUG << "# MC_phasespace   temp_psi_MC_phasespace_end_optimization = " << temp_psi_MC_phasespace_end_optimization << endl;
      logger << LOG_DEBUG << "# MC_phasespace   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;
      temp_check_size += psi->MC_phasespace.n_channel;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      if (temp_psi_MC_phasespace_end_optimization == 0){
	temp_check_size += 1;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	int temp_n_opt_steps = atoi(readin[temp_check_size - 1].c_str());
	logger << LOG_DEBUG << "# MC_phasespace   temp_n_opt_steps = " << temp_n_opt_steps << endl;
	temp_check_size += 2;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	temp_check_size += 3 * psi->MC_phasespace.n_channel;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	//      if (temp_check_size != readin.size()){
	if (psi->MC_phasespace.switch_validation_optimization){
	  temp_check_size += 2;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  temp_check_size += temp_n_opt_steps; // -> diff_w
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  temp_check_size += temp_n_opt_steps * psi->MC_phasespace.n_channel; // -> alpha_it
	  logger << LOG_DEBUG << "temp_n_opt_steps = " << temp_n_opt_steps << endl;
	  logger << LOG_DEBUG << "psi->MC_phasespace.n_channel = " << psi->MC_phasespace.n_channel << endl;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	}
      }
    }
    logger << LOG_DEBUG << "# MC_phasespace   finished   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;
    logger << LOG_DEBUG << "# MC_phasespace end: temp_check_size = " << temp_check_size << endl;

    // -> psi->MC_tau.end_optimization
    logger << LOG_DEBUG << "# MC_tau   temp_check_size (before) = " << temp_check_size << endl;
    if (psi->MC_tau.active_optimization != -1){
      temp_check_size += 1;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      int temp_psi_MC_tau_end_optimization = atoi(readin[temp_check_size - 1].c_str());
      logger << LOG_DEBUG << "# MC_tau   temp_psi_MC_tau_end_optimization = " << temp_psi_MC_tau_end_optimization << endl;
      temp_check_size += psi->MC_tau.n_channel;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      if (temp_psi_MC_tau_end_optimization == 0){
	//    if (psi->MC_tau.end_optimization == 0){
	temp_check_size += 1;
	temp_check_size += 2;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	int temp_n_opt_steps = atoi(readin[temp_check_size - 1].c_str());
	logger << LOG_DEBUG << "# MC_tau   temp_n_opt_steps = " << temp_n_opt_steps << endl;
	temp_check_size += 3 * psi->MC_tau.n_channel;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	//      if (temp_check_size != readin.size()){
	if (psi->MC_tau.switch_validation_optimization){
	  temp_check_size += 2;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  temp_check_size += temp_n_opt_steps; // -> diff_w
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  temp_check_size += temp_n_opt_steps * psi->MC_tau.n_channel; // -> alpha_it
	  logger << LOG_DEBUG << "psi->MC_tau.n_channel = " << psi->MC_tau.n_channel << endl;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	}
      }
    }
    logger << LOG_DEBUG << "# MC_tau   finished   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;
    logger << LOG_DEBUG << "# MC_tau end: temp_check_size = " << temp_check_size << endl;

    if (csi->class_contribution_CS_real){
      for (int i_a = 1; i_a < psi->MC_x_dipole.size(); i_a++){
	// -> psi->MC_x_dipole[i_a].end_optimization
	logger << LOG_DEBUG << "# MC_x_dipole[" << i_a << "]   temp_check_size (before) = " << temp_check_size << endl;
	if (psi->MC_x_dipole[i_a].active_optimization != -1){
	  temp_check_size += 1;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  int temp_psi_MC_x_dipole_i_a_end_optimization = atoi(readin[temp_check_size - 1].c_str());
	  logger << LOG_DEBUG << "# temp_psi_MC_x_dipole[" << i_a << "]   end_optimization = " << temp_psi_MC_x_dipole_i_a_end_optimization << endl;
	  temp_check_size += psi->MC_x_dipole[i_a].n_channel;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  if (temp_psi_MC_x_dipole_i_a_end_optimization == 0){
	    //	if (psi->MC_x_dipole[i_a].end_optimization == 0){
	    temp_check_size += 1;
	    temp_check_size += 2;
	    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	    int temp_n_opt_steps = atoi(readin[temp_check_size - 1].c_str());
	    logger << LOG_DEBUG << "# temp_psi_MC_x_dipole[" << i_a << "]   temp_n_opt_steps = " << temp_n_opt_steps << endl;
	    temp_check_size += 3 * psi->MC_x_dipole[i_a].n_channel;
	    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	    //      if (temp_check_size != readin.size()){
	    if (psi->MC_x_dipole[i_a].switch_validation_optimization){
	      temp_check_size += 2;
	      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	      temp_check_size += temp_n_opt_steps; // -> diff_w
	      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	      temp_check_size += temp_n_opt_steps * psi->MC_x_dipole[i_a].n_channel; // -> alpha_it
	      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	    }
	  }
	}
      }
      logger << LOG_DEBUG << "# MC_x_dipole   finished   readin[" << temp_check_size - 1 << "] = " << readin[temp_check_size - 1] << endl;
      logger << LOG_DEBUG << "# MC_x_dipole end: temp_check_size = " << temp_check_size << endl;
    }


    // Add  switch_validation_optimization  distinction for IS variables !!!
  
    logger << LOG_DEBUG << "# IS_tau   temp_check_size (before) = " << temp_check_size << endl;
    temp_check_size += 1;
    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    int temp_psi_tau_opt_end = atoi(readin[temp_check_size - 1].c_str());
    logger << LOG_DEBUG << "# temp_psi_tau_opt_end = " << temp_psi_tau_opt_end << endl;
    if (temp_psi_tau_opt_end != -1){
      temp_check_size += psi->IS_tau.n_gridsize;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    if (temp_psi_tau_opt_end == 0){
      temp_check_size += 2 + 4 * psi->IS_tau.n_gridsize;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    logger << LOG_DEBUG << "# tau_opt end: temp_check_size = " << temp_check_size << endl;

    logger << LOG_DEBUG << "# IS_x1x2   temp_check_size (before) = " << temp_check_size << endl;
    temp_check_size += 1;
    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    int temp_psi_x1x2_opt_end = atoi(readin[temp_check_size - 1].c_str());
    logger << LOG_DEBUG << "# temp_psi_x1x2_opt_end = " << temp_psi_x1x2_opt_end << endl;
    if (temp_psi_x1x2_opt_end != -1){
      temp_check_size += psi->IS_x1x2.n_gridsize;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    if (temp_psi_x1x2_opt_end == 0){
      temp_check_size += 2 + 4 * psi->IS_x1x2.n_gridsize;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    }
    logger << LOG_DEBUG << "# x1x2_opt end: temp_check_size = " << temp_check_size << endl;

    // Re-organize !!! -> temp_psi_z1z2_opt_end inside i_z loop !!!
    logger << LOG_DEBUG << "# IS_z1z2   temp_check_size (before) = " << temp_check_size << endl;
    ///  temp_check_size += 1;
    ///  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    ///  int temp_psi_z1z2_opt_end = atoi(readin[temp_check_size - 1].c_str());
    ///  logger << LOG_DEBUG << "# temp_psi_z1z2_opt_end = " << temp_psi_z1z2_opt_end << endl;
    /// new rng:
    //  if (csi->class_contribution_CS_collinear){
    if (csi->class_contribution_collinear){
      for (int i_z = 1; i_z < 3; i_z++){
	temp_check_size += 1;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	int temp_psi_z1z2_opt_end = atoi(readin[temp_check_size - 1].c_str());
	logger << LOG_DEBUG << "# temp_psi_z1z2_opt_end = " << temp_psi_z1z2_opt_end << endl;
	if (temp_psi_z1z2_opt_end != -1){
	  temp_check_size += psi->input_IS_z1z2.n_gridsize;
	  if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  if (temp_psi_z1z2_opt_end == 0){
	    temp_check_size += 2 + 4 * psi->input_IS_z1z2.n_gridsize;
	    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
	  }
	}
	logger << LOG_DEBUG << "# TSV z1z2_opt end: temp_check_size = " << temp_check_size << endl;
      }
    }

    if (switch_resummation){
      logger << LOG_DEBUG << "# IS_qTres   temp_check_size (before) = " << temp_check_size << endl;
      temp_check_size += 1;
      if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      int temp_psi_qTres_opt_end = atoi(readin[temp_check_size - 1].c_str());
      logger << LOG_DEBUG << "# temp_psi_qTres_opt_end = " << temp_psi_qTres_opt_end << endl;
      if (temp_psi_qTres_opt_end != -1){
	temp_check_size += psi->IS_qTres.n_gridsize;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      }
      if (temp_psi_qTres_opt_end == 0){
	temp_check_size += 2 + 4 * psi->IS_qTres.n_gridsize;
	if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
      }
      logger << LOG_DEBUG << "# qTres_opt end: temp_check_size = " << temp_check_size << endl;
    }

    logger << LOG_DEBUG << "before psi->random_manager.proceeding_in" << endl;


    temp_check_size += 1;
    if (check_proceeding_in(readin, temp_check_size, counter, int_end) == 2){return;}
    int temp_psi_randommanager_end_optimization = atoi(readin[temp_check_size - 1].c_str());
    if (temp_psi_randommanager_end_optimization != 2){
      //  int temp_psi_randommanager_end_optimization = atoi(readin[temp_check_size - 2].c_str());
      logger << LOG_DEBUG << "# temp_psi_randommanager_end_optimization = " << temp_psi_randommanager_end_optimization << endl;
      // check if this works fine !!!
      //  int temp_temp_check_size = temp_check_size;
      psi->random_manager.check_proceeding_in(int_end, temp_check_size, readin);
      logger << LOG_DEBUG << "# random_manager end: temp_check_size = " << temp_check_size << endl;
      //  temp_check_size += (temp_check_size - temp_temp_check_size);
      logger << LOG_DEBUG << "after psi->random_manager.proceeding_in" << endl;
    }
  }

  // Create  check_proceeding_in  functions for multichannel_set and importancesampling_set !!!

  logger << LOG_INFO << "Checking expected content of proceeding_in succesfully finished:" << endl;
  logger << LOG_INFO << "temp_check_size = " << temp_check_size << " ? " << readin.size() << " = readin.size()" << endl;



  int proc = 0;
  psi->rng.proceeding_in(proc, readin);
  h = atoi(readin[proc++].c_str());
  min = atoi(readin[proc++].c_str());
  sec = atoi(readin[proc++].c_str());
  sec_import = sec + 60 * min + 3600 * h;
  logger << LOG_INFO << "Imported runtime: " << h << " h " << min << " min " << sec << " sec   ->   sec_import = " << sec_import << endl;
  psi->i_gen = atol(readin[proc++].c_str());
  psi->i_acc = atol(readin[proc++].c_str());
  psi->i_rej = atol(readin[proc++].c_str());
  psi->i_tec = atoi(readin[proc++].c_str());
  psi->i_nan = atoi(readin[proc++].c_str());
  logger << LOG_INFO << "Importet events: i_gen = " << psi->i_gen << "   i_acc = " << psi->i_acc << "   i_rej = " << psi->i_rej << "   i_tec = " << psi->i_tec << "   i_nan = " << psi->i_nan << endl;

  psi->last_step_mode = *psi->i_step_mode;

  full_sum_weight = hexastr2double(readin[proc]);
  full_sum_weight2 = hexastr2double(readin[proc + 1]);
  proc += 2;
  logger << LOG_DEBUG << "# standard end: proc = " << proc << endl;

  if (switch_CV){
    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      for (int i_s = 0; i_s < n_scales_CV; i_s++){
	full_sum_weight_CV[i_q][i_s] = hexastr2double(readin[proc++]);
	full_sum_weight2_CV[i_q][i_s] = hexastr2double(readin[proc++]);
	///	full_sum_weight_CV[i_q][i_s] = hexastr2double(readin[proc + 2 * (c * n_scales_CV + s)]);
	///	full_sum_weight2_CV[i_q][i_s] = hexastr2double(readin[proc + 1 + 2 * (c * n_scales_CV + s)]);
      }
    }
    ///    proc += 2 * output_n_qTcut * n_scales_CV;
    logger << LOG_DEBUG << "# CV end: proc = " << proc << endl;
  }

  if (switch_moment){
    for (int i_m = 0; i_m < n_moments; i_m++){
      full_sum_moment[i_m] = hexastr2double(readin[proc++]);
      full_sum_moment2[i_m] = hexastr2double(readin[proc++]);
      if (switch_CV){
	for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    full_sum_moment_CV[i_m][i_q][i_s] = hexastr2double(readin[proc++]);
	    full_sum_moment2_CV[i_m][i_q][i_s] = hexastr2double(readin[proc++]);
	    ///	    full_sum_moment_CV[i_m][i_q][i_s] = hexastr2double(readin[proc + 2 * (i_q * n_scales_CV + i_s)]);
	    ///	    full_sum_moment2_CV[i_m][i_q][i_s] = hexastr2double(readin[proc + 1 + 2 * (i_q * n_scales_CV + i_s)]);
	  }
	}
	///	proc += 2 * output_n_qTcut * full_sum_moment_CV[i_m][0].size();
      }
    }
    logger << LOG_DEBUG << "# n_moments end: proc = " << proc << endl;
  }
  if (switch_distribution){
    ///    int dist_numbers = 3;
    ///    if (switch_CV){dist_numbers += 2 * n_scales_CV;}
    for (int i_d = 0; i_d < dat.size(); i_d++){
      for (int i_b = 0; i_b < dat[i_d].n_bins; i_b++){
	bin_counts[i_d][i_b] = atoi(readin[proc++].c_str());
	bin_weight[i_d][i_b] = hexastr2double(readin[proc++]);
	bin_weight2[i_d][i_b] = hexastr2double(readin[proc++]);
	///	bin_counts[i_d][i_b] = atoi(readin[proc + dist_numbers * i_b].c_str());
	///	bin_weight[i_d][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 1]);
	///	bin_weight2[i_d][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2]);
	if (switch_CV){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    bin_weight_CV[i_s][i_d][i_b] = hexastr2double(readin[proc++]);
	    bin_weight2_CV[i_s][i_d][i_b] = hexastr2double(readin[proc++]);
	    ///	    bin_weight_CV[i_s][i_d][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2 * i_s + 3]);
	    ///	    bin_weight2_CV[i_s][i_d][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2 * i_s + 4]);
	  }
	}
      }
      ///      proc += dist_numbers * dat[i_d].n_bins;
    }

    for (int i_ddd = 0; i_ddd < dddat.size(); i_ddd++){
      for (int i_b = 0; i_b < dddat[i_ddd].n_bins; i_b++){
	bin_counts[dat.size() + i_ddd][i_b] = atoi(readin[proc++].c_str());
	bin_weight[dat.size() + i_ddd][i_b] = hexastr2double(readin[proc++]);
	bin_weight2[dat.size() + i_ddd][i_b] = hexastr2double(readin[proc++]);
	///	bin_counts[dat.size() + i_ddd][i_b] = atoi(readin[proc + dist_numbers * i_b].c_str());
	///	bin_weight[dat.size() + i_ddd][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 1]);
	///	bin_weight2[dat.size() + i_ddd][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2]);
	if (switch_CV){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    bin_weight_CV[i_s][dat.size() + i_ddd][i_b] = hexastr2double(readin[proc++]);
	    bin_weight2_CV[i_s][dat.size() + i_ddd][i_b] = hexastr2double(readin[proc++]);
	    ///	    bin_weight_CV[i_s][dat.size() + i_ddd][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2 * i_s + 3]);
	    ///	    bin_weight2_CV[i_s][dat.size() + i_ddd][i_b] = hexastr2double(readin[proc + dist_numbers * i_b + 2 * i_s + 4]);
	  }
	}
      }
      ///      proc += dist_numbers * dddat[i_ddd].n_bins;
    }
    logger << LOG_DEBUG << "# distribution end: proc = " << proc << endl;
  }


  if (switch_TSV){
    logger << LOG_DEBUG << "# TSV input started" << endl;
    logger << LOG_DEBUG << "# TSV size start: proc = " << proc << endl;
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	  fullsum_weight_TSV[i_s][i_r][i_f] = hexastr2double(readin[proc++]);
	  fullsum_weight2_TSV[i_s][i_r][i_f] = hexastr2double(readin[proc++]);
	  ///	  fullsum_weight_TSV[i_s][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f)]);
	  ///	  fullsum_weight2_TSV[i_s][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f) + 1]);
	}
      }
      ///      proc += 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];

      if (active_qTcut){
	for (int i_q = 0; i_q < output_n_qTcut; i_q++){
	  for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	      fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f] = hexastr2double(readin[proc++]);
	      fullsum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f] = hexastr2double(readin[proc++]);
	      ///	      fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_q * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s] + i_r * n_scale_fact_TSV[i_s] + i_f)]);
	      ///	      fullsum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_q * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s] + i_r * n_scale_fact_TSV[i_s] + i_f) + 1]);
	    }
	  }
	}
	///	proc += 2 * output_n_qTcut * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
      }
    }

    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      if (!switch_moment_TSV[i_s]){continue;}
      for (int i_m = 0; i_m < n_moments; i_m++){
	for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	    fullsum_moment_TSV[i_s][i_m][i_r][i_f] = hexastr2double(readin[proc++]);
	    fullsum_moment2_TSV[i_s][i_m][i_r][i_f] = hexastr2double(readin[proc++]);
	    ///	    fullsum_moment_TSV[i_s][i_m][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_m * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s] + i_r * n_scale_fact_TSV[i_s] + i_f)]);
	    ///	    fullsum_moment2_TSV[i_s][i_m][i_r][i_f] = hexastr2double(readin[proc + 2 * (i_m * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s] + i_r * n_scale_fact_TSV[i_s] + i_f) + 1]);
	  }
	}
      }
      ///      proc += 2 * n_moments * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
    }

    logger << LOG_DEBUG << "no_qTcut_distribution.size() = " << no_qTcut_distribution.size() << endl;

    for (int x_q = 0; x_q < no_qTcut_distribution.size(); x_q++){
      logger << LOG_DEBUG << "no_qTcut_distribution[" << x_q << "] = " << no_qTcut_distribution[x_q] << "   selection_qTcut_distribution[" << x_q << "] = " << no_qTcut_distribution[x_q] << endl;
    }

    int next_no_qTcut = 0;
    //    for (int i_q = 0; i_q < no_qTcut_distribution; i_q++){
    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      //    for (int i_q = 0; i_q < n_qTcut; i_q++){
      if (next_no_qTcut == no_qTcut_distribution.size()){continue;}
      if (i_q != no_qTcut_distribution[next_no_qTcut]){continue;}
      logger << LOG_DEBUG << "next_no_qTcut = " << next_no_qTcut << "   " << no_qTcut_distribution.size() << endl;
      logger << LOG_DEBUG << "no_qTcut_distribution[" << next_no_qTcut << "] = " << no_qTcut_distribution[next_no_qTcut] << endl;
      logger << LOG_DEBUG << "readin at i_q = " << i_q << "   proc = " << proc << endl;
      for (int i_d = 0; i_d < dat.size(); i_d++){
	for (int i_b = 0; i_b < dat[i_d].n_bins; i_b++){
	  bin_count_TSV[i_q][i_d][i_b] = atoi(readin[proc++].c_str());
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
		bin_weight_TSV[i_s][i_q][i_r][i_f][i_d][i_b] = hexastr2double(readin[proc++]);
		bin_weight2_TSV[i_s][i_q][i_r][i_f][i_d][i_b] = hexastr2double(readin[proc++]);
		///		bin_weight_TSV[i_s][i_q][i_r][i_f][i_d][i_b] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f)]);
		///		bin_weight2_TSV[i_s][i_q][i_r][i_f][i_d][i_b] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f) + 1]);
	      }
	    }
	    ///	    proc = proc + 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
	  }
	}
      }
      for (int i_d = 0; i_d < dddat.size(); i_d++){
	int i_ddd = dat.size() + i_d;
	for (int i_b = 0; i_b < dddat[i_d].n_bins; i_b++){
	  bin_count_TSV[i_q][i_ddd][i_b] = atoi(readin[proc++].c_str());
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
		bin_weight_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b] = hexastr2double(readin[proc++]);
		bin_weight2_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b] = hexastr2double(readin[proc++]);
		///		bin_weight_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f)]);
		///		bin_weight2_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b] = hexastr2double(readin[proc + 2 * (i_r * n_scale_fact_TSV[i_s] + i_f) + 1]);
	      }
	    }
	    ///	    proc = proc + 2 * n_scale_ren_TSV[i_s] * n_scale_fact_TSV[i_s];
	  }
	}
      }
      logger << LOG_DEBUG << "Before: next_no_qTcut = " << next_no_qTcut << "   i_q = " << i_q << "   no_qTcut_distribution[" << next_no_qTcut << "] = "<< no_qTcut_distribution[next_no_qTcut] << endl;

      next_no_qTcut++;
    }
    logger << LOG_DEBUG << "# TSV size end: proc = " << proc << endl;
    logger << LOG_DEBUG << "# TSV input finished" << endl;
  }


  psi->end_optimization = atoi(readin[proc++].c_str());
  if (psi->end_optimization != 2){
    psi->MC_phasespace.proceeding_in(proc, readin);
    logger << LOG_DEBUG << "# MC_phasespace end: proc = " << proc << endl;
    psi->MC_tau.proceeding_in(proc, readin);
    logger << LOG_DEBUG << "# MC_tau end: proc = " << proc << endl;
    if (csi->class_contribution_CS_real){for (int i_a = 1; i_a < psi->MC_x_dipole.size(); i_a++){psi->MC_x_dipole[i_a].proceeding_in(proc, readin);}}
    logger << LOG_DEBUG << "# MC_x_dipole end: proc = " << proc << endl;
    psi->IS_tau.proceeding_in(proc, readin);
    logger << LOG_DEBUG << "# tau_opt end: proc = " << proc << endl;
    psi->IS_x1x2.proceeding_in(proc, readin);
    logger << LOG_DEBUG << "# x1x2_opt end: proc = " << proc << endl;
    if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){psi->IS_z1z2[i_z].proceeding_in(proc, readin);}}
    logger << LOG_DEBUG << "# TSV z1z2_opt end: proc = " << proc << endl;
    if (switch_resummation){psi->IS_qTres.proceeding_in(proc, readin);}
    logger << LOG_DEBUG << "# TSV qTres_opt end: proc = " << proc << endl;
    psi->random_manager.end_optimization = atoi(readin[proc++].c_str());
    if (psi->random_manager.end_optimization != 2){psi->random_manager.proceeding_in(proc, readin);}
    logger << LOG_DEBUG << "# random_manager end: proc = " << proc << endl;
  }
  
  for (int j = 0; j < psi->tau_MC_tau_gamma[0].size(); j++){logger << LOG_DEBUG_VERBOSE << "tau_MC_tau_gamma[0][" << j << "]" << " = " << setprecision(20) << setw(28) << psi->tau_MC_tau_gamma[0][j] << "   " << double2hexastr(psi->tau_MC_tau_gamma[0][j]) << endl;}

  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::perform_proceeding_out(){
  Logger logger("observable_set::perform_proceeding_out");
  logger << LOG_DEBUG << "started" << endl;

  logger << LOG_DEBUG << "filename_proceeding = " << filename_proceeding << endl;
  ofstream out_proceeding;
  out_proceeding.open(filename_proceeding.c_str(), ofstream::out | ofstream::trunc);
  out_proceeding << "# random number generator (3)" << endl;
  psi->rng.proceeding_out(out_proceeding);
  out_proceeding << "# runtime: h (1) - min (1) - sec (1)" << endl;
  out_proceeding << h << endl;
  out_proceeding << min << endl;
  out_proceeding << sec << endl;
  out_proceeding << "# events: i_gen (1) - i_acc (1) - i_rej (1) - i_tec (1) - i_nan (1)" << endl;
  out_proceeding << psi->i_gen << endl;
  out_proceeding << psi->i_acc << endl;
  out_proceeding << psi->i_rej << endl;
  out_proceeding << psi->i_tec << endl;
  out_proceeding << psi->i_nan << endl;
  out_proceeding << "# cross section: full_sum_weight - full_sum_weight2 (1 x 2)" << endl;
  out_proceeding << double2hexastr(full_sum_weight) << endl;
  out_proceeding << double2hexastr(full_sum_weight2) << endl;

  if (switch_CV){
    // !!! Reduce output by putting output_n_qTcut !!!
    out_proceeding << "# cross section CV: full_sum_weight - full_sum_weight2 (" << n_qTcut << " x " << n_scales_CV << " x 2)" << endl;
    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      for (int i_s = 0; i_s < n_scales_CV; i_s++){
	out_proceeding << double2hexastr(full_sum_weight_CV[i_q][i_s]) << endl;
	out_proceeding << double2hexastr(full_sum_weight2_CV[i_q][i_s]) << endl;
      }
    }
  }

  if (switch_moment){
    out_proceeding << "# moments: full_sum_moment - full_sum_moment2 (" << n_moments << " x 2)" << endl;
    for (int i_m = 0; i_m < n_moments; i_m++){
      out_proceeding << double2hexastr(full_sum_moment[i_m]) << endl;
      out_proceeding << double2hexastr(full_sum_moment2[i_m]) << endl;
    }
    if (switch_CV){
      out_proceeding << "# cross section CV: full_sum_moment - full_sum_moment2 (" << n_moments << " x "  << n_qTcut << " x " << n_scales_CV << " x 2)" << endl;
      for (int i_m = 0; i_m < n_moments; i_m++){
	for (int i_q = 0; i_q < n_qTcut; i_q++){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    out_proceeding << double2hexastr(full_sum_moment_CV[i_m][i_q][i_s]) << endl;
	    out_proceeding << double2hexastr(full_sum_moment2_CV[i_m][i_q][i_s]) << endl;
	  }
	}
      }
    }
  }

  if (switch_distribution){
    // Could be merged by using 'extended_dat' !!!
    for (int i_d = 0; i_d < dat.size(); i_d++){
      out_proceeding << "# distribution CV: bin_counts - bin_weight - bin_weight2 (" << dat[i_d].n_bins << " x 3 + " << n_scales_CV << " x " << dat[i_d].n_bins << " x 2)" << endl;
      for (int i_b = 0; i_b < dat[i_d].n_bins; i_b++){
	out_proceeding << setprecision(18) << bin_counts[i_d][i_b] << endl;
	out_proceeding << double2hexastr(bin_weight[i_d][i_b]) << endl;
	out_proceeding << double2hexastr(bin_weight2[i_d][i_b]) << endl;
	if (switch_CV){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    out_proceeding << double2hexastr(bin_weight_CV[i_s][i_d][i_b]) << endl;
	    out_proceeding << double2hexastr(bin_weight2_CV[i_s][i_d][i_b]) << endl;
	  }
	}
      }
    }
    for (int i_ddd = 0; i_ddd < dddat.size(); i_ddd++){
      out_proceeding << "# dddistribution CV: bin_counts - bin_weight - bin_weight2 (" << dddat[i_ddd].n_bins << " x 3 + " << n_scales_CV << " x " << dddat[i_ddd].n_bins << " x 2)" << endl;
      for (int i_b = 0; i_b < dddat[i_ddd].n_bins; i_b++){
	out_proceeding << setprecision(18) << bin_counts[dat.size() + i_ddd][i_b] << endl;
	out_proceeding << double2hexastr(bin_weight[dat.size() + i_ddd][i_b]) << endl;
	out_proceeding << double2hexastr(bin_weight2[dat.size() + i_ddd][i_b]) << endl;
	if (switch_CV){
	  for (int i_s = 0; i_s < n_scales_CV; i_s++){
	    out_proceeding << double2hexastr(bin_weight_CV[i_s][dat.size() + i_ddd][i_b]) << endl;
	    out_proceeding << double2hexastr(bin_weight2_CV[i_s][dat.size() + i_ddd][i_b]) << endl;
	  }
	}
      }
    }
  }

  if (switch_TSV){
    out_proceeding << "# TSV output" << endl;
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      out_proceeding << "# cross section TSV (" << name_set_TSV[i_s] << "): fullsum_weight_TSV - fullsum_weight2_TSV (" << n_scale_fact_TSV[i_s] << " x " << n_scale_ren_TSV[i_s] << " x 2)" << endl;
      for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	  out_proceeding << double2hexastr(fullsum_weight_TSV[i_s][i_r][i_f]) << endl;
	  out_proceeding << double2hexastr(fullsum_weight2_TSV[i_s][i_r][i_f]) << endl;
	}
      }
      if (active_qTcut){
      out_proceeding << "# cross section qTcut TSV (" << name_set_TSV[i_s] << "): fullsum_weight_TSV - fullsum_weight2_TSV (" << n_qTcut << " x " << n_scale_fact_TSV[i_s] << " x " << n_scale_ren_TSV[i_s] << " x 2)" << endl;
	for (int i_q = 0; i_q < n_qTcut; i_q++){
	  for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	    for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	      out_proceeding << double2hexastr(fullsum_weight_qTcut_TSV[i_s][i_q][i_r][i_f]) << endl;
	      out_proceeding << double2hexastr(fullsum_weight2_qTcut_TSV[i_s][i_q][i_r][i_f]) << endl;
	    }
	  }
	}
      }
    }

    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      if (!switch_moment_TSV[i_s]){continue;}
      out_proceeding << "# moments TSV (" << name_set_TSV[i_s] << "): fullsum_weight_TSV - fullsum_weight2_TSV (" << n_moments << " x " << n_scale_fact_TSV[i_s] << " x " << n_scale_ren_TSV[i_s] << " x 2)" << endl;
      for (int i_m = 0; i_m < n_moments; i_m++){
	for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	  for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
	    out_proceeding << double2hexastr(fullsum_moment_TSV[i_s][i_m][i_r][i_f]) << endl;
	    out_proceeding << double2hexastr(fullsum_moment2_TSV[i_s][i_m][i_r][i_f]) << endl;
	  }
	}
      }
    }
    //    for (int i_q = 0; i_q < 1; i_q++){
    int next_no_qTcut = 0;
    for (int i_q = 0; i_q < output_n_qTcut; i_q++){
      //    for (int i_q = 0; i_q < n_qTcut; i_q++){
      if (next_no_qTcut == no_qTcut_distribution.size()){continue;}
      if (i_q != no_qTcut_distribution[next_no_qTcut]){continue;}

      if (active_qTcut == 0){out_proceeding << "#   no qTcut" << endl;}
      else {out_proceeding << "#   qTcut[" << setw(3) << i_q << "] = " << setw(15) << setprecision(8) << value_qTcut[i_q] << endl;}

      for (int i_d = 0; i_d < dat.size(); i_d++){
	out_proceeding << "#" << char(9) << dat[i_d].xdistribution_name << endl;
	for (int i_b = 0; i_b < dat[i_d].n_bins; i_b++){
	  out_proceeding << bin_count_TSV[i_q][i_d][i_b] << endl;
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
		out_proceeding << double2hexastr(bin_weight_TSV[i_s][i_q][i_r][i_f][i_d][i_b]) << endl;
		out_proceeding << double2hexastr(bin_weight2_TSV[i_s][i_q][i_r][i_f][i_d][i_b]) << endl;
	      }
	    }
	  }
	}
      }
      for (int i_d = 0; i_d < dddat.size(); i_d++){
	int i_ddd = dat.size() + i_d;
	out_proceeding << "#" << char(9) << dddat[i_d].name << endl;
	for (int i_b = 0; i_b < dddat[i_d].n_bins; i_b++){
	  out_proceeding << bin_count_TSV[i_q][i_ddd][i_b] << endl;
	  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	    for (int i_r = 0; i_r < n_scale_ren_TSV[i_s]; i_r++){
	      for (int i_f = 0; i_f < n_scale_fact_TSV[i_s]; i_f++){
		out_proceeding << double2hexastr(bin_weight_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b]) << endl;
		out_proceeding << double2hexastr(bin_weight2_TSV[i_s][i_q][i_r][i_f][i_ddd][i_b]) << endl;
	      }
	    }
	  }
	}
      }

      next_no_qTcut++;
    }
  }

  // One could avoid all the following output for the case of a completely finalized optimization phase (like in all standard non-grid runs) !!!
  // Introduce a general switch like e.g. 'opt_end', and the following is only written if that switch is 0 (or the other way round) !!!
  out_proceeding << "# end_optimization output" << endl;
  out_proceeding << psi->end_optimization << endl;
  if (psi->end_optimization != 2){
    psi->MC_phasespace.proceeding_out(out_proceeding);
    psi->MC_tau.proceeding_out(out_proceeding);
    if (csi->class_contribution_CS_real){for (int i_a = 1; i_a < psi->MC_x_dipole.size(); i_a++){psi->MC_x_dipole[i_a].proceeding_out(out_proceeding);}}

    psi->IS_tau.proceeding_out(out_proceeding);
    psi->IS_x1x2.proceeding_out(out_proceeding);
    ///  out_proceeding << "# IS_z1z2.end_optimization output" << endl;
    if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){psi->IS_z1z2[i_z].proceeding_out(out_proceeding);}}
    if (switch_resummation){psi->IS_qTres.proceeding_out(out_proceeding);}
    out_proceeding << "# random_manager" << endl;
    out_proceeding << psi->random_manager.end_optimization << endl;
    if (psi->random_manager.end_optimization != 2){psi->random_manager.proceeding_out(out_proceeding);}
  }

  out_proceeding.close();

  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::perform_proceeding_check(){
  Logger logger("observable_set::perform_proceeding_check");
  logger << LOG_DEBUG << "started" << endl;

  double Xsection_delta = sqrt((full_sum_weight2 - pow(full_sum_weight, 2) / psi->i_gen)) / (psi->i_gen - 1);
  double Xsection = full_sum_weight / psi->i_gen;
  double weights2_NLO_LO = abs(Xsection_delta / sigma_normalization);

  logger << LOG_DEBUG << "Xsection_delta = " << Xsection_delta << endl;
  logger << LOG_DEBUG << "Xsection = " << Xsection << endl;
  logger << LOG_DEBUG << "weights2_NLO_LO = " << weights2_NLO_LO << endl;
  logger << LOG_DEBUG << "sigma_LO = " << sigma_normalization << endl;
  logger << LOG_DEBUG << "n_events_max = " << psi->n_events_max << endl;
  logger << LOG_DEBUG << "n_step = " << psi->n_step << endl;
  logger << LOG_DEBUG << "psi->i_acc = " << psi->i_acc << endl;
  logger << LOG_DEBUG << "sigma_LO_deviation = " << sigma_normalization_deviation << endl;
  logger << LOG_DEBUG << "switch_output_proceeding = " << switch_output_proceeding << endl;
  logger << LOG_INFO << "(psi->i_acc >= n_events_max) = " << (psi->i_acc >= psi->n_events_max) << endl;
  logger << LOG_INFO << "(psi->i_acc >= n_events_max - n_step) = " << (psi->i_acc >= psi->n_events_max - psi->n_step) << endl;
  logger << LOG_INFO << "(psi->i_acc >= n_events_min && weights2_NLO_LO < sigma_LO_deviation) = " << (psi->i_acc >= psi->n_events_min && weights2_NLO_LO < sigma_normalization_deviation) << endl;

  if (((psi->i_acc >= psi->n_events_max) && switch_output_proceeding == 1) ||
      ((psi->i_acc >= psi->n_events_max - psi->n_step) && switch_output_proceeding == 2) ||
      ((psi->i_acc >= psi->n_events_min) && (weights2_NLO_LO < sigma_normalization_deviation) && (psi->switch_n_events_opt != 2))){int_end = 1;}
  else {
	//      out_integration << "******************************************************* resumption ******************************************************" << endl;
  }

  logger << LOG_INFO << "int_end = " << int_end << endl;

  logger << LOG_DEBUG << "finished" << endl;
}
