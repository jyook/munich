#include "header.hpp"

void observable_set::initialization(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, event_set * _esi, phasespace_set * _psi, user_defined * _user){
  Logger logger("observable_set::observable_set");
  logger << LOG_DEBUG << "called" << endl;

  csi = _csi;
  msi = _msi;
  esi = _esi;
  psi = _psi;
  user = _user;

  initialization_input_predefinition_TSV(isi);
  initialization_set_default();
  initialization_input(isi);
  initialization_after_input();

  // ...

  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::initialization_input_predefinition_TSV(inputparameter_set * isi){
  static  Logger logger("observable_set::initialization_predefinition_TSV");
  logger << LOG_DEBUG << "called" << endl;

  // scale variation related pre-definitions:

  ////////////////////////////////////////
  //  scale variation parameters - TSV  //
  ////////////////////////////////////////

  switch_TSV = 0;
  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    //  for (int i = 0; i < user_variable.size(); i++){
    if (isi->input_observable[i_i].variable == "switch_TSV"){switch_TSV = atoi(isi->input_observable[i_i].value.c_str());}
  }

  ///  int present_scaleset = -1;
  int counter_scaleset = -1;
  name_set_TSV.resize(0);
  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    //  for (int i = 0; i < user_variable.size(); i++){
    if (isi->input_observable[i_i].variable == "name_set_TSV" || isi->input_observable[i_i].variable == "scaleset"){
      int flag = -1;
      for (int i_s = 0; i_s < counter_scaleset + 1; i_s++){
	if (isi->input_observable[i_i].value == name_set_TSV[i_s]){flag = i_s; break;}
      }
      logger << LOG_DEBUG_VERBOSE << "flag = " << flag << endl;
      if (flag == -1){
	counter_scaleset++;
	name_set_TSV.push_back(isi->input_observable[i_i].value);
	///	present_scaleset = counter_scaleset;
      }
      ///      else {present_scaleset = flag;}
    }
  }

  // to deal with differences between two selected scale choices, acting as a local subtraction for each other:

  ///  int present_scalediffset = -1;
  int counter_scalediffset = -1;
  name_diff_set_TSV.resize(0);
  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    //  for (int i = 0; i < user_variable.size(); i++){
    if (isi->input_observable[i_i].variable == "name_diff_set_TSV"){
      int flag = -1;
      for (int i_s = 0; i_s < counter_scalediffset + 1; i_s++){
	if (isi->input_observable[i_i].value == name_diff_set_TSV[i_s]){flag = i_s; break;}
      }
      logger << LOG_DEBUG_VERBOSE << "flag = " << flag << endl;
      if (flag == -1){
	counter_scalediffset++;
	name_diff_set_TSV.push_back(isi->input_observable[i_i].value);
	///	present_scalediffset = counter_scalediffset;
      }
      ///      else {present_scalediffset = flag;}
    }
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::initialization_set_default(){
  static  Logger logger("observable_set::initialization");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_INFO << "number of scalesets: " << name_set_TSV.size() << endl;
  for (int i_s = 0; i_s < name_set_TSV.size(); i_s++){
    logger << LOG_INFO << "name_set_TSV[" << setw(2) << i_s << "] = " << name_set_TSV[i_s] << endl;
  }
  logger.newLine(LOG_DEBUG);

  n_set_TSV = name_set_TSV.size();
  logger << LOG_DEBUG << "n_set_TSV = " << n_set_TSV << endl;

  central_scale_TSV.resize(n_set_TSV);
  central_scale_ren_TSV.resize(n_set_TSV);
  central_scale_fact_TSV.resize(n_set_TSV);
  relative_central_scale_TSV.resize(n_set_TSV);
  relative_central_scale_ren_TSV.resize(n_set_TSV);
  relative_central_scale_fact_TSV.resize(n_set_TSV);

  factor_scale_TSV.resize(n_set_TSV);
  factor_scale_ren_TSV.resize(n_set_TSV);
  factor_scale_fact_TSV.resize(n_set_TSV);

  dynamic_scale_TSV.resize(n_set_TSV);
  dynamic_scale_ren_TSV.resize(n_set_TSV);
  dynamic_scale_fact_TSV.resize(n_set_TSV);
  min_qTcut_TSV.resize(n_set_TSV);
  max_qTcut_TSV.resize(n_set_TSV);
  min_qTcut_distribution_TSV.resize(n_set_TSV);
  max_qTcut_distribution_TSV.resize(n_set_TSV);

  no_central_scale_ren_TSV.resize(n_set_TSV);
  // !!! no input parameter !!!

  no_central_scale_fact_TSV.resize(n_set_TSV);
  // !!! no input parameter !!!

  //  double empty_double = -1.;
  //  double empty_int = -1;
  for (int i_s = 0; i_s < n_set_TSV; i_s++){
    central_scale_TSV[i_s] = empty_double;
    central_scale_ren_TSV[i_s] = empty_double;
    central_scale_fact_TSV[i_s] = empty_double;
    relative_central_scale_TSV[i_s] = empty_double;
    relative_central_scale_ren_TSV[i_s] = empty_double;
    relative_central_scale_fact_TSV[i_s] = empty_double;
    dynamic_scale_TSV[i_s] = empty_int;
    dynamic_scale_ren_TSV[i_s] = empty_int;
    dynamic_scale_fact_TSV[i_s] = empty_int;
    factor_scale_TSV[i_s] = empty_int;
    factor_scale_ren_TSV[i_s] = empty_int;
    factor_scale_fact_TSV[i_s] = empty_int;
    min_qTcut_TSV[i_s] = empty_double;
    max_qTcut_TSV[i_s] = empty_double;
    min_qTcut_distribution_TSV[i_s] = empty_double;
    max_qTcut_distribution_TSV[i_s] = empty_double;
  }

  // to deal with differences between two selected scale choices, acting as a local subtraction for each other:

  n_diff_set_TSV = name_diff_set_TSV.size();

  name_diff_set_plus_TSV.resize(n_diff_set_TSV, "");
  name_diff_set_minus_TSV.resize(n_diff_set_TSV, "");
  no_diff_set_plus_TSV.resize(n_diff_set_TSV, -1);
  no_diff_set_minus_TSV.resize(n_diff_set_TSV, -1);

  n_extended_set_TSV = n_set_TSV + n_diff_set_TSV;

  n_scale_TSV.resize(n_extended_set_TSV);
  n_scale_ren_TSV.resize(n_extended_set_TSV);
  n_scale_fact_TSV.resize(n_extended_set_TSV);

  switch_distribution_TSV.resize(n_extended_set_TSV);
  switch_moment_TSV.resize(n_extended_set_TSV);

  max_n_integrand_TSV.resize(n_extended_set_TSV);
  // !!! no input parameter !!!

  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
    n_scale_TSV[i_s] = empty_int;
    n_scale_ren_TSV[i_s] = empty_int;
    n_scale_fact_TSV[i_s] = empty_int;

    switch_distribution_TSV[i_s] = empty_int;
    switch_moment_TSV[i_s] = empty_int;
    logger << LOG_DEBUG_VERBOSE << "SSSSS   switch_distribution_TSV[" << i_s << "] = " << switch_distribution_TSV[i_s] << endl;
  }

  name_extended_set_TSV.resize(n_extended_set_TSV, "");
  for (int i_s = 0; i_s < n_set_TSV; i_s++){
    name_extended_set_TSV[i_s] = name_set_TSV[i_s];
  }
  for (int i_s = 0; i_s < n_diff_set_TSV; i_s++){
    name_extended_set_TSV[n_set_TSV + i_s] = name_diff_set_TSV[i_s];
  }

  switch_reference = "";

  name_reference_TSV = "" ;
  no_reference_TSV = -1;
  no_scale_ren_reference_TSV = -1;
  no_scale_fact_reference_TSV = -1;
  no_qTcut_reference_TSV = 0;
  // end

  //////////////////////////
  //  unknown parameters  //
  //////////////////////////

  // Should be removed completely:
  path_to_main = "../../../../";
  switch_old_qT_version = 1;

  // ??? should be replaced by input file !!!
  n_moments = 0;

  ////////////////////////////////////////////////
  //  switches to steer calculation of results  //
  ////////////////////////////////////////////////

  switch_result = 1;
  switch_distribution = 1;
  switch_moment = 0;

  ///////////////////////////////////////////////
  //  switches to steer output of calculation  //
  ///////////////////////////////////////////////

  switch_output_execution = 1;
  switch_output_integration = 1;
  switch_output_maxevent = 1;
  switch_output_comparison = 1;
  switch_output_gnuplot = 1;
  switch_output_proceeding = 1;
  ///  switch_output_weights = 1;
  switch_output_result = 1;
  switch_output_time = 1;
  switch_output_moment = 0;
  switch_output_distribution = 1;

  switch_output_testpoint = 0;
  switch_output_cancellation_check = 0;
  switch_output_cutinfo = 0;

  switch_testcut = 0; // ???

  switch_console_output_runtime = 2;
  switch_console_output_tau_0 = 1;
  switch_console_output_techcut_RA = 1;
  switch_console_output_phasespace_issue = 1;
  switch_console_output_ME2_issue = 1;

  ////////////////////////////////////////////////////////////////////////
  //  unit of calculation output / result output / distribution output  //
  ////////////////////////////////////////////////////////////////////////

  unit_calculation = "fb";
  unit_result = "fb";
  unit_distribution = "fb";

  ///////////////////////
  //  beam parameters  //
  ///////////////////////

  E_beam = empty_double;
  E_CMS = empty_double;
  coll_choice = 0;

  ///////////
  //  PDF  //
  ///////////

  N_f_active = 0;
  pdf_selection.resize(0);
  pdf_disable.resize(0);

  ////////////////////
  //  PDF - LHAPDF  //
  ////////////////////

  LHAPDFname = "";
  LHAPDFsubset = 0;

  ////////////////////////////////////////////////////////
  //  input that still needs to be reasonably assigned  //
  ////////////////////////////////////////////////////////

  // Needed here (e.g. aplitting functions) !!! Should be synchronized with amplitude input !!!
  N_f = 0;
  // amplitude_set:
  N_quarks = 6;
  // actually no input:
  N_nondecoupled = 0;

  /////////////////////////////////
  //  qT-subtraction parameters  //
  /////////////////////////////////
  // Only in qTsubtraction_basic ???
  switch_qTcut = 0;
  n_qTcut = 0;
  min_qTcut = 0.;
  step_qTcut = 0.;
  max_qTcut = 0.;
  binning_qTcut = "linear";
  selection_qTcut = "";

  selection_qTcut_distribution = "";
  selection_no_qTcut_distribution = "";
  selection_qTcut_result = "";
  selection_no_qTcut_result = "";
  selection_qTcut_integration = "";
  selection_no_qTcut_integration = "";


  //////////////////////////////
  //  integration parameters  //
  //////////////////////////////

  sigma_normalization = 0.;
  sigma_normalization_deviation = 0.;

  ///////////////////////////////
  //  scale-choice parameters  //
  ///////////////////////////////

  scale_fact = 0.;
  scale_ren = 0.;
  dynamic_scale = 0;
  prefactor_reference = 1.;

  ///////////////////////////////////////
  //  scale variation parameters - CV  //
  ///////////////////////////////////////

  switch_CV = 0;
  n_scales_CV = 0;
  dynamic_scale_CV = 0;
  variation_mu_ren_CV = 0;
  // ???

  variation_mu_fact_CV = 0;
  // ???

  variation_factor_CV = 0;
  central_scale_CV = 0;
  // ???

  prefactor_CV = 1.;

  ////////////////////////////////////////////////////
  //  technical switches for selected contributions //
  ////////////////////////////////////////////////////
  // actually amplitudes, but also in I-operator etc. !!!
  switch_polenorm = 0;

  switch_KP = 0;
  switch_VI = 0;
  switch_VI_bosonic_fermionic = 0;

  // rename to switch_off_... ???
  switch_H1gg = 0;
  switch_H2 = 0;

  // Should be replaced by switch_amplitude = OL / RCL / ... - and not be part of osi (-> isi) !!!
  switch_OL = 1;
  switch_RCL = 0;

  // part of JY's implementation for Higgs interferences !!!
  switch_yuk = 0;
  order_y = 0;  // yukawa coupling squared

  // meaning ???
  switch_CM = 0;

  // meaning ???
  switch_RS = 0;

  /////////////////////////////////
  //  qT-resummation parameters  //
  /////////////////////////////////

  switch_resummation = 0;
  switch_dynamic_Qres = 0;
  QT_Qres = 0.;
  QT_Qres_prefactor = 1.;

  ////////////////////////
  //  other parameters  //
  ////////////////////////
  // mainly to avoid warnings ??? !!!
  rescaling_factor_alpha_e = 1.;
  alpha_S = 1.;


  // a lot of default input seems to be missing here !!!
  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::initialization_input(inputparameter_set * isi){
  static  Logger logger("observable_set::initialization");
  logger << LOG_DEBUG << "called" << endl;

  int temp_type_perturbative_order = -1;
  int temp_type_contribution = -1;
  int temp_type_correction = -1;

  // Error message if  present_scaleset / present_scalediffset  is not set !!!
  int present_scaleset = -1;
  int present_scalediffset = -1;

  for (int i_i = 0; i_i < isi->input_observable.size(); i_i++){
    logger << LOG_DEBUG_VERBOSE << "isi->input_observable[" << i_i << "] = " << isi->input_observable[i_i] << endl;

    if (isi->input_observable[i_i].variable == ""){}

    //////////////////////////
    //  unknown parameters  //
    //////////////////////////
    // needed ???
    else if (isi->input_observable[i_i].variable == "path_to_main"){path_to_main = isi->input_observable[i_i].value;}
    /////    else if (isi->input_observable[i_i].variable == "ckm_choice"){ckm_choice = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_moments"){n_moments = atoi(isi->input_observable[i_i].value.c_str());}
    /////    else if (isi->input_observable[i_i].variable == "test_output"){test_output = atoi(isi->input_observable[i_i].value.c_str());}

    ////////////////////////////////////////////////
    //  switches to steer calculation of results  //
    ////////////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "switch_distribution"){switch_distribution = atoi(isi->input_observable[i_i].value.c_str());}
    // to be removed...    else if (isi->input_observable[i_i].variable == "distributions"){switch_distribution = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_moment"){switch_moment = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_result"){switch_result = atoi(isi->input_observable[i_i].value.c_str());}

    ///////////////////////////////////////////////
    //  switches to steer output of calculation  //
    ///////////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "switch_output_execution"){switch_output_execution = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_integration"){switch_output_integration = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_maxevent"){switch_output_maxevent = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_cancellation_check"){switch_output_cancellation_check = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_comparison"){switch_output_comparison = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_gnuplot"){switch_output_gnuplot = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_proceeding"){switch_output_proceeding = atoi(isi->input_observable[i_i].value.c_str());}
    ///    else if (isi->input_observable[i_i].variable == "switch_output_weights"){switch_output_weights = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_result"){switch_output_result = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_time"){switch_output_time = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_moment"){switch_output_moment = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_distribution"){switch_output_distribution = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_output_testpoint"){switch_output_testpoint = atoi(isi->input_observable[i_i].value.c_str());}

    else if (isi->input_observable[i_i].variable == "switch_console_output_runtime"){switch_console_output_runtime = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_console_output_tau_0"){switch_console_output_tau_0 = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_console_output_techcut_RA"){switch_console_output_techcut_RA = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_console_output_phasespace_issue"){switch_console_output_phasespace_issue = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_console_output_ME2_issue"){switch_console_output_ME2_issue = atoi(isi->input_observable[i_i].value.c_str());}

    ////////////////////////////////////////////////////////////////////////
    //  unit of calculation output / result output / distribution output  //
    ////////////////////////////////////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "unit_calculation"){unit_calculation = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "unit_result"){unit_result = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "unit_distribution"){unit_distribution = isi->input_observable[i_i].value;}

    ///////////////////////
    //  beam parameters  //
    ///////////////////////

    // maybe not only in psi !!!
    else if (isi->input_observable[i_i].variable == "E"){E_beam = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "E_beam"){E_beam = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "E_CMS"){E_CMS = atof(isi->input_observable[i_i].value.c_str());}
    // maybe not only in psi !!!
    else if (isi->input_observable[i_i].variable == "coll_choice"){coll_choice = atoi(isi->input_observable[i_i].value.c_str());}

    ///////////
    //  PDF  //
    ///////////

    else if (isi->input_observable[i_i].variable == "N_f_active"){N_f_active = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "pdf_selection"){
      //  Reasonable non-overlapping combinations (flavour-blind):
      //  qq,qxqx,gg,aa,qqx,qxq,gq,qg,gqx,qxg,aq,qa,aqx,qxa,ga,ag
      //  Q := q + qx  ->  e.g.  QQ = qqx + qxq + qq + qxqx
      //  Q/q/qx can be replaced by defined flavour.
      //  D := d + dx, U := u + ux, S := s + sx, C := c + cx, B := b + bx, T := t + tx
      if (isi->input_observable[i_i].value == "clear"){pdf_selection.clear();}
      else {pdf_selection.push_back(isi->input_observable[i_i].value);}
    }
    else if (isi->input_observable[i_i].variable == "pdf_disable"){
      //  Partons whose pdf's can be switched off:
      //  g,a,Q,q,qx,D,d,dx,U,u,ux,S,s,sx,C,c,cx,B,b,bx,T,t,tx
      if (isi->input_observable[i_i].value == "clear"){pdf_disable.clear();}
      else {pdf_disable.push_back(isi->input_observable[i_i].value);}
    }

    ////////////////////
    //  PDF - LHAPDF  //
    ////////////////////

    else if (isi->input_observable[i_i].variable == "LHAPDFname"){
      isi->contribution_LHAPDFname[temp_type_perturbative_order] = isi->input_observable[i_i].value;
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << setw(32) << "isi->contribution_LHAPDFname[temp_type_perturbative_order = " << temp_type_perturbative_order << "] = " << isi->contribution_LHAPDFname[temp_type_perturbative_order] << endl;
    }
    else if (isi->input_observable[i_i].variable == "LHAPDFsubset"){
      isi->contribution_LHAPDFsubset[temp_type_perturbative_order] = atoi(isi->input_observable[i_i].value.c_str());
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << setw(32) << "isi->contribution_LHAPDFsubset[temp_type_perturbative_order = " << temp_type_perturbative_order << "] = " << isi->contribution_LHAPDFsubset[temp_type_perturbative_order] << endl;
    }

    ////////////////////////////////////////////////////////
    //  input that still needs to be reasonably assigned  //
    ////////////////////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "N_f"){N_f = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "N_quarks"){N_quarks = atoi(isi->input_observable[i_i].value.c_str());}
    // !! actually set by the Standard Model

    else if (isi->input_observable[i_i].variable == "N_nondecoupled"){N_nondecoupled = atoi(isi->input_observable[i_i].value.c_str());}
    // !! actually internal parameter


    /////////////////////////////////
    //  qT-subtraction parameters  //
    /////////////////////////////////

    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "switch_qTcut"){switch_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_qTcut"){n_qTcut = atoi(isi->input_observable[i_i].value.c_str());}
    // needed elsewhere ???
    else if (isi->input_observable[i_i].variable == "min_qTcut"){min_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "step_qTcut"){step_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "max_qTcut"){max_qTcut = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "binning_qTcut"){binning_qTcut = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "selection_qTcut"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut = "";}
      else {selection_qTcut = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_distribution = "";}
      else {selection_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_distribution"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_distribution = "";}
      else {selection_no_qTcut_distribution = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_result = "";}
      else {selection_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_result"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_result = "";}
      else {selection_no_qTcut_result = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_qTcut_integration = "";}
      else {selection_qTcut_integration = isi->input_observable[i_i].value;}
    }
    else if (isi->input_observable[i_i].variable == "selection_no_qTcut_integration"){
      if (isi->input_observable[i_i].value == "clear"){selection_no_qTcut_integration = "";}
      else {selection_no_qTcut_integration = isi->input_observable[i_i].value;}
    }




    //////////////////////////////
    //  integration parameters  //
    //////////////////////////////

    else if (isi->input_observable[i_i].variable == "sigma_normalization"){sigma_normalization = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "sigma_normalization_deviation"){sigma_normalization_deviation = atof(isi->input_observable[i_i].value.c_str());}

    // to be removed...
    else if (isi->input_observable[i_i].variable == "sigma_LO"){sigma_normalization = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "sigma_LO_deviation"){sigma_normalization_deviation = atof(isi->input_observable[i_i].value.c_str());} // to be removed...

    ///////////////////////////////
    //  scale-choice parameters  //
    ///////////////////////////////

    else if (isi->input_observable[i_i].variable == "scale_fact"){scale_fact = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "scale_ren"){scale_ren = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "dynamic_scale"){dynamic_scale = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "prefactor_reference"){prefactor_reference = atof(isi->input_observable[i_i].value.c_str());}

    ///////////////////////////////////////
    //  scale variation parameters - CV  //
    ///////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "switch_CV"){switch_CV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "variation_CV"){switch_CV = atoi(isi->input_observable[i_i].value.c_str());} // !!! to be removed
    else if (isi->input_observable[i_i].variable == "n_scales_CV"){n_scales_CV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "dynamic_scale_CV"){dynamic_scale_CV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "variation_mu_ren_CV"){variation_mu_ren_CV = atoi(isi->input_observable[i_i].value.c_str());}
    // ???

    else if (isi->input_observable[i_i].variable == "variation_mu_fact_CV"){variation_mu_fact_CV = atoi(isi->input_observable[i_i].value.c_str());}
    // ???

    else if (isi->input_observable[i_i].variable == "variation_factor_CV"){variation_factor_CV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "central_scale_CV"){central_scale_CV = atoi(isi->input_observable[i_i].value.c_str());}
    // ???

    else if (isi->input_observable[i_i].variable == "prefactor_CV"){prefactor_CV = atof(isi->input_observable[i_i].value.c_str());}

    ////////////////////////////////////////
    //  scale variation parameters - TSV  //
    ////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "switch_TSV"){switch_TSV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "name_set_TSV" || isi->input_observable[i_i].variable == "scaleset"){ // to be removed...
      for (int i_s = 0; i_s < n_set_TSV; i_s++){
	if (isi->input_observable[i_i].value == name_set_TSV[i_s]){present_scaleset = i_s; break;}
      }
    }
    else if (isi->input_observable[i_i].variable == "central_scale_TSV"){central_scale_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "central_scale_ren_TSV"){central_scale_ren_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "central_scale_fact_TSV"){central_scale_fact_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "relative_central_scale_TSV"){relative_central_scale_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "relative_central_scale_ren_TSV"){relative_central_scale_ren_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "relative_central_scale_fact_TSV"){relative_central_scale_fact_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_scale_TSV"){n_scale_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_scale_ren_TSV"){n_scale_ren_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "n_scale_fact_TSV"){n_scale_fact_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "factor_scale_TSV"){factor_scale_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "factor_scale_ren_TSV"){factor_scale_ren_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "factor_scale_fact_TSV"){factor_scale_fact_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "dynamic_scale_TSV"){dynamic_scale_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "dynamic_scale_ren_TSV"){dynamic_scale_ren_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "dynamic_scale_fact_TSV"){dynamic_scale_fact_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "min_qTcut_TSV"){min_qTcut_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "max_qTcut_TSV"){max_qTcut_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_distribution_TSV"){switch_distribution_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "min_qTcut_distribution_TSV"){min_qTcut_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "max_qTcut_distribution_TSV"){max_qTcut_TSV[present_scaleset] = atof(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "switch_moment_TSV"){switch_moment_TSV[present_scaleset] = atoi(isi->input_observable[i_i].value.c_str());}

    else if (isi->input_observable[i_i].variable == "name_diff_set_TSV"){ // to be removed...
      for (int i_s = 0; i_s < n_diff_set_TSV; i_s++){
	if (isi->input_observable[i_i].value == name_diff_set_TSV[i_s]){present_scalediffset = i_s; break;}
      }
    }
    else if (isi->input_observable[i_i].variable == "name_diff_set_plus_TSV"){name_diff_set_plus_TSV[present_scalediffset] = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "name_diff_set_minus_TSV"){name_diff_set_minus_TSV[present_scalediffset] = isi->input_observable[i_i].value;}

    else if (isi->input_observable[i_i].variable == "switch_reference"){switch_reference = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "name_reference_TSV"){name_reference_TSV = isi->input_observable[i_i].value;}
    else if (isi->input_observable[i_i].variable == "no_reference_TSV"){no_reference_TSV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "no_scale_ren_reference_TSV"){no_scale_ren_reference_TSV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "no_scale_fact_reference_TSV"){no_scale_fact_reference_TSV = atoi(isi->input_observable[i_i].value.c_str());}
    else if (isi->input_observable[i_i].variable == "no_qTcut_reference_TSV"){no_qTcut_reference_TSV = atoi(isi->input_observable[i_i].value.c_str());}


    ////////////////////////////////////////////////////
    //  technical switches for selected contributions //
    ////////////////////////////////////////////////////

    else if (isi->input_observable[i_i].variable == "switch_polenorm"){switch_polenorm = atoi(isi->input_observable[i_i].value.c_str());}
    // to be removed...
    else if (isi->input_observable[i_i].variable == "polenorm_switch"){switch_polenorm = atoi(isi->input_observable[i_i].value.c_str());}
    // to be removed...
    else if (isi->input_observable[i_i].variable == "switch_old_qT_version"){switch_old_qT_version = atoi(isi->input_observable[i_i].value.c_str());}


    //////////////////////////////////////////////////////////////
    //  selection of process and contribution to be calculated  //
    //////////////////////////////////////////////////////////////

    // Maybe use version from isi !!!

    else if (isi->input_observable[i_i].variable == "type_perturbative_order"){
      temp_type_perturbative_order = -1;
      for (int i_to = 0; i_to < isi->collection_type_perturbative_order.size(); i_to++){
	if (isi->input_observable[i_i].value == "all"){temp_type_perturbative_order = isi->present_type_perturbative_order; break;}
	else if (isi->input_observable[i_i].value == isi->collection_type_perturbative_order[i_to]){temp_type_perturbative_order = i_to; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << setw(32) << "temp_type_perturbative_order = " << temp_type_perturbative_order << endl;
    }
    else if (isi->input_observable[i_i].variable == "type_contribution"){
      temp_type_contribution = -1;
      for (int i_tc = 0; i_tc < isi->collection_type_contribution.size(); i_tc++){
	if (isi->input_observable[i_i].value == "all"){temp_type_contribution = isi->present_type_contribution; break;}
	else if (isi->input_observable[i_i].value == isi->collection_type_contribution[i_tc]){temp_type_contribution = i_tc; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << setw(32) << "temp_type_contribution = " << temp_type_contribution << endl;
    }
    else if (isi->input_observable[i_i].variable == "type_correction"){
      temp_type_correction = -1;
      for (int i_tc = 0; i_tc < isi->collection_type_correction.size(); i_tc++){
	if (isi->input_observable[i_i].value == "all"){temp_type_correction = isi->present_type_correction; break;}
	else if (isi->input_observable[i_i].value == isi->collection_type_correction[i_tc]){temp_type_correction = i_tc; break;}
      }
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << setw(32) << left << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << setw(32) << "temp_type_correction = " << temp_type_correction << endl;
    }


    else if ((temp_type_perturbative_order == isi->present_type_perturbative_order &&
	      temp_type_contribution == isi->present_type_contribution &&
	      temp_type_correction == isi->present_type_correction)){
      logger << LOG_DEBUG << "user_variable[" << i_i << "] = " << left << setw(32) << isi->input_observable[i_i].variable << " = " << setw(32) << isi->input_observable[i_i].value << endl;

      ////////////////////////////////////////////////////
      //  technical switches for selected contributions //
      ////////////////////////////////////////////////////

      if (isi->input_observable[i_i].variable == ""){}
      else if (isi->input_observable[i_i].variable == "switch_KP"){switch_KP = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_VI"){switch_VI = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_VI_bosonic_fermionic"){switch_VI = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_H1gg"){switch_H1gg = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_H2"){switch_H2 = atoi(isi->input_observable[i_i].value.c_str());}

      else if (isi->input_observable[i_i].variable == "switch_CM"){switch_CM = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_OL"){switch_OL = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_RCL"){switch_RCL = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_RS"){switch_RS = atoi(isi->input_observable[i_i].value.c_str());}

      /////////////////////////////////
      //  qT-resummation parameters  //
      /////////////////////////////////

      // All qT-resummation parameters: not only in psi ???
      else if (isi->input_observable[i_i].variable == "switch_resummation"){switch_resummation = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "switch_dynamic_Qres"){switch_dynamic_Qres = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "Qres"){QT_Qres = atof(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "Qres_prefactor"){QT_Qres_prefactor = atof(isi->input_observable[i_i].value.c_str());}


      //////////////////////////////////////////////////////////////////
      //  parameters in context of Higgs interference implementation  //
      //////////////////////////////////////////////////////////////////

      else if (isi->input_observable[i_i].variable == "switch_yuk"){switch_yuk = atoi(isi->input_observable[i_i].value.c_str());}
      else if (isi->input_observable[i_i].variable == "order_y"){order_y = atoi(isi->input_observable[i_i].value.c_str());}

      //////////////////////////////
      //  integration parameters  //
      //////////////////////////////

      // next three not only in psi ???
      ///      else if (isi->input_observable[i_i].variable == "n_events_max"){n_events_max = atoi(isi->input_observable[i_i].value.c_str());}
      ///      else if (isi->input_observable[i_i].variable == "n_events_min"){n_events_min = atoi(isi->input_observable[i_i].value.c_str());}
      ///      else if (isi->input_observable[i_i].variable == "n_step"){n_step = atoi(isi->input_observable[i_i].value.c_str());}

    }
  }

  // !!! check position and isi dependence !!!
  if (isi->present_type_perturbative_order > -1){
    LHAPDFname = isi->contribution_LHAPDFname[isi->present_type_perturbative_order];
    LHAPDFsubset = isi->contribution_LHAPDFsubset[isi->present_type_perturbative_order];
  }

  logger << LOG_DEBUG << "finished" << endl;
}


void observable_set::initialization_after_input(){
  static  Logger logger("observable_set::initialization");
  logger << LOG_DEBUG << "called" << endl;

  // moved from constructor...
  int_end = 0;
  max_dyn_ren = 0;
  max_dyn_fact = 0;

  /////////////////////////
  //  beam / CMS energy  //
  /////////////////////////

  if (E_beam == empty_double && E_CMS == empty_double){
    logger << LOG_ERROR << "E_beam and E_CMS have not been set." << endl;
    exit(1);
  }
  else if (E_beam != empty_double && E_CMS == empty_double){E_CMS = E_beam * 2;}
  else if (E_beam == empty_double && E_CMS != empty_double){E_beam = E_CMS / 2;}
  else {
    if (E_CMS != E_beam * 2){
      logger << LOG_ERROR << "E_beam = " << E_beam << " and E_CMS = " << E_CMS << " are inconsistent." << endl;
      exit(1);
    }
  }
      //  E_beam = E;
      //  E_CMS = 2 * E_beam;

  logger << LOG_INFO << "csi->order_alpha_s_born = " << setw(23) << setprecision(15) << csi->order_alpha_s_born << endl;
  // check if this causes problems !!!
  // logger << LOG_INFO << "order_alphas_born = " << order_alphas_born << endl;
  logger << LOG_INFO << "csi->n_jet_born (no of Born-level jets) = " << csi->n_jet_born << endl;

  initialization_unit();

  ////////////////////
  //  PDF - LHAPDF  //
  ////////////////////
  /*
  // !!! check
  if (isi->present_type_perturbative_order > -1){
    LHAPDFname = isi->contribution_LHAPDFname[isi->present_type_perturbative_order];
    LHAPDFsubset = isi->contribution_LHAPDFsubset[isi->present_type_perturbative_order];
  }
  */
  initialization_LHAPDF();
  logger << LOG_DEBUG << "LHAPDF has been initialized." << endl;

  // rescaling_factor_alpha_e needs to be reconsidered !!!
  // Rescaling is automatically done in OpenLoops (might need to be added for RECOLA) !!!

  logger << LOG_INFO << "rescaling_factor_alpha_e = " << setw(23) << setprecision(15) << rescaling_factor_alpha_e << endl;
  rescaling_factor_alpha_e = 1.;

  logger << LOG_INFO << "alpha_S at various scales for " << LHAPDFname << ":" << endl;
  logger << LOG_INFO << "alpha_S(M_W = " << setprecision(5) << setw(7) << msi->M_W << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi->M_W) << endl;
  logger << LOG_INFO << "alpha_S(M_Z = " << setprecision(6) << setw(7) << msi->M_Z << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi->M_Z) << endl;
  logger << LOG_INFO << "alpha_S(M_t = " << setprecision(4) << setw(7) << msi->M_t << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi->M_t) << endl;
  if (msi->M_b != 0.){logger << LOG_INFO << "alpha_S(M_b = " << setprecision(3) << setw(7) << msi->M_b << " GeV)) = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(msi->M_b) << endl;}
  logger << LOG_INFO << "alpha_S(1 TeV)   = " << setw(23) << setprecision(15) << LHAPDF::alphasPDF(1000.) << endl;

 // really needed ???
  initialization_masses(msi->M, msi->M2);

  initialization_switches();
  initialization_qTcut();
  initialization_integration_parameters();
  initialization_basic_CV();
  initialization_basic_TSV();
  if (csi->subprocess != ""){initialization_filename();}




  // to be continued !!!
  // done until msi initialization, which has to be moved !!!

  // part on scale_reference etc. needs to be imported from isi !!!
  // First attempt: move it here completely:
  // Could be partially or completely shifted to TSV initialization !!!

  for (int i_s = 0; i_s < n_diff_set_TSV; i_s++){
    for (int j_s = 0; j_s < n_set_TSV; j_s++){
      if (name_diff_set_plus_TSV[i_s] == name_set_TSV[j_s]){no_diff_set_plus_TSV[i_s] = j_s;}
      if (name_diff_set_minus_TSV[i_s] == name_set_TSV[j_s]){no_diff_set_minus_TSV[i_s] = j_s;}
    }
    if (no_diff_set_plus_TSV[i_s] == -1){
      logger << LOG_FATAL << "name_diff_set_TSV[" << i_s << "] = " << name_diff_set_TSV[i_s] << "   has an invalid plus scale: name_diff_set_plus_TSV[" << i_s << "] = " << name_diff_set_plus_TSV[i_s] << endl;
      exit(1);
    }
    if (no_diff_set_minus_TSV[i_s] == -1){
      logger << LOG_FATAL << "name_diff_set_TSV[" << i_s << "] = " << name_diff_set_TSV[i_s] << "   has an invalid minus scale: name_diff_set_minus_TSV[" << i_s << "] = " << name_diff_set_minus_TSV[i_s] << endl;
      exit(1);
    }
  }

  for (int i_s = 0; i_s < n_set_TSV; i_s++){
    logger << LOG_INFO << "name_set_TSV[" << i_s << "] = " << name_set_TSV[i_s] << endl;
  }
  logger.newLine(LOG_INFO);
  for (int i_s = 0; i_s < n_diff_set_TSV; i_s++){
    logger << LOG_INFO << "name_diff_set_TSV[" << i_s << "] = " << name_diff_set_TSV[i_s] << endl;
  }
  logger.newLine(LOG_INFO);
  for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
    logger << LOG_INFO << "name_extended_set_TSV[" << i_s << "] = " << name_extended_set_TSV[i_s] << endl;
  }
  logger.newLine(LOG_INFO);

  // set switch_reference if not selected: could be shifted to observable_set !!!
  if (switch_reference == ""){
    if (switch_TSV && switch_CV){
      if (name_reference_TSV != ""){switch_reference = "TSV";}
      else {switch_reference = "CV";}
    }
    else if (switch_TSV){switch_reference = "TSV";}
    else if (switch_CV){switch_reference = "CV";}
  }
  // stop if switch_reference if not reasonably set (no results would be produced):
  if (switch_reference == ""){
    logger << LOG_FATAL << "No reasonable reference scale is set." << endl;
    exit(1);
  }

  // A number of variables need to be filled !!!
  if (switch_TSV){
    //    logger << LOG_INFO << "name_reference_TSV          = " << name_reference_TSV << endl;
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      //      logger << LOG_INFO << "name_extended_set_TSV[" << i_s << "] = " << name_extended_set_TSV[i_s] << endl;
      if (name_reference_TSV == name_extended_set_TSV[i_s]){
	no_reference_TSV = i_s;
	//	logger << LOG_INFO << "no_reference_TSV          = " << no_reference_TSV << endl;
	break;
      }
    }

    if (no_reference_TSV){
      logger << LOG_INFO << "default output from TSV:" << endl;
      logger << LOG_INFO << "name_reference_TSV          = " << name_reference_TSV << endl;
      logger << LOG_INFO << "no_reference_TSV            = " << no_reference_TSV << endl;
      logger << LOG_INFO << "no_scale_ren_reference_TSV  = " << no_scale_ren_reference_TSV << endl;
      logger << LOG_INFO << "no_scale_fact_reference_TSV = " << no_scale_fact_reference_TSV << endl;
      logger << LOG_INFO << "no_qTcut_reference_TSV      = " << no_qTcut_reference_TSV << endl;
    }
    /*
    else {
      logger << LOG_INFO << "No reference scale defined. Standard used for default output." << endl;
      logger << LOG_INFO << "No reference scale defined. Standard used for default output." << endl;
      // central value of first TSV scale is used as reference in summary:
      int x_s = 0;
      string temp_name_reference_TSV = osi_name_extended_set_TSV[x_s];
      oset.no_reference_TSV = x_s;
      oset.no_scale_ren_reference_TSV = oset.no_central_scale_ren_TSV[x_s];
      oset.no_scale_fact_reference_TSV = oset.no_central_scale_fact_TSV[x_s];

    }
*/
    logger.newLine(LOG_INFO);
  }
  //  logger << LOG_FATAL << "name_extended_set_TSV[" << i_s << "] = " << name_extended_set_TSV[i_s] << endl;


  logger << LOG_INFO << setw(35) << "switch_TSV" << " = " << switch_TSV << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
