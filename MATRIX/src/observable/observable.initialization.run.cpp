#include "header.hpp"

void observable_set::initialization_complete(){
  Logger logger("observable_set::initialization_complete");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG_VERBOSE << "csi->type_contribution = " << csi->type_contribution << endl;

  if (csi->type_contribution == "born" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "L2I" ||
      csi->type_contribution == "loop" ||
      csi->type_contribution == "L2RT"){
    initialization_tree();
    perform_selection_content_pdf();
  }

  if (csi->type_contribution == "RA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RA"){
    initialization_RA();
    perform_selection_content_pdf();
  }

  if (csi->type_contribution == "CA" ||
      csi->type_contribution == "RCA" ||
      csi->type_contribution == "L2CA"){
    CA_collinear = &xmunich->collinear;
    if (csi->type_correction == "QCD"){determine_collinear_QCD();}
    if (csi->type_correction == "QEW"){determine_collinear_QEW();}
    initialization_CA();
    if (csi->type_correction == "QCD"){initialization_CS_splitting_coefficients_QCD();}
    if (csi->type_correction == "QEW"){initialization_CS_splitting_coefficients_QEW();}
    perform_selection_content_pdf_collinear();
  }

  if (csi->type_contribution == "VA" ||
      csi->type_contribution == "RVA" ||
      csi->type_contribution == "L2VA"){
    VA_ioperator = &xmunich->ioperator;
    if (csi->type_correction == "QCD" || csi->type_correction == "MIX"){determine_ioperator_QCD();}
    if (csi->type_correction == "QEW" || csi->type_correction == "MIX"){determine_ioperator_QEW();}
    initialization_VA();
    if (csi->type_correction == "QCD"){initialization_CS_splitting_coefficients_QCD();}
    if (csi->type_correction == "QEW"){initialization_CS_splitting_coefficients_QEW();}
    perform_selection_content_pdf();
  }
  
  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "VT" ||
      csi->type_contribution == "L2VT"){
    if (csi->type_correction == "QCD" || csi->type_correction == "MIX"){determine_CX_QCD(1);}
    ///DYmixed    if (csi->type_correction == "QEW" || csi->type_correction == "MIX"){determine_CX_QEW(1);}
    // determine_CX_QCD  contains call to  perform_selection_content_pdf_list(); !!!
    initialization_QT();
  }

  if (csi->type_contribution == "CT2" ||
      csi->type_contribution == "VT2"){
    determine_CX_QCD(2);
    initialization_QT();
    // Some version of perform_selection_content_pdf(); ??? !!!
    // determine_CX_QCD  contains call to  perform_selection_content_pdf_list(); !!!
  }

  initialization_mass_parton();

  initialization_TSV();
  initialization_CV();

  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "VT" ||
      csi->type_contribution == "L2VT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "VT2"){
    initialization_CX_ncollinear();
  }

  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "VT" ||
      csi->type_contribution == "L2VT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "VT2"){
    initialization_QT_coefficients();
  }

  initialization_LHAPDF();

  // possibly shift elsewhere (not inside observable_set) !!!
  xmunich->asi->initialization(this, csi, msi, esi, xmunich->isi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::initialization_mass_parton(){
  static Logger logger("observable_set::initialization_mass_parton");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG_VERBOSE << "n_ps = " << n_ps << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->n_particle = " << csi->n_particle << endl;
  logger << LOG_DEBUG_VERBOSE << "csi->type_parton[0].size() = " << csi->type_parton[0].size() << endl;

  mass_parton.resize(n_ps, vector<double> (csi->n_particle + 3, 0.));
  mass2_parton.resize(n_ps, vector<double> (csi->n_particle + 3, 0.));

  for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){

    logger << LOG_DEBUG_VERBOSE << "csi->type_parton[0][i_p = " << i_p << "] = " << csi->type_parton[0][i_p] << endl;
    logger << LOG_DEBUG_VERBOSE << "abs(csi->type_parton[0][i_p = " << i_p << "]) = " << abs(csi->type_parton[0][i_p]) << endl;
    logger << LOG_DEBUG_VERBOSE << "M.size() = " << M.size() << endl;
    logger << LOG_DEBUG_VERBOSE << "M2.size() = " << M2.size() << endl;

    mass_parton[0][i_p] = msi->M[abs(csi->type_parton[0][i_p])];
    mass2_parton[0][i_p] = msi->M2[abs(csi->type_parton[0][i_p])];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::initialization_unit(){
  static Logger logger("observable_set::initialization_unit ()");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "unit_calculation  = " << unit_calculation << endl;
  logger << LOG_DEBUG << "unit_result       = " << unit_result << endl;
  logger << LOG_DEBUG << "unit_distribution = " << unit_distribution << endl;

  // used in the output files in 'result' and 'distribution':
  unit_factor_calculation = determine_unit_factor(unit_calculation);
  unit_factor2_calculation = pow(unit_factor_calculation, 2);
  // only needed for final collection of 'result':
  unit_factor_result = determine_unit_factor(unit_result) / unit_factor_calculation;
  // only needed for final collection of 'distribution':
  unit_factor_distribution = determine_unit_factor(unit_distribution) / unit_factor_calculation;

  logger << LOG_DEBUG << "unit_calculation  = " << unit_calculation << "   ->   " << unit_calculation << " / fb = " << unit_factor_calculation << endl;
  logger << LOG_DEBUG << "unit_result       = " << unit_result << "   ->   " << unit_result << " / " << unit_calculation << " = " << unit_factor_result << endl;
  logger << LOG_DEBUG << "unit_distribution = " << unit_distribution << "   ->   " << unit_distribution << " / " << unit_calculation << " = " << unit_factor_distribution << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


double observable_set::determine_unit_factor(string unit){
  static Logger logger("determine_unit_factor");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "unit = " << unit << endl;

  double unit_factor;
  // determines the relative factor wrt. fb:
  if      (unit == "fb"){unit_factor = 1.;}
  else if (unit == "pb"){unit_factor = 1.e-3;}
  else if (unit == "nb"){unit_factor = 1.e-6;}
  else if (unit == "µb"){unit_factor = 1.e-9;}
  else if (unit == "mb"){unit_factor = 1.e-12;}
  else if (unit == "ab"){unit_factor = 1.e3;}
  else if (unit == "zb"){unit_factor = 1.e6;}
  else if (unit == "yb"){unit_factor = 1.e9;}
  else {
    logger << LOG_FATAL << "No valid unit chosen: " << unit << " !" << endl;
    exit(1);
  }

  logger << LOG_DEBUG << "unit_factor = " << unit_factor << endl;

  logger << LOG_DEBUG_VERBOSE << "finished - unit_factor = " << unit_factor << endl;
  return unit_factor;
}


// call only for runs, not for summary routine ???

void observable_set::initialization_filename(){
  static Logger logger("observable_set::initialization_filename (---)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int isystem = 0;
  string xorder = "";

  string prozessgnuplot = "";
  map<int, string> gnuplotname;
  fill_gnuplotname(gnuplotname);

  if (csi->process_type == 2){
    prozessgnuplot = gnuplotname[csi->type_parton[0][1]] + gnuplotname[csi->type_parton[0][2]] + "&rightarrow ";
    for (int i = 3; i < csi->type_parton[0].size(); i++){
      prozessgnuplot = prozessgnuplot + gnuplotname[csi->type_parton[0][i]];
    }
  }
  else if (csi->process_type == 1){
    prozessgnuplot = gnuplotname[csi->type_parton[0][0]] + "&rightarrow ";
    for (int i = 1; i < csi->type_parton[0].size(); i++){
      prozessgnuplot = prozessgnuplot + gnuplotname[csi->type_parton[0][i]];
    }
  }

  logger << LOG_DEBUG << "csi->process_type = " << csi->process_type << endl;
  logger << LOG_DEBUG << "csi->subprocess = " << csi->subprocess << endl;
  logger << LOG_DEBUG << "csi->subprocess = " << csi->subprocess << endl;

  string dir_integration = "integration";
  if (switch_output_integration){system_execute(logger, "mkdir " + dir_integration, isystem);}
  filename_integration = dir_integration + "/integration_" + csi->subprocess + ".txt";
  logger << LOG_DEBUG << "filename_integration = " << filename_integration << endl;
  if (switch_CV){
    if (switch_output_integration){system_execute(logger, "mkdir " + dir_integration + "/CV", isystem);}
    filename_integration_CV = dir_integration + "/CV" + "/integration_" + csi->subprocess + ".txt";
  }
  if (switch_TSV){
    filename_integration_TSV.resize(n_extended_set_TSV);
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      logger << LOG_DEBUG << "i_s = " << i_s << endl;
      logger << LOG_DEBUG << "n_extended_set_TSV = " << n_extended_set_TSV << endl;
      logger << LOG_DEBUG << "name_extended_set_TSV.size() = " << name_extended_set_TSV.size() << endl;
      logger << LOG_DEBUG << "name_extended_set_TSV[" << i_s << "] = " << name_extended_set_TSV[i_s] << endl;
      if (switch_output_integration){system_execute(logger, "mkdir " + dir_integration + "/" + name_extended_set_TSV[i_s], isystem);}
      filename_integration_TSV[i_s] = dir_integration + "/" + name_extended_set_TSV[i_s] + "/integration_" + csi->subprocess + ".txt";
    }
  }

  string dir_time = "time";
  filename_time = dir_time + "/time_" + csi->subprocess + ".dat";
  if (switch_output_time){system_execute(logger, "mkdir " + dir_time, isystem);}

  string dir_result = "result";
  if (switch_output_result){system_execute(logger, "mkdir " + dir_result, isystem);}
  filename_result = dir_result + "/result_" + csi->subprocess + ".dat";
  if (switch_TSV){
    filename_result_TSV.resize(n_extended_set_TSV);
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      //      string temp_directory = dir_result + "/" + name_extended_set_TSV[i_s];
      if (switch_output_result){system_execute(logger, "mkdir " + dir_result + "/" + name_extended_set_TSV[i_s], isystem);}
      filename_result_TSV[i_s] = dir_result + "/" + name_extended_set_TSV[i_s] + "/result_" + csi->subprocess + ".txt";
    }
  }

  string dir_maxevent = "maxevent";
  if (switch_output_maxevent){system_execute(logger, "mkdir " + dir_maxevent, isystem);}
  filename_maxevent = dir_maxevent + "/maxevent_" + csi->subprocess + ".txt";

  string dir_comparison = "comparison";
  if (switch_output_comparison){system_execute(logger, "mkdir " + dir_comparison, isystem);}
  filename_comparison = dir_comparison + "/comparison_" + csi->subprocess + ".txt";

  string dir_execution = "execution";
  if (switch_output_execution){system_execute(logger, "mkdir " + dir_execution, isystem);}
  filename_execution = dir_execution + "/execution_" + csi->subprocess + ".dat";

  string dir_proceeding = "proceeding";
  if (switch_output_proceeding){system_execute(logger, "mkdir " + dir_proceeding, isystem);}
  filename_proceeding = dir_proceeding + "/proceeding_" + csi->subprocess + ".dat";
  filename_proceeding_2 = dir_proceeding + "/proceeding_2_" + csi->subprocess + ".dat";

  string dir_gnuplot = "gnuplot";
  if (switch_output_gnuplot){system_execute(logger, "mkdir " + dir_gnuplot, isystem);}
  filename_gnuplot = dir_gnuplot + "/gnuplot_" + csi->subprocess + ".dat";
  filename_makegnuplot = dir_gnuplot + "/gnuplot_" + csi->subprocess + ".gp";
  replace(filename_makegnuplot.begin(), filename_makegnuplot.end(), '~', 'x');

  if (csi->subprocess != ""){
    // **************************************************************************
    // *                                                                        *
    // *  creation of gnuplot deviation check file                              *
    // *                                                                        *
    // **************************************************************************

    ifstream in_gnuplot("../../../../visualize.error/mask.visualize.error.dat");
    //  ifstream in_gnuplot("gnuplot/sigmaplot.maske");
    vector<string> readin;
    //    readin = vs0;
    //    int counter = 0;
    char LineBuffer[256];
    while (in_gnuplot.getline(LineBuffer, 256)){readin.push_back(LineBuffer);}
    ofstream out_makegnuplot;
    out_makegnuplot.open(filename_makegnuplot.c_str(), ofstream::out | ofstream::trunc);
    for (int i = 0; i < readin.size(); i++){
      if (i == 10){
	out_makegnuplot << readin[i] + readin[i + 1] << endl;
	i++;
      }
      else if (i == 18){
	out_makegnuplot << readin[i] << " " << char(34) << char(92) << "$" << prozessgnuplot  << char(92) << "$" << char(34) << " " << readin[i + 1] << endl;
	i++;
      }
      else if (i == 120){
	out_makegnuplot << readin[i] << " '../" << filename_gnuplot << "' " << readin[i + 1] << endl;
	i++;
      }
      else{out_makegnuplot << readin[i] << endl;}
    }
    out_makegnuplot.close();

    xorder = "chmod 744 " + filename_makegnuplot;
    logger << LOG_DEBUG << xorder << endl;
    isystem = system(xorder.c_str());
    logger << LOG_DEBUG << xorder << "   execution status: " << isystem << "." << endl;
  }


  string dir_moment = "moment";
  if (switch_output_moment){system_execute(logger, "mkdir " + dir_moment, isystem);}
  filename_moment.resize(n_moments);
  for (int nm = 0; nm < n_moments; nm++){
    char nmstring[255];
    sprintf(nmstring, "%d", nm);
    filename_moment[nm] = dir_moment + "/moment_" + nmstring + "_" + csi->subprocess + ".dat";
  }


  if (switch_TSV){
    filename_moment_TSV.resize(n_extended_set_TSV);
    for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
      string temp_directory = dir_moment + "/" + name_extended_set_TSV[i_s];
      if (switch_output_moment){system_execute(logger, "mkdir " + temp_directory, isystem);}
      filename_moment_TSV[i_s] = temp_directory + "/moment_" + csi->subprocess + ".txt";
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::initialization_switches(){
  static Logger logger("observable_set::initialization_switches (isi)");
  logger << LOG_DEBUG << "called" << endl;

  if (switch_KP != 0 && switch_KP != 1 && switch_KP != 2){
    logger << LOG_FATAL << "Invalid value of switch_KP!" << endl;
    exit(1);
  }

  if (switch_VI != 0 && switch_VI != 1 && switch_VI != 2){
    logger << LOG_FATAL << "Invalid value of switch_VI!" << endl;
    exit(1);
  }

  if (switch_H1gg != 0 && switch_H1gg != 1){
    logger << LOG_FATAL << "Invalid value of switch_H1gg!" << endl;
    exit(1);
  }

  if (switch_H2 != 0 && switch_H2 != 1){
    logger << LOG_FATAL << "Invalid value of switch_H2!" << endl;
    exit(1);
  }

  logger << LOG_DEBUG << "finished" << endl;
}


// also here, or only in qTsubtraction_basic ???

void observable_set::initialization_qTcut(){
  static Logger logger("observable_set::initialization_qTcut");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (!switch_qTcut){
    n_qTcut = 1;
    min_qTcut = 0;
    step_qTcut = 0;
    max_qTcut = 0;
    value_qTcut = vector<double> (1, 0.);
    logger << LOG_INFO << "No qTcut variation applied!" << endl;
    logger << LOG_INFO << setw(40) << "switch_qTcut" << "     =     " << setw(20) << switch_qTcut << endl;
    logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
    logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
    logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
    logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
    logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
    logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;

  }
  else {
    if (switch_qTcut && !n_qTcut){exit(1);}
    //  value_qTcut = isi.value_qTcut;

    if (binning_qTcut == "linear"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, step_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	double temp_max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
	if (max_qTcut != temp_max_qTcut){logger << LOG_FATAL << "Inconsistently over-defined qTcut input!" << endl; exit(1);}
      }
      // min_qTcut, n_qTcut, step_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0.){
	max_qTcut = min_qTcut + (n_qTcut - 1) * step_qTcut;
      }
      // min_qTcut, n_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = (max_qTcut - min_qTcut) / (n_qTcut - 1);
      }
      // min_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && step_qTcut != 0. && max_qTcut != 0.){
	n_qTcut = (max_qTcut - min_qTcut) / step_qTcut + 1;
      }
      // n_qTcut, step_qTcut, max_qTcut
      else if (min_qTcut != 0. && n_qTcut != 0 && step_qTcut != 0. && max_qTcut != 0.){
	min_qTcut = max_qTcut - (n_qTcut - 1) * step_qTcut;
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for linear binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut + i_q * step_qTcut);
      }
    }

    else if (binning_qTcut == "logarithmic"){
      // possible ways to set input parameters:
      // min_qTcut, n_qTcut, max_qTcut
      if (min_qTcut != 0. && n_qTcut != 0 && max_qTcut != 0.){
	step_qTcut = 0.; // no constant step width in logarithmic binning!
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
      for (int i_q = 0; i_q < n_qTcut; i_q++){
	value_qTcut.push_back(min_qTcut * exp10(log10(max_qTcut / min_qTcut) * double(i_q) / (n_qTcut - 1)));
      }
      value_qTcut[n_qTcut - 1] = max_qTcut;
    }

    else if (binning_qTcut == "irregular"){
      // possible ways to set input parameters:

      // selection_qTcut
      if (selection_qTcut != ""){
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(selection_qTcut[i_b]);}
	}
	n_qTcut = vs_selection.size();
	value_qTcut.resize(n_qTcut);
	for (int i_b = 0; i_b < value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	}
	min_qTcut = value_qTcut[0];
	max_qTcut = value_qTcut[n_qTcut - 1];
      }
      else {
	logger << LOG_FATAL << "Inconsistent qTcut input for logarithmic binning!" << endl;
	exit(1);
      }
    }
  }

  logger << LOG_INFO << setw(40) << "n_qTcut" << "     =     " << setw(20) << n_qTcut << endl;
  logger << LOG_INFO << setw(40) << "min_qTcut" << "     =     " << setw(20) << min_qTcut << endl;
  logger << LOG_INFO << setw(40) << "max_qTcut" << "     =     " << setw(20) << max_qTcut << endl;
  logger << LOG_INFO << setw(40) << "step_qTcut" << "     =     " << setw(20) << step_qTcut << endl;
  logger << LOG_INFO << setw(40) << "binning_qTcut" << "     =     " << setw(20) << binning_qTcut << endl;
  logger << LOG_INFO << setw(40) << "selection_qTcut" << "     =     " << setw(20) << selection_qTcut << endl;
  /*
  for (int i_b = 0; i_b < value_qTcut.size(); i_b++){
    logger << LOG_INFO << "value_qTcut[" << setw(3) << i_b << "] = " << value_qTcut[i_b] << endl;
  }
  */

  logger << LOG_INFO << "csi->type_contribution = " << csi->type_contribution << endl;

  if (csi->type_contribution == "" ||
      csi->type_contribution == "all" ||
      csi->type_contribution == "CT" ||
      csi->type_contribution == "CJ" ||
      csi->type_contribution == "RT" ||
      csi->type_contribution == "CT2" ||
      csi->type_contribution == "CJ2" ||
      csi->type_contribution == "RVA" ||
      csi->type_contribution == "RCA" ||
      csi->type_contribution == "RRA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2CT"){
    active_qTcut = 1;
    output_n_qTcut = n_qTcut;
  }
  else {
    active_qTcut = 0;
    output_n_qTcut = 1;
  }

  logger << LOG_INFO << "output_n_qTcut = " << output_n_qTcut << endl;
  logger << LOG_INFO << "active_qTcut = " << active_qTcut << endl;
  logger << LOG_INFO << "selection_qTcut_distribution = " << selection_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_no_qTcut_distribution = " << selection_no_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_qTcut_result = " << selection_qTcut_result << endl;
  logger << LOG_INFO << "selection_no_qTcut_result = " << selection_no_qTcut_result << endl;
  logger << LOG_INFO << "selection_qTcut_integration = " << selection_qTcut_integration << endl;
  logger << LOG_INFO << "selection_no_qTcut_integration = " << selection_no_qTcut_integration << endl;

  for (int i_m = 0; i_m < 3; i_m++){
    string temp_name;
    string temp_selection_qTcut;
    string temp_selection_no_qTcut;
    vector<int> temp_no_qTcut;
    vector<double> temp_value_qTcut;
    if (i_m == 0){temp_selection_qTcut = selection_qTcut_distribution; temp_selection_no_qTcut = selection_no_qTcut_distribution; temp_name = "distribution";}
    else if (i_m == 1){temp_selection_qTcut = selection_qTcut_result; temp_selection_no_qTcut = selection_no_qTcut_result; temp_name = "result";}
    else if (i_m == 2){temp_selection_qTcut = selection_qTcut_integration; temp_selection_no_qTcut = selection_no_qTcut_integration; temp_name = "integration";}
    else {logger << LOG_ERROR << "Wrong entry in selection_qTcut!" << endl;}

    logger << LOG_INFO << "i_m = " << i_m << "   temp_selection_qTcut = " << temp_selection_qTcut << "   temp_selection_no_qTcut = " << temp_selection_no_qTcut << endl;

    if (active_qTcut){
      if (temp_selection_qTcut == "" && temp_selection_no_qTcut == ""){
	temp_value_qTcut.resize(1, 0.);
	temp_no_qTcut.resize(1, 0);
	logger << LOG_INFO << temp_name << " output generated for lowest qTcut value." << endl;
      }
      else if (temp_selection_qTcut == "all" || temp_selection_no_qTcut == "all"){
	temp_value_qTcut = value_qTcut;
	temp_no_qTcut.resize(n_qTcut);
	for (int i_q = 0; i_q < n_qTcut; i_q++){temp_no_qTcut[i_q] = i_q;}
	logger << LOG_INFO << temp_name << " output generated for all qTcut values." << endl;
      }
      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut == ""){
	logger << LOG_INFO << temp_name << " output generated for selected qTcut values." << endl;

	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_value_qTcut[i_b] = atof(vs_selection[i_b].c_str());
	  int flag = n_qTcut;
	  for (int i_q = 0; i_q < n_qTcut; i_q++){
	    if (abs(temp_value_qTcut[i_b] - value_qTcut[i_q]) < 1.e-12 * value_qTcut[i_q]){flag = i_q; break;}
	  }
	  if (flag == n_qTcut){temp_value_qTcut.erase(temp_value_qTcut.begin() + i_b); i_b--;}
	  else {temp_no_qTcut[i_b] = flag;}
	}
      }

      else if (temp_selection_qTcut == "" && temp_selection_no_qTcut != ""){
	logger << LOG_INFO << temp_name << " output generated for selected no_qTcut values." << endl;
	vector<string> vs_selection(1);
	for (int i_b = 0; i_b < temp_selection_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG_VERBOSE << "i_b = " << i_b << "   vs_selection.size() = " << vs_selection.size() << endl;
	  if (temp_selection_no_qTcut[i_b] == ':'){vs_selection.push_back("");}
	  else if (temp_selection_no_qTcut[i_b] != ':'){vs_selection[vs_selection.size() - 1].push_back(temp_selection_no_qTcut[i_b]);}
	}
	//    int n_selection = vs_selection.size();
	temp_no_qTcut.resize(vs_selection.size());
	temp_value_qTcut.resize(vs_selection.size());

	for (int i_b = 0; i_b < temp_no_qTcut.size(); i_b++){
	  logger << LOG_DEBUG << "vs_selection[" << i_b << "] = " << setprecision(8) << setw(15) << vs_selection[i_b] << endl;
	  temp_no_qTcut[i_b] = atoi(vs_selection[i_b].c_str());
	  temp_value_qTcut[i_b] = value_qTcut[temp_no_qTcut[i_b]];
	}
      }

      else if (temp_selection_qTcut != "" && temp_selection_no_qTcut != ""){
	logger << LOG_FATAL << "Inconsistent qTcut input for " << temp_name << "!" << endl;
	exit(1);
      }

      logger << LOG_DEBUG << temp_name << "   temp_value_qTcut.size() = " << temp_value_qTcut.size() << endl;
      /*
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
      */
    }

    else {
      temp_value_qTcut.resize(1, 0.);
      temp_no_qTcut.resize(1, 0);
      /*
      for (int i_b = 0; i_b < temp_value_qTcut.size(); i_b++){
	logger << LOG_INFO << "value_qTcut_" << temp_name << "[" << i_b << "] = " << setw(15) << setprecision(8) << temp_value_qTcut[i_b] << " -> " << setw(3) << temp_no_qTcut[i_b] << endl;
      }
      */
    }
    if (i_m == 0){no_qTcut_distribution = temp_no_qTcut; value_qTcut_distribution = temp_value_qTcut;}
    else if (i_m == 1){no_qTcut_result = temp_no_qTcut; value_qTcut_result = temp_value_qTcut;}
    else if (i_m == 2){no_qTcut_integration = temp_no_qTcut; value_qTcut_integration = temp_value_qTcut;}

    //    logger << LOG_INFO << "temp_name = " << temp_name << endl;
    //    logger << LOG_INFO << "temp_selection_qTcut = " << temp_selection_qTcut << endl;
  }

  if (csi->type_contribution == "RVA" ||
      csi->type_contribution == "L2RT" ||
      csi->type_contribution == "L2RJ"){
    counter_killed_qTcut.resize(n_qTcut, 0);
    counter_acc_qTcut.resize(n_qTcut, 0);
  }

  logger << LOG_INFO << "output_n_qTcut = " << output_n_qTcut << endl;
  logger << LOG_INFO << "active_qTcut = " << active_qTcut << endl;
  logger << LOG_INFO << left << setw(30) << "selection_qTcut_result" << " = " << setw(50) << selection_qTcut_result << "   " << setw(2) << value_qTcut_result.size() << endl;
  logger << LOG_INFO << left << setw(30) << "selection_qTcut_distribution" << " = " << setw(50) << selection_qTcut_distribution << "   " << setw(2) << value_qTcut_distribution.size() << endl;
  logger << LOG_INFO << left << setw(30) << "selection_qTcut_integration" << " = " << setw(50) << selection_qTcut_integration << "   " << setw(2) << value_qTcut_integration.size() << endl;


  //  logger << LOG_INFO << "selection_qTcut_distribution = " << selection_qTcut_distribution << endl;
  logger << LOG_INFO << "selection_no_qTcut_distribution = " << selection_no_qTcut_distribution << endl;
  //  logger << LOG_INFO << "selection_qTcut_result = " << selection_qTcut_result << endl;
  logger << LOG_INFO << "selection_no_qTcut_result = " << selection_no_qTcut_result << endl;
  //  logger << LOG_INFO << "selection_qTcut_integration = " << selection_qTcut_integration << endl;
  logger << LOG_INFO << "selection_no_qTcut_integration = " << selection_no_qTcut_integration << endl;




  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void observable_set::initialization_integration_parameters(){
  static Logger logger("observable_set::initialization_integration_parameters");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  check_vanishing_ME2_end = 0;
  flag_vanishing_ME2 = 0;
  n_event_vanishing_ME2 = 100; // temporary !!!

  temp_n_step = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


// Should be removed - there is no need for copying masses that are contained in msi (and in psi for modified mappings) !!!

void observable_set::initialization_masses(vector<double> _M, vector<double> _M2){
  static Logger logger("observable_set::initialization_masses");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  M = _M;
  M2 = _M2;

  for (int i = 0; i < 26; i++){logger << LOG_DEBUG << "M[" << setw(2) << i << "]  = " << right << setprecision(15) << setw(23) << M[i] << endl;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void observable_set::initialization_integration(){
  static Logger logger("observable_set::initialization_integration");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_DEBUG << "filename_proceeding = " << filename_proceeding << endl;

  relation_pc_ps.resize(n_pc);
  for (int i_c = 0; i_c < n_pc; i_c++){
    if (n_ps == 1){relation_pc_ps[i_c] = 0;}
    else {relation_pc_ps[i_c] = i_c;}
  }

  // shifted - check if this modification causes trouble !!!
  /////  esi->initialization_integration();

  initialization_distribution();

  logger << LOG_DEBUG << "csi->type_parton.size() = " << csi->type_parton.size() << endl;
  for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){logger << LOG_DEBUG << "csi->type_parton[0][" << i_p << "] = " << csi->type_parton[0][i_p] << endl;}

  // determination if massless or massive subtraction is needed:
  massive_QCD = 0;
  for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){if (mass_parton[0][i_p] > 0. && abs(csi->type_parton[0][i_p]) < 7){massive_QCD = 1; break;}}
  logger << LOG_DEBUG << "massive_QCD = " << massive_QCD << endl;

  massive_QEW = 0;
  for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){
    // decision via particle charge ???
    if (mass_parton[0][i_p] > 0. && (abs(csi->type_parton[0][i_p]) < 7 || abs(csi->type_parton[0][i_p]) == 11 || abs(csi->type_parton[0][i_p]) == 13 || abs(csi->type_parton[0][i_p]) == 15 || abs(csi->type_parton[0][i_p]) == 24)){massive_QEW = 1; break;}
  }
  logger << LOG_DEBUG << "massive_QEW = " << massive_QEW << endl;

  logger << LOG_DEBUG << "n_ps = " << n_ps << endl;
  logger << LOG_DEBUG << "csi->n_particle = " << csi->n_particle << endl;

  moment_symm.resize(n_moments, 0);
  moment.resize(n_moments, vector<double> (n_ps, 0.));
  directed_moment.resize(n_moments, vector<vector<double> > (3, vector<double> (n_ps, 0.)));

  max_integrand = 0.;

  integrand = 0.;
  integrand_CV.resize(n_scales_CV, 0.);
  integrand_qTcut_CV.resize(n_qTcut, integrand_CV);

  integrand_D.resize(3, vector<double> (n_ps, 0.));
  integrand_D_CV.resize(n_scales_CV, integrand_D);
  integrand_D_qTcut_CV.resize(n_qTcut, integrand_D_CV);


  change_cut.resize(n_qTcut + 1, 0);
  //  change_cut.resize(n_qTcut, 0);

  this_psp_weight = 0.;
  this_psp_weight2 = 0.;
  this_psp_weight_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));
  this_psp_weight2_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));

  step_sum_weight = 0.;
  step_sum_weight2 = 0.;
  step_sum_weight_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));
  step_sum_weight2_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));

  full_sum_weight = 0.;
  full_sum_weight2 = 0.;
  full_sum_weight_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));
  full_sum_weight2_CV.resize(n_qTcut, vector<double> (n_scales_CV, 0.));

  this_psp_moment.resize(n_moments, 0.);
  this_psp_moment_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));
  this_psp_moment2.resize(n_moments);
  this_psp_moment2_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));

  step_sum_moment.resize(n_moments, 0.);
  step_sum_moment_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));
  step_sum_moment2.resize(n_moments);
  step_sum_moment2_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));

  full_sum_moment.resize(n_moments, 0.);
  full_sum_moment2.resize(n_moments, 0.);
  full_sum_moment_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));
  full_sum_moment2_CV.resize(n_moments, vector<vector<double> > (n_qTcut, vector<double> (n_scales_CV, 0.)));


  if (switch_distribution){
    bin.resize(dat.size() + dddat.size(), vector<int> (n_ps));
    bin_max.resize(dat.size() + dddat.size(), vector<int> (n_ps));

    bin_weight.resize(dat.size() + dddat.size());
    bin_weight2.resize(dat.size() + dddat.size());
    bin_counts.resize(dat.size() + dddat.size());
    bin_weight_CV.resize(n_scales_CV);
    bin_weight2_CV.resize(n_scales_CV);
    for (int i_d = 0; i_d < dat.size(); i_d++){
      bin_weight[i_d].resize(dat[i_d].n_bins);
      bin_weight2[i_d].resize(dat[i_d].n_bins);
      bin_counts[i_d].resize(dat[i_d].n_bins);
    }
    for (int i_d = 0; i_d < dddat.size(); i_d++){
      bin_weight[dat.size() + i_d].resize(dddat[i_d].n_bins);
      bin_weight2[dat.size() + i_d].resize(dddat[i_d].n_bins);
      bin_counts[dat.size() + i_d].resize(dddat[i_d].n_bins);
    }
    for (int i_s = 0; i_s < n_scales_CV; i_s++){
      bin_weight_CV[i_s] = bin_weight;
      bin_weight2_CV[i_s] = bin_weight2;
    }
  }


  h = 0;
  min = 0;
  sec = 0;
  time_counter = 0;

  sec_import = 0;

  if (csi->class_contribution_CS_real){initialization_specific_RA();}
  // explicitly related to KP terms in CS subtraction, not to phasespace only.
  if (csi->class_contribution_CS_collinear){initialization_specific_CA();}
  if (csi->class_contribution_CS_virtual){initialization_specific_VA();}
  
  if (csi->type_contribution == "VT" ||
      csi->type_contribution == "VT2" ||
      csi->type_contribution == "VJ" ||
      csi->type_contribution == "VJ2" ||
      csi->type_contribution == "L2VT" ||
      csi->type_contribution == "L2VJ"){
    initialization_specific_VT();
    if (switch_old_qT_version){initialization_specific_QT();}
  }
  if (csi->type_contribution == "CT2" ||
      csi->type_contribution == "CJ2"){
    initialization_specific_VT();
    initialization_specific_QT();
  }
  if (csi->type_contribution == "CT" ||
      csi->type_contribution == "CJ" ||
      csi->type_contribution == "L2CT" ||
      csi->type_contribution == "L2CJ"){
    if (switch_old_qT_version){initialization_specific_QT();}
  }

  //  ofstream out_time;
  ofstream out_maxevent;
  ofstream out_comparison;
  ofstream out_integration;
  ofstream out_result;
  ofstream out_gnuplot;
  ofstream out_proceeding;
  ofstream out_distribution;
  ofstream out_execution;


  int newstart = 0;
  ifstream in_proceeding(filename_proceeding.c_str());
  if (in_proceeding){
    perform_proceeding_in();
    logger << LOG_INFO << "int_end = " << int_end << endl;

    if (int_end == 2){
      int_end = 0;
      newstart = 1;
      system_execute(logger, "rm " + filename_proceeding);
    }
    else {
      out_gnuplot.open(filename_gnuplot.c_str(), ofstream::out | ofstream::app);
      out_gnuplot.close();
      perform_proceeding_check();
      out_integration.open(filename_integration.c_str(), ofstream::out | ofstream::app);

      if (int_end == 1){
	logger << LOG_INFO << "integration already finished" << endl;

	// temporary !!!

	logger << LOG_INFO << "psi->end_optimization = " << psi->end_optimization << endl;
	if (psi->end_optimization == 2){
	  psi->end_optimization = 1;
	  if (psi->MC_phasespace.end_optimization == 2){psi->MC_phasespace.end_optimization = 1;}
	  if (psi->MC_tau.end_optimization == 2){psi->MC_tau.end_optimization = 1;}
	  if (csi->class_contribution_CS_real){for (int i_a = 1; i_a < psi->MC_x_dipole.size(); i_a++){if (psi->MC_x_dipole[i_a].end_optimization == 2){psi->MC_x_dipole[i_a].end_optimization = 1;}}}
	  if (psi->IS_tau.end_optimization == 2){psi->IS_tau.end_optimization = 1;}
	  if (psi->IS_x1x2.end_optimization == 2){psi->IS_x1x2.end_optimization = 1;}
	  if (csi->class_contribution_collinear){for (int i_z = 1; i_z < 3; i_z++){if (psi->IS_z1z2[i_z].end_optimization == 2){psi->IS_z1z2[i_z].end_optimization = 1;}}}
	  logger << LOG_INFO << "psi->random_manager.end_optimization = " << psi->random_manager.end_optimization << endl;
	  if (psi->random_manager.end_optimization == 2){psi->random_manager.end_optimization = 1;}
	  for (int i_r = 0; i_r < psi->random_psp.size(); i_r++){
	    logger << LOG_INFO << "psi->random_psp[" << i_r << "]->IS_PS.end_optimization = " << psi->random_psp[i_r]->IS_PS.end_optimization << endl;
	    if (psi->random_psp[i_r]->IS_PS.end_optimization == 2){psi->random_psp[i_r]->IS_PS.end_optimization = 1;}
	  }

	  //  qtres !!!
	  //  random_manager

	  psi->output_optimization_complete();
	  //	psi->random_manager.writeout_weights();
	  logger << LOG_INFO << "weights output has been written anew." << endl;
	}
	
	if (switch_output_distribution){
	  output_distribution_complete();
	  logger << LOG_INFO << "distribution output has been written anew." << endl;
	}
	if (switch_output_result){
	  calculate_intermediate_result();
	  output_step_result();
	  logger << LOG_INFO << "result output has been written anew." << endl;
	}

	// needs to be updated !!! MC_....end_optimization need to be adapted !!!
	// grid output should also be repeated !!!
	/*
	  if (psi_switch_n_events_opt == 2){
	    logger << LOG_INFO << "psi_MC_opt_end = " << psi_MC_opt_end << endl;
	    if (psi_MC_opt_end > 0){psi.weight_output_MCweight_optimization();}
	    psi_random_manager.writeout_weights();
	    logger << LOG_INFO << "psi_tau_opt_end = " << psi_tau_opt_end << endl;
	    if (psi_tau_opt_end > 0){output_weight_vegas(psi_filename_tauweight, psi_tau_alpha);}
	    logger << LOG_INFO << "psi_x1x2_opt_end = " << psi_x1x2_opt_end << endl;
	    if (psi_x1x2_opt_end > 0){output_weight_vegas(psi_filename_x1x2weight, psi_x1x2_alpha);}
	    logger << LOG_INFO << "psi_z1z2_opt_end = " << psi_z1z2_opt_end << endl;
	    if (psi_z1z2_opt_end > 0){for (int i_z = 1; i_z < 3; i_z++){output_weight_vegas(psi_filename_z1z2weight[i_z], psi_z1z2_alpha[i_z]);}}
	    logger << LOG_INFO << "grid output has been written anew." << endl;
	  }
	*/
	exit(0);
      }
      else {
	out_integration << "******************************************************* resumption ******************************************************" << endl;
	out_execution.open(filename_execution.c_str(), ofstream::out | ofstream::trunc);
	out_execution << "restarted" << endl;
	out_execution.close();
      }
    }
    out_integration.close();
  }
  else {newstart = 1;}
  
  if (newstart){
    out_integration.open(filename_integration.c_str(), ofstream::out | ofstream::trunc);
    header_integration(out_integration);
    out_integration.close();
    if (switch_TSV){
      for (int i_s = 0; i_s < n_extended_set_TSV; i_s++){
	out_integration.open((filename_integration_TSV[i_s]).c_str(), ofstream::out | ofstream::trunc);
	out_result.open((filename_result_TSV[i_s]).c_str(), ofstream::out | ofstream::trunc);
	header_integration(out_integration);
	out_integration.close();
	out_result.close();
      }
    }
    out_execution.open(filename_execution.c_str(), ofstream::out | ofstream::trunc);
    out_execution.close();
    out_gnuplot.open(filename_gnuplot.c_str(), ofstream::out | ofstream::trunc);
    out_gnuplot.close();
    if (switch_CV){
      out_integration.open(filename_integration_CV.c_str(), ofstream::out | ofstream::trunc);
      header_integration(out_integration);
      out_integration.close();
    }

    out_result.open(filename_result.c_str(), ofstream::out | ofstream::trunc);
    out_result.close();
    out_maxevent.open(filename_maxevent.c_str(), ofstream::out | ofstream::trunc);
    out_maxevent.close();

    output_step_time();
  }
  in_proceeding.close();

  out_comparison.open(filename_comparison.c_str(), ofstream::out | ofstream::trunc);
  out_comparison.close();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




