#{{{ header
global FFS_list
global scale_dict
global proc_dict
global cuts_dict
FFS_list   = []
scale_dict = {}
proc_dict  = multidim_dict(2)
cuts_dict  = multidim_dict(2)
#}}}
#{{{ pph21 input
proc_dict["pph21"]["process_class"] = "pp-h+X"
proc_dict["pph21"]["scale_ren"] = "125."
proc_dict["pph21"]["scale_fact"] = "125."
proc_dict["pph21"]["dynamic_scale"] = "0"
proc_dict["pph21"]["factor_central_scale"] = "1"
scale_dict["pph21"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["pph21"]["block"] = ['jet', 'h']
cuts_dict["pph21"]["user"] = []
#}}}
#{{{ ppz01 input
proc_dict["ppz01"]["process_class"] = "pp-z+X"
proc_dict["ppz01"]["scale_ren"] = "91.1876"
proc_dict["ppz01"]["scale_fact"] = "91.1876"
proc_dict["ppz01"]["dynamic_scale"] = "0"
proc_dict["ppz01"]["factor_central_scale"] = "1"
scale_dict["ppz01"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppz01"]["block"] = ['jet']
cuts_dict["ppz01"]["user"] = []
#}}}
#{{{ ppw01 input
proc_dict["ppw01"]["process_class"] = "pp-wm+X"
proc_dict["ppw01"]["scale_ren"] = "80.385"
proc_dict["ppw01"]["scale_fact"] = "80.385"
proc_dict["ppw01"]["dynamic_scale"] = "0"
proc_dict["ppw01"]["factor_central_scale"] = "1"
scale_dict["ppw01"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppw01"]["block"] = ['jet']
cuts_dict["ppw01"]["user"] = []
#}}}
#{{{ ppwx01 input
proc_dict["ppwx01"]["process_class"] = "pp-wp+X"
proc_dict["ppwx01"]["scale_ren"] = "80.385"
proc_dict["ppwx01"]["scale_fact"] = "80.385"
proc_dict["ppwx01"]["dynamic_scale"] = "0"
proc_dict["ppwx01"]["factor_central_scale"] = "1"
scale_dict["ppwx01"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppwx01"]["block"] = ['jet']
cuts_dict["ppwx01"]["user"] = []
#}}}
#{{{ ppeex02 input
proc_dict["ppeex02"]["process_class"] = "pp-emep+X"
proc_dict["ppeex02"]["scale_ren"] = "91.1876"
proc_dict["ppeex02"]["scale_fact"] = "91.1876"
proc_dict["ppeex02"]["dynamic_scale"] = "0"
proc_dict["ppeex02"]["factor_central_scale"] = "1"
scale_dict["ppeex02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppeex02"]["block"] = ['jet', ['lep', '25.', '2.47', '1.e99', '2', '99'], 'lm', 'lp']
cuts_dict["ppeex02"]["user"] = [[['user_switch M_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass'], ['user_cut min_M_leplep', '66.', 'requirement on lepton-lepton invariant mass (lower cut)'], ['user_cut max_M_leplep', '116.', 'requirement on lepton-lepton invariant mass (upper cut)']], [['user_switch R_leplep', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on (1) and off (0) cuts on transverse momentum of leptons'], ['user_cut min_pT_lep_1st', '25.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut min_pT_lep_2nd', '15.', 'requirement on second-hardest lepton transverse momentum (lower cut)']]]
#}}}
#{{{ ppnenex02 input
proc_dict["ppnenex02"]["process_class"] = "pp-veve~+X"
proc_dict["ppnenex02"]["scale_ren"] = "91.1876"
proc_dict["ppnenex02"]["scale_fact"] = "91.1876"
proc_dict["ppnenex02"]["dynamic_scale"] = "0"
proc_dict["ppnenex02"]["factor_central_scale"] = "1"
scale_dict["ppnenex02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppnenex02"]["block"] = ['jet', 'missing']
cuts_dict["ppnenex02"]["user"] = []
#}}}
#{{{ ppenex02 input
proc_dict["ppenex02"]["process_class"] = "pp-emve~+X"
proc_dict["ppenex02"]["scale_ren"] = "80.385"
proc_dict["ppenex02"]["scale_fact"] = "80.385"
proc_dict["ppenex02"]["dynamic_scale"] = "0"
proc_dict["ppenex02"]["factor_central_scale"] = "1"
scale_dict["ppenex02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppenex02"]["block"] = ['jet', ['lep', '25.', '2.47', '1.e99', '1', '99'], ['missing', '20.']]
cuts_dict["ppenex02"]["user"] = []
#}}}
#{{{ ppexne02 input
proc_dict["ppexne02"]["process_class"] = "pp-epve+X"
proc_dict["ppexne02"]["scale_ren"] = "80.385"
proc_dict["ppexne02"]["scale_fact"] = "80.385"
proc_dict["ppexne02"]["dynamic_scale"] = "0"
proc_dict["ppexne02"]["factor_central_scale"] = "1"
scale_dict["ppexne02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppexne02"]["block"] = ['jet', ['lep', '25.', '2.47', '1.e99', '1', '99'], ['missing', '20.']]
cuts_dict["ppexne02"]["user"] = []
#}}}
#{{{ ppaa02 input
proc_dict["ppaa02"]["process_class"] = "pp-aa+X"
proc_dict["ppaa02"]["scale_ren"] = "91.1876"
proc_dict["ppaa02"]["scale_fact"] = "91.1876"
proc_dict["ppaa02"]["dynamic_scale"] = "1"
proc_dict["ppaa02"]["factor_central_scale"] = "1"
scale_dict["ppaa02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppaa02"]["block"] = ['jet', ['photon', '25.', '2.5', '1.e99', '2', '99']]
cuts_dict["ppaa02"]["user"] = [[['user_switch M_gamgam', '1', 'switch to turn on (1) and off (0) cuts on photon-photon invariant mass'], ['user_cut min_M_gamgam', '20.', 'requirement on photon-photon invariant mass (lower cut)'], ['user_cut max_M_gamgam', '250.', 'requirement on photon-photon invariant mass (upper cut)']], [['user_switch pT_gam_1st', '1', 'switch to turn on (1) and off (0) cuts on pT of hardest photon'], ['user_cut min_pT_gam_1st', '40.', 'requirement on pT of hardest photon (lower cut)']], [['user_switch gap_eta_gam', '0', 'switch to turn on (1) and off (0) detector gap for eta of photons'], ['user_cut gap_min_eta_gam', '0.', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_gam', '0.', 'upper bound of detector gap in eta']], [['user_switch R_gamgam', '0', 'switch to turn on (1) and off (0) cuts on photon-photon separation'], ['user_cut min_R_gamgam', '0.3', 'requirement on photon-photon separation in y-phi-plane (lower cut)']]]
#}}}
#{{{ ppaaa03 input
proc_dict["ppaaa03"]["process_class"] = "pp-aaa+X"
proc_dict["ppaaa03"]["scale_ren"] = "91.1876"
proc_dict["ppaaa03"]["scale_fact"] = "91.1876"
proc_dict["ppaaa03"]["dynamic_scale"] = "1"
proc_dict["ppaaa03"]["factor_central_scale"] = "1"
scale_dict["ppaaa03"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppaaa03"]["block"] = ['jet', ['photon', '15.', '2.37', '1.e99', '3', '99']]
cuts_dict["ppaaa03"]["user"] = [[['user_switch M_gamgamgam', '1', 'switch to turn on (1) and off (0) cuts on triphoton invariant mass'], ['user_cut min_M_gamgamgam', '50.', 'requirement on triphoton invariant mass (lower cut)'], ['user_cut max_M_gamgamgam', '1.e99', 'requirement on triphoton  invariant mass (upper cut)']], [['user_switch M_gamgam', '0', 'switch to turn on (1) and off (0) cuts on all photon-photon invariant masses'], ['user_cut min_M_gamgam', '0.', 'requirement on all photon-photon invariant masses (lower cut)'], ['user_cut max_M_gamgam', '1.e99', 'requirement on all photon-photon invariant masses (upper cut)']], [['user_switch photon_cuts', '1', 'switch to turn on (1) and off (0) cuts on pT of photons'], ['user_cut min_pT_gam_1st', '27.', 'requirement on pT of hardest photon (lower cut)'], ['user_cut min_pT_gam_2nd', '22.', 'requirement on pT of second-hardest photon (lower cut)'], ['user_cut min_pT_gam_3rd', '15.', 'requirement on pT of third-hardest photon (lower cut)']], [['user_switch gap_eta_gam', '1', 'switch to turn on (1) and off (0) detector gap for eta of photons'], ['user_cut gap_min_eta_gam', '1.37', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_gam', '1.56', 'upper bound of detector gap in eta']], [['user_switch R_gamgam', '1', 'switch to turn on (1) and off (0) cuts on all photon-photon separations'], ['user_cut min_R_gamgam', '0.45', 'requirement on all photon-photon separations in y-phi-plane (lower cut)']]]
#}}}
#{{{ ppeexa03 input
proc_dict["ppeexa03"]["process_class"] = "pp-emepa+X"
proc_dict["ppeexa03"]["scale_ren"] = "91.1876"
proc_dict["ppeexa03"]["scale_fact"] = "91.1876"
proc_dict["ppeexa03"]["dynamic_scale"] = "6"
proc_dict["ppeexa03"]["factor_central_scale"] = "1"
scale_dict["ppeexa03"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: transverse mass of photon (note: mT_photon=pT_photon)', '4: transverse mass of Z boson (lepton system, mT_lep1+lep2)', '5: geometric avarage of mT of photon and mT of Z boson', '6: quadratic sum of Z mass and mT of the photon (mu^2=m_Z^2+mT_photon^2)', '7: quadratic sum of dilepton mass and mT of photon (mu^2=m_lep1+lep2^2+mT_photon^2)']
cuts_dict["ppeexa03"]["block"] = [['jet', '30.', '4.4', '1.e99', '0', '99'], ['lep', '25.', '2.47', '1.e99', '2', '99'], 'lm', 'lp', ['photon', '15.', '2.37', '1.e99', '1', '99']]
cuts_dict["ppeexa03"]["user"] = [[['user_switch M_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass'], ['user_cut min_M_leplep', '40.', 'requirement on lepton-lepton invariant mass (lower cut)'], ['user_cut max_M_leplep', '1.e99', 'requirement on lepton-lepton invariant mass (upper cut)']],[['user_switch M_lepgam', '0', 'switch to turn on (1) and off (0) cuts on lepton-photon invariant mass'], ['user_cut min_M_lepgam', '40.', 'requirement on lepton-photon invariant mass (lower cut)']],[['user_switch M_leplep+M_leplepgam', '0', 'switch to turn on (1) and off (0) cuts on sum of lepton-lepton and lepton-lepton-photon invariant mass'], ['user_cut min_M_leplep+M_leplepgam', '182.', 'requirement on sum of lepton-lepton and lepton-lepton-photon invariant mass (lower cut)'], ['user_cut max_M_leplep+M_leplepgam', '1.e99', 'requirement on sum of lepton-lepton and lepton-lepton-photon invariant mass (upper cut)']],[['user_switch R_leplep', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.5', 'requirement on lepton-photon separation in y-phi-plane (lower cut)']], [['user_switch R_lepgam', '1', 'switch to turn on (1) and off (0) cuts on lepton-photon separation'], ['user_cut min_R_lepgam', '0.7', 'requirement on lepton-photon separation in y-phi-plane (lower cut)']], [['user_switch R_lepjet', '1', 'switch to turn on (1) and off (0) cuts on lepton-jet separation'], ['user_cut min_R_lepjet', '0.3', 'requirement on lepton-jet separation in y-phi-plane (lower cut)']], [['user_switch R_gamjet', '1', 'switch to turn on (1) and off (0) cuts on photon-jet separation'], ['user_cut min_R_gamjet', '0.3', 'requirement on photon-jet separation in y-phi-plane (lower cut)']], [['user_switch pT_lep_1st', '0', 'switch to turn on (1) and off (0) cuts on pT of hardest lepton'], ['user_cut min_pT_lep_1st', '25', 'requirement on pT of hardest lepton (lower cut)']], [['user_switch rel_ET_iso_with_lep', '0', 'switch to turn on (1) and off (0) extra photon isolation with ET ratio (used by CMS)'], ['user_cut max_rel_ET_iso_with_lep', '0.07', 'requirement on ET ratio (upper cut)']], [['user_switch rel_ET_iso_no_lep', '0', 'switch to turn on (1) and off (0) extra photon isolation with ET ratio (used by CMS)'], ['user_cut max_rel_ET_iso_no_lep', '0.07', 'requirement on ET ratio (upper cut)']], [['user_switch pT_leplepgamma', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton-photon transverse momentum'], ['user_cut min_pT_leplepgamma', '0.', 'requirement on lepton-lepton-photon transverse momentum (lower cut)'], ['user_cut max_pT_leplepgamma', '1.e99', 'requirement on lepton-lepton-photon transverse momentum (upper cut)']]]

#}}}
#{{{ ppnenexa03 input
proc_dict["ppnenexa03"]["process_class"] = "pp-veve~a+X"
proc_dict["ppnenexa03"]["scale_ren"] = "91.1876"
proc_dict["ppnenexa03"]["scale_fact"] = "91.1876"
proc_dict["ppnenexa03"]["dynamic_scale"] = "6"
proc_dict["ppnenexa03"]["factor_central_scale"] = "1"
scale_dict["ppnenexa03"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: transverse mass of photon (note: mT_photon=pT_photon)', '4: transverse mass of Z boson (lepton system, mT_nu1+nu2)', '5: geometric avarage of mT of photon and mT of Z boson', '6: quadratic sum of Z mass and mT of the photon (mu^2=m_Z^2+mT_photon^2)']
cuts_dict["ppnenexa03"]["block"] = [['jet', '30.', '4.4', '1.e99', '0', '99'], ['photon', '100.', '2.37', '1.e99', '1', '99'], ['missing', '90.']]
cuts_dict["ppnenexa03"]["user"] = [[['user_switch R_gamjet', '1', 'switch to turn on (1) and off (0) cuts on gamma-jet separation'], ['user_cut min_R_gamjet', '0.3', 'requirement on photon-jet separation in y-phi-plane (lower cut)']]]
#}}}
#{{{ ppenexa03 input
proc_dict["ppenexa03"]["process_class"] = "pp-emve~a+X"
proc_dict["ppenexa03"]["scale_ren"] = "80.385"
proc_dict["ppenexa03"]["scale_fact"] = "80.385"
proc_dict["ppenexa03"]["dynamic_scale"] = "6"
proc_dict["ppenexa03"]["factor_central_scale"] = "1"
scale_dict["ppenexa03"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: transverse mass of photon (note: mT_photon=pT_photon)', '4: transverse mass of W boson (lepton+neutrino system, mT_lep+nu)', '5: geometric avarage of mT of photon and mT of W boson', '6: quadratic sum of W mass and mT of the photon (mu^2=m_W^2+mT_photon^2)']
cuts_dict["ppenexa03"]["block"] = [['jet', '30.', '4.4', '1.e99', '0', '99'], ['lep', '25.', '2.47', '1.e99', '1', '99'], ['photon', '15.', '2.37', '1.e99', '1', '99'], ['missing', '35.']]
cuts_dict["ppenexa03"]["user"] = [[['user_switch R_lepgam', '1', 'switch to turn on (1) and off (0) cuts on lepton-photon separation'], ['user_cut min_R_lepgam', '0.7', 'requirement on lepton-photon separation in y-phi-plane (lower cut)']], [['user_switch R_lepjet', '1', 'switch to turn on (1) and off (0) cuts on lepton-jet separation'], ['user_cut min_R_lepjet', '0.3', 'requirement on lepton-jet separation in y-phi-plane (lower cut)']], [['user_switch R_gamjet', '1', 'switch to turn on (1) and off (0) cuts on photon-jet separation'], ['user_cut min_R_gamjet', '0.3', 'requirement on photon-jet separation in y-phi-plane (lower cut)']], [['user_switch mT_CMS', '0', 'switch to turn on (1) and off (0) cuts on transverse W mass (as defined by CMS)'], ['user_cut min_mT_CMS', '10', 'requirement on transverse W mass (lower cut)']], [['user_switch gap_eta_gam', '0', 'switch to turn on (1) and off (0) detector-geometry-motivated gap for eta of photon'], ['user_cut gap_min_eta_gam', '1.1', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_gam', '1.3', 'upper bound of detector gap in eta']]]
#}}}
#{{{ ppexnea03 input
proc_dict["ppexnea03"]["process_class"] = "pp-epvea+X"
proc_dict["ppexnea03"]["scale_ren"] = "80.385"
proc_dict["ppexnea03"]["scale_fact"] = "80.385"
proc_dict["ppexnea03"]["dynamic_scale"] = "6"
proc_dict["ppexnea03"]["factor_central_scale"] = "1"
scale_dict["ppexnea03"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: transverse mass of photon (note: mT_photon=pT_photon)', '4: transverse mass of W boson (lepton+neutrino system, mT_lep+nu)', '5: geometric avarage of mT of photon and mT of W boson', '6: quadratic sum of W mass and mT of the photon (mu^2=m_W^2+mT_photon^2)']
cuts_dict["ppexnea03"]["block"] = [['jet', '30.', '4.4', '1.e99', '0', '99'], ['lep', '25.', '2.47', '1.e99', '1', '99'], ['photon', '15.', '2.37', '1.e99', '1', '99'], ['missing', '35.']]
cuts_dict["ppexnea03"]["user"] = [[['user_switch R_lepgam', '1', 'switch to turn on (1) and off (0) cuts on lepton-photon separation'], ['user_cut min_R_lepgam', '0.7', 'requirement on lepton-photon separation in y-phi-plane (lower cut)']], [['user_switch R_lepjet', '1', 'switch to turn on (1) and off (0) cuts on lepton-jet separation'], ['user_cut min_R_lepjet', '0.3', 'requirement on lepton-jet separation in y-phi-plane (lower cut)']], [['user_switch R_gamjet', '1', 'switch to turn on (1) and off (0) cuts on photon-jet separation'], ['user_cut min_R_gamjet', '0.3', 'requirement on photon-jet separation in y-phi-plane (lower cut)']], [['user_switch mT_CMS', '0', 'switch to turn on (1) and off (0) cuts on transverse W mass (as defined by CMS)'], ['user_cut min_mT_CMS', '10', 'requirement on transverse W mass (lower cut)']], [['user_switch gap_eta_gam', '0', 'switch to turn on (1) and off (0) detector-geometry-motivated gap for eta of photon'], ['user_cut gap_min_eta_gam', '1.1', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_gam', '1.3', 'upper bound of detector gap in eta']]]
#}}}
#{{{ ppzz02 input
proc_dict["ppzz02"]["process_class"] = "pp-zz+X"
proc_dict["ppzz02"]["scale_ren"] = "91.1876"
proc_dict["ppzz02"]["scale_fact"] = "91.1876"
proc_dict["ppzz02"]["dynamic_scale"] = "0"
proc_dict["ppzz02"]["factor_central_scale"] = "1"
scale_dict["ppzz02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppzz02"]["block"] = ['jet', 'z']
cuts_dict["ppzz02"]["user"] = []
#}}}
#{{{ ppwxw02 input
FFS_list.append("ppwxw02")
proc_dict["ppwxw02"]["process_class"] = "pp-wpwm+X"
proc_dict["ppwxw02"]["scale_ren"] = "80.385"
proc_dict["ppwxw02"]["scale_fact"] = "80.385"
proc_dict["ppwxw02"]["dynamic_scale"] = "0"
proc_dict["ppwxw02"]["factor_central_scale"] = "1"
scale_dict["ppwxw02"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppwxw02"]["block"] = ['jet', 'w', 'wp', 'wm']
cuts_dict["ppwxw02"]["user"] = []
#}}}
#{{{ ppemexmx04 input
proc_dict["ppemexmx04"]["process_class"] = "pp-emmumepmup+X"
proc_dict["ppemexmx04"]["scale_ren"] = "91.1876"
proc_dict["ppemexmx04"]["scale_fact"] = "91.1876"
proc_dict["ppemexmx04"]["dynamic_scale"] = "0"
proc_dict["ppemexmx04"]["factor_central_scale"] = "1"
scale_dict["ppemexmx04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: geometric average of Z-boson transverse masses:', '   sqrt(mT_Z1 * mT_Z2)', '4: sum of Z-boson transverse masses computed with their pole masses:', '   sqrt(M_Z^2+pT_ee^2)+sqrt(M_Z^2+pT_mumu^2)', '5: sum of Z-boson transverse masses:', '   sqrt(M_Z1^2+pT_Z1^2)+sqrt(M_Z1^2+pT_Z2^2)']
cuts_dict["ppemexmx04"]["block"] = ['jet', 'lep', ['e', '7.', '4.9', '1.e99', '2', '99'], ['mu', '7.', '2.7', '1.e99', '2', '99'], 'lm', 'lp']
cuts_dict["ppemexmx04"]["user"] = [[['user_switch M_Zrec', '1', 'switch for invariant mass cut on reconstructed Z-bosons (OSSF lepton pairs)'], ['user_cut min_M_Zrec', '66.', 'requirement on reconstructed Z-boson invariant mass (lower cut)'], ['user_cut max_M_Zrec', '116.', 'requirement on reconstructed Z-boson invariant mass (upper cut)'], ['user_cut min_M_Z1', '0.', 'requirement on primary Z-boson (with smaller |m(lm,lp) - M_Z|) (lower cut)'], ['user_cut max_M_Z1', '1.e99', 'requirement on primary Z-boson (with smaller |m(lm,lp) - M_Z|) (upper cut)']], [['user_switch R_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.2', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '0.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_1st', '1.e99', 'requirement on hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_2nd', '0.', 'requirement on second-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_2nd', '1.e99', 'requirement on second-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_3rd', '0.', 'requirement on third-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_3rd', '1.e99', 'requirement on third-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_4th', '0.', 'requirement on fourth-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_4th', '1.e99', 'requirement on fourth-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut extra_eta_lep', '1.e99', 'additional requirement on lepton pseudo-rapidity (upper cut)'], ['user_int n_observed_min_lep_extra', '0', 'minimal number of observed leptons with additional requirement (with cuts above)'], ['user_int n_observed_max_lep_extra', '99', 'maximal number of observed leptons with additional requirement (with cuts above)']], [['user_switch electron_cuts', '1', 'switch to turn on cuts on electrons (comming from Z-boson)'], ['user_cut min_pT_e_1st', '0.', 'requirement on hardest electron transverse momentum (lower cut)'], ['user_cut max_eta_e_1st', '1.e99', 'requirement on hardest electron pseudo-rapidity (upper cut)'], ['user_cut min_pT_e_2nd', '0.', 'requirement on second-hardest electron transverse momentum (lower cut)'], ['user_cut max_eta_e_2nd', '1.e99', 'requirement on second-hardest electron pseudo-rapidity (upper cut)'], ['user_cut extra_eta_e', '2.5', 'additional requirement on electron pseudo-rapidity (upper cut)'], ['user_int n_observed_min_e_extra', '1', 'minimal number of observed electrons with additional requirement (with cuts above)'], ['user_int n_observed_max_e_extra', '99', 'maximal number of observed electrons with additional requirement (with cuts above)']], [['user_switch muon_cuts', '0', 'switch to turn on cuts on muons (comming from Z-boson)'], ['user_cut min_pT_mu_1st', '0.', 'requirement on hardest muon transverse momentum (lower cut)'], ['user_cut max_eta_mu_1st', '1.e99', 'requirement on hardest muon pseudo-rapidity (upper cut)'], ['user_cut min_pT_mu_2nd', '0.', 'requirement on second-hardest muon transverse momentum (lower cut)'], ['user_cut max_eta_mu_2nd', '1.e99', 'requirement on second-hardest muon pseudo-rapidity (upper cut)'], ['user_cut extra_eta_mu', '1.e99', 'additional requirement on muon pseudo-rapidity (upper cut)'], ['user_int n_observed_min_mu_extra', '0', 'minimal number of observed muons with additional requirement (with cuts above)'], ['user_int n_observed_max_mu_extra', '99', 'maximal number of observed muons with additional requirement (with cuts above)']], [['user_switch M_4lep', '0', 'switch to turn on (1) and off (0) cuts on invariant of 4-lepton system'], ['user_cut min_M_4lep', '120.', 'requirement on invariant mass of 4-lepton system (lower cut)'], ['user_cut max_M_4lep', '130.', 'requirement on invariant mass of 4-lepton system (upper cut)'], ['user_cut min_delta_M_4lep', '1.e99', 'minimal difference of 4-lepton invariant mass to PDG Z mass'], ['user_cut max_delta_M_4lep', '0.', 'maximal difference of 4-lepton invariant mass to PDG Z mass']], [['user_switch lep_iso', '0', 'switch to turn on (1) and off (0) isolation between leptons'], ['user_cut lep_iso_delta_0', '0.4', 'lepton isolation cone size'], ['user_cut lep_iso_epsilon', '0.4', 'lepton isolation threshold ratio']]]
cuts_dict["ppemexmx04"]["fiducial"] = [
    [['##fiducial_cut', '66. < M Zrec < 116.', ''],
     ['##fiducial_cut', 'dReta lep lep > 0.2', ''],
     ['#fiducial_cut', 'dReta e e > 0.0', ''],
     ['#fiducial_cut', 'dReta mu mu > 0.0', ''],
     ['#fiducial_cut', 'dReta e mu > 0.0', ''],
     ['#fiducial_cut', 'pT lep (1) > 7.', ''],
     ['#fiducial_cut', '|eta| lep (1) < 2.7', ''],
     ['#fiducial_cut', 'pT lep (2) > 7.', ''],
     ['#fiducial_cut', '|eta| lep (2) < 2.7', ''],
     ['#fiducial_cut', 'pT lep (3) > 7.', ''],
     ['#fiducial_cut', '|eta| lep (3) < 2.7', ''],
     ['#fiducial_cut', 'pT lep (4) > 7.', ''],
     ['#fiducial_cut', '|eta| lep (4) < 4.9', ''],
     ['#fiducial_cut', 'pT e (1) > 7.', ''],
     ['#fiducial_cut', '|eta| e (1) < 2.7', ''],
     ['#fiducial_cut', 'pT e (2) > 7.', ''],
     ['#fiducial_cut', '|eta| e (2) < 4.9', ''],
     ['#fiducial_cut', 'pT mu (1) > 7.', ''],
     ['#fiducial_cut', '|eta| mu (1) < 2.7', ''],
     ['#fiducial_cut', 'pT mu (2) > 7.', ''],
     ['#fiducial_cut', '|eta| mu (2) < 2.7', ''],
     ['#fiducial_cut', '120. < M lep lep lep lep < 130.', '']]]
#}}}
#{{{ ppeeexex04 input
proc_dict["ppeeexex04"]["process_class"] = "pp-ememepep+X"
proc_dict["ppeeexex04"]["scale_ren"] = "91.1876"
proc_dict["ppeeexex04"]["scale_fact"] = "91.1876"
proc_dict["ppeeexex04"]["dynamic_scale"] = "0"
proc_dict["ppeeexex04"]["factor_central_scale"] = "1"
scale_dict["ppeeexex04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: geometric average of Z-boson transverse masses:', '   sqrt(mT_Z1 * mT_Z2)', '4: sum of Z-boson transverse masses computed with their pole masses:', '   sqrt(M_Z^2+pT_ee^2)+sqrt(M_Z^2+pT_mumu^2)', '5: sum of Z-boson transverse masses:', '   sqrt(M_Z1^2+pT_Z1^2)+sqrt(M_Z1^2+pT_Z2^2)']
cuts_dict["ppeeexex04"]["block"] = ['jet', ['lep', '7.', '4.9', '1.e99', '4', '99'], 'e', 'lm', 'lp']
cuts_dict["ppeeexex04"]["user"] = [[['user_switch lepton_identification', '1', 'switch to identify leptons from Z-bosons; (0) off (1) ATLAS (2) CMS']], [['user_switch M_Zrec', '1', 'switch for invariant mass cut on reconstructed Z-bosons; requires: lepton_identification'], ['user_cut min_M_Zrec', '66.', 'requirement on reconstructed Z-boson invariant mass (lower cut)'], ['user_cut max_M_Zrec', '116.', 'requirement on reconstructed Z-boson invariant mass (upper cut)'], ['user_cut min_M_Z1', '0.', 'requirement on primary Z-boson (with smaller |m(lm,lp) - M_Z|) (lower cut)'], ['user_cut max_M_Z1', '1.e99', 'requirement on primary Z-boson (with smaller |m(lm,lp) - M_Z|) (upper cut)']], [['user_switch M_leplep_OSSF', '0', 'switch to turn on (1) and off (0) cuts on OSSF lepton-lepton invariant mass'], ['user_cut min_M_leplep_OSSF', '0.', 'requirement on OSSF lepton-lepton invariant mass (lower cut) to ensure IR safety']], [['user_switch R_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.2', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch lepton_cuts', '1', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '0.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_1st', '1.e99', 'requirement on hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_2nd', '0.', 'requirement on second-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_2nd', '1.e99', 'requirement on second-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_3rd', '0.', 'requirement on third-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_3rd', '1.e99', 'requirement on third-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut min_pT_lep_4th', '0.', 'requirement on fourth-hardest lepton transverse momentum (lower cut)'], ['user_cut max_eta_lep_4th', '1.e99', 'requirement on fourth-hardest lepton pseudo-rapidity (upper cut)'], ['user_cut extra_eta_lep', '2.5', 'additional requirement on electron pseudo-rapidity (upper cut)'], ['user_int n_observed_min_lep_extra', '3', 'minimal number of observed electrons with additional requirement (with cuts above)'], ['user_int n_observed_max_lep_extra', '99', 'maximal number of observed electrons with additional requirement (with cuts above)']], [['user_switch M_4lep', '0', 'switch to turn on (1) and off (0) cuts on invariant of 4-lepton system'], ['user_cut min_M_4lep', '120.', 'requirement on invariant mass of 4-lepton system (lower cut)'], ['user_cut max_M_4lep', '130.', 'requirement on invariant mass of 4-lepton system (upper cut)'], ['user_cut min_delta_M_4lep', '1.e99', 'minimal difference of 4-lepton invariant mass to PDG Z mass'], ['user_cut max_delta_M_4lep', '0.', 'maximal difference of 4-lepton invariant mass to PDG Z mass']], [['user_switch lep_iso', '0', 'switch to turn on (1) and off (0) isolation between leptons'], ['user_cut lep_iso_delta_0', '0.4', 'lepton isolation cone size'], ['user_cut lep_iso_epsilon', '0.4', 'lepton isolation threshold ratio']]]
#}}}
#{{{ ppeexnmnmx04 input
proc_dict["ppeexnmnmx04"]["process_class"] = "pp-emepvmvm~+X"
proc_dict["ppeexnmnmx04"]["scale_ren"] = "91.1876"
proc_dict["ppeexnmnmx04"]["scale_fact"] = "91.1876"
proc_dict["ppeexnmnmx04"]["dynamic_scale"] = "0"
proc_dict["ppeexnmnmx04"]["factor_central_scale"] = "1"
scale_dict["ppeexnmnmx04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: sum of Z-boson transverse masses computed with Z pole mass:', '   sqrt(M_Z^2+pT_leplep^2)+sqrt(M_Z^2+pT_nunu^2)', '4: sum of Z-boson transverse masses:', '   sqrt(m_ee^2+pT_ee^2)+sqrt(m_nunu^2+pT_nunu^2)']
cuts_dict["ppeexnmnmx04"]["block"] = ['jet', ['lep', '25.', '2.5', '1.e99', '2', '99'], 'lm', 'lp', 'missing']
cuts_dict["ppeexnmnmx04"]["user"] = [[['user_switch M_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass'], ['user_cut min_M_leplep', '76.', 'requirement on lepton-lepton invariant mass (lower cut)'], ['user_cut max_M_leplep', '106.', 'requirement on lepton-lepton invariant mass (upper cut)']], [['user_switch R_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.3', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch R_ejet', '1', 'switch to turn on (1) and off (0) cuts on electron-jet separation'], ['user_cut min_R_ejet', '0.3', 'requirement on electron-jet separation in y-phi-plane (lower cut);'], ['', '', 'jets to close to electrons are removed by creating a modified jet content']], [['user_switch n_jet_modified', '1', 'switch to turn on (1) and off (0) cuts on number of jets in modified jet content'], ['user_int n_jet_min_modified', '0', 'minimal number of jets in modified jet list, eg, R_ejet removes jets from original list'], ['user_int n_jet_max_modified', '0', 'maximal number of jets in modified jet list, eg, R_ejet removes jets from original list']], [['user_switch M_leplepnunu', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton-nu-nu invariant mass'], ['user_cut min_M_leplepnunu', '0.', 'requirement on lepton-lepton-nu-nu invariant mass (lower cut)'], ['user_cut max_M_leplepnunu', '1.e99', 'requirement on lepton-lepton-nu-nu invariant mass (upper cut)'], ['user_cut min_delta_M_leplepnunu', '0.', 'minimal difference to PDG Z mass'], ['user_cut max_delta_M_leplepnunu', '1.e99', 'maximal difference to PDG Z mass']], [['user_switch axial_ETmiss', '1', 'switch to turn on (1) and off (0) cut on axial missing ET (see arXiv:1610.07585)'], ['user_cut min_axial_ETmiss', '90.', 'requirement on axial missing transverse energy (lower cut)']], [['user_switch pT_balance_Zs', '1', "switch to turn on (1) and off (0) cut on pT balance between Z's (see arXiv:1610.07585)"], ['user_cut max_pT_balance_Zs', '0.4', 'requirement on tranvserse-momentum balance between the two Z bosons (upper cut)']]]
#}}}
#{{{ ppemxnmnex04 input
FFS_list.append("ppemxnmnex04")
proc_dict["ppemxnmnex04"]["process_class"] = "pp-emmupvmve~+X"
proc_dict["ppemxnmnex04"]["scale_ren"] = "80.385"
proc_dict["ppemxnmnex04"]["scale_fact"] = "80.385"
proc_dict["ppemxnmnex04"]["dynamic_scale"] = "0"
proc_dict["ppemxnmnex04"]["factor_central_scale"] = "1"
scale_dict["ppemxnmnex04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: sum of W-boson transverse masses computed with W pole mass:', '   sqrt(M_W^2+pT_enu^2)+sqrt(M_W^2+pT_munu^2)', '4: sum of W-boson transverse masses:', '   sqrt(m_enu^2+pT_enu^2)+sqrt(m_munu^2+pT_munu^2)']
cuts_dict["ppemxnmnex04"]["block"] = [['jet', '25.', '4.5', '1.e99', '0', '0'], 'lep', ['e', '20.', '2.47', '1.e99', '1', '99'], ['mu', '20.', '2.4', '1.e99', '1', '99'], ['missing', '20.']]
cuts_dict["ppemxnmnex04"]["user"] = [[['user_switch M_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass'], ['user_cut min_M_leplep', '10.', 'requirement on lepton-lepton invariant mass (lower cut)'], ['user_cut max_M_leplep', '1.e99', 'requirement on lepton-lepton invariant mass (upper cut)']], [['user_switch R_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.1', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch R_ejet', '0', 'switch to turn on (1) and off (0) cuts on electron-jet separation'], ['user_cut min_R_ejet', '0.', 'requirement on electron-jet separation in y-phi-plane (lower cut);'], ['', '', 'jets to close to electrons are removed by creating a modified jet content']], [['user_switch n_jet_modified', '0', 'switch to turn on (1) and off (0) cuts on number of jets in modified jet content'], ['user_int n_jet_min_modified', '0', 'minimal number of jets in modified jet list, eg, R_ejet removes jets from original list'], ['user_int n_jet_max_modified', '2', 'maximal number of jets in modified jet list, eg, R_ejet removes jets from original list']], [['user_switch M_leplepnunu', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton-nu-nu invariant mass'], ['user_cut min_M_leplepnunu', '0.', 'requirement on lepton-lepton-nu-nu invariant mass (lower cut)'], ['user_cut max_M_leplepnunu', '1.e99', 'requirement on lepton-lepton-nu-nu invariant mass (upper cut)'], ['user_cut min_delta_M_leplepnunu', '0.', 'minimal difference to PDG Z mass'], ['user_cut max_delta_M_leplepnunu', '1.e99', 'maximal difference to PDG Z mass']], [['user_switch pT_leplep', '0', 'switch to turn on (1) and off (0) cuts on transverse momentum of lepton pair'], ['user_cut min_pT_leplep', '0.', 'requirement on pT of the lepton pair (lower cut)']], [['user_switch pT_lep_1st', '1', 'switch to turn on (1) and off (0) cuts on pT of hardest lepton'], ['user_cut min_pT_lep_1st', '25.', 'requirement on pT of hardest lepton (lower cut)']], [['user_switch gap_eta_e', '1', 'switch to turn on (1) and off (0) detector gap for eta of electrons'], ['user_cut gap_min_eta_e', '1.37', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_e', '1.52', 'upper bound of detector gap in eta']], [['user_switch rel_pT_miss', '1', 'switch to turn on (1) and off (0) track-based missing-pT cut'], ['user_cut min_rel_pT_miss', '15.', 'track-based cut on missing pT (ATLAS, see http://cds.cern.ch/record/1728248)']], [['user_switch phi_leplep', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_phi_leplep', '0.', 'requirement on lepton-lepton separation in phi-plane (lower cut)'], ['user_cut max_phi_leplep', '1.5708', 'requirement on lepton-lepton separation in phi-plane (upper cut)']], [['user_switch phi_leplep_nunu', '0', 'switch to turn on (1) and off (0) cuts on separation between leptons and neutrinos'], ['user_cut min_phi_leplep_nunu', '1.5708', 'requirement on separation between leptons and neutrinos in phi-plane (lower cut)']], [['user_switch axial_ETmiss', '0', 'switch to turn on (1) and off (0) cut on axial missing ET (see arXiv:1610.07585)'], ['user_cut min_axial_ETmiss', '0.', 'requirement on axial missing transverse energy (lower cut)']], [['user_switch pT_balance_Zs', '0', "switch to turn on (1) and off (0) cut on pT balance between Z's (see arXiv:1610.07585)"], ['user_cut max_pT_balance_Zs', '1.e99', 'requirement on tranvserse-momentum balance between the two Z bosons (upper cut)']], [['user_switch pT_W', '0', 'switch to turn on cut on transverse momentum of the W-bosons (0) off;'], ['', '', '(1) cut on all contributions;'], ['', '', '(2) cut only on O(as^2) (non-loop induced) contributions (default)'], ['user_cut min_pT_W', '0.', 'requirement on pT of the W-bosons (lower cut)'], ['user_cut max_pT_W', '1.e99', 'requirement on pT of the W-bosons (upper cut)']]]
cuts_dict["ppemxnmnex04"]["user_new"] = [
    [['', '', 'jets to close to electrons are removed by creating a modified jet content']],
    [['user_switch n_jet_modified', '0', 'switch to turn on (1) and off (0) cuts on number of jets in modified jet content'],
     ['user_int n_jet_min_modified', '0', 'minimal number of jets in modified jet list, eg, R_ejet removes jets from original list'],
     ['user_int n_jet_max_modified', '2', 'maximal number of jets in modified jet list, eg, R_ejet removes jets from original list']],
    [['user_switch rel_pT_miss', '1', 'switch to turn on (1) and off (0) track-based missing-pT cut'],
     ['user_cut min_rel_pT_miss', '15.', 'track-based cut on missing pT (ATLAS, see http://cds.cern.ch/record/1728248)']],
    [['user_switch axial_ETmiss', '0', 'switch to turn on (1) and off (0) cut on axial missing ET (see arXiv:1610.07585)'],
     ['user_cut min_axial_ETmiss', '0.', 'requirement on axial missing transverse energy (lower cut)']],
    [['user_switch pT_balance_Zs', '0', "switch to turn on (1) and off (0) cut on pT balance between Z's (see arXiv:1610.07585)"],
     ['user_cut max_pT_balance_Zs', '1.e99', 'requirement on tranvserse-momentum balance between the two Z bosons (upper cut)']],
    [['user_switch pT_W', '0', 'switch to turn on cut on transverse momentum of the W-bosons (0) off;'],
     ['', '', '(1) cut on all contributions;'],
     ['', '', '(2) cut only on O(as^2) (non-loop induced) contributions (default)'],
     ['user_cut min_pT_W', '0.', 'requirement on pT of the W-bosons (lower cut)'],
     ['user_cut max_pT_W', '1.e99', 'requirement on pT of the W-bosons (upper cut)']]]
cuts_dict["ppemxnmnex04"]["fiducial"] = [
    [['##fiducial_cut', 'M lep lep > 10.', 'requirement on lepton-lepton invariant mass'],
     ['##fiducial_cut', 'dReta lep lep > 0.1', 'requirement on lepton-lepton separation in eta-phi-plane'],
     ['#fiducial_cut', 'dRy lep lep > 0.1', 'requirement on lepton-lepton separation in y-phi-plane'],
     ['#fiducial_cut', 'dReta e jet > 0.1', 'requirement on electron-jet separation in eta-phi-plane'],
     ['#fiducial_cut', 'dRy e jet > 0.1', 'requirement on electron-jet separation in y-phi-plane'],
     ['#fiducial_cut', 'dReta mu jet > 0.1', ''],
     ['#fiducial_cut', 'dReta lep jet > 0.1', ''],
     ['#fiducial_cut', 'pT dilepton > 0.', ''],
     ['##fiducial_cut', 'pT lep (1) > 25.', ''],
     ['##fiducial_cut', '|eta| e < 1.37 > 1.52', ''],
     ['#fiducial_cut', 'dphi lep lep < 1.5708', ''],
     ['#fiducial_cut', 'dphi dilepton missing > 1.5708', '']]]
#}}}
#{{{ ppeexnenex04 input
FFS_list.append("ppeexnenex04")
proc_dict["ppeexnenex04"]["process_class"] = "pp-emepveve~+X"
proc_dict["ppeexnenex04"]["scale_ren"] = "91.1876"
proc_dict["ppeexnenex04"]["scale_fact"] = "91.1876"
proc_dict["ppeexnenex04"]["dynamic_scale"] = "0"
proc_dict["ppeexnenex04"]["factor_central_scale"] = "1"
scale_dict["ppeexnenex04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: sum of W-boson transverse masses computed with W pole mass:', '   sqrt(M_W^2+pT_emnu^2)+sqrt(M_W^2+pT_epnu^2)', '4: sum of W-boson transverse masses:', '   sqrt(m_enu^2+pT_emnu^2)+sqrt(m_munu^2+pT_epnu^2)']
cuts_dict["ppeexnenex04"]["block"] = ['jet', ['lep', '25.', '2.5', '1.e99', '2', '99'], 'lm', 'lp', 'missing']
cuts_dict["ppeexnenex04"]["user"] = [[['user_switch M_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass'], ['user_cut min_M_leplep', '76.', 'requirement on lepton-lepton invariant mass (lower cut)'], ['user_cut max_M_leplep', '106.', 'requirement on lepton-lepton invariant mass (upper cut)']], [['user_switch R_leplep', '1', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_R_leplep', '0.3', 'requirement on lepton-lepton separation in y-phi-plane (lower cut)']], [['user_switch R_ejet', '1', 'switch to turn on (1) and off (0) cuts on electron-jet separation'], ['user_cut min_R_ejet', '0.3', 'requirement on electron-jet separation in y-phi-plane (lower cut);'], ['', '', 'jets to close to electrons are removed by creating a modified jet content']], [['user_switch n_jet_modified', '1', 'switch to turn on (1) and off (0) cuts on number of jets in modified jet content'], ['user_int n_jet_min_modified', '0', 'minimal number of jets in modified jet list, eg, R_ejet removes jets from original list'], ['user_int n_jet_max_modified', '0', 'maximal number of jets in modified jet list, eg, R_ejet removes jets from original list']], [['user_switch M_leplepnunu', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton-nu-nu invariant mass'], ['user_cut min_M_leplepnunu', '0.', 'requirement on lepton-lepton-nu-nu invariant mass (lower cut)'], ['user_cut max_M_leplepnunu', '1.e99', 'requirement on lepton-lepton-nu-nu invariant mass (upper cut)'], ['user_cut min_delta_M_leplepnunu', '0.', 'minimal difference to PDG Z mass'], ['user_cut max_delta_M_leplepnunu', '1.e99', 'maximal difference to PDG Z mass']], [['user_switch axial_ETmiss', '1', 'switch to turn on (1) and off (0) cut on axial missing ET (see arXiv:1610.07585)'], ['user_cut min_axial_ETmiss', '90.', 'requirement on axial missing transverse energy (lower cut)']], [['user_switch pT_balance_Zs', '1', "switch to turn on (1) and off (0) cut on pT balance between Z's (see arXiv:1610.07585)"], ['user_cut max_pT_balance_Zs', '0.4', 'requirement on tranvserse-momentum balance between the two Z bosons (upper cut)']], [['user_switch pT_leplep', '0', 'switch to turn on (1) and off (0) cuts on transverse momentum of lepton pair'], ['user_cut min_pT_leplep', '0.', 'requirement on pT of the lepton pair (lower cut)']], [['user_switch pT_lep_1st', '0', 'switch to turn on (1) and off (0) cuts on pT of hardest lepton'], ['user_cut min_pT_lep_1st', '0.', 'requirement on pT of hardest lepton (lower cut)']], [['user_switch gap_eta_e', '0', 'switch to turn on (1) and off (0) detector gap for eta of electrons'], ['user_cut gap_min_eta_e', '1.37', 'lower bound of detector gap in eta'], ['user_cut gap_max_eta_e', '1.52', 'upper bound of detector gap in eta']], [['user_switch rel_pT_miss', '0', 'switch to turn on (1) and off (0) track-based missing-pT cut'], ['user_cut min_rel_pT_miss', '0.', 'track-based cut on missing pT (ATLAS, see http://cds.cern.ch/record/1728248)']], [['user_switch phi_leplep', '0', 'switch to turn on (1) and off (0) cuts on lepton-lepton separation'], ['user_cut min_phi_leplep', '0.', 'requirement on lepton-lepton separation in phi-plane (lower cut)'], ['user_cut max_phi_leplep', '3.1416', 'requirement on lepton-lepton separation in phi-plane (upper cut)']], [['user_switch phi_leplep_nunu', '0', 'switch to turn on (1) and off (0) cuts on separation between leptons and neutrinos'], ['user_cut min_phi_leplep_nunu', '0.', 'requirement on separation between leptons and neutrinos in phi-plane (lower cut)']], [['user_switch pT_W', '0', 'switch to turn on cut on transverse momentum of the W-bosons (0) off;'], ['', '', '(1) cut on all contributions;'], ['', '', '(2) cut only on O(as^2) (non-loop induced) contributions (default)'], ['user_cut min_pT_W', '0.', 'requirement on pT of the W-bosons (lower cut)'], ['user_cut max_pT_W', '1.e99', 'requirement on pT of the W-bosons (upper cut)']]]
#}}}
#{{{ ppemexnmx04 input
proc_dict["ppemexnmx04"]["process_class"] = "pp-emmumepvm~+X"
proc_dict["ppemexnmx04"]["scale_ren"] = "85.7863"
proc_dict["ppemexnmx04"]["scale_fact"] = "85.7863"
proc_dict["ppemexnmx04"]["dynamic_scale"] = "0"
proc_dict["ppemexnmx04"]["factor_central_scale"] = "1"
scale_dict["ppemexnmx04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: average of Z- and W-boson transverse masses computed with their pole masses:', '   (sqrt(M_Z^2+pT_ee^2)+sqrt(M_W^2+pT_munu^2))/2', '4: average of Z- and W-boson transverse masses:', '   (sqrt(m_ee^2+pT_ee^2)+sqrt(m_munu^2+pT_munu^2))/2']
cuts_dict["ppemexnmx04"]["block"] = ['jet', ['lep', '15.', '2.5', '1.e99', '3', '99'], 'e', ['mu', '20.', '2.5', '1.e99', '1', '99'], 'missing']
cuts_dict["ppemexnmx04"]["user"] = [[['user_switch M_Zrec', '0', 'switch for invariant mass cut on the electron pair (Z-boson)'], ['user_cut min_M_Zrec', '66.', '(lower cut)'], ['user_cut max_M_Zrec', '116.', '(upper cut)']], [['user_switch delta_M_Zrec_MZ', '1', 'switch for invariant mass cut on the electron pair (Z-boson)'], ['user_cut max_delta_M_Zrec_MZ', '10.', 'maximal difference to PDG Z mass']], [['user_switch delta_M_lepleplep_MZ', '0', 'switch for invariant mass cut on the three leptons'], ['user_cut min_delta_M_lepleplep_MZ', '15.', 'minimal difference to PDG Z mass']], [['user_switch R_leplep', '0', 'switch for lepton separation (between all leptons)'], ['user_cut min_R_leplep', '0.2', '(lower cut)']], [['user_switch R_lepZlepZ', '1', 'switch for lepton separation between the electrons'], ['user_cut min_R_lepZlepZ', '0.2', '(lower cut)']], [['user_switch R_lepZlepW', '1', 'switch for lepton separation between electrons and the muon'], ['user_cut min_R_lepZlepW', '0.3', '(lower cut)']], [['user_switch electron_cuts', '0', 'switch to turn on cuts on electrons (comming from Z-boson)'], ['user_cut min_pT_e_1st', '20.', 'requirement on hardest electron transverse momentum (lower cut)'], ['user_cut min_pT_e_2nd', '10.', 'requirement on second-hardest electron transverse momentum (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '25.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut min_pT_lep_2nd', '0.', 'requirement on second-hardest lepton transverse momentum (lower cut)']], [['user_switch leading_lepton_cuts', '0', 'switch to turn on cuts on the leading lepton'], ['user_cut min_pT_1st_if_e', '25.', 'requirement on leading lepton pT if it is an electron (lower cut)'], ['user_cut min_pT_1st_if_mu', '20.', 'requirement on leading lepton pT if it is a muon (lower cut)']], [['user_switch MT_Wrec', '1', 'switch for transverse mass cut on W-boson'], ['user_cut min_MT_Wrec', '30.', '(lower cut)']]]
#}}}
#{{{ ppeeexnex04 input
proc_dict["ppeeexnex04"]["process_class"] = "pp-ememepve~+X"
proc_dict["ppeeexnex04"]["scale_ren"] = "85.7863"
proc_dict["ppeeexnex04"]["scale_fact"] = "85.7863"
proc_dict["ppeeexnex04"]["dynamic_scale"] = "0"
proc_dict["ppeeexnex04"]["factor_central_scale"] = "1"
scale_dict["ppeeexnex04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: average of Z- and W-boson transverse masses computed with their pole masses:', '   (sqrt(M_Z^2+pT_Zrec^2)+sqrt(M_W^2+pT_Wrec^2))/2 (Zrec/Wrec are the reconstructed Z/W bosons)', '4: average of Z- and W-boson transverse masses:', '   (mT_Zrec+mT_Wrec)/2 (Zrec/Wrec are the reconstructed Z/W bosons)']
cuts_dict["ppeeexnex04"]["block"] = ['jet', ['lep', '15.', '2.5', '1.e99', '3', '99'], 'lm', 'lp', 'missing']
cuts_dict["ppeeexnex04"]["user"] = [[['user_switch lepton_identification', '1', 'switch to identify leptons from Z- and W-boson; (0) off (1) ATLAS (2) CMS']], [['user_switch M_Zrec', '0', 'switch for invariant mass cut on reconstructed Z-boson; requires: lepton_identification'], ['user_cut min_M_Zrec', '66.', '(lower cut)'], ['user_cut max_M_Zrec', '116.', '(upper cut)']], [['user_switch delta_M_Zrec_MZ', '1', 'switch for invariant mass cut on reconstructed Z-boson; requires: lepton_identification'], ['user_cut max_delta_M_Zrec_MZ', '10.', 'maximal difference to PDG Z mass']], [['user_switch M_leplep_OSSF', '0', 'switch for invariant mass cut on opposite-sign, same-flavour (OSSF) lepton pairs'], ['user_cut min_M_leplep_OSSF', '4.', '(lower cut) to ensure IR safety']], [['user_switch delta_M_lepleplep_MZ', '0', 'switch for invariant mass cut on the three leptons'], ['user_cut min_delta_M_lepleplep_MZ', '15.', 'minimal difference to PDG Z mass']], [['user_switch R_leplep', '0', 'switch for lepton separation (between all leptons)'], ['user_cut min_R_leplep', '0.2', '(lower cut) to ensure IR safety']], [['user_switch R_lepZlepZ', '1', 'switch for lepton separation between leptons from Z-boson; requires: lepton_identification'], ['user_cut min_R_lepZlepZ', '0.2', '(lower cut) to ensure IR safety']], [['user_switch R_lepZlepW', '1', 'switch for lepton separation between leptons from Z- and W-boson; requires: lepton_identification'], ['user_cut min_R_lepZlepW', '0.3', '(lower cut) to ensure IR safety']], [['user_switch lepW_cuts', '1', 'switch to turn on cuts on lepton comming from W-boson; requires: lepton_identification'], ['user_cut min_pT_lepW', '20.', 'requirement on W-lepton transverse momentum (lower cut)'], ['user_cut max_eta_lepW', '2.5', 'requirement on W-lepton pseudo rapidity (upper cut)']], [['user_switch lepZ_cuts', '0', 'switch to turn on cuts on leptons comming from Z-boson; requires: lepton_identification'], ['user_cut min_pT_lepZ_1st', '20.', 'requirement on hardest Z-lepton transverse momentum (lower cut)'], ['user_cut min_pT_lepZ_2nd', '10.', 'requirement on second-hardest Z-lepton transverse momentum (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '25.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut min_pT_lep_2nd', '15.', 'requirement on second-hardest lepton transverse momentum (lower cut)']], [['user_switch MT_Wrec', '1', 'switch for transverse mass cut on reconstructed W-boson; requires: lepton_identification'], ['user_cut min_MT_Wrec', '30.', '(lower cut)']]]
#}}}
#{{{ ppeexmxnm04 input
proc_dict["ppeexmxnm04"]["process_class"] = "pp-emepmupvm+X"
proc_dict["ppeexmxnm04"]["scale_ren"] = "85.7863"
proc_dict["ppeexmxnm04"]["scale_fact"] = "85.7863"
proc_dict["ppeexmxnm04"]["dynamic_scale"] = "0"
proc_dict["ppeexmxnm04"]["factor_central_scale"] = "1"
scale_dict["ppeexmxnm04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: average of Z- and W-boson transverse masses computed with their pole masses:', '   (sqrt(M_Z^2+pT_ee^2)+sqrt(M_W^2+pT_munu^2))/2', '4: average of Z- and W-boson transverse masses:', '   (sqrt(m_ee^2+pT_ee^2)+sqrt(m_munu^2+pT_munu^2))/2']
cuts_dict["ppeexmxnm04"]["block"] = ['jet', ['lep', '15.', '2.5', '1.e99', '3', '99'], 'e', ['mu', '20.', '2.5', '1.e99', '1', '99'], 'missing']
cuts_dict["ppeexmxnm04"]["user"] = [[['user_switch M_Zrec', '0', 'switch for invariant mass cut on the electron pair (Z-boson)'], ['user_cut min_M_Zrec', '66.', '(lower cut)'], ['user_cut max_M_Zrec', '116.', '(upper cut)']], [['user_switch delta_M_Zrec_MZ', '1', 'switch for invariant mass cut on the electron pair (Z-boson)'], ['user_cut max_delta_M_Zrec_MZ', '10.', 'maximal difference to PDG Z mass']], [['user_switch delta_M_lepleplep_MZ', '0', 'switch for invariant mass cut on the three leptons'], ['user_cut min_delta_M_lepleplep_MZ', '15.', 'minimal difference to PDG Z mass']], [['user_switch R_leplep', '0', 'switch for lepton separation (between all leptons)'], ['user_cut min_R_leplep', '0.2', '(lower cut)']], [['user_switch R_lepZlepZ', '1', 'switch for lepton separation between the electrons'], ['user_cut min_R_lepZlepZ', '0.2', '(lower cut)']], [['user_switch R_lepZlepW', '1', 'switch for lepton separation between electrons and the muon'], ['user_cut min_R_lepZlepW', '0.3', '(lower cut)']], [['user_switch electron_cuts', '0', 'switch to turn on cuts on electrons (comming from Z-boson)'], ['user_cut min_pT_e_1st', '20.', 'requirement on hardest electron transverse momentum (lower cut)'], ['user_cut min_pT_e_2nd', '10.', 'requirement on second-hardest electron transverse momentum (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '25.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut min_pT_lep_2nd', '0.', 'requirement on second-hardest lepton transverse momentum (lower cut)']], [['user_switch leading_lepton_cuts', '0', 'switch to turn on cuts on the leading lepton'], ['user_cut min_pT_1st_if_e', '25.', 'requirement on leading lepton pT if it is an electron (lower cut)'], ['user_cut min_pT_1st_if_mu', '20.', 'requirement on leading lepton pT if it is a muon (lower cut)']], [['user_switch MT_Wrec', '1', 'switch for transverse mass cut on W-boson'], ['user_cut min_MT_Wrec', '30.', '(lower cut)']]]
#}}}
#{{{ ppeexexne04 input
proc_dict["ppeexexne04"]["process_class"] = "pp-emepepve+X"
proc_dict["ppeexexne04"]["scale_ren"] = "85.7863"
proc_dict["ppeexexne04"]["scale_fact"] = "85.7863"
proc_dict["ppeexexne04"]["dynamic_scale"] = "0"
proc_dict["ppeexexne04"]["factor_central_scale"] = "1"
scale_dict["ppeexexne04"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)', '3: average of Z- and W-boson transverse masses computed with their pole masses:', '   (sqrt(M_Z^2+pT_Zrec^2)+sqrt(M_W^2+pT_Wrec^2))/2 (Zrec/Wrec are the reconstructed Z/W bosons)', '4: average of Z- and W-boson transverse masses:', '   (mT_Zrec+mT_Wrec)/2 (Zrec/Wrec are the reconstructed Z/W bosons)']
cuts_dict["ppeexexne04"]["block"] = ['jet', ['lep', '15.', '2.5', '1.e99', '3', '99'], 'lm', 'lp', 'missing']
cuts_dict["ppeexexne04"]["user"] = [[['user_switch lepton_identification', '1', 'switch to identify leptons from Z- and W-boson; (0) off (1) ATLAS (2) CMS']], [['user_switch M_Zrec', '0', 'switch for invariant mass cut on reconstructed Z-boson; requires: lepton_identification'], ['user_cut min_M_Zrec', '66.', '(lower cut)'], ['user_cut max_M_Zrec', '116.', '(upper cut)']], [['user_switch delta_M_Zrec_MZ', '1', 'switch for invariant mass cut on reconstructed Z-boson; requires: lepton_identification'], ['user_cut max_delta_M_Zrec_MZ', '10.', 'maximal difference to PDG Z mass']], [['user_switch M_leplep_OSSF', '0', 'switch for invariant mass cut on opposite-sign, same-flavour (OSSF) lepton pairs'], ['user_cut min_M_leplep_OSSF', '4.', '(lower cut) to ensure IR safety']], [['user_switch delta_M_lepleplep_MZ', '0', 'switch for invariant mass cut on the three leptons'], ['user_cut min_delta_M_lepleplep_MZ', '15.', 'minimal difference to PDG Z mass']], [['user_switch R_leplep', '0', 'switch for lepton separation (between all leptons)'], ['user_cut min_R_leplep', '0.2', '(lower cut) to ensure IR safety']], [['user_switch R_lepZlepZ', '1', 'switch for lepton separation between leptons from Z-boson; requires: lepton_identification'], ['user_cut min_R_lepZlepZ', '0.2', '(lower cut) to ensure IR safety']], [['user_switch R_lepZlepW', '1', 'switch for lepton separation between leptons from Z- and W-boson; requires: lepton_identification'], ['user_cut min_R_lepZlepW', '0.3', '(lower cut) to ensure IR safety']], [['user_switch lepW_cuts', '1', 'switch to turn on cuts on lepton comming from W-boson; requires: lepton_identification'], ['user_cut min_pT_lepW', '20.', 'requirement on W-lepton transverse momentum (lower cut)'], ['user_cut max_eta_lepW', '2.5', 'requirement on W-lepton pseudo rapidity (upper cut)']], [['user_switch lepZ_cuts', '0', 'switch to turn on cuts on leptons comming from Z-boson; requires: lepton_identification'], ['user_cut min_pT_lepZ_1st', '20.', 'requirement on hardest Z-lepton transverse momentum (lower cut)'], ['user_cut min_pT_lepZ_2nd', '10.', 'requirement on second-hardest Z-lepton transverse momentum (lower cut)']], [['user_switch lepton_cuts', '0', 'switch to turn on cuts on leptons'], ['user_cut min_pT_lep_1st', '25.', 'requirement on hardest lepton transverse momentum (lower cut)'], ['user_cut min_pT_lep_2nd', '15.', 'requirement on second-hardest lepton transverse momentum (lower cut)']], [['user_switch MT_Wrec', '1', 'switch for transverse mass cut on reconstructed W-boson; requires: lepton_identification'], ['user_cut min_MT_Wrec', '30.', '(lower cut)']]]
#}}}
#{{{ ppttx20 input
proc_dict["ppttx20"]["process_class"] = "pp-ttx+X"
proc_dict["ppttx20"]["scale_ren"] = "173.3"
proc_dict["ppttx20"]["scale_fact"] = "173.3"
proc_dict["ppttx20"]["dynamic_scale"] = "0"
proc_dict["ppttx20"]["factor_central_scale"] = "1"
scale_dict["ppttx20"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppttx20"]["block"] = ['jet']
cuts_dict["ppttx20"]["user"] = []
#}}}
#{{{ ppbbx20 input
proc_dict["ppbbx20"]["process_class"] = "pp-bbx+X"
proc_dict["ppbbx20"]["scale_ren"] = "4.2"
proc_dict["ppbbx20"]["scale_fact"] = "4.2"
proc_dict["ppbbx20"]["dynamic_scale"] = "0"
proc_dict["ppbbx20"]["factor_central_scale"] = "1"
scale_dict["ppbbx20"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["ppbbx20"]["block"] = ['jet']
cuts_dict["ppbbx20"]["user"] = []
#}}}
#{{{ pphh22 input
proc_dict["pphh22"]["process_class"] = "pp-hh+X"
proc_dict["pphh22"]["scale_ren"] = "125."
proc_dict["pphh22"]["scale_fact"] = "125."
proc_dict["pphh22"]["dynamic_scale"] = "1"
proc_dict["pphh22"]["factor_central_scale"] = "0.5"
scale_dict["pphh22"] = ['0: fixed scale above', '1: invariant mass (Q) of system (of the colourless final states)', '2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)']
cuts_dict["pphh22"]["block"] = ['jet', 'h']
cuts_dict["pphh22"]["user"] = []
#}}}
