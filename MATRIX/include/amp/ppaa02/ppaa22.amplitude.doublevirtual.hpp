void ppaa22_calculate_H2(observable_set * oset);

double ppaa22_A0(double s, double t, double u);
double ppaa22_H1f(double s, double t, double u);
double ppaa22_H2f(double s, double t, double u, unsigned int Nf, double Qq);
