#include "tdhpl.hpp"

void pplla23_calculate_H2(observable_set * osi);
void pplla23_calculate_amplitude_doublevirtual(observable_set * osi);

double_complex pplla23_alpha0_Z(double u, double v, double e_q);
double_complex pplla23_beta0_Z(double u, double v, double e_q);
//double_complex gamma0_Z(double u, double v, double e_q);
double_complex pplla23_alpha0_W(double u, double v, double e_q, double e_qprime, double q2, double shat);
double_complex pplla23_beta0_W(double u, double v, double e_q, double e_qprime, double q2, double shat);
double_complex pplla23_F2_q(double LR, observable_set * osi);
void pplla23_calculate_H_coefficients(double q2, double s, int order, observable_set * osi);
void pplla23_calculate_helicity_amplitude_perm(int d, int e, int a, int b, int c, const vector<fourvector> &p, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<vector <double> > s, double e_q, double_complex sin_W, double M_V, double Gamma_V, int process, vector<double_complex> &Aplus_0, vector<double_complex> &Aplus_1, vector<double_complex> &Aplus_2_VR, vector<double_complex> &Aplus_2_VL, vector<double_complex> &Aplus_2_P, int order, observable_set * osi);
void pplla23_calculate_helicity_amplitude_perm_FSR(int d, int e, int a, int b, int c, const vector<fourvector> &p, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<vector <double> > s, int process, vector<double_complex> &Aplus_0, vector<double_complex> &Aplus_1, vector<double_complex> &Aplus_2, int order, observable_set * osi);




// pplla23/pplla23.coefficientsVgam1L.cpp
void alpha_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha);
void alpha_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha);
void alpha_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha);
void beta_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta);
void beta_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta);
void beta_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta);
void gamma_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma);
void gamma_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma);
void gamma_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma);

void Omega_1_ZP(double u, double v, double e_q, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha, double_complex &beta, double_complex &gamma);
void Omega_1_W(double u, double v, double e_q, double e_qprime, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha, double_complex &beta, double_complex &gamma);


// pplla23/pplla23.coefficientsVgam2L.cpp
void alpha_2_1(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha);
void alpha_2_2(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha);
void alpha_2_3(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha);
void alpha_2_4(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha);
void beta_2_1(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &beta);
void beta_2_2(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &beta);
void beta_2_3(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &beta);
void beta_2_4(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &beta);
void gamma_2_1(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &gamma);
void gamma_2_2(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &gamma);
void gamma_2_3(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &gamma);
void gamma_2_4(double u, double v, int n_f, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &gamma);

void Omega_2_ZP(double u, double v, double e_q, double_complex sin_W, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha_ZR, double_complex &beta_ZR, double_complex &gamma_ZR, double_complex &alpha_ZL, double_complex &beta_ZL, double_complex &gamma_ZL, double_complex &alpha_P, double_complex &beta_P, double_complex &gamma_P);
void Omega_2_W(double u, double v, double e_q, double e_qprime, double *GHPLs1, double *GHPLs2, double *GHPLs3, double *GHPLs4, double *HPLs1, double *HPLs2, double *HPLs3, double *HPLs4, double_complex &alpha_W, double_complex &beta_W, double_complex &gamma_W);



