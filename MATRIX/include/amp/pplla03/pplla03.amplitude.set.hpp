#ifndef pplla03_AMPLITUDE_SET_HPP
#define pplla03_AMPLITUDE_SET_HPP

void pplla03_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class pplla03_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  pplla03_amplitude_OpenLoops_set();
  ~pplla03_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class pplla03_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  pplla03_amplitude_Recola_set();
  ~pplla03_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
