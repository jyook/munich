void ggllll34_calculate_H1(observable_set * osi);
void ggllll34_calculate_amplitude_doublevirtual(observable_set * osi);

#ifndef TYPEDEF_ENUM
#define TYPEDEF_ENUM

typedef enum {
    V_PHOT = 0,
    V_ZLL,
    V_WMIN,
    V_WPLU,
    V_ZNN
} TypeV;

#endif

double_complex ggllll34_computeM(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<double_complex> &E);
void ggllll34_computeVVprimeHelAmplitudes(observable_set * osi, TypeV V1, TypeV V2, vector<fourvector> &p, const vector<vector<vector<double_complex> > > &E, vector<vector<vector<vector<vector<double_complex> > > > > &M_dressed);
void add_hel_amps(vector<vector<vector<vector<vector<double_complex> > > > > &M1, vector<vector<vector<vector<vector<double_complex> > > > > &M2);


class ggVVprimePrivate;

class ggVVprime {
  public:
    static ggVVprime &getInstance() {
      static ggVVprime instance;
      return instance;
    }
    ~ggVVprime();
    void initAmplitude(int Nf);
    void computeAmplitude(double s, double t, double ma2, double mb2);
    void getAmplitude(int l, int i, int j, double &Er, double &Ei);
  private:
    ggVVprime();
    ggVVprime(ggVVprime const&);
    void operator=(ggVVprime const&);
    
    ggVVprimePrivate* mData;
};
