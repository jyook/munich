void ppllll24_calculate_H2(observable_set * osi);
void ppllll24_calculate_amplitude_doublevirtual(observable_set * osi);

#ifndef TYPEDEF_ENUM
#define TYPEDEF_ENUM

typedef enum {
    V_PHOT = 0,
    V_ZLL,
    V_WMIN,
    V_WPLU,
    V_ZNN
} TypeV;

#endif

double_complex ppllll24_computeM(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<double_complex> &E);
double_complex ppllll24_computeM_FSR_7(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const double_complex F);
void ppllll24_computeE(double s, double t, double ma2, double mb2, int type, vector<double_complex> &E);
void ppllll24_computeVVprimeHelAmplitudes(observable_set * osi, TypeV V1, TypeV V2, int q1, vector<fourvector> &p, const vector<vector<vector<double_complex> > > &E, vector<vector<vector<vector<double_complex> > > > &M_dressed);
void ppllll24_computeVVprimeHelAmplitudesSR(observable_set * osi, int resonant_V, TypeV V1, TypeV V2, int q1, vector<fourvector> &p, vector<double_complex> &F_q, vector<vector<vector<vector<double_complex> > > > &M_dressed);
void add_hel_amps(vector<vector<vector<vector<double_complex> > > > &M1, vector<vector<vector<vector<double_complex> > > > &M2);

class VVprimePrivate;

class VVprime {
  public:
    static VVprime &getInstance() {
      static VVprime instance;
      return instance;
    }
    ~VVprime();
    void initAmplitude(int Nf);
    void computeAmplitude(double s, double t, double ma2, double mb2, bool debug=false);
    void getAmplitude(int l, int i, int j, double &Er, double &Ei);
  private:
    VVprime();
    VVprime(VVprime const&);
    void operator=(VVprime const&);
    
    VVprimePrivate* mData;
};
