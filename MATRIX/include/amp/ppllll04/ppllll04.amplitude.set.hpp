#ifndef ppllll04_AMPLITUDE_SET_HPP
#define ppllll04_AMPLITUDE_SET_HPP

void ppllll04_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class ppllll04_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  ppllll04_amplitude_OpenLoops_set();
  ~ppllll04_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class ppllll04_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  ppllll04_amplitude_Recola_set();
  ~ppllll04_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
