#ifndef ppww02_AMPLITUDE_SET_HPP
#define ppww02_AMPLITUDE_SET_HPP

void ppww02_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class ppww02_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  ppww02_amplitude_OpenLoops_set();
  ~ppww02_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class ppww02_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  ppww02_amplitude_Recola_set();
  ~ppww02_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
