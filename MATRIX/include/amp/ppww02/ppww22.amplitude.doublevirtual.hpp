void ppww22_calculate_H2(observable_set * osi);
void ppww22_calculate_amplitude_doublevirtual(observable_set * osi);

/*
 * WW interference terms
 *
 */

// this is the main function
/* s, t are the usual Mandelstam invariants, m2 is the squared W mass */
void ampww(double s, double t, double m2,
           double& f0_N, double& j0_N, double& k0_N,
           double& f1_CF_N, double& j1_CF_N, double& k1_CF_N,
           double& f11_CF2_N, double& j11_CF2_N, double& k11_CF2_N,
           double& f2_CF2_N, double& j2_CF2_N, double& k2_CF2_N,
           double& f2_CF, double& j2_CF, double& k2_CF,
           double& f2_CF_N_Nf, double& j2_CF_N_Nf, double& k2_CF_N_Nf,
           double& f2_CF_N_NfWW, double& j2_CF_N_NfWW);

// this is an interface to Fortran
extern "C" void ampwwfort_(double* s, double* t, double* m2,
  double* f0_N, double* j0_N, double* k0_N,
  double* f1_CF_N, double* j1_CF_N, double* k1_CF_N,
  double* f11_CF2_N, double* j11_CF2_N, double* k11_CF2_N,
  double* f2_CF2_N, double* j2_CF2_N, double* k2_CF2_N,
  double* f2_CF, double* j2_CF, double* k2_CF,
  double* f2_CF_N_Nf, double* j2_CF_N_Nf, double* k2_CF_N_Nf,
  double* f2_CF_N_NfWW, double* j2_CF_N_NfWW);

