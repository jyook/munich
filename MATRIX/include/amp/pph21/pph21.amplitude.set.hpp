#ifndef pph21_AMPLITUDE_SET_HPP
#define pph21_AMPLITUDE_SET_HPP

void pph21_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class pph21_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  pph21_amplitude_OpenLoops_set();
  ~pph21_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class pph21_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  pph21_amplitude_Recola_set();
  ~pph21_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
