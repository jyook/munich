#ifndef ppllh03_AMPLITUDE_SET_HPP
#define ppllh03_AMPLITUDE_SET_HPP

void ppllh03_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class ppllh03_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  ppllh03_amplitude_OpenLoops_set();
  ~ppllh03_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class ppllh03_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  ppllh03_amplitude_Recola_set();
  ~ppllh03_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
