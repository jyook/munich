#ifndef pph21loop_AMPLITUDE_SET_HPP
#define pph21loop_AMPLITUDE_SET_HPP

void pph21loop_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class pph21loop_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  pph21loop_amplitude_OpenLoops_set();
  ~pph21loop_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class pph21loop_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  pph21loop_amplitude_Recola_set();
  ~pph21loop_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
