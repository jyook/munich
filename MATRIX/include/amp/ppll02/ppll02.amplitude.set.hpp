#ifndef ppll02_AMPLITUDE_SET_HPP
#define ppll02_AMPLITUDE_SET_HPP

void ppll02_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class ppll02_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  ppll02_amplitude_OpenLoops_set();
  ~ppll02_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class ppll02_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  ppll02_amplitude_Recola_set();
  ~ppll02_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
