void ppzz22_calculate_H2(observable_set * oset);
void ppzz22_calculate_amplitude_doublevirtual(observable_set * oset);

void ZZ_computeAmplitudes(double sn, double un, int order, int debug, double &M0M0, double &M0M1, double &M1M1, double &M0M2a, double &M0M2b, double &M0M2c, double &M0M2d);

