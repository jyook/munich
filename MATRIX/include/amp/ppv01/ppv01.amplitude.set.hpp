#ifndef ppv01_AMPLITUDE_SET_HPP
#define ppv01_AMPLITUDE_SET_HPP

void ppv01_amplitude_initialization(munich * MUC);

using namespace std;

#ifdef OPENLOOPS

class ppv01_amplitude_OpenLoops_set : public amplitude_OpenLoops_set {
private:

public:
  ppv01_amplitude_OpenLoops_set();
  ~ppv01_amplitude_OpenLoops_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif


#ifdef RECOLA
class ppv01_amplitude_Recola_set : public amplitude_Recola_set {
private:

public:
  ppv01_amplitude_Recola_set();
  ~ppv01_amplitude_Recola_set();
  
  void calculate_H1gg_2loop();
  void calculate_H2_2loop();

};
#endif

#endif
