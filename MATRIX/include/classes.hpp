//  classes/...
#include "fourvector.h"
#include "contribution.set.hpp"
#include "user.defined.h"
#include "inputparameter.set.h"
#include "model.set.h"
#include "inputline.h"
#include "qTsubtraction.basic.h"
#include "NJsubtraction.basic.h"
#include "runresumption.set.h"
#include "logger.h"
#include "topwidth.h"

//  routines/...
#include "routines.hpp"

//  phasespace/...
#include "phasespace.hpp"
#include "random.number.generator.hpp"
#include "multichannel.set.hpp"
#include "importancesampling.set.hpp"
#include "phasespace.set.hpp"
#include "randomvariable.hpp"
#include "randommanager.hpp"

// event/...
#include "particle.hpp"
#include "define.particle.set.hpp"
#include "fiducialcut.hpp"
#include "event.set.hpp"
#include "event.hpp"

//  dipolesubtraction/...
#include "dipolesubtraction.hpp"
#include "dipole.set.hpp"
#include "ioperator.set.hpp"
#include "collinear.set.hpp"

//  qTsubtraction/...
#include "qTsubtraction.hpp"
#include "multicollinear.set.hpp"

//  observable/...
#include "xdistribution.hpp"
#include "dddistribution.hpp"
#include "observable.set.hpp"

//  amplitude/...
#include "amplitude.set.hpp"
#include "amplitude.OpenLoops.set.hpp"
#include "amplitude.Recola.set.hpp"

// summary/...
#include "summary.distribution.hpp"
#include "summary.subprocess.hpp"
#include "summary.contribution.hpp"
#include "summary.list.hpp"
#include "summary.order.hpp"
#include "summary.generic.hpp"

//  munich/...
#include "munich.hpp"

#include "const.cxx"

using namespace mathconst;
using namespace physconst;

#define setdr right << setw(25) << setprecision(16) << showpoint
#define setdl left << setw(25) << setprecision(16) << showpoint
#define setsr right << setw(15) << setprecision(8) << showpoint
#define setsl left << setw(15) << setprecision(8) << showpoint

