/*
// qTsubtraction/hpl.cpp
void compute_HPLs_weight2(double v, double *HZ1, double *HZ2, double *HZ3);
void compute_GHPLs(int weight, double u, double v, double *GYZ1, double *GYZ2, double *GYZ3, double *GYZ4, double *HZ1, double *HZ2, double *HZ3, double *HZ4);
*/

// qTsubtraction/specialfunctions.cpp
double zbesselk0( double z);
double zbesselk1( double z);
double zbesselk2( double z);
double zbesselk3( double z);
void Itilde(const double xmio, const int order, double &LL1, double &LL2, double &LL3, double &LL4);
double myli3(     double x);
double Itilde1(   double xmio, void *params);
double Itilde2(   double xmio, void *params);
double Itilde3(   double xmio, void *params);
double Itilde4(   double xmio, void *params);
void computeItildaIntegrals(double xmin, double xstep, int steps, vector<double> &I1_int, vector<double> &I2_int, vector<double> &I3_int, vector<double> &I4_int);

// qTsubtraction/auxiliaryfunctions.cpp
double C1qqdelta(double A_F);
double C2qqreg(  double z, double nf);
double C2qg(     double z);
double C2qqb(    double z, double nf);
double C2qqp(    double z, double nf);
double D0int(    double z);
double D1int(    double z);
double Pggreg(   double z);
double Pgq(      double z);
double Pqq(      double z);
double Pqg(      double z);
double Cqq(      double z);
double Cqg(      double z);
double Cgq(      double z);
double Pqqint(   double z);
double Pqqqq(    double z);
double Pqqqg(    double z);
double Pqggq(    double z);
double Pqggg(    double z, double beta0);
double P2qqV(    double x, int nf);
double S2(       double x);
double P2qqbV(   double x);
double P2qg(     double x);
double P2qqS(    double x);
double CqqPqq(   double z);
double CqqPqg(   double z);
double CqgPgq(   double z);
double CqgPgg(   double z, double beta0);
// new          
double Pgqqg (   double z, int nf);
double Pggggreg( double z, double beta0);
double Pgqqq(    double z);
double Pgggq(    double z, double beta0);
double CgqPqg(   double z, int nf);
double CgqPqq(   double z);
double Pgg(      double z);
double P2gg(     double z, int nf);
double P2gq(     double z, int nf );
double C1ggdelta(double A_F);
double C2ggreg(  double z, int nf );
double C2gq(     double z, int nf );
double Ggg(      double z );
double Ggq(      double z );

// qTsubtraction/spinorproducts.cpp
void calcSpinorProducts(vector<fourvector> &p, vector<vector<double_complex > > &za, vector<vector<double_complex > > &zb, vector<vector <double> > &s);

