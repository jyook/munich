#ifndef CONTRIBUTIONSET_H
#define CONTRIBUTIONSET_H

#include <map>
#include <string>
#include <vector>

using namespace std;

class inputparameter_set;
class dipole_set;

class contribution_set{
 private:

 public:
////////////////////
//  constructors  //
////////////////////
  contribution_set();
  virtual ~contribution_set(){}

  void initialization_complete();
  void determination_subprocess_psp(int x_a);
  void combination_subprocess_psp(int x_a);
  void determination_subprocess_dipole(int x_a);

  virtual void determination_subprocess_born(int i_a){}
  virtual void combination_subprocess_born(int i_a){}
  virtual void determination_subprocess_real(int i_a){}
  virtual void combination_subprocess_real(int i_a){}
  virtual void determination_subprocess_doublereal(int i_a){}
  virtual void combination_subprocess_doublereal(int i_a){}

  void determination_dipole_QCD();
  void determination_dipole_QEW();


  void initialization(inputparameter_set * isi);
  void initialization_set_default();
  void initialization_input(inputparameter_set * isi);
  void initialization_after_input();

  void determination_order_alpha_s_born();
  void determination_class_contribution();
  
  void readin_hadronic_process();
  void readin_subprocess();

  void readin_subprocess_from_name(string this_processname, vector<int> & this_type_parton, int temp_switch);
  void fill_code_particle();
  void fill_name_particle();

  void newscheme_readin_subprocess_from_name(string this_processname, vector<int> & this_type_parton, int temp_switch, vector<vector<string> > & particles_name);
  void newscheme_fill_code_particle();
  void newscheme_fill_name_particle();

  void oldscheme_readin_subprocess_from_name(string this_processname, vector<int> & this_type_parton, int temp_switch, vector<vector<string> > & particles_name);
  void oldscheme_fill_code_particle();
  void oldscheme_fill_name_particle();

  void fill_pdg_charge();
  void fill_charge_parton();
  void fill_mass_parton();
  
  void initialization_contribution_order();

  string path_MUNICH;
  
  int switch_to_newscheme;

  string process_class;
  string basic_process_class;
  vector<int> type_hadron;
  vector<int> basic_type_hadron;

  vector<string> decay;

  string type_perturbative_order;
  string type_contribution;
  string type_correction;
  int contribution_order_alpha_s;
  int contribution_order_alpha_e;
  int contribution_order_interference;

 
  string subprocess;
  string basic_subprocess; // probably not needed...

  vector<vector<int> > type_parton;
  vector<vector<int> > basic_type_parton;

  // new: to replace no_prc/no_map and o_prc/o_map in observable_set/phasespace_set
  // need to replace the other ones throughout !!!
  vector<int> no_process_parton; // no_prc
  vector<vector<int> > swap_parton; // o_prc

  // to be imported from osi:
  vector<vector<int> > combination_pdf;

  // to be imported from psi:
  double symmetry_factor;
  vector<double> symmetry_id_factor;

  // To be re-organised and (probably) moved here finally:
  // rename to contribution_order_... and remove the single-entry version above - or vice versa!!!
  //
  /*
  vector<int> order_contribution_alpha_s;
  vector<int> order_contribution_alpha_e;
  vector<int> order_contribution_interference;
  */  
  // to be imported from psi:
  vector<int> phasespace_order_alpha_s;
  vector<int> phasespace_order_alpha_e;
  vector<int> phasespace_order_interference;
  
  int n_particle;
  int process_type;
  int n_particle_born;
  int n_jet_born;
  int n_photon_born;
  int order_alpha_s_born;
  
  map<string,int> code_particle;
  map<int,string> name_particle;

  vector<vector<double> > charge_parton;
  vector<vector<double> > charge2_parton;
  
  vector<vector<double> > mass_parton;
  vector<vector<double> > mass2_parton;

  map<int, double> pdg_charge;
  map<int, double> pdg_charge2;

  map<int, int> lepton_exchange;
  map<int, int> lepton_exchange_inverse;

  int class_contribution_born;
  int class_contribution_CS_collinear;
  int class_contribution_CS_virtual;
  int class_contribution_CS_real;
  int class_contribution_QT_collinear;
  int class_contribution_QT_virtual;
  int class_contribution_QT_real;
  int class_contribution_QT_resummation;
  int class_contribution_NJ_collinear;
  int class_contribution_NJ_virtual;
  int class_contribution_NJ_real;
  int class_contribution_collinear;
  int class_contribution_virtual;
  int class_contribution_real;
  int class_contribution_loopinduced;
  int class_contribution_qTcut_implicit;
  int class_contribution_qTcut_explicit;
  int class_contribution_NJcut_implicit;
  int class_contribution_NJcut_explicit;
  int class_contribution_IRcut_implicit;
  int class_contribution_IRcut_explicit;
  int class_contribution_CS;
  int class_contribution_QT;
  int class_contribution_NJ;
  int class_contribution_QT_CS;
  int class_contribution_NJ_CS;

  int int_end;

  int n_ps;
  vector<dipole_set> dipole;
  //  vector<dipole_set> dipole_candidate;

  vector<vector<double> > singular_region;
  vector<vector<string> > singular_region_name;
  vector<vector<int> > singular_region_list;

  //  string run_mode;
};
#endif
