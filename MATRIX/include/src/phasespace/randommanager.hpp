#ifndef RANDOMMANAGER_H
#define RANDOMMANAGER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "logger.h"
#include "randomvariable.hpp"

//using std::vector;

class randomvariable;
class phasespace_set;

class randommanager{
public:
  randommanager();
  randommanager(phasespace_set & psi);
  ~randommanager();

  int active_optimization;
  int end_optimization;

  vector<randomvariable> random_psp;

  //  void register_variable(randomvariable* new_var);
  void initialization_random_psp(int x_r);
  double get_random(int x_a, int type, int number);

  void psp_IS_optimization(double & psp_weight, double & psp_weight2);

  // Check if both of them are required !!!
  void increase_cut_counter();
  void increase_counter_n_acc();

  void step_optimization(int events);
  void result_optimization();
  void output_optimization();
  
  //  void do_optimization_step(int events);
  void proceeding_out(std::ofstream &out_proceeding);
  void proceeding_in(int &proc, vector<std::string> &readin);
  void check_proceeding_in(int &int_end, int &proc, vector<std::string> &readin);
  //  void proceeding_save();

  void proceeding_group_by_type_out(ofstream & out_proceeding);
  void proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination);

  //  void readin_weights();
  //  void writeout_weights();

  void add_var_to_queue(randomvariable* variable);
  void nancut();

  void output_used();

  phasespace_set * psi;
  int counter_mode_12;

 private:
  vector<randomvariable * > used_queue;
  int queue_counter;
  int queue_threshold;
  vector<string> readin;
};

#endif
