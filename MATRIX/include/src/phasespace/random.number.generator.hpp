#ifndef RANDOM_NUMBER_GENERATOR_H
#define RANDOM_NUMBER_GENERATOR_H

class phasespace_set;

class random_number_generator{
private:
  int n_shift_run;
  int n_random_per_psp;
  int random_sheet;
  int n_shift_on_sheet;
  vector<double> random_generator_set;
  //  vector<double> save_random_generator_set;

 public:
  random_number_generator();
  //  random_number_generator(int _n_random_per_psp, int _n_shift_run, phasespace_set & _psi);
  random_number_generator(int _n_random_per_psp, int _n_shift_run, int _switch_external_random_generator);
  ~random_number_generator();

  //  void initialization_external(int _n_random_per_psp, phasespace_set & psi);
  //  void initialization(int _n_random_per_psp, int _n_shift_run, phasespace_set & _psi);
  void initialization(int _n_random_per_psp, int _n_shift_run, int _switch_external_random_generator);
  void initialization_manipulation(int n_manipulation, random_number_generator & rng);

  void get_random_number_set();
  void get_random_number_set_external(vector<double> & _random_number);
  void get_random_generator_set();
  vector<double> access_random_number(int n_requested_random_number);
  double access_random_number();
  string output_random_generator_set();
  string output_random_number_set();

  void proceeding_out(ofstream &out_proceeding);
  void proceeding_in(int &proc, vector<string> &readin);
  void check_proceeding_in(int &int_end, int &temp_check_size, vector<string> &readin);
  //  void proceeding_save();

  phasespace_set * psi;
  vector<double> random_number;
  // maybe needed only in phasespace_set ???
  int random_counter;

  // needed ???
  int switch_external_random_generator;
};

#endif
