#ifndef IMPORTANCESAMPLING_SET_H
#define IMPORTANCESAMPLING_SET_H

class phasespace_set;

class importancesampling_input {
private:

public:
  // constant over full importancesampling
  string name;
  int switch_optimization;

  int n_gridsize;
  int n_optimization_step;
  int n_event_per_step;

  int threshold_optimization;
  int switch_mode_optimization;
  int switch_validation_optimization;
  int switch_minimum_weight;

  double weight_adaptation_exponent;
  double reserved_minimum_weight;
  double alpha_maximum_weight;

  int switch_smearing_level;

  // feature not implemented yet:
  int switch_increase_n_event_per_step;

  // to add extra optimization steps after certain other steps:
  int n_optimization_step_extra; 

  importancesampling_input();

  friend std::ostream & operator << ( std::ostream &, const importancesampling_input & IS);
};


class importancesampling_set : public importancesampling_input {
private:

public:
  // identical to importancesampling_input -> should acually work via inheritance ???
  /*
  string name;
  int switch_optimization;

  int n_gridsize;
  int n_optimization_step;
  int n_event_per_step;

  int threshold_optimization;
  int switch_mode_optimization;
  int switch_validation_optimization;
  int switch_minimum_weight;

  double weight_adaptation_exponent;
  double reserved_minimum_weight;
  double alpha_maximum_weight;

  int switch_smearing_level;
  */

  bool is_gridsize_power_of_two;
  
  string filename;
  string filename_readin;

  //  double limit_minimum_weight;

  // used only inside optimization routines
  double alpha_reserved_min;
  
  // changes for each phase-space point
  int channel;
  vector<double> g_channel;
  vector<double> g_IS_channel;

  // changes at optimization steps
  vector<double> alpha;
  vector<double> beta;

  // needed for optimization phase
  int active_optimization;
  int end_optimization;

  int n_acc;
  int n_rej;
  vector<int> n_acc_channel;
  vector<int> n_rej_channel;

  //  vector<double> w_channel_sum;
  vector<double> w_channel_av;

  vector<double> sum_channel_weight;
  vector<double> sum_channel_weight2;

  // imported from multichannel_set:
  int counter_minimum_weight;
  vector<int> no_channel_minimum_weight;

  int i_alpha_it;
  vector<vector<double> > alpha_it;
  vector<double> diff_w;
  int x_minimum_diff_w;
  int x_alpha_it_min;

  vector<double> c_w_channel_sum;
  double c_w_norm;
  
  importancesampling_set();
  importancesampling_set(const importancesampling_input & input, string _filename, string _filename_readin);

  void vegasgrid_average();
  void psp_IS_optimization(double & this_psp_weight, double & this_psp_weight2);
  void step_IS_optimization(int i_step_mode);
  void result_IS_optimization(int i_step_mode);
  void output_IS_optimization(int i_step_mode);
  void readin_IS_optimization();

  void output_optimization();
  void readin_optimization();

  void proceeding_out(ofstream & out_proceeding);
  void proceeding_in(int & proc, vector<string> & readin);
  void proceeding_group_by_type_out(ofstream & out_proceeding);
  void proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination);

  friend std::ostream & operator << ( std::ostream &, const importancesampling_set & IS);
  int operator == (const importancesampling_set & IS) const;
};
#endif
