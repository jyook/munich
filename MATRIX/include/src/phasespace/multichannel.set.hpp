#ifndef MULTICHANNEL_SET_H
#define MULTICHANNEL_SET_H

class phasespace_set;

class multichannel_input {
private:

public:
  // constant over full input_optimization
  string name;
  int switch_optimization;
  
  int n_channel;
  int n_optimization_step; // -> replaces  n_alpha_steps;
  int n_event_per_step; // -> n_alpha_events;

  int threshold_optimization;
  int switch_mode_optimization;
  int switch_validation_optimization; // always treated as if set to 1 !!!
  int switch_minimum_weight;

  double weight_adaptation_exponent;
  double reserved_minimum_weight;
  double alpha_maximum_weight;

  // feature not implemented yet:
  int switch_increase_n_event_per_step;

  // to add extra optimization steps after certain other steps:
  int n_optimization_step_extra;

  multichannel_input();

  friend std::ostream & operator << ( std::ostream &, const multichannel_input & MC);
};


class multichannel_set : public multichannel_input {
private:

public:
  // identical to multichannel_input -> should acually work via inheritance ???
  /*
  string name;
  int switch_optimization;

  int n_channel;
  int n_alpha_steps;
  int n_alpha_events;

  int threshold_optimization;
  int switch_mode_optimization;
  int switch_validation_optimization;
  int switch_minimum_weight;
  
  double weight_adaptation_exponent;
  double reserved_minimum_weight;
  double alpha_maximum_weight;
  */
  
  //  int n_alpha_steps;
  //  int n_alpha_events;

  string filename;
  string filename_readin;

  // used only inside optimization routines
  double alpha_reserved_min;

  // changes for each phase-space point
  int channel;
  vector<double> g_channel;
  vector<double> g_IS_channel;

  // changes at optimization steps
  vector<double> alpha;
  vector<double> beta;

  // needed for optimization phase
  int active_optimization;
  int end_optimization;

  int n_acc;
  int n_rej;
  vector<int> n_acc_channel;
  vector<int> n_rej_channel;
  vector<double> w_channel_sum;
  vector<double> w_channel_av;

  // could be private
  int counter_minimum_weight;
  vector<int> no_channel_minimum_weight;

  int i_alpha_it;
  vector<vector<double> > alpha_it;
  vector<double> diff_w; // -> MC_diff_w_step
  int x_minimum_diff_w;
  int x_alpha_it_min;
  
  vector<double> c_w_channel_sum;
  double c_w_norm;
  
  multichannel_set();
  multichannel_set(const multichannel_input & input, string _filename, string _filename_readin);

  void psp_MCweight_optimization(double & integrand, double & g_tot);
  void step_MCweight_optimization(int i_acc);
  void result_MCweight_optimization(int i_acc);
  void output_MCweight_optimization(int i_step_mode, string & filename_MCweight);
  void readin_MCweight_optimization();
  
  void output_optimization();
  void readin_optimization();
  
  void proceeding_out(ofstream & out_proceeding);
  void proceeding_in(int & proc, vector<string> & readin);
  void proceeding_group_by_type_out(ofstream & out_proceeding);
  void proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination);

  friend std::ostream & operator << ( std::ostream &, const multichannel_set & MC);
  int operator == (const multichannel_set & MC) const;
};
#endif
