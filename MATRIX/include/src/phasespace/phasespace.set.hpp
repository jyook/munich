#ifndef PHASESPACESET_H
#define PHASESPACESET_H

#include <string>
#include <vector>

#include "randommanager.hpp"
#include "logger.h"
#include "multichannel.set.hpp"
#include "importancesampling.set.hpp"

using namespace std;

class phasespace_set{
private:

public:
////////////////////
//  constructors  //
////////////////////
  phasespace_set();
  virtual ~phasespace_set();

  void optimize_minv();

  int determination_MCchannels(int x_a);
  void ac_tau_psp(int x_a, vector<int> & tau_MC_map);
  void ax_psp(int x_a);
  void ac_psp(int x_a, int channel);
  void ag_psp(int x_a, int zero);

  int determination_MCchannels_dipole(int x_a);
  void ac_tau_psp_dipole(int x_a, vector<int> & tau_MC_map);
  void ax_psp_dipole(int x_a);
  void ac_psp_dipole(int x_a, int channel);
  void ag_psp_dipole(int x_a, int zero);

  void ac_tau_psp_doubledipole(int x_a, vector<int> & tau_MC_map);

  virtual void optimize_minv_born(){}
  virtual int determination_MCchannels_born(int x_a){return 0;}
  virtual void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){}
  virtual void ax_psp_born(int x_a){}
  virtual void ac_psp_born(int x_a, int channel){}
  virtual void ag_psp_born(int x_a, int zero){}

  virtual void optimize_minv_real(){}
  virtual int determination_MCchannels_real(int x_a){return 0;}
  virtual void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){}
  virtual void ax_psp_real(int x_a){}
  virtual void ac_psp_real(int x_a, int channel){}
  virtual void ag_psp_real(int x_a, int zero){}

  virtual void optimize_minv_doublereal(){}
  virtual int determination_MCchannels_doublereal(int x_a){return 0;}
  virtual void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){}
  virtual void ax_psp_doublereal(int x_a){}
  virtual void ac_psp_doublereal(int x_a, int channel){}
  virtual void ag_psp_doublereal(int x_a, int zero){}

  // To create pseudo-dipoles if needed:
  phasespace_set * fake_psi;

  // phasespace.initialization.cpp
  void initialization(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, event_set * _esi, user_defined * _user);
  void initialization_set_default();
  void initialization_input(inputparameter_set * isi);
  void initialization_after_input();

  // phasespace.initialization.run.cpp
  // Shift completely to csi ??? Not completely, but remainder should be moved elsewhere !!!
  void initialization_contribution_order();
  void initialization_complete();
  void initialization_phasespace_optimization_fiducialcut();
  // Re-organize the connection of dipole !!!
  void initialization_complete_RA();
  void initialization_optimization();
  void initialization_optimization_grid();
  void initialization_filename();
  // It makes sense to have an independent set of masses and widths only for phase space generation !!!
  void initialization_masses();
  // Most of these functions don't really belong here -> should be grouped in a more meaningful way !!!
  void initialization_MC_IS_name_type();
  void initialization_MC_IS_name_list();

  void initialization_phasespace_born();
  void initialization_phasespace_RA();

  void initialization_phasespace_subprocess();
  void initialization_phasespace_subprocess_optimization();
  void initialization_phasespace_subprocess_dipole_IS_RA();
  void initialization_phasespace_subprocess_dipole_RA();
  void initialization_phasespace_subprocess_optimization_dipole_RA();

  void initialization_phasespace_MC_tau();
  void initialization_phasespace_MC_x_dipole_RA();

  void initialization_phasespace_IS();
  void initialization_phasespace_IS_CA();
  void initialization_phasespace_IS_QT();

  void initialization_MC_rng();
  void initialization_MC();

  void initialization_fake_dipole_mapping_RT();
  void initialization_fake_dipole_mapping_RRA();

  // phasespace.initialization.initial.cpp
  void initialization_minimum_tau();
  void initialization_minimum_phasespacecut();

  ////////////////////////////////////////



  //  dipolesubtraction/phasespace.determination.dipoles.cpp
  void determine_dipole_phasespace_RA();
  void phasespace_ij_k(int x_a);
  void phasespace_ij_a(int x_a);
  void phasespace_ai_k(int x_a);
  void phasespace_ai_b(int x_a);
  void phasespace_ij_k_massive(int x_a);
  void phasespace_ij_a_massive(int x_a);

  //  dipolesubtraction/phasespace.generation.dipoles.cpp
  void ac_psp_RA_dipoles();
  void ag_psp_RA_dipoles();

  //  dipolesubtraction/phasespace.generation.dipoles.CS.cpp
  void ac_psp_RA_group_ij_k(int x_a);
  void ag_psp_RA_group_ij_k(int x_a);
  void ac_psp_RA_group_ij_a(double & sinx_ij_a_min, int x_a);
  void ag_psp_RA_group_ij_a(double & sinx_ij_a_min, int x_a);
  void ac_psp_RA_group_ai_k(double & sinx_ik_a_min, int x_a);
  void ag_psp_RA_group_ai_k(double & sinx_ik_a_min, int x_a);
  void ac_psp_RA_group_ai_b(double & sinx_i_ab_min, int x_a);
  void ag_psp_RA_group_ai_b(double & sinx_i_ab_min, int x_a);

  //  dipolesubtraction/phasespace.generation.dipoles.CDST.cpp
  void ac_psp_RA_group_ij_k_massive(int x_a);
  void ag_psp_RA_group_ij_k_massive(int x_a);
  void ac_psp_RA_group_ij_a_massive(double & sinx_ij_a_min, int x_a);
  void ag_psp_RA_group_ij_a_massive(double & sinx_ij_a_min, int x_a);
  void ac_psp_RA_group_ai_k_massive(double & sinx_ik_a_min, int x_a);
  void ag_psp_RA_group_ai_k_massive(double & sinx_ik_a_min, int x_a);



  void correct_phasespacepoint_real();
  void correct_phasespacepoint_dipole();


  void calculate_g_tot();

  void calculate_IS();
  void calculate_IS_RA();
  void calculate_IS_tau_x1x2_MC();
  void calculate_initial_tau_IS_x1x2_IS();
  void calculate_initial_tau_IS_x1x2();
  void calculate_initial_tau_x1x2_IS();
  void calculate_initial_tau_x1x2();
  void calculate_initial_tau_fixed_x1x2_IS();
  void calculate_initial_tau_fixed_x1x2();

  void calculate_initial_collinear_z1z2_IS();

  void calculate_IS_CT();
  void calculate_IS_VT();

  void calculate_IS_QT_from_CX();
  void calculate_IS_CA_from_CT();
  void calculate_IS_CX_from_CT();
  void calculate_IS_CX_from_QT();

  void calculate_IS_CX();
  void calculate_IS_QT();


  double c_phi(double r1);
  //  double c_phi(double & r1);
  //  double c_phi(int i_r);
  double inv_phi(double & phi);

  double c_costheta(double & r1);
  //  double c_costheta(int i_r);
  double inv_costheta(double & costheta);

  void c_propagator(int i_a, int m, int out, double & smin, double & smax, double r1);
  //  void c_propagator(int i_a, int m, int out, double smin, double smax, int i_r1);
  double g_propagator(int i_a, int m, int out, double smin, double smax);
  void inv_propagator(int i_a, int m, int out, double smin, double smax, double & r1);

  double c_propagator_Breit_Wigner(double & r1, int m, double & smin, double & smax);
  double g_propagator_Breit_Wigner(double & s, int m, double & smin, double & smax);
  double inv_propagator_Breit_Wigner(double s, int m, double & smin, double & smax);

  double c_propagator_narrow_width();
  double g_propagator_narrow_width(int m);
  double inv_propagator_narrow_width();

  double c_propagator_vanishing_width(double & r, double & mass2, double & smin, double & smax, double & nu);
  //  double c_propagator_vanishing_width(double r, double mass2, double smin, double smax, double nu);
  double g_propagator_vanishing_width(double s, double mass2, double smin, double smax, double nu);
  double inv_propagator_vanishing_width(double s, double mass2, double smin, double smax, double nu);

  void c_timelikeinvariant(int i_a, int out, double & smin, double & smax, double r1);
  //  void c_timelikeinvariant(int i_a, int out, double smin, double smax, int i_r1);
  double g_timelikeinvariant(double smin, double smax);
  void inv_timelikeinvariant(int i_a, int out, double smin, double smax, double & r1);

  double c_t_propagator(double & r1, int m, double smin, double smax);
  //  double c_t_propagator(double r, int m, double smin, double smax);
  double g_t_propagator(double s, int m, double smin, double smax);
  double inv_t_propagator(double s, int m, double smin, double smax);

  void c_decay(int i_a, int in, int out1, int out2, double r1, double r2);
  //  void c_decay(int i_a, int in, int out1, int out2, int i_r1, int i_r2);
  double g_decay(int i_a, int in, int out1, int out2);
  void inv_decay(int i_a, int in, int out1, int out2, double & r1, double & r2);

  void c_tchannel(int i_a, int m, int in, int in1, int in2, int out1, int out2, double r1, double r2);
  //  void c_tchannel(int i_a, int m, int in, int in1, int in2, int out1, int out2, int i_r1, int i_r2);
  double g_tchannel(int i_a, int m, int in, int in1, int in2, int out1, int out2);
  void inv_tchannel(int i_a, int m, int in, int in1, int in2, int out1, int out2, double & r1, double & r2);

  void c_tchannel_opt(int i_a, int m, int out, int in1, int in2, int out1, int out2, double r1, double r2);
  //  void c_tchannel_opt(int i_a, int m, int out, int in1, int in2, int out1, int out2, int i_r1, int i_r2);
  double g_tchannel_opt(int i_a, int m, int out, int in1, int in2, int out1, int out2);
  void inv_tchannel_opt(int i_a, int m, int out, int in1, int in2, int out1, int out2, double & r1, double & r2);


  //  void psp_MCweight_optimization(double & integrand, double & this_psp_weight, double & this_psp_weight2, double & optimization_modifier);
  //  void psp_MCweight_optimization_CA(double & integrand, double & this_psp_weight, double & this_psp_weight2);

  //  void step_MCweight_optimization();
  //  void result_MCweight_optimization();
  void weight_output_MCweight_optimization();

  //  void step_tau_weight_optimization();
  //  void step_x1x2_weight_optimization();
  //  void step_z1z2_weight_optimization();

  void psp_optimization_complete(double & integrand, double & this_psp_weight, double & this_psp_weight2, double & optimization_modifier);
  void step_optimization_complete();
  void result_optimization_complete();
  void output_optimization_complete();

  void output_check_tau_0();

  void handling_cut_psp();
  void handling_techcut_psp();




  //  double get_random(int x_a, int x_t, int x_i);


  //  call_generic * generic;

  // These entries could be shifted to contribution_set ???

  /*
  vector<int> no_map;
  vector<vector<int> > o_map;
  // most likely actually not needed:
  vector<int> no_prc;
  vector<vector<int> > o_prc;
  // ... until here.
  */


  string dir_MCweights;

  randommanager random_manager;
  vector<randomvariable*> random_psp;
  ///  vector<randomvariable*> phasespace_randoms;

  vector<randomvariable*> QT_random_z;
  randomvariable* QT_random_z1;
  randomvariable* QT_random_z2;
  randomvariable* QT_random_qt;

  vector<double> sran;


  // random numbers could become part of multichannel/importancesampling sets !!!
  vector<double> r;

  vector<double> random_MC;
  double n_random_MC;
  vector<double> random_MC_tau;
  double n_random_MC_tau;
  vector<double> random_tau;
  double n_random_tau;
  vector<double> random_x12;
  double n_random_x12;
  vector<double> random_qTres;
  double n_random_qTres;
  vector<vector<double> > random_z;
  double n_random_z;

  Logger *logger; // ???


  /*//
  int switch_MC;
  int switch_MC_tau;
  int switch_MC_x_dipole;

  int switch_IS_MC;
  int switch_IS_tau;
  int switch_IS_x1x2;
  int switch_IS_z1z2;

  int switch_IS_qTres;
  int n_qTres_steps;
  int n_qTres_events;
  int n_qTres_bins;
  */
  
  int switch_n_events_opt;

  int switch_qTcut;


  // MC parameters fixed for the whole run, maybe up to modifications depending on optimization procedure

  //  vector<vector<int> > container_IS_startvalue;
  //  vector<string> container_IS_name; // MC_IS_name_list
  //  vector<int> container_IS_switch;

  vector<vector<vector<string> > > MC_IS_name_type;
  vector<string> MC_IS_name_list;
  map <int, vector<int> > map_list_to_type_MC_IS;
  map <vector<int>, int > map_type_to_list_MC_IS;

  vector<vector<vector<int> > > c_p;
  vector<vector<vector<int> > > c_f;
  vector<vector<vector<int> > > c_t;
  vector<vector<vector<int> > > c_d;


  vector<vector<double> > v_smin;
  vector<vector<double> > v_smax;

  vector<vector<double> > g_p;
  vector<vector<double> > g_f;
  vector<vector<double> > g_t;
  vector<vector<double> > g_d;

  vector<vector<vector<int> > > needed_v_smin;
  vector<vector<vector<int> > > needed_v_smax;

  vector<vector<vector<int> > > needed_g_p;
  vector<vector<vector<int> > > needed_g_f;
  vector<vector<vector<int> > > needed_g_t;
  vector<vector<vector<int> > > needed_g_d;


  // variables changing at each phase-space point

  vector<vector<fourvector> > xbp_all;
  vector<vector<double> > xbs_all;
  vector<vector<double> > xbsqrts_all;



  // MC parameters fixed for the whole run
  /*//
  int n_alpha_steps;
  int n_alpha_events;
  int n_alpha_epc;

  int switch_mode_optimization;
  int switch_validation_optimization;

  double weight_adaptation_exponent;
  double alpha_maximum_weight;

  int switch_minimum_weight_MC;
  double reserved_minimum_weight_MC;
  int n_MC_alpha_steps;
  int n_MC_alpha_events;
  int n_MC_alpha_epc;

  int switch_minimum_weight_MC_tau;
  double reserved_minimum_weight_MC_tau;
  //  double reserved_minimum_weight_tau;
  int n_MC_tau_alpha_steps;
  int n_MC_tau_alpha_events;
  int n_MC_tau_alpha_epc;

  int switch_mode_optimization_IS;
  int switch_validation_optimization_IS;
  int switch_minimum_weight_IS;
  int switch_smearing_level_IS;

  double reserved_minimum_weight_IS;
  double alpha_maximum_weight_IS;
  double weight_adaptation_exponent_IS;

  int n_tau_steps;
  int n_tau_events;
  int n_tau_bins;

  int n_x1x2_steps;
  int n_x1x2_events;
  int n_x1x2_bins;

  int n_z1z2_steps;
  int n_z1z2_events;
  int n_z1z2_bins;

  int n_IS_events;
  int n_IS_events_factor;
  int n_IS_steps;
  int n_IS_gridsize;

  vector<int> n_IS_gridsize_all;

  int n_IS_gridsize_p;
  int n_IS_gridsize_f;
  int n_IS_gridsize_t_t;
  int n_IS_gridsize_t_phi;
  int n_IS_gridsize_d_cth;
  int n_IS_gridsize_d_phi;
  int n_IS_gridsize_xy;
  int n_IS_gridsize_zuv;
  int n_IS_gridsize_phi;
*/
  int n_events_max;
  int n_events_min;
  int n_step;

  int n_events_MC_opt;
  int opt_n_events_min;

  int xb_max;
  int xb_max_dipoles;
  int xb_out_dipoles;
  int no_random;

  vector<int> no_random_dipole;

  vector<vector<fourvector> > start_xbp_all;
  vector<vector<double> > start_xbs_all;
  vector<vector<double> > start_xbsqrts_all;

  vector<vector<fourvector> > corrected_xbp_all;
  vector<vector<double> > corrected_xbs_all;
  vector<vector<double> > corrected_xbsqrts_all;


  int n_dipoles;
  vector<double> dipole_sinx_min;
  vector<double> dipole_x;

  double s_had;
  double tau_0_s_had;

  //  double nu;
  double nuxs;
  double nuxt;
  double exp_pdf;
  //  double exp_pT;
  //  double exp_y;
  double exp_ij_k_y;
  double exp_ij_k_z;
  double exp_ij_a_x;
  double exp_ij_a_z;
  double exp_ai_k_x;
  double exp_ai_k_u;
  double exp_ai_b_x;
  double exp_ai_b_v;
  double map_technical_r;
  double map_technical_s;
  double map_technical_t;
  double map_technical_x;
  double mass0;

  //  double cut2_pT;
  //  double cut_pT_lep;

  vector<vector<double> > smin_opt;
  vector<vector<double> > sqrtsmin_opt;

  double E;
  double tau_0;
  int coll_choice;
  double min_qTcut;

  vector<double> mapping_cut_pT;

  double g_onshell_decay;

  vector<double> tau_0_num;

  // MC parameters modified at each optimization step

  /////  int i_alpha_it;
  /////  vector<vector<double> > alpha_it;
  /////  vector<double> diff_w; // -> MC_diff_w_step
  /////  int Dmin;
  int MCweight_lc_min;
  ///  double a_reserved_min;
  int switch_use_alpha_after_IS;
  int switch_step_mode_grid;
  int x_alpha_it_min;


  // model parameters, fixed for the whole run
  // could always be taken from model_set msi ???
  vector<double> M;
  vector<double> M2;
  vector<double> Gamma;
  vector<double_complex> cM2;
  vector<double> map_Gamma;
  vector<double> reg_Gamma;



  // input/output filenames, fixed for the whole run
  // could be replaced by a single file ???
  string filename_weight_readin;
  string filename_weight_writeout;
  /* // replaced by one single file:
  // to be removed when switch to single-file mode !!!  
  string filename_MCweight;
  string filename_IS_MCweight;
  string filename_tauweight;
  string filename_x1x2weight;
  vector<string> filename_z1z2weight;
  string filename_qTresweight;

  string filename_MCweight_in_contribution;
  string filename_IS_MCweight_in_contribution;
  string filename_tauweight_in_contribution;
  string filename_x1x2weight_in_contribution;
  vector<string> filename_z1z2weight_in_contribution;
  string filename_qTresweight_in_contribution;
  */


  // MC optimization parameters

  //  int weight_opt;
  int switch_IS_mode_phasespace;
  //  string weight_in_contribution;
  string weight_in_directory;
  //  int weight_min;
  //  double weight_limit_min;
  //  double weight_limit_max;



  // MC parameters changing on runtime (not used so far !!!)

  //  vector<double> *r;
  int MC_optswitch;


  // number of channels within respective dipole
  vector<int> MC_n_channel_phasespace;
  // number of channels summed up to respective dipole
  vector<int> MC_sum_channel_phasespace;
  // number of channels in total
  int MC_n_channel;
  // selected channel within respective dipole (referring to MC_n_channel_phasespace[x_a])
  int MC_channel_phasespace;
  // selected channel in total (referring to MC_n_channel)
  int MC_channel;

  double hcf;
  double ps_factor;



  double g_tot;
  double g_global_NWA;

  double g_MC;
  double g_pdf;
  double g_tau;
  double g_x1x2;

  double g_qTres;

  double MC_g_IS_global;

  /////  vector<double> temp_sum_w_channel;
  /////  vector<double> sum_w_channel;
  //  vector<double> MC_sum_w_channel;

  /////  vector<double> w_channel_av;

  /////  vector<double> MC_alpha;
  /////  vector<double> MC_beta;

  /////  vector<double> MC_g_channel;
  //  vector<double> MC_g_IS_channel;

  //  vector<int> cuts_channel;
  //  vector<int> MC_n_rej_channel;

  //  vector<int> counts_channel;
  //  vector<int> MC_n_acc_channel;

  int active_optimization;
  int end_optimization;

  int MC_opt_end;
  int IS_opt_end;
  int n_tau_MC_channel;
  vector<int> tau_MC_map;
  vector<vector<double> > tau_MC_tau_gamma;

  vector<multichannel_input * > input_MC;
  
  multichannel_input input_MC_phasespace;
  multichannel_set MC_phasespace;

  // IS-multichannelling with optimization
  multichannel_input input_MC_tau;
  multichannel_set MC_tau;

  // IS-multichannelling inside dipole mappings with optimization
  multichannel_input input_MC_x_dipole;
  
  vector<vector<int> > MC_x_dipole_mapping;
  vector<multichannel_set> MC_x_dipole;

  vector<importancesampling_input * > input_IS;

  importancesampling_input input_IS_tau;
  importancesampling_set IS_tau;

  importancesampling_input input_IS_x1x2;
  importancesampling_set IS_x1x2;

  importancesampling_input input_IS_z1z2;
  vector<importancesampling_set> IS_z1z2;

  importancesampling_input input_IS_qTres;
  importancesampling_set IS_qTres;

  importancesampling_input input_IS_PS_p;
  importancesampling_input input_IS_PS_f;
  importancesampling_input input_IS_PS_d_phi;
  importancesampling_input input_IS_PS_d_costheta;
  importancesampling_input input_IS_PS_t_t;
  importancesampling_input input_IS_PS_t_phi;
  importancesampling_input input_IS_PS_dipole_xy;
  importancesampling_input input_IS_PS_dipole_uvz;
  importancesampling_input input_IS_PS_dipole_phi;

  int z1z2_opt_end;



  // Should maybe be copied/shifted to esi (as possibly also z_coll, etc.):
  double boost;
  vector<double> x_pdf;

  // SK: Is that one used ??? clarify !!!
  vector<vector<double> > xz_pdf;

  vector<double> z_coll;
  vector<vector<double> > all_xz_coll_pdf;
  vector<double> g_z_coll;

  // QT definitions - should later be removed and unified with the ones used in CA !!!
  double QT_qt2;
  //  double QT_qt;
  //  double QT_y;
  //  double QT_Q;
  double QT_jacqt2;

  int switch_resummation;
  int dynamical_Qres;
  double Qres;
  double Qres_prefactor;
  // Check how the resummation-related variables are connected !!!
  int switch_dynamic_Qres;


  ///  bool do_resummation;
  double lambda_qt2; // for generation of qT in a resummed computation

  // SK: QT_random_qt2 needed any longer ???
  double QT_random_qt2;
  // careful: contains z1,z2, NOT x1/z1,x2/z2
  vector<double> QT_zx_pdf;
  vector<double> QT_xz_pdf;
  // SK: zx_pdf and xz_pdf not used by me: we need to unify the notation here !!!
  //  vector<double> zx_pdf;
  //  vector<double> xz_pdf;
  //
  vector<double> zz_pdf;
  vector<double> z_pdf;
  vector<vector<int> > zall_pdf;
  double QT_g_z1;
  double QT_g_z2;
  double QT_eps;
  double QT_random_IS;
  double QT_g_IS_;
  //



  long long i_gen;
  long long i_rej;
  long long i_acc;
  long long i_nan;
  long long i_tec;

  long long last_step_mode;
  long long *i_step_mode;

  int random_seed;

  /*//csi//
  string process_class;
  string subprocess;
  string type_perturbative_order;
  string type_contribution;
  string type_correction;
  */
  /*
  vector<int> contribution_order_alpha_s;
  vector<int> contribution_order_alpha_e;
  vector<int> contribution_order_interference;
  */
  /*
  vector<int> phasespace_order_alpha_s;
  vector<int> phasespace_order_alpha_e;
  vector<int> phasespace_order_interference;
  */

  /*
  vector<vector<double> > RA_singular_region;
  vector<vector<string> > RA_singular_region_name;
  vector<vector<int> > RA_singular_region_list;
  */

  int RA_techcut;
  int RA_x_a;

  double cut_technical;

  //  double symmetry_factor;

  // needed ? Maybe only once when setting parameters ???
  contribution_set *csi;
  model_set *msi;
  event_set *esi;
  user_defined *user;

  string run_mode;

  //  int switch_console_output_tau_0;

  int switch_console_output_phasespace_issue;

  int switch_off_RS_mapping;
  int switch_off_RS_mapping_ij_k;
  int switch_off_RS_mapping_ij_a;
  int switch_off_RS_mapping_ai_k;
  int switch_off_RS_mapping_ai_b;

  //  vector<dipole_set> * RA_dipole;

  int n_random_per_psp;
  int switch_off_random_generator;
  int switch_IO_generator;

  int switch_output_weights;
  
  random_number_generator rng;
  ///  vector<random_number_generator> rng_IS;

  int n_shift_run;


  /*
  vector<double> *random_number;
  int *random_counter;

  // only to mimick behaviour of old implementation via randommananger !!!
  vector<double> *random_number_;
  int *random_counter_2;
  */
};

#endif
