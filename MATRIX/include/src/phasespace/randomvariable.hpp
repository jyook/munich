#ifndef RANDOMVARIABLE_H
#define RANDOMVARIABLE_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "logger.h"
#include "randommanager.hpp"
#include "importancesampling.set.hpp"

//using std::vector;

class randommanager;

class randomvariable {
public:
  randomvariable();
  //  randomvariable(int _n_events, int _switch_IS_MC, int _n_steps, int _n_bins, const string & _name);
  //  randomvariable(const importancesampling_input & input, const string & _name);
  randomvariable(importancesampling_input input, string name, randommanager * _manager);
  
  //  void initialization();

  //  void vegasgrid_average();
  void psp_IS_optimization(double & psp_weight, double & psp_weight2);
  void step_IS_optimization(int events);
  void result_IS_optimization(int i_step_mode);

  double get_random();
  void get_g_IS(double r, double & g_IS);
  ///  void multiply_g_IS(double r, double & g_IS);

  void proceeding_out(std::ofstream & out_proceeding);
  void proceeding_in(int & proc, vector<string> & readin);
  void check_proceeding_in(int & int_end, int & proc, vector<string> & readin);
  //  void proceeding_save();

  void proceeding_group_by_type_out(ofstream & out_proceeding);
  void proceeding_group_by_type_in(int & proc, vector<string> & readin, bool switch_combination);

  void save_weights(std::ofstream & out_weights);
  void readin_weights(vector<string> & readin);

  friend std::ostream & operator << ( std::ostream &, const randomvariable & IS);

  //  int n_power_of_two;
  
  bool used;
  bool imp_sampling;
  importancesampling_set IS_PS;
  randommanager * manager;
  int number;

 private:
  //  void init(int _n_events, int _switch_IS_MC, int _n_steps, int _n_bins, const std::string & _name);
  void get_random(double &x, double &g_IS, vector<double> &s);

};

#endif
