#ifndef QTSUBTRACTIONSET_H
#define QTSUBTRACTIONSET_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <vector>
/*
#include "randommanager.h"
#include "logger.h"
#include "ftypes.h"
//include "../../include/ftypes.h"
#include "fourvector.h"
#include "particle.h"
#include "dddistribution.h"
#include "xdistribution.h"
#include "dipole.set.h"
#include "collinear.set.h"
#include "ioperator.set.h"
#include "phasespace.set.h"
#include "../../NJsubtraction/NJsubtraction.h"
*/
using namespace std;

class munich;

class qTsubtraction_set{
 private:

 public:
////////////////////
//  constructors  //
////////////////////
  qTsubtraction_set();


  void initialization_QT();
  void initialization_QT_coefficients();
  void initialization_specific_VT();
  void initialization_specific_QT();
  void initialization_specific_CT(phasespace_set & psi);

  void event_selection_qTcut(int x_a);

  void determine_splitting_tH1F(phasespace_set & psi);
  void determine_splitting_tH1F_QEW(phasespace_set & psi);
  void determine_splitting_tH1F_tH1(phasespace_set & psi);
  void determine_splitting_tH1F_tH1_QEW(phasespace_set & psi);
  void determine_splitting_tH1F_tH1_without_H1_delta(phasespace_set & psi);
  void determine_splitting_tH1_only_H1_delta(phasespace_set & psi);
  void determine_splitting_tgaga_tcga_tgamma2(phasespace_set & psi);
  void determine_splitting_tH2(phasespace_set & psi);


  void determine_splitting_tH1F(vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);
  void determine_splitting_tH1F_tH1(vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);
  void determine_splitting_tH1F_tH1_scale_dependent(double & H1_delta, vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);
  void determine_splitting_tgaga_tcga_tgamma2(double & A_F, vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);
  void determine_splitting_tgaga_tcga_tgamma2_scale_dependent(double & A_F, vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);
  void determine_splitting_tH2(vector<vector<double> > & sum_coll, vector<vector<vector<double> > > & coll, phasespace_set & psi);

  void calculate_A_F();
  void calculate_B2_A_F();

  void calculate_QT_CM_kinematics();
  
  void calculate_Ft1born();
  void calculate_Gamma1(); // to be replaced...
  void calculate_Gamma1born();
  void calculate_Gamma1loop();

  void calculate_Ft1born_QEW();
  void calculate_Gamma1born_QEW();
  
  void calculate_Gamma2();
  void calculate_commutator_Gamma1_Ft1();
  void calculate_anticommutator_Gamma1_Ft1();
  void calculate_Gamma1_Ft1();
  void calculate_Ft1_Gamma1();
  void calculate_Gamma1_squared();

  double calculate_sigma12(double & pdf_factor_x1x2);
  double calculate_sigma11(double & pdf_factor_x1x2, double & tH1F, double & LQ);
  double calculate_sigma24(double & pdf_factor_x1x2);
  double calculate_sigma23(double & pdf_factor_x1x2, double & sig11);
  double calculate_sigma22(double & pdf_factor_x1x2, double & sig11, double & tH1, double & tH1F, double & H1full, double & tgaga, double & LR, double & LF, double & LQ);

  double calculate_sigma21(double & pdf_factor_x1x2, double & sig11, double & tH1, double & tH1F, double & H1full, double & tgaga, double & tcga, double & tgamma2, double & LR, double & LF, double & LQ, double & A_F);
  double calculate_sigma21(double & pdf_factor_x1x2, double & sig11, double & tH1_only_H1_delta, double & tH1, double & tH1F, double & H1full, double & tgaga, double & tcga, double & tgamma2, double & LR, double & LF, double & LQ, double & A_F);

  double calculate_H1(double pdf_factor_x1x2, double tH1, double tH1F, double LR, double LF, double LQ);
  double calculate_H1(double pdf_factor_x1x2, double tH1_only_H1_delta, double tH1, double tH1F, double LR, double LF, double LQ);
  double calculate_H2(double pdf_factor_x1x2, double tH1, double tH1F, double H1full, double sig11, double tgaga, double tcga, double tgamma2, double tH2, double LR, double LF, double LQ);

  void determine_integrand_CX_ncollinear_CT(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_CT_QEW(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_CT2(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_VT(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_VT_QEW(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_VT2(phasespace_set & psi);

  void determine_integrand_CX_ncollinear_CJ(phasespace_set & psi);
  void determine_integrand_CX_ncollinear_VJ(phasespace_set & psi);

  void determine_integrand_CX_multicollinear_CT(phasespace_set & psi);
  void determine_integrand_CX_CT2(phasespace_set & psi);

  void determine_integrand_CT(phasespace_set & psi);
  void determine_integrand_CT2(phasespace_set & psi);

  void determine_integrand_VT(phasespace_set & psi);
  void determine_integrand_VT2(phasespace_set & psi);

  // to be removed later ...
  void determine_psp_weight_VT(phasespace_set & psi);
  // to be removed later ...
  void determine_psp_weight_CT(phasespace_set & psi);
  void determine_psp_weight_CT2(phasespace_set & psi);

  void output_integrand_maximum_VT2(phasespace_set & psi);

  void output_testpoint_VT_result(ofstream & out_comparison);


  void output_pdf_comparison_CA_to_CT(phasespace_set & psi);
  void output_pdf_comparison_CX_to_CT(phasespace_set & psi);
  void output_pdf_comparison_CX_to_CT2(phasespace_set & psi);
  void output_pdf_comparison_CX_multicollinear_to_CT2(phasespace_set & psi);

  // only temporarily:
  double determine_integrand_CT(double qt2, double q2, double z1, double z2, double g_z1, double g_z2, double x1, double x2, double pdf_factor_x1x2, double pdf_factor_z1x2, double pdf_factor_x1z2, double pdf_factor_gx2, double pdf_factor_x1g, double pdf_factor_gg, double pdf_factor_qx2, double pdf_factor_x1q, double pdf_factor_z1z2, double pdf_factor_gz2, double pdf_factor_z1g, double pdf_factor_qbx2, double pdf_factor_x1qb, int order, double A_F, double LR, double LF, double LQ, vector<double> & sigma_qT, phasespace_set & psi);

  // new
  void calculate_sigma_gg( double qt2, double q2, double z1, double z2, double x1, double x2, double pdf_factor_x1x2, double pdf_factor_z1x2, double pdf_factor_x1z2, double pdf_factor_qx2, double pdf_factor_x1q, double pdf_factor_z1z2, double pdf_factor_qz2, double pdf_factor_z1q, double pdf_factor_qq, int order, double A_F,  double LR, double LF, vector<double> & sigma_qT );       
                                                              
  double calculate_virtual_gg(                    double z1, double z2, double x1, double x2, double pdf_factor_x1x2, double pdf_factor_z1x2, double pdf_factor_x1z2, double pdf_factor_qx2, double pdf_factor_x1q, double pdf_factor_z1z2, double pdf_factor_qz2, double pdf_factor_z1q, double pdf_factor_qq, int order, double H1_delta, double H2_delta, double LR, double LF);
                                                             

  double calculate_sigma_qqbar(double qt2, double q2, double z1, double z2, double g_z1, double g_z2, double x1, double x2, double pdf_factor_x1x2, double pdf_factor_z1x2, double pdf_factor_x1z2, double pdf_factor_gx2, double pdf_factor_x1g, double pdf_factor_gg, double pdf_factor_qx2, double pdf_factor_x1q, double pdf_factor_z1z2, double pdf_factor_gz2, double pdf_factor_z1g, double pdf_factor_qbx2, double pdf_factor_x1qb, int order, double A_F, double LR, double LF, double LQ, vector<double> & sigma_qT, phasespace_set & psi);

  double virtual_qqbar(double z1, double z2, double g_z1, double g_z2, double x1, double x2, double pdf_factor_x1x2, double pdf_factor_z1x2, double pdf_factor_x1z2, double pdf_factor_gx2, double pdf_factor_x1g, double pdf_factor_gg, double pdf_factor_qx2, double pdf_factor_x1q, double pdf_factor_z1z2, double pdf_factor_gz2, double pdf_factor_z1g, double pdf_factor_qbx2, double pdf_factor_x1qb, int order, double H1_delta, double H2_delta, double LR, double LF, double LQ);


  int switch_qTcut;
  int n_qTcut;
  double min_qTcut;
  double step_qTcut;
  double max_qTcut;
  string binning_qTcut;
  string selection_qTcut;

  vector<double> value_qTcut;
  
  string selection_qTcut_distribution;
  string selection_no_qTcut_distribution;
  vector<int> no_qTcut_distribution;
  vector<double> value_qTcut_distribution;

  string selection_qTcut_result;
  string selection_no_qTcut_result;
  vector<int> no_qTcut_result;
  vector<double> value_qTcut_result;

  string selection_qTcut_integration;
  string selection_no_qTcut_integration;
  vector<int> no_qTcut_integration;
  vector<double> value_qTcut_integration;

  vector<int> counter_killed_qTcut;
  vector<int> counter_acc_qTcut;

  fourvector QT_Q;
  double QT_QT;
  double QT_sqrtQ2;


  vector<vector<multicollinear_set> > multicollinear;
  vector<multicollinear_set> ncollinear;

  int active_qTcut;
  int output_n_qTcut;


  // constants relevant for qT subtraction

  int order_alphas_born;
  int initial_channel;

  int initial_gg;
  int initial_aa;
  int initial_qqx;

  int initial_pdf_gg;
  int initial_pdf_aa;
  int initial_pdf_qqx;
  int initial_pdf_gq;
  int initial_pdf_aq;
  int initial_pdf_ag;
  int initial_pdf_rest;

  int initial_pdf_diag;

  int initial_diag;
  int initial_diag_gg;
  int initial_diag_aa;
  int initial_diag_qqx;

  int QT_initialstate_type;
  int QT_finalstate_massive_coloured;
  int QT_finalstate_massive_charged;

  double m_HQ;
  double m2_HQ;

  vector<vector<vector<double> > > QT_Q2f;

  
  vector<correlationoperator> QT_correlationoperator;
  vector<double> QT_ME2_cf;

  vector<double> QT_ME2_loopcf;

  vector<double> QT_ME2_Born4cf;

  vector<double> I1_int;
  vector<double> I2_int;
  vector<double> I3_int;
  vector<double> I4_int;

  vector<double> LL0_int;
  vector<double> LL1_int;
  vector<double> LL2_int;
  vector<double> LL3_int;

  double beta0;
  double beta1;
  double Kappa;

  double A1g;
  double B1g;
  double A2g;
  double Delta2gg;
  double B2g;

  double D0gggg;
  double D1gggg;
  double Deltagggg;

  double H2ggD0;

  double A1q;
  double B1q;
  double A2q;
  double Delta2qq;
  double B2q;

  double A01q;
  double B01q;
  
  double gamma2Q;
  double gammacusp2;


  double q2;
  double v34;
  double log_v34;
  double dilogpT2m2;
  double logmT2m2;
  double L34;
  double gammacusp2_v;

  //  CC    Coefficients of D0 and D1 in P*P (as/pi normalization)
  double D0qqqq;
  double D1qqqq;
  //CC    Coefficients of delta(1-z) in P*P
  double Deltaqqqq;

  double H2qqD0;

  double A_F;
  double B2_A_F;


  
  double Ft1born;
  vector<double> Ft1born_4correlator;
  double Ft1loop;

  double Gamma1;
  double Gamma1born;
  double Gamma1loop;

  double Gamma2;
  double Gamma1squared;
  double commutator_Gammat1_Ft1;
  double anticommutator_Gammat1_Ft1;
  double Gammat1_Ft1;
  double Ft1_Gammat1;

  double Ft1;

  vector<vector<double> > coll_tH1F_contribution;
  vector<vector<double> > coll_tH1_contribution;
  vector<vector<double> > coll_tH1_only_H1_delta_contribution;
  vector<vector<double> > coll_tH1_without_H1_delta_contribution;
  vector<vector<double> > coll_tgaga_contribution;
  vector<vector<double> > coll_tcga_contribution;
  vector<vector<double> > coll_tgamma2_contribution;
  vector<vector<double> > coll_tH2_contribution;

  vector<double> coll_tH1F;
  vector<double> coll_tH1;
  vector<double> coll_tH1_only_H1_delta;
  vector<double> coll_tH1_without_H1_delta;
  vector<double> coll_tgaga;
  vector<double> coll_tcga;
  vector<double> coll_tgamma2;
  vector<double> coll_tH2;

  // central - CV - D
  vector<vector<double> > value_LR;
  vector<vector<double> > value_LF;
  vector<vector<double> > value_LQ;
 
  // central - CV
  vector<vector<vector<double > > > coll_tH1F_pdf;
  vector<vector<vector<double > > > coll_tH1_pdf;
  vector<vector<vector<double > > > coll_tH1_only_H1_delta_pdf;
  vector<vector<vector<double > > > coll_tH1_without_H1_delta_pdf;
  vector<vector<vector<double > > > coll_tgaga_pdf;
  vector<vector<vector<double > > > coll_tcga_pdf;
  vector<vector<vector<double > > > coll_tgamma2_pdf;
  vector<vector<vector<double > > > coll_tH2_pdf;

  vector<vector<double> > value_sig11;
  vector<vector<double> > value_tH1F;
  vector<vector<double> > value_tH1;
  vector<vector<double> > value_tH1_only_H1_delta;
  vector<vector<double> > value_tH1_without_H1_delta;

  vector<vector<double> > value_tgaga;
  vector<vector<double> > value_tcga;
  vector<vector<double> > value_tgamma2;

  vector<vector<double> > value_sig12;
  vector<vector<double> > value_sig23;
  vector<vector<double> > value_sig24;
  vector<vector<vector<vector<double> > > > value_sig21;
  vector<vector<vector<vector<double> > > > value_sig22;

  vector<vector<double> > value_tH2;

  vector<vector<vector<vector<double> > > > value_H1full;

  // D
  vector<vector<vector<vector<double> > > > coll_tH1F_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tH1_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tH1_only_H1_delta_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tH1_without_H1_delta_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tgaga_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tcga_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tgamma2_pdf_D;
  vector<vector<vector<vector<double> > > > coll_tH2_pdf_D;

  vector<vector<vector<double> > > value_sig11_D;
  vector<vector<vector<double> > > value_tH1F_D;
  vector<vector<vector<double> > > value_tH1_D;
  vector<vector<vector<double> > > value_tH1_only_H1_delta_D;
  vector<vector<vector<double> > > value_tH1_without_H1_delta_D;

  vector<vector<vector<double> > > value_tgaga_D;
  vector<vector<vector<double> > > value_tcga_D;
  vector<vector<vector<double> > > value_tgamma2_D;
 
  vector<vector<vector<double> > > value_sig12_D;
  vector<vector<vector<double> > > value_sig23_D;
  vector<vector<vector<double> > > value_sig24_D;
  vector<vector<vector<vector<vector<double> > > > > value_sig21_D;
  vector<vector<vector<vector<vector<double> > > > > value_sig22_D;

  vector<vector<vector<double> > > value_tH2_D;

  vector<vector<vector<vector<vector<double> > > > > value_H1full_D;

  // TSV
  vector<vector<vector<vector<double> > > > coll_tH1F_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tH1_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tH1_only_H1_delta_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tH1_without_H1_delta_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tgaga_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tcga_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tgamma2_pdf_TSV;
  vector<vector<vector<vector<double> > > > coll_tH2_pdf_TSV;

  vector<vector<double> > value_LR_TSV;
  vector<vector<double> > value_LF_TSV;
  vector<vector<double> > value_LQ_TSV;

  vector<vector<vector<double> > > value_sig11_TSV;
  vector<vector<vector<double> > > value_tH1F_TSV;
  vector<vector<vector<double> > > value_tH1_TSV; 
  vector<vector<vector<double> > > value_tH1_only_H1_delta_TSV; 
  vector<vector<vector<double> > > value_tH1_without_H1_delta_TSV; 
  
  vector<vector<vector<double> > > value_tgaga_TSV;
  vector<vector<vector<double> > > value_tcga_TSV;
  vector<vector<vector<double> > > value_tgamma2_TSV;
  
  vector<vector<vector<vector<vector<double> > > > > value_H1full_TSV; 
  
  vector<vector<vector<double> > > value_sig12_TSV;
  vector<vector<vector<double> > > value_sig23_TSV; 
  vector<vector<vector<double> > > value_sig24_TSV;
  vector<vector<vector<vector<vector<double> > > > > value_sig21_TSV;
  vector<vector<vector<vector<vector<double> > > > > value_sig22_TSV;


  vector<vector<vector<double> > > value_tH2_TSV;


  

};

#endif
