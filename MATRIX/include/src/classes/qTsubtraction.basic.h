#ifndef QTSUBTRACTIONBASIC_H
#define QTSUBTRACTIONBASIC_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <vector>

class event_set;
class phasespace_set;
class observable_set;
class inputparameter_set;

class qTsubtraction_basic{
 private:

 public:
////////////////////
//  constructors  //
////////////////////
  qTsubtraction_basic();
  //  qTsubtraction_basic(observable_set & osi);
  qTsubtraction_basic(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi);

  void initialization(inputparameter_set * isi, contribution_set * _csi, event_set * _esi, phasespace_set * _psi);
  void initialization_set_default();
  void initialization_input(inputparameter_set * isi);
  void initialization_after_input();

  void event_selection_qTcut(int x_a);
  void output();

  int switch_qTcut;
  int active_qTcut;
  int n_qTcut;
  int output_n_qTcut;
  double min_qTcut;
  double step_qTcut;
  double max_qTcut;
  string binning_qTcut;
  string selection_qTcut;

  vector<double> value_qTcut;
  
  string selection_qTcut_distribution;
  string selection_no_qTcut_distribution;
  vector<int> no_qTcut_distribution;
  vector<double> value_qTcut_distribution;

  string selection_qTcut_result;
  string selection_no_qTcut_result;
  vector<int> no_qTcut_result;
  vector<double> value_qTcut_result;

  string selection_qTcut_integration;
  string selection_no_qTcut_integration;
  vector<int> no_qTcut_integration;
  vector<double> value_qTcut_integration;

  vector<int> counter_killed_qTcut;
  vector<int> counter_acc_qTcut;

  fourvector QT_Q;
  double QT_QT;
  double QT_sqrtQ2;

  contribution_set * csi;
  phasespace_set * psi;
  event_set * esi;

};

#endif
