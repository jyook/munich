#ifndef INPUTLINE_H
#define INPUTLINE_H

class inputline{
 private:

 public:
////////////////////
//  constructors  //
////////////////////
  inputline();
  inputline(string _variable, string _value);
  inputline(string _variable, string _specifier, string _value);
  inputline(string readin_line);

  friend std::ostream & operator << (std::ostream &, const inputline & input);

  string variable;
  string specifier;
  string value;

  bool valid;
  bool assigned;
  
};
#endif
