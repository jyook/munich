#ifndef EVENTSET_H
#define EVENTSET_H

#include <map>
#include <string>
#include <vector>
#include "user.defined.h"
#include "qTsubtraction.basic.h"
#include "NJsubtraction.basic.h"

using namespace std;

class inputparameter_set;
class model_set;
class contribution_set;
class phasespace_set;
class fiducialcut;

class event_set{
 private:

 public:
////////////////////
//  constructors  //
////////////////////
  event_set();

  virtual ~event_set(){}

  virtual void particles(int i_a){}
  virtual void cuts(int i_a){}
  virtual void cuts_test(int i_a){}

  void phasespacepoint();

  virtual void phasespacepoint_born(){}
  virtual void phasespacepoint_collinear(){}
  virtual void phasespacepoint_real(){}
  virtual void phasespacepoint_realcollinear(){}
  virtual void phasespacepoint_doublereal(){}

  void define_fiducial_cut_list();
  void determine_p_parton();

  void initialization(inputparameter_set * isi, contribution_set * _csi, model_set * _msi, user_defined * _user);
  void initialization_set_default();
  void initialization_input(inputparameter_set * isi);
  void initialization_after_input();

  void initialization_complete();
  void initialization_integration();
  void initialization_runtime_partonlevel();
  void output_initialization_runtime_partonlevel();


  void determine_phasespace_object_partonlevel();
  void determine_object_definition();
  void determine_object_partonlevel();
  void determine_equivalent_object();
  void determine_relevant_object();
  void determine_runtime_object();
  void output_determine_runtime_object();

  void determine_n_partonlevel(vector<vector<int> > & type_parton);

  void define_basic_object_list();
  void define_specific_object_list();

  void perform_event_selection();
  //  void perform_event_selection(call_generic & generic);
  void perform_event_selection_phasespace(int i_a);
  void advanced_sort_by_pT(int x_a, int k_g);

  //  void event_selection_qTcut(int x_a);
  //  void event_selection_NJcut(int x_a);

  // varia/event.photon.algorithm.cpp
  void perform_photon_recombination(vector<int> & no_unrecombined_photon, int i_a);

  double frixione_discr(double delta);
  double frixione_discr(double delta, double delta_dynamic);
  double frixione_discriminant_R2(double delta);
  double frixione_discriminant_R2(double delta, double delta_dynamic);
  void perform_frixione_isolation(int & number_photon, vector<particle> & isolated_photon, particle & photon, vector<particle> & protojet);

  void jet_algorithm_flavour();
  void sc_Ellis_Soper_flavour();
  void sc_Ellis_Soper_mod_flavour();
  void kTrun2_flavour();
  void antikT_flavour();



  map<string, int> observed_object;
  vector<string> object_list;
  vector<int> object_category;

  map<string, int> observed_object_selection;
  vector<string> object_list_selection;
  vector<int> object_category_selection;

  vector<define_particle_set> pda;  // particle definition all
  vector<define_particle_set> pds;  // particle definition selection

  int n_parton_nu;

  vector<string> name_fiducial_cut;
  vector<string> fiducial_cut_list;
  vector<fiducialcut> fiducial_cut;



  // to be moved here from observable_set (and initialized) !!!
  map<string, int> access_object;
  int runtime_jet_algorithm; // ???
  vector<int> runtime_jet_recombination;
  vector<int> runtime_photon_recombination;
  vector<int> runtime_photon_isolation;
  vector<int> runtime_original;
  vector<int> runtime_missing;
  vector<int> runtime_object;
  vector<vector<int> > runtime_order;
  vector<vector<int> > runtime_order_inverse;

  vector<vector<int> > ps_n_partonlevel;
  vector<vector<int> > ps_relevant_n_partonlevel;

  vector<vector<int> > ps_runtime_jet_algorithm;
  vector<vector<int> > ps_runtime_photon;
  vector<vector<int> > ps_runtime_photon_recombination;
  vector<vector<int> > ps_runtime_photon_isolation;

  vector<vector<int> > ps_runtime_jet_recombination;

  vector<vector<int> > ps_runtime_subtraction_QCD;
  vector<vector<int> > ps_runtime_subtraction_QEW;

  vector<vector<int> > ps_runtime_original;
  vector<vector<int> > ps_runtime_missing;
  vector<vector<int> > ps_runtime_object;
  vector<vector<vector<int> > > ps_runtime_order;
  vector<vector<vector<int> > > ps_runtime_order_inverse;

  map<string, string> equivalent_object;
  map<int, int> equivalent_no_object;
  map<string, int> no_relevant_object;

  int frixione_isolation;
  double frixione_n;
  double frixione_epsilon;
  double frixione_fixed_ET_max;
  double frixione_delta_0;
  int frixione_jet_removal;

  int photon_recombination;
  int photon_R_definition;
  double photon_R;
  double photon_R2;
  double photon_E_threshold_ratio;
  int photon_jet_algorithm;

  int photon_photon_recombination;
  double photon_photon_recombination_R;

  int jet_algorithm;
  int jet_R_definition;
  double jet_R;
  double jet_R2;
  double parton_y_max;
  double parton_eta_max;
  // until here...

  // placed elsewhere in observable_set:
  vector<string> jet_algorithm_selection;
  vector<string> jet_algorithm_disable;
  vector<int> jet_algorithm_list;

  vector<string> photon_recombination_selection;
  vector<string> photon_recombination_disable;
  vector<int> photon_recombination_list;
   // until here...


  vector<vector<fourvector> > p_parton;
  vector<vector<vector<particle> > > particle_event;
  vector<vector<int> > n_object;

  vector<vector<vector<particle> > > user_particle;

  // used in event selection: could be treated in more elegant way...
  // use vector of pointers to photon particles ???
  vector<int> no_unrecombined_photon;

  // use jet-algorithm class ???
  vector<particle> particle_protojet;
  vector<vector<int> > protojet_flavour;
  vector<vector<int> > protojet_parton_origin;

  vector<particle> particle_protojet_photon;
  vector<vector<int> > protojet_flavour_photon;
  vector<vector<int> > protojet_parton_origin_photon;

  vector<particle> particle_jet;
  vector<vector<int> > jet_flavour;
  vector<vector<int> > jet_parton_origin;
  vector<particle_id> particle_jet_id;

  vector<vector<fourvector> > start_p_parton;
  vector<vector<vector<particle> > > start_particle_event;
  vector<vector<int> > start_n_object;

  vector<int> recombination_history;

  vector<int> cut_ps;
  int first_non_cut_ps;
  vector<int> change_cut;

  // to be initialised !!!
  int switch_output_cutinfo;
  int switch_testcut;
  stringstream info_cut;

  /*
  int switch_qTcut;
  int n_qTcut;
  int output_n_qTcut;
  */

  model_set * msi;
  contribution_set * csi;
  phasespace_set * psi;
  user_defined * user;

  qTsubtraction_basic * qT_basic;
  NJsubtraction_basic * NJ_basic;
};
#endif
