#ifndef SUMMARY_DISTRIBUTION_H
#define SUMMARY_DISTRIBUTION_H

class summary_subprocess;
class summary_contribution;
class summary_generic;

class summary_distribution {
private:

public:

  ////////////////////
  //  constructors  //
  ////////////////////

  summary_distribution();
  summary_distribution(int _i_d, int _i_s, int _x_q, summary_subprocess & _ysubprocess);

  void initialization();
  void readin(int i_m, int i_z, vector<vector<string> > & readin_data);
  void cleanup();

  ///////////////////////
  //  access elements  //
  ///////////////////////

  summary_subprocess *ysubprocess;
  // each 'summary_subprocess' belongs to a specific 'summary_contribution'
  summary_contribution *ycontribution;
  // only one 'summary_generic'
  summary_generic *ygeneric;
  // only one 'observable_set'
  observable_set *osi;

  string name;
  string fullname;

  int i_d;
  int i_s;
  int x_q;

  // [x_q] binwise number of events for each [i_m][i_z][i_b] combination (independent of [i_s][i_r][i_f]):
  vector<vector<vector<long long> > > group_run_N_binwise_TSV;

  // [i_s][x_q] binwise result for each [i_m][i_z][i_b][i_r][i_f] combination :
  // binwise result for each [i_m][i_z][i_b][x_q][i_s][i_r][i_f] combination :
  vector<vector<vector<vector<vector<double> > > > > group_run_result_TSV;
  // [i_s][x_q] binwise deviation for each [i_m][i_z][i_b][i_r][i_f] combination :
  vector<vector<vector<vector<vector<double> > > > > group_run_deviation_TSV;

};
#endif
