#ifndef SUMMARY_SUBPROCESS_H
#define SUMMARY_SUBPROCESS_H

class summary_contribution;
class summary_generic;

class summary_subprocess {
private:

public:
  summary_subprocess();
  summary_subprocess(summary_generic * _ygeneric);
  summary_subprocess(string _name, summary_contribution * _contribution);
  summary_subprocess(string _name, int _n_seed, int _default_size_CV, summary_contribution * _contribution);

  //  virtual ~summary_subprocess(){}

  void initialization(string _name, int _n_seed, int _default_size_CV, summary_contribution * _contribution);

  //  the following two functions can most likely be deleted !!!
  void combine_result_original();
  void combine_result_alternative();

  double get_time_stamp(int i_s, int i_m, int i_z);
  void set_file_distribution(int i_s, int i_m, int i_z);
  void check_reset_file_distribution(int i_s, int i_m, int i_z);
  void cleanup_file_distribution();
  void cleanup();

  //  summary/summary.subprocess.runtime.cpp
  void initialization_runtime();
  void readin_runtime(string result_moment);

  //  summary/summary.subprocess.result.cpp
  void readin_result_TSV(string & result_moment);
  void combination_result_TSV(string & result_moment);
  void removal_exceptional_runs_result_TSV();
  void combine_result_original_TSV();
  void combine_result_hybrid_TSV();
  void combine_result_alternative_TSV();

  void readin_result_CV(string & result_moment);
  void combination_result_CV(string & result_moment);
  void removal_exceptional_runs_result_CV();
  void combine_result_original_CV();
  void combine_result_alternative_CV();
  void combine_result_hybrid_CV();


  //  summary/summary.subprocess.distribution.cpp
  void initialization_distribution_TSV();
  void readin_distribution_TSV();
  void combine_distribution_conservative_TSV(int x_q, int i_d, int i_b, int i_s, int i_r, int i_f, summary_distribution * distribution);

  void initialization_distribution_CV();
  void readin_distribution_CV();
  void combination_distribution_CV();
  void combine_distribution_conservative_CV(int i_d, int i_b, int i_s);

  //  summary/summary.subprocess.output.distribution.cpp
  void output_distribution_qTcut_TSV();


  //  void old_combine_distribution_conservative_TSV(int x_q, int i_d, int i_b, int i_s, int i_r, int i_f);
  //  void combine_distribution_aggressive_TSV(int x_q, int i_d, int i_b, int i_s, int i_r, int i_f);
  //  void combination_distribution_TSV();
  //  void combine_distribution_hybrid_TSV(int x_q, int i_d, int i_b, int i_s, int i_r, int i_f);

  
  summary_generic * ygeneric;
  observable_set * osi;
  summary_contribution * ycontribution;

  string name;
  int n_seed;
  int default_size_CV;

  int n_ps;

  double error2_time;
  double time_per_event;
  double used_runtime;
  long long used_n_event;

  double extrapolated_runtime;
  double extrapolated_n_event;
  double extrapolated_sigma_deviation;
  double extrapolated_sigma_normalization;
  double extrapolated_sigma_normalization_deviation;

  int max_number_of_jobs_for_one_channel;

  vector<double> result_run;
  vector<double> deviation_run;

  vector<vector<int> > remove_run_qTcut;
  vector<vector<int> > remove_run_qTcut_TSV;
  vector<vector<int> > remove_run_qTcut_CV;

  vector<double> error2_time_run;
  vector<double> time_per_event_run;
  vector<double> used_runtime_run;
  vector<long long> used_n_event_run;



  // Create corresponding group variables for tail-enhanced runs [i_m]:
  // The normal one could refer to the combination later (and be equal to the standard mapping for now):
  // Need to be implemented in the runtime management...

  vector<double> error2_time_group;
  vector<double> time_per_event_group;
  vector<double> used_runtime_group;
  vector<long long> used_n_event_group;

  vector<double> extrapolated_runtime_group;
  vector<double> extrapolated_n_event_group;
  vector<double> extrapolated_sigma_deviation_group;
  vector<double> extrapolated_sigma_normalization_group;
  vector<double> extrapolated_sigma_normalization_deviation_group;

  vector<vector<double> > error2_time_group_run;
  vector<vector<double> > time_per_event_group_run;
  vector<vector<double> > used_runtime_group_run;
  vector<vector<long long> > used_n_event_group_run;

  // needed ???
  vector<vector<double> > result_group_run;
  vector<vector<double> > deviation_group_run;
  vector<vector<long long> > N_group;
  // until here ...
  // needed to avoid reduction of events per run:
  vector<long long> N_group_max;




  long long no_results;

  // only for reference result !!!
  vector<long long> N;
  long long N_all;


  double result;
  double deviation;
  double chi2;

  double Nd_event_CV;
  long long N_event_CV;
  vector<long long> N_valid_event_CV;
  // 1st: qTcut
  vector<vector<double> > result_CV;
  vector<vector<double> > deviation_CV;
  vector<vector<double> > chi2_CV;

 //  check meaning of n_parallel_runs_CV and related ones !!!
  vector<int> n_parallel_runs_reference_CV;
  vector<int> n_valid_parallel_runs_reference_CV;
  vector<vector<int> > n_parallel_runs_CV;
  vector<vector<int> > n_valid_parallel_runs_CV;

  vector<long long> N_event_run_CV;
  vector<vector<vector<double> > > result_run_CV;
  vector<vector<vector<double> > > deviation_run_CV;



  //  long long no_results_TSV;

  double Nd_event_TSV;
  long long N_event_TSV;
  vector<long long> N_valid_event_TSV;
  // 1st: qTcut
  vector<vector<vector<vector<vector<double> > > > > result_TSV;
  vector<vector<vector<vector<vector<double> > > > > deviation_TSV;
  vector<vector<vector<vector<vector<double> > > > > chi2_TSV;
  //  check meaning of n_parallel_runs_TSV !!!
  vector<int> n_parallel_runs_reference_TSV;
  vector<int> n_valid_parallel_runs_reference_TSV;
  vector<vector<vector<vector<vector<int> > > > > n_parallel_runs_TSV;
  vector<vector<vector<vector<vector<int> > > > > n_valid_parallel_runs_TSV;

  vector<long long> N_event_run_TSV;
  vector<vector<vector<vector<vector<vector<double> > > > > > result_run_TSV;
  vector<vector<vector<vector<vector<vector<double> > > > > > deviation_run_TSV;



  vector<vector<vector<double> > > distribution_result_CV;
  vector<vector<vector<double> > > distribution_deviation_CV;
  vector<vector<vector<double> > > distribution_chi2_CV;

  // in analogy to TSV:

  // total number of events for each [i_m] combination (summed over all [i_z], independent of [i_d][i_b][i_s]
  vector<long long> distribution_group_N_CV; // needed ???
  // binwise number of events for each [i_m][i_d][i_b][x_q] combination (summed over all [i_z], independent of [i_s]):
  vector<vector<vector<long long> > > distribution_group_N_binwise_CV;

  // combination of [i_m] combinations
  vector<vector<vector<vector<double> > > > distribution_group_result_CV;
  vector<vector<vector<vector<double> > > > distribution_group_deviation_CV;
  vector<vector<vector<vector<double> > > > distribution_group_chi2_CV;

  // total number of events for each [i_m][i_z] combination (independent of [i_d][i_b][i_s]):
  vector<vector<long long> > distribution_group_run_N_CV;
  // binwise number of events for each [i_m][i_z][i_d][i_b] combination (independent of [i_s]):
  vector<vector<vector<vector<long long> > > > distribution_group_run_N_binwise_CV;

  vector<vector<vector<vector<vector<double> > > > > distribution_group_run_result_CV;
  vector<vector<vector<vector<vector<double> > > > > distribution_group_run_deviation_CV;



  // total number of events from runs that have not been removed form a certain [i_m][i_d][i_b] result (removal determined separately for each [i_d][i_b] contribution):
  vector<vector<vector<long long> > > distribution_group_N_total_CV;
  // new: counts events per bin after removal of runs:
  vector<vector<vector<long long> > > distribution_group_N_total_binwise_CV;

  // counter of removed/nonzero runs (sum over all i_z) for certain [i_m][i_d][i_b]:
  vector<vector<vector<int> > > distribution_group_counter_removal_run_CV;
  vector<vector<vector<int> > > distribution_group_counter_nonzero_run_CV;

  //  switch for removal (0/1) of runs for certain [i_m][i_z][i_d][i_b]:
  vector<vector<vector<vector<int> > > > distribution_group_run_removal_run_CV;

  vector<vector<long long> > distribution_N_total_CV;
  // new: counts events per bin after removal of runs:
  vector<vector<long long> > distribution_N_total_binwise_CV;



  // TSV distribution

  // total number of contributing runs
  vector<long long> distribution_group_counter_run_TSV;

  // combination of [i_m] combinations
  vector<vector<vector<vector<vector<vector<double> > > > > > distribution_result_TSV;
  vector<vector<vector<vector<vector<vector<double> > > > > > distribution_deviation_TSV;
  vector<vector<vector<vector<vector<vector<double> > > > > > distribution_chi2_TSV;

  // total number of events for each [i_m] combination (summed over all [i_z], independent of [i_d][i_b][x_q][i_s][i_r][i_f]
  vector<long long> distribution_group_N_TSV; // needed ???
  // binwise number of events for each [i_m][i_d][i_b][x_q] combination (summed over all [i_z], independent of [x_q][i_s][i_r][i_f]):
  vector<vector<vector<vector<long long> > > > distribution_group_N_binwise_TSV;

  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > distribution_group_result_TSV;
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > distribution_group_deviation_TSV;
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > distribution_group_chi2_TSV;

  // total number of events for each [i_m][i_z] combination (independent of [i_d][i_b][x_q][i_s][i_r][i_f]):
  vector<vector<long long> > distribution_group_run_N_TSV;
  // binwise number of events for each [i_m][i_z][i_d][i_b][x_q] combination (independent of [i_s][i_r][i_f]):
  vector<vector<vector<vector<vector<long long> > > > > distribution_group_run_N_binwise_TSV;
  // binwise result for each [i_m][i_z][i_d][i_b][x_q][i_s][i_r][i_f] combination :
  vector<vector<vector<vector<vector<vector<vector<vector<double> > > > > > > > distribution_group_run_result_TSV;
  // binwise deviation for each [i_m][i_z][i_d][i_b][x_q][i_s][i_r][i_f] combination :
  vector<vector<vector<vector<vector<vector<vector<vector<double> > > > > > > > distribution_group_run_deviation_TSV;

  // total number of events from runs that have not been removed form a certain [i_m][i_d][i_b][x_q] result (removal determined separately for each [i_d][i_b][x_q] contribution):
  vector<vector<vector<vector<long long> > > > distribution_group_N_total_TSV;
  // new: counts events per bin after removal of runs:
  vector<vector<vector<vector<long long> > > > distribution_group_N_total_binwise_TSV;

  vector<vector<vector<long long> > > distribution_N_total_TSV;
  // new: counts events per bin after removal of runs:
  vector<vector<vector<long long> > > distribution_N_total_binwise_TSV;


  // counter of removed/nonzero runs (sum over all i_z) for certain [i_m][i_d][i_b][x_q]:
  vector<vector<vector<vector<int> > > > distribution_group_counter_removal_run_qTcut_TSV;
  vector<vector<vector<vector<int> > > > distribution_group_counter_nonzero_run_qTcut_TSV;

  //  switch for removal (0/1) of runs for certain [i_m][i_z][i_d][i_b][x_q]:
  vector<vector<vector<vector<vector<int> > > > > distribution_group_run_removal_run_qTcut_TSV;



  vector<vector<int> > type_parton;

  // distribution infile variables
  vector<vector<vector<int> > > index_contributing_run;

  vector<vector<vector<ifstream * > > > in_result_vector;
  vector<vector<vector<vector<streampos> > > > pos_line;
  vector<vector<vector<string> > > filepath_time;
  vector<vector<vector<string> > > filepath_distribution;
  vector<vector<vector<double> > > time_stamp;

  vector<vector<vector<vector<int> > > > no_line_distribution;
  vector<vector<vector<vector<vector<streampos> > > > > pos_qTcut_distribution;

  // same format as for summary_contribution/list/order
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > distribution_result_qTcut_TSV;
  vector<vector<vector<vector<vector<vector<vector<double> > > > > > > distribution_deviation_qTcut_TSV;
  // 1st: x_q - selection_n_qTcut (number of qTcut values selected for distributions)
  // 2nd: i_g - 1 (subgroup.size() (number of subprocess groups: 0 entry -> sum over all subgroups))
  // 3rd: i_d - n_distribution (number of (dd) distributions)
  // 4th: i_b - n_bin (number of bins of the respective distribution)
  // 5th: i_s - n_scale_set_TSV (number of scale sets in TSV)
  // 6th: i_r - n_scales_ren_TSV (number of renormalization scales evaluated in TSV files)
  // 7th: i_f - n_scales_fact_TSV (number of renormalization scales evaluated in TSV files)

};
#endif
