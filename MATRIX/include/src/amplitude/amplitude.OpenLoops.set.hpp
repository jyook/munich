#ifndef AMPLITUDE_OPENLOOPSSET_H
#define AMPLITUDE_OPENLOOPSSET_H
#ifdef OPENLOOPS

using namespace std;

class phasespace_set;

class amplitude_OpenLoops_set : virtual public amplitude_set {
 private:

 public:
  amplitude_OpenLoops_set();
  amplitude_OpenLoops_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi);
  
  virtual ~amplitude_OpenLoops_set();

  virtual void initialization(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi);
  virtual void initialization(inputparameter_set * isi);
  virtual void initialization_set_default();
  virtual void initialization_input(inputparameter_set * isi);
  virtual void initialization_parameter();
  virtual void initialization_process();
  virtual void output_parameter(string label);
  virtual void generate_testpoint();
  virtual void set_phasespacepoint();
  virtual void set_phasespacepoint_dipole(const int x_a);
  virtual void output_phasespacepoints();

  // Born
  virtual void calculate_ME2_born();
  virtual void check_vanishing_ME2_born();

  // VA
  virtual void set_mu_and_delta_VA();
  virtual void set_testpoint_alpha_S(const double alpha_S);
  virtual void set_testpoint_rescale_I_ME2();
  virtual void calculate_ME2_ioperator_VA_QCD();
  virtual void calculate_ME2_VA_QCD();
  virtual void calculate_ME2_ioperator_VA_QEW();
  virtual void calculate_ME2_VA_QEW();
  virtual void calculate_ME2_ioperator_VA_MIX();
  virtual void calculate_ME2_VA_MIX();

  // CA
  virtual void calculate_ME2_CA_QCD();
  virtual void calculate_ME2_CA_QEW();

  // RA
  virtual void calculate_ME2_RA_QCD();
  virtual void calculate_dipole_Acc_QCD(int x_a, double & Dfactor);
  virtual void calculate_dipole_Asc_QCD(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector);
  virtual void calculate_ME2_RA_QEW();
  virtual void calculate_dipole_Acc_QEW(int x_a, double & Dfactor);
  virtual void calculate_dipole_Asc_QEW(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector);
  virtual void calculate_ME2_RA_MIX();

  // VT
  virtual void calculate_ME2_VT_QCD();

  // CT
  virtual void calculate_ME2_CT_QCD();

  // VT2
  virtual void calculate_ME2_VT2_QCD();

  // CT2
  virtual void calculate_ME2_CT2_QCD();

  // external 2-loop components:
  virtual void calculate_H1gg_2loop(){}
  //  virtual void calculate_H2_2loop(){}


  virtual int register_OL_subprocess(int i_a, int amptype);

  vector<string> OL_parameter;
  vector<string> OL_value;

  // Should be unified with OL_mu_ren:
  char * renscale;

  char * fact_uv;
  char * fact_ir;
  char * OL_mu_ren;
  char * OL_mu_reg;
  char * pole_uv;
  char * pole_ir1;
  char * pole_ir2;
  char * polenorm;
  char * OL_alpha_s;

  char * OL_ct_on;

  // Unify the tree amplitude labels !!!
  double M2tree;
  double M2L0;
  double * M2L1;
  double * IRL1;
  double * M2L2;
  double * IRL2;
  double ME2tree;
  double ME2loop;

  int n_momentum;
  double * P_OpenLoops;

  int n_cc_check;
  double *M2cc_check;

  int n_momentum_dipole;
  vector <double*> P_OL;

  int n_cc;
  double *M2cc;
  double *M2loopcc;

  int n_sc;
  vector <double*> P_sc;
  double * M2sc;

  double ewcc;
  double one;
  double acc;

};
#endif
#endif
