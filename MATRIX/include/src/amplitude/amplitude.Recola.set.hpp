#ifndef AMPLITUDE_RECOLASET_H
#define AMPLITUDE_RECOLASET_H
#ifdef RECOLA

using namespace std;

class phasespace_set;

class amplitude_Recola_set : virtual public amplitude_set {
 private:

 public:
  amplitude_Recola_set();

  amplitude_Recola_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi);
  
  virtual ~amplitude_Recola_set() {}

  virtual void initialization(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi);
  virtual void initialization(inputparameter_set * isi);
  virtual void initialization_set_default();
  virtual void initialization_input(inputparameter_set * isi);
  virtual void initialization_parameter();
  virtual void initialization_process();
  virtual void output_parameter(string label);
 // not available yet !!!
  virtual void generate_testpoint();
  virtual void set_phasespacepoint(double P_Recola [][4]);
  virtual void set_phasespacepoint_dipole(double P_Recola [][4], const int x_a);
  virtual void set_phasespacepoint_OLformat();

  void calculate_ME2_born();
  void check_vanishing_ME2_born();

  // VA
  void set_mu_and_delta_VA();
  void set_testpoint_alpha_S(const double alpha_S);
  void set_testpoint_rescale_I_ME2();
  void calculate_ME2_ioperator_VA_QCD();
  void calculate_ME2_VA_QCD();
  void calculate_ME2_ioperator_VA_QEW();
  void calculate_ME2_VA_QEW();
  void calculate_ME2_ioperator_VA_MIX();
  void calculate_ME2_VA_MIX();

  // CA
  void calculate_ME2_CA_QCD();
  void calculate_ME2_CA_QEW();

  // RA
  void calculate_ME2_RA_QCD();
  void calculate_dipole_Acc_QCD(int x_a, double & Dfactor);
  void calculate_dipole_Asc_QCD(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector);
  void calculate_ME2_RA_QEW();
  void calculate_dipole_Acc_QEW(int x_a, double & Dfactor);
  void calculate_dipole_Asc_QEW(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector);
  void calculate_ME2_RA_MIX();

  // VT
  void calculate_ME2_VT_QCD();

  // CT
  void calculate_ME2_CT_QCD();

  // VT2
  void calculate_ME2_VT2_QCD();

  // CT2
  void calculate_ME2_CT2_QCD();

  // external 2-loop components:
  virtual void calculate_H1gg_2loop(){}
  //  virtual void calculate_H2_2loop(){}


  void register_RCL_subprocess(int i_a, int amptype, int & process_id_rcl);

  vector<string> RCL_parameter;
  vector<string> RCL_value;

  int n_momentum;
  double * P_OLformat;

};
#endif
#endif
