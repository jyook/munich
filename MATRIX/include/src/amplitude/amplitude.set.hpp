#ifndef AMPLITUDESET_H
#define AMPLITUDESET_H

using namespace std;

class phasespace_set;

class amplitude_set {
 private:

 public:
  amplitude_set();
  amplitude_set(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi);

  virtual ~amplitude_set(){}

  virtual void initialization(){cout << "amplitude_set initialization called..." << endl;}
  virtual void initialization(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi);
  virtual void initialization_basic(observable_set * _osi, contribution_set * _csi, model_set * _msi, event_set * _esi, inputparameter_set * isi);
  virtual void initialization_set_default();
  virtual void initialization_input(inputparameter_set * isi);
  virtual void initialization_parameter(){}
  virtual void initialization_process(){}
  virtual void output_parameter(string label){}
  virtual void generate_testpoint(){}
  virtual void output_phasespacepoints(){}

  // Born
  virtual void calculate_ME2_born(){cout << "amplitude_set calculate_ME2_born called..." << endl;}
  virtual void calculate_ME2check_born();
  virtual void check_vanishing_ME2_born(){}

  // dipole subtraction
  virtual void set_testpoint_scale_CA();
  virtual void set_testpoint_scale_TSV(int i_a);

  // VA
  virtual void set_mu_and_delta_VA(){}
  virtual void set_testpoint_alpha_S(const double alpha_S){}
  virtual void set_testpoint_rescale_I_ME2(){}
  virtual void calculate_ME2_ioperator_VA_QCD(){}
  virtual void calculate_ME2_VA_QCD(){}
  virtual void calculate_ME2check_VA_QCD();
  virtual void calculate_ME2_ioperator_VA_QEW(){}
  virtual void calculate_ME2_VA_QEW(){}
  virtual void calculate_ME2check_VA_QEW();
  virtual void calculate_ME2_ioperator_VA_MIX(){}
  virtual void calculate_ME2_VA_MIX(){}
  virtual void calculate_ME2check_VA_MIX();

  // CA
  virtual void calculate_ME2_CA_QCD(){}
  virtual void calculate_ME2check_CA_QCD();
  virtual void calculate_ME2_CA_QEW(){}
  virtual void calculate_ME2check_CA_QEW();

  // RA
  virtual void calculate_ME2_RA_QCD(){}
  virtual void calculate_ME2check_RA_QCD(phasespace_set * psi);
  virtual void calculate_dipole_Acc_QCD(int x_a, double & Dfactor){}
  virtual void calculate_dipole_Asc_QCD(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector){}
  virtual double calculate_dipole_QCD_A_ij_k(int x_a);
  virtual double calculate_dipole_QCD_A_ij_k_massive(int x_a);
  virtual double calculate_dipole_QCD_A_ij_a(int x_a);
  virtual double calculate_dipole_QCD_A_ij_a_massive(int x_a);
  virtual double calculate_dipole_QCD_A_ai_k(int x_a);
  virtual double calculate_dipole_QCD_A_ai_b(int x_a);
  virtual void calculate_ME2_RA_QEW(){}
  virtual void calculate_ME2check_RA_QEW(phasespace_set * psi);
  virtual void calculate_dipole_Acc_QEW(int x_a, double & Dfactor){}
  virtual void calculate_dipole_Asc_QEW(int x_a, fourvector & Vtensor, double & ME2_metric, double & ME2_vector){}
  virtual double calculate_dipole_QEW_A_ij_k(int x_a);
  virtual double calculate_dipole_QEW_A_ij_k_massive(int x_a);
  virtual double calculate_dipole_QEW_A_ij_a(int x_a);
  virtual double calculate_dipole_QEW_A_ij_a_massive(int x_a);
  virtual double calculate_dipole_QEW_A_ai_k(int x_a);
  virtual double calculate_dipole_QEW_A_ai_b(int x_a);
  virtual void calculate_ME2_RA_MIX(){}
  virtual void calculate_ME2check_RA_MIX(phasespace_set * psi);

  // VT
  virtual void calculate_ME2_VT_QCD(){}
  virtual void calculate_ME2check_VT_QCD();
  virtual string output_amplitude_VT_QCD();

  // CT
  virtual void calculate_ME2_CT_QCD(){}
  virtual void calculate_ME2check_CT_QCD();
  virtual string output_amplitude_CT_QCD();

  // VT2
  virtual void calculate_ME2_VT2_QCD(){}
  virtual void calculate_ME2check_VT2_QCD();
  virtual string output_amplitude_VT2_QCD();

  // CT2
  virtual void calculate_ME2_CT2_QCD(){}
  virtual void calculate_ME2check_CT2_QCD();
  virtual string output_amplitude_CT2_QCD();

  // external 2-loop components:
  virtual void calculate_H1gg_2loop(){}
  virtual void calculate_H2_2loop(){}

  // massive qT subtration - auxiliary functions
  virtual void calculate_QT_kinematics(){}
  virtual void calculate_fourcolourcorrelators(){}
  virtual void calculate_Ft1born(){}
  virtual void calculate_Ft1born_4correlator(){}
  virtual void calculate_Gamma1born(){}
  virtual void calculate_Gamma1loop(){}
  virtual void calculate_commutator_Gamma1_Ft1(){}
  virtual void calculate_Gamma2(){}
  virtual void calculate_Gamma1_squared(){}


  contribution_set * csi;
  model_set * msi;
  event_set * esi;
  observable_set * osi;

  int N_quarks;
  int N_nondecoupled;
  int process_id;

  int allowed_Delta_IRneqUV;

  int errorflag;
};
#endif
