#ifndef MUNICH_H
#define MUNICH_H

#include <string>
#include <vector>
#include <chrono>
#include <ctime>

using namespace std;

class amplitude_set;

class munich{
 private:

 public:
  munich();
  munich(int argc, char *argv[], string basic_process_class);

  void initialization();

  void walltime_start();
  void walltime_now();
  void walltime_end();

  void run_initialization();
  void run_integration();

  void get_summary();

  void calculate_p_parton();

  void integration_born();

  void integration_VA_QCD();
  void integration_CA_QCD();
  void integration_RA_QCD();
  void integration_VA_QEW();
  void integration_CA_QEW();
  void integration_RA_QEW();
  void integration_VA_MIX();
  void integration_RA_MIX();

  void integration_VT_QCD();
  void integration_CT_QCD();
  void integration_VT2_QCD();
  void integration_CT2_QCD();

  void handling_vanishing_me2();
  void errorhandling_c_psp();
  void errorhandling_me2();
  void errorhandling_alpha_S();
  void errorhandling_pdf();
  void errorhandling_gtot();
  void errorhandling_OL();
  void errorhandling_cut_technical();
  void errorhandling_c_xbpsp();
  void errorhandling_c_psp_initial();
  void errorhandling_RA_me2(int xswitch);
  void errorhandling_RA_gtot();
  void errorhandling_collinear_me2();


  string subprocess;
  string order;
  string infilename;
  string infilename_scaleband;

  inputparameter_set * isi;
  contribution_set * csi;
  model_set * msi;
  event_set * esi;
  observable_set * osi;
  phasespace_set * psi;
  amplitude_set * asi;
  user_defined * user;
  summary_generic * ysi;

  qTsubtraction_basic * qT_basic;
  NJsubtraction_basic * NJ_basic;


  // Catani--Seymour subtraction
  vector<vector<ioperator_set> > ioperator;
  vector<vector<collinear_set> > collinear;

  time_t y2k_time;
  chrono::system_clock::time_point start_time_point;
  time_t start_time;
  chrono::system_clock::time_point now_time_point;
  time_t now_time;
  chrono::system_clock::time_point end_time_point;
  time_t end_time;

  clock_t start_CPU_time;
  clock_t now_CPU_time;
  clock_t end_CPU_time;
  
};

#endif
