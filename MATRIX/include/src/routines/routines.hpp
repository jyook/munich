//  routines/routines.generic.cpp
// -> summary_subprocess
void output_memory_consumption(const string identifier);
// -> inputparameter
string get_path();
// general (observable, summary, phasespace)
void system_execute(Logger & logger, string xorder);
void system_execute(Logger & logger, string xorder, int isystem);
int intpow(int basis, int exponent);
// -> observable (needed?)
double g_from_alpha(double alpha);
// -> general (phasespace, ...)
double lambda(double x, double y, double z);
string double2hexastr(double d);
double hexastr2double(const string & s);
// -> general (amplitude, ...)
char * stch(string temp_s);
char stchar(string & temp_s);

// ->amplitude/OpenLoops
void munich_ol_setparameter_int(string parameter, int value, Logger & logger);
void munich_ol_setparameter_double(string parameter, double value, Logger & logger);
void munich_ol_setparameter_string(string parameter, string value, Logger & logger);
void munich_ol_getparameter_int(string parameter, int & value, Logger & logger);
void munich_ol_getparameter_double(string parameter, double & value, Logger & logger);
void munich_ol_getparameter_string(string parameter, string & value, Logger & logger);

// -> generator/create
vector<int> get_vector_from_array_int(int * temp_array, int size_array);
// -> general (summary, ...)
string time_hms_from_double(double & time);

// general (phasespace, generator)
double max_subset_from_binary(vector<double> & xbsqrtsmin_opt, int b);
vector<int> vectorint_from_binary(int b);
vector<int> vectorbinary_from_binary(int b);
//double ran(vector<double> & s);
// general (phasespace, ...)
void randomvector(vector<double> & s, int n, vector<double> & r);
//int sign(int x);

// general
bool munich_isnan(double & temp);
bool munich_isinf(double & temp);
// -> inputparameter
void get_userinput_from_readin(vector<string> & user_variable, vector<string> & user_variable_additional, vector<string> & user_value, vector<string> & readin);


//  routines/routines.output.cpp
string output_subprocess(vector<int> & subprocess);
void output_weight_vegas(string & filename_alpha, vector<double> & alpha);
void output_weight_optimization(phasespace_set & psi, observable_set & oset, int & size_proc_generic, vector<int> & size_proceeding);
void output_momenta(ofstream & out_comparison, observable_set & oset);
void output_result_VA(ofstream & out_comparison, double ME2, double V_ME2, double X_ME2, double I_ME2);
void output_gnuplotfile(string & filename, int icount, double reldeviation, int n_events_max);
void get_latex_subprocess(vector<string> & latex_subprocess, vector<string> & subprocess);
void get_latex_subgroup(vector<string> & latex_subgroup, vector<string> & subgroup);
string output_result_deviation(double av_result, double av_deviation, int digits_deviation);
string output_percent(double av_result, double result, int digits_deviation);
string output_latex_percent(double av_result, double result, int digits_deviation);
string output_commadigits(double result, int digits);
void table_4_start(ofstream & out_res, int is, int scale_number, int columns, vector<string> v_dir_directory);
void table_4_end(ofstream & out_res);
void write_infile_int(ofstream & out_file, string name, int value_i);
void write_infile_long_long(ofstream & out_file, string name, long long value_i);
void write_infile_double(ofstream & out_file, string name, double value_d);
void write_infile_string(ofstream & out_file, string name, string value_s);


// -> observable
//  routines/routines.particle.code.cpp
void fill_name_particle(map<int, string> & name_particle);
void fill_datname(map<int, string> & datname);
//void fill_charge_particle(map<int, double> & charge_particle);
//void fill_latexname(map<int, string> & latexname);
void fill_gnuplotname(map<int, string> & gnuplotname);

// -> contribution
//  routines/routines.particle.code.cpp
void fill_code_particle(map<string, int> & code_particle);
void determine_process(string & subprocess, vector<vector<string> > & particles_name, int & process_type, int & n_particle, vector<int> & pa, map<string, int> & code_particle);

// -> summary_subprocess
//  routines/routines.particle.code.cpp
void subprocess_readin(string & process_class, string & subprocess, vector<string> & decay, int & process_type, int & n_particle, vector<vector<int> > & type_parton);



// routines/routines.selection.parton.cpp
void fill_parton_content(vector<vector<int> > & content_parton);
void fill_relation_parton_content(vector<vector<int> > & content_parton, map<string, vector<int> > & relation_content_parton);

void determine_selection_content_particle(vector<int> & content_list, vector<string> & content_selection, vector<string> & content_disable);

