#ifndef ppwx01_EVENT_SET_HPP
#define ppwx01_EVENT_SET_HPP

using namespace std;

class ppwx01_event_set : public event_set {
private:

public:
  ppwx01_event_set(){}
  ~ppwx01_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
