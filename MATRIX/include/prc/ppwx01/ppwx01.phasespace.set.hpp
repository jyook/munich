#ifndef ppwx01_PHASESPACE_SET_HPP
#define ppwx01_PHASESPACE_SET_HPP

using namespace std;

class ppwx01_phasespace_set : public phasespace_set {
private:

public:
  ppwx01_phasespace_set(){}
  ~ppwx01_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_010_udx_wp(int x_a);
  void ac_psp_010_udx_wp(int x_a, int channel);
  void ag_psp_010_udx_wp(int x_a, int zero);
  void ax_psp_010_usx_wp(int x_a);
  void ac_psp_010_usx_wp(int x_a, int channel);
  void ag_psp_010_usx_wp(int x_a, int zero);
  void ax_psp_010_ubx_wp(int x_a);
  void ac_psp_010_ubx_wp(int x_a, int channel);
  void ag_psp_010_ubx_wp(int x_a, int zero);
  void ax_psp_010_cdx_wp(int x_a);
  void ac_psp_010_cdx_wp(int x_a, int channel);
  void ag_psp_010_cdx_wp(int x_a, int zero);
  void ax_psp_010_csx_wp(int x_a);
  void ac_psp_010_csx_wp(int x_a, int channel);
  void ag_psp_010_csx_wp(int x_a, int zero);
  void ax_psp_010_cbx_wp(int x_a);
  void ac_psp_010_cbx_wp(int x_a, int channel);
  void ag_psp_010_cbx_wp(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_110_gu_wpd(int x_a);
  void ac_psp_110_gu_wpd(int x_a, int channel);
  void ag_psp_110_gu_wpd(int x_a, int zero);
  void ax_psp_110_gu_wps(int x_a);
  void ac_psp_110_gu_wps(int x_a, int channel);
  void ag_psp_110_gu_wps(int x_a, int zero);
  void ax_psp_110_gu_wpb(int x_a);
  void ac_psp_110_gu_wpb(int x_a, int channel);
  void ag_psp_110_gu_wpb(int x_a, int zero);
  void ax_psp_110_gc_wpd(int x_a);
  void ac_psp_110_gc_wpd(int x_a, int channel);
  void ag_psp_110_gc_wpd(int x_a, int zero);
  void ax_psp_110_gc_wps(int x_a);
  void ac_psp_110_gc_wps(int x_a, int channel);
  void ag_psp_110_gc_wps(int x_a, int zero);
  void ax_psp_110_gc_wpb(int x_a);
  void ac_psp_110_gc_wpb(int x_a, int channel);
  void ag_psp_110_gc_wpb(int x_a, int zero);
  void ax_psp_110_gdx_wpux(int x_a);
  void ac_psp_110_gdx_wpux(int x_a, int channel);
  void ag_psp_110_gdx_wpux(int x_a, int zero);
  void ax_psp_110_gdx_wpcx(int x_a);
  void ac_psp_110_gdx_wpcx(int x_a, int channel);
  void ag_psp_110_gdx_wpcx(int x_a, int zero);
  void ax_psp_110_gsx_wpux(int x_a);
  void ac_psp_110_gsx_wpux(int x_a, int channel);
  void ag_psp_110_gsx_wpux(int x_a, int zero);
  void ax_psp_110_gsx_wpcx(int x_a);
  void ac_psp_110_gsx_wpcx(int x_a, int channel);
  void ag_psp_110_gsx_wpcx(int x_a, int zero);
  void ax_psp_110_gbx_wpux(int x_a);
  void ac_psp_110_gbx_wpux(int x_a, int channel);
  void ag_psp_110_gbx_wpux(int x_a, int zero);
  void ax_psp_110_gbx_wpcx(int x_a);
  void ac_psp_110_gbx_wpcx(int x_a, int channel);
  void ag_psp_110_gbx_wpcx(int x_a, int zero);
  void ax_psp_110_udx_wpg(int x_a);
  void ac_psp_110_udx_wpg(int x_a, int channel);
  void ag_psp_110_udx_wpg(int x_a, int zero);
  void ax_psp_110_usx_wpg(int x_a);
  void ac_psp_110_usx_wpg(int x_a, int channel);
  void ag_psp_110_usx_wpg(int x_a, int zero);
  void ax_psp_110_ubx_wpg(int x_a);
  void ac_psp_110_ubx_wpg(int x_a, int channel);
  void ag_psp_110_ubx_wpg(int x_a, int zero);
  void ax_psp_110_cdx_wpg(int x_a);
  void ac_psp_110_cdx_wpg(int x_a, int channel);
  void ag_psp_110_cdx_wpg(int x_a, int zero);
  void ax_psp_110_csx_wpg(int x_a);
  void ac_psp_110_csx_wpg(int x_a, int channel);
  void ag_psp_110_csx_wpg(int x_a, int zero);
  void ax_psp_110_cbx_wpg(int x_a);
  void ac_psp_110_cbx_wpg(int x_a, int channel);
  void ag_psp_110_cbx_wpg(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_210_gg_wpdux(int x_a);
  void ac_psp_210_gg_wpdux(int x_a, int channel);
  void ag_psp_210_gg_wpdux(int x_a, int zero);
  void ax_psp_210_gg_wpdcx(int x_a);
  void ac_psp_210_gg_wpdcx(int x_a, int channel);
  void ag_psp_210_gg_wpdcx(int x_a, int zero);
  void ax_psp_210_gg_wpsux(int x_a);
  void ac_psp_210_gg_wpsux(int x_a, int channel);
  void ag_psp_210_gg_wpsux(int x_a, int zero);
  void ax_psp_210_gg_wpscx(int x_a);
  void ac_psp_210_gg_wpscx(int x_a, int channel);
  void ag_psp_210_gg_wpscx(int x_a, int zero);
  void ax_psp_210_gg_wpbux(int x_a);
  void ac_psp_210_gg_wpbux(int x_a, int channel);
  void ag_psp_210_gg_wpbux(int x_a, int zero);
  void ax_psp_210_gg_wpbcx(int x_a);
  void ac_psp_210_gg_wpbcx(int x_a, int channel);
  void ag_psp_210_gg_wpbcx(int x_a, int zero);
  void ax_psp_210_gu_wpgd(int x_a);
  void ac_psp_210_gu_wpgd(int x_a, int channel);
  void ag_psp_210_gu_wpgd(int x_a, int zero);
  void ax_psp_210_gu_wpgs(int x_a);
  void ac_psp_210_gu_wpgs(int x_a, int channel);
  void ag_psp_210_gu_wpgs(int x_a, int zero);
  void ax_psp_210_gu_wpgb(int x_a);
  void ac_psp_210_gu_wpgb(int x_a, int channel);
  void ag_psp_210_gu_wpgb(int x_a, int zero);
  void ax_psp_210_gc_wpgd(int x_a);
  void ac_psp_210_gc_wpgd(int x_a, int channel);
  void ag_psp_210_gc_wpgd(int x_a, int zero);
  void ax_psp_210_gc_wpgs(int x_a);
  void ac_psp_210_gc_wpgs(int x_a, int channel);
  void ag_psp_210_gc_wpgs(int x_a, int zero);
  void ax_psp_210_gc_wpgb(int x_a);
  void ac_psp_210_gc_wpgb(int x_a, int channel);
  void ag_psp_210_gc_wpgb(int x_a, int zero);
  void ax_psp_210_gdx_wpgux(int x_a);
  void ac_psp_210_gdx_wpgux(int x_a, int channel);
  void ag_psp_210_gdx_wpgux(int x_a, int zero);
  void ax_psp_210_gdx_wpgcx(int x_a);
  void ac_psp_210_gdx_wpgcx(int x_a, int channel);
  void ag_psp_210_gdx_wpgcx(int x_a, int zero);
  void ax_psp_210_gsx_wpgux(int x_a);
  void ac_psp_210_gsx_wpgux(int x_a, int channel);
  void ag_psp_210_gsx_wpgux(int x_a, int zero);
  void ax_psp_210_gsx_wpgcx(int x_a);
  void ac_psp_210_gsx_wpgcx(int x_a, int channel);
  void ag_psp_210_gsx_wpgcx(int x_a, int zero);
  void ax_psp_210_gbx_wpgux(int x_a);
  void ac_psp_210_gbx_wpgux(int x_a, int channel);
  void ag_psp_210_gbx_wpgux(int x_a, int zero);
  void ax_psp_210_gbx_wpgcx(int x_a);
  void ac_psp_210_gbx_wpgcx(int x_a, int channel);
  void ag_psp_210_gbx_wpgcx(int x_a, int zero);
  void ax_psp_210_du_wpdd(int x_a);
  void ac_psp_210_du_wpdd(int x_a, int channel);
  void ag_psp_210_du_wpdd(int x_a, int zero);
  void ax_psp_210_du_wpds(int x_a);
  void ac_psp_210_du_wpds(int x_a, int channel);
  void ag_psp_210_du_wpds(int x_a, int zero);
  void ax_psp_210_du_wpdb(int x_a);
  void ac_psp_210_du_wpdb(int x_a, int channel);
  void ag_psp_210_du_wpdb(int x_a, int zero);
  void ax_psp_210_dc_wpdd(int x_a);
  void ac_psp_210_dc_wpdd(int x_a, int channel);
  void ag_psp_210_dc_wpdd(int x_a, int zero);
  void ax_psp_210_dc_wpds(int x_a);
  void ac_psp_210_dc_wpds(int x_a, int channel);
  void ag_psp_210_dc_wpds(int x_a, int zero);
  void ax_psp_210_dc_wpdb(int x_a);
  void ac_psp_210_dc_wpdb(int x_a, int channel);
  void ag_psp_210_dc_wpdb(int x_a, int zero);
  void ax_psp_210_ddx_wpdux(int x_a);
  void ac_psp_210_ddx_wpdux(int x_a, int channel);
  void ag_psp_210_ddx_wpdux(int x_a, int zero);
  void ax_psp_210_ddx_wpdcx(int x_a);
  void ac_psp_210_ddx_wpdcx(int x_a, int channel);
  void ag_psp_210_ddx_wpdcx(int x_a, int zero);
  void ax_psp_210_ddx_wpsux(int x_a);
  void ac_psp_210_ddx_wpsux(int x_a, int channel);
  void ag_psp_210_ddx_wpsux(int x_a, int zero);
  void ax_psp_210_ddx_wpscx(int x_a);
  void ac_psp_210_ddx_wpscx(int x_a, int channel);
  void ag_psp_210_ddx_wpscx(int x_a, int zero);
  void ax_psp_210_ddx_wpbux(int x_a);
  void ac_psp_210_ddx_wpbux(int x_a, int channel);
  void ag_psp_210_ddx_wpbux(int x_a, int zero);
  void ax_psp_210_ddx_wpbcx(int x_a);
  void ac_psp_210_ddx_wpbcx(int x_a, int channel);
  void ag_psp_210_ddx_wpbcx(int x_a, int zero);
  void ax_psp_210_dsx_wpdux(int x_a);
  void ac_psp_210_dsx_wpdux(int x_a, int channel);
  void ag_psp_210_dsx_wpdux(int x_a, int zero);
  void ax_psp_210_dsx_wpdcx(int x_a);
  void ac_psp_210_dsx_wpdcx(int x_a, int channel);
  void ag_psp_210_dsx_wpdcx(int x_a, int zero);
  void ax_psp_210_dbx_wpdux(int x_a);
  void ac_psp_210_dbx_wpdux(int x_a, int channel);
  void ag_psp_210_dbx_wpdux(int x_a, int zero);
  void ax_psp_210_dbx_wpdcx(int x_a);
  void ac_psp_210_dbx_wpdcx(int x_a, int channel);
  void ag_psp_210_dbx_wpdcx(int x_a, int zero);
  void ax_psp_210_uu_wpdu(int x_a);
  void ac_psp_210_uu_wpdu(int x_a, int channel);
  void ag_psp_210_uu_wpdu(int x_a, int zero);
  void ax_psp_210_uu_wpus(int x_a);
  void ac_psp_210_uu_wpus(int x_a, int channel);
  void ag_psp_210_uu_wpus(int x_a, int zero);
  void ax_psp_210_uu_wpub(int x_a);
  void ac_psp_210_uu_wpub(int x_a, int channel);
  void ag_psp_210_uu_wpub(int x_a, int zero);
  void ax_psp_210_us_wpds(int x_a);
  void ac_psp_210_us_wpds(int x_a, int channel);
  void ag_psp_210_us_wpds(int x_a, int zero);
  void ax_psp_210_us_wpss(int x_a);
  void ac_psp_210_us_wpss(int x_a, int channel);
  void ag_psp_210_us_wpss(int x_a, int zero);
  void ax_psp_210_us_wpsb(int x_a);
  void ac_psp_210_us_wpsb(int x_a, int channel);
  void ag_psp_210_us_wpsb(int x_a, int zero);
  void ax_psp_210_uc_wpdu(int x_a);
  void ac_psp_210_uc_wpdu(int x_a, int channel);
  void ag_psp_210_uc_wpdu(int x_a, int zero);
  void ax_psp_210_uc_wpdc(int x_a);
  void ac_psp_210_uc_wpdc(int x_a, int channel);
  void ag_psp_210_uc_wpdc(int x_a, int zero);
  void ax_psp_210_uc_wpus(int x_a);
  void ac_psp_210_uc_wpus(int x_a, int channel);
  void ag_psp_210_uc_wpus(int x_a, int zero);
  void ax_psp_210_uc_wpub(int x_a);
  void ac_psp_210_uc_wpub(int x_a, int channel);
  void ag_psp_210_uc_wpub(int x_a, int zero);
  void ax_psp_210_uc_wpsc(int x_a);
  void ac_psp_210_uc_wpsc(int x_a, int channel);
  void ag_psp_210_uc_wpsc(int x_a, int zero);
  void ax_psp_210_uc_wpcb(int x_a);
  void ac_psp_210_uc_wpcb(int x_a, int channel);
  void ag_psp_210_uc_wpcb(int x_a, int zero);
  void ax_psp_210_ub_wpdb(int x_a);
  void ac_psp_210_ub_wpdb(int x_a, int channel);
  void ag_psp_210_ub_wpdb(int x_a, int zero);
  void ax_psp_210_ub_wpsb(int x_a);
  void ac_psp_210_ub_wpsb(int x_a, int channel);
  void ag_psp_210_ub_wpsb(int x_a, int zero);
  void ax_psp_210_ub_wpbb(int x_a);
  void ac_psp_210_ub_wpbb(int x_a, int channel);
  void ag_psp_210_ub_wpbb(int x_a, int zero);
  void ax_psp_210_udx_wpgg(int x_a);
  void ac_psp_210_udx_wpgg(int x_a, int channel);
  void ag_psp_210_udx_wpgg(int x_a, int zero);
  void ax_psp_210_udx_wpddx(int x_a);
  void ac_psp_210_udx_wpddx(int x_a, int channel);
  void ag_psp_210_udx_wpddx(int x_a, int zero);
  void ax_psp_210_udx_wpuux(int x_a);
  void ac_psp_210_udx_wpuux(int x_a, int channel);
  void ag_psp_210_udx_wpuux(int x_a, int zero);
  void ax_psp_210_udx_wpucx(int x_a);
  void ac_psp_210_udx_wpucx(int x_a, int channel);
  void ag_psp_210_udx_wpucx(int x_a, int zero);
  void ax_psp_210_udx_wpsdx(int x_a);
  void ac_psp_210_udx_wpsdx(int x_a, int channel);
  void ag_psp_210_udx_wpsdx(int x_a, int zero);
  void ax_psp_210_udx_wpssx(int x_a);
  void ac_psp_210_udx_wpssx(int x_a, int channel);
  void ag_psp_210_udx_wpssx(int x_a, int zero);
  void ax_psp_210_udx_wpccx(int x_a);
  void ac_psp_210_udx_wpccx(int x_a, int channel);
  void ag_psp_210_udx_wpccx(int x_a, int zero);
  void ax_psp_210_udx_wpbdx(int x_a);
  void ac_psp_210_udx_wpbdx(int x_a, int channel);
  void ag_psp_210_udx_wpbdx(int x_a, int zero);
  void ax_psp_210_udx_wpbbx(int x_a);
  void ac_psp_210_udx_wpbbx(int x_a, int channel);
  void ag_psp_210_udx_wpbbx(int x_a, int zero);
  void ax_psp_210_uux_wpdux(int x_a);
  void ac_psp_210_uux_wpdux(int x_a, int channel);
  void ag_psp_210_uux_wpdux(int x_a, int zero);
  void ax_psp_210_uux_wpdcx(int x_a);
  void ac_psp_210_uux_wpdcx(int x_a, int channel);
  void ag_psp_210_uux_wpdcx(int x_a, int zero);
  void ax_psp_210_uux_wpsux(int x_a);
  void ac_psp_210_uux_wpsux(int x_a, int channel);
  void ag_psp_210_uux_wpsux(int x_a, int zero);
  void ax_psp_210_uux_wpscx(int x_a);
  void ac_psp_210_uux_wpscx(int x_a, int channel);
  void ag_psp_210_uux_wpscx(int x_a, int zero);
  void ax_psp_210_uux_wpbux(int x_a);
  void ac_psp_210_uux_wpbux(int x_a, int channel);
  void ag_psp_210_uux_wpbux(int x_a, int zero);
  void ax_psp_210_uux_wpbcx(int x_a);
  void ac_psp_210_uux_wpbcx(int x_a, int channel);
  void ag_psp_210_uux_wpbcx(int x_a, int zero);
  void ax_psp_210_usx_wpgg(int x_a);
  void ac_psp_210_usx_wpgg(int x_a, int channel);
  void ag_psp_210_usx_wpgg(int x_a, int zero);
  void ax_psp_210_usx_wpddx(int x_a);
  void ac_psp_210_usx_wpddx(int x_a, int channel);
  void ag_psp_210_usx_wpddx(int x_a, int zero);
  void ax_psp_210_usx_wpdsx(int x_a);
  void ac_psp_210_usx_wpdsx(int x_a, int channel);
  void ag_psp_210_usx_wpdsx(int x_a, int zero);
  void ax_psp_210_usx_wpuux(int x_a);
  void ac_psp_210_usx_wpuux(int x_a, int channel);
  void ag_psp_210_usx_wpuux(int x_a, int zero);
  void ax_psp_210_usx_wpucx(int x_a);
  void ac_psp_210_usx_wpucx(int x_a, int channel);
  void ag_psp_210_usx_wpucx(int x_a, int zero);
  void ax_psp_210_usx_wpssx(int x_a);
  void ac_psp_210_usx_wpssx(int x_a, int channel);
  void ag_psp_210_usx_wpssx(int x_a, int zero);
  void ax_psp_210_usx_wpccx(int x_a);
  void ac_psp_210_usx_wpccx(int x_a, int channel);
  void ag_psp_210_usx_wpccx(int x_a, int zero);
  void ax_psp_210_usx_wpbsx(int x_a);
  void ac_psp_210_usx_wpbsx(int x_a, int channel);
  void ag_psp_210_usx_wpbsx(int x_a, int zero);
  void ax_psp_210_usx_wpbbx(int x_a);
  void ac_psp_210_usx_wpbbx(int x_a, int channel);
  void ag_psp_210_usx_wpbbx(int x_a, int zero);
  void ax_psp_210_ucx_wpdcx(int x_a);
  void ac_psp_210_ucx_wpdcx(int x_a, int channel);
  void ag_psp_210_ucx_wpdcx(int x_a, int zero);
  void ax_psp_210_ucx_wpscx(int x_a);
  void ac_psp_210_ucx_wpscx(int x_a, int channel);
  void ag_psp_210_ucx_wpscx(int x_a, int zero);
  void ax_psp_210_ucx_wpbcx(int x_a);
  void ac_psp_210_ucx_wpbcx(int x_a, int channel);
  void ag_psp_210_ucx_wpbcx(int x_a, int zero);
  void ax_psp_210_ubx_wpgg(int x_a);
  void ac_psp_210_ubx_wpgg(int x_a, int channel);
  void ag_psp_210_ubx_wpgg(int x_a, int zero);
  void ax_psp_210_ubx_wpddx(int x_a);
  void ac_psp_210_ubx_wpddx(int x_a, int channel);
  void ag_psp_210_ubx_wpddx(int x_a, int zero);
  void ax_psp_210_ubx_wpdbx(int x_a);
  void ac_psp_210_ubx_wpdbx(int x_a, int channel);
  void ag_psp_210_ubx_wpdbx(int x_a, int zero);
  void ax_psp_210_ubx_wpuux(int x_a);
  void ac_psp_210_ubx_wpuux(int x_a, int channel);
  void ag_psp_210_ubx_wpuux(int x_a, int zero);
  void ax_psp_210_ubx_wpucx(int x_a);
  void ac_psp_210_ubx_wpucx(int x_a, int channel);
  void ag_psp_210_ubx_wpucx(int x_a, int zero);
  void ax_psp_210_ubx_wpssx(int x_a);
  void ac_psp_210_ubx_wpssx(int x_a, int channel);
  void ag_psp_210_ubx_wpssx(int x_a, int zero);
  void ax_psp_210_ubx_wpsbx(int x_a);
  void ac_psp_210_ubx_wpsbx(int x_a, int channel);
  void ag_psp_210_ubx_wpsbx(int x_a, int zero);
  void ax_psp_210_ubx_wpccx(int x_a);
  void ac_psp_210_ubx_wpccx(int x_a, int channel);
  void ag_psp_210_ubx_wpccx(int x_a, int zero);
  void ax_psp_210_ubx_wpbbx(int x_a);
  void ac_psp_210_ubx_wpbbx(int x_a, int channel);
  void ag_psp_210_ubx_wpbbx(int x_a, int zero);
  void ax_psp_210_sc_wpds(int x_a);
  void ac_psp_210_sc_wpds(int x_a, int channel);
  void ag_psp_210_sc_wpds(int x_a, int zero);
  void ax_psp_210_sc_wpss(int x_a);
  void ac_psp_210_sc_wpss(int x_a, int channel);
  void ag_psp_210_sc_wpss(int x_a, int zero);
  void ax_psp_210_sc_wpsb(int x_a);
  void ac_psp_210_sc_wpsb(int x_a, int channel);
  void ag_psp_210_sc_wpsb(int x_a, int zero);
  void ax_psp_210_sdx_wpsux(int x_a);
  void ac_psp_210_sdx_wpsux(int x_a, int channel);
  void ag_psp_210_sdx_wpsux(int x_a, int zero);
  void ax_psp_210_sdx_wpscx(int x_a);
  void ac_psp_210_sdx_wpscx(int x_a, int channel);
  void ag_psp_210_sdx_wpscx(int x_a, int zero);
  void ax_psp_210_ssx_wpdux(int x_a);
  void ac_psp_210_ssx_wpdux(int x_a, int channel);
  void ag_psp_210_ssx_wpdux(int x_a, int zero);
  void ax_psp_210_ssx_wpdcx(int x_a);
  void ac_psp_210_ssx_wpdcx(int x_a, int channel);
  void ag_psp_210_ssx_wpdcx(int x_a, int zero);
  void ax_psp_210_ssx_wpsux(int x_a);
  void ac_psp_210_ssx_wpsux(int x_a, int channel);
  void ag_psp_210_ssx_wpsux(int x_a, int zero);
  void ax_psp_210_ssx_wpscx(int x_a);
  void ac_psp_210_ssx_wpscx(int x_a, int channel);
  void ag_psp_210_ssx_wpscx(int x_a, int zero);
  void ax_psp_210_ssx_wpbux(int x_a);
  void ac_psp_210_ssx_wpbux(int x_a, int channel);
  void ag_psp_210_ssx_wpbux(int x_a, int zero);
  void ax_psp_210_ssx_wpbcx(int x_a);
  void ac_psp_210_ssx_wpbcx(int x_a, int channel);
  void ag_psp_210_ssx_wpbcx(int x_a, int zero);
  void ax_psp_210_sbx_wpsux(int x_a);
  void ac_psp_210_sbx_wpsux(int x_a, int channel);
  void ag_psp_210_sbx_wpsux(int x_a, int zero);
  void ax_psp_210_sbx_wpscx(int x_a);
  void ac_psp_210_sbx_wpscx(int x_a, int channel);
  void ag_psp_210_sbx_wpscx(int x_a, int zero);
  void ax_psp_210_cc_wpdc(int x_a);
  void ac_psp_210_cc_wpdc(int x_a, int channel);
  void ag_psp_210_cc_wpdc(int x_a, int zero);
  void ax_psp_210_cc_wpsc(int x_a);
  void ac_psp_210_cc_wpsc(int x_a, int channel);
  void ag_psp_210_cc_wpsc(int x_a, int zero);
  void ax_psp_210_cc_wpcb(int x_a);
  void ac_psp_210_cc_wpcb(int x_a, int channel);
  void ag_psp_210_cc_wpcb(int x_a, int zero);
  void ax_psp_210_cb_wpdb(int x_a);
  void ac_psp_210_cb_wpdb(int x_a, int channel);
  void ag_psp_210_cb_wpdb(int x_a, int zero);
  void ax_psp_210_cb_wpsb(int x_a);
  void ac_psp_210_cb_wpsb(int x_a, int channel);
  void ag_psp_210_cb_wpsb(int x_a, int zero);
  void ax_psp_210_cb_wpbb(int x_a);
  void ac_psp_210_cb_wpbb(int x_a, int channel);
  void ag_psp_210_cb_wpbb(int x_a, int zero);
  void ax_psp_210_cdx_wpgg(int x_a);
  void ac_psp_210_cdx_wpgg(int x_a, int channel);
  void ag_psp_210_cdx_wpgg(int x_a, int zero);
  void ax_psp_210_cdx_wpddx(int x_a);
  void ac_psp_210_cdx_wpddx(int x_a, int channel);
  void ag_psp_210_cdx_wpddx(int x_a, int zero);
  void ax_psp_210_cdx_wpuux(int x_a);
  void ac_psp_210_cdx_wpuux(int x_a, int channel);
  void ag_psp_210_cdx_wpuux(int x_a, int zero);
  void ax_psp_210_cdx_wpsdx(int x_a);
  void ac_psp_210_cdx_wpsdx(int x_a, int channel);
  void ag_psp_210_cdx_wpsdx(int x_a, int zero);
  void ax_psp_210_cdx_wpssx(int x_a);
  void ac_psp_210_cdx_wpssx(int x_a, int channel);
  void ag_psp_210_cdx_wpssx(int x_a, int zero);
  void ax_psp_210_cdx_wpcux(int x_a);
  void ac_psp_210_cdx_wpcux(int x_a, int channel);
  void ag_psp_210_cdx_wpcux(int x_a, int zero);
  void ax_psp_210_cdx_wpccx(int x_a);
  void ac_psp_210_cdx_wpccx(int x_a, int channel);
  void ag_psp_210_cdx_wpccx(int x_a, int zero);
  void ax_psp_210_cdx_wpbdx(int x_a);
  void ac_psp_210_cdx_wpbdx(int x_a, int channel);
  void ag_psp_210_cdx_wpbdx(int x_a, int zero);
  void ax_psp_210_cdx_wpbbx(int x_a);
  void ac_psp_210_cdx_wpbbx(int x_a, int channel);
  void ag_psp_210_cdx_wpbbx(int x_a, int zero);
  void ax_psp_210_cux_wpdux(int x_a);
  void ac_psp_210_cux_wpdux(int x_a, int channel);
  void ag_psp_210_cux_wpdux(int x_a, int zero);
  void ax_psp_210_cux_wpsux(int x_a);
  void ac_psp_210_cux_wpsux(int x_a, int channel);
  void ag_psp_210_cux_wpsux(int x_a, int zero);
  void ax_psp_210_cux_wpbux(int x_a);
  void ac_psp_210_cux_wpbux(int x_a, int channel);
  void ag_psp_210_cux_wpbux(int x_a, int zero);
  void ax_psp_210_csx_wpgg(int x_a);
  void ac_psp_210_csx_wpgg(int x_a, int channel);
  void ag_psp_210_csx_wpgg(int x_a, int zero);
  void ax_psp_210_csx_wpddx(int x_a);
  void ac_psp_210_csx_wpddx(int x_a, int channel);
  void ag_psp_210_csx_wpddx(int x_a, int zero);
  void ax_psp_210_csx_wpdsx(int x_a);
  void ac_psp_210_csx_wpdsx(int x_a, int channel);
  void ag_psp_210_csx_wpdsx(int x_a, int zero);
  void ax_psp_210_csx_wpuux(int x_a);
  void ac_psp_210_csx_wpuux(int x_a, int channel);
  void ag_psp_210_csx_wpuux(int x_a, int zero);
  void ax_psp_210_csx_wpssx(int x_a);
  void ac_psp_210_csx_wpssx(int x_a, int channel);
  void ag_psp_210_csx_wpssx(int x_a, int zero);
  void ax_psp_210_csx_wpcux(int x_a);
  void ac_psp_210_csx_wpcux(int x_a, int channel);
  void ag_psp_210_csx_wpcux(int x_a, int zero);
  void ax_psp_210_csx_wpccx(int x_a);
  void ac_psp_210_csx_wpccx(int x_a, int channel);
  void ag_psp_210_csx_wpccx(int x_a, int zero);
  void ax_psp_210_csx_wpbsx(int x_a);
  void ac_psp_210_csx_wpbsx(int x_a, int channel);
  void ag_psp_210_csx_wpbsx(int x_a, int zero);
  void ax_psp_210_csx_wpbbx(int x_a);
  void ac_psp_210_csx_wpbbx(int x_a, int channel);
  void ag_psp_210_csx_wpbbx(int x_a, int zero);
  void ax_psp_210_ccx_wpdux(int x_a);
  void ac_psp_210_ccx_wpdux(int x_a, int channel);
  void ag_psp_210_ccx_wpdux(int x_a, int zero);
  void ax_psp_210_ccx_wpdcx(int x_a);
  void ac_psp_210_ccx_wpdcx(int x_a, int channel);
  void ag_psp_210_ccx_wpdcx(int x_a, int zero);
  void ax_psp_210_ccx_wpsux(int x_a);
  void ac_psp_210_ccx_wpsux(int x_a, int channel);
  void ag_psp_210_ccx_wpsux(int x_a, int zero);
  void ax_psp_210_ccx_wpscx(int x_a);
  void ac_psp_210_ccx_wpscx(int x_a, int channel);
  void ag_psp_210_ccx_wpscx(int x_a, int zero);
  void ax_psp_210_ccx_wpbux(int x_a);
  void ac_psp_210_ccx_wpbux(int x_a, int channel);
  void ag_psp_210_ccx_wpbux(int x_a, int zero);
  void ax_psp_210_ccx_wpbcx(int x_a);
  void ac_psp_210_ccx_wpbcx(int x_a, int channel);
  void ag_psp_210_ccx_wpbcx(int x_a, int zero);
  void ax_psp_210_cbx_wpgg(int x_a);
  void ac_psp_210_cbx_wpgg(int x_a, int channel);
  void ag_psp_210_cbx_wpgg(int x_a, int zero);
  void ax_psp_210_cbx_wpddx(int x_a);
  void ac_psp_210_cbx_wpddx(int x_a, int channel);
  void ag_psp_210_cbx_wpddx(int x_a, int zero);
  void ax_psp_210_cbx_wpdbx(int x_a);
  void ac_psp_210_cbx_wpdbx(int x_a, int channel);
  void ag_psp_210_cbx_wpdbx(int x_a, int zero);
  void ax_psp_210_cbx_wpuux(int x_a);
  void ac_psp_210_cbx_wpuux(int x_a, int channel);
  void ag_psp_210_cbx_wpuux(int x_a, int zero);
  void ax_psp_210_cbx_wpssx(int x_a);
  void ac_psp_210_cbx_wpssx(int x_a, int channel);
  void ag_psp_210_cbx_wpssx(int x_a, int zero);
  void ax_psp_210_cbx_wpsbx(int x_a);
  void ac_psp_210_cbx_wpsbx(int x_a, int channel);
  void ag_psp_210_cbx_wpsbx(int x_a, int zero);
  void ax_psp_210_cbx_wpcux(int x_a);
  void ac_psp_210_cbx_wpcux(int x_a, int channel);
  void ag_psp_210_cbx_wpcux(int x_a, int zero);
  void ax_psp_210_cbx_wpccx(int x_a);
  void ac_psp_210_cbx_wpccx(int x_a, int channel);
  void ag_psp_210_cbx_wpccx(int x_a, int zero);
  void ax_psp_210_cbx_wpbbx(int x_a);
  void ac_psp_210_cbx_wpbbx(int x_a, int channel);
  void ag_psp_210_cbx_wpbbx(int x_a, int zero);
  void ax_psp_210_bdx_wpbux(int x_a);
  void ac_psp_210_bdx_wpbux(int x_a, int channel);
  void ag_psp_210_bdx_wpbux(int x_a, int zero);
  void ax_psp_210_bdx_wpbcx(int x_a);
  void ac_psp_210_bdx_wpbcx(int x_a, int channel);
  void ag_psp_210_bdx_wpbcx(int x_a, int zero);
  void ax_psp_210_bsx_wpbux(int x_a);
  void ac_psp_210_bsx_wpbux(int x_a, int channel);
  void ag_psp_210_bsx_wpbux(int x_a, int zero);
  void ax_psp_210_bsx_wpbcx(int x_a);
  void ac_psp_210_bsx_wpbcx(int x_a, int channel);
  void ag_psp_210_bsx_wpbcx(int x_a, int zero);
  void ax_psp_210_bbx_wpdux(int x_a);
  void ac_psp_210_bbx_wpdux(int x_a, int channel);
  void ag_psp_210_bbx_wpdux(int x_a, int zero);
  void ax_psp_210_bbx_wpdcx(int x_a);
  void ac_psp_210_bbx_wpdcx(int x_a, int channel);
  void ag_psp_210_bbx_wpdcx(int x_a, int zero);
  void ax_psp_210_bbx_wpsux(int x_a);
  void ac_psp_210_bbx_wpsux(int x_a, int channel);
  void ag_psp_210_bbx_wpsux(int x_a, int zero);
  void ax_psp_210_bbx_wpscx(int x_a);
  void ac_psp_210_bbx_wpscx(int x_a, int channel);
  void ag_psp_210_bbx_wpscx(int x_a, int zero);
  void ax_psp_210_bbx_wpbux(int x_a);
  void ac_psp_210_bbx_wpbux(int x_a, int channel);
  void ag_psp_210_bbx_wpbux(int x_a, int zero);
  void ax_psp_210_bbx_wpbcx(int x_a);
  void ac_psp_210_bbx_wpbcx(int x_a, int channel);
  void ag_psp_210_bbx_wpbcx(int x_a, int zero);
  void ax_psp_210_dxdx_wpdxux(int x_a);
  void ac_psp_210_dxdx_wpdxux(int x_a, int channel);
  void ag_psp_210_dxdx_wpdxux(int x_a, int zero);
  void ax_psp_210_dxdx_wpdxcx(int x_a);
  void ac_psp_210_dxdx_wpdxcx(int x_a, int channel);
  void ag_psp_210_dxdx_wpdxcx(int x_a, int zero);
  void ax_psp_210_dxux_wpuxux(int x_a);
  void ac_psp_210_dxux_wpuxux(int x_a, int channel);
  void ag_psp_210_dxux_wpuxux(int x_a, int zero);
  void ax_psp_210_dxux_wpuxcx(int x_a);
  void ac_psp_210_dxux_wpuxcx(int x_a, int channel);
  void ag_psp_210_dxux_wpuxcx(int x_a, int zero);
  void ax_psp_210_dxsx_wpdxux(int x_a);
  void ac_psp_210_dxsx_wpdxux(int x_a, int channel);
  void ag_psp_210_dxsx_wpdxux(int x_a, int zero);
  void ax_psp_210_dxsx_wpdxcx(int x_a);
  void ac_psp_210_dxsx_wpdxcx(int x_a, int channel);
  void ag_psp_210_dxsx_wpdxcx(int x_a, int zero);
  void ax_psp_210_dxsx_wpuxsx(int x_a);
  void ac_psp_210_dxsx_wpuxsx(int x_a, int channel);
  void ag_psp_210_dxsx_wpuxsx(int x_a, int zero);
  void ax_psp_210_dxsx_wpsxcx(int x_a);
  void ac_psp_210_dxsx_wpsxcx(int x_a, int channel);
  void ag_psp_210_dxsx_wpsxcx(int x_a, int zero);
  void ax_psp_210_dxcx_wpuxcx(int x_a);
  void ac_psp_210_dxcx_wpuxcx(int x_a, int channel);
  void ag_psp_210_dxcx_wpuxcx(int x_a, int zero);
  void ax_psp_210_dxcx_wpcxcx(int x_a);
  void ac_psp_210_dxcx_wpcxcx(int x_a, int channel);
  void ag_psp_210_dxcx_wpcxcx(int x_a, int zero);
  void ax_psp_210_dxbx_wpdxux(int x_a);
  void ac_psp_210_dxbx_wpdxux(int x_a, int channel);
  void ag_psp_210_dxbx_wpdxux(int x_a, int zero);
  void ax_psp_210_dxbx_wpdxcx(int x_a);
  void ac_psp_210_dxbx_wpdxcx(int x_a, int channel);
  void ag_psp_210_dxbx_wpdxcx(int x_a, int zero);
  void ax_psp_210_dxbx_wpuxbx(int x_a);
  void ac_psp_210_dxbx_wpuxbx(int x_a, int channel);
  void ag_psp_210_dxbx_wpuxbx(int x_a, int zero);
  void ax_psp_210_dxbx_wpcxbx(int x_a);
  void ac_psp_210_dxbx_wpcxbx(int x_a, int channel);
  void ag_psp_210_dxbx_wpcxbx(int x_a, int zero);
  void ax_psp_210_uxsx_wpuxux(int x_a);
  void ac_psp_210_uxsx_wpuxux(int x_a, int channel);
  void ag_psp_210_uxsx_wpuxux(int x_a, int zero);
  void ax_psp_210_uxsx_wpuxcx(int x_a);
  void ac_psp_210_uxsx_wpuxcx(int x_a, int channel);
  void ag_psp_210_uxsx_wpuxcx(int x_a, int zero);
  void ax_psp_210_uxbx_wpuxux(int x_a);
  void ac_psp_210_uxbx_wpuxux(int x_a, int channel);
  void ag_psp_210_uxbx_wpuxux(int x_a, int zero);
  void ax_psp_210_uxbx_wpuxcx(int x_a);
  void ac_psp_210_uxbx_wpuxcx(int x_a, int channel);
  void ag_psp_210_uxbx_wpuxcx(int x_a, int zero);
  void ax_psp_210_sxsx_wpuxsx(int x_a);
  void ac_psp_210_sxsx_wpuxsx(int x_a, int channel);
  void ag_psp_210_sxsx_wpuxsx(int x_a, int zero);
  void ax_psp_210_sxsx_wpsxcx(int x_a);
  void ac_psp_210_sxsx_wpsxcx(int x_a, int channel);
  void ag_psp_210_sxsx_wpsxcx(int x_a, int zero);
  void ax_psp_210_sxcx_wpuxcx(int x_a);
  void ac_psp_210_sxcx_wpuxcx(int x_a, int channel);
  void ag_psp_210_sxcx_wpuxcx(int x_a, int zero);
  void ax_psp_210_sxcx_wpcxcx(int x_a);
  void ac_psp_210_sxcx_wpcxcx(int x_a, int channel);
  void ag_psp_210_sxcx_wpcxcx(int x_a, int zero);
  void ax_psp_210_sxbx_wpuxsx(int x_a);
  void ac_psp_210_sxbx_wpuxsx(int x_a, int channel);
  void ag_psp_210_sxbx_wpuxsx(int x_a, int zero);
  void ax_psp_210_sxbx_wpuxbx(int x_a);
  void ac_psp_210_sxbx_wpuxbx(int x_a, int channel);
  void ag_psp_210_sxbx_wpuxbx(int x_a, int zero);
  void ax_psp_210_sxbx_wpsxcx(int x_a);
  void ac_psp_210_sxbx_wpsxcx(int x_a, int channel);
  void ag_psp_210_sxbx_wpsxcx(int x_a, int zero);
  void ax_psp_210_sxbx_wpcxbx(int x_a);
  void ac_psp_210_sxbx_wpcxbx(int x_a, int channel);
  void ag_psp_210_sxbx_wpcxbx(int x_a, int zero);
  void ax_psp_210_cxbx_wpuxcx(int x_a);
  void ac_psp_210_cxbx_wpuxcx(int x_a, int channel);
  void ag_psp_210_cxbx_wpuxcx(int x_a, int zero);
  void ax_psp_210_cxbx_wpcxcx(int x_a);
  void ac_psp_210_cxbx_wpcxcx(int x_a, int channel);
  void ag_psp_210_cxbx_wpcxcx(int x_a, int zero);
  void ax_psp_210_bxbx_wpuxbx(int x_a);
  void ac_psp_210_bxbx_wpuxbx(int x_a, int channel);
  void ag_psp_210_bxbx_wpuxbx(int x_a, int zero);
  void ax_psp_210_bxbx_wpcxbx(int x_a);
  void ac_psp_210_bxbx_wpcxbx(int x_a, int channel);
  void ag_psp_210_bxbx_wpcxbx(int x_a, int zero);

};
#endif
