#ifndef ppwx01_OBSERVABLE_SET_HPP
#define ppwx01_OBSERVABLE_SET_HPP

using namespace std;

class ppwx01_observable_set : public observable_set {
private:

public:
  ppwx01_observable_set(){}
  ~ppwx01_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
