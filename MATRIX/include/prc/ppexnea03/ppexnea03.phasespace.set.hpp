#ifndef ppexnea03_PHASESPACE_SET_HPP
#define ppexnea03_PHASESPACE_SET_HPP

using namespace std;

class ppexnea03_phasespace_set : public phasespace_set {
private:

public:
  ppexnea03_phasespace_set(){}
  ~ppexnea03_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_030_udx_epvea(int x_a);
  void ac_psp_030_udx_epvea(int x_a, int channel);
  void ag_psp_030_udx_epvea(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_130_gu_epvead(int x_a);
  void ac_psp_130_gu_epvead(int x_a, int channel);
  void ag_psp_130_gu_epvead(int x_a, int zero);
  void ax_psp_130_gdx_epveaux(int x_a);
  void ac_psp_130_gdx_epveaux(int x_a, int channel);
  void ag_psp_130_gdx_epveaux(int x_a, int zero);
  void ax_psp_130_udx_epveag(int x_a);
  void ac_psp_130_udx_epveag(int x_a, int channel);
  void ag_psp_130_udx_epveag(int x_a, int zero);
  void ax_psp_040_ua_epvead(int x_a);
  void ac_psp_040_ua_epvead(int x_a, int channel);
  void ag_psp_040_ua_epvead(int x_a, int zero);
  void ax_psp_040_dxa_epveaux(int x_a);
  void ac_psp_040_dxa_epveaux(int x_a, int channel);
  void ag_psp_040_dxa_epveaux(int x_a, int zero);
  void ax_psp_040_udx_epveaa(int x_a);
  void ac_psp_040_udx_epveaa(int x_a, int channel);
  void ag_psp_040_udx_epveaa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_230_gg_epveadux(int x_a);
  void ac_psp_230_gg_epveadux(int x_a, int channel);
  void ag_psp_230_gg_epveadux(int x_a, int zero);
  void ax_psp_230_gu_epveagd(int x_a);
  void ac_psp_230_gu_epveagd(int x_a, int channel);
  void ag_psp_230_gu_epveagd(int x_a, int zero);
  void ax_psp_230_gdx_epveagux(int x_a);
  void ac_psp_230_gdx_epveagux(int x_a, int channel);
  void ag_psp_230_gdx_epveagux(int x_a, int zero);
  void ax_psp_230_du_epveadd(int x_a);
  void ac_psp_230_du_epveadd(int x_a, int channel);
  void ag_psp_230_du_epveadd(int x_a, int zero);
  void ax_psp_230_dc_epveads(int x_a);
  void ac_psp_230_dc_epveads(int x_a, int channel);
  void ag_psp_230_dc_epveads(int x_a, int zero);
  void ax_psp_230_ddx_epveadux(int x_a);
  void ac_psp_230_ddx_epveadux(int x_a, int channel);
  void ag_psp_230_ddx_epveadux(int x_a, int zero);
  void ax_psp_230_ddx_epveascx(int x_a);
  void ac_psp_230_ddx_epveascx(int x_a, int channel);
  void ag_psp_230_ddx_epveascx(int x_a, int zero);
  void ax_psp_230_dsx_epveadcx(int x_a);
  void ac_psp_230_dsx_epveadcx(int x_a, int channel);
  void ag_psp_230_dsx_epveadcx(int x_a, int zero);
  void ax_psp_230_uu_epveadu(int x_a);
  void ac_psp_230_uu_epveadu(int x_a, int channel);
  void ag_psp_230_uu_epveadu(int x_a, int zero);
  void ax_psp_230_uc_epveadc(int x_a);
  void ac_psp_230_uc_epveadc(int x_a, int channel);
  void ag_psp_230_uc_epveadc(int x_a, int zero);
  void ax_psp_230_udx_epveagg(int x_a);
  void ac_psp_230_udx_epveagg(int x_a, int channel);
  void ag_psp_230_udx_epveagg(int x_a, int zero);
  void ax_psp_230_udx_epveaddx(int x_a);
  void ac_psp_230_udx_epveaddx(int x_a, int channel);
  void ag_psp_230_udx_epveaddx(int x_a, int zero);
  void ax_psp_230_udx_epveauux(int x_a);
  void ac_psp_230_udx_epveauux(int x_a, int channel);
  void ag_psp_230_udx_epveauux(int x_a, int zero);
  void ax_psp_230_udx_epveassx(int x_a);
  void ac_psp_230_udx_epveassx(int x_a, int channel);
  void ag_psp_230_udx_epveassx(int x_a, int zero);
  void ax_psp_230_udx_epveaccx(int x_a);
  void ac_psp_230_udx_epveaccx(int x_a, int channel);
  void ag_psp_230_udx_epveaccx(int x_a, int zero);
  void ax_psp_230_uux_epveadux(int x_a);
  void ac_psp_230_uux_epveadux(int x_a, int channel);
  void ag_psp_230_uux_epveadux(int x_a, int zero);
  void ax_psp_230_uux_epveascx(int x_a);
  void ac_psp_230_uux_epveascx(int x_a, int channel);
  void ag_psp_230_uux_epveascx(int x_a, int zero);
  void ax_psp_230_usx_epveadsx(int x_a);
  void ac_psp_230_usx_epveadsx(int x_a, int channel);
  void ag_psp_230_usx_epveadsx(int x_a, int zero);
  void ax_psp_230_usx_epveaucx(int x_a);
  void ac_psp_230_usx_epveaucx(int x_a, int channel);
  void ag_psp_230_usx_epveaucx(int x_a, int zero);
  void ax_psp_230_ucx_epveadcx(int x_a);
  void ac_psp_230_ucx_epveadcx(int x_a, int channel);
  void ag_psp_230_ucx_epveadcx(int x_a, int zero);
  void ax_psp_230_dxdx_epveadxux(int x_a);
  void ac_psp_230_dxdx_epveadxux(int x_a, int channel);
  void ag_psp_230_dxdx_epveadxux(int x_a, int zero);
  void ax_psp_230_dxux_epveauxux(int x_a);
  void ac_psp_230_dxux_epveauxux(int x_a, int channel);
  void ag_psp_230_dxux_epveauxux(int x_a, int zero);
  void ax_psp_230_dxsx_epveadxcx(int x_a);
  void ac_psp_230_dxsx_epveadxcx(int x_a, int channel);
  void ag_psp_230_dxsx_epveadxcx(int x_a, int zero);
  void ax_psp_230_dxcx_epveauxcx(int x_a);
  void ac_psp_230_dxcx_epveauxcx(int x_a, int channel);
  void ag_psp_230_dxcx_epveauxcx(int x_a, int zero);

};
#endif
