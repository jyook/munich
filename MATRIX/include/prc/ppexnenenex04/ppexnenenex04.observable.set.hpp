#ifndef ppexnenenex04_OBSERVABLE_SET_HPP
#define ppexnenenex04_OBSERVABLE_SET_HPP

using namespace std;

class ppexnenenex04_observable_set : public observable_set {
private:

public:
  ppexnenenex04_observable_set(){}
  ~ppexnenenex04_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
