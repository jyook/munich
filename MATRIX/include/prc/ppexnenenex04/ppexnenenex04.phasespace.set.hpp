#ifndef ppexnenenex04_PHASESPACE_SET_HPP
#define ppexnenenex04_PHASESPACE_SET_HPP

using namespace std;

class ppexnenenex04_phasespace_set : public phasespace_set {
private:

public:
  ppexnenenex04_phasespace_set(){}
  ~ppexnenenex04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_udx_epvevevex(int x_a);
  void ac_psp_040_udx_epvevevex(int x_a, int channel);
  void ag_psp_040_udx_epvevevex(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gu_epvevevexd(int x_a);
  void ac_psp_140_gu_epvevevexd(int x_a, int channel);
  void ag_psp_140_gu_epvevevexd(int x_a, int zero);
  void ax_psp_140_gdx_epvevevexux(int x_a);
  void ac_psp_140_gdx_epvevevexux(int x_a, int channel);
  void ag_psp_140_gdx_epvevevexux(int x_a, int zero);
  void ax_psp_140_udx_epvevevexg(int x_a);
  void ac_psp_140_udx_epvevevexg(int x_a, int channel);
  void ag_psp_140_udx_epvevevexg(int x_a, int zero);
  void ax_psp_050_ua_epvevevexd(int x_a);
  void ac_psp_050_ua_epvevevexd(int x_a, int channel);
  void ag_psp_050_ua_epvevevexd(int x_a, int zero);
  void ax_psp_050_dxa_epvevevexux(int x_a);
  void ac_psp_050_dxa_epvevevexux(int x_a, int channel);
  void ag_psp_050_dxa_epvevevexux(int x_a, int zero);
  void ax_psp_050_udx_epvevevexa(int x_a);
  void ac_psp_050_udx_epvevevexa(int x_a, int channel);
  void ag_psp_050_udx_epvevevexa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_epvevevexdux(int x_a);
  void ac_psp_240_gg_epvevevexdux(int x_a, int channel);
  void ag_psp_240_gg_epvevevexdux(int x_a, int zero);
  void ax_psp_240_gu_epvevevexgd(int x_a);
  void ac_psp_240_gu_epvevevexgd(int x_a, int channel);
  void ag_psp_240_gu_epvevevexgd(int x_a, int zero);
  void ax_psp_240_gdx_epvevevexgux(int x_a);
  void ac_psp_240_gdx_epvevevexgux(int x_a, int channel);
  void ag_psp_240_gdx_epvevevexgux(int x_a, int zero);
  void ax_psp_240_du_epvevevexdd(int x_a);
  void ac_psp_240_du_epvevevexdd(int x_a, int channel);
  void ag_psp_240_du_epvevevexdd(int x_a, int zero);
  void ax_psp_240_dc_epvevevexds(int x_a);
  void ac_psp_240_dc_epvevevexds(int x_a, int channel);
  void ag_psp_240_dc_epvevevexds(int x_a, int zero);
  void ax_psp_240_ddx_epvevevexdux(int x_a);
  void ac_psp_240_ddx_epvevevexdux(int x_a, int channel);
  void ag_psp_240_ddx_epvevevexdux(int x_a, int zero);
  void ax_psp_240_ddx_epvevevexscx(int x_a);
  void ac_psp_240_ddx_epvevevexscx(int x_a, int channel);
  void ag_psp_240_ddx_epvevevexscx(int x_a, int zero);
  void ax_psp_240_dsx_epvevevexdcx(int x_a);
  void ac_psp_240_dsx_epvevevexdcx(int x_a, int channel);
  void ag_psp_240_dsx_epvevevexdcx(int x_a, int zero);
  void ax_psp_240_uu_epvevevexdu(int x_a);
  void ac_psp_240_uu_epvevevexdu(int x_a, int channel);
  void ag_psp_240_uu_epvevevexdu(int x_a, int zero);
  void ax_psp_240_uc_epvevevexdc(int x_a);
  void ac_psp_240_uc_epvevevexdc(int x_a, int channel);
  void ag_psp_240_uc_epvevevexdc(int x_a, int zero);
  void ax_psp_240_udx_epvevevexgg(int x_a);
  void ac_psp_240_udx_epvevevexgg(int x_a, int channel);
  void ag_psp_240_udx_epvevevexgg(int x_a, int zero);
  void ax_psp_240_udx_epvevevexddx(int x_a);
  void ac_psp_240_udx_epvevevexddx(int x_a, int channel);
  void ag_psp_240_udx_epvevevexddx(int x_a, int zero);
  void ax_psp_240_udx_epvevevexuux(int x_a);
  void ac_psp_240_udx_epvevevexuux(int x_a, int channel);
  void ag_psp_240_udx_epvevevexuux(int x_a, int zero);
  void ax_psp_240_udx_epvevevexssx(int x_a);
  void ac_psp_240_udx_epvevevexssx(int x_a, int channel);
  void ag_psp_240_udx_epvevevexssx(int x_a, int zero);
  void ax_psp_240_udx_epvevevexccx(int x_a);
  void ac_psp_240_udx_epvevevexccx(int x_a, int channel);
  void ag_psp_240_udx_epvevevexccx(int x_a, int zero);
  void ax_psp_240_uux_epvevevexdux(int x_a);
  void ac_psp_240_uux_epvevevexdux(int x_a, int channel);
  void ag_psp_240_uux_epvevevexdux(int x_a, int zero);
  void ax_psp_240_uux_epvevevexscx(int x_a);
  void ac_psp_240_uux_epvevevexscx(int x_a, int channel);
  void ag_psp_240_uux_epvevevexscx(int x_a, int zero);
  void ax_psp_240_usx_epvevevexdsx(int x_a);
  void ac_psp_240_usx_epvevevexdsx(int x_a, int channel);
  void ag_psp_240_usx_epvevevexdsx(int x_a, int zero);
  void ax_psp_240_usx_epvevevexucx(int x_a);
  void ac_psp_240_usx_epvevevexucx(int x_a, int channel);
  void ag_psp_240_usx_epvevevexucx(int x_a, int zero);
  void ax_psp_240_ucx_epvevevexdcx(int x_a);
  void ac_psp_240_ucx_epvevevexdcx(int x_a, int channel);
  void ag_psp_240_ucx_epvevevexdcx(int x_a, int zero);
  void ax_psp_240_dxdx_epvevevexdxux(int x_a);
  void ac_psp_240_dxdx_epvevevexdxux(int x_a, int channel);
  void ag_psp_240_dxdx_epvevevexdxux(int x_a, int zero);
  void ax_psp_240_dxux_epvevevexuxux(int x_a);
  void ac_psp_240_dxux_epvevevexuxux(int x_a, int channel);
  void ag_psp_240_dxux_epvevevexuxux(int x_a, int zero);
  void ax_psp_240_dxsx_epvevevexdxcx(int x_a);
  void ac_psp_240_dxsx_epvevevexdxcx(int x_a, int channel);
  void ag_psp_240_dxsx_epvevevexdxcx(int x_a, int zero);
  void ax_psp_240_dxcx_epvevevexuxcx(int x_a);
  void ac_psp_240_dxcx_epvevevexuxcx(int x_a, int channel);
  void ag_psp_240_dxcx_epvevevexuxcx(int x_a, int zero);

};
#endif
