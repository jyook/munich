#ifndef ppeexh03_CONTRIBUTION_SET_HPP
#define ppeexh03_CONTRIBUTION_SET_HPP

using namespace std;

class ppeexh03_contribution_set : public contribution_set {
private:

public:
  ppeexh03_contribution_set(){}
  ~ppeexh03_contribution_set();

  void determination_subprocess_born(int i_a);
  void combination_subprocess_born(int i_a);
  void determination_subprocess_real(int i_a);
  void combination_subprocess_real(int i_a);
  void determination_subprocess_doublereal(int i_a);
  void combination_subprocess_doublereal(int i_a);

};
#endif
