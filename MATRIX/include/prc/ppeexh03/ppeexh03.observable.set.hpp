#ifndef ppeexh03_OBSERVABLE_SET_HPP
#define ppeexh03_OBSERVABLE_SET_HPP

using namespace std;

class ppeexh03_observable_set : public observable_set {
private:

public:
  ppeexh03_observable_set(){}
  ~ppeexh03_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
