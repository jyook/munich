#ifndef ppwx01nockm_PHASESPACE_SET_HPP
#define ppwx01nockm_PHASESPACE_SET_HPP

using namespace std;

class ppwx01nockm_phasespace_set : public phasespace_set {
private:

public:
  ppwx01nockm_phasespace_set(){}
  ~ppwx01nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_010_udx_wp(int x_a);
  void ac_psp_010_udx_wp(int x_a, int channel);
  void ag_psp_010_udx_wp(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_110_gu_wpd(int x_a);
  void ac_psp_110_gu_wpd(int x_a, int channel);
  void ag_psp_110_gu_wpd(int x_a, int zero);
  void ax_psp_110_gdx_wpux(int x_a);
  void ac_psp_110_gdx_wpux(int x_a, int channel);
  void ag_psp_110_gdx_wpux(int x_a, int zero);
  void ax_psp_110_udx_wpg(int x_a);
  void ac_psp_110_udx_wpg(int x_a, int channel);
  void ag_psp_110_udx_wpg(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_210_gg_wpdux(int x_a);
  void ac_psp_210_gg_wpdux(int x_a, int channel);
  void ag_psp_210_gg_wpdux(int x_a, int zero);
  void ax_psp_210_gu_wpgd(int x_a);
  void ac_psp_210_gu_wpgd(int x_a, int channel);
  void ag_psp_210_gu_wpgd(int x_a, int zero);
  void ax_psp_210_gdx_wpgux(int x_a);
  void ac_psp_210_gdx_wpgux(int x_a, int channel);
  void ag_psp_210_gdx_wpgux(int x_a, int zero);
  void ax_psp_210_du_wpdd(int x_a);
  void ac_psp_210_du_wpdd(int x_a, int channel);
  void ag_psp_210_du_wpdd(int x_a, int zero);
  void ax_psp_210_dc_wpds(int x_a);
  void ac_psp_210_dc_wpds(int x_a, int channel);
  void ag_psp_210_dc_wpds(int x_a, int zero);
  void ax_psp_210_ddx_wpdux(int x_a);
  void ac_psp_210_ddx_wpdux(int x_a, int channel);
  void ag_psp_210_ddx_wpdux(int x_a, int zero);
  void ax_psp_210_ddx_wpscx(int x_a);
  void ac_psp_210_ddx_wpscx(int x_a, int channel);
  void ag_psp_210_ddx_wpscx(int x_a, int zero);
  void ax_psp_210_dsx_wpdcx(int x_a);
  void ac_psp_210_dsx_wpdcx(int x_a, int channel);
  void ag_psp_210_dsx_wpdcx(int x_a, int zero);
  void ax_psp_210_uu_wpdu(int x_a);
  void ac_psp_210_uu_wpdu(int x_a, int channel);
  void ag_psp_210_uu_wpdu(int x_a, int zero);
  void ax_psp_210_uc_wpdc(int x_a);
  void ac_psp_210_uc_wpdc(int x_a, int channel);
  void ag_psp_210_uc_wpdc(int x_a, int zero);
  void ax_psp_210_udx_wpgg(int x_a);
  void ac_psp_210_udx_wpgg(int x_a, int channel);
  void ag_psp_210_udx_wpgg(int x_a, int zero);
  void ax_psp_210_udx_wpddx(int x_a);
  void ac_psp_210_udx_wpddx(int x_a, int channel);
  void ag_psp_210_udx_wpddx(int x_a, int zero);
  void ax_psp_210_udx_wpuux(int x_a);
  void ac_psp_210_udx_wpuux(int x_a, int channel);
  void ag_psp_210_udx_wpuux(int x_a, int zero);
  void ax_psp_210_udx_wpssx(int x_a);
  void ac_psp_210_udx_wpssx(int x_a, int channel);
  void ag_psp_210_udx_wpssx(int x_a, int zero);
  void ax_psp_210_udx_wpccx(int x_a);
  void ac_psp_210_udx_wpccx(int x_a, int channel);
  void ag_psp_210_udx_wpccx(int x_a, int zero);
  void ax_psp_210_uux_wpdux(int x_a);
  void ac_psp_210_uux_wpdux(int x_a, int channel);
  void ag_psp_210_uux_wpdux(int x_a, int zero);
  void ax_psp_210_uux_wpscx(int x_a);
  void ac_psp_210_uux_wpscx(int x_a, int channel);
  void ag_psp_210_uux_wpscx(int x_a, int zero);
  void ax_psp_210_usx_wpdsx(int x_a);
  void ac_psp_210_usx_wpdsx(int x_a, int channel);
  void ag_psp_210_usx_wpdsx(int x_a, int zero);
  void ax_psp_210_usx_wpucx(int x_a);
  void ac_psp_210_usx_wpucx(int x_a, int channel);
  void ag_psp_210_usx_wpucx(int x_a, int zero);
  void ax_psp_210_ucx_wpdcx(int x_a);
  void ac_psp_210_ucx_wpdcx(int x_a, int channel);
  void ag_psp_210_ucx_wpdcx(int x_a, int zero);
  void ax_psp_210_dxdx_wpdxux(int x_a);
  void ac_psp_210_dxdx_wpdxux(int x_a, int channel);
  void ag_psp_210_dxdx_wpdxux(int x_a, int zero);
  void ax_psp_210_dxux_wpuxux(int x_a);
  void ac_psp_210_dxux_wpuxux(int x_a, int channel);
  void ag_psp_210_dxux_wpuxux(int x_a, int zero);
  void ax_psp_210_dxsx_wpdxcx(int x_a);
  void ac_psp_210_dxsx_wpdxcx(int x_a, int channel);
  void ag_psp_210_dxsx_wpdxcx(int x_a, int zero);
  void ax_psp_210_dxcx_wpuxcx(int x_a);
  void ac_psp_210_dxcx_wpuxcx(int x_a, int channel);
  void ag_psp_210_dxcx_wpuxcx(int x_a, int zero);

};
#endif
