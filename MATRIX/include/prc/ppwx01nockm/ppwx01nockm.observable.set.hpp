#ifndef ppwx01nockm_OBSERVABLE_SET_HPP
#define ppwx01nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppwx01nockm_observable_set : public observable_set {
private:

public:
  ppwx01nockm_observable_set(){}
  ~ppwx01nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
