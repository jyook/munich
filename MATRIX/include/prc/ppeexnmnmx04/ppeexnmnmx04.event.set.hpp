#ifndef ppeexnmnmx04_EVENT_SET_HPP
#define ppeexnmnmx04_EVENT_SET_HPP

using namespace std;

class ppeexnmnmx04_event_set : public event_set {
private:

public:
  ppeexnmnmx04_event_set(){}
  ~ppeexnmnmx04_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
