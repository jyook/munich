#ifndef ppnenexh03_OBSERVABLE_SET_HPP
#define ppnenexh03_OBSERVABLE_SET_HPP

using namespace std;

class ppnenexh03_observable_set : public observable_set {
private:

public:
  ppnenexh03_observable_set(){}
  ~ppnenexh03_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
