#ifndef ppenexa03_PHASESPACE_SET_HPP
#define ppenexa03_PHASESPACE_SET_HPP

using namespace std;

class ppenexa03_phasespace_set : public phasespace_set {
private:

public:
  ppenexa03_phasespace_set(){}
  ~ppenexa03_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_030_dux_emvexa(int x_a);
  void ac_psp_030_dux_emvexa(int x_a, int channel);
  void ag_psp_030_dux_emvexa(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_130_gd_emvexau(int x_a);
  void ac_psp_130_gd_emvexau(int x_a, int channel);
  void ag_psp_130_gd_emvexau(int x_a, int zero);
  void ax_psp_130_gux_emvexadx(int x_a);
  void ac_psp_130_gux_emvexadx(int x_a, int channel);
  void ag_psp_130_gux_emvexadx(int x_a, int zero);
  void ax_psp_130_dux_emvexag(int x_a);
  void ac_psp_130_dux_emvexag(int x_a, int channel);
  void ag_psp_130_dux_emvexag(int x_a, int zero);
  void ax_psp_040_da_emvexau(int x_a);
  void ac_psp_040_da_emvexau(int x_a, int channel);
  void ag_psp_040_da_emvexau(int x_a, int zero);
  void ax_psp_040_uxa_emvexadx(int x_a);
  void ac_psp_040_uxa_emvexadx(int x_a, int channel);
  void ag_psp_040_uxa_emvexadx(int x_a, int zero);
  void ax_psp_040_dux_emvexaa(int x_a);
  void ac_psp_040_dux_emvexaa(int x_a, int channel);
  void ag_psp_040_dux_emvexaa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_230_gg_emvexaudx(int x_a);
  void ac_psp_230_gg_emvexaudx(int x_a, int channel);
  void ag_psp_230_gg_emvexaudx(int x_a, int zero);
  void ax_psp_230_gd_emvexagu(int x_a);
  void ac_psp_230_gd_emvexagu(int x_a, int channel);
  void ag_psp_230_gd_emvexagu(int x_a, int zero);
  void ax_psp_230_gux_emvexagdx(int x_a);
  void ac_psp_230_gux_emvexagdx(int x_a, int channel);
  void ag_psp_230_gux_emvexagdx(int x_a, int zero);
  void ax_psp_230_dd_emvexadu(int x_a);
  void ac_psp_230_dd_emvexadu(int x_a, int channel);
  void ag_psp_230_dd_emvexadu(int x_a, int zero);
  void ax_psp_230_du_emvexauu(int x_a);
  void ac_psp_230_du_emvexauu(int x_a, int channel);
  void ag_psp_230_du_emvexauu(int x_a, int zero);
  void ax_psp_230_ds_emvexadc(int x_a);
  void ac_psp_230_ds_emvexadc(int x_a, int channel);
  void ag_psp_230_ds_emvexadc(int x_a, int zero);
  void ax_psp_230_dc_emvexauc(int x_a);
  void ac_psp_230_dc_emvexauc(int x_a, int channel);
  void ag_psp_230_dc_emvexauc(int x_a, int zero);
  void ax_psp_230_ddx_emvexaudx(int x_a);
  void ac_psp_230_ddx_emvexaudx(int x_a, int channel);
  void ag_psp_230_ddx_emvexaudx(int x_a, int zero);
  void ax_psp_230_ddx_emvexacsx(int x_a);
  void ac_psp_230_ddx_emvexacsx(int x_a, int channel);
  void ag_psp_230_ddx_emvexacsx(int x_a, int zero);
  void ax_psp_230_dux_emvexagg(int x_a);
  void ac_psp_230_dux_emvexagg(int x_a, int channel);
  void ag_psp_230_dux_emvexagg(int x_a, int zero);
  void ax_psp_230_dux_emvexaddx(int x_a);
  void ac_psp_230_dux_emvexaddx(int x_a, int channel);
  void ag_psp_230_dux_emvexaddx(int x_a, int zero);
  void ax_psp_230_dux_emvexauux(int x_a);
  void ac_psp_230_dux_emvexauux(int x_a, int channel);
  void ag_psp_230_dux_emvexauux(int x_a, int zero);
  void ax_psp_230_dux_emvexassx(int x_a);
  void ac_psp_230_dux_emvexassx(int x_a, int channel);
  void ag_psp_230_dux_emvexassx(int x_a, int zero);
  void ax_psp_230_dux_emvexaccx(int x_a);
  void ac_psp_230_dux_emvexaccx(int x_a, int channel);
  void ag_psp_230_dux_emvexaccx(int x_a, int zero);
  void ax_psp_230_dsx_emvexausx(int x_a);
  void ac_psp_230_dsx_emvexausx(int x_a, int channel);
  void ag_psp_230_dsx_emvexausx(int x_a, int zero);
  void ax_psp_230_dcx_emvexadsx(int x_a);
  void ac_psp_230_dcx_emvexadsx(int x_a, int channel);
  void ag_psp_230_dcx_emvexadsx(int x_a, int zero);
  void ax_psp_230_dcx_emvexaucx(int x_a);
  void ac_psp_230_dcx_emvexaucx(int x_a, int channel);
  void ag_psp_230_dcx_emvexaucx(int x_a, int zero);
  void ax_psp_230_uux_emvexaudx(int x_a);
  void ac_psp_230_uux_emvexaudx(int x_a, int channel);
  void ag_psp_230_uux_emvexaudx(int x_a, int zero);
  void ax_psp_230_uux_emvexacsx(int x_a);
  void ac_psp_230_uux_emvexacsx(int x_a, int channel);
  void ag_psp_230_uux_emvexacsx(int x_a, int zero);
  void ax_psp_230_ucx_emvexausx(int x_a);
  void ac_psp_230_ucx_emvexausx(int x_a, int channel);
  void ag_psp_230_ucx_emvexausx(int x_a, int zero);
  void ax_psp_230_dxux_emvexadxdx(int x_a);
  void ac_psp_230_dxux_emvexadxdx(int x_a, int channel);
  void ag_psp_230_dxux_emvexadxdx(int x_a, int zero);
  void ax_psp_230_dxcx_emvexadxsx(int x_a);
  void ac_psp_230_dxcx_emvexadxsx(int x_a, int channel);
  void ag_psp_230_dxcx_emvexadxsx(int x_a, int zero);
  void ax_psp_230_uxux_emvexadxux(int x_a);
  void ac_psp_230_uxux_emvexadxux(int x_a, int channel);
  void ag_psp_230_uxux_emvexadxux(int x_a, int zero);
  void ax_psp_230_uxcx_emvexadxcx(int x_a);
  void ac_psp_230_uxcx_emvexadxcx(int x_a, int channel);
  void ag_psp_230_uxcx_emvexadxcx(int x_a, int zero);

};
#endif
