#ifndef ppenmnexnmx04_PHASESPACE_SET_HPP
#define ppenmnexnmx04_PHASESPACE_SET_HPP

using namespace std;

class ppenmnexnmx04_phasespace_set : public phasespace_set {
private:

public:
  ppenmnexnmx04_phasespace_set(){}
  ~ppenmnexnmx04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_dux_emvmvexvmx(int x_a);
  void ac_psp_040_dux_emvmvexvmx(int x_a, int channel);
  void ag_psp_040_dux_emvmvexvmx(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gd_emvmvexvmxu(int x_a);
  void ac_psp_140_gd_emvmvexvmxu(int x_a, int channel);
  void ag_psp_140_gd_emvmvexvmxu(int x_a, int zero);
  void ax_psp_140_gux_emvmvexvmxdx(int x_a);
  void ac_psp_140_gux_emvmvexvmxdx(int x_a, int channel);
  void ag_psp_140_gux_emvmvexvmxdx(int x_a, int zero);
  void ax_psp_140_dux_emvmvexvmxg(int x_a);
  void ac_psp_140_dux_emvmvexvmxg(int x_a, int channel);
  void ag_psp_140_dux_emvmvexvmxg(int x_a, int zero);
  void ax_psp_050_da_emvmvexvmxu(int x_a);
  void ac_psp_050_da_emvmvexvmxu(int x_a, int channel);
  void ag_psp_050_da_emvmvexvmxu(int x_a, int zero);
  void ax_psp_050_uxa_emvmvexvmxdx(int x_a);
  void ac_psp_050_uxa_emvmvexvmxdx(int x_a, int channel);
  void ag_psp_050_uxa_emvmvexvmxdx(int x_a, int zero);
  void ax_psp_050_dux_emvmvexvmxa(int x_a);
  void ac_psp_050_dux_emvmvexvmxa(int x_a, int channel);
  void ag_psp_050_dux_emvmvexvmxa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_emvmvexvmxudx(int x_a);
  void ac_psp_240_gg_emvmvexvmxudx(int x_a, int channel);
  void ag_psp_240_gg_emvmvexvmxudx(int x_a, int zero);
  void ax_psp_240_gd_emvmvexvmxgu(int x_a);
  void ac_psp_240_gd_emvmvexvmxgu(int x_a, int channel);
  void ag_psp_240_gd_emvmvexvmxgu(int x_a, int zero);
  void ax_psp_240_gux_emvmvexvmxgdx(int x_a);
  void ac_psp_240_gux_emvmvexvmxgdx(int x_a, int channel);
  void ag_psp_240_gux_emvmvexvmxgdx(int x_a, int zero);
  void ax_psp_240_dd_emvmvexvmxdu(int x_a);
  void ac_psp_240_dd_emvmvexvmxdu(int x_a, int channel);
  void ag_psp_240_dd_emvmvexvmxdu(int x_a, int zero);
  void ax_psp_240_du_emvmvexvmxuu(int x_a);
  void ac_psp_240_du_emvmvexvmxuu(int x_a, int channel);
  void ag_psp_240_du_emvmvexvmxuu(int x_a, int zero);
  void ax_psp_240_ds_emvmvexvmxdc(int x_a);
  void ac_psp_240_ds_emvmvexvmxdc(int x_a, int channel);
  void ag_psp_240_ds_emvmvexvmxdc(int x_a, int zero);
  void ax_psp_240_dc_emvmvexvmxuc(int x_a);
  void ac_psp_240_dc_emvmvexvmxuc(int x_a, int channel);
  void ag_psp_240_dc_emvmvexvmxuc(int x_a, int zero);
  void ax_psp_240_ddx_emvmvexvmxudx(int x_a);
  void ac_psp_240_ddx_emvmvexvmxudx(int x_a, int channel);
  void ag_psp_240_ddx_emvmvexvmxudx(int x_a, int zero);
  void ax_psp_240_ddx_emvmvexvmxcsx(int x_a);
  void ac_psp_240_ddx_emvmvexvmxcsx(int x_a, int channel);
  void ag_psp_240_ddx_emvmvexvmxcsx(int x_a, int zero);
  void ax_psp_240_dux_emvmvexvmxgg(int x_a);
  void ac_psp_240_dux_emvmvexvmxgg(int x_a, int channel);
  void ag_psp_240_dux_emvmvexvmxgg(int x_a, int zero);
  void ax_psp_240_dux_emvmvexvmxddx(int x_a);
  void ac_psp_240_dux_emvmvexvmxddx(int x_a, int channel);
  void ag_psp_240_dux_emvmvexvmxddx(int x_a, int zero);
  void ax_psp_240_dux_emvmvexvmxuux(int x_a);
  void ac_psp_240_dux_emvmvexvmxuux(int x_a, int channel);
  void ag_psp_240_dux_emvmvexvmxuux(int x_a, int zero);
  void ax_psp_240_dux_emvmvexvmxssx(int x_a);
  void ac_psp_240_dux_emvmvexvmxssx(int x_a, int channel);
  void ag_psp_240_dux_emvmvexvmxssx(int x_a, int zero);
  void ax_psp_240_dux_emvmvexvmxccx(int x_a);
  void ac_psp_240_dux_emvmvexvmxccx(int x_a, int channel);
  void ag_psp_240_dux_emvmvexvmxccx(int x_a, int zero);
  void ax_psp_240_dsx_emvmvexvmxusx(int x_a);
  void ac_psp_240_dsx_emvmvexvmxusx(int x_a, int channel);
  void ag_psp_240_dsx_emvmvexvmxusx(int x_a, int zero);
  void ax_psp_240_dcx_emvmvexvmxdsx(int x_a);
  void ac_psp_240_dcx_emvmvexvmxdsx(int x_a, int channel);
  void ag_psp_240_dcx_emvmvexvmxdsx(int x_a, int zero);
  void ax_psp_240_dcx_emvmvexvmxucx(int x_a);
  void ac_psp_240_dcx_emvmvexvmxucx(int x_a, int channel);
  void ag_psp_240_dcx_emvmvexvmxucx(int x_a, int zero);
  void ax_psp_240_uux_emvmvexvmxudx(int x_a);
  void ac_psp_240_uux_emvmvexvmxudx(int x_a, int channel);
  void ag_psp_240_uux_emvmvexvmxudx(int x_a, int zero);
  void ax_psp_240_uux_emvmvexvmxcsx(int x_a);
  void ac_psp_240_uux_emvmvexvmxcsx(int x_a, int channel);
  void ag_psp_240_uux_emvmvexvmxcsx(int x_a, int zero);
  void ax_psp_240_ucx_emvmvexvmxusx(int x_a);
  void ac_psp_240_ucx_emvmvexvmxusx(int x_a, int channel);
  void ag_psp_240_ucx_emvmvexvmxusx(int x_a, int zero);
  void ax_psp_240_dxux_emvmvexvmxdxdx(int x_a);
  void ac_psp_240_dxux_emvmvexvmxdxdx(int x_a, int channel);
  void ag_psp_240_dxux_emvmvexvmxdxdx(int x_a, int zero);
  void ax_psp_240_dxcx_emvmvexvmxdxsx(int x_a);
  void ac_psp_240_dxcx_emvmvexvmxdxsx(int x_a, int channel);
  void ag_psp_240_dxcx_emvmvexvmxdxsx(int x_a, int zero);
  void ax_psp_240_uxux_emvmvexvmxdxux(int x_a);
  void ac_psp_240_uxux_emvmvexvmxdxux(int x_a, int channel);
  void ag_psp_240_uxux_emvmvexvmxdxux(int x_a, int zero);
  void ax_psp_240_uxcx_emvmvexvmxdxcx(int x_a);
  void ac_psp_240_uxcx_emvmvexvmxdxcx(int x_a, int channel);
  void ag_psp_240_uxcx_emvmvexvmxdxcx(int x_a, int zero);

};
#endif
