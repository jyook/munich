#ifndef ppexneh03_PHASESPACE_SET_HPP
#define ppexneh03_PHASESPACE_SET_HPP

using namespace std;

class ppexneh03_phasespace_set : public phasespace_set {
private:

public:
  ppexneh03_phasespace_set(){}
  ~ppexneh03_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_030_udx_epveh(int x_a);
  void ac_psp_030_udx_epveh(int x_a, int channel);
  void ag_psp_030_udx_epveh(int x_a, int zero);
  void ax_psp_030_usx_epveh(int x_a);
  void ac_psp_030_usx_epveh(int x_a, int channel);
  void ag_psp_030_usx_epveh(int x_a, int zero);
  void ax_psp_030_ubx_epveh(int x_a);
  void ac_psp_030_ubx_epveh(int x_a, int channel);
  void ag_psp_030_ubx_epveh(int x_a, int zero);
  void ax_psp_030_cdx_epveh(int x_a);
  void ac_psp_030_cdx_epveh(int x_a, int channel);
  void ag_psp_030_cdx_epveh(int x_a, int zero);
  void ax_psp_030_csx_epveh(int x_a);
  void ac_psp_030_csx_epveh(int x_a, int channel);
  void ag_psp_030_csx_epveh(int x_a, int zero);
  void ax_psp_030_cbx_epveh(int x_a);
  void ac_psp_030_cbx_epveh(int x_a, int channel);
  void ag_psp_030_cbx_epveh(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_130_gu_epvehd(int x_a);
  void ac_psp_130_gu_epvehd(int x_a, int channel);
  void ag_psp_130_gu_epvehd(int x_a, int zero);
  void ax_psp_130_gu_epvehs(int x_a);
  void ac_psp_130_gu_epvehs(int x_a, int channel);
  void ag_psp_130_gu_epvehs(int x_a, int zero);
  void ax_psp_130_gu_epvehb(int x_a);
  void ac_psp_130_gu_epvehb(int x_a, int channel);
  void ag_psp_130_gu_epvehb(int x_a, int zero);
  void ax_psp_130_gc_epvehd(int x_a);
  void ac_psp_130_gc_epvehd(int x_a, int channel);
  void ag_psp_130_gc_epvehd(int x_a, int zero);
  void ax_psp_130_gc_epvehs(int x_a);
  void ac_psp_130_gc_epvehs(int x_a, int channel);
  void ag_psp_130_gc_epvehs(int x_a, int zero);
  void ax_psp_130_gc_epvehb(int x_a);
  void ac_psp_130_gc_epvehb(int x_a, int channel);
  void ag_psp_130_gc_epvehb(int x_a, int zero);
  void ax_psp_130_gdx_epvehux(int x_a);
  void ac_psp_130_gdx_epvehux(int x_a, int channel);
  void ag_psp_130_gdx_epvehux(int x_a, int zero);
  void ax_psp_130_gdx_epvehcx(int x_a);
  void ac_psp_130_gdx_epvehcx(int x_a, int channel);
  void ag_psp_130_gdx_epvehcx(int x_a, int zero);
  void ax_psp_130_gsx_epvehux(int x_a);
  void ac_psp_130_gsx_epvehux(int x_a, int channel);
  void ag_psp_130_gsx_epvehux(int x_a, int zero);
  void ax_psp_130_gsx_epvehcx(int x_a);
  void ac_psp_130_gsx_epvehcx(int x_a, int channel);
  void ag_psp_130_gsx_epvehcx(int x_a, int zero);
  void ax_psp_130_gbx_epvehux(int x_a);
  void ac_psp_130_gbx_epvehux(int x_a, int channel);
  void ag_psp_130_gbx_epvehux(int x_a, int zero);
  void ax_psp_130_gbx_epvehcx(int x_a);
  void ac_psp_130_gbx_epvehcx(int x_a, int channel);
  void ag_psp_130_gbx_epvehcx(int x_a, int zero);
  void ax_psp_130_udx_epvehg(int x_a);
  void ac_psp_130_udx_epvehg(int x_a, int channel);
  void ag_psp_130_udx_epvehg(int x_a, int zero);
  void ax_psp_130_usx_epvehg(int x_a);
  void ac_psp_130_usx_epvehg(int x_a, int channel);
  void ag_psp_130_usx_epvehg(int x_a, int zero);
  void ax_psp_130_ubx_epvehg(int x_a);
  void ac_psp_130_ubx_epvehg(int x_a, int channel);
  void ag_psp_130_ubx_epvehg(int x_a, int zero);
  void ax_psp_040_ua_epvehd(int x_a);
  void ac_psp_040_ua_epvehd(int x_a, int channel);
  void ag_psp_040_ua_epvehd(int x_a, int zero);
  void ax_psp_040_ua_epvehs(int x_a);
  void ac_psp_040_ua_epvehs(int x_a, int channel);
  void ag_psp_040_ua_epvehs(int x_a, int zero);
  void ax_psp_040_ua_epvehb(int x_a);
  void ac_psp_040_ua_epvehb(int x_a, int channel);
  void ag_psp_040_ua_epvehb(int x_a, int zero);
  void ax_psp_130_cdx_epvehg(int x_a);
  void ac_psp_130_cdx_epvehg(int x_a, int channel);
  void ag_psp_130_cdx_epvehg(int x_a, int zero);
  void ax_psp_130_csx_epvehg(int x_a);
  void ac_psp_130_csx_epvehg(int x_a, int channel);
  void ag_psp_130_csx_epvehg(int x_a, int zero);
  void ax_psp_130_cbx_epvehg(int x_a);
  void ac_psp_130_cbx_epvehg(int x_a, int channel);
  void ag_psp_130_cbx_epvehg(int x_a, int zero);
  void ax_psp_040_ca_epvehd(int x_a);
  void ac_psp_040_ca_epvehd(int x_a, int channel);
  void ag_psp_040_ca_epvehd(int x_a, int zero);
  void ax_psp_040_ca_epvehs(int x_a);
  void ac_psp_040_ca_epvehs(int x_a, int channel);
  void ag_psp_040_ca_epvehs(int x_a, int zero);
  void ax_psp_040_ca_epvehb(int x_a);
  void ac_psp_040_ca_epvehb(int x_a, int channel);
  void ag_psp_040_ca_epvehb(int x_a, int zero);
  void ax_psp_040_dxa_epvehux(int x_a);
  void ac_psp_040_dxa_epvehux(int x_a, int channel);
  void ag_psp_040_dxa_epvehux(int x_a, int zero);
  void ax_psp_040_dxa_epvehcx(int x_a);
  void ac_psp_040_dxa_epvehcx(int x_a, int channel);
  void ag_psp_040_dxa_epvehcx(int x_a, int zero);
  void ax_psp_040_sxa_epvehux(int x_a);
  void ac_psp_040_sxa_epvehux(int x_a, int channel);
  void ag_psp_040_sxa_epvehux(int x_a, int zero);
  void ax_psp_040_sxa_epvehcx(int x_a);
  void ac_psp_040_sxa_epvehcx(int x_a, int channel);
  void ag_psp_040_sxa_epvehcx(int x_a, int zero);
  void ax_psp_040_bxa_epvehux(int x_a);
  void ac_psp_040_bxa_epvehux(int x_a, int channel);
  void ag_psp_040_bxa_epvehux(int x_a, int zero);
  void ax_psp_040_bxa_epvehcx(int x_a);
  void ac_psp_040_bxa_epvehcx(int x_a, int channel);
  void ag_psp_040_bxa_epvehcx(int x_a, int zero);
  void ax_psp_040_udx_epveha(int x_a);
  void ac_psp_040_udx_epveha(int x_a, int channel);
  void ag_psp_040_udx_epveha(int x_a, int zero);
  void ax_psp_040_usx_epveha(int x_a);
  void ac_psp_040_usx_epveha(int x_a, int channel);
  void ag_psp_040_usx_epveha(int x_a, int zero);
  void ax_psp_040_ubx_epveha(int x_a);
  void ac_psp_040_ubx_epveha(int x_a, int channel);
  void ag_psp_040_ubx_epveha(int x_a, int zero);
  void ax_psp_040_cdx_epveha(int x_a);
  void ac_psp_040_cdx_epveha(int x_a, int channel);
  void ag_psp_040_cdx_epveha(int x_a, int zero);
  void ax_psp_040_csx_epveha(int x_a);
  void ac_psp_040_csx_epveha(int x_a, int channel);
  void ag_psp_040_csx_epveha(int x_a, int zero);
  void ax_psp_040_cbx_epveha(int x_a);
  void ac_psp_040_cbx_epveha(int x_a, int channel);
  void ag_psp_040_cbx_epveha(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_230_gg_epvehdux(int x_a);
  void ac_psp_230_gg_epvehdux(int x_a, int channel);
  void ag_psp_230_gg_epvehdux(int x_a, int zero);
  void ax_psp_230_gg_epvehdcx(int x_a);
  void ac_psp_230_gg_epvehdcx(int x_a, int channel);
  void ag_psp_230_gg_epvehdcx(int x_a, int zero);
  void ax_psp_230_gg_epvehsux(int x_a);
  void ac_psp_230_gg_epvehsux(int x_a, int channel);
  void ag_psp_230_gg_epvehsux(int x_a, int zero);
  void ax_psp_230_gg_epvehscx(int x_a);
  void ac_psp_230_gg_epvehscx(int x_a, int channel);
  void ag_psp_230_gg_epvehscx(int x_a, int zero);
  void ax_psp_230_gg_epvehbux(int x_a);
  void ac_psp_230_gg_epvehbux(int x_a, int channel);
  void ag_psp_230_gg_epvehbux(int x_a, int zero);
  void ax_psp_230_gg_epvehbcx(int x_a);
  void ac_psp_230_gg_epvehbcx(int x_a, int channel);
  void ag_psp_230_gg_epvehbcx(int x_a, int zero);
  void ax_psp_230_gu_epvehgd(int x_a);
  void ac_psp_230_gu_epvehgd(int x_a, int channel);
  void ag_psp_230_gu_epvehgd(int x_a, int zero);
  void ax_psp_230_gu_epvehgs(int x_a);
  void ac_psp_230_gu_epvehgs(int x_a, int channel);
  void ag_psp_230_gu_epvehgs(int x_a, int zero);
  void ax_psp_230_gu_epvehgb(int x_a);
  void ac_psp_230_gu_epvehgb(int x_a, int channel);
  void ag_psp_230_gu_epvehgb(int x_a, int zero);
  void ax_psp_230_gc_epvehgd(int x_a);
  void ac_psp_230_gc_epvehgd(int x_a, int channel);
  void ag_psp_230_gc_epvehgd(int x_a, int zero);
  void ax_psp_230_gc_epvehgs(int x_a);
  void ac_psp_230_gc_epvehgs(int x_a, int channel);
  void ag_psp_230_gc_epvehgs(int x_a, int zero);
  void ax_psp_230_gc_epvehgb(int x_a);
  void ac_psp_230_gc_epvehgb(int x_a, int channel);
  void ag_psp_230_gc_epvehgb(int x_a, int zero);
  void ax_psp_230_gdx_epvehgux(int x_a);
  void ac_psp_230_gdx_epvehgux(int x_a, int channel);
  void ag_psp_230_gdx_epvehgux(int x_a, int zero);
  void ax_psp_230_gdx_epvehgcx(int x_a);
  void ac_psp_230_gdx_epvehgcx(int x_a, int channel);
  void ag_psp_230_gdx_epvehgcx(int x_a, int zero);
  void ax_psp_230_gsx_epvehgux(int x_a);
  void ac_psp_230_gsx_epvehgux(int x_a, int channel);
  void ag_psp_230_gsx_epvehgux(int x_a, int zero);
  void ax_psp_230_gsx_epvehgcx(int x_a);
  void ac_psp_230_gsx_epvehgcx(int x_a, int channel);
  void ag_psp_230_gsx_epvehgcx(int x_a, int zero);
  void ax_psp_230_gbx_epvehgux(int x_a);
  void ac_psp_230_gbx_epvehgux(int x_a, int channel);
  void ag_psp_230_gbx_epvehgux(int x_a, int zero);
  void ax_psp_230_gbx_epvehgcx(int x_a);
  void ac_psp_230_gbx_epvehgcx(int x_a, int channel);
  void ag_psp_230_gbx_epvehgcx(int x_a, int zero);
  void ax_psp_230_du_epvehdd(int x_a);
  void ac_psp_230_du_epvehdd(int x_a, int channel);
  void ag_psp_230_du_epvehdd(int x_a, int zero);
  void ax_psp_230_du_epvehds(int x_a);
  void ac_psp_230_du_epvehds(int x_a, int channel);
  void ag_psp_230_du_epvehds(int x_a, int zero);
  void ax_psp_230_du_epvehdb(int x_a);
  void ac_psp_230_du_epvehdb(int x_a, int channel);
  void ag_psp_230_du_epvehdb(int x_a, int zero);
  void ax_psp_230_dc_epvehdd(int x_a);
  void ac_psp_230_dc_epvehdd(int x_a, int channel);
  void ag_psp_230_dc_epvehdd(int x_a, int zero);
  void ax_psp_230_dc_epvehds(int x_a);
  void ac_psp_230_dc_epvehds(int x_a, int channel);
  void ag_psp_230_dc_epvehds(int x_a, int zero);
  void ax_psp_230_dc_epvehdb(int x_a);
  void ac_psp_230_dc_epvehdb(int x_a, int channel);
  void ag_psp_230_dc_epvehdb(int x_a, int zero);
  void ax_psp_230_ddx_epvehdux(int x_a);
  void ac_psp_230_ddx_epvehdux(int x_a, int channel);
  void ag_psp_230_ddx_epvehdux(int x_a, int zero);
  void ax_psp_230_ddx_epvehdcx(int x_a);
  void ac_psp_230_ddx_epvehdcx(int x_a, int channel);
  void ag_psp_230_ddx_epvehdcx(int x_a, int zero);
  void ax_psp_230_ddx_epvehsux(int x_a);
  void ac_psp_230_ddx_epvehsux(int x_a, int channel);
  void ag_psp_230_ddx_epvehsux(int x_a, int zero);
  void ax_psp_230_ddx_epvehscx(int x_a);
  void ac_psp_230_ddx_epvehscx(int x_a, int channel);
  void ag_psp_230_ddx_epvehscx(int x_a, int zero);
  void ax_psp_230_ddx_epvehbux(int x_a);
  void ac_psp_230_ddx_epvehbux(int x_a, int channel);
  void ag_psp_230_ddx_epvehbux(int x_a, int zero);
  void ax_psp_230_ddx_epvehbcx(int x_a);
  void ac_psp_230_ddx_epvehbcx(int x_a, int channel);
  void ag_psp_230_ddx_epvehbcx(int x_a, int zero);
  void ax_psp_230_dsx_epvehdux(int x_a);
  void ac_psp_230_dsx_epvehdux(int x_a, int channel);
  void ag_psp_230_dsx_epvehdux(int x_a, int zero);
  void ax_psp_230_dsx_epvehdcx(int x_a);
  void ac_psp_230_dsx_epvehdcx(int x_a, int channel);
  void ag_psp_230_dsx_epvehdcx(int x_a, int zero);
  void ax_psp_230_dbx_epvehdux(int x_a);
  void ac_psp_230_dbx_epvehdux(int x_a, int channel);
  void ag_psp_230_dbx_epvehdux(int x_a, int zero);
  void ax_psp_230_dbx_epvehdcx(int x_a);
  void ac_psp_230_dbx_epvehdcx(int x_a, int channel);
  void ag_psp_230_dbx_epvehdcx(int x_a, int zero);
  void ax_psp_230_uu_epvehdu(int x_a);
  void ac_psp_230_uu_epvehdu(int x_a, int channel);
  void ag_psp_230_uu_epvehdu(int x_a, int zero);
  void ax_psp_230_uu_epvehus(int x_a);
  void ac_psp_230_uu_epvehus(int x_a, int channel);
  void ag_psp_230_uu_epvehus(int x_a, int zero);
  void ax_psp_230_uu_epvehub(int x_a);
  void ac_psp_230_uu_epvehub(int x_a, int channel);
  void ag_psp_230_uu_epvehub(int x_a, int zero);
  void ax_psp_230_us_epvehds(int x_a);
  void ac_psp_230_us_epvehds(int x_a, int channel);
  void ag_psp_230_us_epvehds(int x_a, int zero);
  void ax_psp_230_us_epvehss(int x_a);
  void ac_psp_230_us_epvehss(int x_a, int channel);
  void ag_psp_230_us_epvehss(int x_a, int zero);
  void ax_psp_230_us_epvehsb(int x_a);
  void ac_psp_230_us_epvehsb(int x_a, int channel);
  void ag_psp_230_us_epvehsb(int x_a, int zero);
  void ax_psp_230_uc_epvehdu(int x_a);
  void ac_psp_230_uc_epvehdu(int x_a, int channel);
  void ag_psp_230_uc_epvehdu(int x_a, int zero);
  void ax_psp_230_uc_epvehdc(int x_a);
  void ac_psp_230_uc_epvehdc(int x_a, int channel);
  void ag_psp_230_uc_epvehdc(int x_a, int zero);
  void ax_psp_230_uc_epvehus(int x_a);
  void ac_psp_230_uc_epvehus(int x_a, int channel);
  void ag_psp_230_uc_epvehus(int x_a, int zero);
  void ax_psp_230_uc_epvehub(int x_a);
  void ac_psp_230_uc_epvehub(int x_a, int channel);
  void ag_psp_230_uc_epvehub(int x_a, int zero);
  void ax_psp_230_uc_epvehsc(int x_a);
  void ac_psp_230_uc_epvehsc(int x_a, int channel);
  void ag_psp_230_uc_epvehsc(int x_a, int zero);
  void ax_psp_230_uc_epvehcb(int x_a);
  void ac_psp_230_uc_epvehcb(int x_a, int channel);
  void ag_psp_230_uc_epvehcb(int x_a, int zero);
  void ax_psp_230_ub_epvehdb(int x_a);
  void ac_psp_230_ub_epvehdb(int x_a, int channel);
  void ag_psp_230_ub_epvehdb(int x_a, int zero);
  void ax_psp_230_ub_epvehsb(int x_a);
  void ac_psp_230_ub_epvehsb(int x_a, int channel);
  void ag_psp_230_ub_epvehsb(int x_a, int zero);
  void ax_psp_230_ub_epvehbb(int x_a);
  void ac_psp_230_ub_epvehbb(int x_a, int channel);
  void ag_psp_230_ub_epvehbb(int x_a, int zero);
  void ax_psp_230_udx_epvehgg(int x_a);
  void ac_psp_230_udx_epvehgg(int x_a, int channel);
  void ag_psp_230_udx_epvehgg(int x_a, int zero);
  void ax_psp_230_udx_epvehddx(int x_a);
  void ac_psp_230_udx_epvehddx(int x_a, int channel);
  void ag_psp_230_udx_epvehddx(int x_a, int zero);
  void ax_psp_230_udx_epvehuux(int x_a);
  void ac_psp_230_udx_epvehuux(int x_a, int channel);
  void ag_psp_230_udx_epvehuux(int x_a, int zero);
  void ax_psp_230_udx_epvehucx(int x_a);
  void ac_psp_230_udx_epvehucx(int x_a, int channel);
  void ag_psp_230_udx_epvehucx(int x_a, int zero);
  void ax_psp_230_udx_epvehsdx(int x_a);
  void ac_psp_230_udx_epvehsdx(int x_a, int channel);
  void ag_psp_230_udx_epvehsdx(int x_a, int zero);
  void ax_psp_230_udx_epvehssx(int x_a);
  void ac_psp_230_udx_epvehssx(int x_a, int channel);
  void ag_psp_230_udx_epvehssx(int x_a, int zero);
  void ax_psp_230_udx_epvehccx(int x_a);
  void ac_psp_230_udx_epvehccx(int x_a, int channel);
  void ag_psp_230_udx_epvehccx(int x_a, int zero);
  void ax_psp_230_udx_epvehbdx(int x_a);
  void ac_psp_230_udx_epvehbdx(int x_a, int channel);
  void ag_psp_230_udx_epvehbdx(int x_a, int zero);
  void ax_psp_230_udx_epvehbbx(int x_a);
  void ac_psp_230_udx_epvehbbx(int x_a, int channel);
  void ag_psp_230_udx_epvehbbx(int x_a, int zero);
  void ax_psp_230_uux_epvehdux(int x_a);
  void ac_psp_230_uux_epvehdux(int x_a, int channel);
  void ag_psp_230_uux_epvehdux(int x_a, int zero);
  void ax_psp_230_uux_epvehdcx(int x_a);
  void ac_psp_230_uux_epvehdcx(int x_a, int channel);
  void ag_psp_230_uux_epvehdcx(int x_a, int zero);
  void ax_psp_230_uux_epvehsux(int x_a);
  void ac_psp_230_uux_epvehsux(int x_a, int channel);
  void ag_psp_230_uux_epvehsux(int x_a, int zero);
  void ax_psp_230_uux_epvehscx(int x_a);
  void ac_psp_230_uux_epvehscx(int x_a, int channel);
  void ag_psp_230_uux_epvehscx(int x_a, int zero);
  void ax_psp_230_uux_epvehbux(int x_a);
  void ac_psp_230_uux_epvehbux(int x_a, int channel);
  void ag_psp_230_uux_epvehbux(int x_a, int zero);
  void ax_psp_230_uux_epvehbcx(int x_a);
  void ac_psp_230_uux_epvehbcx(int x_a, int channel);
  void ag_psp_230_uux_epvehbcx(int x_a, int zero);
  void ax_psp_230_usx_epvehgg(int x_a);
  void ac_psp_230_usx_epvehgg(int x_a, int channel);
  void ag_psp_230_usx_epvehgg(int x_a, int zero);
  void ax_psp_230_usx_epvehddx(int x_a);
  void ac_psp_230_usx_epvehddx(int x_a, int channel);
  void ag_psp_230_usx_epvehddx(int x_a, int zero);
  void ax_psp_230_usx_epvehdsx(int x_a);
  void ac_psp_230_usx_epvehdsx(int x_a, int channel);
  void ag_psp_230_usx_epvehdsx(int x_a, int zero);
  void ax_psp_230_usx_epvehuux(int x_a);
  void ac_psp_230_usx_epvehuux(int x_a, int channel);
  void ag_psp_230_usx_epvehuux(int x_a, int zero);
  void ax_psp_230_usx_epvehucx(int x_a);
  void ac_psp_230_usx_epvehucx(int x_a, int channel);
  void ag_psp_230_usx_epvehucx(int x_a, int zero);
  void ax_psp_230_usx_epvehssx(int x_a);
  void ac_psp_230_usx_epvehssx(int x_a, int channel);
  void ag_psp_230_usx_epvehssx(int x_a, int zero);
  void ax_psp_230_usx_epvehccx(int x_a);
  void ac_psp_230_usx_epvehccx(int x_a, int channel);
  void ag_psp_230_usx_epvehccx(int x_a, int zero);
  void ax_psp_230_usx_epvehbsx(int x_a);
  void ac_psp_230_usx_epvehbsx(int x_a, int channel);
  void ag_psp_230_usx_epvehbsx(int x_a, int zero);
  void ax_psp_230_usx_epvehbbx(int x_a);
  void ac_psp_230_usx_epvehbbx(int x_a, int channel);
  void ag_psp_230_usx_epvehbbx(int x_a, int zero);
  void ax_psp_230_ucx_epvehdcx(int x_a);
  void ac_psp_230_ucx_epvehdcx(int x_a, int channel);
  void ag_psp_230_ucx_epvehdcx(int x_a, int zero);
  void ax_psp_230_ucx_epvehscx(int x_a);
  void ac_psp_230_ucx_epvehscx(int x_a, int channel);
  void ag_psp_230_ucx_epvehscx(int x_a, int zero);
  void ax_psp_230_ucx_epvehbcx(int x_a);
  void ac_psp_230_ucx_epvehbcx(int x_a, int channel);
  void ag_psp_230_ucx_epvehbcx(int x_a, int zero);
  void ax_psp_230_ubx_epvehgg(int x_a);
  void ac_psp_230_ubx_epvehgg(int x_a, int channel);
  void ag_psp_230_ubx_epvehgg(int x_a, int zero);
  void ax_psp_230_ubx_epvehddx(int x_a);
  void ac_psp_230_ubx_epvehddx(int x_a, int channel);
  void ag_psp_230_ubx_epvehddx(int x_a, int zero);
  void ax_psp_230_ubx_epvehdbx(int x_a);
  void ac_psp_230_ubx_epvehdbx(int x_a, int channel);
  void ag_psp_230_ubx_epvehdbx(int x_a, int zero);
  void ax_psp_230_ubx_epvehuux(int x_a);
  void ac_psp_230_ubx_epvehuux(int x_a, int channel);
  void ag_psp_230_ubx_epvehuux(int x_a, int zero);
  void ax_psp_230_ubx_epvehucx(int x_a);
  void ac_psp_230_ubx_epvehucx(int x_a, int channel);
  void ag_psp_230_ubx_epvehucx(int x_a, int zero);
  void ax_psp_230_ubx_epvehssx(int x_a);
  void ac_psp_230_ubx_epvehssx(int x_a, int channel);
  void ag_psp_230_ubx_epvehssx(int x_a, int zero);
  void ax_psp_230_ubx_epvehsbx(int x_a);
  void ac_psp_230_ubx_epvehsbx(int x_a, int channel);
  void ag_psp_230_ubx_epvehsbx(int x_a, int zero);
  void ax_psp_230_ubx_epvehccx(int x_a);
  void ac_psp_230_ubx_epvehccx(int x_a, int channel);
  void ag_psp_230_ubx_epvehccx(int x_a, int zero);
  void ax_psp_230_ubx_epvehbbx(int x_a);
  void ac_psp_230_ubx_epvehbbx(int x_a, int channel);
  void ag_psp_230_ubx_epvehbbx(int x_a, int zero);
  void ax_psp_230_sc_epvehds(int x_a);
  void ac_psp_230_sc_epvehds(int x_a, int channel);
  void ag_psp_230_sc_epvehds(int x_a, int zero);
  void ax_psp_230_sc_epvehss(int x_a);
  void ac_psp_230_sc_epvehss(int x_a, int channel);
  void ag_psp_230_sc_epvehss(int x_a, int zero);
  void ax_psp_230_sc_epvehsb(int x_a);
  void ac_psp_230_sc_epvehsb(int x_a, int channel);
  void ag_psp_230_sc_epvehsb(int x_a, int zero);
  void ax_psp_230_sdx_epvehsux(int x_a);
  void ac_psp_230_sdx_epvehsux(int x_a, int channel);
  void ag_psp_230_sdx_epvehsux(int x_a, int zero);
  void ax_psp_230_sdx_epvehscx(int x_a);
  void ac_psp_230_sdx_epvehscx(int x_a, int channel);
  void ag_psp_230_sdx_epvehscx(int x_a, int zero);
  void ax_psp_230_ssx_epvehdux(int x_a);
  void ac_psp_230_ssx_epvehdux(int x_a, int channel);
  void ag_psp_230_ssx_epvehdux(int x_a, int zero);
  void ax_psp_230_ssx_epvehdcx(int x_a);
  void ac_psp_230_ssx_epvehdcx(int x_a, int channel);
  void ag_psp_230_ssx_epvehdcx(int x_a, int zero);
  void ax_psp_230_ssx_epvehsux(int x_a);
  void ac_psp_230_ssx_epvehsux(int x_a, int channel);
  void ag_psp_230_ssx_epvehsux(int x_a, int zero);
  void ax_psp_230_ssx_epvehscx(int x_a);
  void ac_psp_230_ssx_epvehscx(int x_a, int channel);
  void ag_psp_230_ssx_epvehscx(int x_a, int zero);
  void ax_psp_230_ssx_epvehbux(int x_a);
  void ac_psp_230_ssx_epvehbux(int x_a, int channel);
  void ag_psp_230_ssx_epvehbux(int x_a, int zero);
  void ax_psp_230_ssx_epvehbcx(int x_a);
  void ac_psp_230_ssx_epvehbcx(int x_a, int channel);
  void ag_psp_230_ssx_epvehbcx(int x_a, int zero);
  void ax_psp_230_sbx_epvehsux(int x_a);
  void ac_psp_230_sbx_epvehsux(int x_a, int channel);
  void ag_psp_230_sbx_epvehsux(int x_a, int zero);
  void ax_psp_230_sbx_epvehscx(int x_a);
  void ac_psp_230_sbx_epvehscx(int x_a, int channel);
  void ag_psp_230_sbx_epvehscx(int x_a, int zero);
  void ax_psp_230_cc_epvehdc(int x_a);
  void ac_psp_230_cc_epvehdc(int x_a, int channel);
  void ag_psp_230_cc_epvehdc(int x_a, int zero);
  void ax_psp_230_cc_epvehsc(int x_a);
  void ac_psp_230_cc_epvehsc(int x_a, int channel);
  void ag_psp_230_cc_epvehsc(int x_a, int zero);
  void ax_psp_230_cc_epvehcb(int x_a);
  void ac_psp_230_cc_epvehcb(int x_a, int channel);
  void ag_psp_230_cc_epvehcb(int x_a, int zero);
  void ax_psp_230_cb_epvehdb(int x_a);
  void ac_psp_230_cb_epvehdb(int x_a, int channel);
  void ag_psp_230_cb_epvehdb(int x_a, int zero);
  void ax_psp_230_cb_epvehsb(int x_a);
  void ac_psp_230_cb_epvehsb(int x_a, int channel);
  void ag_psp_230_cb_epvehsb(int x_a, int zero);
  void ax_psp_230_cb_epvehbb(int x_a);
  void ac_psp_230_cb_epvehbb(int x_a, int channel);
  void ag_psp_230_cb_epvehbb(int x_a, int zero);
  void ax_psp_230_cdx_epvehgg(int x_a);
  void ac_psp_230_cdx_epvehgg(int x_a, int channel);
  void ag_psp_230_cdx_epvehgg(int x_a, int zero);
  void ax_psp_230_cdx_epvehddx(int x_a);
  void ac_psp_230_cdx_epvehddx(int x_a, int channel);
  void ag_psp_230_cdx_epvehddx(int x_a, int zero);
  void ax_psp_230_cdx_epvehuux(int x_a);
  void ac_psp_230_cdx_epvehuux(int x_a, int channel);
  void ag_psp_230_cdx_epvehuux(int x_a, int zero);
  void ax_psp_230_cdx_epvehsdx(int x_a);
  void ac_psp_230_cdx_epvehsdx(int x_a, int channel);
  void ag_psp_230_cdx_epvehsdx(int x_a, int zero);
  void ax_psp_230_cdx_epvehssx(int x_a);
  void ac_psp_230_cdx_epvehssx(int x_a, int channel);
  void ag_psp_230_cdx_epvehssx(int x_a, int zero);
  void ax_psp_230_cdx_epvehcux(int x_a);
  void ac_psp_230_cdx_epvehcux(int x_a, int channel);
  void ag_psp_230_cdx_epvehcux(int x_a, int zero);
  void ax_psp_230_cdx_epvehccx(int x_a);
  void ac_psp_230_cdx_epvehccx(int x_a, int channel);
  void ag_psp_230_cdx_epvehccx(int x_a, int zero);
  void ax_psp_230_cdx_epvehbdx(int x_a);
  void ac_psp_230_cdx_epvehbdx(int x_a, int channel);
  void ag_psp_230_cdx_epvehbdx(int x_a, int zero);
  void ax_psp_230_cdx_epvehbbx(int x_a);
  void ac_psp_230_cdx_epvehbbx(int x_a, int channel);
  void ag_psp_230_cdx_epvehbbx(int x_a, int zero);
  void ax_psp_230_cux_epvehdux(int x_a);
  void ac_psp_230_cux_epvehdux(int x_a, int channel);
  void ag_psp_230_cux_epvehdux(int x_a, int zero);
  void ax_psp_230_cux_epvehsux(int x_a);
  void ac_psp_230_cux_epvehsux(int x_a, int channel);
  void ag_psp_230_cux_epvehsux(int x_a, int zero);
  void ax_psp_230_cux_epvehbux(int x_a);
  void ac_psp_230_cux_epvehbux(int x_a, int channel);
  void ag_psp_230_cux_epvehbux(int x_a, int zero);
  void ax_psp_230_csx_epvehgg(int x_a);
  void ac_psp_230_csx_epvehgg(int x_a, int channel);
  void ag_psp_230_csx_epvehgg(int x_a, int zero);
  void ax_psp_230_csx_epvehddx(int x_a);
  void ac_psp_230_csx_epvehddx(int x_a, int channel);
  void ag_psp_230_csx_epvehddx(int x_a, int zero);
  void ax_psp_230_csx_epvehdsx(int x_a);
  void ac_psp_230_csx_epvehdsx(int x_a, int channel);
  void ag_psp_230_csx_epvehdsx(int x_a, int zero);
  void ax_psp_230_csx_epvehuux(int x_a);
  void ac_psp_230_csx_epvehuux(int x_a, int channel);
  void ag_psp_230_csx_epvehuux(int x_a, int zero);
  void ax_psp_230_csx_epvehssx(int x_a);
  void ac_psp_230_csx_epvehssx(int x_a, int channel);
  void ag_psp_230_csx_epvehssx(int x_a, int zero);
  void ax_psp_230_csx_epvehcux(int x_a);
  void ac_psp_230_csx_epvehcux(int x_a, int channel);
  void ag_psp_230_csx_epvehcux(int x_a, int zero);
  void ax_psp_230_csx_epvehccx(int x_a);
  void ac_psp_230_csx_epvehccx(int x_a, int channel);
  void ag_psp_230_csx_epvehccx(int x_a, int zero);
  void ax_psp_230_csx_epvehbsx(int x_a);
  void ac_psp_230_csx_epvehbsx(int x_a, int channel);
  void ag_psp_230_csx_epvehbsx(int x_a, int zero);
  void ax_psp_230_csx_epvehbbx(int x_a);
  void ac_psp_230_csx_epvehbbx(int x_a, int channel);
  void ag_psp_230_csx_epvehbbx(int x_a, int zero);
  void ax_psp_230_ccx_epvehdux(int x_a);
  void ac_psp_230_ccx_epvehdux(int x_a, int channel);
  void ag_psp_230_ccx_epvehdux(int x_a, int zero);
  void ax_psp_230_ccx_epvehdcx(int x_a);
  void ac_psp_230_ccx_epvehdcx(int x_a, int channel);
  void ag_psp_230_ccx_epvehdcx(int x_a, int zero);
  void ax_psp_230_ccx_epvehsux(int x_a);
  void ac_psp_230_ccx_epvehsux(int x_a, int channel);
  void ag_psp_230_ccx_epvehsux(int x_a, int zero);
  void ax_psp_230_ccx_epvehscx(int x_a);
  void ac_psp_230_ccx_epvehscx(int x_a, int channel);
  void ag_psp_230_ccx_epvehscx(int x_a, int zero);
  void ax_psp_230_ccx_epvehbux(int x_a);
  void ac_psp_230_ccx_epvehbux(int x_a, int channel);
  void ag_psp_230_ccx_epvehbux(int x_a, int zero);
  void ax_psp_230_ccx_epvehbcx(int x_a);
  void ac_psp_230_ccx_epvehbcx(int x_a, int channel);
  void ag_psp_230_ccx_epvehbcx(int x_a, int zero);
  void ax_psp_230_cbx_epvehgg(int x_a);
  void ac_psp_230_cbx_epvehgg(int x_a, int channel);
  void ag_psp_230_cbx_epvehgg(int x_a, int zero);
  void ax_psp_230_cbx_epvehddx(int x_a);
  void ac_psp_230_cbx_epvehddx(int x_a, int channel);
  void ag_psp_230_cbx_epvehddx(int x_a, int zero);
  void ax_psp_230_cbx_epvehdbx(int x_a);
  void ac_psp_230_cbx_epvehdbx(int x_a, int channel);
  void ag_psp_230_cbx_epvehdbx(int x_a, int zero);
  void ax_psp_230_cbx_epvehuux(int x_a);
  void ac_psp_230_cbx_epvehuux(int x_a, int channel);
  void ag_psp_230_cbx_epvehuux(int x_a, int zero);
  void ax_psp_230_cbx_epvehssx(int x_a);
  void ac_psp_230_cbx_epvehssx(int x_a, int channel);
  void ag_psp_230_cbx_epvehssx(int x_a, int zero);
  void ax_psp_230_cbx_epvehsbx(int x_a);
  void ac_psp_230_cbx_epvehsbx(int x_a, int channel);
  void ag_psp_230_cbx_epvehsbx(int x_a, int zero);
  void ax_psp_230_cbx_epvehcux(int x_a);
  void ac_psp_230_cbx_epvehcux(int x_a, int channel);
  void ag_psp_230_cbx_epvehcux(int x_a, int zero);
  void ax_psp_230_cbx_epvehccx(int x_a);
  void ac_psp_230_cbx_epvehccx(int x_a, int channel);
  void ag_psp_230_cbx_epvehccx(int x_a, int zero);
  void ax_psp_230_cbx_epvehbbx(int x_a);
  void ac_psp_230_cbx_epvehbbx(int x_a, int channel);
  void ag_psp_230_cbx_epvehbbx(int x_a, int zero);
  void ax_psp_230_bdx_epvehbux(int x_a);
  void ac_psp_230_bdx_epvehbux(int x_a, int channel);
  void ag_psp_230_bdx_epvehbux(int x_a, int zero);
  void ax_psp_230_bdx_epvehbcx(int x_a);
  void ac_psp_230_bdx_epvehbcx(int x_a, int channel);
  void ag_psp_230_bdx_epvehbcx(int x_a, int zero);
  void ax_psp_230_bsx_epvehbux(int x_a);
  void ac_psp_230_bsx_epvehbux(int x_a, int channel);
  void ag_psp_230_bsx_epvehbux(int x_a, int zero);
  void ax_psp_230_bsx_epvehbcx(int x_a);
  void ac_psp_230_bsx_epvehbcx(int x_a, int channel);
  void ag_psp_230_bsx_epvehbcx(int x_a, int zero);
  void ax_psp_230_bbx_epvehdux(int x_a);
  void ac_psp_230_bbx_epvehdux(int x_a, int channel);
  void ag_psp_230_bbx_epvehdux(int x_a, int zero);
  void ax_psp_230_bbx_epvehdcx(int x_a);
  void ac_psp_230_bbx_epvehdcx(int x_a, int channel);
  void ag_psp_230_bbx_epvehdcx(int x_a, int zero);
  void ax_psp_230_bbx_epvehsux(int x_a);
  void ac_psp_230_bbx_epvehsux(int x_a, int channel);
  void ag_psp_230_bbx_epvehsux(int x_a, int zero);
  void ax_psp_230_bbx_epvehscx(int x_a);
  void ac_psp_230_bbx_epvehscx(int x_a, int channel);
  void ag_psp_230_bbx_epvehscx(int x_a, int zero);
  void ax_psp_230_bbx_epvehbux(int x_a);
  void ac_psp_230_bbx_epvehbux(int x_a, int channel);
  void ag_psp_230_bbx_epvehbux(int x_a, int zero);
  void ax_psp_230_bbx_epvehbcx(int x_a);
  void ac_psp_230_bbx_epvehbcx(int x_a, int channel);
  void ag_psp_230_bbx_epvehbcx(int x_a, int zero);
  void ax_psp_230_dxdx_epvehdxux(int x_a);
  void ac_psp_230_dxdx_epvehdxux(int x_a, int channel);
  void ag_psp_230_dxdx_epvehdxux(int x_a, int zero);
  void ax_psp_230_dxdx_epvehdxcx(int x_a);
  void ac_psp_230_dxdx_epvehdxcx(int x_a, int channel);
  void ag_psp_230_dxdx_epvehdxcx(int x_a, int zero);
  void ax_psp_230_dxux_epvehuxux(int x_a);
  void ac_psp_230_dxux_epvehuxux(int x_a, int channel);
  void ag_psp_230_dxux_epvehuxux(int x_a, int zero);
  void ax_psp_230_dxux_epvehuxcx(int x_a);
  void ac_psp_230_dxux_epvehuxcx(int x_a, int channel);
  void ag_psp_230_dxux_epvehuxcx(int x_a, int zero);
  void ax_psp_230_dxsx_epvehdxux(int x_a);
  void ac_psp_230_dxsx_epvehdxux(int x_a, int channel);
  void ag_psp_230_dxsx_epvehdxux(int x_a, int zero);
  void ax_psp_230_dxsx_epvehdxcx(int x_a);
  void ac_psp_230_dxsx_epvehdxcx(int x_a, int channel);
  void ag_psp_230_dxsx_epvehdxcx(int x_a, int zero);
  void ax_psp_230_dxsx_epvehuxsx(int x_a);
  void ac_psp_230_dxsx_epvehuxsx(int x_a, int channel);
  void ag_psp_230_dxsx_epvehuxsx(int x_a, int zero);
  void ax_psp_230_dxsx_epvehsxcx(int x_a);
  void ac_psp_230_dxsx_epvehsxcx(int x_a, int channel);
  void ag_psp_230_dxsx_epvehsxcx(int x_a, int zero);
  void ax_psp_230_dxcx_epvehuxcx(int x_a);
  void ac_psp_230_dxcx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_dxcx_epvehuxcx(int x_a, int zero);
  void ax_psp_230_dxcx_epvehcxcx(int x_a);
  void ac_psp_230_dxcx_epvehcxcx(int x_a, int channel);
  void ag_psp_230_dxcx_epvehcxcx(int x_a, int zero);
  void ax_psp_230_dxbx_epvehdxux(int x_a);
  void ac_psp_230_dxbx_epvehdxux(int x_a, int channel);
  void ag_psp_230_dxbx_epvehdxux(int x_a, int zero);
  void ax_psp_230_dxbx_epvehdxcx(int x_a);
  void ac_psp_230_dxbx_epvehdxcx(int x_a, int channel);
  void ag_psp_230_dxbx_epvehdxcx(int x_a, int zero);
  void ax_psp_230_dxbx_epvehuxbx(int x_a);
  void ac_psp_230_dxbx_epvehuxbx(int x_a, int channel);
  void ag_psp_230_dxbx_epvehuxbx(int x_a, int zero);
  void ax_psp_230_dxbx_epvehcxbx(int x_a);
  void ac_psp_230_dxbx_epvehcxbx(int x_a, int channel);
  void ag_psp_230_dxbx_epvehcxbx(int x_a, int zero);
  void ax_psp_230_uxsx_epvehuxux(int x_a);
  void ac_psp_230_uxsx_epvehuxux(int x_a, int channel);
  void ag_psp_230_uxsx_epvehuxux(int x_a, int zero);
  void ax_psp_230_uxsx_epvehuxcx(int x_a);
  void ac_psp_230_uxsx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_uxsx_epvehuxcx(int x_a, int zero);
  void ax_psp_230_uxbx_epvehuxux(int x_a);
  void ac_psp_230_uxbx_epvehuxux(int x_a, int channel);
  void ag_psp_230_uxbx_epvehuxux(int x_a, int zero);
  void ax_psp_230_uxbx_epvehuxcx(int x_a);
  void ac_psp_230_uxbx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_uxbx_epvehuxcx(int x_a, int zero);
  void ax_psp_230_sxsx_epvehuxsx(int x_a);
  void ac_psp_230_sxsx_epvehuxsx(int x_a, int channel);
  void ag_psp_230_sxsx_epvehuxsx(int x_a, int zero);
  void ax_psp_230_sxsx_epvehsxcx(int x_a);
  void ac_psp_230_sxsx_epvehsxcx(int x_a, int channel);
  void ag_psp_230_sxsx_epvehsxcx(int x_a, int zero);
  void ax_psp_230_sxcx_epvehuxcx(int x_a);
  void ac_psp_230_sxcx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_sxcx_epvehuxcx(int x_a, int zero);
  void ax_psp_230_sxcx_epvehcxcx(int x_a);
  void ac_psp_230_sxcx_epvehcxcx(int x_a, int channel);
  void ag_psp_230_sxcx_epvehcxcx(int x_a, int zero);
  void ax_psp_230_sxbx_epvehuxsx(int x_a);
  void ac_psp_230_sxbx_epvehuxsx(int x_a, int channel);
  void ag_psp_230_sxbx_epvehuxsx(int x_a, int zero);
  void ax_psp_230_sxbx_epvehuxbx(int x_a);
  void ac_psp_230_sxbx_epvehuxbx(int x_a, int channel);
  void ag_psp_230_sxbx_epvehuxbx(int x_a, int zero);
  void ax_psp_230_sxbx_epvehsxcx(int x_a);
  void ac_psp_230_sxbx_epvehsxcx(int x_a, int channel);
  void ag_psp_230_sxbx_epvehsxcx(int x_a, int zero);
  void ax_psp_230_sxbx_epvehcxbx(int x_a);
  void ac_psp_230_sxbx_epvehcxbx(int x_a, int channel);
  void ag_psp_230_sxbx_epvehcxbx(int x_a, int zero);
  void ax_psp_230_cxbx_epvehuxcx(int x_a);
  void ac_psp_230_cxbx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_cxbx_epvehuxcx(int x_a, int zero);
  void ax_psp_230_cxbx_epvehcxcx(int x_a);
  void ac_psp_230_cxbx_epvehcxcx(int x_a, int channel);
  void ag_psp_230_cxbx_epvehcxcx(int x_a, int zero);
  void ax_psp_230_bxbx_epvehuxbx(int x_a);
  void ac_psp_230_bxbx_epvehuxbx(int x_a, int channel);
  void ag_psp_230_bxbx_epvehuxbx(int x_a, int zero);
  void ax_psp_230_bxbx_epvehcxbx(int x_a);
  void ac_psp_230_bxbx_epvehcxbx(int x_a, int channel);
  void ag_psp_230_bxbx_epvehcxbx(int x_a, int zero);

};
#endif
