#ifndef ppw01nockm_CONTRIBUTION_SET_HPP
#define ppw01nockm_CONTRIBUTION_SET_HPP

using namespace std;

class ppw01nockm_contribution_set : public contribution_set {
private:

public:
  ppw01nockm_contribution_set(){}
  ~ppw01nockm_contribution_set();

  void determination_subprocess_born(int i_a);
  void combination_subprocess_born(int i_a);
  void determination_subprocess_real(int i_a);
  void combination_subprocess_real(int i_a);
  void determination_subprocess_doublereal(int i_a);
  void combination_subprocess_doublereal(int i_a);

};
#endif
