#ifndef ppw01nockm_OBSERVABLE_SET_HPP
#define ppw01nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppw01nockm_observable_set : public observable_set {
private:

public:
  ppw01nockm_observable_set(){}
  ~ppw01nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
