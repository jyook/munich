#ifndef ppw01nockm_EVENT_SET_HPP
#define ppw01nockm_EVENT_SET_HPP

using namespace std;

class ppw01nockm_event_set : public event_set {
private:

public:
  ppw01nockm_event_set(){}
  ~ppw01nockm_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
