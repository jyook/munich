#ifndef ppw01nockm_PHASESPACE_SET_HPP
#define ppw01nockm_PHASESPACE_SET_HPP

using namespace std;

class ppw01nockm_phasespace_set : public phasespace_set {
private:

public:
  ppw01nockm_phasespace_set(){}
  ~ppw01nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_010_dux_wm(int x_a);
  void ac_psp_010_dux_wm(int x_a, int channel);
  void ag_psp_010_dux_wm(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_110_gd_wmu(int x_a);
  void ac_psp_110_gd_wmu(int x_a, int channel);
  void ag_psp_110_gd_wmu(int x_a, int zero);
  void ax_psp_110_gux_wmdx(int x_a);
  void ac_psp_110_gux_wmdx(int x_a, int channel);
  void ag_psp_110_gux_wmdx(int x_a, int zero);
  void ax_psp_110_dux_wmg(int x_a);
  void ac_psp_110_dux_wmg(int x_a, int channel);
  void ag_psp_110_dux_wmg(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_210_gg_wmudx(int x_a);
  void ac_psp_210_gg_wmudx(int x_a, int channel);
  void ag_psp_210_gg_wmudx(int x_a, int zero);
  void ax_psp_210_gd_wmgu(int x_a);
  void ac_psp_210_gd_wmgu(int x_a, int channel);
  void ag_psp_210_gd_wmgu(int x_a, int zero);
  void ax_psp_210_gux_wmgdx(int x_a);
  void ac_psp_210_gux_wmgdx(int x_a, int channel);
  void ag_psp_210_gux_wmgdx(int x_a, int zero);
  void ax_psp_210_dd_wmdu(int x_a);
  void ac_psp_210_dd_wmdu(int x_a, int channel);
  void ag_psp_210_dd_wmdu(int x_a, int zero);
  void ax_psp_210_du_wmuu(int x_a);
  void ac_psp_210_du_wmuu(int x_a, int channel);
  void ag_psp_210_du_wmuu(int x_a, int zero);
  void ax_psp_210_ds_wmdc(int x_a);
  void ac_psp_210_ds_wmdc(int x_a, int channel);
  void ag_psp_210_ds_wmdc(int x_a, int zero);
  void ax_psp_210_dc_wmuc(int x_a);
  void ac_psp_210_dc_wmuc(int x_a, int channel);
  void ag_psp_210_dc_wmuc(int x_a, int zero);
  void ax_psp_210_ddx_wmudx(int x_a);
  void ac_psp_210_ddx_wmudx(int x_a, int channel);
  void ag_psp_210_ddx_wmudx(int x_a, int zero);
  void ax_psp_210_ddx_wmcsx(int x_a);
  void ac_psp_210_ddx_wmcsx(int x_a, int channel);
  void ag_psp_210_ddx_wmcsx(int x_a, int zero);
  void ax_psp_210_dux_wmgg(int x_a);
  void ac_psp_210_dux_wmgg(int x_a, int channel);
  void ag_psp_210_dux_wmgg(int x_a, int zero);
  void ax_psp_210_dux_wmddx(int x_a);
  void ac_psp_210_dux_wmddx(int x_a, int channel);
  void ag_psp_210_dux_wmddx(int x_a, int zero);
  void ax_psp_210_dux_wmuux(int x_a);
  void ac_psp_210_dux_wmuux(int x_a, int channel);
  void ag_psp_210_dux_wmuux(int x_a, int zero);
  void ax_psp_210_dux_wmssx(int x_a);
  void ac_psp_210_dux_wmssx(int x_a, int channel);
  void ag_psp_210_dux_wmssx(int x_a, int zero);
  void ax_psp_210_dux_wmccx(int x_a);
  void ac_psp_210_dux_wmccx(int x_a, int channel);
  void ag_psp_210_dux_wmccx(int x_a, int zero);
  void ax_psp_210_dsx_wmusx(int x_a);
  void ac_psp_210_dsx_wmusx(int x_a, int channel);
  void ag_psp_210_dsx_wmusx(int x_a, int zero);
  void ax_psp_210_dcx_wmdsx(int x_a);
  void ac_psp_210_dcx_wmdsx(int x_a, int channel);
  void ag_psp_210_dcx_wmdsx(int x_a, int zero);
  void ax_psp_210_dcx_wmucx(int x_a);
  void ac_psp_210_dcx_wmucx(int x_a, int channel);
  void ag_psp_210_dcx_wmucx(int x_a, int zero);
  void ax_psp_210_uux_wmudx(int x_a);
  void ac_psp_210_uux_wmudx(int x_a, int channel);
  void ag_psp_210_uux_wmudx(int x_a, int zero);
  void ax_psp_210_uux_wmcsx(int x_a);
  void ac_psp_210_uux_wmcsx(int x_a, int channel);
  void ag_psp_210_uux_wmcsx(int x_a, int zero);
  void ax_psp_210_ucx_wmusx(int x_a);
  void ac_psp_210_ucx_wmusx(int x_a, int channel);
  void ag_psp_210_ucx_wmusx(int x_a, int zero);
  void ax_psp_210_dxux_wmdxdx(int x_a);
  void ac_psp_210_dxux_wmdxdx(int x_a, int channel);
  void ag_psp_210_dxux_wmdxdx(int x_a, int zero);
  void ax_psp_210_dxcx_wmdxsx(int x_a);
  void ac_psp_210_dxcx_wmdxsx(int x_a, int channel);
  void ag_psp_210_dxcx_wmdxsx(int x_a, int zero);
  void ax_psp_210_uxux_wmdxux(int x_a);
  void ac_psp_210_uxux_wmdxux(int x_a, int channel);
  void ag_psp_210_uxux_wmdxux(int x_a, int zero);
  void ax_psp_210_uxcx_wmdxcx(int x_a);
  void ac_psp_210_uxcx_wmdxcx(int x_a, int channel);
  void ag_psp_210_uxcx_wmdxcx(int x_a, int zero);

};
#endif
