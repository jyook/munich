#ifndef ppeeexnex04_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppeeexnex04_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppeeexnex04_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppeeexnex04_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppeeexnex04_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
