#ifndef ppeeexnex04_PHASESPACE_SET_HPP
#define ppeeexnex04_PHASESPACE_SET_HPP

using namespace std;

class ppeeexnex04_phasespace_set : public phasespace_set {
private:

public:
  ppeeexnex04_phasespace_set(){}
  ~ppeeexnex04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_dux_ememepvex(int x_a);
  void ac_psp_040_dux_ememepvex(int x_a, int channel);
  void ag_psp_040_dux_ememepvex(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gd_ememepvexu(int x_a);
  void ac_psp_140_gd_ememepvexu(int x_a, int channel);
  void ag_psp_140_gd_ememepvexu(int x_a, int zero);
  void ax_psp_140_gux_ememepvexdx(int x_a);
  void ac_psp_140_gux_ememepvexdx(int x_a, int channel);
  void ag_psp_140_gux_ememepvexdx(int x_a, int zero);
  void ax_psp_140_dux_ememepvexg(int x_a);
  void ac_psp_140_dux_ememepvexg(int x_a, int channel);
  void ag_psp_140_dux_ememepvexg(int x_a, int zero);
  void ax_psp_050_da_ememepvexu(int x_a);
  void ac_psp_050_da_ememepvexu(int x_a, int channel);
  void ag_psp_050_da_ememepvexu(int x_a, int zero);
  void ax_psp_050_uxa_ememepvexdx(int x_a);
  void ac_psp_050_uxa_ememepvexdx(int x_a, int channel);
  void ag_psp_050_uxa_ememepvexdx(int x_a, int zero);
  void ax_psp_050_dux_ememepvexa(int x_a);
  void ac_psp_050_dux_ememepvexa(int x_a, int channel);
  void ag_psp_050_dux_ememepvexa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_ememepvexudx(int x_a);
  void ac_psp_240_gg_ememepvexudx(int x_a, int channel);
  void ag_psp_240_gg_ememepvexudx(int x_a, int zero);
  void ax_psp_240_gd_ememepvexgu(int x_a);
  void ac_psp_240_gd_ememepvexgu(int x_a, int channel);
  void ag_psp_240_gd_ememepvexgu(int x_a, int zero);
  void ax_psp_240_gux_ememepvexgdx(int x_a);
  void ac_psp_240_gux_ememepvexgdx(int x_a, int channel);
  void ag_psp_240_gux_ememepvexgdx(int x_a, int zero);
  void ax_psp_240_dd_ememepvexdu(int x_a);
  void ac_psp_240_dd_ememepvexdu(int x_a, int channel);
  void ag_psp_240_dd_ememepvexdu(int x_a, int zero);
  void ax_psp_240_du_ememepvexuu(int x_a);
  void ac_psp_240_du_ememepvexuu(int x_a, int channel);
  void ag_psp_240_du_ememepvexuu(int x_a, int zero);
  void ax_psp_240_ds_ememepvexdc(int x_a);
  void ac_psp_240_ds_ememepvexdc(int x_a, int channel);
  void ag_psp_240_ds_ememepvexdc(int x_a, int zero);
  void ax_psp_240_dc_ememepvexuc(int x_a);
  void ac_psp_240_dc_ememepvexuc(int x_a, int channel);
  void ag_psp_240_dc_ememepvexuc(int x_a, int zero);
  void ax_psp_240_ddx_ememepvexudx(int x_a);
  void ac_psp_240_ddx_ememepvexudx(int x_a, int channel);
  void ag_psp_240_ddx_ememepvexudx(int x_a, int zero);
  void ax_psp_240_ddx_ememepvexcsx(int x_a);
  void ac_psp_240_ddx_ememepvexcsx(int x_a, int channel);
  void ag_psp_240_ddx_ememepvexcsx(int x_a, int zero);
  void ax_psp_240_dux_ememepvexgg(int x_a);
  void ac_psp_240_dux_ememepvexgg(int x_a, int channel);
  void ag_psp_240_dux_ememepvexgg(int x_a, int zero);
  void ax_psp_240_dux_ememepvexddx(int x_a);
  void ac_psp_240_dux_ememepvexddx(int x_a, int channel);
  void ag_psp_240_dux_ememepvexddx(int x_a, int zero);
  void ax_psp_240_dux_ememepvexuux(int x_a);
  void ac_psp_240_dux_ememepvexuux(int x_a, int channel);
  void ag_psp_240_dux_ememepvexuux(int x_a, int zero);
  void ax_psp_240_dux_ememepvexssx(int x_a);
  void ac_psp_240_dux_ememepvexssx(int x_a, int channel);
  void ag_psp_240_dux_ememepvexssx(int x_a, int zero);
  void ax_psp_240_dux_ememepvexccx(int x_a);
  void ac_psp_240_dux_ememepvexccx(int x_a, int channel);
  void ag_psp_240_dux_ememepvexccx(int x_a, int zero);
  void ax_psp_240_dsx_ememepvexusx(int x_a);
  void ac_psp_240_dsx_ememepvexusx(int x_a, int channel);
  void ag_psp_240_dsx_ememepvexusx(int x_a, int zero);
  void ax_psp_240_dcx_ememepvexdsx(int x_a);
  void ac_psp_240_dcx_ememepvexdsx(int x_a, int channel);
  void ag_psp_240_dcx_ememepvexdsx(int x_a, int zero);
  void ax_psp_240_dcx_ememepvexucx(int x_a);
  void ac_psp_240_dcx_ememepvexucx(int x_a, int channel);
  void ag_psp_240_dcx_ememepvexucx(int x_a, int zero);
  void ax_psp_240_uux_ememepvexudx(int x_a);
  void ac_psp_240_uux_ememepvexudx(int x_a, int channel);
  void ag_psp_240_uux_ememepvexudx(int x_a, int zero);
  void ax_psp_240_uux_ememepvexcsx(int x_a);
  void ac_psp_240_uux_ememepvexcsx(int x_a, int channel);
  void ag_psp_240_uux_ememepvexcsx(int x_a, int zero);
  void ax_psp_240_ucx_ememepvexusx(int x_a);
  void ac_psp_240_ucx_ememepvexusx(int x_a, int channel);
  void ag_psp_240_ucx_ememepvexusx(int x_a, int zero);
  void ax_psp_240_dxux_ememepvexdxdx(int x_a);
  void ac_psp_240_dxux_ememepvexdxdx(int x_a, int channel);
  void ag_psp_240_dxux_ememepvexdxdx(int x_a, int zero);
  void ax_psp_240_dxcx_ememepvexdxsx(int x_a);
  void ac_psp_240_dxcx_ememepvexdxsx(int x_a, int channel);
  void ag_psp_240_dxcx_ememepvexdxsx(int x_a, int zero);
  void ax_psp_240_uxux_ememepvexdxux(int x_a);
  void ac_psp_240_uxux_ememepvexdxux(int x_a, int channel);
  void ag_psp_240_uxux_ememepvexdxux(int x_a, int zero);
  void ax_psp_240_uxcx_ememepvexdxcx(int x_a);
  void ac_psp_240_uxcx_ememepvexdxcx(int x_a, int channel);
  void ag_psp_240_uxcx_ememepvexdxcx(int x_a, int zero);

};
#endif
