#ifndef ppeexexne04_PHASESPACE_SET_HPP
#define ppeexexne04_PHASESPACE_SET_HPP

using namespace std;

class ppeexexne04_phasespace_set : public phasespace_set {
private:

public:
  ppeexexne04_phasespace_set(){}
  ~ppeexexne04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_udx_emepepve(int x_a);
  void ac_psp_040_udx_emepepve(int x_a, int channel);
  void ag_psp_040_udx_emepepve(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gu_emepepved(int x_a);
  void ac_psp_140_gu_emepepved(int x_a, int channel);
  void ag_psp_140_gu_emepepved(int x_a, int zero);
  void ax_psp_140_gdx_emepepveux(int x_a);
  void ac_psp_140_gdx_emepepveux(int x_a, int channel);
  void ag_psp_140_gdx_emepepveux(int x_a, int zero);
  void ax_psp_140_udx_emepepveg(int x_a);
  void ac_psp_140_udx_emepepveg(int x_a, int channel);
  void ag_psp_140_udx_emepepveg(int x_a, int zero);
  void ax_psp_050_ua_emepepved(int x_a);
  void ac_psp_050_ua_emepepved(int x_a, int channel);
  void ag_psp_050_ua_emepepved(int x_a, int zero);
  void ax_psp_050_dxa_emepepveux(int x_a);
  void ac_psp_050_dxa_emepepveux(int x_a, int channel);
  void ag_psp_050_dxa_emepepveux(int x_a, int zero);
  void ax_psp_050_udx_emepepvea(int x_a);
  void ac_psp_050_udx_emepepvea(int x_a, int channel);
  void ag_psp_050_udx_emepepvea(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_emepepvedux(int x_a);
  void ac_psp_240_gg_emepepvedux(int x_a, int channel);
  void ag_psp_240_gg_emepepvedux(int x_a, int zero);
  void ax_psp_240_gu_emepepvegd(int x_a);
  void ac_psp_240_gu_emepepvegd(int x_a, int channel);
  void ag_psp_240_gu_emepepvegd(int x_a, int zero);
  void ax_psp_240_gdx_emepepvegux(int x_a);
  void ac_psp_240_gdx_emepepvegux(int x_a, int channel);
  void ag_psp_240_gdx_emepepvegux(int x_a, int zero);
  void ax_psp_240_du_emepepvedd(int x_a);
  void ac_psp_240_du_emepepvedd(int x_a, int channel);
  void ag_psp_240_du_emepepvedd(int x_a, int zero);
  void ax_psp_240_dc_emepepveds(int x_a);
  void ac_psp_240_dc_emepepveds(int x_a, int channel);
  void ag_psp_240_dc_emepepveds(int x_a, int zero);
  void ax_psp_240_ddx_emepepvedux(int x_a);
  void ac_psp_240_ddx_emepepvedux(int x_a, int channel);
  void ag_psp_240_ddx_emepepvedux(int x_a, int zero);
  void ax_psp_240_ddx_emepepvescx(int x_a);
  void ac_psp_240_ddx_emepepvescx(int x_a, int channel);
  void ag_psp_240_ddx_emepepvescx(int x_a, int zero);
  void ax_psp_240_dsx_emepepvedcx(int x_a);
  void ac_psp_240_dsx_emepepvedcx(int x_a, int channel);
  void ag_psp_240_dsx_emepepvedcx(int x_a, int zero);
  void ax_psp_240_uu_emepepvedu(int x_a);
  void ac_psp_240_uu_emepepvedu(int x_a, int channel);
  void ag_psp_240_uu_emepepvedu(int x_a, int zero);
  void ax_psp_240_uc_emepepvedc(int x_a);
  void ac_psp_240_uc_emepepvedc(int x_a, int channel);
  void ag_psp_240_uc_emepepvedc(int x_a, int zero);
  void ax_psp_240_udx_emepepvegg(int x_a);
  void ac_psp_240_udx_emepepvegg(int x_a, int channel);
  void ag_psp_240_udx_emepepvegg(int x_a, int zero);
  void ax_psp_240_udx_emepepveddx(int x_a);
  void ac_psp_240_udx_emepepveddx(int x_a, int channel);
  void ag_psp_240_udx_emepepveddx(int x_a, int zero);
  void ax_psp_240_udx_emepepveuux(int x_a);
  void ac_psp_240_udx_emepepveuux(int x_a, int channel);
  void ag_psp_240_udx_emepepveuux(int x_a, int zero);
  void ax_psp_240_udx_emepepvessx(int x_a);
  void ac_psp_240_udx_emepepvessx(int x_a, int channel);
  void ag_psp_240_udx_emepepvessx(int x_a, int zero);
  void ax_psp_240_udx_emepepveccx(int x_a);
  void ac_psp_240_udx_emepepveccx(int x_a, int channel);
  void ag_psp_240_udx_emepepveccx(int x_a, int zero);
  void ax_psp_240_uux_emepepvedux(int x_a);
  void ac_psp_240_uux_emepepvedux(int x_a, int channel);
  void ag_psp_240_uux_emepepvedux(int x_a, int zero);
  void ax_psp_240_uux_emepepvescx(int x_a);
  void ac_psp_240_uux_emepepvescx(int x_a, int channel);
  void ag_psp_240_uux_emepepvescx(int x_a, int zero);
  void ax_psp_240_usx_emepepvedsx(int x_a);
  void ac_psp_240_usx_emepepvedsx(int x_a, int channel);
  void ag_psp_240_usx_emepepvedsx(int x_a, int zero);
  void ax_psp_240_usx_emepepveucx(int x_a);
  void ac_psp_240_usx_emepepveucx(int x_a, int channel);
  void ag_psp_240_usx_emepepveucx(int x_a, int zero);
  void ax_psp_240_ucx_emepepvedcx(int x_a);
  void ac_psp_240_ucx_emepepvedcx(int x_a, int channel);
  void ag_psp_240_ucx_emepepvedcx(int x_a, int zero);
  void ax_psp_240_dxdx_emepepvedxux(int x_a);
  void ac_psp_240_dxdx_emepepvedxux(int x_a, int channel);
  void ag_psp_240_dxdx_emepepvedxux(int x_a, int zero);
  void ax_psp_240_dxux_emepepveuxux(int x_a);
  void ac_psp_240_dxux_emepepveuxux(int x_a, int channel);
  void ag_psp_240_dxux_emepepveuxux(int x_a, int zero);
  void ax_psp_240_dxsx_emepepvedxcx(int x_a);
  void ac_psp_240_dxsx_emepepvedxcx(int x_a, int channel);
  void ag_psp_240_dxsx_emepepvedxcx(int x_a, int zero);
  void ax_psp_240_dxcx_emepepveuxcx(int x_a);
  void ac_psp_240_dxcx_emepepveuxcx(int x_a, int channel);
  void ag_psp_240_dxcx_emepepveuxcx(int x_a, int zero);

};
#endif
