#ifndef ppenex02nockm_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppenex02nockm_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppenex02nockm_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppenex02nockm_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppenex02nockm_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
