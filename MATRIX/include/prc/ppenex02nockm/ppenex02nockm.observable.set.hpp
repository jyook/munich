#ifndef ppenex02nockm_OBSERVABLE_SET_HPP
#define ppenex02nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppenex02nockm_observable_set : public observable_set {
private:

public:
  ppenex02nockm_observable_set(){}
  ~ppenex02nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
