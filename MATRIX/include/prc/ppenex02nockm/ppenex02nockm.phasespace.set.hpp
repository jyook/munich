#ifndef ppenex02nockm_PHASESPACE_SET_HPP
#define ppenex02nockm_PHASESPACE_SET_HPP

using namespace std;

class ppenex02nockm_phasespace_set : public phasespace_set {
private:

public:
  ppenex02nockm_phasespace_set(){}
  ~ppenex02nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_020_dux_emvex(int x_a);
  void ac_psp_020_dux_emvex(int x_a, int channel);
  void ag_psp_020_dux_emvex(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_120_gd_emvexu(int x_a);
  void ac_psp_120_gd_emvexu(int x_a, int channel);
  void ag_psp_120_gd_emvexu(int x_a, int zero);
  void ax_psp_120_gux_emvexdx(int x_a);
  void ac_psp_120_gux_emvexdx(int x_a, int channel);
  void ag_psp_120_gux_emvexdx(int x_a, int zero);
  void ax_psp_120_dux_emvexg(int x_a);
  void ac_psp_120_dux_emvexg(int x_a, int channel);
  void ag_psp_120_dux_emvexg(int x_a, int zero);
  void ax_psp_030_da_emvexu(int x_a);
  void ac_psp_030_da_emvexu(int x_a, int channel);
  void ag_psp_030_da_emvexu(int x_a, int zero);
  void ax_psp_030_uxa_emvexdx(int x_a);
  void ac_psp_030_uxa_emvexdx(int x_a, int channel);
  void ag_psp_030_uxa_emvexdx(int x_a, int zero);
  void ax_psp_030_dux_emvexa(int x_a);
  void ac_psp_030_dux_emvexa(int x_a, int channel);
  void ag_psp_030_dux_emvexa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_220_gg_emvexudx(int x_a);
  void ac_psp_220_gg_emvexudx(int x_a, int channel);
  void ag_psp_220_gg_emvexudx(int x_a, int zero);
  void ax_psp_220_gd_emvexgu(int x_a);
  void ac_psp_220_gd_emvexgu(int x_a, int channel);
  void ag_psp_220_gd_emvexgu(int x_a, int zero);
  void ax_psp_220_gux_emvexgdx(int x_a);
  void ac_psp_220_gux_emvexgdx(int x_a, int channel);
  void ag_psp_220_gux_emvexgdx(int x_a, int zero);
  void ax_psp_220_dd_emvexdu(int x_a);
  void ac_psp_220_dd_emvexdu(int x_a, int channel);
  void ag_psp_220_dd_emvexdu(int x_a, int zero);
  void ax_psp_220_du_emvexuu(int x_a);
  void ac_psp_220_du_emvexuu(int x_a, int channel);
  void ag_psp_220_du_emvexuu(int x_a, int zero);
  void ax_psp_220_ds_emvexdc(int x_a);
  void ac_psp_220_ds_emvexdc(int x_a, int channel);
  void ag_psp_220_ds_emvexdc(int x_a, int zero);
  void ax_psp_220_dc_emvexuc(int x_a);
  void ac_psp_220_dc_emvexuc(int x_a, int channel);
  void ag_psp_220_dc_emvexuc(int x_a, int zero);
  void ax_psp_220_db_emvexub(int x_a);
  void ac_psp_220_db_emvexub(int x_a, int channel);
  void ag_psp_220_db_emvexub(int x_a, int zero);
  void ax_psp_220_ddx_emvexudx(int x_a);
  void ac_psp_220_ddx_emvexudx(int x_a, int channel);
  void ag_psp_220_ddx_emvexudx(int x_a, int zero);
  void ax_psp_220_ddx_emvexcsx(int x_a);
  void ac_psp_220_ddx_emvexcsx(int x_a, int channel);
  void ag_psp_220_ddx_emvexcsx(int x_a, int zero);
  void ax_psp_220_dux_emvexgg(int x_a);
  void ac_psp_220_dux_emvexgg(int x_a, int channel);
  void ag_psp_220_dux_emvexgg(int x_a, int zero);
  void ax_psp_220_dux_emvexddx(int x_a);
  void ac_psp_220_dux_emvexddx(int x_a, int channel);
  void ag_psp_220_dux_emvexddx(int x_a, int zero);
  void ax_psp_220_dux_emvexuux(int x_a);
  void ac_psp_220_dux_emvexuux(int x_a, int channel);
  void ag_psp_220_dux_emvexuux(int x_a, int zero);
  void ax_psp_220_dux_emvexssx(int x_a);
  void ac_psp_220_dux_emvexssx(int x_a, int channel);
  void ag_psp_220_dux_emvexssx(int x_a, int zero);
  void ax_psp_220_dux_emvexccx(int x_a);
  void ac_psp_220_dux_emvexccx(int x_a, int channel);
  void ag_psp_220_dux_emvexccx(int x_a, int zero);
  void ax_psp_220_dux_emvexbbx(int x_a);
  void ac_psp_220_dux_emvexbbx(int x_a, int channel);
  void ag_psp_220_dux_emvexbbx(int x_a, int zero);
  void ax_psp_220_dsx_emvexusx(int x_a);
  void ac_psp_220_dsx_emvexusx(int x_a, int channel);
  void ag_psp_220_dsx_emvexusx(int x_a, int zero);
  void ax_psp_220_dcx_emvexdsx(int x_a);
  void ac_psp_220_dcx_emvexdsx(int x_a, int channel);
  void ag_psp_220_dcx_emvexdsx(int x_a, int zero);
  void ax_psp_220_dcx_emvexucx(int x_a);
  void ac_psp_220_dcx_emvexucx(int x_a, int channel);
  void ag_psp_220_dcx_emvexucx(int x_a, int zero);
  void ax_psp_220_dbx_emvexubx(int x_a);
  void ac_psp_220_dbx_emvexubx(int x_a, int channel);
  void ag_psp_220_dbx_emvexubx(int x_a, int zero);
  void ax_psp_220_uux_emvexudx(int x_a);
  void ac_psp_220_uux_emvexudx(int x_a, int channel);
  void ag_psp_220_uux_emvexudx(int x_a, int zero);
  void ax_psp_220_uux_emvexcsx(int x_a);
  void ac_psp_220_uux_emvexcsx(int x_a, int channel);
  void ag_psp_220_uux_emvexcsx(int x_a, int zero);
  void ax_psp_220_ucx_emvexusx(int x_a);
  void ac_psp_220_ucx_emvexusx(int x_a, int channel);
  void ag_psp_220_ucx_emvexusx(int x_a, int zero);
  void ax_psp_220_bux_emvexbdx(int x_a);
  void ac_psp_220_bux_emvexbdx(int x_a, int channel);
  void ag_psp_220_bux_emvexbdx(int x_a, int zero);
  void ax_psp_220_bbx_emvexudx(int x_a);
  void ac_psp_220_bbx_emvexudx(int x_a, int channel);
  void ag_psp_220_bbx_emvexudx(int x_a, int zero);
  void ax_psp_220_dxux_emvexdxdx(int x_a);
  void ac_psp_220_dxux_emvexdxdx(int x_a, int channel);
  void ag_psp_220_dxux_emvexdxdx(int x_a, int zero);
  void ax_psp_220_dxcx_emvexdxsx(int x_a);
  void ac_psp_220_dxcx_emvexdxsx(int x_a, int channel);
  void ag_psp_220_dxcx_emvexdxsx(int x_a, int zero);
  void ax_psp_220_uxux_emvexdxux(int x_a);
  void ac_psp_220_uxux_emvexdxux(int x_a, int channel);
  void ag_psp_220_uxux_emvexdxux(int x_a, int zero);
  void ax_psp_220_uxcx_emvexdxcx(int x_a);
  void ac_psp_220_uxcx_emvexdxcx(int x_a, int channel);
  void ag_psp_220_uxcx_emvexdxcx(int x_a, int zero);
  void ax_psp_220_uxbx_emvexdxbx(int x_a);
  void ac_psp_220_uxbx_emvexdxbx(int x_a, int channel);
  void ag_psp_220_uxbx_emvexdxbx(int x_a, int zero);

};
#endif
