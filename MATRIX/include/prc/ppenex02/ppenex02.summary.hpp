#ifndef ppenex02_SUMMARY_HPP
#define ppenex02_SUMMARY_HPP

using namespace std;

class ppenex02_summary_generic : public summary_generic {
private:

public:
  ppenex02_summary_generic(){}
  ~ppenex02_summary_generic(){}

  ppenex02_summary_generic(munich * xmunich);

  void initialization_summary_order(size_t n_order);
  void initialization_summary_list(size_t n_list);

};

class ppenex02_summary_order : public summary_order {
private:

public:
  ppenex02_summary_order(){}
  ~ppenex02_summary_order(){}

};

class ppenex02_summary_list : public summary_list {
private:

public:
  ppenex02_summary_list(){}
  ~ppenex02_summary_list(){}

  void initialization_summary_contribution(size_t n_contribution);

};

class ppenex02_summary_contribution : public summary_contribution {
private:

public:
  ppenex02_summary_contribution(){}
  ~ppenex02_summary_contribution(){}

  void list_subprocess_born();
  void list_subprocess_C_QCD();
  void list_subprocess_C_QEW();
  void list_subprocess_V_QCD();
  void list_subprocess_V_QEW();
  void list_subprocess_C2_QCD();
  void list_subprocess_V2_QCD();
  void list_subprocess_R_QCD();
  void list_subprocess_R_QEW();
  void list_subprocess_RC_QCD();
  void list_subprocess_RV_QCD();
  void list_subprocess_RR_QCD();

};
#endif
