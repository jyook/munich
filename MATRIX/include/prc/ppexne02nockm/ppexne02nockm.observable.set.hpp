#ifndef ppexne02nockm_OBSERVABLE_SET_HPP
#define ppexne02nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppexne02nockm_observable_set : public observable_set {
private:

public:
  ppexne02nockm_observable_set(){}
  ~ppexne02nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
