#ifndef ppexne02nockm_EVENT_SET_HPP
#define ppexne02nockm_EVENT_SET_HPP

using namespace std;

class ppexne02nockm_event_set : public event_set {
private:

public:
  ppexne02nockm_event_set(){}
  ~ppexne02nockm_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
