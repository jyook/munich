#ifndef ppexne02nockm_PHASESPACE_SET_HPP
#define ppexne02nockm_PHASESPACE_SET_HPP

using namespace std;

class ppexne02nockm_phasespace_set : public phasespace_set {
private:

public:
  ppexne02nockm_phasespace_set(){}
  ~ppexne02nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_020_udx_epve(int x_a);
  void ac_psp_020_udx_epve(int x_a, int channel);
  void ag_psp_020_udx_epve(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_120_gu_epved(int x_a);
  void ac_psp_120_gu_epved(int x_a, int channel);
  void ag_psp_120_gu_epved(int x_a, int zero);
  void ax_psp_120_gdx_epveux(int x_a);
  void ac_psp_120_gdx_epveux(int x_a, int channel);
  void ag_psp_120_gdx_epveux(int x_a, int zero);
  void ax_psp_120_udx_epveg(int x_a);
  void ac_psp_120_udx_epveg(int x_a, int channel);
  void ag_psp_120_udx_epveg(int x_a, int zero);
  void ax_psp_030_ua_epved(int x_a);
  void ac_psp_030_ua_epved(int x_a, int channel);
  void ag_psp_030_ua_epved(int x_a, int zero);
  void ax_psp_030_dxa_epveux(int x_a);
  void ac_psp_030_dxa_epveux(int x_a, int channel);
  void ag_psp_030_dxa_epveux(int x_a, int zero);
  void ax_psp_030_udx_epvea(int x_a);
  void ac_psp_030_udx_epvea(int x_a, int channel);
  void ag_psp_030_udx_epvea(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_220_gg_epvedux(int x_a);
  void ac_psp_220_gg_epvedux(int x_a, int channel);
  void ag_psp_220_gg_epvedux(int x_a, int zero);
  void ax_psp_220_gu_epvegd(int x_a);
  void ac_psp_220_gu_epvegd(int x_a, int channel);
  void ag_psp_220_gu_epvegd(int x_a, int zero);
  void ax_psp_220_gdx_epvegux(int x_a);
  void ac_psp_220_gdx_epvegux(int x_a, int channel);
  void ag_psp_220_gdx_epvegux(int x_a, int zero);
  void ax_psp_220_du_epvedd(int x_a);
  void ac_psp_220_du_epvedd(int x_a, int channel);
  void ag_psp_220_du_epvedd(int x_a, int zero);
  void ax_psp_220_dc_epveds(int x_a);
  void ac_psp_220_dc_epveds(int x_a, int channel);
  void ag_psp_220_dc_epveds(int x_a, int zero);
  void ax_psp_220_ddx_epvedux(int x_a);
  void ac_psp_220_ddx_epvedux(int x_a, int channel);
  void ag_psp_220_ddx_epvedux(int x_a, int zero);
  void ax_psp_220_ddx_epvescx(int x_a);
  void ac_psp_220_ddx_epvescx(int x_a, int channel);
  void ag_psp_220_ddx_epvescx(int x_a, int zero);
  void ax_psp_220_dsx_epvedcx(int x_a);
  void ac_psp_220_dsx_epvedcx(int x_a, int channel);
  void ag_psp_220_dsx_epvedcx(int x_a, int zero);
  void ax_psp_220_uu_epvedu(int x_a);
  void ac_psp_220_uu_epvedu(int x_a, int channel);
  void ag_psp_220_uu_epvedu(int x_a, int zero);
  void ax_psp_220_uc_epvedc(int x_a);
  void ac_psp_220_uc_epvedc(int x_a, int channel);
  void ag_psp_220_uc_epvedc(int x_a, int zero);
  void ax_psp_220_ub_epvedb(int x_a);
  void ac_psp_220_ub_epvedb(int x_a, int channel);
  void ag_psp_220_ub_epvedb(int x_a, int zero);
  void ax_psp_220_udx_epvegg(int x_a);
  void ac_psp_220_udx_epvegg(int x_a, int channel);
  void ag_psp_220_udx_epvegg(int x_a, int zero);
  void ax_psp_220_udx_epveddx(int x_a);
  void ac_psp_220_udx_epveddx(int x_a, int channel);
  void ag_psp_220_udx_epveddx(int x_a, int zero);
  void ax_psp_220_udx_epveuux(int x_a);
  void ac_psp_220_udx_epveuux(int x_a, int channel);
  void ag_psp_220_udx_epveuux(int x_a, int zero);
  void ax_psp_220_udx_epvessx(int x_a);
  void ac_psp_220_udx_epvessx(int x_a, int channel);
  void ag_psp_220_udx_epvessx(int x_a, int zero);
  void ax_psp_220_udx_epveccx(int x_a);
  void ac_psp_220_udx_epveccx(int x_a, int channel);
  void ag_psp_220_udx_epveccx(int x_a, int zero);
  void ax_psp_220_udx_epvebbx(int x_a);
  void ac_psp_220_udx_epvebbx(int x_a, int channel);
  void ag_psp_220_udx_epvebbx(int x_a, int zero);
  void ax_psp_220_uux_epvedux(int x_a);
  void ac_psp_220_uux_epvedux(int x_a, int channel);
  void ag_psp_220_uux_epvedux(int x_a, int zero);
  void ax_psp_220_uux_epvescx(int x_a);
  void ac_psp_220_uux_epvescx(int x_a, int channel);
  void ag_psp_220_uux_epvescx(int x_a, int zero);
  void ax_psp_220_usx_epvedsx(int x_a);
  void ac_psp_220_usx_epvedsx(int x_a, int channel);
  void ag_psp_220_usx_epvedsx(int x_a, int zero);
  void ax_psp_220_usx_epveucx(int x_a);
  void ac_psp_220_usx_epveucx(int x_a, int channel);
  void ag_psp_220_usx_epveucx(int x_a, int zero);
  void ax_psp_220_ucx_epvedcx(int x_a);
  void ac_psp_220_ucx_epvedcx(int x_a, int channel);
  void ag_psp_220_ucx_epvedcx(int x_a, int zero);
  void ax_psp_220_ubx_epvedbx(int x_a);
  void ac_psp_220_ubx_epvedbx(int x_a, int channel);
  void ag_psp_220_ubx_epvedbx(int x_a, int zero);
  void ax_psp_220_bdx_epvebux(int x_a);
  void ac_psp_220_bdx_epvebux(int x_a, int channel);
  void ag_psp_220_bdx_epvebux(int x_a, int zero);
  void ax_psp_220_bbx_epvedux(int x_a);
  void ac_psp_220_bbx_epvedux(int x_a, int channel);
  void ag_psp_220_bbx_epvedux(int x_a, int zero);
  void ax_psp_220_dxdx_epvedxux(int x_a);
  void ac_psp_220_dxdx_epvedxux(int x_a, int channel);
  void ag_psp_220_dxdx_epvedxux(int x_a, int zero);
  void ax_psp_220_dxux_epveuxux(int x_a);
  void ac_psp_220_dxux_epveuxux(int x_a, int channel);
  void ag_psp_220_dxux_epveuxux(int x_a, int zero);
  void ax_psp_220_dxsx_epvedxcx(int x_a);
  void ac_psp_220_dxsx_epvedxcx(int x_a, int channel);
  void ag_psp_220_dxsx_epvedxcx(int x_a, int zero);
  void ax_psp_220_dxcx_epveuxcx(int x_a);
  void ac_psp_220_dxcx_epveuxcx(int x_a, int channel);
  void ag_psp_220_dxcx_epveuxcx(int x_a, int zero);
  void ax_psp_220_dxbx_epveuxbx(int x_a);
  void ac_psp_220_dxbx_epveuxbx(int x_a, int channel);
  void ag_psp_220_dxbx_epveuxbx(int x_a, int zero);

};
#endif
