#ifndef ppz01_OBSERVABLE_SET_HPP
#define ppz01_OBSERVABLE_SET_HPP

using namespace std;

class ppz01_observable_set : public observable_set {
private:

public:
  ppz01_observable_set(){}
  ~ppz01_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
