#ifndef ppz01_EVENT_SET_HPP
#define ppz01_EVENT_SET_HPP

using namespace std;

class ppz01_event_set : public event_set {
private:

public:
  ppz01_event_set(){}
  ~ppz01_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
