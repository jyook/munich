#ifndef ppnenex02_OBSERVABLE_SET_HPP
#define ppnenex02_OBSERVABLE_SET_HPP

using namespace std;

class ppnenex02_observable_set : public observable_set {
private:

public:
  ppnenex02_observable_set(){}
  ~ppnenex02_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
