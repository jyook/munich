#ifndef ppaa02_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppaa02_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppaa02_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppaa02_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppaa02_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
