#ifndef ppenexh03nockm_OBSERVABLE_SET_HPP
#define ppenexh03nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppenexh03nockm_observable_set : public observable_set {
private:

public:
  ppenexh03nockm_observable_set(){}
  ~ppenexh03nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
