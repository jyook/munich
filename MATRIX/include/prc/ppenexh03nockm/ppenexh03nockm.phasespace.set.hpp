#ifndef ppenexh03nockm_PHASESPACE_SET_HPP
#define ppenexh03nockm_PHASESPACE_SET_HPP

using namespace std;

class ppenexh03nockm_phasespace_set : public phasespace_set {
private:

public:
  ppenexh03nockm_phasespace_set(){}
  ~ppenexh03nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_030_dux_emvexh(int x_a);
  void ac_psp_030_dux_emvexh(int x_a, int channel);
  void ag_psp_030_dux_emvexh(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_130_gd_emvexhu(int x_a);
  void ac_psp_130_gd_emvexhu(int x_a, int channel);
  void ag_psp_130_gd_emvexhu(int x_a, int zero);
  void ax_psp_130_gux_emvexhdx(int x_a);
  void ac_psp_130_gux_emvexhdx(int x_a, int channel);
  void ag_psp_130_gux_emvexhdx(int x_a, int zero);
  void ax_psp_130_dux_emvexhg(int x_a);
  void ac_psp_130_dux_emvexhg(int x_a, int channel);
  void ag_psp_130_dux_emvexhg(int x_a, int zero);
  void ax_psp_040_da_emvexhu(int x_a);
  void ac_psp_040_da_emvexhu(int x_a, int channel);
  void ag_psp_040_da_emvexhu(int x_a, int zero);
  void ax_psp_040_uxa_emvexhdx(int x_a);
  void ac_psp_040_uxa_emvexhdx(int x_a, int channel);
  void ag_psp_040_uxa_emvexhdx(int x_a, int zero);
  void ax_psp_040_dux_emvexha(int x_a);
  void ac_psp_040_dux_emvexha(int x_a, int channel);
  void ag_psp_040_dux_emvexha(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_230_gg_emvexhudx(int x_a);
  void ac_psp_230_gg_emvexhudx(int x_a, int channel);
  void ag_psp_230_gg_emvexhudx(int x_a, int zero);
  void ax_psp_230_gd_emvexhgu(int x_a);
  void ac_psp_230_gd_emvexhgu(int x_a, int channel);
  void ag_psp_230_gd_emvexhgu(int x_a, int zero);
  void ax_psp_230_gux_emvexhgdx(int x_a);
  void ac_psp_230_gux_emvexhgdx(int x_a, int channel);
  void ag_psp_230_gux_emvexhgdx(int x_a, int zero);
  void ax_psp_230_dd_emvexhdu(int x_a);
  void ac_psp_230_dd_emvexhdu(int x_a, int channel);
  void ag_psp_230_dd_emvexhdu(int x_a, int zero);
  void ax_psp_230_du_emvexhuu(int x_a);
  void ac_psp_230_du_emvexhuu(int x_a, int channel);
  void ag_psp_230_du_emvexhuu(int x_a, int zero);
  void ax_psp_230_ds_emvexhdc(int x_a);
  void ac_psp_230_ds_emvexhdc(int x_a, int channel);
  void ag_psp_230_ds_emvexhdc(int x_a, int zero);
  void ax_psp_230_dc_emvexhuc(int x_a);
  void ac_psp_230_dc_emvexhuc(int x_a, int channel);
  void ag_psp_230_dc_emvexhuc(int x_a, int zero);
  void ax_psp_230_ddx_emvexhudx(int x_a);
  void ac_psp_230_ddx_emvexhudx(int x_a, int channel);
  void ag_psp_230_ddx_emvexhudx(int x_a, int zero);
  void ax_psp_230_ddx_emvexhcsx(int x_a);
  void ac_psp_230_ddx_emvexhcsx(int x_a, int channel);
  void ag_psp_230_ddx_emvexhcsx(int x_a, int zero);
  void ax_psp_230_dux_emvexhgg(int x_a);
  void ac_psp_230_dux_emvexhgg(int x_a, int channel);
  void ag_psp_230_dux_emvexhgg(int x_a, int zero);
  void ax_psp_230_dux_emvexhddx(int x_a);
  void ac_psp_230_dux_emvexhddx(int x_a, int channel);
  void ag_psp_230_dux_emvexhddx(int x_a, int zero);
  void ax_psp_230_dux_emvexhuux(int x_a);
  void ac_psp_230_dux_emvexhuux(int x_a, int channel);
  void ag_psp_230_dux_emvexhuux(int x_a, int zero);
  void ax_psp_230_dux_emvexhssx(int x_a);
  void ac_psp_230_dux_emvexhssx(int x_a, int channel);
  void ag_psp_230_dux_emvexhssx(int x_a, int zero);
  void ax_psp_230_dux_emvexhccx(int x_a);
  void ac_psp_230_dux_emvexhccx(int x_a, int channel);
  void ag_psp_230_dux_emvexhccx(int x_a, int zero);
  void ax_psp_230_dsx_emvexhusx(int x_a);
  void ac_psp_230_dsx_emvexhusx(int x_a, int channel);
  void ag_psp_230_dsx_emvexhusx(int x_a, int zero);
  void ax_psp_230_dcx_emvexhdsx(int x_a);
  void ac_psp_230_dcx_emvexhdsx(int x_a, int channel);
  void ag_psp_230_dcx_emvexhdsx(int x_a, int zero);
  void ax_psp_230_dcx_emvexhucx(int x_a);
  void ac_psp_230_dcx_emvexhucx(int x_a, int channel);
  void ag_psp_230_dcx_emvexhucx(int x_a, int zero);
  void ax_psp_230_uux_emvexhudx(int x_a);
  void ac_psp_230_uux_emvexhudx(int x_a, int channel);
  void ag_psp_230_uux_emvexhudx(int x_a, int zero);
  void ax_psp_230_uux_emvexhcsx(int x_a);
  void ac_psp_230_uux_emvexhcsx(int x_a, int channel);
  void ag_psp_230_uux_emvexhcsx(int x_a, int zero);
  void ax_psp_230_ucx_emvexhusx(int x_a);
  void ac_psp_230_ucx_emvexhusx(int x_a, int channel);
  void ag_psp_230_ucx_emvexhusx(int x_a, int zero);
  void ax_psp_230_dxux_emvexhdxdx(int x_a);
  void ac_psp_230_dxux_emvexhdxdx(int x_a, int channel);
  void ag_psp_230_dxux_emvexhdxdx(int x_a, int zero);
  void ax_psp_230_dxcx_emvexhdxsx(int x_a);
  void ac_psp_230_dxcx_emvexhdxsx(int x_a, int channel);
  void ag_psp_230_dxcx_emvexhdxsx(int x_a, int zero);
  void ax_psp_230_uxux_emvexhdxux(int x_a);
  void ac_psp_230_uxux_emvexhdxux(int x_a, int channel);
  void ag_psp_230_uxux_emvexhdxux(int x_a, int zero);
  void ax_psp_230_uxcx_emvexhdxcx(int x_a);
  void ac_psp_230_uxcx_emvexhdxcx(int x_a, int channel);
  void ag_psp_230_uxcx_emvexhdxcx(int x_a, int zero);

};
#endif
