#ifndef ppemexnmx04_PHASESPACE_SET_HPP
#define ppemexnmx04_PHASESPACE_SET_HPP

using namespace std;

class ppemexnmx04_phasespace_set : public phasespace_set {
private:

public:
  ppemexnmx04_phasespace_set(){}
  ~ppemexnmx04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_dux_emmumepvmx(int x_a);
  void ac_psp_040_dux_emmumepvmx(int x_a, int channel);
  void ag_psp_040_dux_emmumepvmx(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gd_emmumepvmxu(int x_a);
  void ac_psp_140_gd_emmumepvmxu(int x_a, int channel);
  void ag_psp_140_gd_emmumepvmxu(int x_a, int zero);
  void ax_psp_140_gux_emmumepvmxdx(int x_a);
  void ac_psp_140_gux_emmumepvmxdx(int x_a, int channel);
  void ag_psp_140_gux_emmumepvmxdx(int x_a, int zero);
  void ax_psp_140_dux_emmumepvmxg(int x_a);
  void ac_psp_140_dux_emmumepvmxg(int x_a, int channel);
  void ag_psp_140_dux_emmumepvmxg(int x_a, int zero);
  void ax_psp_050_da_emmumepvmxu(int x_a);
  void ac_psp_050_da_emmumepvmxu(int x_a, int channel);
  void ag_psp_050_da_emmumepvmxu(int x_a, int zero);
  void ax_psp_050_uxa_emmumepvmxdx(int x_a);
  void ac_psp_050_uxa_emmumepvmxdx(int x_a, int channel);
  void ag_psp_050_uxa_emmumepvmxdx(int x_a, int zero);
  void ax_psp_050_dux_emmumepvmxa(int x_a);
  void ac_psp_050_dux_emmumepvmxa(int x_a, int channel);
  void ag_psp_050_dux_emmumepvmxa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_emmumepvmxudx(int x_a);
  void ac_psp_240_gg_emmumepvmxudx(int x_a, int channel);
  void ag_psp_240_gg_emmumepvmxudx(int x_a, int zero);
  void ax_psp_240_gd_emmumepvmxgu(int x_a);
  void ac_psp_240_gd_emmumepvmxgu(int x_a, int channel);
  void ag_psp_240_gd_emmumepvmxgu(int x_a, int zero);
  void ax_psp_240_gux_emmumepvmxgdx(int x_a);
  void ac_psp_240_gux_emmumepvmxgdx(int x_a, int channel);
  void ag_psp_240_gux_emmumepvmxgdx(int x_a, int zero);
  void ax_psp_240_dd_emmumepvmxdu(int x_a);
  void ac_psp_240_dd_emmumepvmxdu(int x_a, int channel);
  void ag_psp_240_dd_emmumepvmxdu(int x_a, int zero);
  void ax_psp_240_du_emmumepvmxuu(int x_a);
  void ac_psp_240_du_emmumepvmxuu(int x_a, int channel);
  void ag_psp_240_du_emmumepvmxuu(int x_a, int zero);
  void ax_psp_240_ds_emmumepvmxdc(int x_a);
  void ac_psp_240_ds_emmumepvmxdc(int x_a, int channel);
  void ag_psp_240_ds_emmumepvmxdc(int x_a, int zero);
  void ax_psp_240_dc_emmumepvmxuc(int x_a);
  void ac_psp_240_dc_emmumepvmxuc(int x_a, int channel);
  void ag_psp_240_dc_emmumepvmxuc(int x_a, int zero);
  void ax_psp_240_ddx_emmumepvmxudx(int x_a);
  void ac_psp_240_ddx_emmumepvmxudx(int x_a, int channel);
  void ag_psp_240_ddx_emmumepvmxudx(int x_a, int zero);
  void ax_psp_240_ddx_emmumepvmxcsx(int x_a);
  void ac_psp_240_ddx_emmumepvmxcsx(int x_a, int channel);
  void ag_psp_240_ddx_emmumepvmxcsx(int x_a, int zero);
  void ax_psp_240_dux_emmumepvmxgg(int x_a);
  void ac_psp_240_dux_emmumepvmxgg(int x_a, int channel);
  void ag_psp_240_dux_emmumepvmxgg(int x_a, int zero);
  void ax_psp_240_dux_emmumepvmxddx(int x_a);
  void ac_psp_240_dux_emmumepvmxddx(int x_a, int channel);
  void ag_psp_240_dux_emmumepvmxddx(int x_a, int zero);
  void ax_psp_240_dux_emmumepvmxuux(int x_a);
  void ac_psp_240_dux_emmumepvmxuux(int x_a, int channel);
  void ag_psp_240_dux_emmumepvmxuux(int x_a, int zero);
  void ax_psp_240_dux_emmumepvmxssx(int x_a);
  void ac_psp_240_dux_emmumepvmxssx(int x_a, int channel);
  void ag_psp_240_dux_emmumepvmxssx(int x_a, int zero);
  void ax_psp_240_dux_emmumepvmxccx(int x_a);
  void ac_psp_240_dux_emmumepvmxccx(int x_a, int channel);
  void ag_psp_240_dux_emmumepvmxccx(int x_a, int zero);
  void ax_psp_240_dsx_emmumepvmxusx(int x_a);
  void ac_psp_240_dsx_emmumepvmxusx(int x_a, int channel);
  void ag_psp_240_dsx_emmumepvmxusx(int x_a, int zero);
  void ax_psp_240_dcx_emmumepvmxdsx(int x_a);
  void ac_psp_240_dcx_emmumepvmxdsx(int x_a, int channel);
  void ag_psp_240_dcx_emmumepvmxdsx(int x_a, int zero);
  void ax_psp_240_dcx_emmumepvmxucx(int x_a);
  void ac_psp_240_dcx_emmumepvmxucx(int x_a, int channel);
  void ag_psp_240_dcx_emmumepvmxucx(int x_a, int zero);
  void ax_psp_240_uux_emmumepvmxudx(int x_a);
  void ac_psp_240_uux_emmumepvmxudx(int x_a, int channel);
  void ag_psp_240_uux_emmumepvmxudx(int x_a, int zero);
  void ax_psp_240_uux_emmumepvmxcsx(int x_a);
  void ac_psp_240_uux_emmumepvmxcsx(int x_a, int channel);
  void ag_psp_240_uux_emmumepvmxcsx(int x_a, int zero);
  void ax_psp_240_ucx_emmumepvmxusx(int x_a);
  void ac_psp_240_ucx_emmumepvmxusx(int x_a, int channel);
  void ag_psp_240_ucx_emmumepvmxusx(int x_a, int zero);
  void ax_psp_240_dxux_emmumepvmxdxdx(int x_a);
  void ac_psp_240_dxux_emmumepvmxdxdx(int x_a, int channel);
  void ag_psp_240_dxux_emmumepvmxdxdx(int x_a, int zero);
  void ax_psp_240_dxcx_emmumepvmxdxsx(int x_a);
  void ac_psp_240_dxcx_emmumepvmxdxsx(int x_a, int channel);
  void ag_psp_240_dxcx_emmumepvmxdxsx(int x_a, int zero);
  void ax_psp_240_uxux_emmumepvmxdxux(int x_a);
  void ac_psp_240_uxux_emmumepvmxdxux(int x_a, int channel);
  void ag_psp_240_uxux_emmumepvmxdxux(int x_a, int zero);
  void ax_psp_240_uxcx_emmumepvmxdxcx(int x_a);
  void ac_psp_240_uxcx_emmumepvmxdxcx(int x_a, int channel);
  void ag_psp_240_uxcx_emmumepvmxdxcx(int x_a, int zero);

};
#endif
