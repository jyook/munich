#ifndef pph21_OBSERVABLE_SET_HPP
#define pph21_OBSERVABLE_SET_HPP

using namespace std;

class pph21_observable_set : public observable_set {
private:

public:
  pph21_observable_set(){}
  ~pph21_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
