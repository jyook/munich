#ifndef ppeexmxnm04_PHASESPACE_SET_HPP
#define ppeexmxnm04_PHASESPACE_SET_HPP

using namespace std;

class ppeexmxnm04_phasespace_set : public phasespace_set {
private:

public:
  ppeexmxnm04_phasespace_set(){}
  ~ppeexmxnm04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_udx_emepmupvm(int x_a);
  void ac_psp_040_udx_emepmupvm(int x_a, int channel);
  void ag_psp_040_udx_emepmupvm(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gu_emepmupvmd(int x_a);
  void ac_psp_140_gu_emepmupvmd(int x_a, int channel);
  void ag_psp_140_gu_emepmupvmd(int x_a, int zero);
  void ax_psp_140_gdx_emepmupvmux(int x_a);
  void ac_psp_140_gdx_emepmupvmux(int x_a, int channel);
  void ag_psp_140_gdx_emepmupvmux(int x_a, int zero);
  void ax_psp_140_udx_emepmupvmg(int x_a);
  void ac_psp_140_udx_emepmupvmg(int x_a, int channel);
  void ag_psp_140_udx_emepmupvmg(int x_a, int zero);
  void ax_psp_050_ua_emepmupvmd(int x_a);
  void ac_psp_050_ua_emepmupvmd(int x_a, int channel);
  void ag_psp_050_ua_emepmupvmd(int x_a, int zero);
  void ax_psp_050_dxa_emepmupvmux(int x_a);
  void ac_psp_050_dxa_emepmupvmux(int x_a, int channel);
  void ag_psp_050_dxa_emepmupvmux(int x_a, int zero);
  void ax_psp_050_udx_emepmupvma(int x_a);
  void ac_psp_050_udx_emepmupvma(int x_a, int channel);
  void ag_psp_050_udx_emepmupvma(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_emepmupvmdux(int x_a);
  void ac_psp_240_gg_emepmupvmdux(int x_a, int channel);
  void ag_psp_240_gg_emepmupvmdux(int x_a, int zero);
  void ax_psp_240_gu_emepmupvmgd(int x_a);
  void ac_psp_240_gu_emepmupvmgd(int x_a, int channel);
  void ag_psp_240_gu_emepmupvmgd(int x_a, int zero);
  void ax_psp_240_gdx_emepmupvmgux(int x_a);
  void ac_psp_240_gdx_emepmupvmgux(int x_a, int channel);
  void ag_psp_240_gdx_emepmupvmgux(int x_a, int zero);
  void ax_psp_240_du_emepmupvmdd(int x_a);
  void ac_psp_240_du_emepmupvmdd(int x_a, int channel);
  void ag_psp_240_du_emepmupvmdd(int x_a, int zero);
  void ax_psp_240_dc_emepmupvmds(int x_a);
  void ac_psp_240_dc_emepmupvmds(int x_a, int channel);
  void ag_psp_240_dc_emepmupvmds(int x_a, int zero);
  void ax_psp_240_ddx_emepmupvmdux(int x_a);
  void ac_psp_240_ddx_emepmupvmdux(int x_a, int channel);
  void ag_psp_240_ddx_emepmupvmdux(int x_a, int zero);
  void ax_psp_240_ddx_emepmupvmscx(int x_a);
  void ac_psp_240_ddx_emepmupvmscx(int x_a, int channel);
  void ag_psp_240_ddx_emepmupvmscx(int x_a, int zero);
  void ax_psp_240_dsx_emepmupvmdcx(int x_a);
  void ac_psp_240_dsx_emepmupvmdcx(int x_a, int channel);
  void ag_psp_240_dsx_emepmupvmdcx(int x_a, int zero);
  void ax_psp_240_uu_emepmupvmdu(int x_a);
  void ac_psp_240_uu_emepmupvmdu(int x_a, int channel);
  void ag_psp_240_uu_emepmupvmdu(int x_a, int zero);
  void ax_psp_240_uc_emepmupvmdc(int x_a);
  void ac_psp_240_uc_emepmupvmdc(int x_a, int channel);
  void ag_psp_240_uc_emepmupvmdc(int x_a, int zero);
  void ax_psp_240_udx_emepmupvmgg(int x_a);
  void ac_psp_240_udx_emepmupvmgg(int x_a, int channel);
  void ag_psp_240_udx_emepmupvmgg(int x_a, int zero);
  void ax_psp_240_udx_emepmupvmddx(int x_a);
  void ac_psp_240_udx_emepmupvmddx(int x_a, int channel);
  void ag_psp_240_udx_emepmupvmddx(int x_a, int zero);
  void ax_psp_240_udx_emepmupvmuux(int x_a);
  void ac_psp_240_udx_emepmupvmuux(int x_a, int channel);
  void ag_psp_240_udx_emepmupvmuux(int x_a, int zero);
  void ax_psp_240_udx_emepmupvmssx(int x_a);
  void ac_psp_240_udx_emepmupvmssx(int x_a, int channel);
  void ag_psp_240_udx_emepmupvmssx(int x_a, int zero);
  void ax_psp_240_udx_emepmupvmccx(int x_a);
  void ac_psp_240_udx_emepmupvmccx(int x_a, int channel);
  void ag_psp_240_udx_emepmupvmccx(int x_a, int zero);
  void ax_psp_240_uux_emepmupvmdux(int x_a);
  void ac_psp_240_uux_emepmupvmdux(int x_a, int channel);
  void ag_psp_240_uux_emepmupvmdux(int x_a, int zero);
  void ax_psp_240_uux_emepmupvmscx(int x_a);
  void ac_psp_240_uux_emepmupvmscx(int x_a, int channel);
  void ag_psp_240_uux_emepmupvmscx(int x_a, int zero);
  void ax_psp_240_usx_emepmupvmdsx(int x_a);
  void ac_psp_240_usx_emepmupvmdsx(int x_a, int channel);
  void ag_psp_240_usx_emepmupvmdsx(int x_a, int zero);
  void ax_psp_240_usx_emepmupvmucx(int x_a);
  void ac_psp_240_usx_emepmupvmucx(int x_a, int channel);
  void ag_psp_240_usx_emepmupvmucx(int x_a, int zero);
  void ax_psp_240_ucx_emepmupvmdcx(int x_a);
  void ac_psp_240_ucx_emepmupvmdcx(int x_a, int channel);
  void ag_psp_240_ucx_emepmupvmdcx(int x_a, int zero);
  void ax_psp_240_dxdx_emepmupvmdxux(int x_a);
  void ac_psp_240_dxdx_emepmupvmdxux(int x_a, int channel);
  void ag_psp_240_dxdx_emepmupvmdxux(int x_a, int zero);
  void ax_psp_240_dxux_emepmupvmuxux(int x_a);
  void ac_psp_240_dxux_emepmupvmuxux(int x_a, int channel);
  void ag_psp_240_dxux_emepmupvmuxux(int x_a, int zero);
  void ax_psp_240_dxsx_emepmupvmdxcx(int x_a);
  void ac_psp_240_dxsx_emepmupvmdxcx(int x_a, int channel);
  void ag_psp_240_dxsx_emepmupvmdxcx(int x_a, int zero);
  void ax_psp_240_dxcx_emepmupvmuxcx(int x_a);
  void ac_psp_240_dxcx_emepmupvmuxcx(int x_a, int channel);
  void ag_psp_240_dxcx_emepmupvmuxcx(int x_a, int zero);

};
#endif
