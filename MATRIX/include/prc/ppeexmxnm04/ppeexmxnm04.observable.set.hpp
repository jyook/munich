#ifndef ppeexmxnm04_OBSERVABLE_SET_HPP
#define ppeexmxnm04_OBSERVABLE_SET_HPP

using namespace std;

class ppeexmxnm04_observable_set : public observable_set {
private:

public:
  ppeexmxnm04_observable_set(){}
  ~ppeexmxnm04_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
