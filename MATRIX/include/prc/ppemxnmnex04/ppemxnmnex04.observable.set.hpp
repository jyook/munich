#ifndef ppemxnmnex04_OBSERVABLE_SET_HPP
#define ppemxnmnex04_OBSERVABLE_SET_HPP

using namespace std;

class ppemxnmnex04_observable_set : public observable_set {
private:

public:
  ppemxnmnex04_observable_set(){}
  ~ppemxnmnex04_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
