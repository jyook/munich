#ifndef ppemxnmnex04_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppemxnmnex04_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppemxnmnex04_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppemxnmnex04_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppemxnmnex04_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
