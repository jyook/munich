#ifndef ppemexmx04_EVENT_SET_HPP
#define ppemexmx04_EVENT_SET_HPP

using namespace std;

class ppemexmx04_event_set : public event_set {
private:

public:
  ppemexmx04_event_set(){}
  ~ppemexmx04_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
