#ifndef ppemexmx04_CONTRIBUTION_SET_HPP
#define ppemexmx04_CONTRIBUTION_SET_HPP

using namespace std;

class ppemexmx04_contribution_set : public contribution_set {
private:

public:
  ppemexmx04_contribution_set(){}
  ~ppemexmx04_contribution_set();

  void determination_subprocess_born(int i_a);
  void combination_subprocess_born(int i_a);
  void determination_subprocess_real(int i_a);
  void combination_subprocess_real(int i_a);
  void determination_subprocess_doublereal(int i_a);
  void combination_subprocess_doublereal(int i_a);

};
#endif
