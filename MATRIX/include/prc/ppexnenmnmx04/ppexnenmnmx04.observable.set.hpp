#ifndef ppexnenmnmx04_OBSERVABLE_SET_HPP
#define ppexnenmnmx04_OBSERVABLE_SET_HPP

using namespace std;

class ppexnenmnmx04_observable_set : public observable_set {
private:

public:
  ppexnenmnmx04_observable_set(){}
  ~ppexnenmnmx04_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
