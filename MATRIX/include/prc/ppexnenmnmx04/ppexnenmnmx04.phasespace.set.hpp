#ifndef ppexnenmnmx04_PHASESPACE_SET_HPP
#define ppexnenmnmx04_PHASESPACE_SET_HPP

using namespace std;

class ppexnenmnmx04_phasespace_set : public phasespace_set {
private:

public:
  ppexnenmnmx04_phasespace_set(){}
  ~ppexnenmnmx04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_udx_epvevmvmx(int x_a);
  void ac_psp_040_udx_epvevmvmx(int x_a, int channel);
  void ag_psp_040_udx_epvevmvmx(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gu_epvevmvmxd(int x_a);
  void ac_psp_140_gu_epvevmvmxd(int x_a, int channel);
  void ag_psp_140_gu_epvevmvmxd(int x_a, int zero);
  void ax_psp_140_gdx_epvevmvmxux(int x_a);
  void ac_psp_140_gdx_epvevmvmxux(int x_a, int channel);
  void ag_psp_140_gdx_epvevmvmxux(int x_a, int zero);
  void ax_psp_140_udx_epvevmvmxg(int x_a);
  void ac_psp_140_udx_epvevmvmxg(int x_a, int channel);
  void ag_psp_140_udx_epvevmvmxg(int x_a, int zero);
  void ax_psp_050_ua_epvevmvmxd(int x_a);
  void ac_psp_050_ua_epvevmvmxd(int x_a, int channel);
  void ag_psp_050_ua_epvevmvmxd(int x_a, int zero);
  void ax_psp_050_dxa_epvevmvmxux(int x_a);
  void ac_psp_050_dxa_epvevmvmxux(int x_a, int channel);
  void ag_psp_050_dxa_epvevmvmxux(int x_a, int zero);
  void ax_psp_050_udx_epvevmvmxa(int x_a);
  void ac_psp_050_udx_epvevmvmxa(int x_a, int channel);
  void ag_psp_050_udx_epvevmvmxa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_epvevmvmxdux(int x_a);
  void ac_psp_240_gg_epvevmvmxdux(int x_a, int channel);
  void ag_psp_240_gg_epvevmvmxdux(int x_a, int zero);
  void ax_psp_240_gu_epvevmvmxgd(int x_a);
  void ac_psp_240_gu_epvevmvmxgd(int x_a, int channel);
  void ag_psp_240_gu_epvevmvmxgd(int x_a, int zero);
  void ax_psp_240_gdx_epvevmvmxgux(int x_a);
  void ac_psp_240_gdx_epvevmvmxgux(int x_a, int channel);
  void ag_psp_240_gdx_epvevmvmxgux(int x_a, int zero);
  void ax_psp_240_du_epvevmvmxdd(int x_a);
  void ac_psp_240_du_epvevmvmxdd(int x_a, int channel);
  void ag_psp_240_du_epvevmvmxdd(int x_a, int zero);
  void ax_psp_240_dc_epvevmvmxds(int x_a);
  void ac_psp_240_dc_epvevmvmxds(int x_a, int channel);
  void ag_psp_240_dc_epvevmvmxds(int x_a, int zero);
  void ax_psp_240_ddx_epvevmvmxdux(int x_a);
  void ac_psp_240_ddx_epvevmvmxdux(int x_a, int channel);
  void ag_psp_240_ddx_epvevmvmxdux(int x_a, int zero);
  void ax_psp_240_ddx_epvevmvmxscx(int x_a);
  void ac_psp_240_ddx_epvevmvmxscx(int x_a, int channel);
  void ag_psp_240_ddx_epvevmvmxscx(int x_a, int zero);
  void ax_psp_240_dsx_epvevmvmxdcx(int x_a);
  void ac_psp_240_dsx_epvevmvmxdcx(int x_a, int channel);
  void ag_psp_240_dsx_epvevmvmxdcx(int x_a, int zero);
  void ax_psp_240_uu_epvevmvmxdu(int x_a);
  void ac_psp_240_uu_epvevmvmxdu(int x_a, int channel);
  void ag_psp_240_uu_epvevmvmxdu(int x_a, int zero);
  void ax_psp_240_uc_epvevmvmxdc(int x_a);
  void ac_psp_240_uc_epvevmvmxdc(int x_a, int channel);
  void ag_psp_240_uc_epvevmvmxdc(int x_a, int zero);
  void ax_psp_240_udx_epvevmvmxgg(int x_a);
  void ac_psp_240_udx_epvevmvmxgg(int x_a, int channel);
  void ag_psp_240_udx_epvevmvmxgg(int x_a, int zero);
  void ax_psp_240_udx_epvevmvmxddx(int x_a);
  void ac_psp_240_udx_epvevmvmxddx(int x_a, int channel);
  void ag_psp_240_udx_epvevmvmxddx(int x_a, int zero);
  void ax_psp_240_udx_epvevmvmxuux(int x_a);
  void ac_psp_240_udx_epvevmvmxuux(int x_a, int channel);
  void ag_psp_240_udx_epvevmvmxuux(int x_a, int zero);
  void ax_psp_240_udx_epvevmvmxssx(int x_a);
  void ac_psp_240_udx_epvevmvmxssx(int x_a, int channel);
  void ag_psp_240_udx_epvevmvmxssx(int x_a, int zero);
  void ax_psp_240_udx_epvevmvmxccx(int x_a);
  void ac_psp_240_udx_epvevmvmxccx(int x_a, int channel);
  void ag_psp_240_udx_epvevmvmxccx(int x_a, int zero);
  void ax_psp_240_uux_epvevmvmxdux(int x_a);
  void ac_psp_240_uux_epvevmvmxdux(int x_a, int channel);
  void ag_psp_240_uux_epvevmvmxdux(int x_a, int zero);
  void ax_psp_240_uux_epvevmvmxscx(int x_a);
  void ac_psp_240_uux_epvevmvmxscx(int x_a, int channel);
  void ag_psp_240_uux_epvevmvmxscx(int x_a, int zero);
  void ax_psp_240_usx_epvevmvmxdsx(int x_a);
  void ac_psp_240_usx_epvevmvmxdsx(int x_a, int channel);
  void ag_psp_240_usx_epvevmvmxdsx(int x_a, int zero);
  void ax_psp_240_usx_epvevmvmxucx(int x_a);
  void ac_psp_240_usx_epvevmvmxucx(int x_a, int channel);
  void ag_psp_240_usx_epvevmvmxucx(int x_a, int zero);
  void ax_psp_240_ucx_epvevmvmxdcx(int x_a);
  void ac_psp_240_ucx_epvevmvmxdcx(int x_a, int channel);
  void ag_psp_240_ucx_epvevmvmxdcx(int x_a, int zero);
  void ax_psp_240_dxdx_epvevmvmxdxux(int x_a);
  void ac_psp_240_dxdx_epvevmvmxdxux(int x_a, int channel);
  void ag_psp_240_dxdx_epvevmvmxdxux(int x_a, int zero);
  void ax_psp_240_dxux_epvevmvmxuxux(int x_a);
  void ac_psp_240_dxux_epvevmvmxuxux(int x_a, int channel);
  void ag_psp_240_dxux_epvevmvmxuxux(int x_a, int zero);
  void ax_psp_240_dxsx_epvevmvmxdxcx(int x_a);
  void ac_psp_240_dxsx_epvevmvmxdxcx(int x_a, int channel);
  void ag_psp_240_dxsx_epvevmvmxdxcx(int x_a, int zero);
  void ax_psp_240_dxcx_epvevmvmxuxcx(int x_a);
  void ac_psp_240_dxcx_epvevmvmxuxcx(int x_a, int channel);
  void ag_psp_240_dxcx_epvevmvmxuxcx(int x_a, int zero);

};
#endif
