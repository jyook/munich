#ifndef ppexneh03nockm_PHASESPACE_SET_HPP
#define ppexneh03nockm_PHASESPACE_SET_HPP

using namespace std;

class ppexneh03nockm_phasespace_set : public phasespace_set {
private:

public:
  ppexneh03nockm_phasespace_set(){}
  ~ppexneh03nockm_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_030_udx_epveh(int x_a);
  void ac_psp_030_udx_epveh(int x_a, int channel);
  void ag_psp_030_udx_epveh(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_130_gu_epvehd(int x_a);
  void ac_psp_130_gu_epvehd(int x_a, int channel);
  void ag_psp_130_gu_epvehd(int x_a, int zero);
  void ax_psp_130_gdx_epvehux(int x_a);
  void ac_psp_130_gdx_epvehux(int x_a, int channel);
  void ag_psp_130_gdx_epvehux(int x_a, int zero);
  void ax_psp_130_udx_epvehg(int x_a);
  void ac_psp_130_udx_epvehg(int x_a, int channel);
  void ag_psp_130_udx_epvehg(int x_a, int zero);
  void ax_psp_040_ua_epvehd(int x_a);
  void ac_psp_040_ua_epvehd(int x_a, int channel);
  void ag_psp_040_ua_epvehd(int x_a, int zero);
  void ax_psp_040_dxa_epvehux(int x_a);
  void ac_psp_040_dxa_epvehux(int x_a, int channel);
  void ag_psp_040_dxa_epvehux(int x_a, int zero);
  void ax_psp_040_udx_epveha(int x_a);
  void ac_psp_040_udx_epveha(int x_a, int channel);
  void ag_psp_040_udx_epveha(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_230_gg_epvehdux(int x_a);
  void ac_psp_230_gg_epvehdux(int x_a, int channel);
  void ag_psp_230_gg_epvehdux(int x_a, int zero);
  void ax_psp_230_gu_epvehgd(int x_a);
  void ac_psp_230_gu_epvehgd(int x_a, int channel);
  void ag_psp_230_gu_epvehgd(int x_a, int zero);
  void ax_psp_230_gdx_epvehgux(int x_a);
  void ac_psp_230_gdx_epvehgux(int x_a, int channel);
  void ag_psp_230_gdx_epvehgux(int x_a, int zero);
  void ax_psp_230_du_epvehdd(int x_a);
  void ac_psp_230_du_epvehdd(int x_a, int channel);
  void ag_psp_230_du_epvehdd(int x_a, int zero);
  void ax_psp_230_dc_epvehds(int x_a);
  void ac_psp_230_dc_epvehds(int x_a, int channel);
  void ag_psp_230_dc_epvehds(int x_a, int zero);
  void ax_psp_230_ddx_epvehdux(int x_a);
  void ac_psp_230_ddx_epvehdux(int x_a, int channel);
  void ag_psp_230_ddx_epvehdux(int x_a, int zero);
  void ax_psp_230_ddx_epvehscx(int x_a);
  void ac_psp_230_ddx_epvehscx(int x_a, int channel);
  void ag_psp_230_ddx_epvehscx(int x_a, int zero);
  void ax_psp_230_dsx_epvehdcx(int x_a);
  void ac_psp_230_dsx_epvehdcx(int x_a, int channel);
  void ag_psp_230_dsx_epvehdcx(int x_a, int zero);
  void ax_psp_230_uu_epvehdu(int x_a);
  void ac_psp_230_uu_epvehdu(int x_a, int channel);
  void ag_psp_230_uu_epvehdu(int x_a, int zero);
  void ax_psp_230_uc_epvehdc(int x_a);
  void ac_psp_230_uc_epvehdc(int x_a, int channel);
  void ag_psp_230_uc_epvehdc(int x_a, int zero);
  void ax_psp_230_udx_epvehgg(int x_a);
  void ac_psp_230_udx_epvehgg(int x_a, int channel);
  void ag_psp_230_udx_epvehgg(int x_a, int zero);
  void ax_psp_230_udx_epvehddx(int x_a);
  void ac_psp_230_udx_epvehddx(int x_a, int channel);
  void ag_psp_230_udx_epvehddx(int x_a, int zero);
  void ax_psp_230_udx_epvehuux(int x_a);
  void ac_psp_230_udx_epvehuux(int x_a, int channel);
  void ag_psp_230_udx_epvehuux(int x_a, int zero);
  void ax_psp_230_udx_epvehssx(int x_a);
  void ac_psp_230_udx_epvehssx(int x_a, int channel);
  void ag_psp_230_udx_epvehssx(int x_a, int zero);
  void ax_psp_230_udx_epvehccx(int x_a);
  void ac_psp_230_udx_epvehccx(int x_a, int channel);
  void ag_psp_230_udx_epvehccx(int x_a, int zero);
  void ax_psp_230_uux_epvehdux(int x_a);
  void ac_psp_230_uux_epvehdux(int x_a, int channel);
  void ag_psp_230_uux_epvehdux(int x_a, int zero);
  void ax_psp_230_uux_epvehscx(int x_a);
  void ac_psp_230_uux_epvehscx(int x_a, int channel);
  void ag_psp_230_uux_epvehscx(int x_a, int zero);
  void ax_psp_230_usx_epvehdsx(int x_a);
  void ac_psp_230_usx_epvehdsx(int x_a, int channel);
  void ag_psp_230_usx_epvehdsx(int x_a, int zero);
  void ax_psp_230_usx_epvehucx(int x_a);
  void ac_psp_230_usx_epvehucx(int x_a, int channel);
  void ag_psp_230_usx_epvehucx(int x_a, int zero);
  void ax_psp_230_ucx_epvehdcx(int x_a);
  void ac_psp_230_ucx_epvehdcx(int x_a, int channel);
  void ag_psp_230_ucx_epvehdcx(int x_a, int zero);
  void ax_psp_230_dxdx_epvehdxux(int x_a);
  void ac_psp_230_dxdx_epvehdxux(int x_a, int channel);
  void ag_psp_230_dxdx_epvehdxux(int x_a, int zero);
  void ax_psp_230_dxux_epvehuxux(int x_a);
  void ac_psp_230_dxux_epvehuxux(int x_a, int channel);
  void ag_psp_230_dxux_epvehuxux(int x_a, int zero);
  void ax_psp_230_dxsx_epvehdxcx(int x_a);
  void ac_psp_230_dxsx_epvehdxcx(int x_a, int channel);
  void ag_psp_230_dxsx_epvehdxcx(int x_a, int zero);
  void ax_psp_230_dxcx_epvehuxcx(int x_a);
  void ac_psp_230_dxcx_epvehuxcx(int x_a, int channel);
  void ag_psp_230_dxcx_epvehuxcx(int x_a, int zero);

};
#endif
