#ifndef ppexneh03nockm_OBSERVABLE_SET_HPP
#define ppexneh03nockm_OBSERVABLE_SET_HPP

using namespace std;

class ppexneh03nockm_observable_set : public observable_set {
private:

public:
  ppexneh03nockm_observable_set(){}
  ~ppexneh03nockm_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
