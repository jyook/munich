#ifndef pph21loop_OBSERVABLE_SET_HPP
#define pph21loop_OBSERVABLE_SET_HPP

using namespace std;

class pph21loop_observable_set : public observable_set {
private:

public:
  pph21loop_observable_set(){}
  ~pph21loop_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
