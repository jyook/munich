#ifndef pph21loop_EVENT_SET_HPP
#define pph21loop_EVENT_SET_HPP

using namespace std;

class pph21loop_event_set : public event_set {
private:

public:
  pph21loop_event_set(){}
  ~pph21loop_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
