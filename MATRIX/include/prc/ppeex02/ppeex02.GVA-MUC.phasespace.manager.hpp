#ifndef ppeex02_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppeex02_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppeex02_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppeex02_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppeex02_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
