#ifndef ppeex02_EVENT_SET_HPP
#define ppeex02_EVENT_SET_HPP

using namespace std;

class ppeex02_event_set : public event_set {
private:

public:
  ppeex02_event_set(){}
  ~ppeex02_event_set();

  void particles(int i_a);
  void cuts(int i_a);
  void phasespacepoint_born();
  void phasespacepoint_collinear();
  void phasespacepoint_real();
  void phasespacepoint_realcollinear();
  void phasespacepoint_doublereal();

};
#endif
