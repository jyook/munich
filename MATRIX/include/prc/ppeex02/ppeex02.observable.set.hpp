#ifndef ppeex02_OBSERVABLE_SET_HPP
#define ppeex02_OBSERVABLE_SET_HPP

using namespace std;

class ppeex02_observable_set : public observable_set {
private:

public:
  ppeex02_observable_set(){}
  ~ppeex02_observable_set();

  void moments();
  void calculate_dynamic_scale(int i_a);
  void calculate_dynamic_scale_RA(int i_a);
  void calculate_dynamic_scale_TSV(int i_a);

};
#endif
