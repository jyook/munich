#ifndef ppnenexa03_GVA_MUC_PHASESPACE_MANAGER_HPP
#define ppnenexa03_GVA_MUC_PHASESPACE_MANAGER_HPP

using namespace std;

class ppnenexa03_GVA_MUC_phasespace_manager : public GVA_MUC_phasespace_manager {
private:

public:
  ppnenexa03_GVA_MUC_phasespace_manager(string _processname, bool _switch_combination = false);
  ~ppnenexa03_GVA_MUC_phasespace_manager();

  void geneva_initialization_order(size_t x_o);
  void geneva_initialization_channel();

};
#endif
