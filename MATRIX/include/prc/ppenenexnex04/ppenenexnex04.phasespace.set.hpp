#ifndef ppenenexnex04_PHASESPACE_SET_HPP
#define ppenenexnex04_PHASESPACE_SET_HPP

using namespace std;

class ppenenexnex04_phasespace_set : public phasespace_set {
private:

public:
  ppenenexnex04_phasespace_set(){}
  ~ppenenexnex04_phasespace_set();

  void optimize_minv_born();
  int determination_MCchannels_born(int x_a);
  void ac_tau_psp_born(int x_a, vector<int> & tau_MC_map);
  void ax_psp_born(int x_a);
  void ac_psp_born(int x_a, int channel);
  void ag_psp_born(int x_a, int zero);
  void ax_psp_040_dux_emvevexvex(int x_a);
  void ac_psp_040_dux_emvevexvex(int x_a, int channel);
  void ag_psp_040_dux_emvevexvex(int x_a, int zero);

  void optimize_minv_real();
  int determination_MCchannels_real(int x_a);
  void ac_tau_psp_real(int x_a, vector<int> & tau_MC_map);
  void ax_psp_real(int x_a);
  void ac_psp_real(int x_a, int channel);
  void ag_psp_real(int x_a, int zero);
  void ax_psp_140_gd_emvevexvexu(int x_a);
  void ac_psp_140_gd_emvevexvexu(int x_a, int channel);
  void ag_psp_140_gd_emvevexvexu(int x_a, int zero);
  void ax_psp_140_gux_emvevexvexdx(int x_a);
  void ac_psp_140_gux_emvevexvexdx(int x_a, int channel);
  void ag_psp_140_gux_emvevexvexdx(int x_a, int zero);
  void ax_psp_140_dux_emvevexvexg(int x_a);
  void ac_psp_140_dux_emvevexvexg(int x_a, int channel);
  void ag_psp_140_dux_emvevexvexg(int x_a, int zero);
  void ax_psp_050_da_emvevexvexu(int x_a);
  void ac_psp_050_da_emvevexvexu(int x_a, int channel);
  void ag_psp_050_da_emvevexvexu(int x_a, int zero);
  void ax_psp_050_uxa_emvevexvexdx(int x_a);
  void ac_psp_050_uxa_emvevexvexdx(int x_a, int channel);
  void ag_psp_050_uxa_emvevexvexdx(int x_a, int zero);
  void ax_psp_050_dux_emvevexvexa(int x_a);
  void ac_psp_050_dux_emvevexvexa(int x_a, int channel);
  void ag_psp_050_dux_emvevexvexa(int x_a, int zero);

  void optimize_minv_doublereal();
  int determination_MCchannels_doublereal(int x_a);
  void ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map);
  void ax_psp_doublereal(int x_a);
  void ac_psp_doublereal(int x_a, int channel);
  void ag_psp_doublereal(int x_a, int zero);
  void ax_psp_240_gg_emvevexvexudx(int x_a);
  void ac_psp_240_gg_emvevexvexudx(int x_a, int channel);
  void ag_psp_240_gg_emvevexvexudx(int x_a, int zero);
  void ax_psp_240_gd_emvevexvexgu(int x_a);
  void ac_psp_240_gd_emvevexvexgu(int x_a, int channel);
  void ag_psp_240_gd_emvevexvexgu(int x_a, int zero);
  void ax_psp_240_gux_emvevexvexgdx(int x_a);
  void ac_psp_240_gux_emvevexvexgdx(int x_a, int channel);
  void ag_psp_240_gux_emvevexvexgdx(int x_a, int zero);
  void ax_psp_240_dd_emvevexvexdu(int x_a);
  void ac_psp_240_dd_emvevexvexdu(int x_a, int channel);
  void ag_psp_240_dd_emvevexvexdu(int x_a, int zero);
  void ax_psp_240_du_emvevexvexuu(int x_a);
  void ac_psp_240_du_emvevexvexuu(int x_a, int channel);
  void ag_psp_240_du_emvevexvexuu(int x_a, int zero);
  void ax_psp_240_ds_emvevexvexdc(int x_a);
  void ac_psp_240_ds_emvevexvexdc(int x_a, int channel);
  void ag_psp_240_ds_emvevexvexdc(int x_a, int zero);
  void ax_psp_240_dc_emvevexvexuc(int x_a);
  void ac_psp_240_dc_emvevexvexuc(int x_a, int channel);
  void ag_psp_240_dc_emvevexvexuc(int x_a, int zero);
  void ax_psp_240_ddx_emvevexvexudx(int x_a);
  void ac_psp_240_ddx_emvevexvexudx(int x_a, int channel);
  void ag_psp_240_ddx_emvevexvexudx(int x_a, int zero);
  void ax_psp_240_ddx_emvevexvexcsx(int x_a);
  void ac_psp_240_ddx_emvevexvexcsx(int x_a, int channel);
  void ag_psp_240_ddx_emvevexvexcsx(int x_a, int zero);
  void ax_psp_240_dux_emvevexvexgg(int x_a);
  void ac_psp_240_dux_emvevexvexgg(int x_a, int channel);
  void ag_psp_240_dux_emvevexvexgg(int x_a, int zero);
  void ax_psp_240_dux_emvevexvexddx(int x_a);
  void ac_psp_240_dux_emvevexvexddx(int x_a, int channel);
  void ag_psp_240_dux_emvevexvexddx(int x_a, int zero);
  void ax_psp_240_dux_emvevexvexuux(int x_a);
  void ac_psp_240_dux_emvevexvexuux(int x_a, int channel);
  void ag_psp_240_dux_emvevexvexuux(int x_a, int zero);
  void ax_psp_240_dux_emvevexvexssx(int x_a);
  void ac_psp_240_dux_emvevexvexssx(int x_a, int channel);
  void ag_psp_240_dux_emvevexvexssx(int x_a, int zero);
  void ax_psp_240_dux_emvevexvexccx(int x_a);
  void ac_psp_240_dux_emvevexvexccx(int x_a, int channel);
  void ag_psp_240_dux_emvevexvexccx(int x_a, int zero);
  void ax_psp_240_dsx_emvevexvexusx(int x_a);
  void ac_psp_240_dsx_emvevexvexusx(int x_a, int channel);
  void ag_psp_240_dsx_emvevexvexusx(int x_a, int zero);
  void ax_psp_240_dcx_emvevexvexdsx(int x_a);
  void ac_psp_240_dcx_emvevexvexdsx(int x_a, int channel);
  void ag_psp_240_dcx_emvevexvexdsx(int x_a, int zero);
  void ax_psp_240_dcx_emvevexvexucx(int x_a);
  void ac_psp_240_dcx_emvevexvexucx(int x_a, int channel);
  void ag_psp_240_dcx_emvevexvexucx(int x_a, int zero);
  void ax_psp_240_uux_emvevexvexudx(int x_a);
  void ac_psp_240_uux_emvevexvexudx(int x_a, int channel);
  void ag_psp_240_uux_emvevexvexudx(int x_a, int zero);
  void ax_psp_240_uux_emvevexvexcsx(int x_a);
  void ac_psp_240_uux_emvevexvexcsx(int x_a, int channel);
  void ag_psp_240_uux_emvevexvexcsx(int x_a, int zero);
  void ax_psp_240_ucx_emvevexvexusx(int x_a);
  void ac_psp_240_ucx_emvevexvexusx(int x_a, int channel);
  void ag_psp_240_ucx_emvevexvexusx(int x_a, int zero);
  void ax_psp_240_dxux_emvevexvexdxdx(int x_a);
  void ac_psp_240_dxux_emvevexvexdxdx(int x_a, int channel);
  void ag_psp_240_dxux_emvevexvexdxdx(int x_a, int zero);
  void ax_psp_240_dxcx_emvevexvexdxsx(int x_a);
  void ac_psp_240_dxcx_emvevexvexdxsx(int x_a, int channel);
  void ag_psp_240_dxcx_emvevexvexdxsx(int x_a, int zero);
  void ax_psp_240_uxux_emvevexvexdxux(int x_a);
  void ac_psp_240_uxux_emvevexvexdxux(int x_a, int channel);
  void ag_psp_240_uxux_emvevexvexdxux(int x_a, int zero);
  void ax_psp_240_uxcx_emvevexvexdxcx(int x_a);
  void ac_psp_240_uxcx_emvevexvexdxcx(int x_a, int channel);
  void ag_psp_240_uxcx_emvevexvexdxcx(int x_a, int zero);

};
#endif
