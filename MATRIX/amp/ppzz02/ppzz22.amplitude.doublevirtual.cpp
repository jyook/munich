#include "header.hpp"

#include "ppzz22.amplitude.doublevirtual.hpp"
//#include <ME2_ZZ.h>

void ppzz22_calculate_H2(observable_set * osi){
  static Logger logger("ppzz22_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppzz22_calculate_amplitude_doublevirtual(osi);

  if (osi->QT_A0 == 0) {
    return;
  }

  osi->QT_H1_delta = osi->QT_A1;
  osi->QT_H2_delta = osi->QT_A2;

  osi->QT_H1_delta /= 2;
  osi->QT_H2_delta /= 4;

  osi->QT_H1_delta /= (osi->alpha_S * inv2pi * osi->QT_A0);
  osi->QT_H2_delta /= (pow(osi->alpha_S * inv2pi, 2) * osi->QT_A0);   

  logger << LOG_DEBUG << "osi->QT_H1_delta = " << osi->QT_H1_delta << ", osi->QT_H2_delta = " << osi->QT_H2_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz22_calculate_amplitude_doublevirtual(observable_set * osi) {
  static Logger logger("ppzz22_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // [5] and [6] should not exist !!!
  /*
  fourvector q1 = osi->p_parton[0][3] + osi->p_parton[0][5];
  fourvector q2 = osi->p_parton[0][4] + osi->p_parton[0][6];

  double s = (osi->p_parton[0][1] + osi->p_parton[0][2]).m2();
  double u = (osi->p_parton[0][2] - q1).m2();
  double t = (osi->p_parton[0][2] - q2).m2();
  */
  double s = (osi->esi->p_parton[0][1] + osi->esi->p_parton[0][2]).m2();
  double u = (osi->esi->p_parton[0][2] - osi->esi->p_parton[0][3]).m2();
  double t = (osi->esi->p_parton[0][2] - osi->esi->p_parton[0][4]).m2();
  
  //  double M0M1_f,M1M1_f,A_2,A_1;

  static double M_Z = osi->msi->M_Z;
  
  double x = s/2/M_Z/M_Z-1-s/2/M_Z/M_Z*sqrt(1-4*M_Z*M_Z/s);
  double z = -u/M_Z/M_Z;
  double y = -t/M_Z/M_Z;
  logger << LOG_DEBUG << "s=" << s << ", t=" << t << ", u=" << u << ", sqrt(0.5*(s+t+u))=" << sqrt(0.5*(s+t+u)) << endl;

  logger << LOG_DEBUG << setprecision(16) << "x=" << x << ", y=" << y << ", z=" << z << ", x*z=" << x*z << ", check: " << 1+2*x+x*x-x*s/M_Z/M_Z << endl;

  if (x*z>=1.01 || 1-x*z<1e-5) {// || x<1e-3 || z/x-1<1e-2) {
    logger << LOG_DEBUG << "point killed!" << endl;
    osi->QT_A0=0;
    osi->QT_A1=0;
    osi->QT_A2=0;
    osi->QT_H1_delta=0;
    osi->QT_H2_delta=0;
    return;
  }

  assert(0<x);
  assert(x<1);
  assert(x<z);
  assert(z<1.0/x);
 
  double e_q,I_3;
  //  if (osi->name_process == "uu~_zz") {
  //  else if (osi->name_process == "dd~_zz") {
  if (osi->csi->type_parton[0][1] == 1 || osi->csi->type_parton[0][1] == 3 || osi->csi->type_parton[0][1] == 5) { // d, s, b
    e_q = -1.0/3;
    I_3 = -0.5;
  } 
  else if (osi->csi->type_parton[0][1] == 2 || osi->csi->type_parton[0][1] == 4) { // u, c
    e_q = 2.0/3;
    I_3 = 0.5;
  }
  else {
    logger << LOG_ERROR << "calc_amplitudes: unknown process: " << osi->csi->subprocess << endl; exit(1);
  }
    

  static double_complex cos_W = osi->msi->ccos_w;
  static double_complex sin_W = osi->msi->csin_w;
  double_complex g_V = 0.5*(I_3-2.0*sin_W*sin_W*e_q)/(sin_W*cos_W);
  double_complex g_A = 0.5*0.5/(sin_W*cos_W);
  double_complex prefactor = pow(g_V,4)+pow(g_A,4)+6.0*g_A*g_A*g_V*g_V;

  double_complex g_V_up = 0.5*(0.5-2.0*sin_W*sin_W*2.0/3.0)/(sin_W*cos_W);
  double_complex g_V_down = 0.5*(-0.5-2.0*sin_W*sin_W*(-1.0/3.0))/(sin_W*cos_W);
  double N_FZ = abs((g_V*g_V+g_A*g_A)*(3.0*g_V_down*g_V_down+2.0*g_V_up*g_V_up+5.0*g_A*g_A))/abs(prefactor);

  //logger << LOG_DEBUG << N_FZ << ", " << abs((g_V*g_V+g_A*g_A)*(2.0*g_V_up*g_V_up+2.0*g_A*g_A))/abs(prefactor) << ", " << abs((g_V*g_V+g_A*g_A)*(3.0*g_V_down*g_V_down+3.0*g_A*g_A))/abs(prefactor) << endl;

  int order=2;
  int debug=1;

  double sn=s/M_Z/M_Z;
  double un=u/M_Z/M_Z;

  double M0M0,M0M1,M1M1,M0M2a,M0M2b,M0M2c,M0M2d;

  logger << LOG_DEBUG << "sn=" << sn << ", un=" << un << endl;

  ZZ_computeAmplitudes(sn,un,order,debug,M0M0,M0M1,M1M1,M0M2a,M0M2b,M0M2c,M0M2d);
  // add color structures
  double Nc=3;
  double CF=4.0/3;
  double Nf=5;
  osi->QT_A0 = Nc*M0M0;
  osi->QT_A1 = 2*CF*Nc*M0M1;

  osi->QT_A2 = CF*CF*Nc*M1M1;
  // leading color, replace by
  // 2*(Nc/2*Nc/2*Nc*M0M2c+CF*Nf*Nc*M0M2a)
  // for old lc result
  osi->QT_A2 += 2*(CF*CF*Nc*M0M2c+CF*Nf*Nc*M0M2a);
  // subleading color
  osi->QT_A2 += 2*(CF*Nc*N_FZ*M0M2b+CF*M0M2d);

//  A2 = 2*CF*CF*Nc*M0M2c;
//  A2 = 2*CF*Nf*Nc*M0M2a;
//  A2 = 2*CF*Nc*N_FZ*M0M2b;
//  A2 = 2*CF*M0M2d;
//  A2=CF*CF*Nc*M1M1;

  logger << LOG_DEBUG << "M0M0: " << 3*M0M0 << endl;
  logger << LOG_DEBUG << "M0M1: " << 3*CF*M0M1 << endl;
  logger << LOG_DEBUG << "M1M1: " << Nc*CF*CF*M1M1 << endl;
  logger << LOG_DEBUG << "M0M2: " << 2*(CF*CF*Nc*M0M2c+CF*Nf*Nc*M0M2a) + 2*(CF*Nc*N_FZ*M0M2b+CF*M0M2d) << endl;
  //beta0=(11*CA*3.0/2*3.0/2/CF-4*0.5*Nf*CF)/6/CF;
  //logger << LOG_DEBUG << "M0M2: " << 3*3*3*M0M2a+CF*Nf*3*M0M2b-beta0*log(s/M_Z/M_Z)*3*M0M1 << endl;
//  logger << LOG_DEBUG << "M0M2: " << 3*3*3.0/4*M0M2c+CF*Nf*3*M0M2a-beta0*log(s/M_Z/M_Z)*3*CF*M0M1 << endl;

  static double alpha_e = osi->msi->alpha_e;
  double Born_factor = abs(prefactor);
  Born_factor *= pow(4*M_PI*alpha_e,2);
  Born_factor /= (4*9); // averaging factor
  // Z branching fraction
//  Born_factor *= (BR_Zll*BR_Zll);
  // phase space volume and remaining propagator contributions
//  Born_factor *= pow(16.0*M_PI/M_Z/Gamma_Z,2);
  // final state degeneracy
//  Born_factor *= 2;



  osi->QT_A0 *= Born_factor;
  osi->QT_A1 *= Born_factor;
  osi->QT_A2 *= Born_factor;


  osi->QT_A1 *= pow(osi->alpha_S/2/M_PI,1);
  osi->QT_A2 *= pow(osi->alpha_S/2/M_PI,2);

  logger << LOG_DEBUG << "amplitudes: " << osi->QT_A0 << ", " << osi->QT_A1 << ", " << osi->QT_A2 << ", " << M1M1 << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
