#include "header.hpp"

#include "ggllh33.amplitude.doublevirtual.hpp"

void ggllh33_calculate_H1(observable_set * osi){
  static Logger logger("ggllh33_calculate_H1");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ggllh33_calculate_amplitude_doublevirtual(osi);

  osi->QT_H1_delta = osi->QT_A1;

  osi->QT_H1_delta /= 2;

  osi->QT_H1_delta /= (osi->alpha_S * inv2pi * osi->QT_A0);
   
  logger << LOG_DEBUG << "osi->QT_H1_delta = " << osi->QT_H1_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void ggllh33_calculate_amplitude_doublevirtual(observable_set * osi) {
  static Logger logger("ggllh33_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

 
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
