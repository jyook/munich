#include "header.hpp"
#include "ggaa32.amplitude.doublevirtual.hpp"

void ggaa32_calculate_H1(observable_set * osi){
  static Logger logger("ggaa32_calculate_H1");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ggaa32_calculate_amplitude_doublevirtual(osi);

  osi->QT_H1_delta = osi->QT_A1;

  osi->QT_H1_delta /= 2;

  osi->QT_H1_delta /= (osi->alpha_S * inv2pi * osi->QT_A0);
   
  logger << LOG_DEBUG << "osi->QT_H1_delta = " << osi->QT_H1_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void ggaa32_calculate_amplitude_doublevirtual(observable_set * osi){
  static Logger logger("ggaa32_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

 
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
