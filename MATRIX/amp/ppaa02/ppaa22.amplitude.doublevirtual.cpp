#include "header.hpp"

#include "ppaa22.amplitude.doublevirtual.hpp"

void ppaa22_calculate_H2(observable_set * osi){
  static Logger logger("ppaa22_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  double s = (osi->esi->p_parton[0][1]+osi->esi->p_parton[0][2]).m2();
  double t = (osi->esi->p_parton[0][1]-osi->esi->p_parton[0][4]).m2();
  double u = (osi->esi->p_parton[0][1]-osi->esi->p_parton[0][3]).m2();
  
//   cout << "s=" << s << ", t=" << t << ", u=" << u << endl;

  double Q_factor;
  double Qq;
  if (osi->csi->subprocess == "uu~_aa") {
    Q_factor=pow(2.0/3,4);
    Qq=2.0/3;
  }
  else if (osi->csi->subprocess == "dd~_aa" || osi->csi->subprocess == "bb~_aa") {
    Q_factor=pow(1.0/3,4);
    Qq=-1.0/3;
  }
  else {
    assert(false && "ERROR: sub-process not known!");
  }
  

  osi->QT_A0 = Q_factor*ppaa22_A0(s,t,u)*pow(4*M_PI*osi->msi->alpha_e,2);
  
  osi->QT_A0 /= (4*9); // averaging factor
  osi->QT_A0 *= 0.5; // symmetry factor
  
  // H1
  osi->QT_A1 = 2*(inv2pi * osi->alpha_S)*ppaa22_H1f(s,t,u)*osi->QT_A0;
  osi->QT_H1_delta = osi->QT_A1;
  
  // H2
  osi->QT_A2 = pow(2*inv2pi * osi->alpha_S,2)*ppaa22_H2f(s,t,u,osi->N_f,Qq)*osi->QT_A0;
  osi->QT_H2_delta = osi->QT_A2;
   
  // adapt alpha_S/Pi normalisation
  osi->QT_H1_delta /= 2;
  osi->QT_H2_delta /= 4;
  
  osi->QT_H1_delta /= (inv2pi * osi->alpha_S * osi->QT_A0);
  osi->QT_H2_delta /= (pow(inv2pi * osi->alpha_S, 2) * osi->QT_A0);
  
//   cout << "H1=" << osi->QT_H1_delta << endl;
//   cout << "H2=" << osi->QT_H2_delta << endl;
       
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

