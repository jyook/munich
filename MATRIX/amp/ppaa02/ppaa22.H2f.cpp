//      H2.cpp
//      
//      Copyright 2010 Leandro <leandro@ubuntu>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <assert.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

//#include <classes.cxx>
#include "header.hpp"


//#define Pi4 97.40909104
//#define Pi2 9.869604401
// maybe even more digits !!!
#define Pi4 97.4090910340024
#define Pi2 9.86960440108936

#define __DEB_PI    3.14159265358979323846
#define __DEB_2PI   6.28318530717958647692
#define __DEB_4PI2  157.91367041742973790108
#define __DEB_1_8PI 0.03978873577297383395


#define Z2 1.6449340668482262
#define Z3 1.2020569031595942
#define Z4 1.082323233711138

// CARGAS DE LOS Q'S
#define Qu  0.66666666666666666666 
#define Qd -0.33333333333333333333
#define Qu4 0.19753086419753086419
#define Qd4 0.01234567901234567901
#define Tr 0.5
#define sumQq2 1.2222222222222222222

//N[PolyLog[2, -25]]
#define PolyLog2m25  -6.785907899712118
// N[PolyLog[3, -25]]
#define PolyLog3m25  -10.893189584831829
//N[PolyLog[4, -25]]
#define PolyLog4m25  -14.848948252983412
// (*N[PolyLog[2,-10]]*)
#define PolyLog2m10  4.198277886858104
// (*N[PolyLog[3,-10]]*)
#define PolyLog3m10  -5.921064803756975
// (*N[PolyLog[3,-5]]*)
#define PolyLog3m5  -3.537511437618607
// (*N[PolyLog[2,-5]]*)
#define PolyLog2m5  -2.749279126060808
// (*N[PolyLog[4,-5]]*)
#define PolyLog4m5  -4.10646797909497
// (*N[PolyLog[4,-10]]*)
#define PolyLog4m10  -7.326570248027082
// (*N[PolyLog[4,-2]]*)
#define PolyLog4m2 -1.813126015328491
// (* N[PolyLog[3,-2]]*)
#define PolyLog3m2 -1.6682833639665713
// (*N[PolyLog[2,-2]]*)
#define PolyLog2m2 -1.4367463668836808


using namespace std;

double Asmitad(double, double, double);
double Bsmitad(double, double, double, double);
double D2smitad(double, double, double, double);
double E3smitad(double, double, double, double);
double G1smitad(double, double, double, double);



double Finite0x2(double, double, double, double, unsigned int, double);
double Finite1x1(double, double, double, double);

double Li2(double);

double Li2(double x){
  return gsl_sf_dilog(x);
} 
	
double Li3(double x){
return myli3(x);
  } 

double Li4(double );

double ppaa22_A0(double s, double t, double u) {
  double v=-u/s;
  
  return 24.0*(1-2*v+2*v*v)/(v*(1-v));
}

double ppaa22_H1f(double s, double t, double u) {
  double Cf=4.0/3;
  
  double v=-u/s;
  
  // Eq. (89) from 1311.1654v2
  double H1gammagamma = (pow(1-v,2)+1)*log(1-v)*log(1-v)+v*(v+2)*log(1-v)+(v*v+1)*log(v)*log(v)+(1-v)*(3-v)*log(v);
  H1gammagamma = Cf/2*(M_PI*M_PI-7+1.0/(pow(1-v,2)+v*v)*H1gammagamma);
  
  return H1gammagamma;
}


double  ppaa22_H2f(double s, double t, double u, unsigned int Nf, double Qq) {
//     double t =S(1,5), u =S(2,5),
//                 s=S(1,2);
//           double s13 = S(1,5), s14 = S(1,4);

  double Cf=4.0/3;
  double Ca=3;
  
  double ALO = ppaa22_A0(s,t,u);
  double muR2=s;
  double H1gammagamma=ppaa22_H1f(s,t,u);
  
  // Eq. (90) from 1311.1654v2
  double H2gammagamma = 1.0/4/ALO*(Finite0x2(t,u,s,muR2,Nf,Qq)+Finite1x1(t,u,s,muR2))+3.0*Z2*Cf*H1gammagamma;
  H2gammagamma += Cf*Cf*(-45.0/4*Z4)+Cf*Ca*(607.0/324+1181.0/144*Z2-187.0/144.0*Z3-105.0/32*Z4);
  H2gammagamma += Cf*Nf*(-41.0/162-97.0/72*Z2+17.0/72*Z3);
  
  return H2gammagamma; 
}

double Finite0x2(double t, double u, double s,double muR2,unsigned int Nf, double Qq){
double Cf=4.0/3;
double Nc=3;


// double X=log(-t/s),Y=log(-u/s),S=log(s/muR2),U=log(-u/muR2);

double finite0x2=2.0*Nc*((sumQq2/(Qq*Qq))*Tr*Cf*(Asmitad(u,t,s)+Asmitad(t,u,s))
+Cf*Cf*(Bsmitad(u,t,s,muR2)+Bsmitad(t,u,s,muR2))
+Cf*Nc*(D2smitad(u,t,s,muR2)+D2smitad(t,u,s,muR2))+Nf*Cf*(E3smitad(u,t,s,muR2)+E3smitad(t,u,s,muR2)));

//Chequeado!!!

return finite0x2;
}


double Finite1x1(double t, double u, double s,double muR2){
double Cf=4.0/3;
double Nc=3;

// double X=log(-t/s),Y=log(-u/s),S=log(s/muR2),U=log(-u/muR2);

double finite1x1=Nc*Cf*Cf*(G1smitad(u,t,s,muR2)+G1smitad(t,u,s,muR2));

//Chequeado!!!!

return finite1x1;
}

double Li4(double x){
	
		
	
	if(x<=-2.0){
	
	
		
return	1.0/360.0*(-60.0/x*(6.0+ 6.0/625.0/(x*x*x*x) + 3.0/128.0/(x*x*x) + 2.0/27.0/(x*x) + 3.0/8.0/x )-7.0*Pi4 - 30.0*Pi2*log(-x)*log(-x) - 15.0*log(-x)*log(-x)*log(-x)*log(-x) );


}
	if(x>-2.0 && x<-1.0){
	
	
	return (pow((2.0 + x),10.0)* (-25437292.0 + 71241525.0* log(3.0) + 62364492.0* PolyLog2m2 - 
    22044960.0 *PolyLog3m2))/225740390400.0 + (pow((2.0 + x),9.0)* (-2404252.0 + 
    7176033.0* log(3.0) + 6657228.0* PolyLog2m2 - 
    2449440.0* PolyLog3m2))/11287019520.0 + (pow((2.0 + x),8.0)* (-82123.0 + 
    265923.0* log(3.0) + 264627.0* PolyLog2m2 - 
    102060.0* PolyLog3m2))/209018880.0 + (pow((2.0 + x),6.0)* (-1438.0 + 
    6075.0* log(3.0) + 7398.0* PolyLog2m2 - 
    3240.0* PolyLog3m2))/1244160.0 + (pow((2.0 + x),5.0)* (-58.0 + 315.0* log(3.0) + 
    450.0* PolyLog2m2 - 216.0* PolyLog3m2))/34560.0 + (pow((2.0 + x),4.0)* (-2.0 + 18.0* log(3) 
    + 33.0* PolyLog2m2 - 
    18.0* PolyLog3m2))/1152.0 + 
 1.0/48.0 *pow((2.0 + x),3.0)* (log(3.0) + 3.0* PolyLog2m2 - 2.0* PolyLog3m2) + 
 1.0/8.0* pow((2.0 + x),2.0)* (PolyLog2m2 - PolyLog3m2) - 
 1.0/2.0* (2.0 + x)* PolyLog3m2 + (pow((2.0 + x),7.0)* (10962.0* log(3.0) + 11907.0* PolyLog2m2 - 
    5.0* (607.0 + 972.0* PolyLog3m2)))/4354560.0 + PolyLog4m2;
}
	
	if(x<=0.7 && x>=-1.0){
return	x+x*x/16+pow(x,3.0)/81+pow(x,4.0)/256+pow(x,5.0)/625+pow(x,6.0)/1296+pow(x,7.0)/2401+pow(x,8.0)/4096
	+pow(x,9.0)/6561+pow(x,10.0)/10000+pow(x,11.0)/14641+pow(x,12.0)/20736+pow(x,13.0)/28561+pow(x,14.0)/38416
	+pow(x,15.0)/50625+pow(x,16.0)/65536+pow(x,17.0)/83521+pow(x,18.0)/104976+pow(x,19.0)/130321
	+pow(x,20.0)/160000+pow(x,21.0)/194481+pow(x,22.0)/234256+pow(x,23.0)/279841+pow(x,24.0)/331776
	+pow(x,25.0)/390625+pow(x,26.0)/456976+pow(x,27.0)/531441+pow(x,28.0)/614656+pow(x,29.0)/707281
	+pow(x,30.0)/810000+pow(x,31.0)/923521+pow(x,32.0)/1048576+pow(x,33.0)/1185921+pow(x,34.0)/1336336
	+pow(x,35.0)/1500625+pow(x,36.)/1679616+pow(x,37.0)/1874161+pow(x,38.0)/2085136+pow(x,39.0)/2313441
	+pow(x,40.0)/2560000+pow(x,41.0)/2825761+pow(x,42.0)/3111696+pow(x,43.0)/3418801+pow(x,44.0)/3748096
	+pow(x,45.0)/4100625+pow(x,46.0)/4477456+pow(x,47.0)/4879681+pow(x,48.0)/5308416+pow(x,49.0)/5764801
	+pow(x,50.0)/6250000;}
			if(x<1.0 && x>0.7){
		
return		1.0/457228800.0*(5080320.0* Pi4-pow((-1.0+x),3.0)* (-1381393255.0+x* (4840853127.0+x* 
(-9435621561.0+x* (11568105449.0+x* (-9128211801.0+x* (4518682089.0+x* 
(-1281356743.0+159233895* x)))))))+1512.0* Pi2* pow((-1.0+x),2.0)* (177133.0+x* 
(-617934.0+x* (1341449.0+x* (-1931968.0+x* (1883165.0+x* (-1233718.0+x* (522099.0+2.0* x* 
(-64642.0+7129.0* x))))))))+2520.0* pow((-1.0+x),3.0)* (-420475.0+x* (1615443.0+x* 
(-3282009.0+x* (4114961.0+x *(-3292089.0+x* (1644801.0+x *(-469507.0+58635.0* x)))))))* 
log(1.0-x)-181440.0* (-1.0+x)* (-7381.0+x* (17819.0+x* (-38881.0+x* (61919.0+x* (-70381.0+x* 
(56627.0+x* (-31573.0+7.0* x* (1661.0+4.0* x* (-91.0+9.0* x)))))))))* Z3);
		
		
		//Ver esta nueva parte como se comporta!!!!!!!!!!!!!
		
		
		}
		
		
		
		
		//Solamente el Li4 recibe valores de z : Li2 y Li3 solo se evaluan en x e y porlo uqe
		// sus argumentos estan acotados entre 0 y 1 y por eso no tiran error
	
		
//		-276278651/91445760+(177133*pow(__DEB_PI,2.0))/302400+pow(__DEB_PI ,4.0)/90+(3081287* x)/156800-
//		(4861* pow(__DEB_PI,2.0)* x)/1512-(3122484523* pow(x,2.0))/50803200+(6121*pow( __DEB_PI,2.0)* x*x)/672
//		+(1162060891* x*x*x)/9525600-(6541* pow(__DEB_PI,2.0)* x*x*x)/378-(610954333* x*x*x*x)/3628800
//		+(6751* pow(__DEB_PI,2.0)* x*x*x*x)/288+(6035179* x*x*x*x*x)/36288-(6877*pow( __DEB_PI,2.0)*pow(x,5.0))/300
//		-(182959673*pow( x,6.0))/1555200+(6961*pow( __DEB_PI,2.0)*pow( x,6.0))/432
//		+(185330293*pow( x,7.0))/3175200-(1003*pow( __DEB_PI,2.0)*pow( x,7.0))/126
//		-(982272667*pow( x,8.0))/50803200+(3533* pow(__DEB_PI,2.0)*pow( x,8.0))/1344
//		+(439764607*pow( x,9.0))/114307200-(263*pow( __DEB_PI ,2.0)*pow( x,9.0))/504
//		-(3538531*pow( x,10.0))/10160640+(7129* pow(__DEB_PI,2.0)*pow( x,10.0))/151200
//		+(84095* log(1.0-x))/36288-(79913* x* log(1.0-x))/5040
//		+(115923* x*x* log(1.0-x))/2240-(400579* x*x*x* log(1.0-x))/3780
//		+71689/480* x*x*x*x*log(1.0-x)-1197/8*pow( x,5.0)* log(1.0-x)
//		+(461789*pow( x,6.0)* log(1.0-x))/4320-22439/420*pow( x,7.0)* log(1.0-x)
//		+(39867*pow( x,8.0)*log(1.0-x))/2240-(161353*pow( x,9.0)* log(1.0-x))/45360
//		+(1303*pow( x,10.0)* log(1.0-x))/4032-(7381* Z3)/2520+10* x *Z3-45/2* x*x* Z3+40* x*x*x* Z3
//		-105/2*pow( x,4.0)* Z3+252/5*pow( x,5.0)* Z3-35*pow( x,6.0)* Z3+120/7*pow( x,7.0)* Z3
//		-45/8*pow( x,8.0)* Z3+10/9*pow( x,9.0)* Z3-1/10*pow( x,10.0)* Z3;
		
	
		//Parte vieja sin Simplify
		
		if(x==1){return Z4;}
		
		else{ cout<< "Mal valor de x en Li4" << x <<endl;
		
		return 0.0;}
		
	}

double Asmitad(double u,double t,double s){
	
	double x=-t/s,y=-u/s,z=-u/t;
	double X=log(-t/s),Y=log(-u/s);
	
	double Asm=
(128.0*Li4(z)-128.0*Li4(x)+128.0*Li4(y)+(-64.0/3.0+128*Y)*Li3(x)
+(64.0/3.0*X-64.0/3.0*__DEB_PI*__DEB_PI)*Li2(x)+16.0/3.0*X*X*X*X-64.0/3.0*X*X*X*Y+(-16.0+32.0/3.0*__DEB_PI*__DEB_PI+32.0*Y*Y)*X*X
+(-64/3.0*__DEB_PI*__DEB_PI*Y+48.0+160.0/9.0*__DEB_PI*__DEB_PI)*X+64.0/3.0*Z3+224.0/45.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI-128.0*Y*Z3)*(t/u)
+(32.0/3.0*Li3(x)-32.0/3.0*Li3(y)+(-32.0/3.0*X-32.0/3.0*Y)*Li2(x)
+(-32.0/3.0*Y*Y-80.0/9.0*__DEB_PI*__DEB_PI-64.0/3.0)*X+(64.0/3.0+32.0/3.0*__DEB_PI*__DEB_PI)*Y)*(t*t/s/s)+24.0*X*X*t*t/u/u
+(416.0/3.0*Li3(x)+64.0*Li3(y)*X-416.0/3.0*Li2(x)*X+(8.0*Y*Y+16)*X*X
+(-8.0/3.0*Y+80.0/3.0+112.0/9.0*__DEB_PI*__DEB_PI-64.0*Z3-64.0*Y*Y)*X-416.0/3.0*Z3-148.0/9.0*__DEB_PI*__DEB_PI+44.0/45.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI);
	
	
//Chequeado!!!!!!!!!!!!!!!!	
	
//	cout << Li4(0.1) <<endl;
	
	return Asm;
	
}




double Bsmitad(double u,double t,double s,double muR2){
	
	double x=-t/s,y=-u/s,z=-u/t;
	double X=log(-t/s),Y=log(-u/s),S=log(s/muR2);
	
//	double U=log(-u/muR2);
	
	double Bsm=
	
(-112.0*Li4(z)-88.0*Li4(y)+(-128.0*Y+48*X-64.0)*Li3(x)
+(-16.0*Y-16.0*X+12.0)*Li3(y)+(12.0*Y-4.0*Y*Y+8.0*X*X-8.0*__DEB_PI*__DEB_PI+64.0*X)*Li2(x)
+2.0/3.0*X*X*X*X+56.0/3.0*X*X*X*Y+(44.0*Y-4.0*__DEB_PI*__DEB_PI+2.0-32.0*Y*Y)*X*X
+(-4.0*Y*Y*Y-8.0-32.0*Z3-80.0/3.0*__DEB_PI*__DEB_PI+6.0*Y*Y+56.0/3.0*__DEB_PI*__DEB_PI*Y)*X
+Y*Y*Y*Y+6.0*Y*Y*Y+(-10.0/3.0*__DEB_PI*__DEB_PI-5.0)*Y*Y+(-39.0-18.0*__DEB_PI*__DEB_PI+144.0*Z3)*Y
+3*S+187.0/4.0-4.0*__DEB_PI*__DEB_PI*S+4.0/45.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI-5.0*__DEB_PI*__DEB_PI-20.0*Z3+48.0*Z3*S)*t/u
+(-12.0*X*X+(24.0*Y+24.0)*X-12.0*Y*Y-24.0*Y-12*__DEB_PI*__DEB_PI)*t*t/s/s+8.0*X*X*t*t/u/u
+(-80.0*Li4(y)+32.0*X*Li3(x)+(-128.0*X-152.0)*Li3(y)+152.0*Li2(x)*X
+8.0*Y*Y*Li2(y)+(-16.0*Y*Y-24.0)*X*X+(60.0*Y*Y+(28.0+32.0/3.0*__DEB_PI*__DEB_PI)*Y-58.0)*X
+14.0/3.0*Y*Y*Y*Y+44.0/3.0*Y*Y*Y+8.0/3.0*Y*Y*__DEB_PI*__DEB_PI+(96.0*Z3-32.0/3.0*__DEB_PI*__DEB_PI)*Y+32.0/45.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI+16.0*Z3
-86.0/3.0*__DEB_PI*__DEB_PI-2.0);

//Chequeado!!!!!!!!!!!!!

return Bsm;
}

double D2smitad(double u,double t,double s,double muR2){
	
	
	double x=-t/s,y=-u/s,z=-u/t;
	double X=log(-t/s),Y=log(-u/s),S=log(s/muR2);
	
	// double U=log(-u/muR2);
	
	double D2sm=
	
(48.0*Li4(z)-16.0*Li4(x)+24.0*Li4(y)+(56.0*Y-8.0*X+20.0)*Li3(x)
+(8.0*X-12.0+16.0*Y)*Li3(y)+(16.0/3.0*__DEB_PI*__DEB_PI-20.0*X-12.0*Y-8.0*X*X+4.0*Y*Y)*Li2(x)
+1.0/3.0*X*X*X*X+(-8.0*Y-70.0/9.0)*X*X*X+(-4.0*__DEB_PI*__DEB_PI+286.0/9.0-16.0*Y+14.0*Y*Y-44.0/3.0*S)*X*X
+(-22.0/9.0*__DEB_PI*__DEB_PI+4.0*Y*Y*Y-8.0*__DEB_PI*__DEB_PI*Y-6.0*Y*Y)*X-44.0/9.0*Y*Y*Y+(-4.0/3.0*__DEB_PI*__DEB_PI+35.0/9.0-22.0/3.0*S)*Y*Y
+(57-26.0/9.0*__DEB_PI*__DEB_PI-72.0*Z3-22.0*S)*Y+479.0/9.0*Z3+19.0/60.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI-52.0*Z3*S+1141.0/27.0*S
-215.0/18.0*__DEB_PI*__DEB_PI-43417.0/324.0+23.0/6.0*__DEB_PI*__DEB_PI*S)*t/u
+(6.0*X*X+(-12.0-12.0*Y)*X+6.0*Y*Y+12*Y+6.0*__DEB_PI*__DEB_PI)*t*t/s/s-6.0*X*X*t*t/u/u
+(16.0*Li4(y)+48.0*Li3(x)*Y+64.0*Li3(y)-8.0*Y*Y*Li2(y)-64.0*Li2(x)*X
-4.0/3.0*X*X*X*X+(-20.0/3.0*__DEB_PI*__DEB_PI+6.0*Y*Y)*X*X+(-24.0*Y*Y+(-16.0/3.0*__DEB_PI*__DEB_PI-14.0)*Y
-148.0/9.0*__DEB_PI*__DEB_PI)*X-112.0/9.0*Y*Y*Y+(-44.0/3.0*S+298.0/9.0)*Y*Y+(538.0/9.0-48.0*Z3-44.0/3.0*S)*Y
-8.0*Z3-1.0/3.0*__DEB_PI*__DEB_PI*__DEB_PI*__DEB_PI+61.0/9.0*__DEB_PI*__DEB_PI);

//Chequeado!!!!!!!!!!!!!!!!!!

return D2sm;
}


double E3smitad(double u,double t,double s,double muR2){
	
	
//	double x=-t/s,y=-u/s,z=-u/t;
	double X=log(-t/s),Y=log(-u/s),S=log(s/muR2);
//	double U=log(-u/muR2);
	
	
	
	double E3sm=

(16.0/9.0*X*X*X+(-76.0/9.0+8.0/3.0*S)*X*X+16.0/9.0*__DEB_PI*__DEB_PI*X+8.0/9.0*Y*Y*Y+(4.0/3.0*S-2.0/9.0)*Y*Y
+(8.0/9.0*__DEB_PI*__DEB_PI+4.0*S-10.0)*Y-1.0/3.0*__DEB_PI*__DEB_PI*S-202.0/27.0*S+19.0/9.0*__DEB_PI*__DEB_PI-2.0/9.0*Z3+3401.0/162.0)*t/u
+(16.0/9.0*__DEB_PI*__DEB_PI*X+16.0/9.0*Y*Y*Y+(8.0/3.0*S-52.0/9.0)*Y*Y+(-76.0/9.0+8.0/3.0*S)*Y+8.0/9.0*__DEB_PI*__DEB_PI);


 

//Chequeado!!!!!!!!!!!!!!!!!!!!!!!!

return E3sm;
}


double G1smitad(double u,double t,double s,double muR2){
	
	
//	double x=-t/s,y=-u/s,z=-u/t;
	double X=log(-t/s),Y=log(-u/s);
//	double U=log(-u/muR2),S=log(s/muR2);
	
	double G1sm=
	
(14.0*X*X*X*X+28.0*X*X*X+8.0*X*X*Y*Y+56.0*X*X*__DEB_PI*__DEB_PI-48.0*X*X+12.0*X*X*Y+32.0*X*Y*__DEB_PI*__DEB_PI
+80.0*__DEB_PI*__DEB_PI*X+2.0*Y*Y*Y*Y+12.0*Y*Y*Y-10.0*Y*Y+8.0*Y*Y*__DEB_PI*__DEB_PI+26.0*__DEB_PI*__DEB_PI+24.0*__DEB_PI*__DEB_PI*Y-84.0*Y
+102.0)*t/u+8.0*X*(X*X*X+X*X+4.0*__DEB_PI*__DEB_PI*X+2.0*__DEB_PI*__DEB_PI)*t*t/u/u+2.0*X*X*(X*X+4.0*__DEB_PI*__DEB_PI)*t*t*t/u/u/u
+(32.0*X*X*X+8.0*X*X*Y*Y+80.0*__DEB_PI*__DEB_PI*X+32.0*X*Y*__DEB_PI*__DEB_PI+8.0*Y*Y*X+8.0*Y*Y*Y*Y+32.0*Y*Y*__DEB_PI*__DEB_PI
-32.0*Y*Y-4.0-56.0*Y+24.0*__DEB_PI*__DEB_PI);


//Chequeado!!!!!!!!!!!!!!!!!!!!!!!!!!

return G1sm;
}
