#include "header.hpp"

#include "pph21loop.amplitude.set.hpp"
#include "pph41loop.amplitude.doublevirtual.hpp"

void pph21loop_amplitude_initialization(munich * MUC){
  static Logger logger("pph21loop_amplitude_initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MUC->osi->switch_OL){
#ifdef OPENLOOPS
    MUC->asi = new pph21loop_amplitude_OpenLoops_set();
#endif
  }
  else if (MUC->osi->switch_RCL){
#ifdef RECOLA
    MUC->asi = new pph21loop_amplitude_Recola_set();
#endif
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




#ifdef OPENLOOPS

pph21loop_amplitude_OpenLoops_set::pph21loop_amplitude_OpenLoops_set(){
  Logger logger("pph21loop_amplitude_OpenLoops_set::pph21loop_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

pph21loop_amplitude_OpenLoops_set::~pph21loop_amplitude_OpenLoops_set(){
  Logger logger("pph21loop_amplitude_OpenLoops_set::~pph21loop_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_amplitude_OpenLoops_set::calculate_H2_2loop(){
  static Logger logger("pph21loop_amplitude_OpenLoops_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pph41loop_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_amplitude_OpenLoops_set::calculate_H1gg_2loop(){
  static Logger logger("pph21loop_amplitude_OpenLoops_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_FATAL << "Amplitude call does not make sense here." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif



#ifdef RECOLA

pph21loop_amplitude_Recola_set::pph21loop_amplitude_Recola_set(){
  Logger logger("pph21loop_amplitude_Recola_set::pph21loop_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

pph21loop_amplitude_Recola_set::~pph21loop_amplitude_Recola_set(){
  Logger logger("pph21loop_amplitude_Recola_set::~pph21loop_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_amplitude_Recola_set::calculate_H2_2loop(){
  static Logger logger("pph21loop_amplitude_Recola_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pph41loop_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_amplitude_Recola_set::calculate_H1gg_2loop(){
  static Logger logger("pph21loop_amplitude_Recola_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_FATAL << "Amplitude call does not make sense here." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
