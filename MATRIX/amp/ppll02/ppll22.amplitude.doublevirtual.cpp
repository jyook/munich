#include "header.hpp"

#include "ppll22.amplitude.doublevirtual.hpp"

double_complex ppll22_F2_q(double LR, observable_set * osi) {
  static Logger logger("ppeexa03_F2_q");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  double_complex F2 = C_F * C_F * (255.0 / 32 + 29.0 * pi2_24 - 11.0 * pi4_180 - 15.0 * zeta3 / 2) + C_A * C_F * (-51157.0 / 2592 - 107.0 * pi2_144 + 31.0 * pi4_480 + 313.0 * zeta3 / 36 - 11.0 / 48 * PolyGamma_21) + C_F * osi->N_f * (4085.0 / 1296 + 7.0 * pi2_72 + zeta3 / 18 + PolyGamma_21 / 24);
  F2 += (LR - ri * pi) * (C_F * C_F * (-3.0 / 8 + pi2_2 - 6 * zeta3) + C_A * C_F * (-1339.0 / 216 - 23.0 * pi2_48 + 13.0 * zeta3 / 2) + C_F * osi->N_f * (119.0 / 108 + pi2_24));
  return F2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppll22_calculate_H2(observable_set * osi){
  static Logger logger("ppll22_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  double LR = 0;

  // quark form factors
  double_complex F1 = -4*C_F;
  double_complex F2 = ppll22_F2_q(LR, osi);
  
  double delta_I1 = 0.5 * C_F * pi2;
  const double beta0 = (11 * C_A - 4 * 0.5 * osi->N_f) / 6;
  
  osi->QT_A0 = osi->VA_b_ME2;
  
  // H1
  osi->QT_A1 = pow(osi->alpha_S * inv2pi, 1)*2*real(F1)*osi->QT_A0;
  
  // H2
  osi->QT_A2 = pow(osi->alpha_S * inv2pi, 2)*(norm(F1)+2*real(F2))*osi->QT_A0;
  osi->QT_H2_delta = osi->QT_A2;
  
  const double K = (67.0 / 18 - pi2_6) * C_A - 10.0 / 9 / 2 * osi->N_f;
  double delta_qT = zeta2 * K - 2.0 / 3 * zeta3 * beta0 + C_A * (-1214.0 / 81 + 5 * zeta2 * zeta2) + osi->N_f * 164.0 / 81 + 4 * zeta3 * beta0 - 4 * zeta2 * zeta2 * C_A;
  const double D2a = -14.0 / 9 * beta0 * C_F - (8.0 / 9 - 7.0 / 2 * zeta3) * C_F * C_A;
  const double g1a = (-3 + 24 * zeta2 - 48 * zeta3) * C_F * C_F + (-17.0 / 3 - 88.0 / 3 * zeta2 + 24 * zeta3) * C_F * C_A + (4.0 / 3 + 32.0 / 3 * zeta2) * C_F * 0.5 * osi->N_f;

  double delta_I2 = -0.5 * C_F * delta_qT + 0.5 * C_F * C_F * pi4 + C_F * (33.0 / 24 * beta0 * pi2 + 7.0 / 8 * K * pi2 + 7.0 / 12 * beta0 * PolyGamma_21);

  // scale dependent part
  delta_I2 = delta_I2 - 1. / 12 * LR * (24 * D2a + 3 * g1a + 36 * C_F * K - 7 * beta0 * C_F * pi2);

  // Catani scheme -> qT scheme
  osi->QT_H2_delta += 2 * osi->QT_A1 * delta_I1 * osi->alpha_S * inv2pi;
  osi->QT_H2_delta += osi->QT_A0 * delta_I2 * pow(osi->alpha_S * inv2pi,2);
  // Catani scheme -> qT scheme
  osi->QT_A1 += 2 * osi->QT_A0 * delta_I1 * osi->alpha_S * inv2pi;
  osi->QT_H1_delta = osi->QT_A1;
  // adapt alpha_S/Pi normalisation
  osi->QT_H1_delta /= 2;
  osi->QT_H2_delta /= 4;
  
  osi->QT_H1_delta /= (inv2pi * osi->alpha_S * osi->QT_A0);
  osi->QT_H2_delta /= (pow(inv2pi * osi->alpha_S, 2) * osi->QT_A0);
  
  //  cout << "H1=" << osi->QT_H1_delta << endl;
  //  cout << "H2=" << osi->QT_H2_delta << endl;
  //  cout << "H2=" << 4*( 59.0*zeta3/18 - 1535.0/192 + 215.0*pi2/216 - pi4/240.0 ) + 4.0/9 * ( -15*zeta3 + 511.0/16 - 67.0*pi2/12 + 17.0*pi4/45 ) + 4.0/3*osi->N_f/864*( 192*zeta3 + 1143 - 152*pi2 )<<endl;;

       
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

