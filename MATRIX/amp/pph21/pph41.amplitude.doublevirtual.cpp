#include "header.hpp"

#include "pph41.amplitude.doublevirtual.hpp"

void pph41_calculate_H2(observable_set * osi){
  static Logger logger("pph41_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  osi->QT_A0 = osi->VA_b_ME2;
  double LQ = 2.0*log( osi->msi->M_H / osi->msi->M_t ); 

  osi->QT_H1_delta = (C_A*pi2 + 11)/2.0 ;
  osi->QT_H2_delta = 11399.0/144 + 19.0/8*LQ + 133.0*pi2/8 + 13.0/16*pi4 - 55.0/2*zeta3 + osi->N_f*(-1189.0/144 + 2.0*LQ/3 - 5.0*pi2/12); // formula from HNNLO (/Need/setup.f)
      
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

