#include "header.hpp"

#include "ggllll34.amplitude.doublevirtual.hpp"

void ggllll34_calculate_H1(observable_set * osi){
  static Logger logger("ggllll34_calculate_H1");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  logger << LOG_DEBUG_VERBOSE << "NEW VT2 implementation called" << endl;

  ggllll34_calculate_amplitude_doublevirtual(osi);

  osi->QT_H1_delta = osi->QT_A1;

  osi->QT_H1_delta /= 2;

  osi->QT_H1_delta /= (osi->alpha_S * inv2pi * osi->QT_A0);
   
  logger << LOG_DEBUG << "osi->QT_H1_delta = " << osi->QT_H1_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

double_complex ggllll34_computeM(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<double_complex> &E) {
  static Logger logger("ggllll34_computeM");
  i5-=2;
  i6-=2;
  i7-=2;
  i8-=2;

//  cout << i1 << ", " << i2 << ", " << i5 << ", " << i6 << ", " << i7 << ", " << i8 << endl;

  double_complex result=0.0;
  result += E[0]*za[i5][i7]*zb[i6][i8];
  result += E[1]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i1][i8];
  result += E[2]*za[i1][i5]*za[i2][i7]*zb[i1][i6]*zb[i2][i8];
  result += E[3]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
  result += E[4]*za[i2][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  // multiply first bracket (...) of eqn. (3.12)
  result *= (zb[i2][i5]*za[i5][i1]+zb[i2][i6]*za[i6][i1]); // = |2 /p3 1>

  result += E[5]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i2][i8];
  result += E[6]*za[i1][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
  result += E[7]*za[i1][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  result += E[8]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i2][i8];

  if (hel==0) {
    result *= (zb[i1][i5]*za[i5][i2]+zb[i1][i6]*za[i6][i2]) * za[i1][i2]/zb[i1][i2]; // = C_LL = |1 /p3 2> * <12>/[12]
  } else if (hel==1) {
    result *= (zb[i2][i5]*za[i5][i1]+zb[i2][i6]*za[i6][i1]); // = C_LR = |2 /p3 1>
  }

  logger << LOG_DEBUG << "result=" << result << endl;
  return result;
}

void ggllll34_computeVVprimeHelAmplitudes(observable_set * osi, TypeV V1, TypeV V2, vector<fourvector> &p, const vector<vector<vector<double_complex> > > &E, vector<vector<vector<vector<vector<double_complex> > > > > &M_dressed) {
  // options for V1 and V2:
  // ----------------------
  // V_PHOT = 0: photon -> ll  decay
  // V_ZLL  = 1: Z   -> ll     decay
  // V_WMIN = 2: W^- -> l^- nu decay
  // V_WPLU = 3: W^+ -> l^+ nu decay
  // V_ZNN  = 4: Z   -> nu nu  decay

  double_complex cos_W = osi->msi->ccos_w;
  double_complex sin_W = osi->msi->csin_w;
  double M_Z = osi->msi->M_Z;
  double Gamma_Z = osi->msi->Gamma_Z;
  double M_W = osi->msi->M_W;
  double Gamma_W = osi->msi->Gamma_W;
  double Nf=osi->N_f;
  double_complex DV1,DV2; // propagators for the vector-boson decays
  double_complex LR56[2],LR78[2];
  double_complex Q_lep[4]; // charge left-handed [0] and right handed [1] particle of first decay 
                           // and the same for the second decay [2], [3]
  double_complex I_lep[4]; // isospin left-handed [0] and right handed [1] particle of first decay
                           // and the same for the second decay [2], [3]
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();

  // set propagators (DV1), and couplings (LR56) for decay of first boson (V1)
  if (V1==0) { // photon -> ll
    DV1 = ma2; // photon propagator

    Q_lep[0]=-1;   // left-handed lepton charge
    Q_lep[1]=-1;   // right-handed lepton charge
    LR56[0] = Q_lep[0]; // left-handed  photon->ll coupling
    LR56[1] = Q_lep[1]; // right-handed photon->ll coupling
  } else if (V1==1 || V1 == 4) { // Z -> ll or Z -> nu nu
    DV1 = ma2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z; // Z-boson propagator

    if (V1==1){ // Z -> ll
      Q_lep[0]=-1;   // left-handed lepton charge
      I_lep[0]=-0.5; // left-handed lepton isospin
      Q_lep[1]=-1;   // right-handed lepton charge
      I_lep[1]=0;    // right-handed lepton isospin
    } else if (V1==4){ // Z -> nu nu
      Q_lep[0]=0;    // left-handed neutrino charge
      I_lep[0]=0.5;  // left-handed neutrino isospin
      Q_lep[1]=0;    // no right-handed neutrino
      I_lep[1]=0;    // no right-handed neutrino
    }
    LR56[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W; // left-handed  Z -> ll or Z -> nu nu coupling
    LR56[1] = (I_lep[1]-Q_lep[1]*sin_W*sin_W)/sin_W/cos_W; // right-handed Z -> ll or Z -> nu nu coupling
  } else if (V1 == 2 || V1 == 3) { // Z -> ll or Z -> nu nu
    DV1 = ma2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W; // W-boson propagator

    LR56[0] = 1.0/sqrt(2)/sin_W; // left-handed  W -> l nu coupling
    LR56[1] = 0.0;               // right-handed W -> l nu coupling
  }
  // set propagators (DV2), and couplings (LR78) for decay of second boson (V2)
  if (V2==0) { // photon -> ll
    DV2 = mb2; // photon propagator

    Q_lep[2]=-1;   // left-handed lepton charge
    Q_lep[3]=-1;   // right-handed lepton charge
    LR78[0] = Q_lep[2]; // left-handed  photon->ll coupling
    LR78[1] = Q_lep[3]; // right-handed photon->ll coupling
  } else if (V2==1 || V2 == 4) { // W -> l nu decay
    DV2 = mb2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z; // Z-boson propagator

    if (V2==1){ // Z -> ll
      Q_lep[2]=-1;   // left-handed lepton charge
      I_lep[2]=-0.5; // left-handed lepton isospin
      Q_lep[3]=-1;   // right-handed lepton charge
      I_lep[3]=0;    // right-handed lepton isospin
    } else if (V2==4){ // Z -> nu nu
      Q_lep[2]=0;    // left-handed neutrino charge
      I_lep[2]=0.5;  // left-handed neutrino isospin
      Q_lep[3]=0;    // no right-handed neutrino
      I_lep[3]=0;    // no right-handed neutrino
    }
    LR78[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W; // left-handed  Z -> ll or Z -> nu nu coupling
    LR78[1] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W; // right-handed Z -> ll or Z -> nu nu coupling
  } else if (V2 == 2 || V2 == 3) { // W -> l nu decay
    DV2 = mb2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W; // W-boson propagator

    LR78[0] = 1.0/sqrt(2)/sin_W; // left-handed  W -> l nu coupling
    LR78[1] = 0.0;               // right-handed W -> l nu coupling
  }

  // set couplings (N_VV) of bosons to closed fermion loop
  double_complex N_VV=0.0;
  double_complex LZu=(0.5-2.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZu = osi->msi->cCminus_Zuu;
  double_complex LZd=(-0.5+1.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZd = osi->msi->cCminus_Zdd;
  double_complex RZu=-2.0/3*sin_W/cos_W;
  //  static double_complex RZu = osi->msi->cCminus_Zuu;
  double_complex RZd=1.0/3*sin_W/cos_W;
  //  static double_complex RZd = osi->msi->cCminus_Zdd;
  double_complex LW=1.0/sqrt(2)/sin_W;
  //  static double_complex LW = osi->msi->cCminus_W
  if (V1==0 && V2==0) { // gamma gamma
    N_VV = 2*(2.0/3)*(2.0/3)+(Nf-2)*(1.0/3)*(1.0/3);
  } else if (((V1==1 || V1==4) && V2==0) || (V1==0 && (V2==1 || V2==4))) { // Z gamma
    // note: sign mistake in 1503.04812, consistent with 1112.1531
    // MW: mistake in definition of L^gamma_f1f2 meant by this comment; 
    // there is indeed an additional minus sign in eqn. (3.15)?
    N_VV = 0.5*(2.0*(LZu+RZu)*2.0/3.0 + (Nf-2.0)*(LZd+RZd)*(-1.0/3.0));
  } else if ((V1==1 || V1==4) && (V2==1 || V2==4)) { // ZZ
    N_VV = 0.5*(2.0*(LZu*LZu+RZu*RZu)+(Nf-2.0)*(LZd*LZd+RZd*RZd));
  } else if ((V1==2 || V1==3) && (V2==2 || V2==3)) { // WW
    int Ng = Nf/2; // number of massless quark generations
    N_VV = 0.5*Ng*LW*LW;
  }

  // compute spinor products
  vector<vector<double_complex > > za, zb;
  vector<vector <double> > s_inv;
  calcSpinorProducts(p,za,zb,s_inv);

  // compute undressed amplitudes
  double_complex M_undressed[2][2][2][2][3];
  for (int loop=0; loop<2; loop++) {
    M_undressed[0][0][0][0][loop] = ggllll34_computeM(1,2,5,6,7,8,0,za,zb,E[loop][0]); // M_LLLL
    M_undressed[0][0][0][1][loop] = ggllll34_computeM(1,2,5,6,8,7,0,za,zb,E[loop][0]); // M_LLLR
    M_undressed[0][0][1][0][loop] = ggllll34_computeM(1,2,6,5,7,8,0,za,zb,E[loop][0]); // M_LLRL
    M_undressed[0][0][1][1][loop] = ggllll34_computeM(1,2,6,5,8,7,0,za,zb,E[loop][0]); // M_LLRR
    
    M_undressed[0][1][0][0][loop] = ggllll34_computeM(1,2,5,6,7,8,1,za,zb,E[loop][1]); // M_LRLL
    M_undressed[0][1][0][1][loop] = ggllll34_computeM(1,2,5,6,8,7,1,za,zb,E[loop][1]); // M_LRLR
    M_undressed[0][1][1][0][loop] = ggllll34_computeM(1,2,6,5,7,8,1,za,zb,E[loop][1]); // M_LRRL
    M_undressed[0][1][1][1][loop] = ggllll34_computeM(1,2,6,5,8,7,1,za,zb,E[loop][1]); // M_LRRR

    // get other amplitudes via charge conjucate [...]^C by replacing spinor products:
    // <ij> <-> [ij], i.e., replacing za <-> zb
    M_undressed[1][0][0][0][loop] = ggllll34_computeM(1,2,6,5,8,7,0,zb,za,E[loop][0]); // M_RLLL
    M_undressed[1][0][0][1][loop] = ggllll34_computeM(1,2,6,5,7,8,0,zb,za,E[loop][0]); // M_RLLR
    M_undressed[1][0][1][0][loop] = ggllll34_computeM(1,2,5,6,8,7,0,zb,za,E[loop][0]); // M_RLRL
    M_undressed[1][0][1][1][loop] = ggllll34_computeM(1,2,5,6,7,8,0,zb,za,E[loop][0]); // M_RLRR
     
    M_undressed[1][1][0][0][loop] = ggllll34_computeM(1,2,6,5,8,7,1,zb,za,E[loop][1]); // M_RRLL
    M_undressed[1][1][0][1][loop] = ggllll34_computeM(1,2,6,5,7,8,1,zb,za,E[loop][1]); // M_RRLR
    M_undressed[1][1][1][0][loop] = ggllll34_computeM(1,2,5,6,8,7,1,zb,za,E[loop][1]); // M_RRRL
    M_undressed[1][1][1][1][loop] = ggllll34_computeM(1,2,5,6,7,8,1,zb,za,E[loop][1]); // M_RRRR
  }

  // compute (and return) dressed amplitudes
  for (int loop=0; loop<2; loop++) {
    for (int h1=0; h1<2; h1++) {
      for (int h2=0; h2<2; h2++) {
        for (int h3=0; h3<2; h3++) {
	  for (int h4=0; h4<2; h4++) {
	    M_dressed[h1][h2][h3][h4][loop]  = N_VV*M_undressed[h1][h2][h3][h4][loop];
	    M_dressed[h1][h2][h3][h4][loop] *= (LR56[h3]*LR78[h4]/DV1/DV2);         
	    M_dressed[h1][h2][h3][h4][loop] *= double_complex(0,1);
	  }
        }
      }
    }
  }
}



void add_hel_amps(vector<vector<vector<vector<vector<double_complex> > > > > &M1, vector<vector<vector<vector<vector<double_complex> > > > > &M2) {
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
	for (int h4=0; h4<2; h4++) {
	  for (int loop=0; loop<2; loop++) {
	    M1[h1][h2][h3][h4][loop] += M2[h1][h2][h3][h4][loop];
	  }
	}
      }
    }
  }
}

void ggllll34_calculate_amplitude_doublevirtual(observable_set * osi) {
  static Logger logger("ggllll34_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<fourvector> p(osi->esi->p_parton[0].size()); 
  vector<vector<vector<vector<vector<double_complex> > > > > M_dressed,M_dressed2;
  vector<vector<vector<vector<vector<double_complex> > > > > M_dressed_tmp;
  M_dressed.resize(2);
  M_dressed2.resize(2);
  M_dressed_tmp.resize(2);
  for (int h1=0; h1<2; h1++) {
    M_dressed[h1].resize(2);
    M_dressed2[h1].resize(2);
    M_dressed_tmp[h1].resize(2);
    for (int h2=0; h2<2; h2++) {
      M_dressed[h1][h2].resize(2);
      M_dressed2[h1][h2].resize(2);
      M_dressed_tmp[h1][h2].resize(2);
      for (int h3=0; h3<2; h3++) {
        M_dressed[h1][h2][h3].resize(2);
        M_dressed2[h1][h2][h3].resize(2);
        M_dressed_tmp[h1][h2][h3].resize(2);
	for (int h4=0; h4<2; h4++) {
	  M_dressed[h1][h2][h3][h4].resize(2,0.0);
	  M_dressed2[h1][h2][h3][h4].resize(2,0.0);
	  M_dressed_tmp[h1][h2][h3][h4].resize(2);
	}
      }
    }
  }

  double sym_factor=1;

  if (osi->csi->subprocess == "gg_emmupvmve~") {
    // WW
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][6];
    p[5]=osi->esi->p_parton[0][5];
    p[6]=osi->esi->p_parton[0][4];
  } else if (osi->csi->subprocess == "gg_emmumepmup") {
    // ZZ -> 2l 2l' (DF)
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][5];
    p[5]=osi->esi->p_parton[0][4];
    p[6]=osi->esi->p_parton[0][6];
  } else if (osi->csi->subprocess == "gg_emepveve~" || osi->csi->subprocess == "gg_emepvmvm~") {
    // ZZ -> 2l 2nu (SF and DF); WW contribution to SF below
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][4];
    p[5]=osi->esi->p_parton[0][5];
    p[6]=osi->esi->p_parton[0][6];
  } else if (osi->csi->subprocess == "gg_ememepep") {
    // ZZ -> 4l (SF)
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][5];
    p[5]=osi->esi->p_parton[0][4];
    p[6]=osi->esi->p_parton[0][6];

    sym_factor=1.0/4;
  }
  
  vector<vector<vector<double_complex> > > E(2,vector<vector<double_complex> >(2,vector<double_complex>(9)));
  double s = (p[1]+p[2]).m2();
  double t = (p[1]-(p[3]+p[4])).m2();
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();
  
  logger << LOG_DEBUG << "sqrt(s)=" << sqrt(s) << ", sqrt(-t)=" << sqrt(-t) << ", sqrt(-u)=" << sqrt(-ma2-mb2+s+t) << ", ma=" << sqrt(ma2) << ", mb=" << sqrt(mb2) << endl;
  
  int static init=1;
  // initialize the form factor library
  if (init==1) {
    ggVVprime::getInstance().initAmplitude(osi->N_f);
    init=0;
  }

  //  bool debug = Log::getLogThreshold()<=LOG_DEBUG; // no debug mode implemented for ggVVamp
  
  // compute the form factors
  ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
  
  for (int loop=0; loop<2; loop++) {
    for (int hel=0; hel<2; hel++) {
      for (int i=1; i<=9; i++) {
        double Er,Ei;
        ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
        E[loop][hel][i-1] = double_complex(Er,Ei);
      }
    }
  }

  // compute dressed amplitudes for all contributions to respective process
  if (osi->csi->subprocess == "gg_emmupvmve~") { 
    // WW process
    // WW contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_WMIN, V_WPLU, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else if (osi->csi->subprocess == "gg_emmumepmup" || osi->csi->subprocess == "gg_ememepep") {
    // ZZ -> 2l 2l' (DF) and ZZ -> 4l (SF) process; additional ZZ contribution to SF below
    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_ZLL, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamgam contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_PHOT, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_PHOT, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // Zgam contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_ZLL, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else if (osi->csi->subprocess == "gg_emepveve~" || osi->csi->subprocess == "gg_emepvmvm~") {
    // ZZ -> 2l 2nu (SF and DF); WW contribution to SF below
    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_ZLL, V_ZNN, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_PHOT, V_ZNN, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else {
    assert(false);
  }
  
  // SF: take into account additional crossings
  if (osi->csi->subprocess == "gg_ememepep" || osi->csi->subprocess == "dd~_ememepep") {
    // SF: ZZ -> 4l
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][6];
    p[5]=osi->esi->p_parton[0][4];
    p[6]=osi->esi->p_parton[0][5];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
    for (int loop=0; loop<2; loop++) {
      for (int hel=0; hel<2; hel++) {
	for (int i=1; i<=9; i++) {
	  double Er,Ei;
	  ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
	  E[loop][hel][i-1] = double_complex(Er,Ei);
	}
      }
    }

    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_ZLL, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    // gamgam contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_PHOT, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_PHOT, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);    

    // Zgam contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_ZLL, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

  } else if (osi->csi->subprocess == "gg_emepveve~") {
    // SF: ZZ -> 2l 2nu
    p[0]=osi->esi->p_parton[0][0];
    p[1]=osi->esi->p_parton[0][1];
    p[2]=osi->esi->p_parton[0][2];
    p[3]=osi->esi->p_parton[0][3];
    p[4]=osi->esi->p_parton[0][6];
    p[5]=osi->esi->p_parton[0][5];
    p[6]=osi->esi->p_parton[0][4];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
    for (int loop=0; loop<2; loop++) {
      for (int hel=0; hel<2; hel++) {
	for (int i=1; i<=9; i++) {
	  double Er,Ei;
	  ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
	  E[loop][hel][i-1] = double_complex(Er,Ei);
	}
      }
    }

    // WW contribution
    ggllll34_computeVVprimeHelAmplitudes(osi, V_WMIN, V_WPLU, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }

  
  // square and add up helicity amplitudes
  osi->QT_A0=0;
  osi->QT_A1=0;
  osi->QT_A2=0;
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
	for (int h4=0; h4<2; h4++) {
	  osi->QT_A0 += norm(M_dressed[h1][h2][h3][h4][0])+norm(M_dressed2[h1][h2][h3][h4][0]);
	  osi->QT_A1 += 2*real(M_dressed[h1][h2][h3][h4][0]*conj(M_dressed[h1][h2][h3][h4][1]))+2*real(M_dressed2[h1][h2][h3][h4][0]*conj(M_dressed2[h1][h2][h3][h4][1]));
	  if (h3==h4) {
	    osi->QT_A0 -= 2.0*real(conj(M_dressed[h1][h2][h3][h4][0])*M_dressed2[h1][h2][h3][h4][0]);
          
	    osi->QT_A1 -= 2.0*real(conj(M_dressed[h1][h2][h3][h4][0])*M_dressed2[h1][h2][h3][h4][1]);
	    osi->QT_A1 -= 2.0*real(conj(M_dressed2[h1][h2][h3][h4][0])*M_dressed[h1][h2][h3][h4][1]);
	  }
        }
      }
    }
  }
  
  double alpha_e = osi->msi->alpha_e;

  osi->QT_A0 *= pow(4*M_PI*alpha_e,4);
  osi->QT_A0 /= (4*64); // averaging factor
  osi->QT_A0 *= 2; // color factor 1/2 d^AB * 1/2 d^AB
  osi->QT_A0 *= sym_factor;

  osi->QT_A1 *= pow(4*M_PI*alpha_e,4);
  osi->QT_A1 /= (4*64); // averaging factor
  osi->QT_A1 *= 2; // color factor 1/2 d^AB * 1/2 d^AB
  osi->QT_A1 *= sym_factor;

  // multiply with alpha_s^2 1-loop LO and alpha_s^3 2-loop NLO
  osi->QT_A0 *= pow(osi->alpha_S/2./M_PI,2);
  osi->QT_A1 *= pow(osi->alpha_S/2./M_PI,3);

  cout << pow(osi->alpha_S/2./M_PI,2) << endl;
  cout << osi->alpha_S << endl;

  // no idea why there is a factor of 4 missing:
  osi->QT_A0 *= 4.;
  osi->QT_A1 *= 4.;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
