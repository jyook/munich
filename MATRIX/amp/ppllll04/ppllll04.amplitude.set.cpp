#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"
#include "ppllll24.amplitude.doublevirtual.hpp"
#include "ggllll34.amplitude.doublevirtual.hpp"


void ppllll04_amplitude_initialization(munich * MUC){
  static Logger logger("ppllll04_amplitude_initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MUC->osi->switch_OL){
#ifdef OPENLOOPS
    MUC->asi = new ppllll04_amplitude_OpenLoops_set();
#endif
  }
  else if (MUC->osi->switch_RCL){
#ifdef RECOLA
    MUC->asi = new ppllll04_amplitude_Recola_set();
#endif
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




#ifdef OPENLOOPS

ppllll04_amplitude_OpenLoops_set::ppllll04_amplitude_OpenLoops_set(){
  Logger logger("ppllll04_amplitude_OpenLoops_set::ppllll04_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

ppllll04_amplitude_OpenLoops_set::~ppllll04_amplitude_OpenLoops_set(){
  Logger logger("ppllll04_amplitude_OpenLoops_set::~ppllll04_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppllll04_amplitude_OpenLoops_set::calculate_H2_2loop(){
  static Logger logger("ppllll04_amplitude_OpenLoops_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppllll24_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppllll04_amplitude_OpenLoops_set::calculate_H1gg_2loop(){
  static Logger logger("ppllll04_amplitude_OpenLoops_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ggllll34_calculate_H1(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif



#ifdef RECOLA

ppllll04_amplitude_Recola_set::ppllll04_amplitude_Recola_set(){
  Logger logger("ppllll04_amplitude_Recola_set::ppllll04_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

ppllll04_amplitude_Recola_set::~ppllll04_amplitude_Recola_set(){
  Logger logger("ppllll04_amplitude_Recola_set::~ppllll04_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppllll04_amplitude_Recola_set::calculate_H2_2loop(){
  static Logger logger("ppllll04_amplitude_Recola_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppllll24_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppllll04_amplitude_Recola_set::calculate_H1gg_2loop(){
  static Logger logger("ppllll04_amplitude_Recola_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ggllll34_calculate_H1(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
