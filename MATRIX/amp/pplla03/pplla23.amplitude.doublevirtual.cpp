#include "header.hpp"

#include "pplla23.amplitude.doublevirtual.hpp"

/*
#include <ginac/ginac.h>
#include <ginac/numeric.h>

void compute_GHPLs_Ginac(int weight, double u, double v, double *GPL1, double *GPL2, double *GPL3, double *GPL4, double *HPL1, double *HPL2, double *HPL3, double *HPL4) {
  
  GiNaC::Digits=17;
 
  assert(u>=0);
  assert(u<=1-v);
  assert(v>=0);
  assert(v<=1);
  if (weight < 1 || weight > 4) {
    assert(false && "ERROR: weight in compute_GHPLs_Ginac must be between 1 and 4. Exiting...");
  }

  for (int i=0; i<2; i++) {
    HPL1[i] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::H(GiNaC::lst(i),v).evalf()).to_double();
    if (weight >= 2) {
      for (int j=0; j<2; j++) {
 	HPL2[i+2*j] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::H(GiNaC::lst(i,j),v).evalf()).to_double();
 	if (weight >= 3) {
 	  for (int k=0; k<2; k++) {
 	    HPL3[i+2*j+2*2*k] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::H(GiNaC::lst(i,j,k),v).evalf()).to_double();
 	    if (weight >= 4) {
 	      for (int l=0; l<2; l++) {
 		HPL4[i+2*j+2*2*k+2*2*2*l] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::H(GiNaC::lst(i,j,k,l),v).evalf()).to_double();
 	      }
 	    }
 	  }
 	}
      }
    }
  }
  
  double mapping[] = {0,1,1-v,-v};
    
  for (int i=0; i<4; i++) {
    double ai = mapping[i];
    GPL1[i] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst(ai),u).evalf()).to_double();
    if (weight >= 2) {
      for (int j=0; j<4; j++) {
	double aj = mapping[j];
 	GPL2[i+4*j] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst(ai,aj),u).evalf()).to_double();
 	if (weight >= 3) {
 	  for (int k=0; k<4; k++) {
 	    double ak = mapping[k];
 	    GPL3[i+4*j+4*4*k] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst(ai,aj,ak),u).evalf()).to_double();
 	    if (weight >= 4) {
 	      for (int l=0; l<4; l++) {
 		double al = mapping[l];
 		GPL4[i+4*j+4*4*k+4*4*4*l] = GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst(ai,aj,ak,al),u).evalf()).to_double();
 	      }
 	    }
 	  }
 	}
      }
    }
  }
}
*/

double_complex pplla23_alpha0_Z(double u, double v, double e_q){return e_q;}
double_complex pplla23_beta0_Z(double u, double v, double e_q){return e_q;}
//double_complex gamma0_Z(double u, double v, double e_q){return 0;}
double_complex pplla23_alpha0_W(double u, double v, double e_q, double e_qprime, double q2, double shat){return e_q + (e_qprime - e_q) * (1. - u / (1. - v));}
double_complex pplla23_beta0_W(double u, double v, double e_q, double e_qprime, double q2, double shat){return e_qprime - (e_qprime - e_q) * u / (1. - v);}

double_complex pplla23_F2_q(double LR, observable_set * osi) {
  static Logger logger("pplla23_F2_q");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  double Nf = 5;
  double_complex F2 = C_F * C_F * (255.0 / 32 + 29.0 * pi2_24 - 11.0 * pi4_180 - 15.0 * zeta3 / 2) + C_A * C_F * (-51157.0 / 2592 - 107.0 * pi2_144 + 31.0 * pi4_480 + 313.0 * zeta3 / 36 - 11.0 / 48 * PolyGamma_21) + C_F * osi->N_f * (4085.0 / 1296 + 7.0 * pi2_72 + zeta3 / 18 + PolyGamma_21 / 24);
  F2 += (LR - ri * pi) * (C_F * C_F * (-3.0 / 8 + pi2_2 - 6 * zeta3) + C_A * C_F * (-1339.0 / 216 - 23.0 * pi2_48 + 13.0 * zeta3 / 2) + C_F * osi->N_f * (119.0 / 108 + pi2_24));
  return F2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pplla23_calculate_H2(observable_set * osi){
  static Logger logger("pplla23_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pplla23_calculate_amplitude_doublevirtual(osi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
void pplla23_calculate_H2(observable_set * osi){
  static Logger logger("pplla23_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pplla23_calculate_amplitude_doublevirtual(osi);

  //psi_xbs_all[0][0] -> s_hat
  double s_hat = 2 * osi->esi->p_parton[0][1] * osi->esi->p_parton[0][2];

  double LR = log(s_hat / osi->var_mu_ren / osi->var_mu_ren);
  double LF = log(s_hat / osi->var_mu_fact / osi->var_mu_fact);
  double LQ = log(s_hat / osi->QT_Qres / osi->QT_Qres);
  if (osi->QT_Qres == 0){LQ = 0.;}
  double A_F = 0.;

  //  cout << "osi->QT_A0 = " << osi->QT_A0 << endl;
  // shifted here from 'determine.integrand.VT.cxx'
  if (osi->QT_A0 == 0) {
    cout << "Should not happen anymore ?!?" << endl;
	
    osi->QT_A0 = osi->VA_b_ME2;
    osi->QT_A1 = osi->ME2;
    A_F = f2pi / osi->alpha_S * (osi->VA_V_ME2 + osi->VA_X_ME2) / osi->VA_b_ME2 - 2. / 3 * C_F * pi2 - 2. * gamma_q * LR + C_F * LR * LR;
    osi->QT_H1_delta = pi2_6 * C_F + 0.5 * A_F;
  }
  else {
    //    double s_hat = 2 * osi->esi->p_parton[0][1] * osi->esi->p_parton[0][2];
    double q2 = 2 * osi->esi->p_parton[0][3] * osi->esi->p_parton[0][4];
    //    s_hat =?= s[1][2]
    //    q2 =?= s[3][4]
    // FIXME: only works for Vgamma

    pplla23_calculate_H_coefficients(q2, s_hat, 2, osi);
    
    // adapt alpha_S/Pi normalisation
    osi->QT_H1_delta /= 2;
    osi->QT_H2_delta /= 4;
    
    osi->QT_H1_delta /= (inv2pi * osi->alpha_S * osi->QT_A0);
    osi->QT_H2_delta /= (pow(inv2pi * osi->alpha_S, 2) * osi->QT_A0);
    
    //    A_F = f2pi / osi->alpha_S * (osi->VA_V_ME2 + osi->VA_X_ME2) / osi->VA_b_ME2 - 2./3 * C_F * pi2 - 2. * gamma_q * LR + C_F * LR * LR;
    logger << LOG_DEBUG_VERBOSE << "H1: " << (pi2_6 * C_F + 0.5 * A_F) * osi->VA_b_ME2 << ", " << osi->QT_H1_delta * osi->QT_A0 << ", " <<  (pi2_6 * C_F + 0.5 * A_F)/osi->QT_H1_delta * osi->VA_b_ME2/osi->QT_A0 << endl;
  }
      
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

// takes the one- and two-loop amplitudes, subtracted with the Catani I operator (identical with the one used by GT) 
// and evaluated at the scale q2 and computes H1_delta and H2_delta at the scale s
void pplla23_calculate_H_coefficients(double q2, double s, int order, observable_set * osi){
  static Logger logger("pplla23_calculate_H_coefficients");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /*
  double LR = log(s_hat / osi->var_mu_ren / osi->var_mu_ren);
  double LF = log(s_hat / osi->var_mu_fact / osi->var_mu_fact);
  double LQ = log(s_hat / osi->QT_Qres / osi->QT_Qres);
  if (osi->QT_Qres == 0){LQ = 0.;}
  double A_F = 0.;
  */
  double delta_I1 = 0.5 * C_F * pi2;
  const double beta0 = (11 * C_A - 4 * 0.5 * osi->N_f) / 6;

  if (order == 2) {
    osi->QT_H2_delta = osi->QT_A2;
    // adapt I1, I2 conventions (Lorenzo -> Massimiliano)
    const double K = (67.0 / 18 - pi2_6) * C_A - 10.0 / 9 / 2 * osi->N_f;
    double delta_qT = zeta2 * K - 2.0 / 3 * zeta3 * beta0 + C_A * (-1214.0 / 81 + 5 * zeta2 * zeta2) + osi->N_f * 164.0 / 81 + 4 * zeta3 * beta0 - 4 * zeta2 * zeta2 * C_A;
    const double D2a = -14.0 / 9 * beta0 * C_F - (8.0 / 9 - 7.0 / 2 * zeta3) * C_F * C_A;
    const double g1a = (-3 + 24 * zeta2 - 48 * zeta3) * C_F * C_F + (-17.0 / 3 - 88.0 / 3 * zeta2 + 24 * zeta3) * C_F * C_A + (4.0 / 3 + 32.0 / 3 * zeta2) * C_F * 0.5 * osi->N_f;
    double delta_I2 = -0.5 * C_F * delta_qT + 0.5 * C_F * C_F * pi4 + C_F * (33.0 / 24 * beta0 * pi2 + 7.0 / 8 * K * pi2 + 7.0 / 12 * beta0 * PolyGamma_21);

    // scale dependent part
    // the two-loop amplitudes are given at muR=q, so we have to use this scale here
    double LR = log(q2 / s);
    delta_I2 = delta_I2 - 1. / 12 * LR * (24 * D2a + 3 * g1a + 36 * C_F * K - 7 * beta0 * C_F * pi2);

    osi->QT_H2_delta += 2 * osi->QT_A1 * delta_I1 * osi->alpha_S * inv2pi;
    osi->QT_H2_delta += osi->QT_A0 * delta_I2 * pow(osi->alpha_S * inv2pi,2);
  }

  osi->QT_H1_delta = osi->QT_A1;

  // adapt I1 conventions (Lorenzo -> Massimiliano)
  osi->QT_A1 += 2 * osi->QT_A0 * delta_I1 * osi->alpha_S * inv2pi;
  osi->QT_H1_delta += 2 * osi->QT_A0 * delta_I1 * osi->alpha_S * inv2pi;

  // go to right scale
  osi->QT_H2_delta += beta0 * log(s / q2) * osi->QT_H1_delta * osi->alpha_S * inv2pi;
  //  osi->QT_H2_delta=4.0 * pow(alpha_S * inv2pi,2) * osi->QT_A0;
  //  cout << "osi->QT_H2_delta: " << osi->QT_H2_delta / 4 / pow(alpha_S * inv2pi,2) << endl;

  // adapt alpha_S/Pi normalisation
  osi->QT_H1_delta /= 2;
  osi->QT_H2_delta /= 4;
  
  osi->QT_H1_delta /= (inv2pi * osi->alpha_S * osi->QT_A0);
  osi->QT_H2_delta /= (pow(inv2pi * osi->alpha_S, 2) * osi->QT_A0);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void pplla23_calculate_helicity_amplitude_perm(int d, int e, int a, int b, int c, const vector<fourvector> &p, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<vector <double> > s, double e_q, double_complex sin_W, double M_V, double Gamma_V, int process, vector<double_complex> &Aplus_0, vector<double_complex> &Aplus_1, vector<double_complex> &Aplus_2_VR, vector<double_complex> &Aplus_2_VL, vector<double_complex> &Aplus_2_P, int order, observable_set * osi) {
  static Logger logger("pplla23_calculate_helicity_amplitude_perm");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

//  cout << d << e << a << b << c << endl;

  // adapt conventions
  d -= 2;
  e -= 2;
  if (a == 3){a = 5;}
  if (b == 3){b = 5;}
  if (c == 3){c = 5;}

  double q2 = (p[d] + p[e]).m2();
  double shat = (p[a] + p[c]).m2();
  //  cout << "q=" << sqrt(q2) << endl;

  double u = -s[a][b] / s[a][c];
  double v = q2 / s[a][c];
  //  cout << "u = " << u << ", v = " << v << ", u / (1 - v) = " << u / (1 - v) << ", q = " << sqrt(q2) << ", sqrt(s12) = " << sqrt(shat) << endl;

  double_complex alpha0;
  double_complex beta0;

  double e_qprime= 0.0 ;

  if (process == 1 || process == 4) {
    alpha0 = pplla23_alpha0_Z(u, v, e_q);
    beta0 = pplla23_beta0_Z(u, v, e_q);
    //    gamma0 = gamma0_Z(u, v, e_q);
  } 
  else if (process == 2 || process == 3) {
    if (process == 2) {
      if (a == 1) {
        e_q = -1.0 / 3;
        e_qprime = 2.0 / 3;
      } else {
        e_q = -2.0 / 3;
        e_qprime = 1.0 / 3;
      }
    } else {
      if (a == 1) {
        e_q = -2.0 / 3;
        e_qprime = 1.0 / 3;
      } else {
        e_q = -1.0 / 3;
        e_qprime = 2.0 / 3;
      }
    }

    alpha0 = pplla23_alpha0_W(u, v, e_q, e_qprime, q2, shat);
    beta0 = pplla23_beta0_W(u, v, e_q, e_qprime, q2, shat);
  }

  // Born
  Aplus_0[0] = alpha0 * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
  Aplus_0[0] -= beta0 * (za[c][d] * zb[b][e] / za[a][b]);
//  Aplus_0[0] += gamma0 * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
  Aplus_0[0] *= -(2 * sqrt2);

  Aplus_0[1] = alpha0 * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
  Aplus_0[1] -= beta0 * conj(za[c][d] * zb[b][e] / za[a][b]);
//  Aplus_0[1] += gamma0 * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
  Aplus_0[1] *= -(2 * sqrt2);

//  cout << "alpha: " << alpha0 * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b])) << ", alpha = " << alpha0 << endl;
//  cout << "beta: " << beta0 * conj(za[c][d] * zb[b][e] / za[a][b]) << ", beta = " << beta0 << endl;
//  cout << "total: " << Aplus_0[0] << endl;
//  cout << "spinor structure: " << (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b])) << endl;
//  cout << "spinor structure: " << (za[c][d] * zb[b][e] / za[a][b]) << endl;
//  cout << "gives " << norm(Aplus_0[0]) << endl;

  double GYZ1[4], GYZ2[4 * 4], GYZ3[4 * 4 * 4], GYZ4[4 * 4 * 4 * 4];
  double HZ1[2], HZ2[2 * 2], HZ3[2 * 2 * 2], HZ4[2 * 2 * 2 * 2];

  if (process == 1 || process == 4) {
    double_complex alpha1;
    double_complex beta1;
    double_complex gamma1;
    double_complex alpha2_VR;
    double_complex beta2_VR;
    double_complex gamma2_VR;
    double_complex alpha2_VL;
    double_complex beta2_VL;
    double_complex gamma2_VL;
    double_complex alpha2_P;
    double_complex beta2_P;
    double_complex gamma2_P;

    if (order>=1) {
      if (order == 1)
// old version:        compute_GHPLs(2, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);
        compute_GHPLs(3, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);
        // compute_GHPLs_Ginac(3, u, v, GYZ1g, GYZ2g, GYZ3g, GYZ4g, HZ1g, HZ2g, HZ3g, HZ4g);
      else if (order == 2)
        compute_GHPLs(4, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);
	// compute_GHPLs_Ginac(4, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);

      Omega_1_ZP(u, v, e_q, GYZ1, GYZ2, HZ1, HZ2, alpha1, beta1, gamma1);
//      cout << "Omegas: " << alpha1 << ", " << beta1 << ", " << gamma1 << endl;

      Aplus_1[0] = alpha1 * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
      Aplus_1[0] -= beta1 * (za[c][d] * zb[b][e] / za[a][b]);
      Aplus_1[0] += gamma1 * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
      Aplus_1[0] *= -(2 * sqrt2);

//      cout << "tensors: " << za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]) << ", " << za[c][d] * zb[b][e] / za[a][b] << ", " << za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]) << endl;

      Aplus_1[1] = alpha1 * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
      Aplus_1[1] -= beta1 * conj(za[c][d] * zb[b][e] / za[a][b]);
      Aplus_1[1] += gamma1 * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
      Aplus_1[1] *= -(2 * sqrt2);

//      cout << Aplus_1[0] << endl;

      if (order == 2) {
        Omega_2_ZP(u, v, e_q, sin_W, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4, alpha2_VR, beta2_VR, gamma2_VR, alpha2_VL, beta2_VL, gamma2_VL, alpha2_P, beta2_P, gamma2_P);
  //      cout << alpha1 <<", " << beta1 << ", " << gamma1 << endl;

        Aplus_2_VR[0] = alpha2_VR * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VR[0] -= beta2_VR * (za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VR[0] += gamma2_VR * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VR[0] *= -(2 * sqrt2);

        Aplus_2_VL[0] = alpha2_VL * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VL[0] -= beta2_VL * (za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VL[0] += gamma2_VL * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VL[0] *= -(2 * sqrt2);

        Aplus_2_P[0] = alpha2_P * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_P[0] -= beta2_P * (za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_P[0] += gamma2_P * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_P[0] *= -(2 * sqrt2);

        Aplus_2_VR[1] = alpha2_VR * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VR[1] -= beta2_VR * conj(za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VR[1] += gamma2_VR * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VR[1] *= -(2 * sqrt2);

        Aplus_2_VL[1] = alpha2_VL * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VL[1] -= beta2_VL * conj(za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VL[1] += gamma2_VL * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VL[1] *= -(2 * sqrt2);

        Aplus_2_P[1] = alpha2_P * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_P[1] -= beta2_P * conj(za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_P[1] += gamma2_P * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_P[1] *= -(2 * sqrt2);

/*        Aplus_2_VR[0] = 0;
        Aplus_2_VL[0] = 0;
        Aplus_2_P[0] = 0;
        Aplus_2_VR[1] = 0;
        Aplus_2_VL[1] = 0;
        Aplus_2_P[1] = 0;
*/
      }

    }
  } else if (process == 2 || process == 3) {
    double_complex alpha1;
    double_complex beta1;
    double_complex gamma1;
    double_complex alpha2_W;
    double_complex beta2_W;
    double_complex gamma2_W;

    if (order>=1) {
      if (order == 1)
        compute_GHPLs(2, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);
      else if (order == 2)
        compute_GHPLs(4, u, v, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4);

      Omega_1_W(u, v, e_q, e_qprime, GYZ1, GYZ2, HZ1, HZ2, alpha1, beta1, gamma1);
//      cout << "Omegas: " << alpha1 << ", " << beta1 << ", " << gamma1 << endl;

      Aplus_1[0] = alpha1 * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
      Aplus_1[0] -= beta1 * (za[c][d] * zb[b][e] / za[a][b]);
      Aplus_1[0] += gamma1 * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
      Aplus_1[0] *= -(2 * sqrt2);

//      cout << "tensors: " << za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]) << ", " << za[c][d] * zb[b][e] / za[a][b] << ", " << za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]) << endl;

      Aplus_1[1] = alpha1 * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
      Aplus_1[1] -= beta1 * conj(za[c][d] * zb[b][e] / za[a][b]);
      Aplus_1[1] += gamma1 * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
      Aplus_1[1] *= -(2 * sqrt2);

//      cout << Aplus_1[0] << endl;

      if (order == 2) {
        Omega_2_W(u, v, e_q, e_qprime, GYZ1, GYZ2, GYZ3, GYZ4, HZ1, HZ2, HZ3, HZ4, alpha2_W, beta2_W, gamma2_W);
  //      cout << alpha1 <<", " << beta1 << ", " << gamma1 << endl;

        Aplus_2_VR[0] = alpha2_W * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VR[0] -= beta2_W * (za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VR[0] += gamma2_W * (za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VR[0] *= -(2 * sqrt2);

        Aplus_2_VR[1] = alpha2_W * conj(za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
        Aplus_2_VR[1] -= beta2_W * conj(za[c][d] * zb[b][e] / za[a][b]);
        Aplus_2_VR[1] += gamma2_W * conj(za[a][d] * zb[a][b] * zb[b][e] / (za[a][b] * zb[c][b]));
        Aplus_2_VR[1] *= -(2 * sqrt2);
      }
    }
  }

//  Aplus_0[0] = 0; Aplus_0[1] = 0;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void pplla23_calculate_helicity_amplitude_perm_FSR(int d, int e, int a, int b, int c, const vector<fourvector> &p, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<vector <double> > s, int process, vector<double_complex> &Aplus_0, vector<double_complex> &Aplus_1, vector<double_complex> &Aplus_2, int order, observable_set * osi) {
  static Logger logger("pplla23_calculate_helicity_amplitude_perm_FSR");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

//  cout << d << e << a << b << c << endl;

  // adapt conventions
  d -= 2;
  e -= 2;
  b = 5;

  // d = 5, e= 6; a = 1, b = 3, c = 2
//
//      Aplus_0[0] = 0;
//    Aplus_0[1] = 0;
//    Aplus_1[0] = 0;
//    Aplus_1[1] = 0;
//    Aplus_2[0] = 0;
//    Aplus_2[1] = 0;
//
//    return;

  // Born
  if (process == 1) {
    Aplus_0[0] = za[a][e] * za[c][d] * zb[a][e] / (za[b][e] * za[a][b]);
    Aplus_0[0] -= za[c][d] * zb[a][b] / za[b][e];
    Aplus_0[0] -= za[a][d] * zb[a][e] * za[c][d] / (za[a][b] * za[b][d]);
  } else if (process == 2 || process == 3) {
    double q2 = (p[d] + p[e]).m2();
    double shat = (p[a] + p[c]).m2();

    if ( (a == 1 && process == 2) || (a == 2 && process == 3)) {
      Aplus_0[0] = -za[a][e] * za[c][d] * zb[a][e] / (za[b][e] * za[a][b]);
      Aplus_0[0] += za[c][d] * zb[a][b] / za[b][e];
    } else {
      // note that the crossing can not be done as in 1112.1531 for the FSR part
//      Aplus_0[0] = -conj(za[5][4] * za[2][3] * (zb[4][1] * zb[4][1]+zb[4][1] * zb[5][4] * zb[2][1] / zb[2][5]) / (zb[1][5] * s[5][4]));
      Aplus_0[0] = -conj(-za[a][e] * zb[c][d] * zb[a][d] / (zb[b][d] * zb[a][b]));
//      cout << conj(-za[a][e] * zb[c][d] * zb[a][d] / (zb[b][d] * zb[a][b])) << endl;
    }
//    cout << "Aplus=" << Aplus_0[0] << endl;


    Aplus_0[0] += s[c][b] / (shat - q2) * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b]));
    Aplus_0[0] += s[a][b] / (shat - q2) * (za[c][d] * zb[b][e] / za[a][b]);

//    cout << s[c][b] / (shat - q2) * (za[c][d] * za[a][c] * zb[a][e] / (za[a][b] * za[c][b])) << endl;
//    cout << s[a][b] / (shat - q2) * (za[c][d] * zb[b][e] / za[a][b]) << endl;
//    cout << "Aplus=" << Aplus_0[0] << endl;
  } else if (process == 4) {
    Aplus_0[0] = 0;
    Aplus_0[1] = 0;
    Aplus_1[0] = 0;
    Aplus_1[1] = 0;
    Aplus_2[0] = 0;
    Aplus_2[1] = 0;

    return;
  }
  Aplus_0[0] *= (-2 * sqrt2);

  Aplus_0[1] = conj(Aplus_0[0]);

  if (order >= 1) {
    //  const double CF = 4.0 / 3;
    Aplus_1[0] = -4 * C_F * Aplus_0[0];
    Aplus_1[1] = -4 * C_F * Aplus_0[1];
  }

  if (order == 2) {
//    const double zeta2 = M_PI * M_PI / 6;
//    const double zeta3 = 1.20205690315959429;
//    double N = 3;
//    double Nf = 5;
//    const double CA = N;
//    const double CF = 4.0 / 3;
//    const double beta0 = (11 * C_A - 4 * 0.5 * Nf) / 6;
//    const double PolyGamma_21 = -2 * zeta3;

    double LR = log(s[3][4] / s[1][2]);

//    double_complex F2 = C_F * C_F * (255.0 / 32 + 29.0 * M_PI * M_PI / 24 - 11.0 * pow(M_PI,4) / 180 - 15.0 * zeta3 / 2)  +  C_A * C_F * (-51157.0 / 2592 - 107.0 * M_PI * M_PI / 144 + 31.0 * pow(M_PI,4) / 480 + 313.0 * zeta3 / 36 - 11.0 / 48 * PolyGamma_21)  +  C_F * Nf * (4085.0 / 1296 + 7.0 * M_PI * M_PI / 72 + zeta3 / 18 + PolyGamma_21 / 24);
//    F2 += (LR - double_complex(0,1) * M_PI) * (C_F * C_F * (-3.0 / 8 + M_PI * M_PI / 2 - 6 * zeta3) + C_A * C_F * (-1339.0 / 216 - 23.0 * M_PI * M_PI / 48 + 13.0 * zeta3 / 2) + C_F * Nf * (119.0 / 108 + M_PI * M_PI / 24));

    double_complex F2 = pplla23_F2_q(LR, osi);

    Aplus_2[0] = F2 * Aplus_0[0];
    Aplus_2[1] = F2 * Aplus_0[1];
  }

//  Aplus_0[0] = 0; Aplus_0[1] = 0;
//  Aplus_1[0] = 0; Aplus_1[1] = 0;
//  Aplus_2[0] = 0; Aplus_2[1] = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void pplla23_calculate_amplitude_doublevirtual(observable_set * osi){
  static Logger logger("pplla23_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;

  static int no_process_class;
  static int parton_identity;
  static vector<fourvector> p_amp(osi->esi->p_parton[0].size());

  if (initialization == 1){
    if (osi->csi->process_class == ""){}
    else if (osi->csi->process_class == "pp-emepa+X"){ // Z -> e⁻e⁺ gamma 
      no_process_class = 1;
      if      (osi->csi->subprocess == "uu~_emepa"){parton_identity = -2;}
      else if (osi->csi->subprocess == "cc~_emepa"){parton_identity = -4;}
      else if (osi->csi->subprocess == "dd~_emepa"){parton_identity = -1;}
      else if (osi->csi->subprocess == "ss~_emepa"){parton_identity = -3;}
      else if (osi->csi->subprocess == "bb~_emepa"){parton_identity = -5;}
      else {logger << LOG_FATAL << "calc_amplitudes: unknown process: " << osi->csi->subprocess << endl; exit(1);}
    }
    else if (osi->csi->process_class == "pp-emve~a+X"){ // W⁻ -> e⁻ ne~ gamma
      no_process_class = 2;
    }
    else if (osi->csi->process_class == "pp-epvea+X"){ // W⁺ -> e⁺ ne gamma
      no_process_class = 3;
    }
    else if (osi->csi->process_class == "pp-veve~a+X"){ // Z -> ne ne~ gamma
      no_process_class = 4;
      if      (osi->csi->subprocess == "uu~_veve~a"){parton_identity = -2;}
      else if (osi->csi->subprocess == "cc~_veve~a"){parton_identity = -4;}
      else if (osi->csi->subprocess == "dd~_veve~a"){parton_identity = -1;}
      else if (osi->csi->subprocess == "ss~_veve~a"){parton_identity = -3;}
      else if (osi->csi->subprocess == "bb~_veve~a"){parton_identity = -5;}
      else {logger << LOG_FATAL << "calc_amplitudes: unknown process: " << osi->csi->subprocess << endl; exit(1);}
    } else {
      logger << LOG_FATAL << "calc_amplitudes: unknown process class: " << osi->csi->process_class << endl;
      exit(1);
    }
  }

  if      (no_process_class == 1){
    p_amp[0] = osi->esi->p_parton[0][0];
    p_amp[1] = osi->esi->p_parton[0][2];
    p_amp[2] = osi->esi->p_parton[0][1];
    p_amp[3] = osi->esi->p_parton[0][4];
    p_amp[4] = osi->esi->p_parton[0][3];
    p_amp[5] = -osi->esi->p_parton[0][5];
  }
  else if (no_process_class == 2){
    p_amp[0] = osi->esi->p_parton[0][0];
    p_amp[1] = osi->esi->p_parton[0][2];
    p_amp[2] = osi->esi->p_parton[0][1];
    p_amp[3] = osi->esi->p_parton[0][4];
    p_amp[4] = osi->esi->p_parton[0][3];
    p_amp[5] = -osi->esi->p_parton[0][5];
  }
  else if (no_process_class == 3){
    p_amp[0] = osi->esi->p_parton[0][0];
    p_amp[1] = osi->esi->p_parton[0][2];
    p_amp[2] = osi->esi->p_parton[0][1];
    p_amp[3] = osi->esi->p_parton[0][3];
    p_amp[4] = osi->esi->p_parton[0][4];
    p_amp[5] = -osi->esi->p_parton[0][5];
  }
  else if (no_process_class == 4){
    p_amp[0] = osi->esi->p_parton[0][0];
    p_amp[1] = osi->esi->p_parton[0][2];
    p_amp[2] = osi->esi->p_parton[0][1];
    p_amp[3] = osi->esi->p_parton[0][4];
    p_amp[4] = osi->esi->p_parton[0][3];
    p_amp[5] = -osi->esi->p_parton[0][5];
  }

  logger << LOG_DEBUG_VERBOSE << "p_amp set." << endl;

  static vector<vector<double_complex> > za;//(p_amp.size());
  static vector<vector<double_complex> > zb;//(p_amp.size());
  static vector<vector<double> > s;//(p_amp.size());

  calcSpinorProducts(p_amp, za, zb, s);

  static int order = 2;

  static double I3_q;
  static double e_q;
  static double e_f;
  static double I3_f;
  if (initialization == 1){
    if (no_process_class == 1 || no_process_class == 4) {
      if (parton_identity == 2 || parton_identity == 4) {
	I3_q = 0.5;
	e_q = 2. / 3.;
      } 
      else if (parton_identity == 1 || parton_identity == 3 || parton_identity == 5) {
	I3_q = -0.5;
	e_q = -1. / 3.;
      } 
      else if (parton_identity == -2 || parton_identity == -4) {
	I3_q = -0.5;
	e_q = -2. / 3.;
      } 
      else if (parton_identity == -1 || parton_identity == -3 || parton_identity == -5) {
	I3_q = 0.5;
	e_q = 1.0 / 3.;
      }
    }

    if (no_process_class == 1) { // Zgam
      e_f = -1.; // charge of e^-
      I3_f = -0.5; // isospin of e^-
    } 
    else if (no_process_class == 2) { // W-gam
      e_f = -1.;
      I3_f = -0.5;
    } 
    else if (no_process_class == 3) { // W+gam
      e_f = 1.;
      I3_f = 0.5;
    } 
    else if (no_process_class == 4) { // Zgam -> nunugam
      e_f = 0.;
      I3_f = 0.5;
    }

    initialization = 0;
  }

  static double M_Z = osi->msi->M_Z;
  static double Gamma_Z = osi->msi->Gamma_Z;
  static double M_W = osi->msi->M_W;
  static double Gamma_W = osi->msi->Gamma_W;
  static double alpha_e = osi->msi->alpha_e;
  static double_complex cos_W = osi->msi->ccos_w;
  static double_complex sin_W = osi->msi->csin_w;

  // for MCFM comparisons
  //  sin_W = sqrt(0.22262651564387248);
  //  cos_W = sqrt(1.0-sin_W * sin_W);
  //logger << LOG_DEBUG_VERBOSE << "difference: " << norm(0.22262651564387248/sin_W/sin_W) << endl;

  // 56132, resonant piece
  vector<double_complex> Aplus56132_0(2);
  vector<double_complex> Aplus56132_1(2);
  vector<double_complex> Aplus56132_2_VR(2);
  vector<double_complex> Aplus56132_2_VL(2);
  vector<double_complex> Aplus56132_2_P(2);
  pplla23_calculate_helicity_amplitude_perm(5, 6, 1, 3, 2, p_amp, za, zb, s, e_q, sin_W, M_W, Gamma_W, no_process_class, Aplus56132_0, Aplus56132_1, Aplus56132_2_VR, Aplus56132_2_VL, Aplus56132_2_P, order, osi);
 
  // 56132 DY like piece
  vector<double_complex> Aplus56132_0_FSR(2);
  vector<double_complex> Aplus56132_1_FSR(2);
  vector<double_complex> Aplus56132_2_FSR(2);
  pplla23_calculate_helicity_amplitude_perm_FSR(5, 6, 1, 3, 2, p_amp, za, zb, s, no_process_class, Aplus56132_0_FSR, Aplus56132_1_FSR, Aplus56132_2_FSR, order, osi);

  // 65231, resonant piece
  vector<double_complex> Aplus65231_0(2);
  vector<double_complex> Aplus65231_1(2);
  vector<double_complex> Aplus65231_2_VR(2);
  vector<double_complex> Aplus65231_2_VL(2);
  vector<double_complex> Aplus65231_2_P(2);
  pplla23_calculate_helicity_amplitude_perm(6, 5, 2, 3, 1, p_amp, za, zb, s, e_q, sin_W, M_W, Gamma_W, no_process_class, Aplus65231_0, Aplus65231_1, Aplus65231_2_VR, Aplus65231_2_VL, Aplus65231_2_P, order, osi);

  // 65231, DY like piece
  vector<double_complex> Aplus65231_0_FSR(2);
  vector<double_complex> Aplus65231_1_FSR(2);
  vector<double_complex> Aplus65231_2_FSR(2);
  pplla23_calculate_helicity_amplitude_perm_FSR(6, 5, 2, 3, 1, p_amp, za, zb, s, no_process_class, Aplus65231_0_FSR, Aplus65231_1_FSR, Aplus65231_2_FSR, order, osi);

  // 56231, resonant piece
  vector<double_complex> Aplus56231_0(2);
  vector<double_complex> Aplus56231_1(2);
  vector<double_complex> Aplus56231_2_VR(2);
  vector<double_complex> Aplus56231_2_VL(2);
  vector<double_complex> Aplus56231_2_P(2);
  pplla23_calculate_helicity_amplitude_perm(5, 6, 2, 3, 1, p_amp, za, zb, s, e_q, sin_W, M_W, Gamma_W, no_process_class, Aplus56231_0, Aplus56231_1, Aplus56231_2_VR, Aplus56231_2_VL, Aplus56231_2_P, order, osi);

  // 56231, DY like piece
  vector<double_complex> Aplus56231_0_FSR(2);
  vector<double_complex> Aplus56231_1_FSR(2);
  vector<double_complex> Aplus56231_2_FSR(2);
  pplla23_calculate_helicity_amplitude_perm_FSR(5, 6, 2, 3, 1, p_amp, za, zb, s, no_process_class, Aplus56231_0_FSR, Aplus56231_1_FSR, Aplus56231_2_FSR, order, osi);

  // 65132, resonant piece
  vector<double_complex> Aplus65132_0(2);
  vector<double_complex> Aplus65132_1(2);
  vector<double_complex> Aplus65132_2_VR(2);
  vector<double_complex> Aplus65132_2_VL(2);
  vector<double_complex> Aplus65132_2_P(2);
  pplla23_calculate_helicity_amplitude_perm(6, 5, 1, 3, 2, p_amp, za, zb, s, e_q, sin_W, M_W, Gamma_W, no_process_class, Aplus65132_0, Aplus65132_1, Aplus65132_2_VR, Aplus65132_2_VL, Aplus65132_2_P, order, osi);

  // 65132, DY like piece
  vector<double_complex> Aplus65132_0_FSR(2);
  vector<double_complex> Aplus65132_1_FSR(2);
  vector<double_complex> Aplus65132_2_FSR(2);
  pplla23_calculate_helicity_amplitude_perm_FSR(6, 5, 1, 3, 2, p_amp, za, zb, s, no_process_class, Aplus65132_0_FSR, Aplus65132_1_FSR, Aplus65132_2_FSR, order, osi);

  // Z couplings
  static double_complex LZ_q1q2 = (I3_q - sin_W * sin_W * e_q) / (sin_W * cos_W);
  static double_complex LZ_f5f6 = (I3_f - sin_W * sin_W * e_f) / (sin_W * cos_W);
  static double_complex RZ_q1q2 = -sin_W * e_q / cos_W;
  static double_complex RZ_f5f6 = -sin_W * e_f / cos_W;

  // photon couplings
  static double LP_q1q2 = -e_q;
  static double LP_f5f6 = -e_f;
  static double RP_q1q2 = LP_q1q2;
  static double RP_f5f6 = LP_f5f6;

  // W couplings
  static double_complex LW_q1q2 = 1. / (sqrt2 * sin_W);
  static double_complex LW_f5f6 = 1. / (sqrt2 * sin_W);

  // propagator denominators
  double q2 = (p_amp[3] + p_amp[4]).m2();
  double_complex DZ = q2 - M_Z * M_Z + double_complex(0, 1) * Gamma_Z * M_Z;
  double_complex DP = q2;
  double_complex DW = q2 - M_W * M_W + double_complex(0, 1) * Gamma_W * M_W;

  // propagator denominators (FSR)
  double s12 = (p_amp[1] + p_amp[2]).m2();
  double_complex DZ_FSR = s12 - M_Z * M_Z + double_complex(0, 1) * Gamma_Z * M_Z;
  double_complex DP_FSR = s12;
  double_complex DW_FSR = s12 - M_W * M_W + double_complex(0, 1) * Gamma_W * M_W;

  osi->QT_A0 = 0.;
  osi->QT_A1 = 0.;
  osi->QT_A2 = 0.;

  if (no_process_class == 1 || no_process_class == 4) {
    vector<double_complex> Aplus56132_0_tot(2);
    vector<double_complex> Aplus65132_0_tot(2);
    vector<double_complex> Aplus56231_0_tot(2);
    vector<double_complex> Aplus65231_0_tot(2);

    Aplus56132_0_tot[0] = (RZ_q1q2 * RZ_f5f6 / DZ + RP_q1q2 * RP_f5f6 / DP) * Aplus56132_0[0] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56132_0_FSR[0];
    Aplus65132_0_tot[1] = -(LZ_q1q2 * RZ_f5f6 / DZ + LP_q1q2 * RP_f5f6 / DP) * Aplus65132_0[1] + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65132_0_FSR[1];
    Aplus56231_0_tot[0] = -(LZ_q1q2 * RZ_f5f6 / DZ + LP_q1q2 * RP_f5f6 / DP) * Aplus56231_0[0] + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56231_0_FSR[0];
    Aplus65231_0_tot[1] = (RZ_q1q2 * RZ_f5f6 / DZ + RP_q1q2 * RP_f5f6 / DP) * Aplus65231_0[1] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65231_0_FSR[1];

    Aplus65132_0_tot[0] = (RZ_q1q2 * LZ_f5f6 / DZ + RP_q1q2 * LP_f5f6 / DP) * Aplus65132_0[0] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65132_0_FSR[0];
    Aplus56132_0_tot[1] = -(LZ_q1q2 * LZ_f5f6 / DZ + LP_q1q2 * LP_f5f6 / DP) * Aplus56132_0[1] - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56132_0_FSR[1];
    Aplus65231_0_tot[0] = -(LZ_q1q2 * LZ_f5f6 / DZ + LP_q1q2 * LP_f5f6 / DP) * Aplus65231_0[0] - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65231_0_FSR[0];
    Aplus56231_0_tot[1] = (RZ_q1q2 * LZ_f5f6 / DZ + RP_q1q2 * LP_f5f6 / DP) * Aplus56231_0[1] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56231_0_FSR[1];

    osi->QT_A0 += norm(Aplus56132_0_tot[0]);
    osi->QT_A0 += norm(Aplus65132_0_tot[1]);
    osi->QT_A0 += norm(Aplus56231_0_tot[0]);
    osi->QT_A0 += norm(Aplus65231_0_tot[1]);

    osi->QT_A0 += norm(Aplus65132_0_tot[0]);
    osi->QT_A0 += norm(Aplus56132_0_tot[1]);
    osi->QT_A0 += norm(Aplus65231_0_tot[0]);
    osi->QT_A0 += norm(Aplus56231_0_tot[1]);

    if (order >= 1) {
      vector<double_complex> Aplus56132_1_tot(2);
      vector<double_complex> Aplus65132_1_tot(2);
      vector<double_complex> Aplus56231_1_tot(2);
      vector<double_complex> Aplus65231_1_tot(2);
      
      Aplus56132_1_tot[0] = (RZ_q1q2 * RZ_f5f6 / DZ + RP_q1q2 * RP_f5f6 / DP) * Aplus56132_1[0] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56132_1_FSR[0];
      Aplus65132_1_tot[1] = -(LZ_q1q2 * RZ_f5f6 / DZ + LP_q1q2 * RP_f5f6 / DP) * Aplus65132_1[1] + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65132_1_FSR[1];
      Aplus56231_1_tot[0] = -(LZ_q1q2 * RZ_f5f6 / DZ + LP_q1q2 * RP_f5f6 / DP) * Aplus56231_1[0] + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56231_1_FSR[0];
      Aplus65231_1_tot[1] = (RZ_q1q2 * RZ_f5f6 / DZ + RP_q1q2 * RP_f5f6 / DP) * Aplus65231_1[1] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65231_1_FSR[1];
      
      Aplus65132_1_tot[0] = (RZ_q1q2 * LZ_f5f6 / DZ + RP_q1q2 * LP_f5f6 / DP) * Aplus65132_1[0] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65132_1_FSR[0];
      Aplus56132_1_tot[1] = -(LZ_q1q2 * LZ_f5f6 / DZ + LP_q1q2 * LP_f5f6 / DP) * Aplus56132_1[1] - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56132_1_FSR[1];
      Aplus65231_1_tot[0] = -(LZ_q1q2 * LZ_f5f6 / DZ + LP_q1q2 * LP_f5f6 / DP) * Aplus65231_1[0] - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65231_1_FSR[0];
      Aplus56231_1_tot[1] = (RZ_q1q2 * LZ_f5f6 / DZ + RP_q1q2 * LP_f5f6 / DP) * Aplus56231_1[1] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56231_1_FSR[1];
      
      osi->QT_A1 += 2 * real(Aplus56132_0_tot[0] * conj(Aplus56132_1_tot[0]));
      osi->QT_A1 += 2 * real(Aplus65132_0_tot[1] * conj(Aplus65132_1_tot[1]));
      osi->QT_A1 += 2 * real(Aplus56231_0_tot[0] * conj(Aplus56231_1_tot[0]));
      osi->QT_A1 += 2 * real(Aplus65231_0_tot[1] * conj(Aplus65231_1_tot[1]));
      
      osi->QT_A1 += 2 * real(Aplus65132_0_tot[0] * conj(Aplus65132_1_tot[0]));
      osi->QT_A1 += 2 * real(Aplus56132_0_tot[1] * conj(Aplus56132_1_tot[1]));
      osi->QT_A1 += 2 * real(Aplus65231_0_tot[0] * conj(Aplus65231_1_tot[0]));
      osi->QT_A1 += 2 * real(Aplus56231_0_tot[1] * conj(Aplus56231_1_tot[1]));
      if (order == 2) {
	vector<double_complex> Aplus56132_2_tot(2);
	vector<double_complex> Aplus65132_2_tot(2);
	vector<double_complex> Aplus56231_2_tot(2);
	vector<double_complex> Aplus65231_2_tot(2);

	Aplus56132_2_tot[0] = RZ_q1q2 * RZ_f5f6 / DZ * Aplus56132_2_VR[0] + RP_q1q2 * RP_f5f6 / DP * Aplus56132_2_P[0] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56132_2_FSR[0];
	Aplus65132_2_tot[1] = -(LZ_q1q2 * RZ_f5f6 / DZ * Aplus65132_2_VL[1] + LP_q1q2 * RP_f5f6 / DP * Aplus65132_2_P[1]) + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65132_2_FSR[1];
	Aplus56231_2_tot[0] = -(LZ_q1q2 * RZ_f5f6 / DZ * Aplus56231_2_VL[0] + LP_q1q2 * RP_f5f6 / DP * Aplus56231_2_P[0]) + (LZ_q1q2 * RZ_f5f6 / DZ_FSR + LP_q1q2 * RP_f5f6 / DP_FSR) * Aplus56231_2_FSR[0];
	Aplus65231_2_tot[1] = RZ_q1q2 * RZ_f5f6 / DZ * Aplus65231_2_VR[1] + RP_q1q2 * RP_f5f6 / DP * Aplus65231_2_P[1] + (RZ_q1q2 * RZ_f5f6 / DZ_FSR + RP_q1q2 * RP_f5f6 / DP_FSR) * Aplus65231_2_FSR[1];
	
	Aplus65132_2_tot[0] = RZ_q1q2 * LZ_f5f6 / DZ * Aplus65132_2_VR[0] + RP_q1q2 * LP_f5f6 / DP * Aplus65132_2_P[0] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65132_2_FSR[0];
	Aplus56132_2_tot[1] = -(LZ_q1q2 * LZ_f5f6 / DZ * Aplus56132_2_VL[1] + LP_q1q2 * LP_f5f6 / DP * Aplus56132_2_P[1]) - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56132_2_FSR[1];
	Aplus65231_2_tot[0] = -(LZ_q1q2 * LZ_f5f6 / DZ * Aplus65231_2_VL[0] + LP_q1q2 * LP_f5f6 / DP * Aplus65231_2_P[0]) - (LZ_q1q2 * LZ_f5f6 / DZ_FSR + LP_q1q2 * LP_f5f6 / DP_FSR) * Aplus65231_2_FSR[0];
	Aplus56231_2_tot[1] = RZ_q1q2 * LZ_f5f6 / DZ * Aplus56231_2_VR[1] + RP_q1q2 * LP_f5f6 / DP * Aplus56231_2_P[1] - (RZ_q1q2 * LZ_f5f6 / DZ_FSR + RP_q1q2 * LP_f5f6 / DP_FSR) * Aplus56231_2_FSR[1];

        // one loop times one loop
        osi->QT_A2 += norm(Aplus56132_1_tot[0]);
        osi->QT_A2 += norm(Aplus65132_1_tot[1]);
        osi->QT_A2 += norm(Aplus56231_1_tot[0]);
        osi->QT_A2 += norm(Aplus65231_1_tot[1]);

        osi->QT_A2 += norm(Aplus65132_1_tot[0]);
        osi->QT_A2 += norm(Aplus56132_1_tot[1]);
        osi->QT_A2 += norm(Aplus65231_1_tot[0]);
        osi->QT_A2 += norm(Aplus56231_1_tot[1]);

        // two loop times Born
        osi->QT_A2 += 2 * real(Aplus56132_0_tot[0] * conj(Aplus56132_2_tot[0]));
        osi->QT_A2 += 2 * real(Aplus65132_0_tot[1] * conj(Aplus65132_2_tot[1]));
        osi->QT_A2 += 2 * real(Aplus56231_0_tot[0] * conj(Aplus56231_2_tot[0]));
        osi->QT_A2 += 2 * real(Aplus65231_0_tot[1] * conj(Aplus65231_2_tot[1]));

        osi->QT_A2 += 2 * real(Aplus65132_0_tot[0] * conj(Aplus65132_2_tot[0]));
        osi->QT_A2 += 2 * real(Aplus56132_0_tot[1] * conj(Aplus56132_2_tot[1]));
        osi->QT_A2 += 2 * real(Aplus65231_0_tot[0] * conj(Aplus65231_2_tot[0]));
        osi->QT_A2 += 2 * real(Aplus56231_0_tot[1] * conj(Aplus56231_2_tot[1]));
      }
    }
  } 
  else if (no_process_class == 2 || no_process_class == 3) {
    vector<double_complex> Aplus56132_0_tot(2);
    vector<double_complex> Aplus65231_0_tot(2);
    Aplus56132_0_tot[1] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus56132_0[1] + 1.0 / DW_FSR * Aplus56132_0_FSR[1]);
    Aplus65231_0_tot[0] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus65231_0[0] + 1.0 / DW_FSR * Aplus65231_0_FSR[0]);
    
    //    logger << LOG_DEBUG_VERBOSE << -LW_q1q2 * LW_f5f6 * 1.0 / DW * Aplus56132_0[1] << endl;
    //    logger << LOG_DEBUG_VERBOSE << LW_q1q2 * LW_f5f6 *  1.0 / DW_FSR * Aplus56132_0_FSR[1] << endl;
    //    logger << LOG_DEBUG_VERBOSE << -LW_q1q2 * LW_f5f6 * 1.0 / DW * Aplus65231_0[0] << endl;
    //    logger << LOG_DEBUG_VERBOSE << LW_q1q2 * LW_f5f6 *  1.0 / DW_FSR * Aplus65231_0_FSR[0] << endl;
    
    osi->QT_A0 += norm(Aplus56132_0_tot[1]);
    //    logger << LOG_DEBUG_VERBOSE << osi->QT_A0 / 8 / norm(LW_q1q2 * LW_f5f6) << endl;
    osi->QT_A0 += norm(Aplus65231_0_tot[0]);
    //    logger << LOG_DEBUG_VERBOSE << osi->QT_A0 / 8 / norm(LW_q1q2 * LW_f5f6) << endl;
    
    //    logger << LOG_DEBUG_VERBOSE << norm(Aplus56132_0_tot[1]) / (8.0 / sin_W / sin_W / sin_W / sin_W / 4.0) << ", " << norm(Aplus65231_0_tot[0]) / (8.0 / sin_W / sin_W / sin_W / sin_W / 4.0) << endl;
    //    logger << LOG_DEBUG_VERBOSE << "sum =" << osi->QT_A0 / (8.0 / sin_W / sin_W / sin_W / sin_W / 4.0) << endl;
    //    logger << LOG_DEBUG_VERBOSE << norm(Aplus56132_0_FSR[1]) << ", " << norm(Aplus65231_0_FSR[0]) << endl;
    
    if (order >=1 ) {
      vector<double_complex> Aplus56132_1_tot(2),Aplus65231_1_tot(2);
      
      Aplus56132_1_tot[1] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus56132_1[1] + 1.0 / DW_FSR * Aplus56132_1_FSR[1]);
      Aplus65231_1_tot[0] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus65231_1[0] + 1.0 / DW_FSR * Aplus65231_1_FSR[0]);
      
      osi->QT_A1 += 2 * real(Aplus56132_0_tot[1] * conj(Aplus56132_1_tot[1]));
      osi->QT_A1 += 2 * real(Aplus65231_0_tot[0] * conj(Aplus65231_1_tot[0]));
      
      if (order == 2) {
        vector<double_complex> Aplus56132_2_tot(2),Aplus65231_2_tot(2);

        Aplus56132_2_tot[1] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus56132_2_VR[1] + 1.0 / DW_FSR * Aplus56132_2_FSR[1]);
        Aplus65231_2_tot[0] = -LW_q1q2 * LW_f5f6 * (1.0 / DW * Aplus65231_2_VR[0] + 1.0 / DW_FSR * Aplus65231_2_FSR[0]);

        // one loop times one loop
        osi->QT_A2 += norm(Aplus56132_1_tot[1]);
        osi->QT_A2 += norm(Aplus65231_1_tot[0]);

        // two loop times Born
        osi->QT_A2 += 2 * real( Aplus56132_0_tot[1] * conj(Aplus56132_2_tot[1]));
        osi->QT_A2 += 2 * real( Aplus65231_0_tot[0] * conj(Aplus65231_2_tot[0]));
      }
    }
  }

  //  logger << LOG_DEBUG_VERBOSE << "osi->QT_A0=" << osi->QT_A0 << endl;
  osi->QT_A0 *= pow(f4pi * alpha_e, 3);
  osi->QT_A0 /= (4 * 9); // averaging factor
  osi->QT_A0 *= 3; // color factor
  //  logger << LOG_DEBUG_VERBOSE << "osi->QT_A0=" << osi->QT_A0 << endl;
  //  logger << LOG_DEBUG_VERBOSE << "fac=" << pow(f4pi * alpha_e,3) / 36 * 3 * 8 / sin_W / sin_W / sin_W / sin_W / 4.0 << endl;

  osi->QT_A1 *= pow(f4pi * alpha_e, 3);
  osi->QT_A1 /= (4 * 9); // averaging factor
  osi->QT_A1 *= 3; // color factor
  osi->QT_A1 *= (osi->alpha_S * inv2pi);

  osi->QT_A2 *= pow(f4pi * alpha_e, 3);
  osi->QT_A2 /= (4 * 9); // averaging factor
  osi->QT_A2 *= 3; // color factor
  osi->QT_A2 *= pow(osi->alpha_S * inv2pi, 2);

  
  //  logger << LOG_DEBUG_VERBOSE << "VF=" << pow(norm(LW_q1q2),2) * 8 * 3.0 / 4 / 9 * pow(f4pi * alpha_e,3) << endl;
  
  
  // for comparison with OL
  //  double LR=log(140 * 140 / s[1][2]);
  //  osi->QT_A1 -= 2 * (osi->alpha_S * inv2pi) * 0.5 / 3 * 4.0 / 3 * (3 * LR * LR + 9 * LR-f4pi * M_PI) * osi->QT_A0;
  
  
  //  logger << LOG_DEBUG_VERBOSE << setprecision(16) << osi->QT_A0 << ", " << osi->QT_A1 / (osi->alpha_S * inv2pi) << ", " << osi->QT_A2 / pow(osi->alpha_S * inv2pi,2) << endl;
  //  logger << LOG_DEBUG_VERBOSE << tmp * pow(f4pi * alpha_e,3) / 4 / 9 * 3 << endl;
  
  
  /* 
  // shift to respective process
  // include CKM factors
  double CKM_factor = 1.;
  if (osi->csi->process_class == "du~_emve~a" || osi->csi->process_class == "ud~_epvea"){CKM_factor = norm(osi->msi->V_ckm[1][1]);} 
  else if (osi->csi->process_class == "dc~_emve~a" || osi->csi->process_class == "uc~_epvea") {CKM_factor = norm(osi->msi->V_ckm[2][1]);}
  */
  logger << LOG_DEBUG_VERBOSE << "osi->QT_A0 = " << osi->QT_A0 << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->QT_A1 = " << osi->QT_A1 << endl;
  logger << LOG_DEBUG_VERBOSE << "osi->QT_A2 = " << osi->QT_A2 << endl;
  
  /*
  double s_hat = 2 * osi->esi->p_parton[0][1] * osi->esi->p_parton[0][2];

  double LR = log(s_hat / osi->var_mu_ren / osi->var_mu_ren);
  double LF = log(s_hat / osi->var_mu_fact / osi->var_mu_fact);
  double LQ = log(s_hat / osi->QT_Qres / osi->QT_Qres);
  if (osi->QT_Qres == 0){LQ = 0.;}
  double A_F = 0.;
  */
  //    double q2 = 2 * osi->esi->p_parton[0][3] * osi->esi->p_parton[0][4];
  //    s_hat =?= s[1][2]
  //    q2 =?= s[3][4]
  // FIXME: only works for Vgamma

  double s_hat = 2 * osi->esi->p_parton[0][1] * osi->esi->p_parton[0][2];

  pplla23_calculate_H_coefficients(q2, s_hat, 2, osi);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

