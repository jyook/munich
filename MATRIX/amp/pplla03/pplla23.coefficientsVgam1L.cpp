#include "header.hpp"

// 1-loop helicity amplitude coefficients for qq_bar -> Wgam/Zgam,
// see T. Gehrmann, L. Tancredi, 1112.1531

void alpha_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha) {
  alpha =
          CF * (
          + zeta2
          - GHPLs1[3]*HPLs1[0]
          + GHPLs2[0*4+3]
          + GHPLs1[0]*HPLs1[0]
          - HPLs2[0*2+0]
          + HPLs2[0*2+1]
          )

       + CF*double_complex(0,1)*M_PI * (
          + GHPLs1[3]
          + HPLs1[0]
          );

}

void alpha_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha) {
  alpha =
          CF*pow(u,-2) * (
          + GHPLs1[2]*HPLs1[0]
          + GHPLs2[2*4+1]
          - GHPLs1[1]*HPLs1[0]
          - GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-2)*v * (
          - 2.0*GHPLs1[2]*HPLs1[0]
          - 2.0*GHPLs2[2*4+1]
          + 2.0*GHPLs1[1]*HPLs1[0]
          + 2.0*GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-2)*pow(v,2) * (
          + GHPLs1[2]*HPLs1[0]
          + GHPLs2[2*4+1]
          - GHPLs1[1]*HPLs1[0]
          - GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-1) * (
          + GHPLs1[2]
          - HPLs1[1]
          )

       + CF*pow(u,-1)*v * (
          - 2.0*GHPLs1[2]
          + HPLs1[0]
          + 2.0*HPLs1[1]
          )

       + CF*pow(u,-1)*pow(v,2) * (
          + GHPLs1[2]
          - HPLs1[0]
          - HPLs1[1]
          )

       + CF * (
          - 7.0/2
          + 1.0/2*GHPLs1[2]
          - 1.0/2*HPLs1[1]
          )

       + CF*v*pow(1-u,-1) * (
          - 1.0/2
          - GHPLs1[2]
          + HPLs1[0]
          + HPLs1[1]
          )

       + CF*pow(v,2)*pow(1-u,-2) * (
          - 1.0/2*GHPLs1[2]
          + 1.0/2*HPLs1[0]
          + 1.0/2*HPLs1[1]
          )

       + CF*pow(v,2)*pow(1-u,-1) * (
          + GHPLs1[2]
          - HPLs1[0]
          - HPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2) * (
          + GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2)*v * (
          - 2.0*GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2)*pow(v,2) * (
          + GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1) * (
          + 1.0
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1)*v * (
          - 2.0
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1)*pow(v,2) * (
          + 1.0
          )

       + CF*double_complex(0,1)*M_PI * (
          + 1.0/2
          )

       + CF*double_complex(0,1)*M_PI*v*pow(1-u,-1) * (
          - 1.0
          )

       + CF*double_complex(0,1)*M_PI*pow(v,2)*pow(1-u,-2) * (
          - 1.0/2
          )

       + CF*double_complex(0,1)*M_PI*pow(v,2)*pow(1-u,-1) * (
          + 1.0
          );
}

void alpha_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha) {
  alpha =
          CF * (
          - 4.0
          )

       + CF*u*pow(1-v,-1) * (
          + 4.0
          );

}

void beta_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta) {
  beta =
          CF*pow(1-v,-1) * (
          - 3.0/2*HPLs1[0]
          )

       + CF * (
          - 9.0/2
          + zeta2
          - GHPLs1[3]*HPLs1[0]
          + GHPLs2[0*4+3]
          + GHPLs1[0]*HPLs1[0]
          + 3.0/2*HPLs1[0]
          - HPLs2[0*2+0]
          + HPLs2[0*2+1]
          )

       + CF*u*pow(1-v,-2) * (
          + 1.0/2*HPLs1[0]
          )

       + CF*u*pow(1-v,-1) * (
          + 1.0/2
          - 1.0/2*HPLs1[0]
          )

       + CF*double_complex(0,1)*M_PI * (
          + GHPLs1[3]
          + HPLs1[0]
          );

}

void beta_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta) {
  beta =
          CF*pow(u,-2) * (
          + GHPLs1[2]*HPLs1[0]
          + GHPLs2[2*4+1]
          - GHPLs1[1]*HPLs1[0]
          - GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-2)*v * (
          - GHPLs1[2]*HPLs1[0]
          - GHPLs2[2*4+1]
          + GHPLs1[1]*HPLs1[0]
          + GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-1) * (
          + GHPLs1[2]
          - HPLs1[1]
          )

       + CF*pow(u,-1)*v * (
          - GHPLs1[2]
          + GHPLs1[2]*HPLs1[0]
          + GHPLs2[2*4+1]
          - GHPLs1[1]*HPLs1[0]
          - GHPLs1[1]*HPLs1[1]
          + HPLs1[0]
          + HPLs1[1]
          )

       + CF*pow(1-v,-1) * (
          + 3.0/2*HPLs1[0]
          )

       + CF * (
          + 1.0/2
          + 1.0/2*GHPLs1[2]
          - 3.0/2*HPLs1[0]
          - 1.0/2*HPLs1[1]
          )

       + CF*v*pow(1-u,-1) * (
          + 1.0/2*GHPLs1[2]
          - 1.0/2*HPLs1[0]
          - 1.0/2*HPLs1[1]
          )

       + CF*u*pow(1-v,-2) * (
          + 1.0/2*HPLs1[0]
          )

       + CF*u*pow(1-v,-1) * (
          + 1.0/2
          - 1.0/2*HPLs1[0]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2) * (
          + GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2)*v * (
          - GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1) * (
          + 1.0
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1)*v * (
          - 1.0
          + GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI * (
          + 1.0/2
          )

       + CF*double_complex(0,1)*M_PI*v*pow(1-u,-1) * (
          + 1.0/2
          );

}

void beta_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &beta) {
  beta =
          CF*u*pow(1-v,-1) * (
          + 4.0
          );

}

void gamma_1_1(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma) {
  gamma =
          CF*pow(1-v,-1) * (
          + 1.0/2*HPLs1[0]
          )

       + CF * (
          + 1.0/2
          - 1.0/2*HPLs1[0]
          )

       + CF*u*pow(1-v,-2) * (
          - 1.0/2*HPLs1[0]
          )

       + CF*u*pow(1-v,-1) * (
          - 1.0/2
          + 1.0/2*HPLs1[0]
          );

}

void gamma_1_2(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma) {
  gamma =
          CF*pow(u,-2)*v * (
          + GHPLs1[2]*HPLs1[0]
          + GHPLs2[2*4+1]
          - GHPLs1[1]*HPLs1[0]
          - GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-2)*pow(v,2) * (
          - GHPLs1[2]*HPLs1[0]
          - GHPLs2[2*4+1]
          + GHPLs1[1]*HPLs1[0]
          + GHPLs1[1]*HPLs1[1]
          )

       + CF*pow(u,-1)*v * (
          + GHPLs1[2]
          - GHPLs1[2]*HPLs1[0]
          - GHPLs2[2*4+1]
          + GHPLs1[1]*HPLs1[0]
          + GHPLs1[1]*HPLs1[1]
          - HPLs1[1]
          )

       + CF*pow(u,-1)*pow(v,2) * (
          - GHPLs1[2]
          + HPLs1[0]
          + HPLs1[1]
          )

       + CF*pow(1-v,-1) * (
          - 1.0/2*HPLs1[0]
          )

       + CF * (
          + 1.0/2*HPLs1[0]
          )

       + CF*v*pow(1-u,-1) * (
          + 1.0/2
          - 1.0/2*GHPLs1[2]
          + 1.0/2*HPLs1[0]
          + 1.0/2*HPLs1[1]
          )

       + CF*pow(v,2)*pow(1-u,-2) * (
          + 1.0/2*GHPLs1[2]
          - 1.0/2*HPLs1[0]
          - 1.0/2*HPLs1[1]
          )

       + CF*pow(v,2)*pow(1-u,-1) * (
          - GHPLs1[2]
          + HPLs1[0]
          + HPLs1[1]
          )

       + CF*u*pow(1-v,-2) * (
          - 1.0/2*HPLs1[0]
          )

       + CF*u*pow(1-v,-1) * (
          - 1.0/2
          + 1.0/2*HPLs1[0]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2)*v * (
          + GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-2)*pow(v,2) * (
          - GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1)*v * (
          + 1.0
          - GHPLs1[1]
          )

       + CF*double_complex(0,1)*M_PI*pow(u,-1)*pow(v,2) * (
          - 1.0
          )

       + CF*double_complex(0,1)*M_PI*v*pow(1-u,-1) * (
          - 1.0/2
          )

       + CF*double_complex(0,1)*M_PI*pow(v,2)*pow(1-u,-2) * (
          + 1.0/2
          )

       + CF*double_complex(0,1)*M_PI*pow(v,2)*pow(1-u,-1) * (
          - 1.0
          );

}

void gamma_1_3(double u, double v, double CF, double zeta2, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &gamma) {
  gamma = 0;
//          CF*u*pow(1-v,-1) * (
//          + 4.0
//          );

}

void Omega_1_ZP(double u, double v, double e_q, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha, double_complex &beta, double_complex &gamma) {
  double_complex Omega1, Omega2;
  //  const double zeta2 = pi2_6;
  //  const double CF=4.0/3;

  alpha_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  alpha_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  alpha = e_q * (Omega1 + Omega2);

  beta_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  beta_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  beta = e_q * (Omega1 + Omega2);

  gamma_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  gamma_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  gamma = e_q * (Omega1 + Omega2);
}

void Omega_1_W(double u, double v, double e_q, double e_qprime, double *GHPLs1, double *GHPLs2, double *HPLs1, double *HPLs2, double_complex &alpha, double_complex &beta, double_complex &gamma) {
  double_complex Omega1, Omega2, Omega3;
  //  const double zeta2 = pi2_6;
  //  const double CF = 4.0/3;

  alpha_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  alpha_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  alpha_1_3(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega3);
  alpha = e_qprime * Omega1 + e_q * Omega2 + Omega3;

  beta_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  beta_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  beta_1_3(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega3);
  beta = e_qprime * Omega1 + e_q * Omega2 + Omega3;

  gamma_1_1(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega1);
  gamma_1_2(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega2);
  gamma_1_3(u, v, C_F, zeta2, GHPLs1, GHPLs2, HPLs1, HPLs2, Omega3);
  gamma = e_qprime * Omega1 + e_q * Omega2 + Omega3;
}
