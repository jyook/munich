#include "header.hpp"

#include "pplla03.amplitude.set.hpp"
#include "pplla23.amplitude.doublevirtual.hpp"

void pplla03_amplitude_initialization(munich * MUC){
  static Logger logger("pplla03_amplitude_initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MUC->osi->switch_OL){
#ifdef OPENLOOPS
    MUC->asi = new pplla03_amplitude_OpenLoops_set();
#endif
  }
  else if (MUC->osi->switch_RCL){
#ifdef RECOLA
    MUC->asi = new pplla03_amplitude_Recola_set();
#endif
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




#ifdef OPENLOOPS

pplla03_amplitude_OpenLoops_set::pplla03_amplitude_OpenLoops_set(){
  Logger logger("pplla03_amplitude_OpenLoops_set::pplla03_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

pplla03_amplitude_OpenLoops_set::~pplla03_amplitude_OpenLoops_set(){
  Logger logger("pplla03_amplitude_OpenLoops_set::~pplla03_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pplla03_amplitude_OpenLoops_set::calculate_H2_2loop(){
  static Logger logger("pplla03_amplitude_OpenLoops_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pplla23_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pplla03_amplitude_OpenLoops_set::calculate_H1gg_2loop(){
  static Logger logger("pplla03_amplitude_OpenLoops_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  gglla33_calculate_H1(osi);
  logger << LOG_FATAL << "Amplitudes have not been implemented yet." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif



#ifdef RECOLA

pplla03_amplitude_Recola_set::pplla03_amplitude_Recola_set(){
  Logger logger("pplla03_amplitude_Recola_set::pplla03_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

pplla03_amplitude_Recola_set::~pplla03_amplitude_Recola_set(){
  Logger logger("pplla03_amplitude_Recola_set::~pplla03_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pplla03_amplitude_Recola_set::calculate_H2_2loop(){
  static Logger logger("pplla03_amplitude_Recola_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  pplla23_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pplla03_amplitude_Recola_set::calculate_H1gg_2loop(){
  static Logger logger("pplla03_amplitude_Recola_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  gglla33_calculate_H1(osi);
  logger << LOG_FATAL << "Amplitudes have not been implemented yet." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
