#include "header.hpp"

#include "ppww22.amplitude.doublevirtual.hpp"
//#include <ampww.h>

void ppww22_calculate_H2(observable_set * osi){
  static Logger logger("ppww22_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppww22_calculate_amplitude_doublevirtual(osi);

  osi->QT_H1_delta = osi->QT_A1;
  osi->QT_H2_delta = osi->QT_A2;

  osi->QT_H1_delta /= 2;
  osi->QT_H2_delta /= 4;

  osi->QT_H1_delta /= (osi->alpha_S * inv2pi * osi->QT_A0);
  osi->QT_H2_delta /= (pow(osi->alpha_S * inv2pi, 2) * osi->QT_A0);   

  logger << LOG_DEBUG << "osi->QT_H1_delta = " << osi->QT_H1_delta << ", osi->QT_H2_delta = " << osi->QT_H2_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppww22_calculate_amplitude_doublevirtual(observable_set * osi) {
  static Logger logger("ppww22_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static double g_L;
  static double g_R;
  static double Q;

  //  static double M_Z = osi->msi->M_Z;
  static double M2_Z = osi->msi->M2_Z;
  //  static double Gamma_Z = osi->msi->Gamma_Z;
  //  static double M_W = osi->msi->M_W;
  static double M2_W = osi->msi->M2_W;
  //  static double Gamma_W = osi->msi->Gamma_W;
  //  static double alpha_e = osi->msi->alpha_e;
  static double cos_w = osi->msi->cos_w;
  static double sin_w = osi->msi->sin_w;
  static double sin2_w = osi->msi->sin2_w;
  static double Cminus_Zdd = osi->msi->Cminus_Zdd;
  static double Cplus_Zdd = osi->msi->Cplus_Zdd;
  static double Cminus_Zuu = osi->msi->Cminus_Zuu;
  static double Cplus_Zuu = osi->msi->Cplus_Zuu;
  static double C_ZWminusWplus = osi->msi->C_ZWminusWplus;
  static vector<double> e_pow = osi->msi->e_pow;

  if (initialization == 1){
    if (osi->csi->type_parton[0][1] == 1 || osi->csi->type_parton[0][1] == 3 || osi->csi->type_parton[0][1] == 5) { // d, s, b
      g_L = 1. / (2. * sin_w * cos_w) * (-0.5 + 1.0 / 3. * sin2_w);
      logger << LOG_DEBUG << "g_L = " << g_L << endl;
      g_L = .5 * 1. * Cminus_Zdd;
      logger << LOG_DEBUG << "g_L = " << g_L << endl;
      g_R = 1. / (2. * sin_w * cos_w) / 3. * sin2_w;
      logger << LOG_DEBUG << "g_R = " << g_R << endl;
      g_R = .5 * 1. * Cplus_Zdd;
      logger << LOG_DEBUG << "g_R = " << g_R << endl;
      Q = -1./3.;
    } 
    else if (osi->csi->type_parton[0][1] == 2 || osi->csi->type_parton[0][1] == 4) { // u, c
      g_L = 1. / (2 * sin_w * cos_w)*(0.5 - 2. / 3. * sin2_w);
      logger << LOG_DEBUG << "g_L = " << g_L << endl;
      g_L = .5 * 1. * Cminus_Zuu;
      logger << LOG_DEBUG << "g_L = " << g_L << endl;
      g_R = -1. / (2. * sin_w * cos_w) * 2. / 3. * sin2_w;
      logger << LOG_DEBUG << "g_R = " << g_R << endl;
      g_R = .5 * 1. * Cplus_Zuu;
      logger << LOG_DEBUG << "g_R = " << g_R << endl;
      Q = 2./3.;
    }
    else {
      assert(false);
    }
    initialization = 0;
  }

  double s = (osi->esi->p_parton[0][1] + osi->esi->p_parton[0][2]).m2();
  double t = (osi->esi->p_parton[0][1] - osi->esi->p_parton[0][3]).m2();
  double u = (osi->esi->p_parton[0][1] - osi->esi->p_parton[0][4]).m2();

  //  static double e_Z = 1.*cos_W/sin_w;
  static double e_Z = C_ZWminusWplus;
  static double c_tt = e_pow[4] / (16 * pow(sin2_w, 2));

  double c_ts = e_pow[4] / (4 * s * sin2_w) * (Q + 2 * e_Z * g_L * s / (s - M2_Z));
  double c_ss = e_pow[4] / pow(s, 2) * (pow(Q + e_Z * (g_L + g_R) * s / (s - M2_Z), 2) + pow(e_Z * (g_L - g_R) * s / (s - M2_Z), 2));

  double N_Q = 2; // number of massless quark families; FIXME: fixed to 4-flavour-scheme

  double c_tt_closed = e_pow[4] / (32 * pow(sin_w, 4)) * N_Q;
  double c_ts_closed = e_pow[4] / (4 * s * sin2_w) * (Q + e_Z * (g_L + g_R) * s / (s - M2_Z)) * N_Q;

  //  double F,J,K;
  double f0_N,j0_N,k0_N,f1_CF_N,j1_CF_N,k1_CF_N,
           f11_CF2_N,j11_CF2_N,k11_CF2_N,
           f2_CF2_N,j2_CF2_N,k2_CF2_N,
           f2_CF,j2_CF,k2_CF,
           f2_CF_N_Nf,j2_CF_N_Nf,k2_CF_N_Nf,
           f2_CF_N_NfWW,j2_CF_N_NfWW;

    if (osi->csi->type_parton[0][1] == 1 || osi->csi->type_parton[0][1] == 3 || osi->csi->type_parton[0][1] == 5) { // d, s, b
      //      F = 16 * (u * t / pow(M2_W, 2) - 1) * (1.0 / 4 + pow(M2_W, 2) / u / u) + 16 * s / M2_W;
      //      J = -(16 * (u * t / pow(M2_W, 2) - 1) * (s / 4 - M2_W / 2 - pow(M2_W, 2) / u) + 16 * s * (s / M2_W - 2 + 2 * M2_W / u));
      //      K = 8 * (u * t / pow(M2_W, 2) - 1) * (s * s / 4 - s * M2_W + 3 * pow(M2_W, 2)) + 8 * s * s * (s / M2_W - 4);
      
      ampww(s,u,M2_W,f0_N,j0_N,k0_N,
	    f1_CF_N,j1_CF_N,k1_CF_N,
	    f11_CF2_N,j11_CF2_N,k11_CF2_N,
	    f2_CF2_N,j2_CF2_N,k2_CF2_N,
	    f2_CF,j2_CF,k2_CF,
	    f2_CF_N_Nf,j2_CF_N_Nf,k2_CF_N_Nf,
	    f2_CF_N_NfWW,j2_CF_N_NfWW);
      
      j0_N = -j0_N;
      j1_CF_N = -j1_CF_N;
      j11_CF2_N = -j11_CF2_N;
      j2_CF2_N = -j2_CF2_N;
      j2_CF = -j2_CF;
      j2_CF_N_Nf = -j2_CF_N_Nf;
      j2_CF_N_NfWW = -j2_CF_N_NfWW;
    }
    else if (osi->csi->type_parton[0][1] == 2 || osi->csi->type_parton[0][1] == 4) { // u, c
      //      F = 16 * (u * t / pow(M2_W, 2) - 1) * (1.0 / 4 + pow(M2_W,2) / t / t) + 16 * s / M2_W;
      //      J = 16 * (u * t / pow(M2_W,2) - 1) * (s / 4 - M2_W / 2 - pow(M2_W, 2) / t) + 16 * s * (s / M2_W - 2 + 2 * M2_W / t);
      //      K = 8 * (u * t / pow(M2_W, 2) - 1) * (s * s / 4 - s * M2_W + 3 * pow(M2_W, 2)) + 8 * s * s * (s / M2_W - 4);
      
      ampww(s,t,M2_W,f0_N,j0_N,k0_N,
	    f1_CF_N,j1_CF_N,k1_CF_N,
	    f11_CF2_N,j11_CF2_N,k11_CF2_N,
	    f2_CF2_N,j2_CF2_N,k2_CF2_N,
	    f2_CF,j2_CF,k2_CF,
	    f2_CF_N_Nf,j2_CF_N_Nf,k2_CF_N_Nf,
	    f2_CF_N_NfWW,j2_CF_N_NfWW);
    }
    else {
      assert(false);
    }
    
  /*
  logger << LOG_DEBUG << "coeff. functions: " << endl;
  logger << LOG_DEBUG << F << ", " << f0_N << endl;
  logger << LOG_DEBUG << J << ", " << j0_N << endl;
  logger << LOG_DEBUG << K << ", " << k0_N << endl;
  */
    //  static double Nc = 3;
  //  static double CF = C_F;
    //  static double Nf = osi->N_f; // FIXME
  /*
  double Nc=3;
  double CF=4.0/3;
  double Nf=osi->N_f; // FIXME
  */
  double NfWW = 0; //FIXME // ???
  NfWW = 1;

  osi->QT_A0 = N_c * (c_tt * f0_N - c_ts * j0_N + c_ss * k0_N);
//  osi->QT_A0 = N_c*(c_tt*F - c_ts*J + c_ss*K);
  osi->QT_A1 = N_c * C_F * (c_tt * f1_CF_N - c_ts * j1_CF_N + c_ss * k1_CF_N);

  osi->QT_A2 = N_c * C_F * C_F * (c_tt * f11_CF2_N - c_ts * j11_CF2_N + c_ss * k11_CF2_N);
  //  double compare_QT_A2 = osi->QT_A2;

  osi->QT_A2 += N_c * C_F * C_F * (c_tt * f2_CF2_N - c_ts * j2_CF2_N + c_ss * k2_CF2_N);
  osi->QT_A2 += C_F *(c_tt * f2_CF - c_ts * j2_CF + c_ss * k2_CF);
  osi->QT_A2 += N_c * C_F * osi->N_f * (c_tt * f2_CF_N_Nf - c_ts * j2_CF_N_Nf + c_ss * k2_CF_N_Nf);
  osi->QT_A2 += N_c * C_F * NfWW * (c_tt_closed * f2_CF_N_NfWW - c_ts_closed * j2_CF_N_NfWW);
//  logger << LOG_DEBUG << c_tt*F << ", " << c_ts*J << ", " << c_ss*K << endl;
//  logger << LOG_DEBUG <<  s << ", " << t << ", " << u << ", " << (u*t/pow(M_W,4)-1) << endl;

  double Born_factor = 1.;
  Born_factor /= (4 * 9); // averaging factor

  osi->QT_A0 *= Born_factor;
  osi->QT_A1 *= Born_factor;
  osi->QT_A2 *= Born_factor;

  osi->QT_A1 *= osi->alpha_S * inv2pi;
  osi->QT_A2 *= pow(osi->alpha_S * inv2pi, 2);

  /*
  logger << LOG_DEBUG << "amps:" << endl;
  logger << LOG_DEBUG << "osi->QT_A0 = " << osi->QT_A0 << ", " << b_ME2 << endl;
  logger << LOG_DEBUG << "osi->QT_A1 = " << osi->QT_A1 << ", " << M2L1[0] << endl;
  compare_QT_A2 *= Born_factor;
  compare_QT_A2 *= pow(osi->alpha_S/2/pi,2);
  logger << LOG_DEBUG << "osi->QT_A2 = " << compare_QT_A2 << ", " << M2L2[0] << endl;
  logger << LOG_DEBUG << endl;
  */
  //  olloopparametersscales_(&mu_ren, &one, &one);
   // ppwxw02callme2doublevirtual_(&b_ME2, M2L1, IRL1, M2L2, IRL2, ccP, &no_prc, &no_mod, &no_var);
//  V_ME2 = M2L1[0];
//  osi->QT_H1_delta = V_ME2;
////  osi->QT_H2_delta = 0.000001*b_ME2;
//  osi->QT_A0 = b_ME2;



  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
