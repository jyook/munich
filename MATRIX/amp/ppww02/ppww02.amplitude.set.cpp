#include "header.hpp"

#include "ppww02.amplitude.set.hpp"
#include "ppww22.amplitude.doublevirtual.hpp"

void ppww02_amplitude_initialization(munich * MUC){
  static Logger logger("ppww02_amplitude_initialization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MUC->osi->switch_OL){
#ifdef OPENLOOPS
    MUC->asi = new ppww02_amplitude_OpenLoops_set();
#endif
  }
  else if (MUC->osi->switch_RCL){
#ifdef RECOLA
    MUC->asi = new ppww02_amplitude_Recola_set();
#endif
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




#ifdef OPENLOOPS

ppww02_amplitude_OpenLoops_set::ppww02_amplitude_OpenLoops_set(){
  Logger logger("ppww02_amplitude_OpenLoops_set::ppww02_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

ppww02_amplitude_OpenLoops_set::~ppww02_amplitude_OpenLoops_set(){
  Logger logger("ppww02_amplitude_OpenLoops_set::~ppww02_amplitude_OpenLoops_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppww02_amplitude_OpenLoops_set::calculate_H2_2loop(){
  static Logger logger("ppww02_amplitude_OpenLoops_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppww22_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppww02_amplitude_OpenLoops_set::calculate_H1gg_2loop(){
  static Logger logger("ppww02_amplitude_OpenLoops_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  ggllll34_calculate_H1(osi);
  logger << LOG_FATAL << "Amplitudes have not been implemeted." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif



#ifdef RECOLA

ppww02_amplitude_Recola_set::ppww02_amplitude_Recola_set(){
  Logger logger("ppww02_amplitude_Recola_set::ppww02_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "constructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

ppww02_amplitude_Recola_set::~ppww02_amplitude_Recola_set(){
  Logger logger("ppww02_amplitude_Recola_set::~ppww02_amplitude_Recola_set ()");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  logger << LOG_INFO << "destructor called..." << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppww02_amplitude_Recola_set::calculate_H2_2loop(){
  static Logger logger("ppww02_amplitude_Recola_set::calculate_H2_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ppww22_calculate_H2(osi);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppww02_amplitude_Recola_set::calculate_H1gg_2loop(){
  static Logger logger("ppww02_amplitude_Recola_set::calculate_H1gg_2loop");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  ggllll34_calculate_H1(osi);
  logger << LOG_FATAL << "Amplitudes have not been implemeted." << endl;
  exit(1);
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

#endif
