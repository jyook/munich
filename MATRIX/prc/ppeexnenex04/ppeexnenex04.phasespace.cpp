#include "header.hpp"

#include "ppeexnenex04.phasespace.set.hpp"

ppeexnenex04_phasespace_set::~ppeexnenex04_phasespace_set(){
  static Logger logger("ppeexnenex04_phasespace_set::~ppeexnenex04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppeexnenex04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnenex04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 45;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 19;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 19;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 21;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 18;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 43;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnenex04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -23, -25};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-23);
      //  tau_MC_map.push_back(35);
      tau_MC_map.push_back(-36);
      tau_MC_map.push_back(-25);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_040_ddx_emepvevex(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_040_uux_emepvevex(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_040_bbx_emepvevex(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_040_aa_emepvevex(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_240_gg_emepvevex(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppeexnenex04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_040_ddx_emepvevex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_040_uux_emepvevex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_040_bbx_emepvevex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_040_aa_emepvevex(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_240_gg_emepvevex(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppeexnenex04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_040_ddx_emepvevex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_040_uux_emepvevex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_040_bbx_emepvevex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_040_aa_emepvevex(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_240_gg_emepvevex(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppeexnenex04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnenex04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 148;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 43;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 43;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 47;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 43;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 43;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 47;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 43;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 43;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 47;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 396;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 111;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 111;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 101;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 111;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 84;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 402;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 51;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 51;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 51;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 51;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 51;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 51;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 51;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 51;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 51;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnenex04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0, -23, -25};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      //  tau_MC_map.push_back(35);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  6){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 14){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 16){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_140_gd_emepvevexd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_140_gu_emepvevexu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_140_gb_emepvevexb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_140_gdx_emepvevexdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_140_gux_emepvevexux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_140_gbx_emepvevexbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_140_ddx_emepvevexg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_140_uux_emepvevexg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_140_bbx_emepvevexg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_050_da_emepvevexd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_050_ua_emepvevexu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_050_ba_emepvevexb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_050_dxa_emepvevexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_050_uxa_emepvevexux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_050_bxa_emepvevexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_050_ddx_emepvevexa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_050_uux_emepvevexa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_050_bbx_emepvevexa(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_050_aa_emepvevexa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_340_gg_emepvevexg(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_340_gd_emepvevexd(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_340_gu_emepvevexu(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_340_gb_emepvevexb(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_340_gdx_emepvevexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_340_gux_emepvevexux(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_340_gbx_emepvevexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_340_ddx_emepvevexg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_340_uux_emepvevexg(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_340_bbx_emepvevexg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppeexnenex04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_140_gd_emepvevexd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_140_gu_emepvevexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_140_gb_emepvevexb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_140_gdx_emepvevexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_140_gux_emepvevexux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_140_gbx_emepvevexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_140_ddx_emepvevexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_140_uux_emepvevexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_140_bbx_emepvevexg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_050_da_emepvevexd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_050_ua_emepvevexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_050_ba_emepvevexb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_050_dxa_emepvevexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_050_uxa_emepvevexux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_050_bxa_emepvevexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_050_ddx_emepvevexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_050_uux_emepvevexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_050_bbx_emepvevexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_050_aa_emepvevexa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_340_gg_emepvevexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_340_gd_emepvevexd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_340_gu_emepvevexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_340_gb_emepvevexb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_340_gdx_emepvevexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_340_gux_emepvevexux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_340_gbx_emepvevexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_340_ddx_emepvevexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_340_uux_emepvevexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_340_bbx_emepvevexg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppeexnenex04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_140_gd_emepvevexd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_140_gu_emepvevexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_140_gb_emepvevexb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_140_gdx_emepvevexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_140_gux_emepvevexux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_140_gbx_emepvevexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_140_ddx_emepvevexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_140_uux_emepvevexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_140_bbx_emepvevexg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_050_da_emepvevexd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_050_ua_emepvevexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_050_ba_emepvevexb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_050_dxa_emepvevexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_050_uxa_emepvevexux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_050_bxa_emepvevexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_050_ddx_emepvevexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_050_uux_emepvevexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_050_bbx_emepvevexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_050_aa_emepvevexa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_340_gg_emepvevexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_340_gd_emepvevexd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_340_gu_emepvevexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_340_gb_emepvevexb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_340_gdx_emepvevexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_340_gux_emepvevexux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_340_gbx_emepvevexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_340_ddx_emepvevexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_340_uux_emepvevexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_340_bbx_emepvevexg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppeexnenex04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnenex04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 1240;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 187;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 187;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 203;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 187;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 187;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 203;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 187;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 187;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 203;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 187;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 38){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 187;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 46){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 220;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 203;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 220;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 62){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 204;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 102;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 106;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 220;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnenex04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 46){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 62){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppeexnenex04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gg_emepvevexddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gg_emepvevexuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_240_gg_emepvevexbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_gd_emepvevexgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_gu_emepvevexgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_gb_emepvevexgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_gdx_emepvevexgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_gux_emepvevexgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_gbx_emepvevexgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_dd_emepvevexdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_du_emepvevexdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_ds_emepvevexds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_dc_emepvevexdc(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_240_dc_emepvevexus(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_db_emepvevexdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_ddx_emepvevexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_240_ddx_emepvevexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_ddx_emepvevexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_ddx_emepvevexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_ddx_emepvevexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_ddx_emepvevexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_240_dux_emepvevexdux(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_240_dux_emepvevexscx(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_240_dsx_emepvevexdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_240_dsx_emepvevexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_240_dcx_emepvevexdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_240_dbx_emepvevexdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_240_uu_emepvevexuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_240_uc_emepvevexuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_240_ub_emepvevexub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_240_udx_emepvevexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 38){ax_psp_240_udx_emepvevexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_240_uux_emepvevexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_240_uux_emepvevexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_240_uux_emepvevexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_240_uux_emepvevexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_240_uux_emepvevexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_240_uux_emepvevexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_240_usx_emepvevexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 46){ax_psp_240_ucx_emepvevexdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_240_ucx_emepvevexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_240_ubx_emepvevexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_240_bb_emepvevexbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_240_bdx_emepvevexbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_240_bux_emepvevexbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_240_bbx_emepvevexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_240_bbx_emepvevexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_240_bbx_emepvevexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_240_bbx_emepvevexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_240_dxdx_emepvevexdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_240_dxux_emepvevexdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_240_dxsx_emepvevexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_240_dxcx_emepvevexdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 62){ax_psp_240_dxcx_emepvevexuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_240_dxbx_emepvevexdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_240_uxux_emepvevexuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_240_uxcx_emepvevexuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_240_uxbx_emepvevexuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_240_bxbx_emepvevexbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppeexnenex04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gg_emepvevexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gg_emepvevexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_240_gg_emepvevexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_gd_emepvevexgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_gu_emepvevexgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_gb_emepvevexgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_gdx_emepvevexgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_gux_emepvevexgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_gbx_emepvevexgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_dd_emepvevexdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_du_emepvevexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_ds_emepvevexds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_dc_emepvevexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_240_dc_emepvevexus(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_db_emepvevexdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_ddx_emepvevexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_240_ddx_emepvevexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_ddx_emepvevexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_ddx_emepvevexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_ddx_emepvevexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_ddx_emepvevexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_240_dux_emepvevexdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_240_dux_emepvevexscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_240_dsx_emepvevexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_240_dsx_emepvevexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_240_dcx_emepvevexdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_240_dbx_emepvevexdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_240_uu_emepvevexuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_240_uc_emepvevexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_240_ub_emepvevexub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_240_udx_emepvevexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 38){ac_psp_240_udx_emepvevexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_240_uux_emepvevexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_240_uux_emepvevexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_240_uux_emepvevexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_240_uux_emepvevexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_240_uux_emepvevexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_240_uux_emepvevexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_240_usx_emepvevexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 46){ac_psp_240_ucx_emepvevexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_240_ucx_emepvevexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_240_ubx_emepvevexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_240_bb_emepvevexbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_240_bdx_emepvevexbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_240_bux_emepvevexbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_240_bbx_emepvevexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_240_bbx_emepvevexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_240_bbx_emepvevexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_240_bbx_emepvevexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_240_dxdx_emepvevexdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_240_dxux_emepvevexdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_240_dxsx_emepvevexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_240_dxcx_emepvevexdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 62){ac_psp_240_dxcx_emepvevexuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_240_dxbx_emepvevexdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_240_uxux_emepvevexuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_240_uxcx_emepvevexuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_240_uxbx_emepvevexuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_240_bxbx_emepvevexbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppeexnenex04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gg_emepvevexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gg_emepvevexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_240_gg_emepvevexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_gd_emepvevexgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_gu_emepvevexgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_gb_emepvevexgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_gdx_emepvevexgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_gux_emepvevexgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_gbx_emepvevexgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_dd_emepvevexdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_du_emepvevexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_ds_emepvevexds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_dc_emepvevexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_240_dc_emepvevexus(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_db_emepvevexdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_ddx_emepvevexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_240_ddx_emepvevexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_ddx_emepvevexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_ddx_emepvevexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_ddx_emepvevexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_ddx_emepvevexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_240_dux_emepvevexdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_240_dux_emepvevexscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_240_dsx_emepvevexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_240_dsx_emepvevexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_240_dcx_emepvevexdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_240_dbx_emepvevexdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_240_uu_emepvevexuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_240_uc_emepvevexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_240_ub_emepvevexub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_240_udx_emepvevexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 38){ag_psp_240_udx_emepvevexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_240_uux_emepvevexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_240_uux_emepvevexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_240_uux_emepvevexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_240_uux_emepvevexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_240_uux_emepvevexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_240_uux_emepvevexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_240_usx_emepvevexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 46){ag_psp_240_ucx_emepvevexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_240_ucx_emepvevexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_240_ubx_emepvevexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_240_bb_emepvevexbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_240_bdx_emepvevexbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_240_bux_emepvevexbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_240_bbx_emepvevexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_240_bbx_emepvevexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_240_bbx_emepvevexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_240_bbx_emepvevexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_240_dxdx_emepvevexdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_240_dxux_emepvevexdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_240_dxsx_emepvevexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_240_dxcx_emepvevexdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 62){ag_psp_240_dxcx_emepvevexuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_240_dxbx_emepvevexdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_240_uxux_emepvevexuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_240_uxcx_emepvevexuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_240_uxbx_emepvevexuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_240_bxbx_emepvevexbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
