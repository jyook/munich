#include "header.hpp"

#include "ppeexnenex04.event.set.hpp"

#define USERSWITCH(__switch__) user->switch_value[user->switch_map[__switch__]]
#define USERINT(__int__) user->int_value[user->int_map[__int__]]
#define USERDOUBLE(__double__) user->double_value[user->double_map[__double__]]
#define USERSTRING(__string__) user->string_value[user->string_map[__string__]]
#define USERCUT(__cut__) user->cut_value[user->cut_map[__cut__]]
#define USERPARTICLE(__particle__) user_particle[user->particle_map[__particle__]][i_a]
#define PARTICLE(__particle__) particle_event[access_object[__particle__]][i_a]
#define NUMBER(__particle__) n_object[access_object[__particle__]][i_a]

ppeexnenex04_event_set::~ppeexnenex04_event_set(){
  static Logger logger("ppeexnenex04_event_set::~ppeexnenex04_event_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::particles(int i_a){
  static Logger logger("ppeexnenex04_event_set::particles");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.particles.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::cuts(int i_a){
  static Logger logger("ppeexnenex04_event_set::cuts");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.cuts.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::phasespacepoint_born(){
  static Logger logger("ppeexnenex04_event_set::phasespacepoint_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "testpoint.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::phasespacepoint_collinear(){
  static Logger logger("ppeexnenex04_event_set::phasespacepoint_collinear");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "testpoint.collinear.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::phasespacepoint_real(){
  static Logger logger("ppeexnenex04_event_set::phasespacepoint_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "testpoint.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::phasespacepoint_realcollinear(){
  static Logger logger("ppeexnenex04_event_set::phasespacepoint_realcollinear");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "testpoint.realcollinear.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_event_set::phasespacepoint_doublereal(){
  static Logger logger("ppeexnenex04_event_set::phasespacepoint_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "testpoint.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
