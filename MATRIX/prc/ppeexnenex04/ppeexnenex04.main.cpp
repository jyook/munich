#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"

#include "ppeexnenex04.contribution.set.hpp"
#include "ppeexnenex04.event.set.hpp"
#include "ppeexnenex04.phasespace.set.hpp"
#include "ppeexnenex04.observable.set.hpp"
#include "ppeexnenex04.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emepveve~+X");

  MUC->csi = new ppeexnenex04_contribution_set();
  MUC->esi = new ppeexnenex04_event_set();
  MUC->psi = new ppeexnenex04_phasespace_set();
  MUC->osi = new ppeexnenex04_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllll04_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppeexnenex04_phasespace_set();
      MUC->psi->fake_psi->csi = new ppeexnenex04_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppeexnenex04_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
