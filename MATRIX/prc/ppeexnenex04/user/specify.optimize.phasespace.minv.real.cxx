if (csi->type_correction == "QCD"){
  static int switch_cut_M_leplep = user->switch_value[user->switch_map["M_leplep"]];
  static double cut_min_M_leplep = user->cut_value[user->cut_map["M_leplep"]];

  static int switch_cut_M_leplepnunu = user->switch_value[user->switch_map["M_leplepnunu"]];
  static double cut_min_M_leplepnunu = user->cut_value[user->cut_map["min_M_leplepnunu"]];

  static int switch_cut_deltaZ_M_leplepnunu = user->switch_value[user->switch_map["delta_M_leplepnunu"]];
  static double cut_max_deltaZ_M_leplepnunu = user->cut_value[user->cut_map["max_delta_M_leplepnunu"]];
  
  for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
    if (switch_cut_M_leplep == 1){sqrtsmin_opt[i_a][12] = cut_min_M_leplep;}
    
    if (switch_cut_M_leplepnunu == 1 && switch_cut_deltaZ_M_leplepnunu == 0){sqrtsmin_opt[i_a][60] = cut_min_M_leplepnunu;}
    if (switch_cut_M_leplepnunu == 0 && switch_cut_deltaZ_M_leplepnunu == 1){sqrtsmin_opt[i_a][60] = M[23] - cut_max_deltaZ_M_leplepnunu;}
    if (switch_cut_M_leplepnunu == 1 && switch_cut_deltaZ_M_leplepnunu == 1){sqrtsmin_opt[i_a][60] = max(cut_min_M_leplepnunu, M[23] - cut_max_deltaZ_M_leplepnunu);}
  }
}
