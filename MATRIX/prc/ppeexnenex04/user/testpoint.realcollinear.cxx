{
  psi->x_pdf[0] =      0.001786875034393859;
  psi->x_pdf[1] =        0.1074864015038463;
  psi->x_pdf[2] =       0.01662419626477046;

  psi->z_coll[1] =        0.1133990997194421;
  psi->z_coll[2] =        0.8439266609202911;

  p_parton[0][0] = fourvector(        295.9001126821332,                        0,                        0,                        0);
  p_parton[0][1] = fourvector(        147.9500563410666,                        0,                        0,        147.9500563410666);
  p_parton[0][2] = fourvector(        147.9500563410666,                        0,                        0,       -147.9500563410666);
  p_parton[0][3] = fourvector(        66.56183455155292,        2.125227720091452,        32.03635110661374,       -58.30637558432036);
  p_parton[0][4] = fourvector(        65.73074627813313,       -64.63249883220206,       0.4977865209460681,        11.95505372490129);
  p_parton[0][5] = fourvector(           77.86965307192,       -18.60178641819188,       -58.59567753225011,        47.79333621049817);
  p_parton[0][6] = fourvector(        84.99448848267429,        80.74673005956984,        26.51550629969955,      -0.9780505043232921);
  p_parton[0][7] = fourvector(       0.7433902978527805,       0.3623274707326419,      -0.4539663950092428,      -0.4639638467558053);
}
