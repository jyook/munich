#include "header.hpp"

#include "ppeexnenex04.summary.hpp"

ppeexnenex04_summary_generic::ppeexnenex04_summary_generic(munich * xmunich){
  Logger logger("ppeexnenex04_summary_generic::ppeexnenex04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeexnenex04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeexnenex04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeexnenex04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeexnenex04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeexnenex04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeexnenex04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_born(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subprocess[4] = "aa_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subprocess[4] = "aa_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subprocess[4] = "aa_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepveve~";
    subprocess[2] = "uu~_emepveve~";
    subprocess[3] = "bb~_emepveve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepveve~d";
    subprocess[2] = "gu_emepveve~u";
    subprocess[3] = "gb_emepveve~b";
    subprocess[4] = "gd~_emepveve~d~";
    subprocess[5] = "gu~_emepveve~u~";
    subprocess[6] = "gb~_emepveve~b~";
    subprocess[7] = "dd~_emepveve~g";
    subprocess[8] = "uu~_emepveve~g";
    subprocess[9] = "bb~_emepveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emepveve~g";
    subprocess[2] = "gd_emepveve~d";
    subprocess[3] = "gu_emepveve~u";
    subprocess[4] = "gb_emepveve~b";
    subprocess[5] = "gd~_emepveve~d~";
    subprocess[6] = "gu~_emepveve~u~";
    subprocess[7] = "gb~_emepveve~b~";
    subprocess[8] = "dd~_emepveve~g";
    subprocess[9] = "uu~_emepveve~g";
    subprocess[10] = "bb~_emepveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emepveve~d";
    subprocess[2] = "ua_emepveve~u";
    subprocess[3] = "ba_emepveve~b";
    subprocess[4] = "d~a_emepveve~d~";
    subprocess[5] = "u~a_emepveve~u~";
    subprocess[6] = "b~a_emepveve~b~";
    subprocess[7] = "dd~_emepveve~a";
    subprocess[8] = "uu~_emepveve~a";
    subprocess[9] = "bb~_emepveve~a";
    subprocess[10] = "aa_emepveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepveve~d";
    subprocess[2] = "gu_emepveve~u";
    subprocess[3] = "gb_emepveve~b";
    subprocess[4] = "gd~_emepveve~d~";
    subprocess[5] = "gu~_emepveve~u~";
    subprocess[6] = "gb~_emepveve~b~";
    subprocess[7] = "dd~_emepveve~g";
    subprocess[8] = "uu~_emepveve~g";
    subprocess[9] = "bb~_emepveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepveve~d";
    subprocess[2] = "ua_emepveve~u";
    subprocess[3] = "ba_emepveve~b";
    subprocess[4] = "d~a_emepveve~d~";
    subprocess[5] = "u~a_emepveve~u~";
    subprocess[6] = "b~a_emepveve~b~";
    subprocess[7] = "dd~_emepveve~a";
    subprocess[8] = "uu~_emepveve~a";
    subprocess[9] = "bb~_emepveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepveve~d";
    subprocess[2] = "gu_emepveve~u";
    subprocess[3] = "gb_emepveve~b";
    subprocess[4] = "gd~_emepveve~d~";
    subprocess[5] = "gu~_emepveve~u~";
    subprocess[6] = "gb~_emepveve~b~";
    subprocess[7] = "dd~_emepveve~g";
    subprocess[8] = "uu~_emepveve~g";
    subprocess[9] = "bb~_emepveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepveve~d";
    subprocess[2] = "ua_emepveve~u";
    subprocess[3] = "ba_emepveve~b";
    subprocess[4] = "d~a_emepveve~d~";
    subprocess[5] = "u~a_emepveve~u~";
    subprocess[6] = "b~a_emepveve~b~";
    subprocess[7] = "dd~_emepveve~a";
    subprocess[8] = "uu~_emepveve~a";
    subprocess[9] = "bb~_emepveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnenex04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeexnenex04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(60);
    subprocess[1] = "gg_emepveve~dd~";
    subprocess[2] = "gg_emepveve~uu~";
    subprocess[3] = "gg_emepveve~bb~";
    subprocess[4] = "gd_emepveve~gd";
    subprocess[5] = "gu_emepveve~gu";
    subprocess[6] = "gb_emepveve~gb";
    subprocess[7] = "gd~_emepveve~gd~";
    subprocess[8] = "gu~_emepveve~gu~";
    subprocess[9] = "gb~_emepveve~gb~";
    subprocess[10] = "dd_emepveve~dd";
    subprocess[11] = "du_emepveve~du";
    subprocess[12] = "ds_emepveve~ds";
    subprocess[13] = "dc_emepveve~dc";
    subprocess[14] = "dc_emepveve~us";
    subprocess[15] = "db_emepveve~db";
    subprocess[16] = "dd~_emepveve~gg";
    subprocess[17] = "dd~_emepveve~dd~";
    subprocess[18] = "dd~_emepveve~uu~";
    subprocess[19] = "dd~_emepveve~ss~";
    subprocess[20] = "dd~_emepveve~cc~";
    subprocess[21] = "dd~_emepveve~bb~";
    subprocess[22] = "du~_emepveve~du~";
    subprocess[23] = "du~_emepveve~sc~";
    subprocess[24] = "ds~_emepveve~ds~";
    subprocess[25] = "ds~_emepveve~uc~";
    subprocess[26] = "dc~_emepveve~dc~";
    subprocess[27] = "db~_emepveve~db~";
    subprocess[28] = "uu_emepveve~uu";
    subprocess[29] = "uc_emepveve~uc";
    subprocess[30] = "ub_emepveve~ub";
    subprocess[31] = "ud~_emepveve~ud~";
    subprocess[32] = "ud~_emepveve~cs~";
    subprocess[33] = "uu~_emepveve~gg";
    subprocess[34] = "uu~_emepveve~dd~";
    subprocess[35] = "uu~_emepveve~uu~";
    subprocess[36] = "uu~_emepveve~ss~";
    subprocess[37] = "uu~_emepveve~cc~";
    subprocess[38] = "uu~_emepveve~bb~";
    subprocess[39] = "us~_emepveve~us~";
    subprocess[40] = "uc~_emepveve~ds~";
    subprocess[41] = "uc~_emepveve~uc~";
    subprocess[42] = "ub~_emepveve~ub~";
    subprocess[43] = "bb_emepveve~bb";
    subprocess[44] = "bd~_emepveve~bd~";
    subprocess[45] = "bu~_emepveve~bu~";
    subprocess[46] = "bb~_emepveve~gg";
    subprocess[47] = "bb~_emepveve~dd~";
    subprocess[48] = "bb~_emepveve~uu~";
    subprocess[49] = "bb~_emepveve~bb~";
    subprocess[50] = "d~d~_emepveve~d~d~";
    subprocess[51] = "d~u~_emepveve~d~u~";
    subprocess[52] = "d~s~_emepveve~d~s~";
    subprocess[53] = "d~c~_emepveve~d~c~";
    subprocess[54] = "d~c~_emepveve~u~s~";
    subprocess[55] = "d~b~_emepveve~d~b~";
    subprocess[56] = "u~u~_emepveve~u~u~";
    subprocess[57] = "u~c~_emepveve~u~c~";
    subprocess[58] = "u~b~_emepveve~u~b~";
    subprocess[59] = "b~b~_emepveve~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
