#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"

#include "ppexnenmnmx04.contribution.set.hpp"
#include "ppexnenmnmx04.event.set.hpp"
#include "ppexnenmnmx04.phasespace.set.hpp"
#include "ppexnenmnmx04.observable.set.hpp"
#include "ppexnenmnmx04.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-epvevmvm~+X");

  MUC->csi = new ppexnenmnmx04_contribution_set();
  MUC->esi = new ppexnenmnmx04_event_set();
  MUC->psi = new ppexnenmnmx04_phasespace_set();
  MUC->osi = new ppexnenmnmx04_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllll04_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppexnenmnmx04_phasespace_set();
      MUC->psi->fake_psi->csi = new ppexnenmnmx04_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppexnenmnmx04_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
