#include "header.hpp"

#include "ppv01.amplitude.set.hpp"

#include "ppwx01.contribution.set.hpp"
#include "ppwx01.event.set.hpp"
#include "ppwx01.phasespace.set.hpp"
#include "ppwx01.observable.set.hpp"
#include "ppwx01.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-wp+X");

  MUC->csi = new ppwx01_contribution_set();
  MUC->esi = new ppwx01_event_set();
  MUC->psi = new ppwx01_phasespace_set();
  MUC->osi = new ppwx01_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppv01_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppwx01_phasespace_set();
      MUC->psi->fake_psi->csi = new ppwx01_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppwx01_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
