#include "header.hpp"

#include "ppwx01.phasespace.set.hpp"

ppwx01_phasespace_set::~ppwx01_phasespace_set(){
  static Logger logger("ppwx01_phasespace_set::~ppwx01_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::optimize_minv_born(){
  static Logger logger("ppwx01_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppwx01_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppwx01_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_010_udx_wp(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_010_usx_wp(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_010_ubx_wp(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_010_cdx_wp(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_010_csx_wp(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_010_cbx_wp(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppwx01_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_010_udx_wp(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_010_usx_wp(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_010_ubx_wp(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_010_cdx_wp(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_010_csx_wp(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_010_cbx_wp(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppwx01_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_010_udx_wp(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_010_usx_wp(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_010_ubx_wp(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_010_cdx_wp(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_010_csx_wp(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_010_cbx_wp(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::optimize_minv_real(){
  static Logger logger("ppwx01_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppwx01_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 12;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppwx01_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_110_gu_wpd(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_110_gu_wps(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_110_gu_wpb(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_110_gc_wpd(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_110_gc_wps(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_110_gc_wpb(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_110_gdx_wpux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_110_gdx_wpcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_110_gsx_wpux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_110_gsx_wpcx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_110_gbx_wpux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_110_gbx_wpcx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_110_udx_wpg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_110_usx_wpg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_110_ubx_wpg(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_110_cdx_wpg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_110_csx_wpg(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_110_cbx_wpg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppwx01_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_110_gu_wpd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_110_gu_wps(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_110_gu_wpb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_110_gc_wpd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_110_gc_wps(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_110_gc_wpb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_110_gdx_wpux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_110_gdx_wpcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_110_gsx_wpux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_110_gsx_wpcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_110_gbx_wpux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_110_gbx_wpcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_110_udx_wpg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_110_usx_wpg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_110_ubx_wpg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_110_cdx_wpg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_110_csx_wpg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_110_cbx_wpg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppwx01_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_110_gu_wpd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_110_gu_wps(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_110_gu_wpb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_110_gc_wpd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_110_gc_wps(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_110_gc_wpb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_110_gdx_wpux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_110_gdx_wpcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_110_gsx_wpux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_110_gsx_wpcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_110_gbx_wpux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_110_gbx_wpcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_110_udx_wpg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_110_usx_wpg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_110_ubx_wpg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_110_cdx_wpg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_110_csx_wpg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_110_cbx_wpg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppwx01_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppwx01_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==   0){n_channel = 60;}
    else if (csi->no_process_parton[x_a] ==   1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  11){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  13){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  14){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  16){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  17){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  18){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  19){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  26){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  27){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  32){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  33){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  34){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  35){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  36){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  37){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  38){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  43){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  44){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  49){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  51){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  52){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  56){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  58){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  59){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  61){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  62){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  63){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  64){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  65){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  66){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  69){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  71){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  72){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  73){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  74){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  77){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  78){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  79){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  80){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  83){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  84){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  86){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  87){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  88){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  89){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  90){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  91){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  92){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  93){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  94){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  95){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  97){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  98){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 100){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 103){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 105){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 106){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 108){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 110){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 112){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 113){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 114){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 116){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 117){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 118){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 120){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 121){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 123){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 126){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 128){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 130){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 131){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 135){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 136){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 139){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 140){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 141){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 142){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 143){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 144){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 147){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 148){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 152){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 155){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 156){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 159){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 161){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 162){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 163){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 164){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 167){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 169){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 170){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 172){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 173){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 174){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 176){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 177){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 179){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 181){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 183){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 184){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 185){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 187){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 190){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 192){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 193){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 195){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 196){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 197){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 198){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 199){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 200){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 201){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 202){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 203){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 204){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 206){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 207){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 210){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 211){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 212){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 213){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 216){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 221){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 222){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 227){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 228){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 229){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 230){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 231){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 232){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 233){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 234){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 235){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 236){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 241){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 242){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 244){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 245){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 246){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 248){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 251){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 252){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 253){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 254){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 256){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 258){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 259){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 260){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 262){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 263){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 267){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 269){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 272){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 273){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 276){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 277){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 278){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 279){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 281){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 282){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 286){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 288){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  33){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  49){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  62){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  64){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  69){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  71){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  72){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  73){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  74){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  77){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  78){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  79){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  80){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  83){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  84){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  86){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  87){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  88){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  89){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  90){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  91){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  92){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  93){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  94){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  95){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  97){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  98){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 100){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 103){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 105){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 106){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 108){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 110){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 112){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 113){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 114){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 116){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 117){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 118){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 120){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 121){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 123){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 126){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 128){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 130){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 131){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 135){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 136){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 139){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 140){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 141){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 142){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 143){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 144){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 147){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 148){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 152){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 155){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 156){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 159){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 161){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 162){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 163){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 164){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 167){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 169){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 170){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 172){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 173){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 174){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 176){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 177){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 179){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 181){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 183){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 184){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 185){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 187){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 190){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 192){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 193){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 195){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 196){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 197){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 198){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 199){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 200){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 201){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 202){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 203){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 204){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 206){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 207){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 210){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 211){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 212){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 213){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 216){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 221){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 222){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 227){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 228){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 229){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 230){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 231){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 232){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 233){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 234){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 235){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 236){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 241){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 242){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 244){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 245){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 246){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 248){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 251){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 252){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 253){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 254){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 256){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 258){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 259){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 260){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 262){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 263){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 267){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 269){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 272){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 273){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 276){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 277){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 278){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 279){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 281){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 282){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 286){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 288){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppwx01_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ax_psp_210_gg_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==   2){ax_psp_210_gg_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==   3){ax_psp_210_gg_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] ==   4){ax_psp_210_gg_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] ==   5){ax_psp_210_gg_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] ==   6){ax_psp_210_gg_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==   7){ax_psp_210_gu_wpgd(x_a);}
    else if (csi->no_process_parton[x_a] ==   8){ax_psp_210_gu_wpgs(x_a);}
    else if (csi->no_process_parton[x_a] ==   9){ax_psp_210_gu_wpgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  10){ax_psp_210_gc_wpgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  11){ax_psp_210_gc_wpgs(x_a);}
    else if (csi->no_process_parton[x_a] ==  12){ax_psp_210_gc_wpgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  13){ax_psp_210_gdx_wpgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  14){ax_psp_210_gdx_wpgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  15){ax_psp_210_gsx_wpgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  16){ax_psp_210_gsx_wpgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  17){ax_psp_210_gbx_wpgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  18){ax_psp_210_gbx_wpgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  19){ax_psp_210_du_wpdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  20){ax_psp_210_du_wpds(x_a);}
    else if (csi->no_process_parton[x_a] ==  21){ax_psp_210_du_wpdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  25){ax_psp_210_dc_wpdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  26){ax_psp_210_dc_wpds(x_a);}
    else if (csi->no_process_parton[x_a] ==  27){ax_psp_210_dc_wpdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  31){ax_psp_210_ddx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  32){ax_psp_210_ddx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  33){ax_psp_210_ddx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] ==  34){ax_psp_210_ddx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  35){ax_psp_210_ddx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] ==  36){ax_psp_210_ddx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  37){ax_psp_210_dsx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  38){ax_psp_210_dsx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  43){ax_psp_210_dbx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  44){ax_psp_210_dbx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  49){ax_psp_210_uu_wpdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  51){ax_psp_210_uu_wpus(x_a);}
    else if (csi->no_process_parton[x_a] ==  52){ax_psp_210_uu_wpub(x_a);}
    else if (csi->no_process_parton[x_a] ==  56){ax_psp_210_us_wpds(x_a);}
    else if (csi->no_process_parton[x_a] ==  58){ax_psp_210_us_wpss(x_a);}
    else if (csi->no_process_parton[x_a] ==  59){ax_psp_210_us_wpsb(x_a);}
    else if (csi->no_process_parton[x_a] ==  61){ax_psp_210_uc_wpdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  62){ax_psp_210_uc_wpdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  63){ax_psp_210_uc_wpus(x_a);}
    else if (csi->no_process_parton[x_a] ==  64){ax_psp_210_uc_wpub(x_a);}
    else if (csi->no_process_parton[x_a] ==  65){ax_psp_210_uc_wpsc(x_a);}
    else if (csi->no_process_parton[x_a] ==  66){ax_psp_210_uc_wpcb(x_a);}
    else if (csi->no_process_parton[x_a] ==  69){ax_psp_210_ub_wpdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  71){ax_psp_210_ub_wpsb(x_a);}
    else if (csi->no_process_parton[x_a] ==  72){ax_psp_210_ub_wpbb(x_a);}
    else if (csi->no_process_parton[x_a] ==  73){ax_psp_210_udx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  74){ax_psp_210_udx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  77){ax_psp_210_udx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  78){ax_psp_210_udx_wpucx(x_a);}
    else if (csi->no_process_parton[x_a] ==  79){ax_psp_210_udx_wpsdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  80){ax_psp_210_udx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  83){ax_psp_210_udx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  84){ax_psp_210_udx_wpbdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  86){ax_psp_210_udx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  87){ax_psp_210_uux_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  88){ax_psp_210_uux_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  89){ax_psp_210_uux_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] ==  90){ax_psp_210_uux_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  91){ax_psp_210_uux_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] ==  92){ax_psp_210_uux_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  93){ax_psp_210_usx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  94){ax_psp_210_usx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  95){ax_psp_210_usx_wpdsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  97){ax_psp_210_usx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  98){ax_psp_210_usx_wpucx(x_a);}
    else if (csi->no_process_parton[x_a] == 100){ax_psp_210_usx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 103){ax_psp_210_usx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 105){ax_psp_210_usx_wpbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 106){ax_psp_210_usx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 108){ax_psp_210_ucx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 110){ax_psp_210_ucx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 112){ax_psp_210_ucx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 113){ax_psp_210_ubx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] == 114){ax_psp_210_ubx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] == 116){ax_psp_210_ubx_wpdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 117){ax_psp_210_ubx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] == 118){ax_psp_210_ubx_wpucx(x_a);}
    else if (csi->no_process_parton[x_a] == 120){ax_psp_210_ubx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 121){ax_psp_210_ubx_wpsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 123){ax_psp_210_ubx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 126){ax_psp_210_ubx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 128){ax_psp_210_sc_wpds(x_a);}
    else if (csi->no_process_parton[x_a] == 130){ax_psp_210_sc_wpss(x_a);}
    else if (csi->no_process_parton[x_a] == 131){ax_psp_210_sc_wpsb(x_a);}
    else if (csi->no_process_parton[x_a] == 135){ax_psp_210_sdx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 136){ax_psp_210_sdx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 139){ax_psp_210_ssx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] == 140){ax_psp_210_ssx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 141){ax_psp_210_ssx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 142){ax_psp_210_ssx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 143){ax_psp_210_ssx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 144){ax_psp_210_ssx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 147){ax_psp_210_sbx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 148){ax_psp_210_sbx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 152){ax_psp_210_cc_wpdc(x_a);}
    else if (csi->no_process_parton[x_a] == 155){ax_psp_210_cc_wpsc(x_a);}
    else if (csi->no_process_parton[x_a] == 156){ax_psp_210_cc_wpcb(x_a);}
    else if (csi->no_process_parton[x_a] == 159){ax_psp_210_cb_wpdb(x_a);}
    else if (csi->no_process_parton[x_a] == 161){ax_psp_210_cb_wpsb(x_a);}
    else if (csi->no_process_parton[x_a] == 162){ax_psp_210_cb_wpbb(x_a);}
    else if (csi->no_process_parton[x_a] == 163){ax_psp_210_cdx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] == 164){ax_psp_210_cdx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] == 167){ax_psp_210_cdx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] == 169){ax_psp_210_cdx_wpsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 170){ax_psp_210_cdx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 172){ax_psp_210_cdx_wpcux(x_a);}
    else if (csi->no_process_parton[x_a] == 173){ax_psp_210_cdx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 174){ax_psp_210_cdx_wpbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 176){ax_psp_210_cdx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 177){ax_psp_210_cux_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] == 179){ax_psp_210_cux_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 181){ax_psp_210_cux_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 183){ax_psp_210_csx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] == 184){ax_psp_210_csx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] == 185){ax_psp_210_csx_wpdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 187){ax_psp_210_csx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] == 190){ax_psp_210_csx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 192){ax_psp_210_csx_wpcux(x_a);}
    else if (csi->no_process_parton[x_a] == 193){ax_psp_210_csx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 195){ax_psp_210_csx_wpbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 196){ax_psp_210_csx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 197){ax_psp_210_ccx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] == 198){ax_psp_210_ccx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 199){ax_psp_210_ccx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 200){ax_psp_210_ccx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 201){ax_psp_210_ccx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 202){ax_psp_210_ccx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 203){ax_psp_210_cbx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] == 204){ax_psp_210_cbx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] == 206){ax_psp_210_cbx_wpdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 207){ax_psp_210_cbx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] == 210){ax_psp_210_cbx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 211){ax_psp_210_cbx_wpsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 212){ax_psp_210_cbx_wpcux(x_a);}
    else if (csi->no_process_parton[x_a] == 213){ax_psp_210_cbx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 216){ax_psp_210_cbx_wpbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 221){ax_psp_210_bdx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 222){ax_psp_210_bdx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 227){ax_psp_210_bsx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 228){ax_psp_210_bsx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 229){ax_psp_210_bbx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] == 230){ax_psp_210_bbx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 231){ax_psp_210_bbx_wpsux(x_a);}
    else if (csi->no_process_parton[x_a] == 232){ax_psp_210_bbx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 233){ax_psp_210_bbx_wpbux(x_a);}
    else if (csi->no_process_parton[x_a] == 234){ax_psp_210_bbx_wpbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 235){ax_psp_210_dxdx_wpdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 236){ax_psp_210_dxdx_wpdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 241){ax_psp_210_dxux_wpuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 242){ax_psp_210_dxux_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 244){ax_psp_210_dxsx_wpdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 245){ax_psp_210_dxsx_wpdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 246){ax_psp_210_dxsx_wpuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 248){ax_psp_210_dxsx_wpsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 251){ax_psp_210_dxcx_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 252){ax_psp_210_dxcx_wpcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 253){ax_psp_210_dxbx_wpdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 254){ax_psp_210_dxbx_wpdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 256){ax_psp_210_dxbx_wpuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 258){ax_psp_210_dxbx_wpcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 259){ax_psp_210_uxsx_wpuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 260){ax_psp_210_uxsx_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 262){ax_psp_210_uxbx_wpuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 263){ax_psp_210_uxbx_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 267){ax_psp_210_sxsx_wpuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 269){ax_psp_210_sxsx_wpsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 272){ax_psp_210_sxcx_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 273){ax_psp_210_sxcx_wpcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 276){ax_psp_210_sxbx_wpuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 277){ax_psp_210_sxbx_wpuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 278){ax_psp_210_sxbx_wpsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 279){ax_psp_210_sxbx_wpcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 281){ax_psp_210_cxbx_wpuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 282){ax_psp_210_cxbx_wpcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 286){ax_psp_210_bxbx_wpuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 288){ax_psp_210_bxbx_wpcxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppwx01_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ac_psp_210_gg_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   2){ac_psp_210_gg_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   3){ac_psp_210_gg_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   4){ac_psp_210_gg_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   5){ac_psp_210_gg_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   6){ac_psp_210_gg_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   7){ac_psp_210_gu_wpgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   8){ac_psp_210_gu_wpgs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   9){ac_psp_210_gu_wpgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  10){ac_psp_210_gc_wpgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  11){ac_psp_210_gc_wpgs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  12){ac_psp_210_gc_wpgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  13){ac_psp_210_gdx_wpgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  14){ac_psp_210_gdx_wpgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  15){ac_psp_210_gsx_wpgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  16){ac_psp_210_gsx_wpgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  17){ac_psp_210_gbx_wpgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  18){ac_psp_210_gbx_wpgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  19){ac_psp_210_du_wpdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  20){ac_psp_210_du_wpds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  21){ac_psp_210_du_wpdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  25){ac_psp_210_dc_wpdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  26){ac_psp_210_dc_wpds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  27){ac_psp_210_dc_wpdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  31){ac_psp_210_ddx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  32){ac_psp_210_ddx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  33){ac_psp_210_ddx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  34){ac_psp_210_ddx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  35){ac_psp_210_ddx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  36){ac_psp_210_ddx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  37){ac_psp_210_dsx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  38){ac_psp_210_dsx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  43){ac_psp_210_dbx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  44){ac_psp_210_dbx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  49){ac_psp_210_uu_wpdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  51){ac_psp_210_uu_wpus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  52){ac_psp_210_uu_wpub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  56){ac_psp_210_us_wpds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  58){ac_psp_210_us_wpss(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  59){ac_psp_210_us_wpsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  61){ac_psp_210_uc_wpdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  62){ac_psp_210_uc_wpdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  63){ac_psp_210_uc_wpus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  64){ac_psp_210_uc_wpub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  65){ac_psp_210_uc_wpsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  66){ac_psp_210_uc_wpcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  69){ac_psp_210_ub_wpdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  71){ac_psp_210_ub_wpsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  72){ac_psp_210_ub_wpbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  73){ac_psp_210_udx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  74){ac_psp_210_udx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  77){ac_psp_210_udx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  78){ac_psp_210_udx_wpucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  79){ac_psp_210_udx_wpsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  80){ac_psp_210_udx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  83){ac_psp_210_udx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  84){ac_psp_210_udx_wpbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  86){ac_psp_210_udx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  87){ac_psp_210_uux_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  88){ac_psp_210_uux_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  89){ac_psp_210_uux_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  90){ac_psp_210_uux_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  91){ac_psp_210_uux_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  92){ac_psp_210_uux_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  93){ac_psp_210_usx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  94){ac_psp_210_usx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  95){ac_psp_210_usx_wpdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  97){ac_psp_210_usx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  98){ac_psp_210_usx_wpucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 100){ac_psp_210_usx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 103){ac_psp_210_usx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 105){ac_psp_210_usx_wpbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 106){ac_psp_210_usx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 108){ac_psp_210_ucx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 110){ac_psp_210_ucx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 112){ac_psp_210_ucx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 113){ac_psp_210_ubx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 114){ac_psp_210_ubx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 116){ac_psp_210_ubx_wpdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 117){ac_psp_210_ubx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 118){ac_psp_210_ubx_wpucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 120){ac_psp_210_ubx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 121){ac_psp_210_ubx_wpsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 123){ac_psp_210_ubx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 126){ac_psp_210_ubx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 128){ac_psp_210_sc_wpds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 130){ac_psp_210_sc_wpss(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 131){ac_psp_210_sc_wpsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 135){ac_psp_210_sdx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 136){ac_psp_210_sdx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 139){ac_psp_210_ssx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 140){ac_psp_210_ssx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 141){ac_psp_210_ssx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 142){ac_psp_210_ssx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 143){ac_psp_210_ssx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 144){ac_psp_210_ssx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 147){ac_psp_210_sbx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 148){ac_psp_210_sbx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 152){ac_psp_210_cc_wpdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 155){ac_psp_210_cc_wpsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 156){ac_psp_210_cc_wpcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 159){ac_psp_210_cb_wpdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 161){ac_psp_210_cb_wpsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 162){ac_psp_210_cb_wpbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 163){ac_psp_210_cdx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 164){ac_psp_210_cdx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 167){ac_psp_210_cdx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 169){ac_psp_210_cdx_wpsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 170){ac_psp_210_cdx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 172){ac_psp_210_cdx_wpcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 173){ac_psp_210_cdx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 174){ac_psp_210_cdx_wpbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 176){ac_psp_210_cdx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 177){ac_psp_210_cux_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 179){ac_psp_210_cux_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 181){ac_psp_210_cux_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 183){ac_psp_210_csx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 184){ac_psp_210_csx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 185){ac_psp_210_csx_wpdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 187){ac_psp_210_csx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 190){ac_psp_210_csx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 192){ac_psp_210_csx_wpcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 193){ac_psp_210_csx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 195){ac_psp_210_csx_wpbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 196){ac_psp_210_csx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 197){ac_psp_210_ccx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 198){ac_psp_210_ccx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 199){ac_psp_210_ccx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 200){ac_psp_210_ccx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 201){ac_psp_210_ccx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 202){ac_psp_210_ccx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 203){ac_psp_210_cbx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 204){ac_psp_210_cbx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 206){ac_psp_210_cbx_wpdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 207){ac_psp_210_cbx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 210){ac_psp_210_cbx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 211){ac_psp_210_cbx_wpsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 212){ac_psp_210_cbx_wpcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 213){ac_psp_210_cbx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 216){ac_psp_210_cbx_wpbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 221){ac_psp_210_bdx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 222){ac_psp_210_bdx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 227){ac_psp_210_bsx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 228){ac_psp_210_bsx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 229){ac_psp_210_bbx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 230){ac_psp_210_bbx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 231){ac_psp_210_bbx_wpsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 232){ac_psp_210_bbx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 233){ac_psp_210_bbx_wpbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 234){ac_psp_210_bbx_wpbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 235){ac_psp_210_dxdx_wpdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 236){ac_psp_210_dxdx_wpdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 241){ac_psp_210_dxux_wpuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 242){ac_psp_210_dxux_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 244){ac_psp_210_dxsx_wpdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 245){ac_psp_210_dxsx_wpdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 246){ac_psp_210_dxsx_wpuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 248){ac_psp_210_dxsx_wpsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 251){ac_psp_210_dxcx_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 252){ac_psp_210_dxcx_wpcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 253){ac_psp_210_dxbx_wpdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 254){ac_psp_210_dxbx_wpdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 256){ac_psp_210_dxbx_wpuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 258){ac_psp_210_dxbx_wpcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 259){ac_psp_210_uxsx_wpuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 260){ac_psp_210_uxsx_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 262){ac_psp_210_uxbx_wpuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 263){ac_psp_210_uxbx_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 267){ac_psp_210_sxsx_wpuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 269){ac_psp_210_sxsx_wpsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 272){ac_psp_210_sxcx_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 273){ac_psp_210_sxcx_wpcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 276){ac_psp_210_sxbx_wpuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 277){ac_psp_210_sxbx_wpuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 278){ac_psp_210_sxbx_wpsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 279){ac_psp_210_sxbx_wpcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 281){ac_psp_210_cxbx_wpuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 282){ac_psp_210_cxbx_wpcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 286){ac_psp_210_bxbx_wpuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 288){ac_psp_210_bxbx_wpcxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppwx01_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ag_psp_210_gg_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   2){ag_psp_210_gg_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   3){ag_psp_210_gg_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   4){ag_psp_210_gg_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   5){ag_psp_210_gg_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   6){ag_psp_210_gg_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   7){ag_psp_210_gu_wpgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   8){ag_psp_210_gu_wpgs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   9){ag_psp_210_gu_wpgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  10){ag_psp_210_gc_wpgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  11){ag_psp_210_gc_wpgs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  12){ag_psp_210_gc_wpgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  13){ag_psp_210_gdx_wpgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  14){ag_psp_210_gdx_wpgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  15){ag_psp_210_gsx_wpgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  16){ag_psp_210_gsx_wpgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  17){ag_psp_210_gbx_wpgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  18){ag_psp_210_gbx_wpgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  19){ag_psp_210_du_wpdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  20){ag_psp_210_du_wpds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  21){ag_psp_210_du_wpdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  25){ag_psp_210_dc_wpdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  26){ag_psp_210_dc_wpds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  27){ag_psp_210_dc_wpdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  31){ag_psp_210_ddx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  32){ag_psp_210_ddx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  33){ag_psp_210_ddx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  34){ag_psp_210_ddx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  35){ag_psp_210_ddx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  36){ag_psp_210_ddx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  37){ag_psp_210_dsx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  38){ag_psp_210_dsx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  43){ag_psp_210_dbx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  44){ag_psp_210_dbx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  49){ag_psp_210_uu_wpdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  51){ag_psp_210_uu_wpus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  52){ag_psp_210_uu_wpub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  56){ag_psp_210_us_wpds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  58){ag_psp_210_us_wpss(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  59){ag_psp_210_us_wpsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  61){ag_psp_210_uc_wpdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  62){ag_psp_210_uc_wpdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  63){ag_psp_210_uc_wpus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  64){ag_psp_210_uc_wpub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  65){ag_psp_210_uc_wpsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  66){ag_psp_210_uc_wpcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  69){ag_psp_210_ub_wpdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  71){ag_psp_210_ub_wpsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  72){ag_psp_210_ub_wpbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  73){ag_psp_210_udx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  74){ag_psp_210_udx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  77){ag_psp_210_udx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  78){ag_psp_210_udx_wpucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  79){ag_psp_210_udx_wpsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  80){ag_psp_210_udx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  83){ag_psp_210_udx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  84){ag_psp_210_udx_wpbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  86){ag_psp_210_udx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  87){ag_psp_210_uux_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  88){ag_psp_210_uux_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  89){ag_psp_210_uux_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  90){ag_psp_210_uux_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  91){ag_psp_210_uux_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  92){ag_psp_210_uux_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  93){ag_psp_210_usx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  94){ag_psp_210_usx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  95){ag_psp_210_usx_wpdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  97){ag_psp_210_usx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  98){ag_psp_210_usx_wpucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 100){ag_psp_210_usx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 103){ag_psp_210_usx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 105){ag_psp_210_usx_wpbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 106){ag_psp_210_usx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 108){ag_psp_210_ucx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 110){ag_psp_210_ucx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 112){ag_psp_210_ucx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 113){ag_psp_210_ubx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 114){ag_psp_210_ubx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 116){ag_psp_210_ubx_wpdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 117){ag_psp_210_ubx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 118){ag_psp_210_ubx_wpucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 120){ag_psp_210_ubx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 121){ag_psp_210_ubx_wpsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 123){ag_psp_210_ubx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 126){ag_psp_210_ubx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 128){ag_psp_210_sc_wpds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 130){ag_psp_210_sc_wpss(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 131){ag_psp_210_sc_wpsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 135){ag_psp_210_sdx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 136){ag_psp_210_sdx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 139){ag_psp_210_ssx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 140){ag_psp_210_ssx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 141){ag_psp_210_ssx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 142){ag_psp_210_ssx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 143){ag_psp_210_ssx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 144){ag_psp_210_ssx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 147){ag_psp_210_sbx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 148){ag_psp_210_sbx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 152){ag_psp_210_cc_wpdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 155){ag_psp_210_cc_wpsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 156){ag_psp_210_cc_wpcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 159){ag_psp_210_cb_wpdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 161){ag_psp_210_cb_wpsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 162){ag_psp_210_cb_wpbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 163){ag_psp_210_cdx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 164){ag_psp_210_cdx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 167){ag_psp_210_cdx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 169){ag_psp_210_cdx_wpsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 170){ag_psp_210_cdx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 172){ag_psp_210_cdx_wpcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 173){ag_psp_210_cdx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 174){ag_psp_210_cdx_wpbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 176){ag_psp_210_cdx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 177){ag_psp_210_cux_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 179){ag_psp_210_cux_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 181){ag_psp_210_cux_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 183){ag_psp_210_csx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 184){ag_psp_210_csx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 185){ag_psp_210_csx_wpdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 187){ag_psp_210_csx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 190){ag_psp_210_csx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 192){ag_psp_210_csx_wpcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 193){ag_psp_210_csx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 195){ag_psp_210_csx_wpbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 196){ag_psp_210_csx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 197){ag_psp_210_ccx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 198){ag_psp_210_ccx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 199){ag_psp_210_ccx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 200){ag_psp_210_ccx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 201){ag_psp_210_ccx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 202){ag_psp_210_ccx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 203){ag_psp_210_cbx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 204){ag_psp_210_cbx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 206){ag_psp_210_cbx_wpdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 207){ag_psp_210_cbx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 210){ag_psp_210_cbx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 211){ag_psp_210_cbx_wpsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 212){ag_psp_210_cbx_wpcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 213){ag_psp_210_cbx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 216){ag_psp_210_cbx_wpbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 221){ag_psp_210_bdx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 222){ag_psp_210_bdx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 227){ag_psp_210_bsx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 228){ag_psp_210_bsx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 229){ag_psp_210_bbx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 230){ag_psp_210_bbx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 231){ag_psp_210_bbx_wpsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 232){ag_psp_210_bbx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 233){ag_psp_210_bbx_wpbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 234){ag_psp_210_bbx_wpbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 235){ag_psp_210_dxdx_wpdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 236){ag_psp_210_dxdx_wpdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 241){ag_psp_210_dxux_wpuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 242){ag_psp_210_dxux_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 244){ag_psp_210_dxsx_wpdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 245){ag_psp_210_dxsx_wpdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 246){ag_psp_210_dxsx_wpuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 248){ag_psp_210_dxsx_wpsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 251){ag_psp_210_dxcx_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 252){ag_psp_210_dxcx_wpcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 253){ag_psp_210_dxbx_wpdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 254){ag_psp_210_dxbx_wpdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 256){ag_psp_210_dxbx_wpuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 258){ag_psp_210_dxbx_wpcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 259){ag_psp_210_uxsx_wpuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 260){ag_psp_210_uxsx_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 262){ag_psp_210_uxbx_wpuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 263){ag_psp_210_uxbx_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 267){ag_psp_210_sxsx_wpuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 269){ag_psp_210_sxsx_wpsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 272){ag_psp_210_sxcx_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 273){ag_psp_210_sxcx_wpcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 276){ag_psp_210_sxbx_wpuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 277){ag_psp_210_sxbx_wpuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 278){ag_psp_210_sxbx_wpsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 279){ag_psp_210_sxbx_wpcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 281){ag_psp_210_cxbx_wpuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 282){ag_psp_210_cxbx_wpcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 286){ag_psp_210_bxbx_wpuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 288){ag_psp_210_bxbx_wpcxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
