{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = PARTICLE("w")[0].m;
    // takes invariant mass of hardest W boson PARTICLE("wm")[0]
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = PARTICLE("w")[0].m;
    double pT = PARTICLE("w")[0].pT;
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m and transverse momentum (***).pT
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
