#include "header.hpp"

#include "ppwx01.summary.hpp"

ppwx01_summary_generic::ppwx01_summary_generic(munich * xmunich){
  Logger logger("ppwx01_summary_generic::ppwx01_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppwx01_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppwx01_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppwx01_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppwx01_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppwx01_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppwx01_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_born(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_wp";
    subprocess[2] = "us~_wp";
    subprocess[3] = "ub~_wp";
    subprocess[4] = "cd~_wp";
    subprocess[5] = "cs~_wp";
    subprocess[6] = "cb~_wp";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_wp";
    subprocess[2] = "us~_wp";
    subprocess[3] = "ub~_wp";
    subprocess[4] = "cd~_wp";
    subprocess[5] = "cs~_wp";
    subprocess[6] = "cb~_wp";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_wp";
    subprocess[2] = "us~_wp";
    subprocess[3] = "ub~_wp";
    subprocess[4] = "cd~_wp";
    subprocess[5] = "cs~_wp";
    subprocess[6] = "cb~_wp";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_wp";
    subprocess[2] = "us~_wp";
    subprocess[3] = "ub~_wp";
    subprocess[4] = "cd~_wp";
    subprocess[5] = "cs~_wp";
    subprocess[6] = "cb~_wp";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_wp";
    subprocess[2] = "us~_wp";
    subprocess[3] = "ub~_wp";
    subprocess[4] = "cd~_wp";
    subprocess[5] = "cs~_wp";
    subprocess[6] = "cb~_wp";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gu_wps";
    subprocess[3] = "gu_wpb";
    subprocess[4] = "gc_wpd";
    subprocess[5] = "gc_wps";
    subprocess[6] = "gc_wpb";
    subprocess[7] = "gd~_wpu~";
    subprocess[8] = "gd~_wpc~";
    subprocess[9] = "gs~_wpu~";
    subprocess[10] = "gs~_wpc~";
    subprocess[11] = "gb~_wpu~";
    subprocess[12] = "gb~_wpc~";
    subprocess[13] = "ud~_wpg";
    subprocess[14] = "us~_wpg";
    subprocess[15] = "ub~_wpg";
    subprocess[16] = "cd~_wpg";
    subprocess[17] = "cs~_wpg";
    subprocess[18] = "cb~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gu_wps";
    subprocess[3] = "gu_wpb";
    subprocess[4] = "gc_wpd";
    subprocess[5] = "gc_wps";
    subprocess[6] = "gc_wpb";
    subprocess[7] = "gd~_wpu~";
    subprocess[8] = "gd~_wpc~";
    subprocess[9] = "gs~_wpu~";
    subprocess[10] = "gs~_wpc~";
    subprocess[11] = "gb~_wpu~";
    subprocess[12] = "gb~_wpc~";
    subprocess[13] = "ud~_wpg";
    subprocess[14] = "us~_wpg";
    subprocess[15] = "ub~_wpg";
    subprocess[16] = "cd~_wpg";
    subprocess[17] = "cs~_wpg";
    subprocess[18] = "cb~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gu_wps";
    subprocess[3] = "gu_wpb";
    subprocess[4] = "gc_wpd";
    subprocess[5] = "gc_wps";
    subprocess[6] = "gc_wpb";
    subprocess[7] = "gd~_wpu~";
    subprocess[8] = "gd~_wpc~";
    subprocess[9] = "gs~_wpu~";
    subprocess[10] = "gs~_wpc~";
    subprocess[11] = "gb~_wpu~";
    subprocess[12] = "gb~_wpc~";
    subprocess[13] = "ud~_wpg";
    subprocess[14] = "us~_wpg";
    subprocess[15] = "ub~_wpg";
    subprocess[16] = "cd~_wpg";
    subprocess[17] = "cs~_wpg";
    subprocess[18] = "cb~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppwx01_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_wpdu~";
    subprocess[2] = "gg_wpdc~";
    subprocess[3] = "gg_wpsu~";
    subprocess[4] = "gg_wpsc~";
    subprocess[5] = "gg_wpbu~";
    subprocess[6] = "gg_wpbc~";
    subprocess[7] = "gu_wpgd";
    subprocess[8] = "gu_wpgs";
    subprocess[9] = "gu_wpgb";
    subprocess[10] = "gc_wpgd";
    subprocess[11] = "gc_wpgs";
    subprocess[12] = "gc_wpgb";
    subprocess[13] = "gd~_wpgu~";
    subprocess[14] = "gd~_wpgc~";
    subprocess[15] = "gs~_wpgu~";
    subprocess[16] = "gs~_wpgc~";
    subprocess[17] = "gb~_wpgu~";
    subprocess[18] = "gb~_wpgc~";
    subprocess[19] = "du_wpdd";
    subprocess[20] = "du_wpds";
    subprocess[21] = "du_wpdb";
    subprocess[22] = "dc_wpdd";
    subprocess[23] = "dc_wpds";
    subprocess[24] = "dc_wpdb";
    subprocess[25] = "dd~_wpdu~";
    subprocess[26] = "dd~_wpdc~";
    subprocess[27] = "dd~_wpsu~";
    subprocess[28] = "dd~_wpsc~";
    subprocess[29] = "dd~_wpbu~";
    subprocess[30] = "dd~_wpbc~";
    subprocess[31] = "ds~_wpdu~";
    subprocess[32] = "ds~_wpdc~";
    subprocess[33] = "db~_wpdu~";
    subprocess[34] = "db~_wpdc~";
    subprocess[35] = "uu_wpdu";
    subprocess[36] = "uu_wpus";
    subprocess[37] = "uu_wpub";
    subprocess[38] = "us_wpds";
    subprocess[39] = "us_wpss";
    subprocess[40] = "us_wpsb";
    subprocess[41] = "uc_wpdu";
    subprocess[42] = "uc_wpdc";
    subprocess[43] = "uc_wpus";
    subprocess[44] = "uc_wpub";
    subprocess[45] = "uc_wpsc";
    subprocess[46] = "uc_wpcb";
    subprocess[47] = "ub_wpdb";
    subprocess[48] = "ub_wpsb";
    subprocess[49] = "ub_wpbb";
    subprocess[50] = "ud~_wpgg";
    subprocess[51] = "ud~_wpdd~";
    subprocess[52] = "ud~_wpuu~";
    subprocess[53] = "ud~_wpuc~";
    subprocess[54] = "ud~_wpsd~";
    subprocess[55] = "ud~_wpss~";
    subprocess[56] = "ud~_wpcc~";
    subprocess[57] = "ud~_wpbd~";
    subprocess[58] = "ud~_wpbb~";
    subprocess[59] = "uu~_wpdu~";
    subprocess[60] = "uu~_wpdc~";
    subprocess[61] = "uu~_wpsu~";
    subprocess[62] = "uu~_wpsc~";
    subprocess[63] = "uu~_wpbu~";
    subprocess[64] = "uu~_wpbc~";
    subprocess[65] = "us~_wpgg";
    subprocess[66] = "us~_wpdd~";
    subprocess[67] = "us~_wpds~";
    subprocess[68] = "us~_wpuu~";
    subprocess[69] = "us~_wpuc~";
    subprocess[70] = "us~_wpss~";
    subprocess[71] = "us~_wpcc~";
    subprocess[72] = "us~_wpbs~";
    subprocess[73] = "us~_wpbb~";
    subprocess[74] = "uc~_wpdc~";
    subprocess[75] = "uc~_wpsc~";
    subprocess[76] = "uc~_wpbc~";
    subprocess[77] = "ub~_wpgg";
    subprocess[78] = "ub~_wpdd~";
    subprocess[79] = "ub~_wpdb~";
    subprocess[80] = "ub~_wpuu~";
    subprocess[81] = "ub~_wpuc~";
    subprocess[82] = "ub~_wpss~";
    subprocess[83] = "ub~_wpsb~";
    subprocess[84] = "ub~_wpcc~";
    subprocess[85] = "ub~_wpbb~";
    subprocess[86] = "sc_wpds";
    subprocess[87] = "sc_wpss";
    subprocess[88] = "sc_wpsb";
    subprocess[89] = "sd~_wpsu~";
    subprocess[90] = "sd~_wpsc~";
    subprocess[91] = "ss~_wpdu~";
    subprocess[92] = "ss~_wpdc~";
    subprocess[93] = "ss~_wpsu~";
    subprocess[94] = "ss~_wpsc~";
    subprocess[95] = "ss~_wpbu~";
    subprocess[96] = "ss~_wpbc~";
    subprocess[97] = "sb~_wpsu~";
    subprocess[98] = "sb~_wpsc~";
    subprocess[99] = "cc_wpdc";
    subprocess[100] = "cc_wpsc";
    subprocess[101] = "cc_wpcb";
    subprocess[102] = "cb_wpdb";
    subprocess[103] = "cb_wpsb";
    subprocess[104] = "cb_wpbb";
    subprocess[105] = "cd~_wpgg";
    subprocess[106] = "cd~_wpdd~";
    subprocess[107] = "cd~_wpuu~";
    subprocess[108] = "cd~_wpsd~";
    subprocess[109] = "cd~_wpss~";
    subprocess[110] = "cd~_wpcu~";
    subprocess[111] = "cd~_wpcc~";
    subprocess[112] = "cd~_wpbd~";
    subprocess[113] = "cd~_wpbb~";
    subprocess[114] = "cu~_wpdu~";
    subprocess[115] = "cu~_wpsu~";
    subprocess[116] = "cu~_wpbu~";
    subprocess[117] = "cs~_wpgg";
    subprocess[118] = "cs~_wpdd~";
    subprocess[119] = "cs~_wpds~";
    subprocess[120] = "cs~_wpuu~";
    subprocess[121] = "cs~_wpss~";
    subprocess[122] = "cs~_wpcu~";
    subprocess[123] = "cs~_wpcc~";
    subprocess[124] = "cs~_wpbs~";
    subprocess[125] = "cs~_wpbb~";
    subprocess[126] = "cc~_wpdu~";
    subprocess[127] = "cc~_wpdc~";
    subprocess[128] = "cc~_wpsu~";
    subprocess[129] = "cc~_wpsc~";
    subprocess[130] = "cc~_wpbu~";
    subprocess[131] = "cc~_wpbc~";
    subprocess[132] = "cb~_wpgg";
    subprocess[133] = "cb~_wpdd~";
    subprocess[134] = "cb~_wpdb~";
    subprocess[135] = "cb~_wpuu~";
    subprocess[136] = "cb~_wpss~";
    subprocess[137] = "cb~_wpsb~";
    subprocess[138] = "cb~_wpcu~";
    subprocess[139] = "cb~_wpcc~";
    subprocess[140] = "cb~_wpbb~";
    subprocess[141] = "bd~_wpbu~";
    subprocess[142] = "bd~_wpbc~";
    subprocess[143] = "bs~_wpbu~";
    subprocess[144] = "bs~_wpbc~";
    subprocess[145] = "bb~_wpdu~";
    subprocess[146] = "bb~_wpdc~";
    subprocess[147] = "bb~_wpsu~";
    subprocess[148] = "bb~_wpsc~";
    subprocess[149] = "bb~_wpbu~";
    subprocess[150] = "bb~_wpbc~";
    subprocess[151] = "d~d~_wpd~u~";
    subprocess[152] = "d~d~_wpd~c~";
    subprocess[153] = "d~u~_wpu~u~";
    subprocess[154] = "d~u~_wpu~c~";
    subprocess[155] = "d~s~_wpd~u~";
    subprocess[156] = "d~s~_wpd~c~";
    subprocess[157] = "d~s~_wpu~s~";
    subprocess[158] = "d~s~_wps~c~";
    subprocess[159] = "d~c~_wpu~c~";
    subprocess[160] = "d~c~_wpc~c~";
    subprocess[161] = "d~b~_wpd~u~";
    subprocess[162] = "d~b~_wpd~c~";
    subprocess[163] = "d~b~_wpu~b~";
    subprocess[164] = "d~b~_wpc~b~";
    subprocess[165] = "u~s~_wpu~u~";
    subprocess[166] = "u~s~_wpu~c~";
    subprocess[167] = "u~b~_wpu~u~";
    subprocess[168] = "u~b~_wpu~c~";
    subprocess[169] = "s~s~_wpu~s~";
    subprocess[170] = "s~s~_wps~c~";
    subprocess[171] = "s~c~_wpu~c~";
    subprocess[172] = "s~c~_wpc~c~";
    subprocess[173] = "s~b~_wpu~s~";
    subprocess[174] = "s~b~_wpu~b~";
    subprocess[175] = "s~b~_wps~c~";
    subprocess[176] = "s~b~_wpc~b~";
    subprocess[177] = "c~b~_wpu~c~";
    subprocess[178] = "c~b~_wpc~c~";
    subprocess[179] = "b~b~_wpu~b~";
    subprocess[180] = "b~b~_wpc~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
