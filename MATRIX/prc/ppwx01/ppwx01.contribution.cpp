#include "header.hpp"

#include "ppwx01.contribution.set.hpp"

ppwx01_contribution_set::~ppwx01_contribution_set(){
  static Logger logger("ppwx01_contribution_set::~ppwx01_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppwx01_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppwx01_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppwx01_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   5)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 18; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppwx01_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  d    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  d    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  s    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  b    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  b    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  d    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  d    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  s    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  b    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  b    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  ux   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> wx  ux   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  cx   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> wx  cx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> wx  ux   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> wx  ux   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> wx  cx   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> wx  cx   //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  ux   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  ux   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  cx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  cx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  g    //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  g    //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  g    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  g    //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  g    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  g    //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  g    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  g    //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  g    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  g    //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  g    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppwx01_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 33; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 49; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 64; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 69; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 71; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 72; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 73; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 74; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 77; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 78; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 79; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 80; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 83; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 84; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 86; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 87; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 88; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 89; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 90; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 91; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 92; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 93; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 94; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 95; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 97; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 98; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 100; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 103; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 105; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 106; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 108; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 110; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 112; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 113; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 114; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 116; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 117; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 118; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 120; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 121; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 123; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 126; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 128; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 130; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 131; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 135; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 136; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 139; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 140; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 141; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 142; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 143; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 144; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 147; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 148; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 152; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 155; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 156; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 159; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 161; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 162; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 163; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 164; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 167; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 169; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 170; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 172; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 173; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 174; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 176; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 177; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 179; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 181; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 183; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 184; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 185; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 187; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 190; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 192; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 193; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 195; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 196; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 197; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 198; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 199; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 200; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 201; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 202; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 203; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 204; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 206; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 207; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 210; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 211; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 212; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 213; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 216; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 221; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 222; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 227; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 228; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 229; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 230; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 231; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 232; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 233; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 234; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 235; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 236; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 241; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 242; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 244; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 245; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 246; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 248; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 251; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 252; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 253; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 254; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 256; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 258; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 259; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 260; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 262; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 263; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 267; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 269; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 272; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 273; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 276; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 277; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 278; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 279; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 281; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 282; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 286; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 288; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 33){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 49){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 64){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 71){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 72){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 73){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 74){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 77){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 78){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 79){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 80){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 83){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 84){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 86){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 87){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 88){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 89){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 90){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 91){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 92){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 93){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 94){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 95){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 97){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 98){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 100){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 103){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 105){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 106){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 108){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 110){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 112){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 113){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 114){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 116){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 117){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 118){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 120){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 121){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 123){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 126){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 128){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 130){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 131){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 135){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 136){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 139){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 140){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 141){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 142){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 143){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 144){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 147){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 148){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 152){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 155){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 156){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 159){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 161){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 162){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 163){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 164){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 167){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 169){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 170){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 172){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 173){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 174){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 176){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 177){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 179){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 181){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 183){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 184){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 185){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 187){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 190){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 192){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 193){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 195){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 196){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 197){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 198){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 199){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 200){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 201){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 202){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 203){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 204){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 206){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 207){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 210){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 211){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 212){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 213){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 216){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 221){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 222){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 227){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 228){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 229){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 230){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 231){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 232){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 233){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 234){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 235){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 236){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 241){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 242){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 244){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 245){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 246){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 248){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 251){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 252){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 253){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 254){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 256){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 258){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 259){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 260){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 262){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 263){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 267){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 269){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 272){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 273){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 276){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 277){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 278){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 279){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 281){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 282){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 286){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 288){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppwx01_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  g   d    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  g   d    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  g   s    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  g   s    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  g   b    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> wx  g   b    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  g   d    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  g   d    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  g   s    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  g   s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> wx  g   b    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> wx  g   b    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  g   ux   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> wx  g   ux   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  g   cx   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> wx  g   cx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> wx  g   ux   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> wx  g   ux   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> wx  g   cx   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> wx  g   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  g   ux   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  g   ux   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  g   cx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  g   cx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> wx  d   d    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> wx  d   d    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> wx  d   s    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> wx  d   s    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> wx  d   b    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> wx  d   b    //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  d   d    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> wx  d   d    //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  d   s    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> wx  d   s    //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  d   b    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> wx  d   b    //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 33){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 49){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> wx  d   u    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> wx  u   s    //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> wx  u   b    //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> wx  d   s    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> wx  d   s    //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> wx  s   s    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> wx  s   s    //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> wx  s   b    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> wx  s   b    //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  d   u    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  d   u    //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  d   c    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  d   c    //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  u   s    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  u   s    //
    }
    else if (no_process_parton[i_a] == 64){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  u   b    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  u   b    //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  s   c    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  s   c    //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  c   b    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> wx  c   b    //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> wx  d   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> wx  d   b    //
    }
    else if (no_process_parton[i_a] == 71){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> wx  s   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> wx  s   b    //
    }
    else if (no_process_parton[i_a] == 72){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> wx  b   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> wx  b   b    //
    }
    else if (no_process_parton[i_a] == 73){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  g   g    //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 74){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 77){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 78){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  u   cx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  u   cx   //
    }
    else if (no_process_parton[i_a] == 79){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  s   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  s   dx   //
    }
    else if (no_process_parton[i_a] == 80){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 83){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 84){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  b   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  b   dx   //
    }
    else if (no_process_parton[i_a] == 86){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 87){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  d   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 88){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  d   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 89){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  s   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 90){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  s   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 91){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  b   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 92){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  b   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 93){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  g   g    //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 94){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 95){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  d   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  d   sx   //
    }
    else if (no_process_parton[i_a] == 97){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 98){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  u   cx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  u   cx   //
    }
    else if (no_process_parton[i_a] == 100){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 103){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 105){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  b   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  b   sx   //
    }
    else if (no_process_parton[i_a] == 106){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 108){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 110){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 112){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 113){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  g   g    //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 114){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 116){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  d   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  d   bx   //
    }
    else if (no_process_parton[i_a] == 117){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 118){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  u   cx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  u   cx   //
    }
    else if (no_process_parton[i_a] == 120){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 121){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  s   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  s   bx   //
    }
    else if (no_process_parton[i_a] == 123){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 126){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 128){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> wx  d   s    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> wx  d   s    //
    }
    else if (no_process_parton[i_a] == 130){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> wx  s   s    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> wx  s   s    //
    }
    else if (no_process_parton[i_a] == 131){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> wx  s   b    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> wx  s   b    //
    }
    else if (no_process_parton[i_a] == 135){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 136){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 139){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 140){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 141){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 142){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 143){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 144){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 147){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 148){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 152){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> wx  d   c    //
    }
    else if (no_process_parton[i_a] == 155){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> wx  s   c    //
    }
    else if (no_process_parton[i_a] == 156){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> wx  c   b    //
    }
    else if (no_process_parton[i_a] == 159){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> wx  d   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> wx  d   b    //
    }
    else if (no_process_parton[i_a] == 161){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> wx  s   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> wx  s   b    //
    }
    else if (no_process_parton[i_a] == 162){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> wx  b   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> wx  b   b    //
    }
    else if (no_process_parton[i_a] == 163){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  g   g    //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 164){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 167){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 169){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  s   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  s   dx   //
    }
    else if (no_process_parton[i_a] == 170){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 172){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  c   ux   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  c   ux   //
    }
    else if (no_process_parton[i_a] == 173){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 174){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  b   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  b   dx   //
    }
    else if (no_process_parton[i_a] == 176){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 177){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> wx  d   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 179){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> wx  s   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 181){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> wx  b   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 183){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  g   g    //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 184){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 185){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  d   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  d   sx   //
    }
    else if (no_process_parton[i_a] == 187){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 190){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 192){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  c   ux   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  c   ux   //
    }
    else if (no_process_parton[i_a] == 193){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 195){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  b   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  b   sx   //
    }
    else if (no_process_parton[i_a] == 196){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 197){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 198){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 199){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 200){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 201){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 202){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 203){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  g   g    //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 204){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  d   dx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  d   dx   //
    }
    else if (no_process_parton[i_a] == 206){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  d   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  d   bx   //
    }
    else if (no_process_parton[i_a] == 207){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  u   ux   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 210){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  s   sx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 211){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  s   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  s   bx   //
    }
    else if (no_process_parton[i_a] == 212){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  c   ux   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  c   ux   //
    }
    else if (no_process_parton[i_a] == 213){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  c   cx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 216){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> wx  b   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 221){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 222){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 227){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 228){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 229){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  d   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 230){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  d   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 231){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  s   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  s   ux   //
    }
    else if (no_process_parton[i_a] == 232){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  s   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 233){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  b   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  b   ux   //
    }
    else if (no_process_parton[i_a] == 234){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  b   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 235){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> wx  dx  ux   //
    }
    else if (no_process_parton[i_a] == 236){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> wx  dx  cx   //
    }
    else if (no_process_parton[i_a] == 241){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> wx  ux  ux   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> wx  ux  ux   //
    }
    else if (no_process_parton[i_a] == 242){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 244){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  dx  ux   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> wx  dx  ux   //
    }
    else if (no_process_parton[i_a] == 245){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  dx  cx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> wx  dx  cx   //
    }
    else if (no_process_parton[i_a] == 246){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  ux  sx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> wx  ux  sx   //
    }
    else if (no_process_parton[i_a] == 248){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  sx  cx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> wx  sx  cx   //
    }
    else if (no_process_parton[i_a] == 251){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 252){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> wx  cx  cx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> wx  cx  cx   //
    }
    else if (no_process_parton[i_a] == 253){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> wx  dx  ux   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> wx  dx  ux   //
    }
    else if (no_process_parton[i_a] == 254){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> wx  dx  cx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> wx  dx  cx   //
    }
    else if (no_process_parton[i_a] == 256){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> wx  ux  bx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> wx  ux  bx   //
    }
    else if (no_process_parton[i_a] == 258){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> wx  cx  bx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> wx  cx  bx   //
    }
    else if (no_process_parton[i_a] == 259){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> wx  ux  ux   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> wx  ux  ux   //
    }
    else if (no_process_parton[i_a] == 260){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 262){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> wx  ux  ux   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> wx  ux  ux   //
    }
    else if (no_process_parton[i_a] == 263){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 267){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -3,  -3};   // sx  sx   -> wx  ux  sx   //
    }
    else if (no_process_parton[i_a] == 269){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -3,  -3};   // sx  sx   -> wx  sx  cx   //
    }
    else if (no_process_parton[i_a] == 272){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 273){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> wx  cx  cx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> wx  cx  cx   //
    }
    else if (no_process_parton[i_a] == 276){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> wx  ux  sx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> wx  ux  sx   //
    }
    else if (no_process_parton[i_a] == 277){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> wx  ux  bx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> wx  ux  bx   //
    }
    else if (no_process_parton[i_a] == 278){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> wx  sx  cx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> wx  sx  cx   //
    }
    else if (no_process_parton[i_a] == 279){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> wx  cx  bx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> wx  cx  bx   //
    }
    else if (no_process_parton[i_a] == 281){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> wx  ux  cx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> wx  ux  cx   //
    }
    else if (no_process_parton[i_a] == 282){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> wx  cx  cx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> wx  cx  cx   //
    }
    else if (no_process_parton[i_a] == 286){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> wx  ux  bx   //
    }
    else if (no_process_parton[i_a] == 288){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> wx  cx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
