#include "header.hpp"

#include "ppeexmxnm04.contribution.set.hpp"

ppeexmxnm04_contribution_set::~ppeexmxnm04_contribution_set(){
  static Logger logger("ppeexmxnm04_contribution_set::~ppeexmxnm04_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[7] = 7;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  -2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   0)){no_process_parton[i_a] = 3; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==   3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  -4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  22)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[7] ==  22)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   ex  mx  nm  d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   ex  mx  nm  s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   ex  mx  nm  d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   ex  mx  nm  s    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   ex  mx  nm  ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   ex  mx  nm  cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   ex  mx  nm  ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   ex  mx  nm  cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> e   ex  mx  nm  d    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> e   ex  mx  nm  s    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> e   ex  mx  nm  d    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> e   ex  mx  nm  s    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> e   ex  mx  nm  ux   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> e   ex  mx  nm  cx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> e   ex  mx  nm  ux   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> e   ex  mx  nm  cx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  a    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  a    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  a    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] == -13) && (tp[6] ==  14) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 26; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppeexmxnm04_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  mx  nm  d   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   ex  mx  nm  s   cx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   ex  mx  nm  g   d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   ex  mx  nm  g   s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   ex  mx  nm  g   d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   ex  mx  nm  g   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   ex  mx  nm  g   ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   ex  mx  nm  g   cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   ex  mx  nm  g   ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   ex  mx  nm  g   cx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   ex  mx  nm  d   d    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> e   ex  mx  nm  s   s    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> e   ex  mx  nm  d   d    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> e   ex  mx  nm  s   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   ex  mx  nm  d   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> e   ex  mx  nm  d   s    //
      combination_pdf[2] = { 1,   5,   2};   // b   u    -> e   ex  mx  nm  d   b    //
      combination_pdf[3] = { 1,   5,   4};   // b   c    -> e   ex  mx  nm  s   b    //
      combination_pdf[4] = {-1,   3,   2};   // u   s    -> e   ex  mx  nm  d   s    //
      combination_pdf[5] = {-1,   1,   4};   // c   d    -> e   ex  mx  nm  d   s    //
      combination_pdf[6] = {-1,   5,   2};   // u   b    -> e   ex  mx  nm  d   b    //
      combination_pdf[7] = {-1,   5,   4};   // c   b    -> e   ex  mx  nm  s   b    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  mx  nm  d   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  mx  nm  s   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  mx  nm  d   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  mx  nm  s   cx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  mx  nm  s   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  mx  nm  d   ux   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> e   ex  mx  nm  d   ux   //
      combination_pdf[3] = { 1,   5,  -5};   // b   bx   -> e   ex  mx  nm  s   cx   //
      combination_pdf[4] = {-1,   1,  -1};   // dx  d    -> e   ex  mx  nm  s   cx   //
      combination_pdf[5] = {-1,   3,  -3};   // sx  s    -> e   ex  mx  nm  d   ux   //
      combination_pdf[6] = {-1,   5,  -5};   // bx  b    -> e   ex  mx  nm  d   ux   //
      combination_pdf[7] = {-1,   5,  -5};   // bx  b    -> e   ex  mx  nm  s   cx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   ex  mx  nm  d   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> e   ex  mx  nm  s   ux   //
      combination_pdf[2] = { 1,   5,  -1};   // b   dx   -> e   ex  mx  nm  b   ux   //
      combination_pdf[3] = { 1,   5,  -3};   // b   sx   -> e   ex  mx  nm  b   cx   //
      combination_pdf[4] = {-1,   3,  -1};   // dx  s    -> e   ex  mx  nm  s   ux   //
      combination_pdf[5] = {-1,   1,  -3};   // sx  d    -> e   ex  mx  nm  d   cx   //
      combination_pdf[6] = {-1,   5,  -1};   // dx  b    -> e   ex  mx  nm  b   ux   //
      combination_pdf[7] = {-1,   5,  -3};   // sx  b    -> e   ex  mx  nm  b   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> e   ex  mx  nm  d   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> e   ex  mx  nm  s   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> e   ex  mx  nm  d   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> e   ex  mx  nm  u   s    //
      combination_pdf[2] = {-1,   4,   2};   // u   c    -> e   ex  mx  nm  u   s    //
      combination_pdf[3] = {-1,   2,   4};   // c   u    -> e   ex  mx  nm  d   c    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  g   g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  g   g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  g   g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  g   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  d   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  s   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  d   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  s   sx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  u   ux   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  c   cx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  u   ux   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  c   cx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  s   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  d   dx   //
      combination_pdf[2] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  b   bx   //
      combination_pdf[3] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  b   bx   //
      combination_pdf[4] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  s   sx   //
      combination_pdf[5] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  d   dx   //
      combination_pdf[6] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  b   bx   //
      combination_pdf[7] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  b   bx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  mx  nm  c   cx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  mx  nm  u   ux   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  mx  nm  c   cx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  mx  nm  u   ux   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  mx  nm  d   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  mx  nm  s   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  mx  nm  d   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  mx  nm  s   cx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  mx  nm  s   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  mx  nm  d   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  mx  nm  s   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  mx  nm  d   ux   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> e   ex  mx  nm  d   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> e   ex  mx  nm  s   dx   //
      combination_pdf[2] = { 1,   2,  -5};   // u   bx   -> e   ex  mx  nm  d   bx   //
      combination_pdf[3] = { 1,   4,  -5};   // c   bx   -> e   ex  mx  nm  s   bx   //
      combination_pdf[4] = {-1,   4,  -1};   // dx  c    -> e   ex  mx  nm  s   dx   //
      combination_pdf[5] = {-1,   2,  -3};   // sx  u    -> e   ex  mx  nm  d   sx   //
      combination_pdf[6] = {-1,   2,  -5};   // bx  u    -> e   ex  mx  nm  d   bx   //
      combination_pdf[7] = {-1,   4,  -5};   // bx  c    -> e   ex  mx  nm  s   bx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> e   ex  mx  nm  u   cx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> e   ex  mx  nm  c   ux   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> e   ex  mx  nm  c   ux   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> e   ex  mx  nm  u   cx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   ex  mx  nm  d   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> e   ex  mx  nm  s   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> e   ex  mx  nm  s   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> e   ex  mx  nm  d   cx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> e   ex  mx  nm  dx  ux   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> e   ex  mx  nm  sx  cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   ex  mx  nm  ux  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> e   ex  mx  nm  cx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> e   ex  mx  nm  ux  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> e   ex  mx  nm  cx  cx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> e   ex  mx  nm  dx  cx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> e   ex  mx  nm  ux  sx   //
      combination_pdf[2] = { 1,  -5,  -1};   // bx  dx   -> e   ex  mx  nm  ux  bx   //
      combination_pdf[3] = { 1,  -5,  -3};   // bx  sx   -> e   ex  mx  nm  cx  bx   //
      combination_pdf[4] = {-1,  -3,  -1};   // dx  sx   -> e   ex  mx  nm  ux  sx   //
      combination_pdf[5] = {-1,  -1,  -3};   // sx  dx   -> e   ex  mx  nm  dx  cx   //
      combination_pdf[6] = {-1,  -5,  -1};   // dx  bx   -> e   ex  mx  nm  ux  bx   //
      combination_pdf[7] = {-1,  -5,  -3};   // sx  bx   -> e   ex  mx  nm  cx  bx   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   ex  mx  nm  ux  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> e   ex  mx  nm  ux  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> e   ex  mx  nm  ux  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> e   ex  mx  nm  ux  cx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
