logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  user_particle[oset.user.particle_map["<particle_name>"]].push_back(<particle>);
  if (csi->process_class == "pp-emepmupvm+X"){
    USERPARTICLE("Zrec").push_back(PARTICLE("e")[0] + PARTICLE("e")[1]);
    USERPARTICLE("Wrec").push_back(PARTICLE("mu")[0] + PARTICLE("nma")[0]);
    USERPARTICLE("lepW").push_back(PARTICLE("mu")[0]);
    USERPARTICLE("lpZ").push_back(PARTICLE("ep")[0]);
    USERPARTICLE("lepZ") = PARTICLE("e");
  }
  else if (csi->process_class == "pp-mummupepve+X"){
    USERPARTICLE("Zrec").push_back(PARTICLE("mu")[0] + PARTICLE("mu")[1]);
    USERPARTICLE("Wrec").push_back(PARTICLE("e")[0] + PARTICLE("nea")[0]);
    USERPARTICLE("lepW").push_back(PARTICLE("e")[0]);
    USERPARTICLE("lpZ").push_back(PARTICLE("mup")[0]);
    USERPARTICLE("lepZ") = PARTICLE("mu");
  }

}
logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
