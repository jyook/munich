#include "header.hpp"

#include "ppeexmxnm04.summary.hpp"

ppeexmxnm04_summary_generic::ppeexmxnm04_summary_generic(munich * xmunich){
  Logger logger("ppeexmxnm04_summary_generic::ppeexmxnm04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeexmxnm04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeexmxnm04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeexmxnm04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeexmxnm04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeexmxnm04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeexmxnm04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_born(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_emepmupvm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_emepmupvmd";
    subprocess[2] = "gd~_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_emepmupvmd";
    subprocess[2] = "d~a_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvma";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_emepmupvmd";
    subprocess[2] = "gd~_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_emepmupvmd";
    subprocess[2] = "d~a_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvma";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_emepmupvmd";
    subprocess[2] = "gd~_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_emepmupvmd";
    subprocess[2] = "d~a_emepmupvmu~";
    subprocess[3] = "ud~_emepmupvma";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexmxnm04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeexmxnm04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_emepmupvmdu~";
    subprocess[2] = "gu_emepmupvmgd";
    subprocess[3] = "gd~_emepmupvmgu~";
    subprocess[4] = "du_emepmupvmdd";
    subprocess[5] = "dc_emepmupvmds";
    subprocess[6] = "dd~_emepmupvmdu~";
    subprocess[7] = "dd~_emepmupvmsc~";
    subprocess[8] = "ds~_emepmupvmdc~";
    subprocess[9] = "uu_emepmupvmdu";
    subprocess[10] = "uc_emepmupvmdc";
    subprocess[11] = "ud~_emepmupvmgg";
    subprocess[12] = "ud~_emepmupvmdd~";
    subprocess[13] = "ud~_emepmupvmuu~";
    subprocess[14] = "ud~_emepmupvmss~";
    subprocess[15] = "ud~_emepmupvmcc~";
    subprocess[16] = "uu~_emepmupvmdu~";
    subprocess[17] = "uu~_emepmupvmsc~";
    subprocess[18] = "us~_emepmupvmds~";
    subprocess[19] = "us~_emepmupvmuc~";
    subprocess[20] = "uc~_emepmupvmdc~";
    subprocess[21] = "d~d~_emepmupvmd~u~";
    subprocess[22] = "d~u~_emepmupvmu~u~";
    subprocess[23] = "d~s~_emepmupvmd~c~";
    subprocess[24] = "d~c~_emepmupvmu~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
