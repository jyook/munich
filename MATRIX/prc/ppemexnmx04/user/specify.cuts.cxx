{
  // invariant-mass cut on leptons from Z decay - minimum/maximum
  static int switch_cut_M_Zrec = USERSWITCH("M_Zrec");
  static double cut_min_M_Zrec = USERCUT("min_M_Zrec");
  static double cut_min_M2_Zrec = pow(cut_min_M_Zrec, 2);
  static double cut_max_M_Zrec = USERCUT("max_M_Zrec");
  static double cut_max_M2_Zrec = pow(cut_max_M_Zrec, 2);

  static int switch_cut_M_lmlp = USERSWITCH("M_leplep_OS");
  static double cut_min_M_lmlp = USERCUT("min_M_leplep_OS");
  static double cut_min_M2_lmlp = pow(cut_min_M_lmlp, 2);
  static double cut_max_M_lmlp = USERCUT("max_M_leplep_OS");
  static double cut_max_M2_lmlp = pow(cut_max_M_lmlp, 2);

  static int switch_cut_M_leplep_IR = USERSWITCH("M_leplep_IR");
  static double cut_min_M_leplep_IR = USERCUT("min_M_leplep_IR");
  static double cut_min_M2_leplep_IR = pow(cut_min_M_leplep_IR, 2);

  // invariant-mass cut on 3-lepton system - symmetric around Z mass
  static int switch_cut_delta_M_lepleplep_MZ = USERSWITCH("delta_M_lepleplep_MZ");
  static double cut_min_delta_M_lepleplep_MZ = USERCUT("min_delta_M_lepleplep_MZ");
  static double cut_max_delta_M_lepleplep_MZ = USERCUT("max_delta_M_lepleplep_MZ");

  // invariant-mass cut on leptons from Z decay - symmetric around Z mass
  static int switch_cut_delta_M_Zrec_MZ = USERSWITCH("delta_M_Zrec_MZ");
  static double cut_max_delta_M_Zrec_MZ = USERCUT("max_delta_M_Zrec_MZ");

  // reconstructed W-boson transverse-mass cut
  static int switch_cut_MT_W = USERSWITCH("MT_Wrec");
  static double cut_min_MT_W = USERCUT("min_MT_Wrec");

  // dR cut between any two leptons
  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);

  // dR cut between the two lepton from Z decay
  static int switch_cut_R_lepZlepZ = USERSWITCH("R_lepZlepZ");
  static double cut_min_R_lepZlepZ = USERCUT("min_R_lepZlepZ");
  static double cut_min_R2_lepZlepZ = pow(cut_min_R_lepZlepZ, 2);

  // dR cut between one lepton from Z decay and one lepton from W decay 
  static int switch_cut_R_lepZlepW = USERSWITCH("R_lepZlepW");
  static double cut_min_R_lepZlepW = USERCUT("min_R_lepZlepW");
  static double cut_min_R2_lepZlepW = pow(cut_min_R_lepZlepW, 2);

  // pT cut on leptons from Z decay
  static int switch_electron_cuts = USERSWITCH("electron_cuts");
  static double cut_min_pT_e_1st  = USERCUT("min_pT_e_1st");
  static double cut_min_pT_e_2nd  = USERCUT("min_pT_e_2nd");

  // pT cut on leptons from W decay
  static int switch_muon_cuts = USERSWITCH("muon_cuts");
  static double cut_min_pT_mu_1st = USERCUT("min_pT_mu_1st");
  static double cut_min_pT_mu_2nd = USERCUT("min_pT_mu_2nd");

  // pT cut on leptons
  static int switch_lepton_cuts = USERSWITCH("lepton_cuts");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");
  static double cut_min_pT_lep_2nd = USERCUT("min_pT_lep_2nd");

  // pT cut on hardest leptons depending on wether it is an electron or a muon
  static int switch_leading_lepton_cuts = USERSWITCH("leading_lepton_cuts");
  static double cut_min_pT_1st_if_e  = USERCUT("min_pT_1st_if_e");
  static double cut_min_pT_1st_if_mu = USERCUT("min_pT_1st_if_mu");

  static int switch_pT_lep_CMS_special = USERSWITCH("pT_lep_CMS_special");


  // invariant-mass cuts on leptons from Z decay
  if (switch_cut_delta_M_Zrec_MZ || switch_cut_M_Zrec){
    double M2_Zrec = PARTICLE("Zrec")[0].m2;
    // invariant-mass cut on leptons from Z decay - symmetric around Z mass
    if (switch_cut_delta_M_Zrec_MZ){
      if (abs(sqrt(M2_Zrec) - msi->M_Z) > cut_max_delta_M_Zrec_MZ) {
	cut_ps[i_a] = -1;
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_max_delta_M_Zrec_MZ" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_max_delta_M_Zrec_MZ cut applied" << endl; 
	return;
      }
    }


    // invariant-mass cut on electron--positron pairs from Z decay - minimum/maximum
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_Zrec = " << switch_cut_M_Zrec << endl;}

    if (switch_cut_M_Zrec){
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(Zrec)[0] = " << PARTICLE("Zrec")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_Zrec = " << M2_Zrec << " > " << cut_min_M2_Zrec << endl;}
      if (M2_Zrec < cut_min_M2_Zrec){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemexnmx04-cut after cut_M_Zrec" << endl;
	  logger << LOG_DEBUG << endl << info_cut.str();
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Zrec min cut applied" << endl; 
	return;
      }
      
      if (switch_output_cutinfo){if (cut_max_M2_Zrec != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_Zrec = " << M2_Zrec << " < " << cut_max_M2_Zrec << endl;}}
      
      if (cut_max_M2_Zrec != 0. && M2_Zrec > cut_max_M2_Zrec){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemexnmx04-cut after cut_M_Zrec" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Zrec max cut applied" << endl; 
	return;
      }
    }
  }

  // invariant-mass cut on 3-lepton system - symmetric around Z mass
  if (switch_cut_delta_M_lepleplep_MZ == 1){
    double abs_M_3l_minus_MZ = abs((PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum).m() - msi->M_Z);
    if (abs_M_3l_minus_MZ < cut_min_delta_M_lepleplep_MZ || 
	(cut_max_delta_M_lepleplep_MZ != 0. && abs_M_3l_minus_MZ > cut_max_delta_M_lepleplep_MZ)) {
      cut_ps[i_a] = -1;
      return;
    }
  }

  // IR safety cut: invariant mass of any em-ep combination must be larger than cut_min_M_leplep_IR
  if (switch_cut_M_leplep_IR){
    if (NUMBER("e") > 1){
      for (int i_lp = 0; i_lp < PARTICLE("ep").size(); i_lp++){
	for (int i_lm = 0; i_lm < PARTICLE("em").size(); i_lm++){
	  double M2_leplep = (PARTICLE("ep")[i_lp].momentum + PARTICLE("em")[i_lm].momentum).m2();
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(ep)[" << i_lp << "] = " << PARTICLE("ep")[i_lp].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(em)[" << i_lm << "] = " << PARTICLE("em")[i_lm].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_leplep_IR << endl;}

	  if (M2_leplep < cut_min_M2_leplep_IR){
	    cut_ps[i_a] = -1; 
	    if (switch_output_cutinfo){
	      info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_leplep_min_IR" << endl; 
	      logger << LOG_DEBUG << endl << info_cut.str(); 
	    }
	    logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
	    return;
	  }
	}
      }
    }
    if (NUMBER("mu") > 1){
      for (int i_lp = 0; i_lp < PARTICLE("mup").size(); i_lp++){
	for (int i_lm = 0; i_lm < PARTICLE("mum").size(); i_lm++){
	  double M2_leplep = (PARTICLE("mup")[i_lp].momentum + PARTICLE("mum")[i_lm].momentum).m2();
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(mup)[" << i_lp << "] = " << PARTICLE("mup")[i_lp].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(mum)[" << i_lm << "] = " << PARTICLE("mum")[i_lm].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_leplep_IR << endl;}

	  if (M2_leplep < cut_min_M2_leplep_IR){
	    cut_ps[i_a] = -1; 
	    if (switch_output_cutinfo){
	      info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_leplep_min_IR" << endl; 
	      logger << LOG_DEBUG << endl << info_cut.str(); 
	    }
	    logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
	    return;
	  }
	}
      }
    }
  }



  // reconstructed W-boson transverse-mass cut (from muon)
  if (switch_cut_MT_W){
    double temp_dphi = abs(PARTICLE("lepW")[0].phi - PARTICLE("nua")[0].phi);
    if (temp_dphi > pi){temp_dphi = f2pi - temp_dphi;}
    double MT_W = sqrt(2 * PARTICLE("lepW")[0].pT * PARTICLE("nua")[0].pT * (1. - cos(temp_dphi)));
    if (MT_W < cut_min_MT_W) {
      cut_ps[i_a] = -1;
      return;
    }
  }


  // dR cut between the two electrons from Z decay
  if (switch_cut_R_lepZlepZ){
    double R2_eta_leplep = R2_eta(PARTICLE("lepZ")[0], PARTICLE("lepZ")[1]);
    if (R2_eta_leplep < cut_min_R2_lepZlepZ) {
      cut_ps[i_a] = -1;
      return;
    }
  }



  // dR cut between one lepton from Z decay and one lepton from W decay 
  if (switch_cut_R_lepZlepW){
    for (int i_l = 0; i_l < PARTICLE("lepZ").size(); i_l++){
      for (int j_l = 0; j_l < PARTICLE("lepW").size(); j_l++){
	double R2_eta_leplep = R2_eta(PARTICLE("lepZ")[i_l], PARTICLE("lepW")[j_l]);
	if (R2_eta_leplep < cut_min_R2_lepZlepW) {
	  cut_ps[i_a] = -1;
	  return;
	}
      }
    }
  }


  // invariant-mass cut on lepton--anti-lepton pairs (not necessarily from Z decay) - minimum/maximum
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_lmlp = " << switch_cut_M_lmlp << endl;}
  if (switch_cut_M_lmlp == 1){
    for (int i_lm = 0; i_lm < NUMBER("lm"); i_lm++){
      for (int i_lp = 0; i_lp < NUMBER("lp"); i_lp++){
	double M2_lmlp = (PARTICLE("lm")[i_lm].momentum + PARTICLE("lp")[i_lp].momentum).m2();

	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lm)[" << i_lm << "] = " << PARTICLE("lm")[i_lm].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lp)[" << i_lp << "] = " << PARTICLE("lp")[i_lp].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_lmlp = " << M2_lmlp << " > " << cut_min_M2_lmlp << endl;}
	
	if (M2_lmlp < cut_min_M2_lmlp){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemexnmx04-cut after cut_M_lmlp" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lmlp min cut applied" << endl; 
	  return;
	}
	
	if (switch_output_cutinfo){if (cut_max_M2_lmlp != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_lmlp = " << M2_lmlp << " < " << cut_max_M2_lmlp << endl;}}
	
	if (cut_max_M2_lmlp != 0 && M2_lmlp > cut_max_M2_lmlp){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemexnmx04-cut after cut_M_lmlp" << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lmlp max cut applied" << endl; 
	  return;
	}
      }
    }
  }



  // lepton--lepton isolation cuts
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_leplep = " << switch_cut_R_leplep << endl; }
  if (switch_cut_R_leplep == 1){
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      for (int j_l = i_l + 1; j_l < PARTICLE("lep").size(); j_l++){
	double R2_eta_leplep = R2_eta(PARTICLE("lep")[i_l], PARTICLE("lep")[j_l]);
	
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << j_l << "] = " << PARTICLE("lep")[j_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_leplep = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}
	
	if (R2_eta_leplep < cut_min_R2_leplep){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemexnmx04-cut after cut_R_leplep of lepton " << i_l << " and " << j_l << "." << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_R_leplep cut applied" << endl; 
	  return;
	}
      }
    }
  }



  // cuts on hardest and second-hardest electron
  if (switch_electron_cuts){
    double pT_e_1st = PARTICLE("e")[0].pT;
    double pT_e_2nd = PARTICLE("e")[1].pT;
    if (pT_e_1st < cut_min_pT_e_1st || pT_e_2nd < cut_min_pT_e_2nd){
      cut_ps[i_a] = -1; 
      return;
    }
  }

  // cuts on hardest and second-hardest muon
  if (switch_muon_cuts){
    double pT_mu_1st = PARTICLE("mu")[0].pT;
    double pT_mu_2nd = PARTICLE("mu")[1].pT;
    if (pT_mu_1st < cut_min_pT_mu_1st || pT_mu_2nd < cut_min_pT_mu_2nd){
      cut_ps[i_a] = -1; 
      return;
    }
  }

  // cuts on hardest and second-hardest lepton
  if (switch_lepton_cuts){
    double pT_lep_1st = PARTICLE("lep")[0].pT;
    double pT_lep_2nd = PARTICLE("lep")[1].pT;
    if (pT_lep_1st < cut_min_pT_lep_1st || pT_lep_2nd < cut_min_pT_lep_2nd){
      cut_ps[i_a] = -1; 
      return;
    }
  }


  // cuts on leading leptons
  if (switch_leading_lepton_cuts){
    double pT_e_1st = PARTICLE("e")[0].pT;
    double pT_mu_1st = PARTICLE("mu")[0].pT;
    double pT_lep_1st = PARTICLE("lep")[0].pT;
    if(pT_e_1st == pT_lep_1st && pT_e_1st < cut_min_pT_1st_if_e) {
      cut_ps[i_a] = -1; 
      return;      
    }
    if(pT_mu_1st == pT_lep_1st && pT_mu_1st < cut_min_pT_1st_if_mu) {
      cut_ps[i_a] = -1; 
      return;      
    }
  }

  // pT_lep_1st cut - special CMS BSM background version: generic implementation to be used for all WZ processes
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_pT_lep_CMS_special = " << switch_pT_lep_CMS_special << endl;}
  if (switch_pT_lep_CMS_special == 1){
    double pT_threshold = 25.;
    if (NUMBER("mu") > 1 && PARTICLE("lep")[0].momentum == PARTICLE("mu")[0].momentum){pT_threshold = 20.;}
    if (PARTICLE("lep")[0].pT < pT_threshold){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0].pT = " << PARTICLE("lep")[0].pT << " < " << pT_threshold << " = " << pT_threshold << endl;}
      return;
    }
  }

  if (switch_output_cutinfo){info_cut << "ppemexnmx04_cuts passed" << endl;}
}
