if (csi->type_correction == "QCD"){
  static int switch_cut_M_Zrec = user->switch_value[user->switch_map["M_Zrec"]];
  static int switch_cut_delta_M_Zrec_MZ = user->switch_value[user->switch_map["delta_M_Zrec_MZ"]];
  if (switch_cut_M_Zrec || switch_cut_delta_M_Zrec_MZ){
    static double cut_min_M_Zrec = user->cut_value[user->cut_map["min_M_Zrec"]];
    static double cut_max_delta_M_Zrec_MZ = user->cut_value[user->cut_map["max_delta_M_Zrec_MZ"]];
    for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
      if (switch_cut_M_Zrec && !switch_cut_delta_M_Zrec_MZ){sqrtsmin_opt[i_a][20] = cut_min_M_Zrec;}
      if (!switch_cut_M_Zrec && switch_cut_delta_M_Zrec_MZ){sqrtsmin_opt[i_a][20] = M[23] - cut_max_delta_M_Zrec_MZ;}
      if (switch_cut_M_Zrec && switch_cut_delta_M_Zrec_MZ){sqrtsmin_opt[i_a][20] = max(cut_min_M_Zrec, M[23] - cut_max_delta_M_Zrec_MZ);}
    }
  }
}

