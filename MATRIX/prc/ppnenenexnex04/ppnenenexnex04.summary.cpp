#include "header.hpp"

#include "ppnenenexnex04.summary.hpp"

ppnenenexnex04_summary_generic::ppnenenexnex04_summary_generic(munich * xmunich){
  Logger logger("ppnenenexnex04_summary_generic::ppnenenexnex04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppnenenexnex04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppnenenexnex04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppnenenexnex04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppnenenexnex04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppnenenexnex04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppnenenexnex04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_born(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veveve~ve~";
    subprocess[2] = "uu~_veveve~ve~";
    subprocess[3] = "bb~_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veveve~ve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veveve~ve~d";
    subprocess[2] = "gu_veveve~ve~u";
    subprocess[3] = "gb_veveve~ve~b";
    subprocess[4] = "gd~_veveve~ve~d~";
    subprocess[5] = "gu~_veveve~ve~u~";
    subprocess[6] = "gb~_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~g";
    subprocess[8] = "uu~_veveve~ve~g";
    subprocess[9] = "bb~_veveve~ve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_veveve~ve~g";
    subprocess[2] = "gd_veveve~ve~d";
    subprocess[3] = "gu_veveve~ve~u";
    subprocess[4] = "gb_veveve~ve~b";
    subprocess[5] = "gd~_veveve~ve~d~";
    subprocess[6] = "gu~_veveve~ve~u~";
    subprocess[7] = "gb~_veveve~ve~b~";
    subprocess[8] = "dd~_veveve~ve~g";
    subprocess[9] = "uu~_veveve~ve~g";
    subprocess[10] = "bb~_veveve~ve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veveve~ve~d";
    subprocess[2] = "ua_veveve~ve~u";
    subprocess[3] = "ba_veveve~ve~b";
    subprocess[4] = "d~a_veveve~ve~d~";
    subprocess[5] = "u~a_veveve~ve~u~";
    subprocess[6] = "b~a_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~a";
    subprocess[8] = "uu~_veveve~ve~a";
    subprocess[9] = "bb~_veveve~ve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veveve~ve~d";
    subprocess[2] = "gu_veveve~ve~u";
    subprocess[3] = "gb_veveve~ve~b";
    subprocess[4] = "gd~_veveve~ve~d~";
    subprocess[5] = "gu~_veveve~ve~u~";
    subprocess[6] = "gb~_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~g";
    subprocess[8] = "uu~_veveve~ve~g";
    subprocess[9] = "bb~_veveve~ve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veveve~ve~d";
    subprocess[2] = "ua_veveve~ve~u";
    subprocess[3] = "ba_veveve~ve~b";
    subprocess[4] = "d~a_veveve~ve~d~";
    subprocess[5] = "u~a_veveve~ve~u~";
    subprocess[6] = "b~a_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~a";
    subprocess[8] = "uu~_veveve~ve~a";
    subprocess[9] = "bb~_veveve~ve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veveve~ve~d";
    subprocess[2] = "gu_veveve~ve~u";
    subprocess[3] = "gb_veveve~ve~b";
    subprocess[4] = "gd~_veveve~ve~d~";
    subprocess[5] = "gu~_veveve~ve~u~";
    subprocess[6] = "gb~_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~g";
    subprocess[8] = "uu~_veveve~ve~g";
    subprocess[9] = "bb~_veveve~ve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veveve~ve~d";
    subprocess[2] = "ua_veveve~ve~u";
    subprocess[3] = "ba_veveve~ve~b";
    subprocess[4] = "d~a_veveve~ve~d~";
    subprocess[5] = "u~a_veveve~ve~u~";
    subprocess[6] = "b~a_veveve~ve~b~";
    subprocess[7] = "dd~_veveve~ve~a";
    subprocess[8] = "uu~_veveve~ve~a";
    subprocess[9] = "bb~_veveve~ve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppnenenexnex04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_veveve~ve~dd~";
    subprocess[2] = "gg_veveve~ve~uu~";
    subprocess[3] = "gg_veveve~ve~bb~";
    subprocess[4] = "gd_veveve~ve~gd";
    subprocess[5] = "gu_veveve~ve~gu";
    subprocess[6] = "gb_veveve~ve~gb";
    subprocess[7] = "gd~_veveve~ve~gd~";
    subprocess[8] = "gu~_veveve~ve~gu~";
    subprocess[9] = "gb~_veveve~ve~gb~";
    subprocess[10] = "dd_veveve~ve~dd";
    subprocess[11] = "du_veveve~ve~du";
    subprocess[12] = "ds_veveve~ve~ds";
    subprocess[13] = "dc_veveve~ve~dc";
    subprocess[14] = "db_veveve~ve~db";
    subprocess[15] = "dd~_veveve~ve~gg";
    subprocess[16] = "dd~_veveve~ve~dd~";
    subprocess[17] = "dd~_veveve~ve~uu~";
    subprocess[18] = "dd~_veveve~ve~ss~";
    subprocess[19] = "dd~_veveve~ve~cc~";
    subprocess[20] = "dd~_veveve~ve~bb~";
    subprocess[21] = "du~_veveve~ve~du~";
    subprocess[22] = "ds~_veveve~ve~ds~";
    subprocess[23] = "dc~_veveve~ve~dc~";
    subprocess[24] = "db~_veveve~ve~db~";
    subprocess[25] = "uu_veveve~ve~uu";
    subprocess[26] = "uc_veveve~ve~uc";
    subprocess[27] = "ub_veveve~ve~ub";
    subprocess[28] = "ud~_veveve~ve~ud~";
    subprocess[29] = "uu~_veveve~ve~gg";
    subprocess[30] = "uu~_veveve~ve~dd~";
    subprocess[31] = "uu~_veveve~ve~uu~";
    subprocess[32] = "uu~_veveve~ve~ss~";
    subprocess[33] = "uu~_veveve~ve~cc~";
    subprocess[34] = "uu~_veveve~ve~bb~";
    subprocess[35] = "us~_veveve~ve~us~";
    subprocess[36] = "uc~_veveve~ve~uc~";
    subprocess[37] = "ub~_veveve~ve~ub~";
    subprocess[38] = "bb_veveve~ve~bb";
    subprocess[39] = "bd~_veveve~ve~bd~";
    subprocess[40] = "bu~_veveve~ve~bu~";
    subprocess[41] = "bb~_veveve~ve~gg";
    subprocess[42] = "bb~_veveve~ve~dd~";
    subprocess[43] = "bb~_veveve~ve~uu~";
    subprocess[44] = "bb~_veveve~ve~bb~";
    subprocess[45] = "d~d~_veveve~ve~d~d~";
    subprocess[46] = "d~u~_veveve~ve~d~u~";
    subprocess[47] = "d~s~_veveve~ve~d~s~";
    subprocess[48] = "d~c~_veveve~ve~d~c~";
    subprocess[49] = "d~b~_veveve~ve~d~b~";
    subprocess[50] = "u~u~_veveve~ve~u~u~";
    subprocess[51] = "u~c~_veveve~ve~u~c~";
    subprocess[52] = "u~b~_veveve~ve~u~b~";
    subprocess[53] = "b~b~_veveve~ve~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
