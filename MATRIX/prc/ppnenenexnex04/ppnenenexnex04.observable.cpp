#include "header.hpp"

#include "ppnenenexnex04.observable.set.hpp"

#define USERSWITCH(__switch__) user->switch_value[user->switch_map[__switch__]]
#define USERINT(__int__) user->int_value[user->int_map[__int__]]
#define USERDOUBLE(__double__) user->double_value[user->double_map[__double__]]
#define USERSTRING(__string__) user->string_value[user->string_map[__string__]]
#define USERCUT(__cut__) user->cut_value[user->cut_map[__cut__]]
#define USERPARTICLE(__particle__) esi->user_particle[user->particle_map[__particle__]][i_a]
#define PARTICLE(__particle__) esi->particle_event[esi->access_object[__particle__]][i_a]
#define NUMBER(__particle__) esi->n_object[esi->access_object[__particle__]][i_a]

ppnenenexnex04_observable_set::~ppnenenexnex04_observable_set(){
  static Logger logger("ppnenenexnex04_observable_set::~ppnenenexnex04_observable_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_observable_set::moments(){
  static Logger logger("ppnenenexnex04_observable_set::moments");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.moments.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_observable_set::calculate_dynamic_scale(int i_a){
  static Logger logger("ppnenenexnex04_observable_set::calculate_dynamic_scale");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.prepare.scales.cxx"
  for (int sd = 1; sd < value_mu_ren_rel.size(); sd++){
    if (value_mu_ren_rel[sd].size() != 0){
      double temp_mu_central = 1.;
#include "specify.scales.cxx"
      for (int ss = 0; ss < value_mu_ren_rel[sd].size(); ss++){
        value_mu_ren[sd][ss] = temp_mu_central * value_mu_ren_rel[sd][ss];
        value_alpha_S[sd][ss] = LHAPDF::alphasPDF(value_mu_ren[sd][ss]);
        value_factor_alpha_S[sd][ss] = pow(value_alpha_S[sd][ss] / alpha_S, csi->contribution_order_alpha_s);
      }
    }
  }
  if (id_scales == 1){
    value_mu_fact = value_mu_ren;
  }
  else {
    for (int sd = 1; sd < value_mu_fact_rel.size(); sd++){
      if (value_mu_fact_rel[sd].size() != 0){
        double temp_mu_central = 1.;
#include "specify.scales.cxx"
        for (int ss = 0; ss < value_mu_fact_rel[sd].size(); ss++){
          value_mu_fact[sd][ss] = temp_mu_central * value_mu_fact_rel[sd][ss];
        }
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_observable_set::calculate_dynamic_scale_RA(int i_a){
  static Logger logger("ppnenenexnex04_observable_set::calculate_dynamic_scale_RA");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.prepare.scales.cxx"
  for (int sd = 1; sd < value_mu_ren_rel.size(); sd++){
    if (value_mu_ren_rel[sd].size() != 0){
      double temp_mu_central = 1.;
#include "specify.scales.cxx"
      for (int ss = 0; ss < value_mu_ren_rel[sd].size(); ss++){
        RA_value_mu_ren[i_a][sd][ss] = temp_mu_central * value_mu_ren_rel[sd][ss];
        RA_value_alpha_S[i_a][sd][ss] = LHAPDF::alphasPDF(RA_value_mu_ren[i_a][sd][ss]);
        RA_value_factor_alpha_S[i_a][sd][ss] = pow(RA_value_alpha_S[i_a][sd][ss] / alpha_S, csi->contribution_order_alpha_s);
      }
    }
  }
  if (id_scales == 1){
    RA_value_mu_fact[i_a] = RA_value_mu_ren[i_a];
  }
  else {
    for (int sd = 1; sd < value_mu_fact_rel.size(); sd++){
      if (value_mu_fact_rel[sd].size() != 0){
        double temp_mu_central = 1.;
#include "specify.scales.cxx"
        for (int ss = 0; ss < value_mu_fact_rel[sd].size(); ss++){
          RA_value_mu_fact[i_a][sd][ss] = temp_mu_central * value_mu_fact_rel[sd][ss];
        }
      }
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_observable_set::calculate_dynamic_scale_TSV(int i_a){
  static Logger logger("ppnenenexnex04_observable_set::calculate_dynamic_scale_TSV");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.prepare.scales.cxx"
  for (int sd = 1; sd < max_dyn_ren + 1; sd++){
    double temp_mu_central = 1.;
#include "specify.scales.cxx"
    for (int ss = 0; ss < n_scale_dyn_ren[sd]; ss++){
      value_scale_ren[i_a][sd][ss] = temp_mu_central * value_relative_scale_ren[sd][ss];
      if (needed_scale2_ren){value_scale2_ren[i_a][sd][ss] = pow(value_scale_ren[i_a][sd][ss], 2);}
      value_alpha_S_TSV[i_a][sd][ss] = LHAPDF::alphasPDF(value_scale_ren[i_a][sd][ss]);
      value_relative_factor_alpha_S[i_a][sd][ss] = pow(value_alpha_S_TSV[i_a][sd][ss] / alpha_S, csi->contribution_order_alpha_s);
    }
  }
  for (int sd = 1; sd < max_dyn_fact + 1; sd++){
    double temp_mu_central = 1.;
#include "specify.scales.cxx"
    value_central_scale_fact[sd] = temp_mu_central;
    if (needed_scale2_fact){value_central_logscale2_fact[sd] = 2 * log(temp_mu_central);}
    for (int ss = 0; ss < n_scale_dyn_fact[sd]; ss++){
      value_scale_fact[i_a][sd][ss] = value_central_scale_fact[sd] * value_relative_scale_fact[sd][ss];
      if (needed_scale2_fact){value_scale2_fact[i_a][sd][ss] = pow(value_scale_fact[i_a][sd][ss], 2);}
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
