#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"

#include "ppnenenexnex04.contribution.set.hpp"
#include "ppnenenexnex04.event.set.hpp"
#include "ppnenenexnex04.phasespace.set.hpp"
#include "ppnenenexnex04.observable.set.hpp"
#include "ppnenenexnex04.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-veveve~ve~+X");

  MUC->csi = new ppnenenexnex04_contribution_set();
  MUC->esi = new ppnenenexnex04_event_set();
  MUC->psi = new ppnenenexnex04_phasespace_set();
  MUC->osi = new ppnenenexnex04_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllll04_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppnenenexnex04_phasespace_set();
      MUC->psi->fake_psi->csi = new ppnenenexnex04_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppnenenexnex04_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
