#include "header.hpp"

#include "ppnenenexnex04.contribution.set.hpp"

ppnenenexnex04_contribution_set::~ppnenenexnex04_contribution_set(){
  static Logger logger("ppnenenexnex04_contribution_set::~ppnenenexnex04_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 5; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex  //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex  //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex  //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex  //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex  //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex  //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex  //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex  //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex  //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex  //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[7] = 7;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 23; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 17; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> ne  ne  nex nex d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> ne  ne  nex nex s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> ne  ne  nex nex d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> ne  ne  nex nex s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ne  ne  nex nex u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ne  ne  nex nex c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ne  ne  nex nex u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ne  ne  nex nex c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> ne  ne  nex nex b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> ne  ne  nex nex b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ne  ne  nex nex dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ne  ne  nex nex sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ne  ne  nex nex dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ne  ne  nex nex sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> ne  ne  nex nex ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> ne  ne  nex nex cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> ne  ne  nex nex ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> ne  ne  nex nex cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ne  ne  nex nex bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ne  ne  nex nex bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> ne  ne  nex nex d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> ne  ne  nex nex s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> ne  ne  nex nex d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> ne  ne  nex nex s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ne  ne  nex nex u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> ne  ne  nex nex c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> ne  ne  nex nex u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> ne  ne  nex nex c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> ne  ne  nex nex b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> ne  ne  nex nex b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> ne  ne  nex nex dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> ne  ne  nex nex sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> ne  ne  nex nex dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> ne  ne  nex nex sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> ne  ne  nex nex ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> ne  ne  nex nex cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> ne  ne  nex nex ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> ne  ne  nex nex cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> ne  ne  nex nex bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> ne  ne  nex nex bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex g   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> ne  ne  nex nex d   //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> ne  ne  nex nex s   //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> ne  ne  nex nex d   //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> ne  ne  nex nex s   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ne  ne  nex nex u   //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ne  ne  nex nex c   //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ne  ne  nex nex u   //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ne  ne  nex nex c   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> ne  ne  nex nex b   //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> ne  ne  nex nex b   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ne  ne  nex nex dx  //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ne  ne  nex nex sx  //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ne  ne  nex nex dx  //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ne  ne  nex nex sx  //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> ne  ne  nex nex ux  //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> ne  ne  nex nex cx  //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> ne  ne  nex nex ux  //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> ne  ne  nex nex cx  //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ne  ne  nex nex bx  //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ne  ne  nex nex bx  //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex g   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex g   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex g   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex g   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex g   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex g   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex g   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex g   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex g   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex g   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  12) && (tp[4] ==  12) && (tp[5] == -12) && (tp[6] == -12) && (tp[out[7]] ==  -5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 8;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 8;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenenexnex04_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppnenenexnex04_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ne  ne  nex nex b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> ne  ne  nex nex g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> ne  ne  nex nex g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> ne  ne  nex nex g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> ne  ne  nex nex g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ne  ne  nex nex g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ne  ne  nex nex g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ne  ne  nex nex g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ne  ne  nex nex g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> ne  ne  nex nex g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> ne  ne  nex nex g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ne  ne  nex nex g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ne  ne  nex nex g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ne  ne  nex nex g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ne  ne  nex nex g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> ne  ne  nex nex g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> ne  ne  nex nex g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> ne  ne  nex nex g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> ne  ne  nex nex g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ne  ne  nex nex g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ne  ne  nex nex g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> ne  ne  nex nex d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> ne  ne  nex nex s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ne  ne  nex nex d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> ne  ne  nex nex s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> ne  ne  nex nex d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> ne  ne  nex nex s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> ne  ne  nex nex d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> ne  ne  nex nex d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ne  ne  nex nex d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> ne  ne  nex nex u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> ne  ne  nex nex u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> ne  ne  nex nex d   c    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> ne  ne  nex nex d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> ne  ne  nex nex s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> ne  ne  nex nex d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> ne  ne  nex nex s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ne  ne  nex nex b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ne  ne  nex nex b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ne  ne  nex nex b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ne  ne  nex nex b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> ne  ne  nex nex d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> ne  ne  nex nex s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> ne  ne  nex nex d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> ne  ne  nex nex s   cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> ne  ne  nex nex d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> ne  ne  nex nex s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> ne  ne  nex nex s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> ne  ne  nex nex d   sx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> ne  ne  nex nex d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> ne  ne  nex nex s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> ne  ne  nex nex s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> ne  ne  nex nex d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> ne  ne  nex nex d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> ne  ne  nex nex s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> ne  ne  nex nex d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> ne  ne  nex nex s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ne  ne  nex nex u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> ne  ne  nex nex c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ne  ne  nex nex u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> ne  ne  nex nex u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> ne  ne  nex nex u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> ne  ne  nex nex c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> ne  ne  nex nex u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> ne  ne  nex nex c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ne  ne  nex nex u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ne  ne  nex nex c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ne  ne  nex nex u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ne  ne  nex nex c   sx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ne  ne  nex nex b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ne  ne  nex nex b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ne  ne  nex nex b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ne  ne  nex nex b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ne  ne  nex nex u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> ne  ne  nex nex c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> ne  ne  nex nex c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> ne  ne  nex nex u   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ne  ne  nex nex u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> ne  ne  nex nex c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> ne  ne  nex nex c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> ne  ne  nex nex u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ne  ne  nex nex u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> ne  ne  nex nex c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> ne  ne  nex nex u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> ne  ne  nex nex c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> ne  ne  nex nex b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> ne  ne  nex nex b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> ne  ne  nex nex b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> ne  ne  nex nex b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> ne  ne  nex nex b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> ne  ne  nex nex b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> ne  ne  nex nex b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> ne  ne  nex nex b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> ne  ne  nex nex b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ne  ne  nex nex b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ne  ne  nex nex b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> ne  ne  nex nex dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> ne  ne  nex nex sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> ne  ne  nex nex dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> ne  ne  nex nex sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> ne  ne  nex nex dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> ne  ne  nex nex sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ne  ne  nex nex dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> ne  ne  nex nex dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> ne  ne  nex nex dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> ne  ne  nex nex ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> ne  ne  nex nex ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> ne  ne  nex nex dx  cx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ne  ne  nex nex dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> ne  ne  nex nex sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> ne  ne  nex nex dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> ne  ne  nex nex sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> ne  ne  nex nex ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> ne  ne  nex nex cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> ne  ne  nex nex ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> ne  ne  nex nex ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> ne  ne  nex nex ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> ne  ne  nex nex cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> ne  ne  nex nex ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> ne  ne  nex nex cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> ne  ne  nex nex bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
