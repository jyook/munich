#include "header.hpp"

#include "ppenex02.summary.hpp"

ppenex02_summary_generic::ppenex02_summary_generic(munich * xmunich){
  Logger logger("ppenex02_summary_generic::ppenex02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppenex02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppenex02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppenex02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppenex02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppenex02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppenex02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_born(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~";
    subprocess[2] = "dc~_emve~";
    subprocess[3] = "su~_emve~";
    subprocess[4] = "sc~_emve~";
    subprocess[5] = "bu~_emve~";
    subprocess[6] = "bc~_emve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gd_emve~c";
    subprocess[3] = "gs_emve~u";
    subprocess[4] = "gs_emve~c";
    subprocess[5] = "gb_emve~u";
    subprocess[6] = "gb_emve~c";
    subprocess[7] = "gu~_emve~d~";
    subprocess[8] = "gu~_emve~s~";
    subprocess[9] = "gu~_emve~b~";
    subprocess[10] = "gc~_emve~d~";
    subprocess[11] = "gc~_emve~s~";
    subprocess[12] = "gc~_emve~b~";
    subprocess[13] = "du~_emve~g";
    subprocess[14] = "dc~_emve~g";
    subprocess[15] = "su~_emve~g";
    subprocess[16] = "sc~_emve~g";
    subprocess[17] = "bu~_emve~g";
    subprocess[18] = "bc~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "da_emve~c";
    subprocess[3] = "sa_emve~u";
    subprocess[4] = "sa_emve~c";
    subprocess[5] = "ba_emve~u";
    subprocess[6] = "ba_emve~c";
    subprocess[7] = "u~a_emve~d~";
    subprocess[8] = "u~a_emve~s~";
    subprocess[9] = "u~a_emve~b~";
    subprocess[10] = "c~a_emve~d~";
    subprocess[11] = "c~a_emve~s~";
    subprocess[12] = "c~a_emve~b~";
    subprocess[13] = "du~_emve~a";
    subprocess[14] = "dc~_emve~a";
    subprocess[15] = "su~_emve~a";
    subprocess[16] = "sc~_emve~a";
    subprocess[17] = "bu~_emve~a";
    subprocess[18] = "bc~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gd_emve~c";
    subprocess[3] = "gs_emve~u";
    subprocess[4] = "gs_emve~c";
    subprocess[5] = "gb_emve~u";
    subprocess[6] = "gb_emve~c";
    subprocess[7] = "gu~_emve~d~";
    subprocess[8] = "gu~_emve~s~";
    subprocess[9] = "gu~_emve~b~";
    subprocess[10] = "gc~_emve~d~";
    subprocess[11] = "gc~_emve~s~";
    subprocess[12] = "gc~_emve~b~";
    subprocess[13] = "du~_emve~g";
    subprocess[14] = "dc~_emve~g";
    subprocess[15] = "su~_emve~g";
    subprocess[16] = "sc~_emve~g";
    subprocess[17] = "bu~_emve~g";
    subprocess[18] = "bc~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "da_emve~c";
    subprocess[3] = "sa_emve~u";
    subprocess[4] = "sa_emve~c";
    subprocess[5] = "ba_emve~u";
    subprocess[6] = "ba_emve~c";
    subprocess[7] = "u~a_emve~d~";
    subprocess[8] = "u~a_emve~s~";
    subprocess[9] = "u~a_emve~b~";
    subprocess[10] = "c~a_emve~d~";
    subprocess[11] = "c~a_emve~s~";
    subprocess[12] = "c~a_emve~b~";
    subprocess[13] = "du~_emve~a";
    subprocess[14] = "dc~_emve~a";
    subprocess[15] = "su~_emve~a";
    subprocess[16] = "sc~_emve~a";
    subprocess[17] = "bu~_emve~a";
    subprocess[18] = "bc~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gd_emve~c";
    subprocess[3] = "gs_emve~u";
    subprocess[4] = "gs_emve~c";
    subprocess[5] = "gb_emve~u";
    subprocess[6] = "gb_emve~c";
    subprocess[7] = "gu~_emve~d~";
    subprocess[8] = "gu~_emve~s~";
    subprocess[9] = "gu~_emve~b~";
    subprocess[10] = "gc~_emve~d~";
    subprocess[11] = "gc~_emve~s~";
    subprocess[12] = "gc~_emve~b~";
    subprocess[13] = "du~_emve~g";
    subprocess[14] = "dc~_emve~g";
    subprocess[15] = "su~_emve~g";
    subprocess[16] = "sc~_emve~g";
    subprocess[17] = "bu~_emve~g";
    subprocess[18] = "bc~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "da_emve~c";
    subprocess[3] = "sa_emve~u";
    subprocess[4] = "sa_emve~c";
    subprocess[5] = "ba_emve~u";
    subprocess[6] = "ba_emve~c";
    subprocess[7] = "u~a_emve~d~";
    subprocess[8] = "u~a_emve~s~";
    subprocess[9] = "u~a_emve~b~";
    subprocess[10] = "c~a_emve~d~";
    subprocess[11] = "c~a_emve~s~";
    subprocess[12] = "c~a_emve~b~";
    subprocess[13] = "du~_emve~a";
    subprocess[14] = "dc~_emve~a";
    subprocess[15] = "su~_emve~a";
    subprocess[16] = "sc~_emve~a";
    subprocess[17] = "bu~_emve~a";
    subprocess[18] = "bc~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppenex02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_emve~ud~";
    subprocess[2] = "gg_emve~us~";
    subprocess[3] = "gg_emve~ub~";
    subprocess[4] = "gg_emve~cd~";
    subprocess[5] = "gg_emve~cs~";
    subprocess[6] = "gg_emve~cb~";
    subprocess[7] = "gd_emve~gu";
    subprocess[8] = "gd_emve~gc";
    subprocess[9] = "gs_emve~gu";
    subprocess[10] = "gs_emve~gc";
    subprocess[11] = "gb_emve~gu";
    subprocess[12] = "gb_emve~gc";
    subprocess[13] = "gu~_emve~gd~";
    subprocess[14] = "gu~_emve~gs~";
    subprocess[15] = "gu~_emve~gb~";
    subprocess[16] = "gc~_emve~gd~";
    subprocess[17] = "gc~_emve~gs~";
    subprocess[18] = "gc~_emve~gb~";
    subprocess[19] = "dd_emve~du";
    subprocess[20] = "dd_emve~dc";
    subprocess[21] = "du_emve~uu";
    subprocess[22] = "du_emve~uc";
    subprocess[23] = "ds_emve~du";
    subprocess[24] = "ds_emve~dc";
    subprocess[25] = "ds_emve~us";
    subprocess[26] = "ds_emve~sc";
    subprocess[27] = "dc_emve~uc";
    subprocess[28] = "dc_emve~cc";
    subprocess[29] = "db_emve~du";
    subprocess[30] = "db_emve~dc";
    subprocess[31] = "db_emve~ub";
    subprocess[32] = "db_emve~cb";
    subprocess[33] = "dd~_emve~ud~";
    subprocess[34] = "dd~_emve~us~";
    subprocess[35] = "dd~_emve~ub~";
    subprocess[36] = "dd~_emve~cd~";
    subprocess[37] = "dd~_emve~cs~";
    subprocess[38] = "dd~_emve~cb~";
    subprocess[39] = "du~_emve~gg";
    subprocess[40] = "du~_emve~dd~";
    subprocess[41] = "du~_emve~ds~";
    subprocess[42] = "du~_emve~db~";
    subprocess[43] = "du~_emve~uu~";
    subprocess[44] = "du~_emve~ss~";
    subprocess[45] = "du~_emve~cu~";
    subprocess[46] = "du~_emve~cc~";
    subprocess[47] = "du~_emve~bb~";
    subprocess[48] = "ds~_emve~us~";
    subprocess[49] = "ds~_emve~cs~";
    subprocess[50] = "dc~_emve~gg";
    subprocess[51] = "dc~_emve~dd~";
    subprocess[52] = "dc~_emve~ds~";
    subprocess[53] = "dc~_emve~db~";
    subprocess[54] = "dc~_emve~uu~";
    subprocess[55] = "dc~_emve~uc~";
    subprocess[56] = "dc~_emve~ss~";
    subprocess[57] = "dc~_emve~cc~";
    subprocess[58] = "dc~_emve~bb~";
    subprocess[59] = "db~_emve~ub~";
    subprocess[60] = "db~_emve~cb~";
    subprocess[61] = "us_emve~uu";
    subprocess[62] = "us_emve~uc";
    subprocess[63] = "ub_emve~uu";
    subprocess[64] = "ub_emve~uc";
    subprocess[65] = "uu~_emve~ud~";
    subprocess[66] = "uu~_emve~us~";
    subprocess[67] = "uu~_emve~ub~";
    subprocess[68] = "uu~_emve~cd~";
    subprocess[69] = "uu~_emve~cs~";
    subprocess[70] = "uu~_emve~cb~";
    subprocess[71] = "uc~_emve~ud~";
    subprocess[72] = "uc~_emve~us~";
    subprocess[73] = "uc~_emve~ub~";
    subprocess[74] = "ss_emve~us";
    subprocess[75] = "ss_emve~sc";
    subprocess[76] = "sc_emve~uc";
    subprocess[77] = "sc_emve~cc";
    subprocess[78] = "sb_emve~us";
    subprocess[79] = "sb_emve~ub";
    subprocess[80] = "sb_emve~sc";
    subprocess[81] = "sb_emve~cb";
    subprocess[82] = "sd~_emve~ud~";
    subprocess[83] = "sd~_emve~cd~";
    subprocess[84] = "su~_emve~gg";
    subprocess[85] = "su~_emve~dd~";
    subprocess[86] = "su~_emve~uu~";
    subprocess[87] = "su~_emve~sd~";
    subprocess[88] = "su~_emve~ss~";
    subprocess[89] = "su~_emve~sb~";
    subprocess[90] = "su~_emve~cu~";
    subprocess[91] = "su~_emve~cc~";
    subprocess[92] = "su~_emve~bb~";
    subprocess[93] = "ss~_emve~ud~";
    subprocess[94] = "ss~_emve~us~";
    subprocess[95] = "ss~_emve~ub~";
    subprocess[96] = "ss~_emve~cd~";
    subprocess[97] = "ss~_emve~cs~";
    subprocess[98] = "ss~_emve~cb~";
    subprocess[99] = "sc~_emve~gg";
    subprocess[100] = "sc~_emve~dd~";
    subprocess[101] = "sc~_emve~uu~";
    subprocess[102] = "sc~_emve~uc~";
    subprocess[103] = "sc~_emve~sd~";
    subprocess[104] = "sc~_emve~ss~";
    subprocess[105] = "sc~_emve~sb~";
    subprocess[106] = "sc~_emve~cc~";
    subprocess[107] = "sc~_emve~bb~";
    subprocess[108] = "sb~_emve~ub~";
    subprocess[109] = "sb~_emve~cb~";
    subprocess[110] = "cb_emve~uc";
    subprocess[111] = "cb_emve~cc";
    subprocess[112] = "cu~_emve~cd~";
    subprocess[113] = "cu~_emve~cs~";
    subprocess[114] = "cu~_emve~cb~";
    subprocess[115] = "cc~_emve~ud~";
    subprocess[116] = "cc~_emve~us~";
    subprocess[117] = "cc~_emve~ub~";
    subprocess[118] = "cc~_emve~cd~";
    subprocess[119] = "cc~_emve~cs~";
    subprocess[120] = "cc~_emve~cb~";
    subprocess[121] = "bb_emve~ub";
    subprocess[122] = "bb_emve~cb";
    subprocess[123] = "bd~_emve~ud~";
    subprocess[124] = "bd~_emve~cd~";
    subprocess[125] = "bu~_emve~gg";
    subprocess[126] = "bu~_emve~dd~";
    subprocess[127] = "bu~_emve~uu~";
    subprocess[128] = "bu~_emve~ss~";
    subprocess[129] = "bu~_emve~cu~";
    subprocess[130] = "bu~_emve~cc~";
    subprocess[131] = "bu~_emve~bd~";
    subprocess[132] = "bu~_emve~bs~";
    subprocess[133] = "bu~_emve~bb~";
    subprocess[134] = "bs~_emve~us~";
    subprocess[135] = "bs~_emve~cs~";
    subprocess[136] = "bc~_emve~gg";
    subprocess[137] = "bc~_emve~dd~";
    subprocess[138] = "bc~_emve~uu~";
    subprocess[139] = "bc~_emve~uc~";
    subprocess[140] = "bc~_emve~ss~";
    subprocess[141] = "bc~_emve~cc~";
    subprocess[142] = "bc~_emve~bd~";
    subprocess[143] = "bc~_emve~bs~";
    subprocess[144] = "bc~_emve~bb~";
    subprocess[145] = "bb~_emve~ud~";
    subprocess[146] = "bb~_emve~us~";
    subprocess[147] = "bb~_emve~ub~";
    subprocess[148] = "bb~_emve~cd~";
    subprocess[149] = "bb~_emve~cs~";
    subprocess[150] = "bb~_emve~cb~";
    subprocess[151] = "d~u~_emve~d~d~";
    subprocess[152] = "d~u~_emve~d~s~";
    subprocess[153] = "d~u~_emve~d~b~";
    subprocess[154] = "d~c~_emve~d~d~";
    subprocess[155] = "d~c~_emve~d~s~";
    subprocess[156] = "d~c~_emve~d~b~";
    subprocess[157] = "u~u~_emve~d~u~";
    subprocess[158] = "u~u~_emve~u~s~";
    subprocess[159] = "u~u~_emve~u~b~";
    subprocess[160] = "u~s~_emve~d~s~";
    subprocess[161] = "u~s~_emve~s~s~";
    subprocess[162] = "u~s~_emve~s~b~";
    subprocess[163] = "u~c~_emve~d~u~";
    subprocess[164] = "u~c~_emve~d~c~";
    subprocess[165] = "u~c~_emve~u~s~";
    subprocess[166] = "u~c~_emve~u~b~";
    subprocess[167] = "u~c~_emve~s~c~";
    subprocess[168] = "u~c~_emve~c~b~";
    subprocess[169] = "u~b~_emve~d~b~";
    subprocess[170] = "u~b~_emve~s~b~";
    subprocess[171] = "u~b~_emve~b~b~";
    subprocess[172] = "s~c~_emve~d~s~";
    subprocess[173] = "s~c~_emve~s~s~";
    subprocess[174] = "s~c~_emve~s~b~";
    subprocess[175] = "c~c~_emve~d~c~";
    subprocess[176] = "c~c~_emve~s~c~";
    subprocess[177] = "c~c~_emve~c~b~";
    subprocess[178] = "c~b~_emve~d~b~";
    subprocess[179] = "c~b~_emve~s~b~";
    subprocess[180] = "c~b~_emve~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
