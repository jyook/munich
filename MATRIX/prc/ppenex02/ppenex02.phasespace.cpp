#include "header.hpp"

#include "ppenex02.phasespace.set.hpp"

ppenex02_phasespace_set::~ppenex02_phasespace_set(){
  static Logger logger("ppenex02_phasespace_set::~ppenex02_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::optimize_minv_born(){
  static Logger logger("ppenex02_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppenex02_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppenex02_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_020_dux_emvex(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_020_dcx_emvex(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_020_sux_emvex(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_020_scx_emvex(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_020_bux_emvex(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_020_bcx_emvex(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppenex02_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_020_dux_emvex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_020_dcx_emvex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_020_sux_emvex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_020_scx_emvex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_020_bux_emvex(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_020_bcx_emvex(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppenex02_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_020_dux_emvex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_020_dcx_emvex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_020_sux_emvex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_020_scx_emvex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_020_bux_emvex(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_020_bcx_emvex(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::optimize_minv_real(){
  static Logger logger("ppenex02_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppenex02_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 12;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 28;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 33){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 33){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppenex02_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_120_gd_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_120_gd_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_120_gs_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_120_gs_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_120_gb_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_120_gb_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_120_gux_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_120_gux_emvexsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_120_gux_emvexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_120_gcx_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_120_gcx_emvexsx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_120_gcx_emvexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_120_dux_emvexg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_120_dcx_emvexg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_120_sux_emvexg(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_120_scx_emvexg(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_120_bux_emvexg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_120_bcx_emvexg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_030_da_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_030_da_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_030_sa_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_030_sa_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_030_ba_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_030_ba_emvexc(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_030_uxa_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_030_uxa_emvexsx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_030_uxa_emvexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_030_cxa_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_030_cxa_emvexsx(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_030_cxa_emvexbx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_030_dux_emvexa(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_030_dcx_emvexa(x_a);}
    else if (csi->no_process_parton[x_a] == 33){ax_psp_030_sux_emvexa(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_030_scx_emvexa(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_030_bux_emvexa(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_030_bcx_emvexa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppenex02_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_120_gd_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_120_gd_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_120_gs_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_120_gs_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_120_gb_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_120_gb_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_120_gux_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_120_gux_emvexsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_120_gux_emvexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_120_gcx_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_120_gcx_emvexsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_120_gcx_emvexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_120_dux_emvexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_120_dcx_emvexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_120_sux_emvexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_120_scx_emvexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_120_bux_emvexg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_120_bcx_emvexg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_030_da_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_030_da_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_030_sa_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_030_sa_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_030_ba_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_030_ba_emvexc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_030_uxa_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_030_uxa_emvexsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_030_uxa_emvexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_030_cxa_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_030_cxa_emvexsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_030_cxa_emvexbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_030_dux_emvexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_030_dcx_emvexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 33){ac_psp_030_sux_emvexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_030_scx_emvexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_030_bux_emvexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_030_bcx_emvexa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppenex02_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_120_gd_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_120_gd_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_120_gs_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_120_gs_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_120_gb_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_120_gb_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_120_gux_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_120_gux_emvexsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_120_gux_emvexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_120_gcx_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_120_gcx_emvexsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_120_gcx_emvexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_120_dux_emvexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_120_dcx_emvexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_120_sux_emvexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_120_scx_emvexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_120_bux_emvexg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_120_bcx_emvexg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_030_da_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_030_da_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_030_sa_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_030_sa_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_030_ba_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_030_ba_emvexc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_030_uxa_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_030_uxa_emvexsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_030_uxa_emvexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_030_cxa_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_030_cxa_emvexsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_030_cxa_emvexbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_030_dux_emvexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_030_dcx_emvexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 33){ag_psp_030_sux_emvexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_030_scx_emvexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_030_bux_emvexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_030_bcx_emvexa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppenex02_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppenex02_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==   0){n_channel = 60;}
    else if (csi->no_process_parton[x_a] ==   1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  11){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  13){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  14){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  16){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  17){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  18){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  26){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  32){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  34){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  35){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  36){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  38){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  41){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  42){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  43){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  44){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  46){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  48){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  49){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  50){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  51){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  52){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  53){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  54){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  55){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  56){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  57){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  58){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  59){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  62){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  64){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  65){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  68){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  70){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  73){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  75){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  76){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  77){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  78){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  79){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  80){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  82){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  85){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  88){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  91){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  94){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  97){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  98){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 100){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 101){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 103){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 104){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 105){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 106){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 107){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 108){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 109){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 110){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 111){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 117){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 119){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 122){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 123){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 126){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 127){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 128){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 129){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 130){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 133){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 136){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 137){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 140){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 142){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 143){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 144){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 145){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 146){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 149){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 150){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 151){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 152){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 153){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 154){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 155){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 156){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 157){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 160){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 161){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 162){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 163){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 164){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 166){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 169){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 172){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 175){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 179){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 180){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 184){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 185){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 186){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 187){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 188){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 189){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 190){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 191){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 192){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 196){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 198){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 199){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 202){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 205){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 206){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 209){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 212){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 214){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 215){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 216){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 217){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 218){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 220){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 223){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 225){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 226){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 229){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 230){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 232){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 235){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 236){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 237){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 238){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 239){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 240){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 241){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 242){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 243){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 244){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 247){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 248){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 249){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 253){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 254){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 255){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 259){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 261){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 262){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 266){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 268){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 269){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 271){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 272){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 273){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 274){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 275){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 276){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 279){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 281){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 282){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 287){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 289){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 290){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 293){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 296){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 297){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 300){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 302){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 303){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  46){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  49){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  57){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  62){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  64){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  68){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  70){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  73){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  75){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  76){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  77){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  78){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  79){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  80){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  82){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  85){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  88){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  91){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  94){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  97){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  98){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 100){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 101){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 103){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 104){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 105){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 106){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 107){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 108){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 109){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 110){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 111){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 117){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 119){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 122){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 123){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 126){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 127){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 128){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 129){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 130){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 133){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 136){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 137){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 140){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 142){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 143){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 144){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 145){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 146){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 149){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 150){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 151){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 152){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 153){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 154){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 155){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 156){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 157){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 160){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 161){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 162){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 163){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 164){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 166){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 169){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 172){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 175){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 179){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 180){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 184){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 185){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 186){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 187){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 188){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 189){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 190){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 191){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 192){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 196){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 198){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 199){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 202){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 205){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 206){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 209){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 212){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 214){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 215){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 216){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 217){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 218){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 220){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 223){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 225){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 226){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 229){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 230){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 232){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 235){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 236){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 237){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 238){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 239){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 240){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 241){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 242){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 243){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 244){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 247){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 248){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 249){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 253){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 254){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 255){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 259){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 261){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 262){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 266){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 268){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 269){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 271){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 272){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 273){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 274){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 275){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 276){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 279){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 281){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 282){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 287){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 289){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 290){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 293){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 296){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 297){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 300){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 302){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 303){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppenex02_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ax_psp_220_gg_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] ==   2){ax_psp_220_gg_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] ==   3){ax_psp_220_gg_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] ==   4){ax_psp_220_gg_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] ==   5){ax_psp_220_gg_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==   6){ax_psp_220_gg_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==   7){ax_psp_220_gd_emvexgu(x_a);}
    else if (csi->no_process_parton[x_a] ==   8){ax_psp_220_gd_emvexgc(x_a);}
    else if (csi->no_process_parton[x_a] ==   9){ax_psp_220_gs_emvexgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  10){ax_psp_220_gs_emvexgc(x_a);}
    else if (csi->no_process_parton[x_a] ==  11){ax_psp_220_gb_emvexgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  12){ax_psp_220_gb_emvexgc(x_a);}
    else if (csi->no_process_parton[x_a] ==  13){ax_psp_220_gux_emvexgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  14){ax_psp_220_gux_emvexgsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  15){ax_psp_220_gux_emvexgbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  16){ax_psp_220_gcx_emvexgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  17){ax_psp_220_gcx_emvexgsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  18){ax_psp_220_gcx_emvexgbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  25){ax_psp_220_dd_emvexdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  26){ax_psp_220_dd_emvexdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  31){ax_psp_220_du_emvexuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  32){ax_psp_220_du_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  34){ax_psp_220_ds_emvexdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  35){ax_psp_220_ds_emvexdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  36){ax_psp_220_ds_emvexus(x_a);}
    else if (csi->no_process_parton[x_a] ==  38){ax_psp_220_ds_emvexsc(x_a);}
    else if (csi->no_process_parton[x_a] ==  41){ax_psp_220_dc_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  42){ax_psp_220_dc_emvexcc(x_a);}
    else if (csi->no_process_parton[x_a] ==  43){ax_psp_220_db_emvexdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  44){ax_psp_220_db_emvexdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  46){ax_psp_220_db_emvexub(x_a);}
    else if (csi->no_process_parton[x_a] ==  48){ax_psp_220_db_emvexcb(x_a);}
    else if (csi->no_process_parton[x_a] ==  49){ax_psp_220_ddx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  50){ax_psp_220_ddx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] ==  51){ax_psp_220_ddx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] ==  52){ax_psp_220_ddx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  53){ax_psp_220_ddx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  54){ax_psp_220_ddx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  55){ax_psp_220_dux_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  56){ax_psp_220_dux_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  57){ax_psp_220_dux_emvexdsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  58){ax_psp_220_dux_emvexdbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  59){ax_psp_220_dux_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  62){ax_psp_220_dux_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  64){ax_psp_220_dux_emvexcux(x_a);}
    else if (csi->no_process_parton[x_a] ==  65){ax_psp_220_dux_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  68){ax_psp_220_dux_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  70){ax_psp_220_dsx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] ==  73){ax_psp_220_dsx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  75){ax_psp_220_dcx_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  76){ax_psp_220_dcx_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  77){ax_psp_220_dcx_emvexdsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  78){ax_psp_220_dcx_emvexdbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  79){ax_psp_220_dcx_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  80){ax_psp_220_dcx_emvexucx(x_a);}
    else if (csi->no_process_parton[x_a] ==  82){ax_psp_220_dcx_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  85){ax_psp_220_dcx_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  88){ax_psp_220_dcx_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  91){ax_psp_220_dbx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] ==  94){ax_psp_220_dbx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  97){ax_psp_220_us_emvexuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  98){ax_psp_220_us_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] == 100){ax_psp_220_ub_emvexuu(x_a);}
    else if (csi->no_process_parton[x_a] == 101){ax_psp_220_ub_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] == 103){ax_psp_220_uux_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 104){ax_psp_220_uux_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 105){ax_psp_220_uux_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 106){ax_psp_220_uux_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 107){ax_psp_220_uux_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 108){ax_psp_220_uux_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 109){ax_psp_220_ucx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 110){ax_psp_220_ucx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 111){ax_psp_220_ucx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 117){ax_psp_220_ss_emvexus(x_a);}
    else if (csi->no_process_parton[x_a] == 119){ax_psp_220_ss_emvexsc(x_a);}
    else if (csi->no_process_parton[x_a] == 122){ax_psp_220_sc_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] == 123){ax_psp_220_sc_emvexcc(x_a);}
    else if (csi->no_process_parton[x_a] == 126){ax_psp_220_sb_emvexus(x_a);}
    else if (csi->no_process_parton[x_a] == 127){ax_psp_220_sb_emvexub(x_a);}
    else if (csi->no_process_parton[x_a] == 128){ax_psp_220_sb_emvexsc(x_a);}
    else if (csi->no_process_parton[x_a] == 129){ax_psp_220_sb_emvexcb(x_a);}
    else if (csi->no_process_parton[x_a] == 130){ax_psp_220_sdx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 133){ax_psp_220_sdx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 136){ax_psp_220_sux_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 137){ax_psp_220_sux_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 140){ax_psp_220_sux_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 142){ax_psp_220_sux_emvexsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 143){ax_psp_220_sux_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 144){ax_psp_220_sux_emvexsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 145){ax_psp_220_sux_emvexcux(x_a);}
    else if (csi->no_process_parton[x_a] == 146){ax_psp_220_sux_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 149){ax_psp_220_sux_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 150){ax_psp_220_ssx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 151){ax_psp_220_ssx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 152){ax_psp_220_ssx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 153){ax_psp_220_ssx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 154){ax_psp_220_ssx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 155){ax_psp_220_ssx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 156){ax_psp_220_scx_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 157){ax_psp_220_scx_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 160){ax_psp_220_scx_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 161){ax_psp_220_scx_emvexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 162){ax_psp_220_scx_emvexsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 163){ax_psp_220_scx_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 164){ax_psp_220_scx_emvexsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 166){ax_psp_220_scx_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 169){ax_psp_220_scx_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 172){ax_psp_220_sbx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 175){ax_psp_220_sbx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 179){ax_psp_220_cb_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] == 180){ax_psp_220_cb_emvexcc(x_a);}
    else if (csi->no_process_parton[x_a] == 184){ax_psp_220_cux_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 185){ax_psp_220_cux_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 186){ax_psp_220_cux_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 187){ax_psp_220_ccx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 188){ax_psp_220_ccx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 189){ax_psp_220_ccx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 190){ax_psp_220_ccx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 191){ax_psp_220_ccx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 192){ax_psp_220_ccx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 196){ax_psp_220_bb_emvexub(x_a);}
    else if (csi->no_process_parton[x_a] == 198){ax_psp_220_bb_emvexcb(x_a);}
    else if (csi->no_process_parton[x_a] == 199){ax_psp_220_bdx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 202){ax_psp_220_bdx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 205){ax_psp_220_bux_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 206){ax_psp_220_bux_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 209){ax_psp_220_bux_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 212){ax_psp_220_bux_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 214){ax_psp_220_bux_emvexcux(x_a);}
    else if (csi->no_process_parton[x_a] == 215){ax_psp_220_bux_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 216){ax_psp_220_bux_emvexbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 217){ax_psp_220_bux_emvexbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 218){ax_psp_220_bux_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 220){ax_psp_220_bsx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 223){ax_psp_220_bsx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 225){ax_psp_220_bcx_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 226){ax_psp_220_bcx_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 229){ax_psp_220_bcx_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 230){ax_psp_220_bcx_emvexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 232){ax_psp_220_bcx_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 235){ax_psp_220_bcx_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 236){ax_psp_220_bcx_emvexbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 237){ax_psp_220_bcx_emvexbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 238){ax_psp_220_bcx_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 239){ax_psp_220_bbx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 240){ax_psp_220_bbx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 241){ax_psp_220_bbx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 242){ax_psp_220_bbx_emvexcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 243){ax_psp_220_bbx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 244){ax_psp_220_bbx_emvexcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 247){ax_psp_220_dxux_emvexdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 248){ax_psp_220_dxux_emvexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 249){ax_psp_220_dxux_emvexdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 253){ax_psp_220_dxcx_emvexdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 254){ax_psp_220_dxcx_emvexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 255){ax_psp_220_dxcx_emvexdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 259){ax_psp_220_uxux_emvexdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 261){ax_psp_220_uxux_emvexuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 262){ax_psp_220_uxux_emvexuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 266){ax_psp_220_uxsx_emvexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 268){ax_psp_220_uxsx_emvexsxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 269){ax_psp_220_uxsx_emvexsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 271){ax_psp_220_uxcx_emvexdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 272){ax_psp_220_uxcx_emvexdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 273){ax_psp_220_uxcx_emvexuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 274){ax_psp_220_uxcx_emvexuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 275){ax_psp_220_uxcx_emvexsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 276){ax_psp_220_uxcx_emvexcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 279){ax_psp_220_uxbx_emvexdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 281){ax_psp_220_uxbx_emvexsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 282){ax_psp_220_uxbx_emvexbxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 287){ax_psp_220_sxcx_emvexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 289){ax_psp_220_sxcx_emvexsxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 290){ax_psp_220_sxcx_emvexsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 293){ax_psp_220_cxcx_emvexdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 296){ax_psp_220_cxcx_emvexsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 297){ax_psp_220_cxcx_emvexcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 300){ax_psp_220_cxbx_emvexdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 302){ax_psp_220_cxbx_emvexsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 303){ax_psp_220_cxbx_emvexbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppenex02_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ac_psp_220_gg_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   2){ac_psp_220_gg_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   3){ac_psp_220_gg_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   4){ac_psp_220_gg_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   5){ac_psp_220_gg_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   6){ac_psp_220_gg_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   7){ac_psp_220_gd_emvexgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   8){ac_psp_220_gd_emvexgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   9){ac_psp_220_gs_emvexgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  10){ac_psp_220_gs_emvexgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  11){ac_psp_220_gb_emvexgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  12){ac_psp_220_gb_emvexgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  13){ac_psp_220_gux_emvexgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  14){ac_psp_220_gux_emvexgsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  15){ac_psp_220_gux_emvexgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  16){ac_psp_220_gcx_emvexgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  17){ac_psp_220_gcx_emvexgsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  18){ac_psp_220_gcx_emvexgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  25){ac_psp_220_dd_emvexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  26){ac_psp_220_dd_emvexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  31){ac_psp_220_du_emvexuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  32){ac_psp_220_du_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  34){ac_psp_220_ds_emvexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  35){ac_psp_220_ds_emvexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  36){ac_psp_220_ds_emvexus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  38){ac_psp_220_ds_emvexsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  41){ac_psp_220_dc_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  42){ac_psp_220_dc_emvexcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  43){ac_psp_220_db_emvexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  44){ac_psp_220_db_emvexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  46){ac_psp_220_db_emvexub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  48){ac_psp_220_db_emvexcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  49){ac_psp_220_ddx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  50){ac_psp_220_ddx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  51){ac_psp_220_ddx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  52){ac_psp_220_ddx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  53){ac_psp_220_ddx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  54){ac_psp_220_ddx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  55){ac_psp_220_dux_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  56){ac_psp_220_dux_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  57){ac_psp_220_dux_emvexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  58){ac_psp_220_dux_emvexdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  59){ac_psp_220_dux_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  62){ac_psp_220_dux_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  64){ac_psp_220_dux_emvexcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  65){ac_psp_220_dux_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  68){ac_psp_220_dux_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  70){ac_psp_220_dsx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  73){ac_psp_220_dsx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  75){ac_psp_220_dcx_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  76){ac_psp_220_dcx_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  77){ac_psp_220_dcx_emvexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  78){ac_psp_220_dcx_emvexdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  79){ac_psp_220_dcx_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  80){ac_psp_220_dcx_emvexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  82){ac_psp_220_dcx_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  85){ac_psp_220_dcx_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  88){ac_psp_220_dcx_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  91){ac_psp_220_dbx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  94){ac_psp_220_dbx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  97){ac_psp_220_us_emvexuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  98){ac_psp_220_us_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 100){ac_psp_220_ub_emvexuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 101){ac_psp_220_ub_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 103){ac_psp_220_uux_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 104){ac_psp_220_uux_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 105){ac_psp_220_uux_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 106){ac_psp_220_uux_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 107){ac_psp_220_uux_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 108){ac_psp_220_uux_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 109){ac_psp_220_ucx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 110){ac_psp_220_ucx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 111){ac_psp_220_ucx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 117){ac_psp_220_ss_emvexus(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 119){ac_psp_220_ss_emvexsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 122){ac_psp_220_sc_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 123){ac_psp_220_sc_emvexcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 126){ac_psp_220_sb_emvexus(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 127){ac_psp_220_sb_emvexub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 128){ac_psp_220_sb_emvexsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 129){ac_psp_220_sb_emvexcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 130){ac_psp_220_sdx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 133){ac_psp_220_sdx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 136){ac_psp_220_sux_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 137){ac_psp_220_sux_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 140){ac_psp_220_sux_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 142){ac_psp_220_sux_emvexsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 143){ac_psp_220_sux_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 144){ac_psp_220_sux_emvexsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 145){ac_psp_220_sux_emvexcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 146){ac_psp_220_sux_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 149){ac_psp_220_sux_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 150){ac_psp_220_ssx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 151){ac_psp_220_ssx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 152){ac_psp_220_ssx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 153){ac_psp_220_ssx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 154){ac_psp_220_ssx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 155){ac_psp_220_ssx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 156){ac_psp_220_scx_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 157){ac_psp_220_scx_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 160){ac_psp_220_scx_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 161){ac_psp_220_scx_emvexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 162){ac_psp_220_scx_emvexsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 163){ac_psp_220_scx_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 164){ac_psp_220_scx_emvexsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 166){ac_psp_220_scx_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 169){ac_psp_220_scx_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 172){ac_psp_220_sbx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 175){ac_psp_220_sbx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 179){ac_psp_220_cb_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 180){ac_psp_220_cb_emvexcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 184){ac_psp_220_cux_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 185){ac_psp_220_cux_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 186){ac_psp_220_cux_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 187){ac_psp_220_ccx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 188){ac_psp_220_ccx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 189){ac_psp_220_ccx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 190){ac_psp_220_ccx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 191){ac_psp_220_ccx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 192){ac_psp_220_ccx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 196){ac_psp_220_bb_emvexub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 198){ac_psp_220_bb_emvexcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 199){ac_psp_220_bdx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 202){ac_psp_220_bdx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 205){ac_psp_220_bux_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 206){ac_psp_220_bux_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 209){ac_psp_220_bux_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 212){ac_psp_220_bux_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 214){ac_psp_220_bux_emvexcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 215){ac_psp_220_bux_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 216){ac_psp_220_bux_emvexbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 217){ac_psp_220_bux_emvexbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 218){ac_psp_220_bux_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 220){ac_psp_220_bsx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 223){ac_psp_220_bsx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 225){ac_psp_220_bcx_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 226){ac_psp_220_bcx_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 229){ac_psp_220_bcx_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 230){ac_psp_220_bcx_emvexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 232){ac_psp_220_bcx_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 235){ac_psp_220_bcx_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 236){ac_psp_220_bcx_emvexbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 237){ac_psp_220_bcx_emvexbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 238){ac_psp_220_bcx_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 239){ac_psp_220_bbx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 240){ac_psp_220_bbx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 241){ac_psp_220_bbx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 242){ac_psp_220_bbx_emvexcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 243){ac_psp_220_bbx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 244){ac_psp_220_bbx_emvexcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 247){ac_psp_220_dxux_emvexdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 248){ac_psp_220_dxux_emvexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 249){ac_psp_220_dxux_emvexdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 253){ac_psp_220_dxcx_emvexdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 254){ac_psp_220_dxcx_emvexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 255){ac_psp_220_dxcx_emvexdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 259){ac_psp_220_uxux_emvexdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 261){ac_psp_220_uxux_emvexuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 262){ac_psp_220_uxux_emvexuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 266){ac_psp_220_uxsx_emvexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 268){ac_psp_220_uxsx_emvexsxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 269){ac_psp_220_uxsx_emvexsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 271){ac_psp_220_uxcx_emvexdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 272){ac_psp_220_uxcx_emvexdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 273){ac_psp_220_uxcx_emvexuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 274){ac_psp_220_uxcx_emvexuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 275){ac_psp_220_uxcx_emvexsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 276){ac_psp_220_uxcx_emvexcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 279){ac_psp_220_uxbx_emvexdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 281){ac_psp_220_uxbx_emvexsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 282){ac_psp_220_uxbx_emvexbxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 287){ac_psp_220_sxcx_emvexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 289){ac_psp_220_sxcx_emvexsxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 290){ac_psp_220_sxcx_emvexsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 293){ac_psp_220_cxcx_emvexdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 296){ac_psp_220_cxcx_emvexsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 297){ac_psp_220_cxcx_emvexcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 300){ac_psp_220_cxbx_emvexdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 302){ac_psp_220_cxbx_emvexsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 303){ac_psp_220_cxbx_emvexbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppenex02_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ag_psp_220_gg_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   2){ag_psp_220_gg_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   3){ag_psp_220_gg_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   4){ag_psp_220_gg_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   5){ag_psp_220_gg_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   6){ag_psp_220_gg_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   7){ag_psp_220_gd_emvexgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   8){ag_psp_220_gd_emvexgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   9){ag_psp_220_gs_emvexgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  10){ag_psp_220_gs_emvexgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  11){ag_psp_220_gb_emvexgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  12){ag_psp_220_gb_emvexgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  13){ag_psp_220_gux_emvexgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  14){ag_psp_220_gux_emvexgsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  15){ag_psp_220_gux_emvexgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  16){ag_psp_220_gcx_emvexgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  17){ag_psp_220_gcx_emvexgsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  18){ag_psp_220_gcx_emvexgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  25){ag_psp_220_dd_emvexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  26){ag_psp_220_dd_emvexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  31){ag_psp_220_du_emvexuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  32){ag_psp_220_du_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  34){ag_psp_220_ds_emvexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  35){ag_psp_220_ds_emvexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  36){ag_psp_220_ds_emvexus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  38){ag_psp_220_ds_emvexsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  41){ag_psp_220_dc_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  42){ag_psp_220_dc_emvexcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  43){ag_psp_220_db_emvexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  44){ag_psp_220_db_emvexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  46){ag_psp_220_db_emvexub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  48){ag_psp_220_db_emvexcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  49){ag_psp_220_ddx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  50){ag_psp_220_ddx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  51){ag_psp_220_ddx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  52){ag_psp_220_ddx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  53){ag_psp_220_ddx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  54){ag_psp_220_ddx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  55){ag_psp_220_dux_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  56){ag_psp_220_dux_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  57){ag_psp_220_dux_emvexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  58){ag_psp_220_dux_emvexdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  59){ag_psp_220_dux_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  62){ag_psp_220_dux_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  64){ag_psp_220_dux_emvexcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  65){ag_psp_220_dux_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  68){ag_psp_220_dux_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  70){ag_psp_220_dsx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  73){ag_psp_220_dsx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  75){ag_psp_220_dcx_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  76){ag_psp_220_dcx_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  77){ag_psp_220_dcx_emvexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  78){ag_psp_220_dcx_emvexdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  79){ag_psp_220_dcx_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  80){ag_psp_220_dcx_emvexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  82){ag_psp_220_dcx_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  85){ag_psp_220_dcx_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  88){ag_psp_220_dcx_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  91){ag_psp_220_dbx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  94){ag_psp_220_dbx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  97){ag_psp_220_us_emvexuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  98){ag_psp_220_us_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 100){ag_psp_220_ub_emvexuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 101){ag_psp_220_ub_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 103){ag_psp_220_uux_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 104){ag_psp_220_uux_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 105){ag_psp_220_uux_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 106){ag_psp_220_uux_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 107){ag_psp_220_uux_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 108){ag_psp_220_uux_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 109){ag_psp_220_ucx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 110){ag_psp_220_ucx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 111){ag_psp_220_ucx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 117){ag_psp_220_ss_emvexus(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 119){ag_psp_220_ss_emvexsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 122){ag_psp_220_sc_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 123){ag_psp_220_sc_emvexcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 126){ag_psp_220_sb_emvexus(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 127){ag_psp_220_sb_emvexub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 128){ag_psp_220_sb_emvexsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 129){ag_psp_220_sb_emvexcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 130){ag_psp_220_sdx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 133){ag_psp_220_sdx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 136){ag_psp_220_sux_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 137){ag_psp_220_sux_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 140){ag_psp_220_sux_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 142){ag_psp_220_sux_emvexsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 143){ag_psp_220_sux_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 144){ag_psp_220_sux_emvexsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 145){ag_psp_220_sux_emvexcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 146){ag_psp_220_sux_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 149){ag_psp_220_sux_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 150){ag_psp_220_ssx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 151){ag_psp_220_ssx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 152){ag_psp_220_ssx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 153){ag_psp_220_ssx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 154){ag_psp_220_ssx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 155){ag_psp_220_ssx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 156){ag_psp_220_scx_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 157){ag_psp_220_scx_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 160){ag_psp_220_scx_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 161){ag_psp_220_scx_emvexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 162){ag_psp_220_scx_emvexsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 163){ag_psp_220_scx_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 164){ag_psp_220_scx_emvexsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 166){ag_psp_220_scx_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 169){ag_psp_220_scx_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 172){ag_psp_220_sbx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 175){ag_psp_220_sbx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 179){ag_psp_220_cb_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 180){ag_psp_220_cb_emvexcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 184){ag_psp_220_cux_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 185){ag_psp_220_cux_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 186){ag_psp_220_cux_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 187){ag_psp_220_ccx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 188){ag_psp_220_ccx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 189){ag_psp_220_ccx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 190){ag_psp_220_ccx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 191){ag_psp_220_ccx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 192){ag_psp_220_ccx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 196){ag_psp_220_bb_emvexub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 198){ag_psp_220_bb_emvexcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 199){ag_psp_220_bdx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 202){ag_psp_220_bdx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 205){ag_psp_220_bux_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 206){ag_psp_220_bux_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 209){ag_psp_220_bux_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 212){ag_psp_220_bux_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 214){ag_psp_220_bux_emvexcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 215){ag_psp_220_bux_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 216){ag_psp_220_bux_emvexbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 217){ag_psp_220_bux_emvexbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 218){ag_psp_220_bux_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 220){ag_psp_220_bsx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 223){ag_psp_220_bsx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 225){ag_psp_220_bcx_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 226){ag_psp_220_bcx_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 229){ag_psp_220_bcx_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 230){ag_psp_220_bcx_emvexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 232){ag_psp_220_bcx_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 235){ag_psp_220_bcx_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 236){ag_psp_220_bcx_emvexbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 237){ag_psp_220_bcx_emvexbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 238){ag_psp_220_bcx_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 239){ag_psp_220_bbx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 240){ag_psp_220_bbx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 241){ag_psp_220_bbx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 242){ag_psp_220_bbx_emvexcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 243){ag_psp_220_bbx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 244){ag_psp_220_bbx_emvexcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 247){ag_psp_220_dxux_emvexdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 248){ag_psp_220_dxux_emvexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 249){ag_psp_220_dxux_emvexdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 253){ag_psp_220_dxcx_emvexdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 254){ag_psp_220_dxcx_emvexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 255){ag_psp_220_dxcx_emvexdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 259){ag_psp_220_uxux_emvexdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 261){ag_psp_220_uxux_emvexuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 262){ag_psp_220_uxux_emvexuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 266){ag_psp_220_uxsx_emvexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 268){ag_psp_220_uxsx_emvexsxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 269){ag_psp_220_uxsx_emvexsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 271){ag_psp_220_uxcx_emvexdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 272){ag_psp_220_uxcx_emvexdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 273){ag_psp_220_uxcx_emvexuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 274){ag_psp_220_uxcx_emvexuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 275){ag_psp_220_uxcx_emvexsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 276){ag_psp_220_uxcx_emvexcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 279){ag_psp_220_uxbx_emvexdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 281){ag_psp_220_uxbx_emvexsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 282){ag_psp_220_uxbx_emvexbxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 287){ag_psp_220_sxcx_emvexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 289){ag_psp_220_sxcx_emvexsxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 290){ag_psp_220_sxcx_emvexsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 293){ag_psp_220_cxcx_emvexdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 296){ag_psp_220_cxcx_emvexsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 297){ag_psp_220_cxcx_emvexcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 300){ag_psp_220_cxbx_emvexdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 302){ag_psp_220_cxbx_emvexsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 303){ag_psp_220_cxbx_emvexbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
