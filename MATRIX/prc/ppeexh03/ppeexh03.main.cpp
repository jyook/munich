#include "header.hpp"

#include "ppllh03.amplitude.set.hpp"

#include "ppeexh03.contribution.set.hpp"
#include "ppeexh03.event.set.hpp"
#include "ppeexh03.phasespace.set.hpp"
#include "ppeexh03.observable.set.hpp"
#include "ppeexh03.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emeph+X");

  MUC->csi = new ppeexh03_contribution_set();
  MUC->esi = new ppeexh03_event_set();
  MUC->psi = new ppeexh03_phasespace_set();
  MUC->osi = new ppeexh03_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllh03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppeexh03_phasespace_set();
      MUC->psi->fake_psi->csi = new ppeexh03_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppeexh03_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
