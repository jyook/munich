logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_M_leplep = USERSWITCH("M_leplep");
  static double cut_min_M_leplep = USERCUT("min_M_leplep");
  static double cut_min_M2_leplep = pow(cut_min_M_leplep, 2);
  static double cut_max_M_leplep = USERCUT("max_M_leplep");
  static double cut_max_M2_leplep = pow(cut_max_M_leplep, 2);

  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);
  
  static int switch_cut_R_lepjet = USERSWITCH("R_lepjet");
  static double cut_min_R_lepjet = USERCUT("min_R_lepjet");
  static double cut_min_R2_lepjet = pow(cut_min_R_lepjet, 2);
  
  static int switch_cut_pT_lep_1st = USERSWITCH("pT_lep_1st");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");



  // cut on hardest (highest-pT) lepton
  if (switch_cut_pT_lep_1st == 1){
    double temp_pT_lep_1st = PARTICLE("lep")[0].pT;
    if (temp_pT_lep_1st < cut_min_pT_lep_1st){
      cut_ps[i_a] = -1;
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_1st << " < cut_min_pT_lep_1st = " << cut_min_pT_lep_1st << endl;
      return;
    }
  }



  // lepton--lepton invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep = " << switch_cut_M_leplep << endl;}
  if (switch_cut_M_leplep == 1){
    double M2_leplep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum).m2();

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_leplep << endl;}

    if (M2_leplep < cut_min_M2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep min cut applied" << endl; 
      return;
    }

    if (switch_output_cutinfo){if (cut_max_M2_leplep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " < " << cut_max_M2_leplep << endl;}}

    if (cut_max_M2_leplep != 0 && M2_leplep > cut_max_M2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep max cut applied" << endl; 
      return;
    }
  }



  // lepton--lepton isolation cuts
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_leplep = " << switch_cut_R_leplep << endl; }
  if (switch_cut_R_leplep == 1){
    double R2_eta_leplep = R2_eta(PARTICLE("lep")[0], PARTICLE("lep")[1]);

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_leplep = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}

    if (R2_eta_leplep < cut_min_R2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_leplep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_R_leplep cut applied" << endl; 
      return;
    }
  }



  for (int i_j = 0; i_j < NUMBER("jet"); i_j++){
    // lepton--jet isolation cuts
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_lepjet = " << switch_cut_R_lepjet << endl;}
    if (switch_cut_R_lepjet == 1){
      for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
	double R2_eta_lepjet = R2_eta(PARTICLE("lep")[i_l], PARTICLE("jet")[i_j]);

	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(jet)[" << i_j << "] = " << PARTICLE("jet")[i_j].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_lepjet = " << R2_eta_lepjet << " > " << cut_min_R2_lepjet << endl;}

	if (R2_eta_lepjet < cut_min_R2_lepjet){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_lepjet" << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepjet cut applied" << endl; 
	  return;
	}
      }
    }
  }
    
  if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}

}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;
