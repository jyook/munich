#include "header.hpp"

#include "ppeexh03.summary.hpp"

ppeexh03_summary_generic::ppeexh03_summary_generic(munich * xmunich){
  Logger logger("ppeexh03_summary_generic::ppeexh03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeexh03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeexh03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeexh03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeexh03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeexh03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeexh03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_born(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emeph";
    subprocess[2] = "uu~_emeph";
    subprocess[3] = "bb~_emeph";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emeph";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emephd";
    subprocess[2] = "gu_emephu";
    subprocess[3] = "gb_emephb";
    subprocess[4] = "gd~_emephd~";
    subprocess[5] = "gu~_emephu~";
    subprocess[6] = "gb~_emephb~";
    subprocess[7] = "dd~_emephg";
    subprocess[8] = "uu~_emephg";
    subprocess[9] = "bb~_emephg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emephg";
    subprocess[2] = "gd_emephd";
    subprocess[3] = "gu_emephu";
    subprocess[4] = "gb_emephb";
    subprocess[5] = "gd~_emephd~";
    subprocess[6] = "gu~_emephu~";
    subprocess[7] = "gb~_emephb~";
    subprocess[8] = "dd~_emephg";
    subprocess[9] = "uu~_emephg";
    subprocess[10] = "bb~_emephg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emephd";
    subprocess[2] = "ua_emephu";
    subprocess[3] = "ba_emephb";
    subprocess[4] = "d~a_emephd~";
    subprocess[5] = "u~a_emephu~";
    subprocess[6] = "b~a_emephb~";
    subprocess[7] = "dd~_emepha";
    subprocess[8] = "uu~_emepha";
    subprocess[9] = "bb~_emepha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emephd";
    subprocess[2] = "gu_emephu";
    subprocess[3] = "gb_emephb";
    subprocess[4] = "gd~_emephd~";
    subprocess[5] = "gu~_emephu~";
    subprocess[6] = "gb~_emephb~";
    subprocess[7] = "dd~_emephg";
    subprocess[8] = "uu~_emephg";
    subprocess[9] = "bb~_emephg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emephd";
    subprocess[2] = "ua_emephu";
    subprocess[3] = "ba_emephb";
    subprocess[4] = "d~a_emephd~";
    subprocess[5] = "u~a_emephu~";
    subprocess[6] = "b~a_emephb~";
    subprocess[7] = "dd~_emepha";
    subprocess[8] = "uu~_emepha";
    subprocess[9] = "bb~_emepha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emephd";
    subprocess[2] = "gu_emephu";
    subprocess[3] = "gb_emephb";
    subprocess[4] = "gd~_emephd~";
    subprocess[5] = "gu~_emephu~";
    subprocess[6] = "gb~_emephb~";
    subprocess[7] = "dd~_emephg";
    subprocess[8] = "uu~_emephg";
    subprocess[9] = "bb~_emephg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emephd";
    subprocess[2] = "ua_emephu";
    subprocess[3] = "ba_emephb";
    subprocess[4] = "d~a_emephd~";
    subprocess[5] = "u~a_emephu~";
    subprocess[6] = "b~a_emephb~";
    subprocess[7] = "dd~_emepha";
    subprocess[8] = "uu~_emepha";
    subprocess[9] = "bb~_emepha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexh03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeexh03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_emephdd~";
    subprocess[2] = "gg_emephuu~";
    subprocess[3] = "gg_emephbb~";
    subprocess[4] = "gd_emephgd";
    subprocess[5] = "gu_emephgu";
    subprocess[6] = "gb_emephgb";
    subprocess[7] = "gd~_emephgd~";
    subprocess[8] = "gu~_emephgu~";
    subprocess[9] = "gb~_emephgb~";
    subprocess[10] = "dd_emephdd";
    subprocess[11] = "du_emephdu";
    subprocess[12] = "ds_emephds";
    subprocess[13] = "dc_emephdc";
    subprocess[14] = "db_emephdb";
    subprocess[15] = "dd~_emephgg";
    subprocess[16] = "dd~_emephdd~";
    subprocess[17] = "dd~_emephuu~";
    subprocess[18] = "dd~_emephss~";
    subprocess[19] = "dd~_emephcc~";
    subprocess[20] = "dd~_emephbb~";
    subprocess[21] = "du~_emephdu~";
    subprocess[22] = "ds~_emephds~";
    subprocess[23] = "dc~_emephdc~";
    subprocess[24] = "db~_emephdb~";
    subprocess[25] = "uu_emephuu";
    subprocess[26] = "uc_emephuc";
    subprocess[27] = "ub_emephub";
    subprocess[28] = "ud~_emephud~";
    subprocess[29] = "uu~_emephgg";
    subprocess[30] = "uu~_emephdd~";
    subprocess[31] = "uu~_emephuu~";
    subprocess[32] = "uu~_emephss~";
    subprocess[33] = "uu~_emephcc~";
    subprocess[34] = "uu~_emephbb~";
    subprocess[35] = "us~_emephus~";
    subprocess[36] = "uc~_emephuc~";
    subprocess[37] = "ub~_emephub~";
    subprocess[38] = "bb_emephbb";
    subprocess[39] = "bd~_emephbd~";
    subprocess[40] = "bu~_emephbu~";
    subprocess[41] = "bb~_emephgg";
    subprocess[42] = "bb~_emephdd~";
    subprocess[43] = "bb~_emephuu~";
    subprocess[44] = "bb~_emephbb~";
    subprocess[45] = "d~d~_emephd~d~";
    subprocess[46] = "d~u~_emephd~u~";
    subprocess[47] = "d~s~_emephd~s~";
    subprocess[48] = "d~c~_emephd~c~";
    subprocess[49] = "d~b~_emephd~b~";
    subprocess[50] = "u~u~_emephu~u~";
    subprocess[51] = "u~c~_emephu~c~";
    subprocess[52] = "u~b~_emephu~b~";
    subprocess[53] = "b~b~_emephb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
