#include "header.hpp"

#include "ppexne02nockm.phasespace.set.hpp"

ppexne02nockm_phasespace_set::~ppexne02nockm_phasespace_set(){
  static Logger logger("ppexne02nockm_phasespace_set::~ppexne02nockm_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::optimize_minv_born(){
  static Logger logger("ppexne02nockm_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexne02nockm_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexne02nockm_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_020_udx_epve(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppexne02nockm_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_020_udx_epve(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppexne02nockm_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_020_udx_epve(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::optimize_minv_real(){
  static Logger logger("ppexne02nockm_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexne02nockm_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 7;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexne02nockm_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_120_gu_epved(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_120_gdx_epveux(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_120_udx_epveg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_030_ua_epved(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_030_dxa_epveux(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_030_udx_epvea(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppexne02nockm_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_120_gu_epved(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_120_gdx_epveux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_120_udx_epveg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_030_ua_epved(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_030_dxa_epveux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_030_udx_epvea(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppexne02nockm_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_120_gu_epved(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_120_gdx_epveux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_120_udx_epveg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_030_ua_epved(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_030_dxa_epveux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_030_udx_epvea(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppexne02nockm_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexne02nockm_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexne02nockm_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppexne02nockm_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_220_gg_epvedux(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_220_gu_epvegd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_220_gdx_epvegux(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_220_du_epvedd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_220_dc_epveds(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_220_ddx_epvedux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_220_ddx_epvescx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_220_dsx_epvedcx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_220_uu_epvedu(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_220_uc_epvedc(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_220_ub_epvedb(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_220_udx_epvegg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_220_udx_epveddx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_220_udx_epveuux(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_220_udx_epvessx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_220_udx_epveccx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_220_udx_epvebbx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_220_uux_epvedux(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_220_uux_epvescx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_220_usx_epvedsx(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_220_usx_epveucx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_220_ucx_epvedcx(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_220_ubx_epvedbx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_220_bdx_epvebux(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_220_bbx_epvedux(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_220_dxdx_epvedxux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_220_dxux_epveuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_220_dxsx_epvedxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_220_dxcx_epveuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_220_dxbx_epveuxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppexne02nockm_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_220_gg_epvedux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_220_gu_epvegd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_220_gdx_epvegux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_220_du_epvedd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_220_dc_epveds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_220_ddx_epvedux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_220_ddx_epvescx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_220_dsx_epvedcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_220_uu_epvedu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_220_uc_epvedc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_220_ub_epvedb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_220_udx_epvegg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_220_udx_epveddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_220_udx_epveuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_220_udx_epvessx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_220_udx_epveccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_220_udx_epvebbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_220_uux_epvedux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_220_uux_epvescx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_220_usx_epvedsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_220_usx_epveucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_220_ucx_epvedcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_220_ubx_epvedbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_220_bdx_epvebux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_220_bbx_epvedux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_220_dxdx_epvedxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_220_dxux_epveuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_220_dxsx_epvedxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_220_dxcx_epveuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_220_dxbx_epveuxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppexne02nockm_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_220_gg_epvedux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_220_gu_epvegd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_220_gdx_epvegux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_220_du_epvedd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_220_dc_epveds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_220_ddx_epvedux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_220_ddx_epvescx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_220_dsx_epvedcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_220_uu_epvedu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_220_uc_epvedc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_220_ub_epvedb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_220_udx_epvegg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_220_udx_epveddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_220_udx_epveuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_220_udx_epvessx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_220_udx_epveccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_220_udx_epvebbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_220_uux_epvedux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_220_uux_epvescx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_220_usx_epvedsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_220_usx_epveucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_220_ucx_epvedcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_220_ubx_epvedbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_220_bdx_epvebux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_220_bbx_epvedux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_220_dxdx_epvedxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_220_dxux_epveuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_220_dxsx_epvedxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_220_dxcx_epveuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_220_dxbx_epveuxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
