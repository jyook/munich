logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static stringstream info_cut;
  if (switch_output_cutinfo){
    info_cut.clear();
    info_cut.str("");
    for (int i_p = 3; i_p < p_parton[i_a].size(); i_p++){
      info_cut << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << csi->type_parton[i_a][i_p] << " -> " << particle_event[0][i_a][i_p].momentum << " pT = " << particle_event[0][i_a][i_p].pT << " eta = " << particle_event[0][i_a][i_p].eta << endl;
    }
    for (int i_o = 0; i_o < object_list_selection.size(); i_o++){
      int j_o = equivalent_no_object[i_o]; 
      // i_o -> number in relevant_object_list
      // j_o -> number in object_list
      //    int j_o = osi_no_relevant_object[object_list_selection[i_o]]; // i_o -> number in relevant_object_list; j_o -> i_o
      info_cut << "[" << setw(2) << i_a << "]   object_list_selection[" << setw(2) << i_o << "] = " << setw(6) << object_list_selection[i_o] << "   n_object[" << setw(2) << j_o << "][" << setw(2) << i_a << "] = " << n_object[j_o][i_a] << "   particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "].size() = " << particle_event[j_o][i_a].size() << endl;
      for (int i_p = 0; i_p < n_object[j_o][i_a]; i_p++){
	info_cut << "       particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "][" << setw(2) << i_p << "] = " << particle_event[j_o][i_a][i_p].momentum << endl;
      }
    }
  }


  // ALL IN OLD IMPLEMENTATION, MUST BE MIGRATED OR REMOVED
  
//   static int switch_cut_HT_all = user->switch_value[user->switch_map["HT_all"]];
//   static double cut_min_HT_all = user->cut_value[user->cut_map["min_HT_all"]];

//   static int switch_cut_HT_jet = user->switch_value[user->switch_map["HT_jet"]];
//   static double cut_min_HT_jet = user->cut_value[user->cut_map["min_HT_jet"]];

//   static int switch_cut_pT_jet_1st = user->switch_value[user->switch_map["pT_jet_1st"]];
//   static double cut_min_pT_jet_1st = user->cut_value[user->cut_map["min_pT_jet_1st"]];
  
//   static int switch_cut_pT_w = user->switch_value[user->switch_map["pT_w"]];
//   static double cut_min_pT_w = user->cut_value[user->cut_map["min_pT_w"]];
  
//   static int switch_cut_dphi_pi_jet = user->switch_value[user->switch_map["dphi_pi_jet"]];
//   static double cut_max_dphi_pi_jet = user->cut_value[user->cut_map["max_dphi_pi_jet"]];

//   static int switch_cut_pTn_wrt_pT1 = user->switch_value[user->switch_map["pTn_wrt_pT1"]];
//   static double cut_max_pTn_wrt_pT1 = user->cut_value[user->cut_map["max_pTn_wrt_pT1"]];

//   static int switch_cut_R_lepgam = user->switch_value[user->switch_map["R_lepgam"]];
//   static double cut_min_R_lepgam = user->cut_value[user->cut_map["min_R_lepgam"]];
//   static double cut_min_R2_lepgam = pow(cut_min_R_lepgam, 2);


//   if (switch_cut_HT_all == 1){
//     double temp_pT = 0.;
//     for (int i_l = 0; i_l < particle_event[access_object["lep"]][i_a].size(); i_l++){
//       temp_pT = temp_pT + particle_event[access_object["lep"]][i_a][i_l].pT; // ET before
//     }
//     for (int i_l = 0; i_l < n_object[access_object["jet"]][i_a]; i_l++){
//       temp_pT = temp_pT + particle_event[access_object["jet"]][i_a][i_l].pT;
//     }
//     if (temp_pT < cut_min_HT_all){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT << " < cut_min_HT_all = " << cut_min_HT_all << endl;
//       return;
//     }
//   }

//   if (switch_cut_HT_jet == 1){
//     double temp_pT = 0.;
//     for (int i_l = 0; i_l < n_object[access_object["jet"]][i_a]; i_l++){
//       temp_pT = temp_pT + particle_event[access_object["jet"]][i_a][i_l].pT;
//     }
//     if (temp_pT < cut_min_HT_jet){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT << " < cut_min_HT_jet = " << cut_min_HT_jet << endl;
//       return;
//     }
//   }
  
//   if (switch_cut_pT_jet_1st == 1){
//     double temp_pT_jet_1st = particle_event[access_object["jet"]][i_a][0].pT;
//     if (temp_pT_jet_1st < cut_min_pT_jet_1st){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_jet_1st << " < cut_min_pT_jet_1st = " << cut_min_pT_jet_1st << endl;
//       return;
//     }
//   }

//   if (switch_cut_pT_w == 1){
//     double temp_pT_w = particle_event[access_object["lep"]][i_a][0].pT;
//     if (temp_pT_w < cut_min_pT_w){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_w << " < cut_min_pT_w = " << cut_min_pT_w << endl;
//       return;
//     }
//   }
    
//   if (switch_cut_dphi_pi_jet == 1 && n_object[access_object["jet"]][i_a] > 1){
//     double temp_dphi = abs(particle_event[access_object["jet"]][i_a][0].phi - particle_event[access_object["jet"]][i_a][1].phi);
//     temp_dphi = min(temp_dphi, f2pi - temp_dphi);
//     logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " with dphi_jetjet = " << temp_dphi << endl;
//     if (temp_dphi > cut_max_dphi_pi_jet * pi){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_dphi << " > cut_max_dphi_pi_jet = " << cut_max_dphi_pi_jet << endl;
//       return;
//     }
//   }

//   if (switch_cut_pTn_wrt_pT1 == 1 && n_object[access_object["jet"]][i_a] > 0){
//     double temp_pTjetn = particle_event[access_object["jet"]][i_a][0].pT;
//     double temp_pTjet1 = particle_event[access_object["jet"]][i_a][0].pT;
//     if (temp_pTjetn > cut_max_pTn_wrt_pT1 * temp_pTjet1){
//       cut_ps[i_a] = -1; 
//       logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pTjetn / temp_pTjet1 << " > cut_max_pTn_wrt_pT1 = " << cut_max_pTn_wrt_pT1 << endl;
//       return;
//     }
//   }
  
//   // lepton--photon isolation cuts
//   if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_lepgam = " << switch_cut_R_lepgam << endl;}
//   if (switch_cut_R_lepgam == 1){
//     for (int i_l = 0; i_l < particle_event[access_object["lep"]][i_a].size(); i_l++){
//       double R2_eta_lepgam = R2_eta(particle_event[access_object["lep"]][i_a][i_l], particle_event[access_object["photon"]][i_a][0]);
      
//       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   particle_event[" << access_object["lep"] << "][" << i_a << "][" << i_l << "] = " << particle_event[access_object["lep"]][i_a][i_l].momentum << endl;}
//       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   particle_event[" << access_object["photon"] << "][" << i_a << "][0] = " << particle_event[access_object["photon"]][i_a][0].momentum << endl;}
//       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_lepgam = " << R2_eta_lepgam << " > " << cut_min_R2_lepgam << endl;}
      
//       if (R2_eta_lepgam < cut_min_R2_lepgam){
// 	cut_ps[i_a] = -1; 
// 	if (switch_output_cutinfo){
// 	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_lepgam" << endl; 
// 	  logger << LOG_DEBUG << endl << info_cut.str(); 
// 	}
	
// 	logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepgam cut applied" << endl; 
// 	return;
//       }
//     }
//   }
  
//   if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}
  
}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;
