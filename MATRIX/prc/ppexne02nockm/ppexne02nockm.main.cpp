#include "header.hpp"

#include "ppll02.amplitude.set.hpp"

#include "ppexne02nockm.contribution.set.hpp"
#include "ppexne02nockm.event.set.hpp"
#include "ppexne02nockm.phasespace.set.hpp"
#include "ppexne02nockm.observable.set.hpp"
#include "ppexne02nockm.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-epve+X");

  MUC->csi = new ppexne02nockm_contribution_set();
  MUC->esi = new ppexne02nockm_event_set();
  MUC->psi = new ppexne02nockm_phasespace_set();
  MUC->osi = new ppexne02nockm_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppll02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppexne02nockm_phasespace_set();
      MUC->psi->fake_psi->csi = new ppexne02nockm_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppexne02nockm_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
