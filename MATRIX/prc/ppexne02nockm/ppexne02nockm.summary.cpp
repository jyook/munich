#include "header.hpp"

#include "ppexne02nockm.summary.hpp"

ppexne02nockm_summary_generic::ppexne02nockm_summary_generic(munich * xmunich){
  Logger logger("ppexne02nockm_summary_generic::ppexne02nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexne02nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexne02nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexne02nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexne02nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexne02nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexne02nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epve";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gd~_epveu~";
    subprocess[3] = "ud~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epved";
    subprocess[2] = "d~a_epveu~";
    subprocess[3] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gd~_epveu~";
    subprocess[3] = "ud~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epved";
    subprocess[2] = "d~a_epveu~";
    subprocess[3] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gd~_epveu~";
    subprocess[3] = "ud~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epved";
    subprocess[2] = "d~a_epveu~";
    subprocess[3] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexne02nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(31);
    subprocess[1] = "gg_epvedu~";
    subprocess[2] = "gu_epvegd";
    subprocess[3] = "gd~_epvegu~";
    subprocess[4] = "du_epvedd";
    subprocess[5] = "dc_epveds";
    subprocess[6] = "dd~_epvedu~";
    subprocess[7] = "dd~_epvesc~";
    subprocess[8] = "ds~_epvedc~";
    subprocess[9] = "uu_epvedu";
    subprocess[10] = "uc_epvedc";
    subprocess[11] = "ub_epvedb";
    subprocess[12] = "ud~_epvegg";
    subprocess[13] = "ud~_epvedd~";
    subprocess[14] = "ud~_epveuu~";
    subprocess[15] = "ud~_epvess~";
    subprocess[16] = "ud~_epvecc~";
    subprocess[17] = "ud~_epvebb~";
    subprocess[18] = "uu~_epvedu~";
    subprocess[19] = "uu~_epvesc~";
    subprocess[20] = "us~_epveds~";
    subprocess[21] = "us~_epveuc~";
    subprocess[22] = "uc~_epvedc~";
    subprocess[23] = "ub~_epvedb~";
    subprocess[24] = "bd~_epvebu~";
    subprocess[25] = "bb~_epvedu~";
    subprocess[26] = "d~d~_epved~u~";
    subprocess[27] = "d~u~_epveu~u~";
    subprocess[28] = "d~s~_epved~c~";
    subprocess[29] = "d~c~_epveu~c~";
    subprocess[30] = "d~b~_epveu~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
