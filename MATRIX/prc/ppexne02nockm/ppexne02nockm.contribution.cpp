#include "header.hpp"

#include "ppexne02nockm.contribution.set.hpp"

ppexne02nockm_contribution_set::~ppexne02nockm_contribution_set(){
  static Logger logger("ppexne02nockm_contribution_set::~ppexne02nockm_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 3; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ex  ne  s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ex  ne  d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ex  ne  cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ex  ne  ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ex  ne  d    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> ex  ne  s    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> ex  ne  d    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> ex  ne  ux   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> ex  ne  cx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> ex  ne  ux   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  a    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  a    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  a    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02nockm_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppexne02nockm_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  d   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  g   d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ex  ne  g   s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ex  ne  g   d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ex  ne  g   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  g   ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ex  ne  g   cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ex  ne  g   ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ex  ne  g   cx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ex  ne  d   d    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> ex  ne  s   s    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> ex  ne  d   d    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> ex  ne  s   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ex  ne  d   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> ex  ne  d   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> ex  ne  d   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> ex  ne  d   s    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  d   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ex  ne  s   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ex  ne  d   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  s   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ex  ne  d   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ex  ne  s   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> ex  ne  d   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> ex  ne  s   ux   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> ex  ne  s   ux   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ex  ne  d   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> ex  ne  s   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  d   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> ex  ne  u   s    //
      combination_pdf[2] = {-1,   4,   2};   // u   c    -> ex  ne  u   s    //
      combination_pdf[3] = {-1,   2,   4};   // c   u    -> ex  ne  d   c    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> ex  ne  d   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> ex  ne  s   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> ex  ne  d   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> ex  ne  s   b    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  g   g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  g   g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  g   g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  d   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  s   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  d   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  u   ux   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  c   cx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  u   ux   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  s   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  d   dx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  s   sx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  c   cx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  u   ux   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  c   cx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  b   bx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  b   bx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  b   bx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  d   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ex  ne  s   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ex  ne  d   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  s   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ex  ne  d   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ex  ne  s   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  d   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> ex  ne  s   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> ex  ne  s   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> ex  ne  d   sx   //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  u   cx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> ex  ne  c   ux   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> ex  ne  c   ux   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> ex  ne  u   cx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ex  ne  d   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> ex  ne  s   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> ex  ne  s   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  d   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> ex  ne  s   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> ex  ne  d   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> ex  ne  s   bx   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> ex  ne  b   ux   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> ex  ne  b   cx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> ex  ne  b   ux   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  d   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> ex  ne  s   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> ex  ne  d   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> ex  ne  dx  ux   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> ex  ne  sx  cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> ex  ne  ux  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> ex  ne  cx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> ex  ne  ux  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> ex  ne  cx  cx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  dx  cx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> ex  ne  ux  sx   //
      combination_pdf[2] = {-1,  -3,  -1};   // dx  sx   -> ex  ne  ux  sx   //
      combination_pdf[3] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  dx  cx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> ex  ne  ux  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> ex  ne  ux  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> ex  ne  ux  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ex  ne  ux  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> ex  ne  cx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> ex  ne  ux  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> ex  ne  cx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
