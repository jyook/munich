#include "header.hpp"

#include "ppllh03.amplitude.set.hpp"

#include "ppnenexh03.contribution.set.hpp"
#include "ppnenexh03.event.set.hpp"
#include "ppnenexh03.phasespace.set.hpp"
#include "ppnenexh03.observable.set.hpp"
#include "ppnenexh03.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-veve~h+X");

  MUC->csi = new ppnenexh03_contribution_set();
  MUC->esi = new ppnenexh03_event_set();
  MUC->psi = new ppnenexh03_phasespace_set();
  MUC->osi = new ppnenexh03_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllh03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppnenexh03_phasespace_set();
      MUC->psi->fake_psi->csi = new ppnenexh03_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppnenexh03_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
