#include "header.hpp"

#include "ppnenexh03.phasespace.set.hpp"

ppnenexh03_phasespace_set::~ppnenexh03_phasespace_set(){
  static Logger logger("ppnenexh03_phasespace_set::~ppnenexh03_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::optimize_minv_born(){
  static Logger logger("ppnenexh03_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexh03_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexh03_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -23};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-23);
      tau_MC_map.push_back(-36);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_030_ddx_vevexh(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_030_uux_vevexh(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_030_bbx_vevexh(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_230_gg_vevexh(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppnenexh03_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_030_ddx_vevexh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_030_uux_vevexh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_030_bbx_vevexh(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_230_gg_vevexh(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppnenexh03_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_030_ddx_vevexh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_030_uux_vevexh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_030_bbx_vevexh(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_230_gg_vevexh(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::optimize_minv_real(){
  static Logger logger("ppnenexh03_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexh03_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 33;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 6;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexh03_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 13){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 15){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 17){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_130_gd_vevexhd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_130_gu_vevexhu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_130_gb_vevexhb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_130_gdx_vevexhdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_130_gux_vevexhux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_130_gbx_vevexhbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_130_ddx_vevexhg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_130_uux_vevexhg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_130_bbx_vevexhg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_040_da_vevexhd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_040_ua_vevexhu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_040_ba_vevexhb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_040_dxa_vevexhdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_040_uxa_vevexhux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_040_bxa_vevexhbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_040_ddx_vevexha(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_040_uux_vevexha(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_040_bbx_vevexha(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_330_gg_vevexhg(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_330_gd_vevexhd(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_330_gu_vevexhu(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_330_gb_vevexhb(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_330_gdx_vevexhdx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_330_gux_vevexhux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_330_gbx_vevexhbx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_330_ddx_vevexhg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_330_uux_vevexhg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_330_bbx_vevexhg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppnenexh03_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_130_gd_vevexhd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_130_gu_vevexhu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_130_gb_vevexhb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_130_gdx_vevexhdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_130_gux_vevexhux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_130_gbx_vevexhbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_130_ddx_vevexhg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_130_uux_vevexhg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_130_bbx_vevexhg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_040_da_vevexhd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_040_ua_vevexhu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_040_ba_vevexhb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_040_dxa_vevexhdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_040_uxa_vevexhux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_040_bxa_vevexhbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_040_ddx_vevexha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_040_uux_vevexha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_040_bbx_vevexha(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_330_gg_vevexhg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_330_gd_vevexhd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_330_gu_vevexhu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_330_gb_vevexhb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_330_gdx_vevexhdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_330_gux_vevexhux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_330_gbx_vevexhbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_330_ddx_vevexhg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_330_uux_vevexhg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_330_bbx_vevexhg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppnenexh03_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_130_gd_vevexhd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_130_gu_vevexhu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_130_gb_vevexhb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_130_gdx_vevexhdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_130_gux_vevexhux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_130_gbx_vevexhbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_130_ddx_vevexhg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_130_uux_vevexhg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_130_bbx_vevexhg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_040_da_vevexhd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_040_ua_vevexhu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_040_ba_vevexhb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_040_dxa_vevexhdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_040_uxa_vevexhux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_040_bxa_vevexhbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_040_ddx_vevexha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_040_uux_vevexha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_040_bbx_vevexha(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_330_gg_vevexhg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_330_gd_vevexhd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_330_gu_vevexhu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_330_gb_vevexhb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_330_gdx_vevexhdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_330_gux_vevexhux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_330_gbx_vevexhbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_330_ddx_vevexhg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_330_uux_vevexhg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_330_bbx_vevexhg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppnenexh03_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexh03_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 8;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexh03_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppnenexh03_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_230_gg_vevexhddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_230_gg_vevexhuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_230_gg_vevexhbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_230_gd_vevexhgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_230_gu_vevexhgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_230_gb_vevexhgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_230_gdx_vevexhgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_230_gux_vevexhgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_230_gbx_vevexhgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_230_dd_vevexhdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_230_du_vevexhdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_230_ds_vevexhds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_230_dc_vevexhdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_230_db_vevexhdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_230_ddx_vevexhgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_230_ddx_vevexhddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_230_ddx_vevexhuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_230_ddx_vevexhssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_230_ddx_vevexhccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_230_ddx_vevexhbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_230_dux_vevexhdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_230_dsx_vevexhdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_230_dcx_vevexhdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_230_dbx_vevexhdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_230_uu_vevexhuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_230_uc_vevexhuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_230_ub_vevexhub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_230_udx_vevexhudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_230_uux_vevexhgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_230_uux_vevexhddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_230_uux_vevexhuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_230_uux_vevexhssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_230_uux_vevexhccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_230_uux_vevexhbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_230_usx_vevexhusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_230_ucx_vevexhucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_230_ubx_vevexhubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_230_bb_vevexhbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_230_bdx_vevexhbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_230_bux_vevexhbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_230_bbx_vevexhgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_230_bbx_vevexhddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_230_bbx_vevexhuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_230_bbx_vevexhbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_230_dxdx_vevexhdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_230_dxux_vevexhdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_230_dxsx_vevexhdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_230_dxcx_vevexhdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_230_dxbx_vevexhdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_230_uxux_vevexhuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_230_uxcx_vevexhuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_230_uxbx_vevexhuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_230_bxbx_vevexhbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppnenexh03_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_230_gg_vevexhddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_230_gg_vevexhuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_230_gg_vevexhbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_230_gd_vevexhgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_230_gu_vevexhgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_230_gb_vevexhgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_230_gdx_vevexhgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_230_gux_vevexhgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_230_gbx_vevexhgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_230_dd_vevexhdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_230_du_vevexhdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_230_ds_vevexhds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_230_dc_vevexhdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_230_db_vevexhdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_230_ddx_vevexhgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_230_ddx_vevexhddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_230_ddx_vevexhuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_230_ddx_vevexhssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_230_ddx_vevexhccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_230_ddx_vevexhbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_230_dux_vevexhdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_230_dsx_vevexhdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_230_dcx_vevexhdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_230_dbx_vevexhdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_230_uu_vevexhuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_230_uc_vevexhuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_230_ub_vevexhub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_230_udx_vevexhudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_230_uux_vevexhgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_230_uux_vevexhddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_230_uux_vevexhuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_230_uux_vevexhssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_230_uux_vevexhccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_230_uux_vevexhbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_230_usx_vevexhusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_230_ucx_vevexhucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_230_ubx_vevexhubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_230_bb_vevexhbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_230_bdx_vevexhbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_230_bux_vevexhbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_230_bbx_vevexhgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_230_bbx_vevexhddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_230_bbx_vevexhuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_230_bbx_vevexhbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_230_dxdx_vevexhdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_230_dxux_vevexhdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_230_dxsx_vevexhdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_230_dxcx_vevexhdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_230_dxbx_vevexhdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_230_uxux_vevexhuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_230_uxcx_vevexhuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_230_uxbx_vevexhuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_230_bxbx_vevexhbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppnenexh03_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_230_gg_vevexhddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_230_gg_vevexhuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_230_gg_vevexhbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_230_gd_vevexhgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_230_gu_vevexhgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_230_gb_vevexhgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_230_gdx_vevexhgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_230_gux_vevexhgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_230_gbx_vevexhgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_230_dd_vevexhdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_230_du_vevexhdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_230_ds_vevexhds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_230_dc_vevexhdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_230_db_vevexhdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_230_ddx_vevexhgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_230_ddx_vevexhddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_230_ddx_vevexhuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_230_ddx_vevexhssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_230_ddx_vevexhccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_230_ddx_vevexhbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_230_dux_vevexhdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_230_dsx_vevexhdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_230_dcx_vevexhdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_230_dbx_vevexhdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_230_uu_vevexhuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_230_uc_vevexhuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_230_ub_vevexhub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_230_udx_vevexhudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_230_uux_vevexhgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_230_uux_vevexhddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_230_uux_vevexhuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_230_uux_vevexhssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_230_uux_vevexhccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_230_uux_vevexhbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_230_usx_vevexhusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_230_ucx_vevexhucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_230_ubx_vevexhubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_230_bb_vevexhbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_230_bdx_vevexhbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_230_bux_vevexhbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_230_bbx_vevexhgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_230_bbx_vevexhddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_230_bbx_vevexhuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_230_bbx_vevexhbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_230_dxdx_vevexhdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_230_dxux_vevexhdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_230_dxsx_vevexhdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_230_dxcx_vevexhdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_230_dxbx_vevexhdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_230_uxux_vevexhuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_230_uxcx_vevexhuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_230_uxbx_vevexhuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_230_bxbx_vevexhbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
