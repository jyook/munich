#include "header.hpp"

#include "ppnenexh03.summary.hpp"

ppnenexh03_summary_generic::ppnenexh03_summary_generic(munich * xmunich){
  Logger logger("ppnenexh03_summary_generic::ppnenexh03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppnenexh03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppnenexh03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppnenexh03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppnenexh03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppnenexh03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppnenexh03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_born(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~h";
    subprocess[2] = "uu~_veve~h";
    subprocess[3] = "bb~_veve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~hd";
    subprocess[2] = "gu_veve~hu";
    subprocess[3] = "gb_veve~hb";
    subprocess[4] = "gd~_veve~hd~";
    subprocess[5] = "gu~_veve~hu~";
    subprocess[6] = "gb~_veve~hb~";
    subprocess[7] = "dd~_veve~hg";
    subprocess[8] = "uu~_veve~hg";
    subprocess[9] = "bb~_veve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_veve~hg";
    subprocess[2] = "gd_veve~hd";
    subprocess[3] = "gu_veve~hu";
    subprocess[4] = "gb_veve~hb";
    subprocess[5] = "gd~_veve~hd~";
    subprocess[6] = "gu~_veve~hu~";
    subprocess[7] = "gb~_veve~hb~";
    subprocess[8] = "dd~_veve~hg";
    subprocess[9] = "uu~_veve~hg";
    subprocess[10] = "bb~_veve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~hd";
    subprocess[2] = "ua_veve~hu";
    subprocess[3] = "ba_veve~hb";
    subprocess[4] = "d~a_veve~hd~";
    subprocess[5] = "u~a_veve~hu~";
    subprocess[6] = "b~a_veve~hb~";
    subprocess[7] = "dd~_veve~ha";
    subprocess[8] = "uu~_veve~ha";
    subprocess[9] = "bb~_veve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~hd";
    subprocess[2] = "gu_veve~hu";
    subprocess[3] = "gb_veve~hb";
    subprocess[4] = "gd~_veve~hd~";
    subprocess[5] = "gu~_veve~hu~";
    subprocess[6] = "gb~_veve~hb~";
    subprocess[7] = "dd~_veve~hg";
    subprocess[8] = "uu~_veve~hg";
    subprocess[9] = "bb~_veve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~hd";
    subprocess[2] = "ua_veve~hu";
    subprocess[3] = "ba_veve~hb";
    subprocess[4] = "d~a_veve~hd~";
    subprocess[5] = "u~a_veve~hu~";
    subprocess[6] = "b~a_veve~hb~";
    subprocess[7] = "dd~_veve~ha";
    subprocess[8] = "uu~_veve~ha";
    subprocess[9] = "bb~_veve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~hd";
    subprocess[2] = "gu_veve~hu";
    subprocess[3] = "gb_veve~hb";
    subprocess[4] = "gd~_veve~hd~";
    subprocess[5] = "gu~_veve~hu~";
    subprocess[6] = "gb~_veve~hb~";
    subprocess[7] = "dd~_veve~hg";
    subprocess[8] = "uu~_veve~hg";
    subprocess[9] = "bb~_veve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~hd";
    subprocess[2] = "ua_veve~hu";
    subprocess[3] = "ba_veve~hb";
    subprocess[4] = "d~a_veve~hd~";
    subprocess[5] = "u~a_veve~hu~";
    subprocess[6] = "b~a_veve~hb~";
    subprocess[7] = "dd~_veve~ha";
    subprocess[8] = "uu~_veve~ha";
    subprocess[9] = "bb~_veve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexh03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppnenexh03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_veve~hdd~";
    subprocess[2] = "gg_veve~huu~";
    subprocess[3] = "gg_veve~hbb~";
    subprocess[4] = "gd_veve~hgd";
    subprocess[5] = "gu_veve~hgu";
    subprocess[6] = "gb_veve~hgb";
    subprocess[7] = "gd~_veve~hgd~";
    subprocess[8] = "gu~_veve~hgu~";
    subprocess[9] = "gb~_veve~hgb~";
    subprocess[10] = "dd_veve~hdd";
    subprocess[11] = "du_veve~hdu";
    subprocess[12] = "ds_veve~hds";
    subprocess[13] = "dc_veve~hdc";
    subprocess[14] = "db_veve~hdb";
    subprocess[15] = "dd~_veve~hgg";
    subprocess[16] = "dd~_veve~hdd~";
    subprocess[17] = "dd~_veve~huu~";
    subprocess[18] = "dd~_veve~hss~";
    subprocess[19] = "dd~_veve~hcc~";
    subprocess[20] = "dd~_veve~hbb~";
    subprocess[21] = "du~_veve~hdu~";
    subprocess[22] = "ds~_veve~hds~";
    subprocess[23] = "dc~_veve~hdc~";
    subprocess[24] = "db~_veve~hdb~";
    subprocess[25] = "uu_veve~huu";
    subprocess[26] = "uc_veve~huc";
    subprocess[27] = "ub_veve~hub";
    subprocess[28] = "ud~_veve~hud~";
    subprocess[29] = "uu~_veve~hgg";
    subprocess[30] = "uu~_veve~hdd~";
    subprocess[31] = "uu~_veve~huu~";
    subprocess[32] = "uu~_veve~hss~";
    subprocess[33] = "uu~_veve~hcc~";
    subprocess[34] = "uu~_veve~hbb~";
    subprocess[35] = "us~_veve~hus~";
    subprocess[36] = "uc~_veve~huc~";
    subprocess[37] = "ub~_veve~hub~";
    subprocess[38] = "bb_veve~hbb";
    subprocess[39] = "bd~_veve~hbd~";
    subprocess[40] = "bu~_veve~hbu~";
    subprocess[41] = "bb~_veve~hgg";
    subprocess[42] = "bb~_veve~hdd~";
    subprocess[43] = "bb~_veve~huu~";
    subprocess[44] = "bb~_veve~hbb~";
    subprocess[45] = "d~d~_veve~hd~d~";
    subprocess[46] = "d~u~_veve~hd~u~";
    subprocess[47] = "d~s~_veve~hd~s~";
    subprocess[48] = "d~c~_veve~hd~c~";
    subprocess[49] = "d~b~_veve~hd~b~";
    subprocess[50] = "u~u~_veve~hu~u~";
    subprocess[51] = "u~c~_veve~hu~c~";
    subprocess[52] = "u~b~_veve~hu~b~";
    subprocess[53] = "b~b~_veve~hb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
