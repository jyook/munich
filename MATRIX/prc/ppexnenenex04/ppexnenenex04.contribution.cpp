#include "header.hpp"

#include "ppexnenenex04.contribution.set.hpp"

ppexnenenex04_contribution_set::~ppexnenenex04_contribution_set(){
  static Logger logger("ppexnenenex04_contribution_set::~ppexnenenex04_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex  //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex  //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex  //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex  //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[7] = 7;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 3; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  ne  nex d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ex  ne  ne  nex s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ex  ne  ne  nex d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ex  ne  ne  nex s    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  ne  nex ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ex  ne  ne  nex cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ex  ne  ne  nex ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ex  ne  ne  nex cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ex  ne  ne  nex d    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> ex  ne  ne  nex s    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> ex  ne  ne  nex d    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> ex  ne  ne  nex s    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> ex  ne  ne  nex ux   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> ex  ne  ne  nex cx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> ex  ne  ne  nex ux   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> ex  ne  ne  nex cx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex a    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex a    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex a    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  12) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 26; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppexnenenex04_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  ne  nex d   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> ex  ne  ne  nex s   cx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  ne  nex g   d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> ex  ne  ne  nex g   s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> ex  ne  ne  nex g   d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> ex  ne  ne  nex g   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  ne  nex g   ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> ex  ne  ne  nex g   cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> ex  ne  ne  nex g   ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> ex  ne  ne  nex g   cx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ex  ne  ne  nex d   d    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> ex  ne  ne  nex s   s    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> ex  ne  ne  nex d   d    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> ex  ne  ne  nex s   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ex  ne  ne  nex d   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> ex  ne  ne  nex d   s    //
      combination_pdf[2] = { 1,   5,   2};   // b   u    -> ex  ne  ne  nex d   b    //
      combination_pdf[3] = { 1,   5,   4};   // b   c    -> ex  ne  ne  nex s   b    //
      combination_pdf[4] = {-1,   3,   2};   // u   s    -> ex  ne  ne  nex d   s    //
      combination_pdf[5] = {-1,   1,   4};   // c   d    -> ex  ne  ne  nex d   s    //
      combination_pdf[6] = {-1,   5,   2};   // u   b    -> ex  ne  ne  nex d   b    //
      combination_pdf[7] = {-1,   5,   4};   // c   b    -> ex  ne  ne  nex s   b    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  ne  nex d   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ex  ne  ne  nex s   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> ex  ne  ne  nex d   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> ex  ne  ne  nex s   cx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  ne  nex s   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> ex  ne  ne  nex d   ux   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> ex  ne  ne  nex d   ux   //
      combination_pdf[3] = { 1,   5,  -5};   // b   bx   -> ex  ne  ne  nex s   cx   //
      combination_pdf[4] = {-1,   1,  -1};   // dx  d    -> ex  ne  ne  nex s   cx   //
      combination_pdf[5] = {-1,   3,  -3};   // sx  s    -> ex  ne  ne  nex d   ux   //
      combination_pdf[6] = {-1,   5,  -5};   // bx  b    -> ex  ne  ne  nex d   ux   //
      combination_pdf[7] = {-1,   5,  -5};   // bx  b    -> ex  ne  ne  nex s   cx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> ex  ne  ne  nex d   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> ex  ne  ne  nex s   ux   //
      combination_pdf[2] = { 1,   5,  -1};   // b   dx   -> ex  ne  ne  nex b   ux   //
      combination_pdf[3] = { 1,   5,  -3};   // b   sx   -> ex  ne  ne  nex b   cx   //
      combination_pdf[4] = {-1,   3,  -1};   // dx  s    -> ex  ne  ne  nex s   ux   //
      combination_pdf[5] = {-1,   1,  -3};   // sx  d    -> ex  ne  ne  nex d   cx   //
      combination_pdf[6] = {-1,   5,  -1};   // dx  b    -> ex  ne  ne  nex b   ux   //
      combination_pdf[7] = {-1,   5,  -3};   // sx  b    -> ex  ne  ne  nex b   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ex  ne  ne  nex d   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> ex  ne  ne  nex s   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  ne  nex d   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> ex  ne  ne  nex u   s    //
      combination_pdf[2] = {-1,   4,   2};   // u   c    -> ex  ne  ne  nex u   s    //
      combination_pdf[3] = {-1,   2,   4};   // c   u    -> ex  ne  ne  nex d   c    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex g   g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex g   g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex g   g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex g   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex d   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex s   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex d   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex s   sx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex u   ux   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex c   cx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex u   ux   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex c   cx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex s   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex d   dx   //
      combination_pdf[2] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex b   bx   //
      combination_pdf[3] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex b   bx   //
      combination_pdf[4] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex s   sx   //
      combination_pdf[5] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex d   dx   //
      combination_pdf[6] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex b   bx   //
      combination_pdf[7] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex b   bx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  ne  nex c   cx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> ex  ne  ne  nex u   ux   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> ex  ne  ne  nex c   cx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> ex  ne  ne  nex u   ux   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  ne  nex d   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ex  ne  ne  nex s   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ex  ne  ne  nex d   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ex  ne  ne  nex s   cx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  ne  nex s   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> ex  ne  ne  nex d   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> ex  ne  ne  nex s   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> ex  ne  ne  nex d   ux   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  ne  nex d   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> ex  ne  ne  nex s   dx   //
      combination_pdf[2] = { 1,   2,  -5};   // u   bx   -> ex  ne  ne  nex d   bx   //
      combination_pdf[3] = { 1,   4,  -5};   // c   bx   -> ex  ne  ne  nex s   bx   //
      combination_pdf[4] = {-1,   4,  -1};   // dx  c    -> ex  ne  ne  nex s   dx   //
      combination_pdf[5] = {-1,   2,  -3};   // sx  u    -> ex  ne  ne  nex d   sx   //
      combination_pdf[6] = {-1,   2,  -5};   // bx  u    -> ex  ne  ne  nex d   bx   //
      combination_pdf[7] = {-1,   4,  -5};   // bx  c    -> ex  ne  ne  nex s   bx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  ne  nex u   cx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> ex  ne  ne  nex c   ux   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> ex  ne  ne  nex c   ux   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> ex  ne  ne  nex u   cx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ex  ne  ne  nex d   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> ex  ne  ne  nex s   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> ex  ne  ne  nex s   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> ex  ne  ne  nex d   cx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> ex  ne  ne  nex dx  ux   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> ex  ne  ne  nex sx  cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> ex  ne  ne  nex ux  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> ex  ne  ne  nex cx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> ex  ne  ne  nex ux  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> ex  ne  ne  nex cx  cx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  ne  nex dx  cx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> ex  ne  ne  nex ux  sx   //
      combination_pdf[2] = { 1,  -5,  -1};   // bx  dx   -> ex  ne  ne  nex ux  bx   //
      combination_pdf[3] = { 1,  -5,  -3};   // bx  sx   -> ex  ne  ne  nex cx  bx   //
      combination_pdf[4] = {-1,  -3,  -1};   // dx  sx   -> ex  ne  ne  nex ux  sx   //
      combination_pdf[5] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  ne  nex dx  cx   //
      combination_pdf[6] = {-1,  -5,  -1};   // dx  bx   -> ex  ne  ne  nex ux  bx   //
      combination_pdf[7] = {-1,  -5,  -3};   // sx  bx   -> ex  ne  ne  nex cx  bx   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> ex  ne  ne  nex ux  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> ex  ne  ne  nex ux  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> ex  ne  ne  nex ux  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> ex  ne  ne  nex ux  cx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
