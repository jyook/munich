#include "header.hpp"

#include "ppexnenenex04.summary.hpp"

ppexnenenex04_summary_generic::ppexnenenex04_summary_generic(munich * xmunich){
  Logger logger("ppexnenenex04_summary_generic::ppexnenenex04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexnenenex04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexnenenex04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexnenenex04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexnenenex04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexnenenex04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexnenenex04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_born(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveveve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epveveve~d";
    subprocess[2] = "gd~_epveveve~u~";
    subprocess[3] = "ud~_epveveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epveveve~d";
    subprocess[2] = "d~a_epveveve~u~";
    subprocess[3] = "ud~_epveveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epveveve~d";
    subprocess[2] = "gd~_epveveve~u~";
    subprocess[3] = "ud~_epveveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epveveve~d";
    subprocess[2] = "d~a_epveveve~u~";
    subprocess[3] = "ud~_epveveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epveveve~d";
    subprocess[2] = "gd~_epveveve~u~";
    subprocess[3] = "ud~_epveveve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epveveve~d";
    subprocess[2] = "d~a_epveveve~u~";
    subprocess[3] = "ud~_epveveve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexnenenex04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_epveveve~du~";
    subprocess[2] = "gu_epveveve~gd";
    subprocess[3] = "gd~_epveveve~gu~";
    subprocess[4] = "du_epveveve~dd";
    subprocess[5] = "dc_epveveve~ds";
    subprocess[6] = "dd~_epveveve~du~";
    subprocess[7] = "dd~_epveveve~sc~";
    subprocess[8] = "ds~_epveveve~dc~";
    subprocess[9] = "uu_epveveve~du";
    subprocess[10] = "uc_epveveve~dc";
    subprocess[11] = "ud~_epveveve~gg";
    subprocess[12] = "ud~_epveveve~dd~";
    subprocess[13] = "ud~_epveveve~uu~";
    subprocess[14] = "ud~_epveveve~ss~";
    subprocess[15] = "ud~_epveveve~cc~";
    subprocess[16] = "uu~_epveveve~du~";
    subprocess[17] = "uu~_epveveve~sc~";
    subprocess[18] = "us~_epveveve~ds~";
    subprocess[19] = "us~_epveveve~uc~";
    subprocess[20] = "uc~_epveveve~dc~";
    subprocess[21] = "d~d~_epveveve~d~u~";
    subprocess[22] = "d~u~_epveveve~u~u~";
    subprocess[23] = "d~s~_epveveve~d~c~";
    subprocess[24] = "d~c~_epveveve~u~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
