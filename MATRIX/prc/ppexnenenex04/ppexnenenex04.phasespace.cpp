#include "header.hpp"

#include "ppexnenenex04.phasespace.set.hpp"

ppexnenenex04_phasespace_set::~ppexnenenex04_phasespace_set(){
  static Logger logger("ppexnenenex04_phasespace_set::~ppexnenenex04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppexnenenex04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexnenenex04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 12;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 12;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexnenenex04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_040_udx_epvevevex(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppexnenenex04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_040_udx_epvevevex(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppexnenenex04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_040_udx_epvevevex(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppexnenenex04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexnenenex04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 44;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 28;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 28;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 28;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 108;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 60;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 60;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 60;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexnenenex04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_140_gu_epvevevexd(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_140_gdx_epvevevexux(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_140_udx_epvevevexg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_050_ua_epvevevexd(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_050_dxa_epvevevexux(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_050_udx_epvevevexa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppexnenenex04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_140_gu_epvevevexd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_140_gdx_epvevevexux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_140_udx_epvevevexg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_050_ua_epvevevexd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_050_dxa_epvevevexux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_050_udx_epvevevexa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppexnenenex04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_140_gu_epvevevexd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_140_gdx_epvevevexux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_140_udx_epvevevexg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_050_ua_epvevevexd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_050_dxa_epvevevexux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_050_udx_epvevevexa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppexnenenex04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexnenenex04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 292;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 124;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 124;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 124;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 72;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 36;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 72;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 36;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 124;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 36;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexnenenex04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppexnenenex04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_240_gg_epvevevexdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gu_epvevevexgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gdx_epvevevexgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_du_epvevevexdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_dc_epvevevexds(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_ddx_epvevevexdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_ddx_epvevevexscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_dsx_epvevevexdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_uu_epvevevexdu(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_240_uc_epvevevexdc(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_240_udx_epvevevexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_240_udx_epvevevexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_240_udx_epvevevexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_udx_epvevevexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_udx_epvevevexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_uux_epvevevexdux(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_uux_epvevevexscx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_240_usx_epvevevexdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_usx_epvevevexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_ucx_epvevevexdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_dxdx_epvevevexdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_dxux_epvevevexuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_dxsx_epvevevexdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_dxcx_epvevevexuxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppexnenenex04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_240_gg_epvevevexdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gu_epvevevexgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gdx_epvevevexgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_du_epvevevexdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_dc_epvevevexds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_ddx_epvevevexdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_ddx_epvevevexscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_dsx_epvevevexdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_uu_epvevevexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_240_uc_epvevevexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_240_udx_epvevevexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_240_udx_epvevevexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_240_udx_epvevevexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_udx_epvevevexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_udx_epvevevexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_uux_epvevevexdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_uux_epvevevexscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_240_usx_epvevevexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_usx_epvevevexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_ucx_epvevevexdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_dxdx_epvevevexdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_dxux_epvevevexuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_dxsx_epvevevexdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_dxcx_epvevevexuxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnenenex04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppexnenenex04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_240_gg_epvevevexdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gu_epvevevexgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gdx_epvevevexgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_du_epvevevexdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_dc_epvevevexds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_ddx_epvevevexdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_ddx_epvevevexscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_dsx_epvevevexdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_uu_epvevevexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_240_uc_epvevevexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_240_udx_epvevevexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_240_udx_epvevevexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_240_udx_epvevevexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_udx_epvevevexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_udx_epvevevexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_uux_epvevevexdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_uux_epvevevexscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_240_usx_epvevevexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_usx_epvevevexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_ucx_epvevevexdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_dxdx_epvevevexdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_dxux_epvevevexuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_dxsx_epvevevexdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_dxcx_epvevevexuxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
