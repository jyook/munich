logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  user_particle[oset.user.particle_map["<particle_name>"]].push_back(<particle>);

  //  static int switch_lepton_identification = USERSWITCH("lepton_identification");

  // lepton identification not needed in this case, but might be useful if for whatever reason event selection is build only on reconstructed Z bosons

//  if (switch_lepton_identification){
    USERPARTICLE("Wmrec").push_back(PARTICLE("lm")[0] + PARTICLE("nux")[0]);
    USERPARTICLE("Wprec").push_back(PARTICLE("nu")[0] + PARTICLE("lp")[0]);
    USERPARTICLE("Wrec").push_back(USERPARTICLE("Wmrec")[0]);
    USERPARTICLE("Wrec").push_back(USERPARTICLE("Wprec")[0]);
    USERPARTICLE("dilepton").push_back(PARTICLE("lm")[0] + PARTICLE("lp")[0]);
//  }

}
logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
