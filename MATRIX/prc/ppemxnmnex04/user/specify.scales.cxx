{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).m();
    // takes sum of momenta of hardest lepton PARTICLE("lep")[0].momentum, second-hardest lepton PARTICLE("lep")[1].momentum, etc. 
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).m();
    double pT = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).pT();
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else if (sd == 3){
    // sum of transverse masses of the two W bosons with the invariant mass replaced by the W-boson pole mass
    fourvector wplus = PARTICLE("lp")[0].momentum + PARTICLE("nu")[0].momentum;
    fourvector wminus = PARTICLE("lm")[0].momentum + PARTICLE("nux")[0].momentum;
    double wplus_ET = sqrt(msi->M2_W + wplus.pT2());
    double wminus_ET = sqrt(msi->M2_W + wminus.pT2());
    temp_mu_central = wplus_ET + wminus_ET;
  }
  else if (sd == 4){
    // sum of transverse masses of the two W bosons
    double wplus_ET  = (PARTICLE("lp")[0].momentum + PARTICLE("nu")[0].momentum).ET();
    double wminus_ET = (PARTICLE("lm")[0].momentum + PARTICLE("nux")[0].momentum).ET();
    temp_mu_central = wplus_ET + wminus_ET;
  }

  else if (sd == 5){ // previously 1
    //    temp_mu_central = msi->M_W;
    
    double HThatlep = PARTICLE("missing")[0].pT;
    //    logger << LOG_DEBUG << "nu HThatlep = " << HThatlep << endl;
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      HThatlep = HThatlep + PARTICLE("lep")[i_l].pT;
      //      logger << LOG_DEBUG << i_l << "  HThatlep = " << HThatlep << endl;
    }
    temp_mu_central = HThatlep;
    //    logger << LOG_DEBUG << "temp_mu_central = " << temp_mu_central << endl;
  }

  else if (sd == -1){ // previously² 1
    double HThat = 0.;
    for (int i_l = 3; i_l < esi->particle_event[0][i_a].size(); i_l++){
      HThat = HThat + esi->particle_event[0][i_a][i_l].ET;
    }
    temp_mu_central = HThat;
  }
  else if (sd == 6){ // previously 2
    double HThat = 0.;
    for (int i_l = 3; i_l < 7; i_l++){
      HThat = HThat + esi->particle_event[0][i_a][i_l].ET;
    }
    temp_mu_central = HThat;
  }
  else if (sd == 7){ // previously 3
    fourvector temp_wm = PARTICLE("em")[0].momentum + PARTICLE("nex")[0].momentum;
    fourvector temp_wp = PARTICLE("mup")[0].momentum + PARTICLE("nm")[0].momentum;
    //    fourvector temp_ze = PARTICLE("em")[0].momentum + PARTICLE("mup")[0].momentum;
    //    fourvector temp_zn = PARTICLE("nm")[0].momentum + PARTICLE("nex")[0].momentum;
    fourvector temp_z4 = temp_wm + temp_wp;
    
    double diff_WW = .5 * (abs(temp_wm.m() - msi->M[24]) + abs(temp_wp.m() - msi->M[24]));
    //    double diff_ZZ = .5 * (abs(temp_ze.m() - msi->M[23]) + abs(temp_zn.m() - msi->M[23]));
    double diff_Z4 = abs(temp_z4.m() - msi->M[23]);
    
    double weight_WW = 1. / pow(diff_WW, 2);
    //    double weight_ZZ = 1. / pow(diff_ZZ, 2);
    double weight_Z4 = 1. / pow(diff_Z4, 2);
    
    double weight_tot = weight_WW + weight_Z4;
    //    double weight_tot = weight_WW + weight_ZZ + weight_Z4;
    
    double factor_WW = weight_WW / weight_tot;
    //    double factor_ZZ = weight_ZZ / weight_tot;
    double factor_Z4 = weight_Z4 / weight_tot;
    
    temp_mu_central = factor_WW * .5 * (temp_wm.ET() + temp_wp.ET()) + factor_Z4 * temp_z4.ET();
    //    temp_mu_central = factor_WW * .5 * (temp_wm.ET() + temp_wp.ET()) + factor_ZZ * .5 * (temp_ze.ET() + temp_zn.ET()) + factor_Z4 * temp_z4.ET();
  }
  else if (sd == 8){ // previously 4
    fourvector temp_wm = PARTICLE("em")[0].momentum + PARTICLE("nex")[0].momentum;
    fourvector temp_wp = PARTICLE("mup")[0].momentum + PARTICLE("nm")[0].momentum;
    
    temp_mu_central = .5 * (temp_wm.ET() + temp_wp.ET());
  }
  else if (sd == 9){ // previously 5
    //    temp_mu_central = msi->M_W;
    
    double HThatlep = PARTICLE("missing")[0].ET;
    //    logger << LOG_DEBUG << "nu HThatlep = " << HThatlep << endl;
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      HThatlep = HThatlep + PARTICLE("lep")[i_l].pT;
      //      logger << LOG_DEBUG << i_l << "  HThatlep = " << HThatlep << endl;
    }
    temp_mu_central = HThatlep;
    //    logger << LOG_DEBUG << "temp_mu_central = " << temp_mu_central << endl;
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
