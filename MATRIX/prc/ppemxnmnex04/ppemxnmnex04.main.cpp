#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"

#include "ppemxnmnex04.contribution.set.hpp"
#include "ppemxnmnex04.event.set.hpp"
#include "ppemxnmnex04.phasespace.set.hpp"
#include "ppemxnmnex04.observable.set.hpp"
#include "ppemxnmnex04.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emmupvmve~+X");

  MUC->csi = new ppemxnmnex04_contribution_set();
  MUC->esi = new ppemxnmnex04_event_set();
  MUC->psi = new ppemxnmnex04_phasespace_set();
  MUC->osi = new ppemxnmnex04_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllll04_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppemxnmnex04_phasespace_set();
      MUC->psi->fake_psi->csi = new ppemxnmnex04_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppemxnmnex04_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
