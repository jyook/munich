#include "header.hpp"

#include "ppemxnmnex04.summary.hpp"

ppemxnmnex04_summary_generic::ppemxnmnex04_summary_generic(munich * xmunich){
  Logger logger("ppemxnmnex04_summary_generic::ppemxnmnex04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppemxnmnex04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppemxnmnex04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppemxnmnex04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppemxnmnex04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppemxnmnex04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppemxnmnex04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_born(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subprocess[4] = "aa_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subprocess[4] = "aa_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subprocess[4] = "aa_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmupvmve~";
    subprocess[2] = "uu~_emmupvmve~";
    subprocess[3] = "bb~_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmupvmve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmupvmve~d";
    subprocess[2] = "gu_emmupvmve~u";
    subprocess[3] = "gb_emmupvmve~b";
    subprocess[4] = "gd~_emmupvmve~d~";
    subprocess[5] = "gu~_emmupvmve~u~";
    subprocess[6] = "gb~_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~g";
    subprocess[8] = "uu~_emmupvmve~g";
    subprocess[9] = "bb~_emmupvmve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emmupvmve~g";
    subprocess[2] = "gd_emmupvmve~d";
    subprocess[3] = "gu_emmupvmve~u";
    subprocess[4] = "gb_emmupvmve~b";
    subprocess[5] = "gd~_emmupvmve~d~";
    subprocess[6] = "gu~_emmupvmve~u~";
    subprocess[7] = "gb~_emmupvmve~b~";
    subprocess[8] = "dd~_emmupvmve~g";
    subprocess[9] = "uu~_emmupvmve~g";
    subprocess[10] = "bb~_emmupvmve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emmupvmve~d";
    subprocess[2] = "ua_emmupvmve~u";
    subprocess[3] = "ba_emmupvmve~b";
    subprocess[4] = "d~a_emmupvmve~d~";
    subprocess[5] = "u~a_emmupvmve~u~";
    subprocess[6] = "b~a_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~a";
    subprocess[8] = "uu~_emmupvmve~a";
    subprocess[9] = "bb~_emmupvmve~a";
    subprocess[10] = "aa_emmupvmve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmupvmve~d";
    subprocess[2] = "gu_emmupvmve~u";
    subprocess[3] = "gb_emmupvmve~b";
    subprocess[4] = "gd~_emmupvmve~d~";
    subprocess[5] = "gu~_emmupvmve~u~";
    subprocess[6] = "gb~_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~g";
    subprocess[8] = "uu~_emmupvmve~g";
    subprocess[9] = "bb~_emmupvmve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emmupvmve~d";
    subprocess[2] = "ua_emmupvmve~u";
    subprocess[3] = "ba_emmupvmve~b";
    subprocess[4] = "d~a_emmupvmve~d~";
    subprocess[5] = "u~a_emmupvmve~u~";
    subprocess[6] = "b~a_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~a";
    subprocess[8] = "uu~_emmupvmve~a";
    subprocess[9] = "bb~_emmupvmve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmupvmve~d";
    subprocess[2] = "gu_emmupvmve~u";
    subprocess[3] = "gb_emmupvmve~b";
    subprocess[4] = "gd~_emmupvmve~d~";
    subprocess[5] = "gu~_emmupvmve~u~";
    subprocess[6] = "gb~_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~g";
    subprocess[8] = "uu~_emmupvmve~g";
    subprocess[9] = "bb~_emmupvmve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emmupvmve~d";
    subprocess[2] = "ua_emmupvmve~u";
    subprocess[3] = "ba_emmupvmve~b";
    subprocess[4] = "d~a_emmupvmve~d~";
    subprocess[5] = "u~a_emmupvmve~u~";
    subprocess[6] = "b~a_emmupvmve~b~";
    subprocess[7] = "dd~_emmupvmve~a";
    subprocess[8] = "uu~_emmupvmve~a";
    subprocess[9] = "bb~_emmupvmve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppemxnmnex04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(60);
    subprocess[1] = "gg_emmupvmve~dd~";
    subprocess[2] = "gg_emmupvmve~uu~";
    subprocess[3] = "gg_emmupvmve~bb~";
    subprocess[4] = "gd_emmupvmve~gd";
    subprocess[5] = "gu_emmupvmve~gu";
    subprocess[6] = "gb_emmupvmve~gb";
    subprocess[7] = "gd~_emmupvmve~gd~";
    subprocess[8] = "gu~_emmupvmve~gu~";
    subprocess[9] = "gb~_emmupvmve~gb~";
    subprocess[10] = "dd_emmupvmve~dd";
    subprocess[11] = "du_emmupvmve~du";
    subprocess[12] = "ds_emmupvmve~ds";
    subprocess[13] = "dc_emmupvmve~dc";
    subprocess[14] = "dc_emmupvmve~us";
    subprocess[15] = "db_emmupvmve~db";
    subprocess[16] = "dd~_emmupvmve~gg";
    subprocess[17] = "dd~_emmupvmve~dd~";
    subprocess[18] = "dd~_emmupvmve~uu~";
    subprocess[19] = "dd~_emmupvmve~ss~";
    subprocess[20] = "dd~_emmupvmve~cc~";
    subprocess[21] = "dd~_emmupvmve~bb~";
    subprocess[22] = "du~_emmupvmve~du~";
    subprocess[23] = "du~_emmupvmve~sc~";
    subprocess[24] = "ds~_emmupvmve~ds~";
    subprocess[25] = "ds~_emmupvmve~uc~";
    subprocess[26] = "dc~_emmupvmve~dc~";
    subprocess[27] = "db~_emmupvmve~db~";
    subprocess[28] = "uu_emmupvmve~uu";
    subprocess[29] = "uc_emmupvmve~uc";
    subprocess[30] = "ub_emmupvmve~ub";
    subprocess[31] = "ud~_emmupvmve~ud~";
    subprocess[32] = "ud~_emmupvmve~cs~";
    subprocess[33] = "uu~_emmupvmve~gg";
    subprocess[34] = "uu~_emmupvmve~dd~";
    subprocess[35] = "uu~_emmupvmve~uu~";
    subprocess[36] = "uu~_emmupvmve~ss~";
    subprocess[37] = "uu~_emmupvmve~cc~";
    subprocess[38] = "uu~_emmupvmve~bb~";
    subprocess[39] = "us~_emmupvmve~us~";
    subprocess[40] = "uc~_emmupvmve~ds~";
    subprocess[41] = "uc~_emmupvmve~uc~";
    subprocess[42] = "ub~_emmupvmve~ub~";
    subprocess[43] = "bb_emmupvmve~bb";
    subprocess[44] = "bd~_emmupvmve~bd~";
    subprocess[45] = "bu~_emmupvmve~bu~";
    subprocess[46] = "bb~_emmupvmve~gg";
    subprocess[47] = "bb~_emmupvmve~dd~";
    subprocess[48] = "bb~_emmupvmve~uu~";
    subprocess[49] = "bb~_emmupvmve~bb~";
    subprocess[50] = "d~d~_emmupvmve~d~d~";
    subprocess[51] = "d~u~_emmupvmve~d~u~";
    subprocess[52] = "d~s~_emmupvmve~d~s~";
    subprocess[53] = "d~c~_emmupvmve~d~c~";
    subprocess[54] = "d~c~_emmupvmve~u~s~";
    subprocess[55] = "d~b~_emmupvmve~d~b~";
    subprocess[56] = "u~u~_emmupvmve~u~u~";
    subprocess[57] = "u~c~_emmupvmve~u~c~";
    subprocess[58] = "u~b~_emmupvmve~u~b~";
    subprocess[59] = "b~b~_emmupvmve~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
