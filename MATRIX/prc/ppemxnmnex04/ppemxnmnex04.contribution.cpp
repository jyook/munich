#include "header.hpp"

#include "ppemxnmnex04.contribution.set.hpp"

ppemxnmnex04_contribution_set::~ppemxnmnex04_contribution_set(){
  static Logger logger("ppemxnmnex04_contribution_set::~ppemxnmnex04_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 6; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex  //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex  //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex  //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex  //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex  //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex  //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex  //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex  //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex  //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex  //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> e   mx  nm  nex  //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[7] = 7;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  22)){no_process_parton[i_a] = 24; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   5)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -1)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -3)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==  -5)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[7] ==   0)){no_process_parton[i_a] = 16; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   mx  nm  nex d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   mx  nm  nex s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   mx  nm  nex d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   mx  nm  nex s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   mx  nm  nex u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   mx  nm  nex c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   mx  nm  nex u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   mx  nm  nex c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   mx  nm  nex b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   mx  nm  nex b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   mx  nm  nex dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   mx  nm  nex sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   mx  nm  nex dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   mx  nm  nex sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   mx  nm  nex ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   mx  nm  nex cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   mx  nm  nex ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   mx  nm  nex cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   mx  nm  nex bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   mx  nm  nex bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 5 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> e   mx  nm  nex d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> e   mx  nm  nex s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> e   mx  nm  nex d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> e   mx  nm  nex s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> e   mx  nm  nex u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> e   mx  nm  nex c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> e   mx  nm  nex u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> e   mx  nm  nex c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> e   mx  nm  nex b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> e   mx  nm  nex b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> e   mx  nm  nex dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> e   mx  nm  nex sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> e   mx  nm  nex dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> e   mx  nm  nex sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   mx  nm  nex ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> e   mx  nm  nex cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> e   mx  nm  nex ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> e   mx  nm  nex cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> e   mx  nm  nex bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> e   mx  nm  nex bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex a    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> e   mx  nm  nex a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex g   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   mx  nm  nex d   //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   mx  nm  nex s   //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   mx  nm  nex d   //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   mx  nm  nex s   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   mx  nm  nex u   //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   mx  nm  nex c   //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   mx  nm  nex u   //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   mx  nm  nex c   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   mx  nm  nex b   //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   mx  nm  nex b   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   mx  nm  nex dx  //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   mx  nm  nex sx  //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   mx  nm  nex dx  //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   mx  nm  nex sx  //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   mx  nm  nex ux  //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   mx  nm  nex cx  //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   mx  nm  nex ux  //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   mx  nm  nex cx  //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   mx  nm  nex bx  //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   mx  nm  nex bx  //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex g   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex g   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex g   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex g   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex g   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex g   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex g   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex g   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex g   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex g   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -3)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -1)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -5) && (tp[out[8]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 46){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemxnmnex04_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppemxnmnex04_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   mx  nm  nex b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   mx  nm  nex g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   mx  nm  nex g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   mx  nm  nex g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   mx  nm  nex g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   mx  nm  nex g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   mx  nm  nex g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   mx  nm  nex g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   mx  nm  nex g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   mx  nm  nex g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   mx  nm  nex g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   mx  nm  nex g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   mx  nm  nex g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   mx  nm  nex g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   mx  nm  nex g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   mx  nm  nex g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   mx  nm  nex g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   mx  nm  nex g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   mx  nm  nex g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   mx  nm  nex g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   mx  nm  nex g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> e   mx  nm  nex d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> e   mx  nm  nex s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   mx  nm  nex d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> e   mx  nm  nex s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> e   mx  nm  nex d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> e   mx  nm  nex s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   mx  nm  nex d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> e   mx  nm  nex d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   mx  nm  nex d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> e   mx  nm  nex u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> e   mx  nm  nex u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> e   mx  nm  nex d   c    //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   mx  nm  nex u   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> e   mx  nm  nex d   c    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> e   mx  nm  nex d   c    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> e   mx  nm  nex u   s    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   mx  nm  nex d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> e   mx  nm  nex s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> e   mx  nm  nex d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> e   mx  nm  nex s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   mx  nm  nex b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   mx  nm  nex b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   mx  nm  nex b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   mx  nm  nex b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   mx  nm  nex d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   mx  nm  nex s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   mx  nm  nex d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   mx  nm  nex s   cx   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   mx  nm  nex s   cx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   mx  nm  nex d   ux   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   mx  nm  nex s   cx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   mx  nm  nex d   ux   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   mx  nm  nex d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> e   mx  nm  nex s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> e   mx  nm  nex s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> e   mx  nm  nex d   sx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   mx  nm  nex u   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> e   mx  nm  nex c   ux   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> e   mx  nm  nex c   ux   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> e   mx  nm  nex u   cx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   mx  nm  nex d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> e   mx  nm  nex s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> e   mx  nm  nex s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> e   mx  nm  nex d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> e   mx  nm  nex d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> e   mx  nm  nex s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> e   mx  nm  nex d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> e   mx  nm  nex s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> e   mx  nm  nex u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> e   mx  nm  nex c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> e   mx  nm  nex u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> e   mx  nm  nex u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> e   mx  nm  nex u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> e   mx  nm  nex c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> e   mx  nm  nex u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> e   mx  nm  nex c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   mx  nm  nex u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   mx  nm  nex c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   mx  nm  nex u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   mx  nm  nex c   sx   //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   mx  nm  nex c   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   mx  nm  nex u   dx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   mx  nm  nex c   sx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   mx  nm  nex u   dx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   mx  nm  nex b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   mx  nm  nex b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   mx  nm  nex b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   mx  nm  nex b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> e   mx  nm  nex u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> e   mx  nm  nex c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> e   mx  nm  nex c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> e   mx  nm  nex u   sx   //
    }
    else if (no_process_parton[i_a] == 46){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   mx  nm  nex d   sx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> e   mx  nm  nex s   dx   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> e   mx  nm  nex s   dx   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> e   mx  nm  nex d   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   mx  nm  nex u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> e   mx  nm  nex c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> e   mx  nm  nex c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> e   mx  nm  nex u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> e   mx  nm  nex u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> e   mx  nm  nex c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> e   mx  nm  nex u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> e   mx  nm  nex c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> e   mx  nm  nex b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> e   mx  nm  nex b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> e   mx  nm  nex b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> e   mx  nm  nex b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> e   mx  nm  nex b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   mx  nm  nex b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> e   mx  nm  nex b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> e   mx  nm  nex b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> e   mx  nm  nex b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   mx  nm  nex b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   mx  nm  nex b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> e   mx  nm  nex dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> e   mx  nm  nex sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   mx  nm  nex dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> e   mx  nm  nex sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> e   mx  nm  nex dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> e   mx  nm  nex sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> e   mx  nm  nex dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> e   mx  nm  nex dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   mx  nm  nex dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> e   mx  nm  nex ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> e   mx  nm  nex ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> e   mx  nm  nex dx  cx   //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   mx  nm  nex ux  sx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> e   mx  nm  nex dx  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> e   mx  nm  nex dx  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> e   mx  nm  nex ux  sx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> e   mx  nm  nex dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> e   mx  nm  nex sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> e   mx  nm  nex dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> e   mx  nm  nex sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   mx  nm  nex ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> e   mx  nm  nex cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   mx  nm  nex ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> e   mx  nm  nex ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   mx  nm  nex ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> e   mx  nm  nex cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> e   mx  nm  nex ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> e   mx  nm  nex cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> e   mx  nm  nex bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
