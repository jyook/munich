#include "header.hpp"

#include "ppenexh03.summary.hpp"

ppenexh03_summary_generic::ppenexh03_summary_generic(munich * xmunich){
  Logger logger("ppenexh03_summary_generic::ppenexh03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppenexh03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppenexh03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppenexh03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppenexh03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppenexh03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppenexh03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_born(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_emve~h";
    subprocess[2] = "dc~_emve~h";
    subprocess[3] = "su~_emve~h";
    subprocess[4] = "sc~_emve~h";
    subprocess[5] = "bu~_emve~h";
    subprocess[6] = "bc~_emve~h";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gd_emve~hc";
    subprocess[3] = "gs_emve~hu";
    subprocess[4] = "gs_emve~hc";
    subprocess[5] = "gb_emve~hu";
    subprocess[6] = "gb_emve~hc";
    subprocess[7] = "gu~_emve~hd~";
    subprocess[8] = "gu~_emve~hs~";
    subprocess[9] = "gu~_emve~hb~";
    subprocess[10] = "gc~_emve~hd~";
    subprocess[11] = "gc~_emve~hs~";
    subprocess[12] = "gc~_emve~hb~";
    subprocess[13] = "du~_emve~hg";
    subprocess[14] = "dc~_emve~hg";
    subprocess[15] = "su~_emve~hg";
    subprocess[16] = "sc~_emve~hg";
    subprocess[17] = "bu~_emve~hg";
    subprocess[18] = "bc~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "da_emve~hc";
    subprocess[3] = "sa_emve~hu";
    subprocess[4] = "sa_emve~hc";
    subprocess[5] = "ba_emve~hu";
    subprocess[6] = "ba_emve~hc";
    subprocess[7] = "u~a_emve~hd~";
    subprocess[8] = "u~a_emve~hs~";
    subprocess[9] = "u~a_emve~hb~";
    subprocess[10] = "c~a_emve~hd~";
    subprocess[11] = "c~a_emve~hs~";
    subprocess[12] = "c~a_emve~hb~";
    subprocess[13] = "du~_emve~ha";
    subprocess[14] = "dc~_emve~ha";
    subprocess[15] = "su~_emve~ha";
    subprocess[16] = "sc~_emve~ha";
    subprocess[17] = "bu~_emve~ha";
    subprocess[18] = "bc~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gd_emve~hc";
    subprocess[3] = "gs_emve~hu";
    subprocess[4] = "gs_emve~hc";
    subprocess[5] = "gb_emve~hu";
    subprocess[6] = "gb_emve~hc";
    subprocess[7] = "gu~_emve~hd~";
    subprocess[8] = "gu~_emve~hs~";
    subprocess[9] = "gu~_emve~hb~";
    subprocess[10] = "gc~_emve~hd~";
    subprocess[11] = "gc~_emve~hs~";
    subprocess[12] = "gc~_emve~hb~";
    subprocess[13] = "du~_emve~hg";
    subprocess[14] = "dc~_emve~hg";
    subprocess[15] = "su~_emve~hg";
    subprocess[16] = "sc~_emve~hg";
    subprocess[17] = "bu~_emve~hg";
    subprocess[18] = "bc~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "da_emve~hc";
    subprocess[3] = "sa_emve~hu";
    subprocess[4] = "sa_emve~hc";
    subprocess[5] = "ba_emve~hu";
    subprocess[6] = "ba_emve~hc";
    subprocess[7] = "u~a_emve~hd~";
    subprocess[8] = "u~a_emve~hs~";
    subprocess[9] = "u~a_emve~hb~";
    subprocess[10] = "c~a_emve~hd~";
    subprocess[11] = "c~a_emve~hs~";
    subprocess[12] = "c~a_emve~hb~";
    subprocess[13] = "du~_emve~ha";
    subprocess[14] = "dc~_emve~ha";
    subprocess[15] = "su~_emve~ha";
    subprocess[16] = "sc~_emve~ha";
    subprocess[17] = "bu~_emve~ha";
    subprocess[18] = "bc~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gd_emve~hc";
    subprocess[3] = "gs_emve~hu";
    subprocess[4] = "gs_emve~hc";
    subprocess[5] = "gb_emve~hu";
    subprocess[6] = "gb_emve~hc";
    subprocess[7] = "gu~_emve~hd~";
    subprocess[8] = "gu~_emve~hs~";
    subprocess[9] = "gu~_emve~hb~";
    subprocess[10] = "gc~_emve~hd~";
    subprocess[11] = "gc~_emve~hs~";
    subprocess[12] = "gc~_emve~hb~";
    subprocess[13] = "du~_emve~hg";
    subprocess[14] = "dc~_emve~hg";
    subprocess[15] = "su~_emve~hg";
    subprocess[16] = "sc~_emve~hg";
    subprocess[17] = "bu~_emve~hg";
    subprocess[18] = "bc~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "da_emve~hc";
    subprocess[3] = "sa_emve~hu";
    subprocess[4] = "sa_emve~hc";
    subprocess[5] = "ba_emve~hu";
    subprocess[6] = "ba_emve~hc";
    subprocess[7] = "u~a_emve~hd~";
    subprocess[8] = "u~a_emve~hs~";
    subprocess[9] = "u~a_emve~hb~";
    subprocess[10] = "c~a_emve~hd~";
    subprocess[11] = "c~a_emve~hs~";
    subprocess[12] = "c~a_emve~hb~";
    subprocess[13] = "du~_emve~ha";
    subprocess[14] = "dc~_emve~ha";
    subprocess[15] = "su~_emve~ha";
    subprocess[16] = "sc~_emve~ha";
    subprocess[17] = "bu~_emve~ha";
    subprocess[18] = "bc~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppenexh03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_emve~hud~";
    subprocess[2] = "gg_emve~hus~";
    subprocess[3] = "gg_emve~hub~";
    subprocess[4] = "gg_emve~hcd~";
    subprocess[5] = "gg_emve~hcs~";
    subprocess[6] = "gg_emve~hcb~";
    subprocess[7] = "gd_emve~hgu";
    subprocess[8] = "gd_emve~hgc";
    subprocess[9] = "gs_emve~hgu";
    subprocess[10] = "gs_emve~hgc";
    subprocess[11] = "gb_emve~hgu";
    subprocess[12] = "gb_emve~hgc";
    subprocess[13] = "gu~_emve~hgd~";
    subprocess[14] = "gu~_emve~hgs~";
    subprocess[15] = "gu~_emve~hgb~";
    subprocess[16] = "gc~_emve~hgd~";
    subprocess[17] = "gc~_emve~hgs~";
    subprocess[18] = "gc~_emve~hgb~";
    subprocess[19] = "dd_emve~hdu";
    subprocess[20] = "dd_emve~hdc";
    subprocess[21] = "du_emve~huu";
    subprocess[22] = "du_emve~huc";
    subprocess[23] = "ds_emve~hdu";
    subprocess[24] = "ds_emve~hdc";
    subprocess[25] = "ds_emve~hus";
    subprocess[26] = "ds_emve~hsc";
    subprocess[27] = "dc_emve~huc";
    subprocess[28] = "dc_emve~hcc";
    subprocess[29] = "db_emve~hdu";
    subprocess[30] = "db_emve~hdc";
    subprocess[31] = "db_emve~hub";
    subprocess[32] = "db_emve~hcb";
    subprocess[33] = "dd~_emve~hud~";
    subprocess[34] = "dd~_emve~hus~";
    subprocess[35] = "dd~_emve~hub~";
    subprocess[36] = "dd~_emve~hcd~";
    subprocess[37] = "dd~_emve~hcs~";
    subprocess[38] = "dd~_emve~hcb~";
    subprocess[39] = "du~_emve~hgg";
    subprocess[40] = "du~_emve~hdd~";
    subprocess[41] = "du~_emve~hds~";
    subprocess[42] = "du~_emve~hdb~";
    subprocess[43] = "du~_emve~huu~";
    subprocess[44] = "du~_emve~hss~";
    subprocess[45] = "du~_emve~hcu~";
    subprocess[46] = "du~_emve~hcc~";
    subprocess[47] = "du~_emve~hbb~";
    subprocess[48] = "ds~_emve~hus~";
    subprocess[49] = "ds~_emve~hcs~";
    subprocess[50] = "dc~_emve~hgg";
    subprocess[51] = "dc~_emve~hdd~";
    subprocess[52] = "dc~_emve~hds~";
    subprocess[53] = "dc~_emve~hdb~";
    subprocess[54] = "dc~_emve~huu~";
    subprocess[55] = "dc~_emve~huc~";
    subprocess[56] = "dc~_emve~hss~";
    subprocess[57] = "dc~_emve~hcc~";
    subprocess[58] = "dc~_emve~hbb~";
    subprocess[59] = "db~_emve~hub~";
    subprocess[60] = "db~_emve~hcb~";
    subprocess[61] = "us_emve~huu";
    subprocess[62] = "us_emve~huc";
    subprocess[63] = "ub_emve~huu";
    subprocess[64] = "ub_emve~huc";
    subprocess[65] = "uu~_emve~hud~";
    subprocess[66] = "uu~_emve~hus~";
    subprocess[67] = "uu~_emve~hub~";
    subprocess[68] = "uu~_emve~hcd~";
    subprocess[69] = "uu~_emve~hcs~";
    subprocess[70] = "uu~_emve~hcb~";
    subprocess[71] = "uc~_emve~hud~";
    subprocess[72] = "uc~_emve~hus~";
    subprocess[73] = "uc~_emve~hub~";
    subprocess[74] = "ss_emve~hus";
    subprocess[75] = "ss_emve~hsc";
    subprocess[76] = "sc_emve~huc";
    subprocess[77] = "sc_emve~hcc";
    subprocess[78] = "sb_emve~hus";
    subprocess[79] = "sb_emve~hub";
    subprocess[80] = "sb_emve~hsc";
    subprocess[81] = "sb_emve~hcb";
    subprocess[82] = "sd~_emve~hud~";
    subprocess[83] = "sd~_emve~hcd~";
    subprocess[84] = "su~_emve~hgg";
    subprocess[85] = "su~_emve~hdd~";
    subprocess[86] = "su~_emve~huu~";
    subprocess[87] = "su~_emve~hsd~";
    subprocess[88] = "su~_emve~hss~";
    subprocess[89] = "su~_emve~hsb~";
    subprocess[90] = "su~_emve~hcu~";
    subprocess[91] = "su~_emve~hcc~";
    subprocess[92] = "su~_emve~hbb~";
    subprocess[93] = "ss~_emve~hud~";
    subprocess[94] = "ss~_emve~hus~";
    subprocess[95] = "ss~_emve~hub~";
    subprocess[96] = "ss~_emve~hcd~";
    subprocess[97] = "ss~_emve~hcs~";
    subprocess[98] = "ss~_emve~hcb~";
    subprocess[99] = "sc~_emve~hgg";
    subprocess[100] = "sc~_emve~hdd~";
    subprocess[101] = "sc~_emve~huu~";
    subprocess[102] = "sc~_emve~huc~";
    subprocess[103] = "sc~_emve~hsd~";
    subprocess[104] = "sc~_emve~hss~";
    subprocess[105] = "sc~_emve~hsb~";
    subprocess[106] = "sc~_emve~hcc~";
    subprocess[107] = "sc~_emve~hbb~";
    subprocess[108] = "sb~_emve~hub~";
    subprocess[109] = "sb~_emve~hcb~";
    subprocess[110] = "cb_emve~huc";
    subprocess[111] = "cb_emve~hcc";
    subprocess[112] = "cu~_emve~hcd~";
    subprocess[113] = "cu~_emve~hcs~";
    subprocess[114] = "cu~_emve~hcb~";
    subprocess[115] = "cc~_emve~hud~";
    subprocess[116] = "cc~_emve~hus~";
    subprocess[117] = "cc~_emve~hub~";
    subprocess[118] = "cc~_emve~hcd~";
    subprocess[119] = "cc~_emve~hcs~";
    subprocess[120] = "cc~_emve~hcb~";
    subprocess[121] = "bb_emve~hub";
    subprocess[122] = "bb_emve~hcb";
    subprocess[123] = "bd~_emve~hud~";
    subprocess[124] = "bd~_emve~hcd~";
    subprocess[125] = "bu~_emve~hgg";
    subprocess[126] = "bu~_emve~hdd~";
    subprocess[127] = "bu~_emve~huu~";
    subprocess[128] = "bu~_emve~hss~";
    subprocess[129] = "bu~_emve~hcu~";
    subprocess[130] = "bu~_emve~hcc~";
    subprocess[131] = "bu~_emve~hbd~";
    subprocess[132] = "bu~_emve~hbs~";
    subprocess[133] = "bu~_emve~hbb~";
    subprocess[134] = "bs~_emve~hus~";
    subprocess[135] = "bs~_emve~hcs~";
    subprocess[136] = "bc~_emve~hgg";
    subprocess[137] = "bc~_emve~hdd~";
    subprocess[138] = "bc~_emve~huu~";
    subprocess[139] = "bc~_emve~huc~";
    subprocess[140] = "bc~_emve~hss~";
    subprocess[141] = "bc~_emve~hcc~";
    subprocess[142] = "bc~_emve~hbd~";
    subprocess[143] = "bc~_emve~hbs~";
    subprocess[144] = "bc~_emve~hbb~";
    subprocess[145] = "bb~_emve~hud~";
    subprocess[146] = "bb~_emve~hus~";
    subprocess[147] = "bb~_emve~hub~";
    subprocess[148] = "bb~_emve~hcd~";
    subprocess[149] = "bb~_emve~hcs~";
    subprocess[150] = "bb~_emve~hcb~";
    subprocess[151] = "d~u~_emve~hd~d~";
    subprocess[152] = "d~u~_emve~hd~s~";
    subprocess[153] = "d~u~_emve~hd~b~";
    subprocess[154] = "d~c~_emve~hd~d~";
    subprocess[155] = "d~c~_emve~hd~s~";
    subprocess[156] = "d~c~_emve~hd~b~";
    subprocess[157] = "u~u~_emve~hd~u~";
    subprocess[158] = "u~u~_emve~hu~s~";
    subprocess[159] = "u~u~_emve~hu~b~";
    subprocess[160] = "u~s~_emve~hd~s~";
    subprocess[161] = "u~s~_emve~hs~s~";
    subprocess[162] = "u~s~_emve~hs~b~";
    subprocess[163] = "u~c~_emve~hd~u~";
    subprocess[164] = "u~c~_emve~hd~c~";
    subprocess[165] = "u~c~_emve~hu~s~";
    subprocess[166] = "u~c~_emve~hu~b~";
    subprocess[167] = "u~c~_emve~hs~c~";
    subprocess[168] = "u~c~_emve~hc~b~";
    subprocess[169] = "u~b~_emve~hd~b~";
    subprocess[170] = "u~b~_emve~hs~b~";
    subprocess[171] = "u~b~_emve~hb~b~";
    subprocess[172] = "s~c~_emve~hd~s~";
    subprocess[173] = "s~c~_emve~hs~s~";
    subprocess[174] = "s~c~_emve~hs~b~";
    subprocess[175] = "c~c~_emve~hd~c~";
    subprocess[176] = "c~c~_emve~hs~c~";
    subprocess[177] = "c~c~_emve~hc~b~";
    subprocess[178] = "c~b~_emve~hd~b~";
    subprocess[179] = "c~b~_emve~hs~b~";
    subprocess[180] = "c~b~_emve~hb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
