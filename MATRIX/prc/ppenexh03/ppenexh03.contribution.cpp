#include "header.hpp"

#include "ppenexh03.contribution.set.hpp"

ppenexh03_contribution_set::~ppenexh03_contribution_set(){
  static Logger logger("ppenexh03_contribution_set::~ppenexh03_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppenexh03_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppenexh03_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppenexh03_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[6] = 6;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   0)){no_process_parton[i_a] = 22; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==   4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -1)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -3)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -5)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -1)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  -5)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 33; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[6] ==  22)){no_process_parton[i_a] = 36; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 33){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppenexh03_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex h   u    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex h   c    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> e   nex h   u    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> e   nex h   c    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   nex h   u    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   nex h   c    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   dx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   dx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   sx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   bx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   bx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   dx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   dx   //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   sx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   sx   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   bx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   bx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   g    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   g    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   g    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   g    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   g    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   g    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   g    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   g    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   g    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 4 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> e   nex h   u    //
      combination_pdf[1] = {-1,   1,   7};   // a   d    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> e   nex h   c    //
      combination_pdf[1] = {-1,   1,   7};   // a   d    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   7};   // s   a    -> e   nex h   u    //
      combination_pdf[1] = {-1,   3,   7};   // a   s    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   7};   // s   a    -> e   nex h   c    //
      combination_pdf[1] = {-1,   3,   7};   // a   s    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> e   nex h   u    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> e   nex h   u    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> e   nex h   c    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> e   nex h   c    //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   nex h   dx   //
      combination_pdf[1] = {-1,  -2,   7};   // a   ux   -> e   nex h   dx   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   nex h   sx   //
      combination_pdf[1] = {-1,  -2,   7};   // a   ux   -> e   nex h   sx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   nex h   bx   //
      combination_pdf[1] = {-1,  -2,   7};   // a   ux   -> e   nex h   bx   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,   7};   // cx  a    -> e   nex h   dx   //
      combination_pdf[1] = {-1,  -4,   7};   // a   cx   -> e   nex h   dx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,   7};   // cx  a    -> e   nex h   sx   //
      combination_pdf[1] = {-1,  -4,   7};   // a   cx   -> e   nex h   sx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,   7};   // cx  a    -> e   nex h   bx   //
      combination_pdf[1] = {-1,  -4,   7};   // a   cx   -> e   nex h   bx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   a    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   a    //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   a    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   a    //
    }
    else if (no_process_parton[i_a] == 33){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   a    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   a    //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   a    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   a    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   a    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   a    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   a    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppenexh03_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[6] = 6; out[7] = 7;}
      if (o == 1){out[6] = 7; out[7] = 6;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   3)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 49; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 57; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 64; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 68; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 70; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 73; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 75; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 76; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 77; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 78; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 79; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 80; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 82; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 85; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 88; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 91; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 94; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 97; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 98; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   2)){no_process_parton[i_a] = 100; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 101; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 103; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 104; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 105; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 106; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 107; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 108; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 109; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 110; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 111; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   3)){no_process_parton[i_a] = 117; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 119; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 122; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 123; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   3)){no_process_parton[i_a] = 126; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 127; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 128; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 129; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 130; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 133; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 136; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 137; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 140; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 142; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 143; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 144; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 145; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 146; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 149; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 150; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 151; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 152; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 153; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 154; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 155; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 156; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 157; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 160; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 161; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 162; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 163; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 164; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 166; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 169; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 172; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 175; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 179; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   4)){no_process_parton[i_a] = 180; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 184; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 185; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 186; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 187; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 188; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 189; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 190; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 191; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 192; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 196; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==   5)){no_process_parton[i_a] = 198; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 199; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 202; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 205; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 206; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 209; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 212; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 214; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 215; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 216; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 217; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 218; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 220; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 223; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_process_parton[i_a] = 225; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 226; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 229; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 230; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 232; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 235; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 236; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 237; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 238; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 239; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 240; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 241; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 242; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 243; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 244; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 247; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 248; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 249; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -1)){no_process_parton[i_a] = 253; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 254; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 255; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 259; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 261; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 262; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 266; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 268; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 269; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -2)){no_process_parton[i_a] = 271; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 272; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 273; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 274; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 275; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 276; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 279; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 281; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 282; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 287; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -3)){no_process_parton[i_a] = 289; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 290; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 293; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -4)){no_process_parton[i_a] = 296; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 297; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 300; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 302; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  25) && (tp[out[6]] ==  -5) && (tp[out[7]] ==  -5)){no_process_parton[i_a] = 303; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 46){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 49){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 57){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 64){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 68){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 70){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 73){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 75){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 76){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 77){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 78){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 79){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 80){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 82){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 85){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 88){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 91){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 94){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 97){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 98){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 100){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 101){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 103){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 104){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 105){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 106){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 107){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 108){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 109){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 110){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 111){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 117){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 119){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 122){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 123){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 126){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 127){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 128){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 129){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 130){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 133){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 136){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 137){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 140){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 142){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 143){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 144){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 145){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 146){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 149){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 150){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 151){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 152){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 153){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 154){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 155){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 156){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 157){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 160){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 161){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 162){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 163){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 164){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 166){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 169){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 172){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 175){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 179){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 180){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 184){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 185){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 186){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 187){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 188){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 189){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 190){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 191){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 192){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 196){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 198){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 199){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 202){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 205){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 206){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 209){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 212){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 214){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 215){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 216){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 217){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 218){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 220){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 223){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 225){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 226){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 229){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 230){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 232){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 235){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 236){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 237){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 238){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 239){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 240){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 241){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 242){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 243){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 244){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 247){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 248){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 249){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 253){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 254){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 255){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 259){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 261){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 262){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 266){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 268){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 269){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 271){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 272){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 273){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 274){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 275){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 276){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 279){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 281){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 282){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 287){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 289){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 290){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 293){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 296){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 297){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 300){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 302){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 303){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppenexh03_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex h   g   u    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> e   nex h   g   u    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex h   g   c    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> e   nex h   g   c    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> e   nex h   g   u    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> e   nex h   g   u    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> e   nex h   g   c    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> e   nex h   g   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   nex h   g   u    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   nex h   g   u    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   nex h   g   c    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   nex h   g   c    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   g   dx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   g   dx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   g   sx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   g   sx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex h   g   bx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> e   nex h   g   bx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   g   dx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   g   dx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   g   sx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   g   sx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> e   nex h   g   bx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> e   nex h   g   bx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> e   nex h   d   u    //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> e   nex h   d   c    //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   nex h   u   u    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> e   nex h   u   u    //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   nex h   d   u    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> e   nex h   d   u    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   nex h   d   c    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> e   nex h   d   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   nex h   u   s    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> e   nex h   u   s    //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   nex h   s   c    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> e   nex h   s   c    //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   nex h   c   c    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> e   nex h   c   c    //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   nex h   d   u    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> e   nex h   d   u    //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   nex h   d   c    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> e   nex h   d   c    //
    }
    else if (no_process_parton[i_a] == 46){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   nex h   u   b    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> e   nex h   u   b    //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   nex h   c   b    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> e   nex h   c   b    //
    }
    else if (no_process_parton[i_a] == 49){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 57){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   d   sx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   d   sx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   d   bx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   d   bx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 64){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   c   ux   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   c   ux   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 68){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 70){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 73){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 75){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 76){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 77){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   d   sx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   d   sx   //
    }
    else if (no_process_parton[i_a] == 78){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   d   bx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   d   bx   //
    }
    else if (no_process_parton[i_a] == 79){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 80){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   u   cx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   u   cx   //
    }
    else if (no_process_parton[i_a] == 82){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 85){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 88){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 91){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 94){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 97){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> e   nex h   u   u    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> e   nex h   u   u    //
    }
    else if (no_process_parton[i_a] == 98){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 100){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> e   nex h   u   u    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> e   nex h   u   u    //
    }
    else if (no_process_parton[i_a] == 101){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 103){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 104){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 105){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 106){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 107){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 108){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 109){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 110){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 111){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 117){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   3,   3};   // s   s    -> e   nex h   u   s    //
    }
    else if (no_process_parton[i_a] == 119){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   3,   3};   // s   s    -> e   nex h   s   c    //
    }
    else if (no_process_parton[i_a] == 122){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 123){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> e   nex h   c   c    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> e   nex h   c   c    //
    }
    else if (no_process_parton[i_a] == 126){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> e   nex h   u   s    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> e   nex h   u   s    //
    }
    else if (no_process_parton[i_a] == 127){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> e   nex h   u   b    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> e   nex h   u   b    //
    }
    else if (no_process_parton[i_a] == 128){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> e   nex h   s   c    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> e   nex h   s   c    //
    }
    else if (no_process_parton[i_a] == 129){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> e   nex h   c   b    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> e   nex h   c   b    //
    }
    else if (no_process_parton[i_a] == 130){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 133){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 136){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 137){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 140){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 142){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   s   dx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   s   dx   //
    }
    else if (no_process_parton[i_a] == 143){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 144){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   s   bx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   s   bx   //
    }
    else if (no_process_parton[i_a] == 145){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   c   ux   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   c   ux   //
    }
    else if (no_process_parton[i_a] == 146){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 149){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 150){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 151){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 152){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 153){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 154){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 155){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 156){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 157){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 160){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 161){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   u   cx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   u   cx   //
    }
    else if (no_process_parton[i_a] == 162){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   s   dx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   s   dx   //
    }
    else if (no_process_parton[i_a] == 163){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 164){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   s   bx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   s   bx   //
    }
    else if (no_process_parton[i_a] == 166){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 169){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 172){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 175){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 179){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> e   nex h   u   c    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> e   nex h   u   c    //
    }
    else if (no_process_parton[i_a] == 180){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> e   nex h   c   c    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> e   nex h   c   c    //
    }
    else if (no_process_parton[i_a] == 184){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 185){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 186){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 187){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 188){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 189){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 190){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 191){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 192){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 196){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> e   nex h   u   b    //
    }
    else if (no_process_parton[i_a] == 198){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> e   nex h   c   b    //
    }
    else if (no_process_parton[i_a] == 199){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 202){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 205){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 206){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 209){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 212){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 214){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   c   ux   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   c   ux   //
    }
    else if (no_process_parton[i_a] == 215){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 216){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   b   dx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   b   dx   //
    }
    else if (no_process_parton[i_a] == 217){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   b   sx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   b   sx   //
    }
    else if (no_process_parton[i_a] == 218){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 220){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 223){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 225){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   g   g    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   g   g    //
    }
    else if (no_process_parton[i_a] == 226){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   d   dx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   d   dx   //
    }
    else if (no_process_parton[i_a] == 229){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   u   ux   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   u   ux   //
    }
    else if (no_process_parton[i_a] == 230){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   u   cx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   u   cx   //
    }
    else if (no_process_parton[i_a] == 232){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   s   sx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   s   sx   //
    }
    else if (no_process_parton[i_a] == 235){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   c   cx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   c   cx   //
    }
    else if (no_process_parton[i_a] == 236){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   b   dx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   b   dx   //
    }
    else if (no_process_parton[i_a] == 237){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   b   sx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   b   sx   //
    }
    else if (no_process_parton[i_a] == 238){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> e   nex h   b   bx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> e   nex h   b   bx   //
    }
    else if (no_process_parton[i_a] == 239){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   u   dx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   u   dx   //
    }
    else if (no_process_parton[i_a] == 240){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   u   sx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   u   sx   //
    }
    else if (no_process_parton[i_a] == 241){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   u   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   u   bx   //
    }
    else if (no_process_parton[i_a] == 242){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   c   dx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   c   dx   //
    }
    else if (no_process_parton[i_a] == 243){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   c   sx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   c   sx   //
    }
    else if (no_process_parton[i_a] == 244){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex h   c   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   nex h   c   bx   //
    }
    else if (no_process_parton[i_a] == 247){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   nex h   dx  dx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> e   nex h   dx  dx   //
    }
    else if (no_process_parton[i_a] == 248){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   nex h   dx  sx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> e   nex h   dx  sx   //
    }
    else if (no_process_parton[i_a] == 249){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   nex h   dx  bx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> e   nex h   dx  bx   //
    }
    else if (no_process_parton[i_a] == 253){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   nex h   dx  dx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> e   nex h   dx  dx   //
    }
    else if (no_process_parton[i_a] == 254){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   nex h   dx  sx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> e   nex h   dx  sx   //
    }
    else if (no_process_parton[i_a] == 255){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   nex h   dx  bx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> e   nex h   dx  bx   //
    }
    else if (no_process_parton[i_a] == 259){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   nex h   dx  ux   //
    }
    else if (no_process_parton[i_a] == 261){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   nex h   ux  sx   //
    }
    else if (no_process_parton[i_a] == 262){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   nex h   ux  bx   //
    }
    else if (no_process_parton[i_a] == 266){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> e   nex h   dx  sx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> e   nex h   dx  sx   //
    }
    else if (no_process_parton[i_a] == 268){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> e   nex h   sx  sx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> e   nex h   sx  sx   //
    }
    else if (no_process_parton[i_a] == 269){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> e   nex h   sx  bx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> e   nex h   sx  bx   //
    }
    else if (no_process_parton[i_a] == 271){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   dx  ux   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   dx  ux   //
    }
    else if (no_process_parton[i_a] == 272){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   dx  cx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   dx  cx   //
    }
    else if (no_process_parton[i_a] == 273){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   ux  sx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   ux  sx   //
    }
    else if (no_process_parton[i_a] == 274){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   ux  bx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   ux  bx   //
    }
    else if (no_process_parton[i_a] == 275){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   sx  cx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   sx  cx   //
    }
    else if (no_process_parton[i_a] == 276){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex h   cx  bx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> e   nex h   cx  bx   //
    }
    else if (no_process_parton[i_a] == 279){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   nex h   dx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> e   nex h   dx  bx   //
    }
    else if (no_process_parton[i_a] == 281){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   nex h   sx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> e   nex h   sx  bx   //
    }
    else if (no_process_parton[i_a] == 282){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   nex h   bx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> e   nex h   bx  bx   //
    }
    else if (no_process_parton[i_a] == 287){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> e   nex h   dx  sx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> e   nex h   dx  sx   //
    }
    else if (no_process_parton[i_a] == 289){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> e   nex h   sx  sx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> e   nex h   sx  sx   //
    }
    else if (no_process_parton[i_a] == 290){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> e   nex h   sx  bx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> e   nex h   sx  bx   //
    }
    else if (no_process_parton[i_a] == 293){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> e   nex h   dx  cx   //
    }
    else if (no_process_parton[i_a] == 296){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> e   nex h   sx  cx   //
    }
    else if (no_process_parton[i_a] == 297){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> e   nex h   cx  bx   //
    }
    else if (no_process_parton[i_a] == 300){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> e   nex h   dx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> e   nex h   dx  bx   //
    }
    else if (no_process_parton[i_a] == 302){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> e   nex h   sx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> e   nex h   sx  bx   //
    }
    else if (no_process_parton[i_a] == 303){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> e   nex h   bx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> e   nex h   bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
