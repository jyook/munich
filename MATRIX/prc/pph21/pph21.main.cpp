#include "header.hpp"

#include "pph21.amplitude.set.hpp"

#include "pph21.contribution.set.hpp"
#include "pph21.event.set.hpp"
#include "pph21.phasespace.set.hpp"
#include "pph21.observable.set.hpp"
#include "pph21.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-h+X");

  MUC->csi = new pph21_contribution_set();
  MUC->esi = new pph21_event_set();
  MUC->psi = new pph21_phasespace_set();
  MUC->osi = new pph21_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    pph21_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new pph21_phasespace_set();
      MUC->psi->fake_psi->csi = new pph21_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new pph21_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
