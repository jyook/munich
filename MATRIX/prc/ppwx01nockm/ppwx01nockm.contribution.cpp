#include "header.hpp"

#include "ppwx01nockm.contribution.set.hpp"

ppwx01nockm_contribution_set::~ppwx01nockm_contribution_set(){
  static Logger logger("ppwx01nockm_contribution_set::~ppwx01nockm_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 3; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> wx  s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> wx  d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> wx  s    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> wx  cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> wx  ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> wx  cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  g    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 24; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppwx01nockm_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  d   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  g   d    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> wx  g   s    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> wx  g   d    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> wx  g   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  g   ux   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> wx  g   cx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> wx  g   ux   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> wx  g   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> wx  d   d    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> wx  s   s    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> wx  d   d    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> wx  s   s    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  d   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> wx  d   s    //
      combination_pdf[2] = { 1,   5,   2};   // b   u    -> wx  d   b    //
      combination_pdf[3] = { 1,   5,   4};   // b   c    -> wx  s   b    //
      combination_pdf[4] = {-1,   3,   2};   // u   s    -> wx  d   s    //
      combination_pdf[5] = {-1,   1,   4};   // c   d    -> wx  d   s    //
      combination_pdf[6] = {-1,   5,   2};   // u   b    -> wx  d   b    //
      combination_pdf[7] = {-1,   5,   4};   // c   b    -> wx  s   b    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  d   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  s   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  d   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  s   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  d   ux   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> wx  d   ux   //
      combination_pdf[3] = { 1,   5,  -5};   // b   bx   -> wx  s   cx   //
      combination_pdf[4] = {-1,   1,  -1};   // dx  d    -> wx  s   cx   //
      combination_pdf[5] = {-1,   3,  -3};   // sx  s    -> wx  d   ux   //
      combination_pdf[6] = {-1,   5,  -5};   // bx  b    -> wx  d   ux   //
      combination_pdf[7] = {-1,   5,  -5};   // bx  b    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> wx  d   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> wx  s   ux   //
      combination_pdf[2] = { 1,   5,  -1};   // b   dx   -> wx  b   ux   //
      combination_pdf[3] = { 1,   5,  -3};   // b   sx   -> wx  b   cx   //
      combination_pdf[4] = {-1,   3,  -1};   // dx  s    -> wx  s   ux   //
      combination_pdf[5] = {-1,   1,  -3};   // sx  d    -> wx  d   cx   //
      combination_pdf[6] = {-1,   5,  -1};   // dx  b    -> wx  b   ux   //
      combination_pdf[7] = {-1,   5,  -3};   // sx  b    -> wx  b   cx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> wx  d   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> wx  s   c    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  d   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> wx  u   s    //
      combination_pdf[2] = {-1,   4,   2};   // u   c    -> wx  u   s    //
      combination_pdf[3] = {-1,   2,   4};   // c   u    -> wx  d   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  g   g    //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  g   g    //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  g   g    //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  g   g    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  d   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  s   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  d   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  s   sx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  u   ux   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  c   cx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  u   ux   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  c   cx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  s   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  d   dx   //
      combination_pdf[2] = { 1,   2,  -1};   // u   dx   -> wx  b   bx   //
      combination_pdf[3] = { 1,   4,  -3};   // c   sx   -> wx  b   bx   //
      combination_pdf[4] = {-1,   2,  -1};   // dx  u    -> wx  s   sx   //
      combination_pdf[5] = {-1,   4,  -3};   // sx  c    -> wx  d   dx   //
      combination_pdf[6] = {-1,   2,  -1};   // dx  u    -> wx  b   bx   //
      combination_pdf[7] = {-1,   4,  -3};   // sx  c    -> wx  b   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  c   cx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  u   ux   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  c   cx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  u   ux   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  d   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  s   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  d   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  s   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  s   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  d   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  s   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  d   ux   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  d   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> wx  s   dx   //
      combination_pdf[2] = { 1,   2,  -5};   // u   bx   -> wx  d   bx   //
      combination_pdf[3] = { 1,   4,  -5};   // c   bx   -> wx  s   bx   //
      combination_pdf[4] = {-1,   4,  -1};   // dx  c    -> wx  s   dx   //
      combination_pdf[5] = {-1,   2,  -3};   // sx  u    -> wx  d   sx   //
      combination_pdf[6] = {-1,   2,  -5};   // bx  u    -> wx  d   bx   //
      combination_pdf[7] = {-1,   4,  -5};   // bx  c    -> wx  s   bx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  u   cx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> wx  c   ux   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> wx  c   ux   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> wx  u   cx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  d   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> wx  s   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> wx  s   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> wx  d   cx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> wx  dx  ux   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> wx  sx  cx   //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> wx  ux  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> wx  cx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> wx  ux  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> wx  cx  cx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  dx  cx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> wx  ux  sx   //
      combination_pdf[2] = { 1,  -5,  -1};   // bx  dx   -> wx  ux  bx   //
      combination_pdf[3] = { 1,  -5,  -3};   // bx  sx   -> wx  cx  bx   //
      combination_pdf[4] = {-1,  -3,  -1};   // dx  sx   -> wx  ux  sx   //
      combination_pdf[5] = {-1,  -1,  -3};   // sx  dx   -> wx  dx  cx   //
      combination_pdf[6] = {-1,  -5,  -1};   // dx  bx   -> wx  ux  bx   //
      combination_pdf[7] = {-1,  -5,  -3};   // sx  bx   -> wx  cx  bx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> wx  ux  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> wx  ux  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> wx  ux  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> wx  ux  cx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
