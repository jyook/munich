#include "header.hpp"

#include "ppwx01nockm.phasespace.set.hpp"

ppwx01nockm_phasespace_set::~ppwx01nockm_phasespace_set(){
  static Logger logger("ppwx01nockm_phasespace_set::~ppwx01nockm_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::optimize_minv_born(){
  static Logger logger("ppwx01nockm_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01nockm_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01nockm_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_010_udx_wp(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppwx01nockm_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_010_udx_wp(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppwx01nockm_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_010_udx_wp(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::optimize_minv_real(){
  static Logger logger("ppwx01nockm_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01nockm_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01nockm_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_110_gu_wpd(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_110_gdx_wpux(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_110_udx_wpg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppwx01nockm_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_110_gu_wpd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_110_gdx_wpux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_110_udx_wpg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppwx01nockm_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_110_gu_wpd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_110_gdx_wpux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_110_udx_wpg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppwx01nockm_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppwx01nockm_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppwx01nockm_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppwx01nockm_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_210_gg_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_210_gu_wpgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_210_gdx_wpgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_210_du_wpdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_210_dc_wpds(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_210_ddx_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_210_ddx_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_210_dsx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_210_uu_wpdu(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_210_uc_wpdc(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_210_udx_wpgg(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_210_udx_wpddx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_210_udx_wpuux(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_210_udx_wpssx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_210_udx_wpccx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_210_uux_wpdux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_210_uux_wpscx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_210_usx_wpdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_210_usx_wpucx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_210_ucx_wpdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_210_dxdx_wpdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_210_dxux_wpuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_210_dxsx_wpdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_210_dxcx_wpuxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppwx01nockm_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_210_gg_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_210_gu_wpgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_210_gdx_wpgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_210_du_wpdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_210_dc_wpds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_210_ddx_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_210_ddx_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_210_dsx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_210_uu_wpdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_210_uc_wpdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_210_udx_wpgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_210_udx_wpddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_210_udx_wpuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_210_udx_wpssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_210_udx_wpccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_210_uux_wpdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_210_uux_wpscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_210_usx_wpdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_210_usx_wpucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_210_ucx_wpdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_210_dxdx_wpdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_210_dxux_wpuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_210_dxsx_wpdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_210_dxcx_wpuxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppwx01nockm_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_210_gg_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_210_gu_wpgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_210_gdx_wpgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_210_du_wpdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_210_dc_wpds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_210_ddx_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_210_ddx_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_210_dsx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_210_uu_wpdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_210_uc_wpdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_210_udx_wpgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_210_udx_wpddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_210_udx_wpuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_210_udx_wpssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_210_udx_wpccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_210_uux_wpdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_210_uux_wpscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_210_usx_wpdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_210_usx_wpucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_210_ucx_wpdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_210_dxdx_wpdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_210_dxux_wpuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_210_dxsx_wpdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_210_dxcx_wpuxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
