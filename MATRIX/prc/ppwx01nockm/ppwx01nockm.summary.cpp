#include "header.hpp"

#include "ppwx01nockm.summary.hpp"

ppwx01nockm_summary_generic::ppwx01nockm_summary_generic(munich * xmunich){
  Logger logger("ppwx01nockm_summary_generic::ppwx01nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppwx01nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppwx01nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppwx01nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppwx01nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppwx01nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppwx01nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_wp";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_wp";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_wp";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_wp";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_wp";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gd~_wpu~";
    subprocess[3] = "ud~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gd~_wpu~";
    subprocess[3] = "ud~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_wpd";
    subprocess[2] = "gd~_wpu~";
    subprocess[3] = "ud~_wpg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwx01nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppwx01nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_wpdu~";
    subprocess[2] = "gu_wpgd";
    subprocess[3] = "gd~_wpgu~";
    subprocess[4] = "du_wpdd";
    subprocess[5] = "dc_wpds";
    subprocess[6] = "dd~_wpdu~";
    subprocess[7] = "dd~_wpsc~";
    subprocess[8] = "ds~_wpdc~";
    subprocess[9] = "uu_wpdu";
    subprocess[10] = "uc_wpdc";
    subprocess[11] = "ud~_wpgg";
    subprocess[12] = "ud~_wpdd~";
    subprocess[13] = "ud~_wpuu~";
    subprocess[14] = "ud~_wpss~";
    subprocess[15] = "ud~_wpcc~";
    subprocess[16] = "uu~_wpdu~";
    subprocess[17] = "uu~_wpsc~";
    subprocess[18] = "us~_wpds~";
    subprocess[19] = "us~_wpuc~";
    subprocess[20] = "uc~_wpdc~";
    subprocess[21] = "d~d~_wpd~u~";
    subprocess[22] = "d~u~_wpu~u~";
    subprocess[23] = "d~s~_wpd~c~";
    subprocess[24] = "d~c~_wpu~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
