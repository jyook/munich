#include "header.hpp"

#include "ppv01.amplitude.set.hpp"

#include "ppwx01nockm.contribution.set.hpp"
#include "ppwx01nockm.event.set.hpp"
#include "ppwx01nockm.phasespace.set.hpp"
#include "ppwx01nockm.observable.set.hpp"
#include "ppwx01nockm.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-wp+X");

  MUC->csi = new ppwx01nockm_contribution_set();
  MUC->esi = new ppwx01nockm_event_set();
  MUC->psi = new ppwx01nockm_phasespace_set();
  MUC->osi = new ppwx01nockm_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppv01_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppwx01nockm_phasespace_set();
      MUC->psi->fake_psi->csi = new ppwx01nockm_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppwx01nockm_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
