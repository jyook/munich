#include "header.hpp"

#include "ppnenexa03.summary.hpp"

ppnenexa03_summary_generic::ppnenexa03_summary_generic(munich * xmunich){
  Logger logger("ppnenexa03_summary_generic::ppnenexa03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppnenexa03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppnenexa03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppnenexa03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppnenexa03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppnenexa03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppnenexa03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_born(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~a";
    subprocess[2] = "uu~_veve~a";
    subprocess[3] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~a";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~ad";
    subprocess[2] = "gu_veve~au";
    subprocess[3] = "gb_veve~ab";
    subprocess[4] = "gd~_veve~ad~";
    subprocess[5] = "gu~_veve~au~";
    subprocess[6] = "gb~_veve~ab~";
    subprocess[7] = "dd~_veve~ag";
    subprocess[8] = "uu~_veve~ag";
    subprocess[9] = "bb~_veve~ag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_veve~ag";
    subprocess[2] = "gd_veve~ad";
    subprocess[3] = "gu_veve~au";
    subprocess[4] = "gb_veve~ab";
    subprocess[5] = "gd~_veve~ad~";
    subprocess[6] = "gu~_veve~au~";
    subprocess[7] = "gb~_veve~ab~";
    subprocess[8] = "dd~_veve~ag";
    subprocess[9] = "uu~_veve~ag";
    subprocess[10] = "bb~_veve~ag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~ad";
    subprocess[2] = "ua_veve~au";
    subprocess[3] = "ba_veve~ab";
    subprocess[4] = "d~a_veve~ad~";
    subprocess[5] = "u~a_veve~au~";
    subprocess[6] = "b~a_veve~ab~";
    subprocess[7] = "dd~_veve~aa";
    subprocess[8] = "uu~_veve~aa";
    subprocess[9] = "bb~_veve~aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~ad";
    subprocess[2] = "gu_veve~au";
    subprocess[3] = "gb_veve~ab";
    subprocess[4] = "gd~_veve~ad~";
    subprocess[5] = "gu~_veve~au~";
    subprocess[6] = "gb~_veve~ab~";
    subprocess[7] = "dd~_veve~ag";
    subprocess[8] = "uu~_veve~ag";
    subprocess[9] = "bb~_veve~ag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~ad";
    subprocess[2] = "ua_veve~au";
    subprocess[3] = "ba_veve~ab";
    subprocess[4] = "d~a_veve~ad~";
    subprocess[5] = "u~a_veve~au~";
    subprocess[6] = "b~a_veve~ab~";
    subprocess[7] = "dd~_veve~aa";
    subprocess[8] = "uu~_veve~aa";
    subprocess[9] = "bb~_veve~aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~ad";
    subprocess[2] = "gu_veve~au";
    subprocess[3] = "gb_veve~ab";
    subprocess[4] = "gd~_veve~ad~";
    subprocess[5] = "gu~_veve~au~";
    subprocess[6] = "gb~_veve~ab~";
    subprocess[7] = "dd~_veve~ag";
    subprocess[8] = "uu~_veve~ag";
    subprocess[9] = "bb~_veve~ag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~ad";
    subprocess[2] = "ua_veve~au";
    subprocess[3] = "ba_veve~ab";
    subprocess[4] = "d~a_veve~ad~";
    subprocess[5] = "u~a_veve~au~";
    subprocess[6] = "b~a_veve~ab~";
    subprocess[7] = "dd~_veve~aa";
    subprocess[8] = "uu~_veve~aa";
    subprocess[9] = "bb~_veve~aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppnenexa03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_veve~add~";
    subprocess[2] = "gg_veve~auu~";
    subprocess[3] = "gg_veve~abb~";
    subprocess[4] = "gd_veve~agd";
    subprocess[5] = "gu_veve~agu";
    subprocess[6] = "gb_veve~agb";
    subprocess[7] = "gd~_veve~agd~";
    subprocess[8] = "gu~_veve~agu~";
    subprocess[9] = "gb~_veve~agb~";
    subprocess[10] = "dd_veve~add";
    subprocess[11] = "du_veve~adu";
    subprocess[12] = "ds_veve~ads";
    subprocess[13] = "dc_veve~adc";
    subprocess[14] = "db_veve~adb";
    subprocess[15] = "dd~_veve~agg";
    subprocess[16] = "dd~_veve~add~";
    subprocess[17] = "dd~_veve~auu~";
    subprocess[18] = "dd~_veve~ass~";
    subprocess[19] = "dd~_veve~acc~";
    subprocess[20] = "dd~_veve~abb~";
    subprocess[21] = "du~_veve~adu~";
    subprocess[22] = "ds~_veve~ads~";
    subprocess[23] = "dc~_veve~adc~";
    subprocess[24] = "db~_veve~adb~";
    subprocess[25] = "uu_veve~auu";
    subprocess[26] = "uc_veve~auc";
    subprocess[27] = "ub_veve~aub";
    subprocess[28] = "ud~_veve~aud~";
    subprocess[29] = "uu~_veve~agg";
    subprocess[30] = "uu~_veve~add~";
    subprocess[31] = "uu~_veve~auu~";
    subprocess[32] = "uu~_veve~ass~";
    subprocess[33] = "uu~_veve~acc~";
    subprocess[34] = "uu~_veve~abb~";
    subprocess[35] = "us~_veve~aus~";
    subprocess[36] = "uc~_veve~auc~";
    subprocess[37] = "ub~_veve~aub~";
    subprocess[38] = "bb_veve~abb";
    subprocess[39] = "bd~_veve~abd~";
    subprocess[40] = "bu~_veve~abu~";
    subprocess[41] = "bb~_veve~agg";
    subprocess[42] = "bb~_veve~add~";
    subprocess[43] = "bb~_veve~auu~";
    subprocess[44] = "bb~_veve~abb~";
    subprocess[45] = "d~d~_veve~ad~d~";
    subprocess[46] = "d~u~_veve~ad~u~";
    subprocess[47] = "d~s~_veve~ad~s~";
    subprocess[48] = "d~c~_veve~ad~c~";
    subprocess[49] = "d~b~_veve~ad~b~";
    subprocess[50] = "u~u~_veve~au~u~";
    subprocess[51] = "u~c~_veve~au~c~";
    subprocess[52] = "u~b~_veve~au~b~";
    subprocess[53] = "b~b~_veve~ab~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
