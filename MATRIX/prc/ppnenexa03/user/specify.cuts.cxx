logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_R_gamjet = USERSWITCH("R_gamjet");
  static double cut_min_R_gamjet = USERCUT("min_R_gamjet");
  static double cut_min_R2_gamjet = pow(cut_min_R_gamjet, 2);
  
  for (int i_j = 0; i_j < NUMBER("jet"); i_j++){
    // photon--jet isolation cuts
    if (switch_cut_R_gamjet == 1){
      if (R2_eta(PARTICLE("photon")[0], PARTICLE("jet")[i_j]) < cut_min_R2_gamjet){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_R_gamjet cut applied" << endl; return;}
    }
  }

  if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}

}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;


