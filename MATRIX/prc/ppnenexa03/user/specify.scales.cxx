{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum + PARTICLE("photon")[0].momentum).m();
    // takes sum of momenta of hardest neutrino PARTICLE("nua")[0].momentum, second-hardest neutrino PARTICLE("nua")[1].momentum
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum + PARTICLE("photon")[0].momentum).m();
    double pT = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum + PARTICLE("photon")[0].momentum).pT();
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }

  else if (sd == 3){
    // transverse mass (mT=ET=pT) of photon
    temp_mu_central = PARTICLE("photon")[0].momentum.pT();
  }
  else if (sd == 4){
    // transverse mass of neutrinos (from Z decay)
    temp_mu_central = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).ET();
  }
  else if (sd == 5){
    // geometric avarage of transverse masses of photon and neutrinos (from Z decay)
    temp_mu_central = sqrt(PARTICLE("photon")[0].pT * (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).ET());
  }
  else if (sd == 6){
    // quadratic sum of Z mass and transverse mass of the photon
    temp_mu_central = sqrt(msi->M2_Z + PARTICLE("photon")[0].momentum.pT2());
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
