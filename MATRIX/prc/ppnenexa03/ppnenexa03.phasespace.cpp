#include "header.hpp"

#include "ppnenexa03.phasespace.set.hpp"

ppnenexa03_phasespace_set::~ppnenexa03_phasespace_set(){
  static Logger logger("ppnenexa03_phasespace_set::~ppnenexa03_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::optimize_minv_born(){
  static Logger logger("ppnenexa03_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexa03_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 6;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexa03_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_030_ddx_vevexa(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_030_uux_vevexa(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_030_bbx_vevexa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_230_gg_vevexa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppnenexa03_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_030_ddx_vevexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_030_uux_vevexa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_030_bbx_vevexa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_230_gg_vevexa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppnenexa03_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_030_ddx_vevexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_030_uux_vevexa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_030_bbx_vevexa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_230_gg_vevexa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::optimize_minv_real(){
  static Logger logger("ppnenexa03_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexa03_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 48;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 10;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexa03_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 13){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 15){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 17){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_130_gd_vevexad(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_130_gu_vevexau(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_130_gb_vevexab(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_130_gdx_vevexadx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_130_gux_vevexaux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_130_gbx_vevexabx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_130_ddx_vevexag(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_130_uux_vevexag(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_130_bbx_vevexag(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_040_da_vevexad(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_040_ua_vevexau(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_040_ba_vevexab(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_040_dxa_vevexadx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_040_uxa_vevexaux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_040_bxa_vevexabx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_040_ddx_vevexaa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_040_uux_vevexaa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_040_bbx_vevexaa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_330_gg_vevexag(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_330_gd_vevexad(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_330_gu_vevexau(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_330_gb_vevexab(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_330_gdx_vevexadx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_330_gux_vevexaux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_330_gbx_vevexabx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_330_ddx_vevexag(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_330_uux_vevexag(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_330_bbx_vevexag(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppnenexa03_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_130_gd_vevexad(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_130_gu_vevexau(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_130_gb_vevexab(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_130_gdx_vevexadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_130_gux_vevexaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_130_gbx_vevexabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_130_ddx_vevexag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_130_uux_vevexag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_130_bbx_vevexag(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_040_da_vevexad(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_040_ua_vevexau(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_040_ba_vevexab(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_040_dxa_vevexadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_040_uxa_vevexaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_040_bxa_vevexabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_040_ddx_vevexaa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_040_uux_vevexaa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_040_bbx_vevexaa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_330_gg_vevexag(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_330_gd_vevexad(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_330_gu_vevexau(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_330_gb_vevexab(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_330_gdx_vevexadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_330_gux_vevexaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_330_gbx_vevexabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_330_ddx_vevexag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_330_uux_vevexag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_330_bbx_vevexag(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppnenexa03_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_130_gd_vevexad(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_130_gu_vevexau(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_130_gb_vevexab(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_130_gdx_vevexadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_130_gux_vevexaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_130_gbx_vevexabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_130_ddx_vevexag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_130_uux_vevexag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_130_bbx_vevexag(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_040_da_vevexad(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_040_ua_vevexau(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_040_ba_vevexab(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_040_dxa_vevexadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_040_uxa_vevexaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_040_bxa_vevexabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_040_ddx_vevexaa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_040_uux_vevexaa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_040_bbx_vevexaa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_330_gg_vevexag(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_330_gd_vevexad(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_330_gu_vevexau(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_330_gb_vevexab(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_330_gdx_vevexadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_330_gux_vevexaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_330_gbx_vevexabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_330_ddx_vevexag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_330_uux_vevexag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_330_bbx_vevexag(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppnenexa03_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenexa03_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 86;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 40;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenexa03_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppnenexa03_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_230_gg_vevexaddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_230_gg_vevexauux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_230_gg_vevexabbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_230_gd_vevexagd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_230_gu_vevexagu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_230_gb_vevexagb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_230_gdx_vevexagdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_230_gux_vevexagux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_230_gbx_vevexagbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_230_dd_vevexadd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_230_du_vevexadu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_230_ds_vevexads(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_230_dc_vevexadc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_230_db_vevexadb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_230_ddx_vevexagg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_230_ddx_vevexaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_230_ddx_vevexauux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_230_ddx_vevexassx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_230_ddx_vevexaccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_230_ddx_vevexabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_230_dux_vevexadux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_230_dsx_vevexadsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_230_dcx_vevexadcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_230_dbx_vevexadbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_230_uu_vevexauu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_230_uc_vevexauc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_230_ub_vevexaub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_230_udx_vevexaudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_230_uux_vevexagg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_230_uux_vevexaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_230_uux_vevexauux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_230_uux_vevexassx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_230_uux_vevexaccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_230_uux_vevexabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_230_usx_vevexausx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_230_ucx_vevexaucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_230_ubx_vevexaubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_230_bb_vevexabb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_230_bdx_vevexabdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_230_bux_vevexabux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_230_bbx_vevexagg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_230_bbx_vevexaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_230_bbx_vevexauux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_230_bbx_vevexabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_230_dxdx_vevexadxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_230_dxux_vevexadxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_230_dxsx_vevexadxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_230_dxcx_vevexadxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_230_dxbx_vevexadxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_230_uxux_vevexauxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_230_uxcx_vevexauxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_230_uxbx_vevexauxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_230_bxbx_vevexabxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppnenexa03_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_230_gg_vevexaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_230_gg_vevexauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_230_gg_vevexabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_230_gd_vevexagd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_230_gu_vevexagu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_230_gb_vevexagb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_230_gdx_vevexagdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_230_gux_vevexagux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_230_gbx_vevexagbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_230_dd_vevexadd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_230_du_vevexadu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_230_ds_vevexads(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_230_dc_vevexadc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_230_db_vevexadb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_230_ddx_vevexagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_230_ddx_vevexaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_230_ddx_vevexauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_230_ddx_vevexassx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_230_ddx_vevexaccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_230_ddx_vevexabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_230_dux_vevexadux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_230_dsx_vevexadsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_230_dcx_vevexadcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_230_dbx_vevexadbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_230_uu_vevexauu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_230_uc_vevexauc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_230_ub_vevexaub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_230_udx_vevexaudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_230_uux_vevexagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_230_uux_vevexaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_230_uux_vevexauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_230_uux_vevexassx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_230_uux_vevexaccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_230_uux_vevexabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_230_usx_vevexausx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_230_ucx_vevexaucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_230_ubx_vevexaubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_230_bb_vevexabb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_230_bdx_vevexabdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_230_bux_vevexabux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_230_bbx_vevexagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_230_bbx_vevexaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_230_bbx_vevexauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_230_bbx_vevexabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_230_dxdx_vevexadxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_230_dxux_vevexadxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_230_dxsx_vevexadxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_230_dxcx_vevexadxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_230_dxbx_vevexadxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_230_uxux_vevexauxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_230_uxcx_vevexauxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_230_uxbx_vevexauxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_230_bxbx_vevexabxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenexa03_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppnenexa03_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_230_gg_vevexaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_230_gg_vevexauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_230_gg_vevexabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_230_gd_vevexagd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_230_gu_vevexagu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_230_gb_vevexagb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_230_gdx_vevexagdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_230_gux_vevexagux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_230_gbx_vevexagbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_230_dd_vevexadd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_230_du_vevexadu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_230_ds_vevexads(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_230_dc_vevexadc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_230_db_vevexadb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_230_ddx_vevexagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_230_ddx_vevexaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_230_ddx_vevexauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_230_ddx_vevexassx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_230_ddx_vevexaccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_230_ddx_vevexabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_230_dux_vevexadux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_230_dsx_vevexadsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_230_dcx_vevexadcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_230_dbx_vevexadbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_230_uu_vevexauu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_230_uc_vevexauc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_230_ub_vevexaub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_230_udx_vevexaudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_230_uux_vevexagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_230_uux_vevexaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_230_uux_vevexauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_230_uux_vevexassx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_230_uux_vevexaccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_230_uux_vevexabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_230_usx_vevexausx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_230_ucx_vevexaucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_230_ubx_vevexaubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_230_bb_vevexabb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_230_bdx_vevexabdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_230_bux_vevexabux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_230_bbx_vevexagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_230_bbx_vevexaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_230_bbx_vevexauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_230_bbx_vevexabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_230_dxdx_vevexadxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_230_dxux_vevexadxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_230_dxsx_vevexadxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_230_dxcx_vevexadxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_230_dxbx_vevexadxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_230_uxux_vevexauxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_230_uxcx_vevexauxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_230_uxbx_vevexauxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_230_bxbx_vevexabxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
