#include "header.hpp"

#include "pplla03.amplitude.set.hpp"

#include "ppnenexa03.contribution.set.hpp"
#include "ppnenexa03.event.set.hpp"
#include "ppnenexa03.phasespace.set.hpp"
#include "ppnenexa03.observable.set.hpp"
#include "ppnenexa03.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-veve~a+X");

  MUC->csi = new ppnenexa03_contribution_set();
  MUC->esi = new ppnenexa03_event_set();
  MUC->psi = new ppnenexa03_phasespace_set();
  MUC->osi = new ppnenexa03_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    pplla03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppnenexa03_phasespace_set();
      MUC->psi->fake_psi->csi = new ppnenexa03_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppnenexa03_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
