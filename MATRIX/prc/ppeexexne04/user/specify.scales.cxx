{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("nua")[0].momentum).m();
    // takes sum of momenta of hardest lepton PARTICLE("lep")[0].momentum, second-hardest lepton PARTICLE("lep")[1].momentum, etc. 
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("nua")[0].momentum).m();
    double pT = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("nua")[0].momentum).pT();
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else if (sd == 3){
    // average of transverse masses of the reconstructed Z and W bosons with the invariant masses replaced by their pole mass
    temp_mu_central = .5 * (sqrt(msi->M2_Z + PARTICLE("Zrec")[0].pT2) + sqrt(msi->M2_W + PARTICLE("Wrec")[0].pT2));
  }
  else if (sd == 4){
    // average of transverse masses of the reconstructed Z and W bosons
    temp_mu_central = .5 * (PARTICLE("Zrec")[0].ET + PARTICLE("Wrec")[0].ET);
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
