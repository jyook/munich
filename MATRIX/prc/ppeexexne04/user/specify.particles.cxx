logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  USERPARTICLE("<particle_name>").push_back(<particle>);

  static int switch_lepton_identification = USERSWITCH("lepton_identification");

  //-----------------------------------------------------------------//
  // apply additional cuts on lepton identified to come from W-boson //
  //-----------------------------------------------------------------//

  // in the equal-flavor case we first have to identify the leptons coming from the Z and the W boson

  vector<particle> lp;
  particle nu;
  particle lm;
  if (csi->process_class == "pp-emepepve+X"){
    lp = PARTICLE("ep"); // list of positrons
    nu = PARTICLE("nea")[0]; // neutrino
    lm = PARTICLE("em")[0]; // list of electrons
  }
  else if (csi->process_class == "pp-mummupmupvm+X"){
    lp = PARTICLE("mup"); // list of anti-muons
    nu = PARTICLE("nma")[0]; // neutrino
    lm = PARTICLE("mum")[0]; // list of muons
  }
  else {
    logger << LOG_FATAL << "Particles are not defined for " << csi->process_class << "." << endl;
    exit(1);
  }
  
  if (switch_lepton_identification == 1){ // ATLAS prescription using "resonant shape" algorithm
    double m2ll_1 = (lp[0].momentum + lm.momentum).m2();
    double m2lpnu_1 = (lp[1].momentum + nu.momentum).m2();

    double P_estimator_1 = 1. / ((pow(m2ll_1 - msi->M2_Z, 2) + msi->M2_Z * pow(msi->Gamma_Z, 2)) * (pow(m2lpnu_1 - msi->M2_W, 2) + msi->M2_W * pow(msi->Gamma_W, 2)));
    
    double m2ll_2 = (lp[1].momentum + lm.momentum).m2();
    double m2lpnu_2 = (lp[0].momentum + nu.momentum).m2();
    double P_estimator_2 = 1. / ((pow(m2ll_2 - msi->M2_Z, 2) + msi->M2_Z * pow(msi->Gamma_Z, 2)) * (pow(m2lpnu_2 - msi->M2_W, 2) + msi->M2_W * pow(msi->Gamma_W, 2)));

    if (P_estimator_1 > P_estimator_2){
      USERPARTICLE("Zrec").push_back(lp[0] + lm);
      USERPARTICLE("Wrec").push_back(lp[1] + nu);
      USERPARTICLE("lepW").push_back(lp[1]);
      USERPARTICLE("lpZ").push_back(lp[0]);
      if(lp[0].momentum.pT() > lm.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lp[0]);
	  USERPARTICLE("lepZ").push_back(lm);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lm);
	  USERPARTICLE("lepZ").push_back(lp[0]);
	}
    }
    else {
      USERPARTICLE("Zrec").push_back(lp[1] + lm);
      USERPARTICLE("Wrec").push_back(lp[0] + nu);
      USERPARTICLE("lepW").push_back(lp[0]);
      USERPARTICLE("lpZ").push_back(lp[1]);
      if(lp[1].momentum.pT() > lm.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lp[1]);
	  USERPARTICLE("lepZ").push_back(lm);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lm);
	  USERPARTICLE("lepZ").push_back(lp[1]);
	}
    }
  }



  else if (switch_lepton_identification == 2){ // CMS prescription reconstructing leptons from Z by invariant mass of two leptons closest to Z mass
    double mll_1 = (lp[0].momentum + lm.momentum).m();
    double P_estimator_1 = abs(mll_1 - msi->M_Z);
    double mll_2 = (lp[1].momentum + lm.momentum).m();
    double P_estimator_2 = abs(mll_2 - msi->M_Z);

    if (P_estimator_1 < P_estimator_2){
      USERPARTICLE("Zrec").push_back(lp[0] + lm);
      USERPARTICLE("Wrec").push_back(lp[1] + nu);
      USERPARTICLE("lepW").push_back(lp[1]);
      USERPARTICLE("lpZ").push_back(lp[0]);
      if(lp[0].momentum.pT() > lm.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lp[0]);
	  USERPARTICLE("lepZ").push_back(lm);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lm);
	  USERPARTICLE("lepZ").push_back(lp[0]);
	}
    }
    else {
      USERPARTICLE("Zrec").push_back(lp[1] + lm);
      USERPARTICLE("Wrec").push_back(lp[0] + nu);
      USERPARTICLE("lepW").push_back(lp[0]);
      USERPARTICLE("lpZ").push_back(lp[1]);
      if(lp[1].momentum.pT() > lm.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lp[1]);
	  USERPARTICLE("lepZ").push_back(lm);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lm);
	  USERPARTICLE("lepZ").push_back(lp[1]);
	}
    }

    //    assert(false && "ERROR: lepton_identification = 2 not implemented yet");
  }
  /*
  else {
    assert(false && "ERROR: Trying to compute observable M_Z, where leptons from Z (and W) must be identified, but switch lepton_identification not set to 1 or 2.");
  }
  */
}

logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
