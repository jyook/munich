{
  //---------------------------------------------\
  // generic lepton cuts (no Z/W identification) |
  // --------------------------------------------/

  // hardest and second-hardest lepton pT cuts (no Z/W identification)
  static int switch_lepton_cuts = USERSWITCH("lepton_cuts");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");
  static double cut_min_pT_lep_2nd = USERCUT("min_pT_lep_2nd");

  static int switch_cut_delta_M_lepleplep = USERSWITCH("delta_M_lepleplep_MZ");
  static double cut_min_delta_M_lepleplep = USERCUT("min_delta_M_lepleplep_MZ");

  // dR cut between any two leptons (no Z/W identification)
  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);

  // invariant-mass cut on all lepton anti-lepton combinations (no Z/W identification)
  static int switch_cut_M_lep_alep = USERSWITCH("M_leplep_OSSF");
  static double cut_min_M_lep_alep = USERCUT("min_M_leplep_OSSF");
  static double cut_min_M2_lep_alep = pow(cut_min_M_lep_alep, 2);


  //--------------------------------\
  // cuts after Z/W identification) |
  // -------------------------------/

  // invariant-mass cut on leptons from Z decay - minimum/maximum
  static int switch_cut_M_Z = USERSWITCH("M_Zrec");
  static double cut_min_M_Z = USERCUT("min_M_Zrec");
  static double cut_min_M2_emep = pow(cut_min_M_Z, 2);
  static double cut_max_M_Z = USERCUT("max_M_Zrec");
  static double cut_max_M2_emep = pow(cut_max_M_Z, 2);

  // invariant-mass cut on leptons from Z decay - symmetric around Z mass
  static int switch_cut_delta_M_Z = USERSWITCH("delta_M_Zrec_MZ");
  static double cut_max_delta_M_Z = USERCUT("max_delta_M_Zrec_MZ");

  // reconstructed W-boson transverse-mass cut
  static int switch_cut_MT_W = USERSWITCH("MT_Wrec");
  static double cut_min_MT_W = USERCUT("min_MT_Wrec");

  // dR cut between the two leptons from Z decay
  static int switch_cut_R_lepZlepZ = USERSWITCH("R_lepZlepZ");
  static double cut_min_R_lepZlepZ = USERCUT("min_R_lepZlepZ");
  static double cut_min_R2_lepZlepZ = pow(cut_min_R_lepZlepZ, 2);

  // dR cut between one lepton from Z decay and one lepton from W decay 
  static int switch_cut_R_lepZlepW = USERSWITCH("R_lepZlepW");
  static double cut_min_R_lepZlepW = USERCUT("min_R_lepZlepW");
  static double cut_min_R2_lepZlepW = pow(cut_min_R_lepZlepW, 2);

  // pT cut on leptons from Z decay
  static int switch_lepZ_cuts = USERSWITCH("lepZ_cuts");
  static double cut_min_pT_lepZ_1st = USERCUT("min_pT_lepZ_1st");
  static double cut_min_pT_lepZ_2nd = USERCUT("min_pT_lepZ_2nd");

  // pT cut on lepton from W decay
  static int switch_lepW_cuts = USERSWITCH("lepW_cuts");
  static double cut_min_pT_lepW = USERCUT("min_pT_lepW");
  static double cut_max_eta_lepW = USERCUT("max_eta_lepW");



  // minumum cut on the distance of the three lepton invariant mass to the the Z mass
  if (switch_cut_delta_M_lepleplep == 1){
    double M2_lepleplep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum).m2();
    if (abs(sqrt(M2_lepleplep)-msi->M[23]) < cut_min_delta_M_lepleplep) {
      cut_ps[i_a] = -1;
      return;
    }
  }

  // cuts on hardest and second-hardest lepton
  if (switch_lepton_cuts){
    double pT_lep_1st = PARTICLE("lep")[0].pT;
    double pT_lep_2nd = PARTICLE("lep")[1].pT;

    if (pT_lep_1st < cut_min_pT_lep_1st || pT_lep_2nd < cut_min_pT_lep_2nd){
      cut_ps[i_a] = -1; 
      return;
    }
  }

  // invariant-mass cut on leptons from Z decay - symmetric around Z mass
  if (switch_cut_delta_M_Z == 1){
    double M2_emep = PARTICLE("Zrec")[0].m2;
    if (abs(sqrt(M2_emep) - msi->M_Z) > cut_max_delta_M_Z) {
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_max_delta_M_Z" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "cut_max_delta_M_Z cut applied" << endl; 
      return;
    }
  }

  // IR safety cut: invariant mass of any em-ep combination must be larger than cut_min_M_lep_alep
  if (switch_cut_M_lep_alep){
    if (NUMBER("e") > 1){
      for (int i_lp = 0; i_lp < PARTICLE("ep").size(); i_lp++){
	for (int i_lm = 0; i_lm < PARTICLE("em").size(); i_lm++){
	  double M2_leplep = (PARTICLE("ep")[i_lp].momentum + PARTICLE("em")[i_lm].momentum).m2();
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(ep)[" << i_lp << "] = " << PARTICLE("ep")[i_lp].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(em)[" << i_lm << "] = " << PARTICLE("em")[i_lm].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_lep_alep << endl;}

	  if (M2_leplep < cut_min_M2_lep_alep){
	    cut_ps[i_a] = -1; 
	    if (switch_output_cutinfo){
	      info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_leplep_min_IR" << endl; 
	      logger << LOG_DEBUG << endl << info_cut.str(); 
	    }
	    logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
	    return;
	  }
	}
      }
    }
    if (NUMBER("mu") > 1){
      for (int i_lp = 0; i_lp < PARTICLE("mup").size(); i_lp++){
	for (int i_lm = 0; i_lm < PARTICLE("mum").size(); i_lm++){
	  double M2_leplep = (PARTICLE("mup")[i_lp].momentum + PARTICLE("mum")[i_lm].momentum).m2();
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(mup)[" << i_lp << "] = " << PARTICLE("mup")[i_lp].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(mum)[" << i_lm << "] = " << PARTICLE("mum")[i_lm].momentum << endl;}
	  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_lep_alep << endl;}

	  if (M2_leplep < cut_min_M2_lep_alep){
	    cut_ps[i_a] = -1; 
	    if (switch_output_cutinfo){
	      info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_leplep_min_IR" << endl; 
	      logger << LOG_DEBUG << endl << info_cut.str(); 
	    }
	    logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
	    return;
	  }
	}
      }
    }
  }

  // reconstructed W-boson transverse-mass cut
  if (switch_cut_MT_W == 1){
    double temp_dphi = abs(PARTICLE("lepW")[0].phi - PARTICLE("nua")[0].phi);
    if (temp_dphi > pi){temp_dphi = f2pi - temp_dphi;}
    double MT_W = sqrt(2 * PARTICLE("lepW")[0].pT * PARTICLE("nua")[0].pT * (1. - cos(temp_dphi)));
    if (MT_W < cut_min_MT_W) {
      cut_ps[i_a] = -1;
      return;
    }
  }
  
  // dR cut between the two leptons from Z decay
  if (switch_cut_R_lepZlepZ == 1){
    double R2_eta_leplep = R2_eta(PARTICLE("lepZ")[0], PARTICLE("lepZ")[1]);
    if (R2_eta_leplep < cut_min_R2_lepZlepZ) {
      cut_ps[i_a] = -1;
      return;
    }
  }

  // dR cut between one lepton from Z decay and one lepton from W decay 
  if (switch_cut_R_lepZlepW == 1){
    for (int i_l = 0; i_l < PARTICLE("lepZ").size(); i_l++){
      double R2_eta_leplep = R2_eta(PARTICLE("lepZ")[i_l], PARTICLE("lepW")[0]);
      if (R2_eta_leplep < cut_min_R2_lepZlepW) {
	cut_ps[i_a] = -1;
	return;
      }
    }
  }

  // invariant-mass cut on leptons from Z decay - minimum/maximum
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_Z = " << switch_cut_M_Z << endl;}
  if (switch_cut_M_Z == 1){
    //    double M2_emep = (PARTICLE("epZ")[0].momentum + PARTICLE("em")[0].momentum).m2();
    double M2_emep = PARTICLE("Zrec")[0].m2;
    

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lepZ)[0] = " << PARTICLE("lepZ")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lepZ)[1] = " << PARTICLE("lepZ")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_emep = " << M2_emep << " > " << cut_min_M2_emep << endl;}

    if (M2_emep < cut_min_M2_emep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_Z" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Z min cut applied" << endl; 
      return;
    }

    if (switch_output_cutinfo){if (cut_max_M2_emep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_emep = " << M2_emep << " < " << cut_max_M2_emep << endl;}}

    if (cut_max_M2_emep != 0 && M2_emep > cut_max_M2_emep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_M_Z" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Z max cut applied" << endl; 
      return;
    }
  }

  // dR cut between any two leptons
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_leplep = " << switch_cut_R_leplep << endl; }
  if (switch_cut_R_leplep == 1){
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      for (int j_l = i_l + 1; j_l < PARTICLE("lep").size(); j_l++){
	double R2_eta_leplep = R2_eta(PARTICLE("lep")[i_l], PARTICLE("lep")[j_l]);
	
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << j_l << "] = " << PARTICLE("lep")[j_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_leplep = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}
	
	if (R2_eta_leplep < cut_min_R2_leplep){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_R_leplep of lepton " << i_l << " and " << j_l << "." << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_R_leplep cut applied" << endl; 
	  return;
	}
      }
    }
  }

  // cuts on hardest and second-hardest electrons and on hardest muon (in particular needed in "SF" case)
  if (switch_lepZ_cuts){
    if (PARTICLE("lepZ")[0].pT < cut_min_pT_lepZ_1st || PARTICLE("lepZ")[1].pT < cut_min_pT_lepZ_2nd){
      cut_ps[i_a] = -1; 
      return;
    }
  }
  if (switch_lepW_cuts){
    if (PARTICLE("lepW")[0].pT < cut_min_pT_lepW || abs(PARTICLE("lepW")[0].eta) > cut_max_eta_lepW){
      cut_ps[i_a] = -1; 
      return;
    } 
  }

  if (switch_output_cutinfo){info_cut << "ppeexexne04_cuts passed" << endl;}
}
