if (csi->type_correction == "QCD"){
  static int switch_cut_M_lep_alep = user->switch_value[user->switch_map["M_leplep_OSSF"]];
  static double cut_min_M_lep_alep = user->cut_value[user->cut_map["min_M_leplep_OSSF"]];

  static int switch_cut_M_Zrec = user->switch_value[user->switch_map["M_Zrec"]];
  static double cut_min_M_Zrec = user->cut_value[user->cut_map["min_M_Zrec"]];

  static int switch_cut_delta_M_Zrec_MZ = user->switch_value[user->switch_map["delta_M_Zrec_MZ"]];
  static double cut_max_delta_M_Zrec_MZ = user->cut_value[user->cut_map["max_delta_M_Zrec_MZ"]];

  static int switch_cut_delta_M_lepleplep_MZ = user->switch_value[user->switch_map["delta_M_lepleplep_MZ"]];
  static double cut_max_delta_M_lepleplep_MZ = user->cut_value[user->cut_map["max_delta_M_lepleplep_MZ"]];

  for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
    if (switch_cut_M_lep_alep){
      if (sqrtsmin_opt[i_a][12] < cut_min_M_lep_alep){
	sqrtsmin_opt[i_a][12] = cut_min_M_lep_alep;
      }
    }
    
    if (switch_cut_M_lep_alep){
      if (sqrtsmin_opt[i_a][20] < cut_min_M_lep_alep){
	sqrtsmin_opt[i_a][20] = cut_min_M_lep_alep;
      }
    }
    
    if (switch_cut_M_Zrec){
      if (sqrtsmin_opt[i_a][28] < cut_min_M_Zrec){
	sqrtsmin_opt[i_a][28] = cut_min_M_Zrec;
      }
    }
    
    if (switch_cut_delta_M_Zrec_MZ){
      if (sqrtsmin_opt[i_a][28] < M[23] - cut_max_delta_M_Zrec_MZ){
	sqrtsmin_opt[i_a][28] = M[23] - cut_max_delta_M_Zrec_MZ;
      }
    }
    
    if (switch_cut_delta_M_lepleplep_MZ){
      if (sqrtsmin_opt[i_a][28] < M[23] - cut_max_delta_M_lepleplep_MZ){
	sqrtsmin_opt[i_a][28] = M[23] - cut_max_delta_M_lepleplep_MZ;
      }
    }
  }
}
