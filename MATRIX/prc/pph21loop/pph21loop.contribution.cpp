#include "header.hpp"

#include "pph21loop.contribution.set.hpp"

pph21loop_contribution_set::~pph21loop_contribution_set(){
  static Logger logger("pph21loop_contribution_set::~pph21loop_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("pph21loop_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("pph21loop_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> h    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("pph21loop_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  25) && (tp[out[4]] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  25) && (tp[out[4]] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  25) && (tp[out[4]] ==   5)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==  -1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==  -3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 7; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("pph21loop_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> h   g    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> h   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> h   s    //
      combination_pdf[2] = { 1,   0,   5};   // g   b    -> h   b    //
      combination_pdf[3] = {-1,   0,   1};   // d   g    -> h   d    //
      combination_pdf[4] = {-1,   0,   3};   // s   g    -> h   s    //
      combination_pdf[5] = {-1,   0,   5};   // b   g    -> h   b    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> h   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> h   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> h   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> h   c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> h   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> h   sx   //
      combination_pdf[2] = { 1,   0,  -5};   // g   bx   -> h   bx   //
      combination_pdf[3] = {-1,   0,  -1};   // dx  g    -> h   dx   //
      combination_pdf[4] = {-1,   0,  -3};   // sx  g    -> h   sx   //
      combination_pdf[5] = {-1,   0,  -5};   // bx  g    -> h   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> h   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> h   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> h   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> h   cx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   g    //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> h   g    //
      combination_pdf[3] = {-1,   1,  -1};   // dx  d    -> h   g    //
      combination_pdf[4] = {-1,   3,  -3};   // sx  s    -> h   g    //
      combination_pdf[5] = {-1,   5,  -5};   // bx  b    -> h   g    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> h   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> h   g    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("pph21loop_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 4 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   1) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   3) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -1) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -3) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  25) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  25) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 41; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 4 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("pph21loop_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 4 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> h   g   g    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(3);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> h   d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> h   s   sx   //
      combination_pdf[2] = { 1,   0,   0};   // g   g    -> h   b   bx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> h   u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> h   c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> h   g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> h   g   s    //
      combination_pdf[2] = { 1,   0,   5};   // g   b    -> h   g   b    //
      combination_pdf[3] = {-1,   0,   1};   // d   g    -> h   g   d    //
      combination_pdf[4] = {-1,   0,   3};   // s   g    -> h   g   s    //
      combination_pdf[5] = {-1,   0,   5};   // b   g    -> h   g   b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> h   g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> h   g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> h   g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> h   g   c    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> h   g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> h   g   sx   //
      combination_pdf[2] = { 1,   0,  -5};   // g   bx   -> h   g   bx   //
      combination_pdf[3] = {-1,   0,  -1};   // dx  g    -> h   g   dx   //
      combination_pdf[4] = {-1,   0,  -3};   // sx  g    -> h   g   sx   //
      combination_pdf[5] = {-1,   0,  -5};   // bx  g    -> h   g   bx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> h   g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> h   g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> h   g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> h   g   cx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(3);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> h   d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> h   s   s    //
      combination_pdf[2] = { 1,   5,   5};   // b   b    -> h   b   b    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> h   d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> h   s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> h   d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> h   s   c    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> h   d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> h   d   s    //
      combination_pdf[2] = { 1,   1,   5};   // d   b    -> h   d   b    //
      combination_pdf[3] = { 1,   3,   5};   // s   b    -> h   s   b    //
      combination_pdf[4] = { 1,   5,   1};   // b   d    -> h   d   b    //
      combination_pdf[5] = { 1,   5,   3};   // b   s    -> h   s   b    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> h   d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> h   u   s    //
      combination_pdf[2] = { 1,   5,   2};   // b   u    -> h   u   b    //
      combination_pdf[3] = { 1,   5,   4};   // b   c    -> h   c   b    //
      combination_pdf[4] = {-1,   3,   2};   // u   s    -> h   u   s    //
      combination_pdf[5] = {-1,   1,   4};   // c   d    -> h   d   c    //
      combination_pdf[6] = {-1,   5,   2};   // u   b    -> h   u   b    //
      combination_pdf[7] = {-1,   5,   4};   // c   b    -> h   c   b    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   g   g    //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> h   g   g    //
      combination_pdf[3] = {-1,   1,  -1};   // dx  d    -> h   g   g    //
      combination_pdf[4] = {-1,   3,  -3};   // sx  s    -> h   g   g    //
      combination_pdf[5] = {-1,   5,  -5};   // bx  b    -> h   g   g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   s   sx   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> h   b   bx   //
      combination_pdf[3] = {-1,   1,  -1};   // dx  d    -> h   d   dx   //
      combination_pdf[4] = {-1,   3,  -3};   // sx  s    -> h   s   sx   //
      combination_pdf[5] = {-1,   5,  -5};   // bx  b    -> h   b   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> h   u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> h   c   cx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(12);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   d   dx   //
      combination_pdf[2] = { 1,   1,  -1};   // d   dx   -> h   b   bx   //
      combination_pdf[3] = { 1,   3,  -3};   // s   sx   -> h   b   bx   //
      combination_pdf[4] = { 1,   5,  -5};   // b   bx   -> h   d   dx   //
      combination_pdf[5] = { 1,   5,  -5};   // b   bx   -> h   s   sx   //
      combination_pdf[6] = {-1,   1,  -1};   // dx  d    -> h   s   sx   //
      combination_pdf[7] = {-1,   3,  -3};   // sx  s    -> h   d   dx   //
      combination_pdf[8] = {-1,   1,  -1};   // dx  d    -> h   b   bx   //
      combination_pdf[9] = {-1,   3,  -3};   // sx  s    -> h   b   bx   //
      combination_pdf[10] = {-1,   5,  -5};   // bx  b    -> h   d   dx   //
      combination_pdf[11] = {-1,   5,  -5};   // bx  b    -> h   s   sx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> h   c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> h   u   ux   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> h   u   ux   //
      combination_pdf[3] = { 1,   5,  -5};   // b   bx   -> h   c   cx   //
      combination_pdf[4] = {-1,   1,  -1};   // dx  d    -> h   c   cx   //
      combination_pdf[5] = {-1,   3,  -3};   // sx  s    -> h   u   ux   //
      combination_pdf[6] = {-1,   5,  -5};   // bx  b    -> h   u   ux   //
      combination_pdf[7] = {-1,   5,  -5};   // bx  b    -> h   c   cx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> h   d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> h   s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> h   d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> h   s   cx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(12);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> h   d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> h   s   dx   //
      combination_pdf[2] = { 1,   1,  -5};   // d   bx   -> h   d   bx   //
      combination_pdf[3] = { 1,   3,  -5};   // s   bx   -> h   s   bx   //
      combination_pdf[4] = { 1,   5,  -1};   // b   dx   -> h   b   dx   //
      combination_pdf[5] = { 1,   5,  -3};   // b   sx   -> h   b   sx   //
      combination_pdf[6] = {-1,   3,  -1};   // dx  s    -> h   s   dx   //
      combination_pdf[7] = {-1,   1,  -3};   // sx  d    -> h   d   sx   //
      combination_pdf[8] = {-1,   5,  -1};   // dx  b    -> h   b   dx   //
      combination_pdf[9] = {-1,   5,  -3};   // sx  b    -> h   b   sx   //
      combination_pdf[10] = {-1,   1,  -5};   // bx  d    -> h   d   bx   //
      combination_pdf[11] = {-1,   3,  -5};   // bx  s    -> h   s   bx   //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> h   d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> h   s   ux   //
      combination_pdf[2] = { 1,   5,  -2};   // b   ux   -> h   b   ux   //
      combination_pdf[3] = { 1,   5,  -4};   // b   cx   -> h   b   cx   //
      combination_pdf[4] = {-1,   3,  -2};   // ux  s    -> h   s   ux   //
      combination_pdf[5] = {-1,   1,  -4};   // cx  d    -> h   d   cx   //
      combination_pdf[6] = {-1,   5,  -2};   // ux  b    -> h   b   ux   //
      combination_pdf[7] = {-1,   5,  -4};   // cx  b    -> h   b   cx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> h   u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> h   c   c    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> h   u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> h   u   c    //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> h   u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> h   c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> h   u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> h   c   sx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> h   g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> h   g   g    //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> h   d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> h   s   sx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> h   u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> h   c   cx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   d   dx   //
      combination_pdf[2] = { 1,   2,  -2};   // u   ux   -> h   b   bx   //
      combination_pdf[3] = { 1,   4,  -4};   // c   cx   -> h   b   bx   //
      combination_pdf[4] = {-1,   2,  -2};   // ux  u    -> h   s   sx   //
      combination_pdf[5] = {-1,   4,  -4};   // cx  c    -> h   d   dx   //
      combination_pdf[6] = {-1,   2,  -2};   // ux  u    -> h   b   bx   //
      combination_pdf[7] = {-1,   4,  -4};   // cx  c    -> h   b   bx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> h   c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> h   u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> h   c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> h   u   ux   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> h   u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> h   c   dx   //
      combination_pdf[2] = { 1,   2,  -5};   // u   bx   -> h   u   bx   //
      combination_pdf[3] = { 1,   4,  -5};   // c   bx   -> h   c   bx   //
      combination_pdf[4] = {-1,   4,  -1};   // dx  c    -> h   c   dx   //
      combination_pdf[5] = {-1,   2,  -3};   // sx  u    -> h   u   sx   //
      combination_pdf[6] = {-1,   2,  -5};   // bx  u    -> h   u   bx   //
      combination_pdf[7] = {-1,   4,  -5};   // bx  c    -> h   c   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> h   u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> h   c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> h   c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> h   u   cx   //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(3);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> h   dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> h   sx  sx   //
      combination_pdf[2] = { 1,  -5,  -5};   // bx  bx   -> h   bx  bx   //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> h   dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> h   sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> h   dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> h   sx  cx   //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(6);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> h   dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> h   dx  sx   //
      combination_pdf[2] = { 1,  -1,  -5};   // dx  bx   -> h   dx  bx   //
      combination_pdf[3] = { 1,  -3,  -5};   // sx  bx   -> h   sx  bx   //
      combination_pdf[4] = { 1,  -5,  -1};   // bx  dx   -> h   dx  bx   //
      combination_pdf[5] = { 1,  -5,  -3};   // bx  sx   -> h   sx  bx   //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> h   dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> h   ux  sx   //
      combination_pdf[2] = { 1,  -5,  -2};   // bx  ux   -> h   ux  bx   //
      combination_pdf[3] = { 1,  -5,  -4};   // bx  cx   -> h   cx  bx   //
      combination_pdf[4] = {-1,  -3,  -2};   // ux  sx   -> h   ux  sx   //
      combination_pdf[5] = {-1,  -1,  -4};   // cx  dx   -> h   dx  cx   //
      combination_pdf[6] = {-1,  -5,  -2};   // ux  bx   -> h   ux  bx   //
      combination_pdf[7] = {-1,  -5,  -4};   // cx  bx   -> h   cx  bx   //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> h   ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> h   cx  cx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> h   ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> h   ux  cx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
