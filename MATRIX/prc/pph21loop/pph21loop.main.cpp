#include "header.hpp"

#include "pph21loop.amplitude.set.hpp"

#include "pph21loop.contribution.set.hpp"
#include "pph21loop.event.set.hpp"
#include "pph21loop.phasespace.set.hpp"
#include "pph21loop.observable.set.hpp"
#include "pph21loop.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-h+X");

  MUC->csi = new pph21loop_contribution_set();
  MUC->esi = new pph21loop_event_set();
  MUC->psi = new pph21loop_phasespace_set();
  MUC->osi = new pph21loop_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    pph21loop_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new pph21loop_phasespace_set();
      MUC->psi->fake_psi->csi = new pph21loop_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new pph21loop_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
