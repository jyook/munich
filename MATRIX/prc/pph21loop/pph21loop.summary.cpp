#include "header.hpp"

#include "pph21loop.summary.hpp"

pph21loop_summary_generic::pph21loop_summary_generic(munich * xmunich){
  Logger logger("pph21loop_summary_generic::pph21loop_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("pph21loop_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new pph21loop_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("pph21loop_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new pph21loop_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("pph21loop_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new pph21loop_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_born(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(8);
    subprocess[1] = "gg_hg";
    subprocess[2] = "gd_hd";
    subprocess[3] = "gu_hu";
    subprocess[4] = "gd~_hd~";
    subprocess[5] = "gu~_hu~";
    subprocess[6] = "dd~_hg";
    subprocess[7] = "uu~_hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(8);
    subprocess[1] = "gg_hg";
    subprocess[2] = "gd_hd";
    subprocess[3] = "gu_hu";
    subprocess[4] = "gd~_hd~";
    subprocess[5] = "gu~_hu~";
    subprocess[6] = "dd~_hg";
    subprocess[7] = "uu~_hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(8);
    subprocess[1] = "gg_hg";
    subprocess[2] = "gd_hd";
    subprocess[3] = "gu_hu";
    subprocess[4] = "gd~_hd~";
    subprocess[5] = "gu~_hu~";
    subprocess[6] = "dd~_hg";
    subprocess[7] = "uu~_hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("pph21loop_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(36);
    subprocess[1] = "gg_hgg";
    subprocess[2] = "gg_hdd~";
    subprocess[3] = "gg_huu~";
    subprocess[4] = "gd_hgd";
    subprocess[5] = "gu_hgu";
    subprocess[6] = "gd~_hgd~";
    subprocess[7] = "gu~_hgu~";
    subprocess[8] = "dd_hdd";
    subprocess[9] = "du_hdu";
    subprocess[10] = "ds_hds";
    subprocess[11] = "dc_hdc";
    subprocess[12] = "dd~_hgg";
    subprocess[13] = "dd~_hdd~";
    subprocess[14] = "dd~_huu~";
    subprocess[15] = "dd~_hss~";
    subprocess[16] = "dd~_hcc~";
    subprocess[17] = "du~_hdu~";
    subprocess[18] = "ds~_hds~";
    subprocess[19] = "dc~_hdc~";
    subprocess[20] = "uu_huu";
    subprocess[21] = "uc_huc";
    subprocess[22] = "ud~_hud~";
    subprocess[23] = "uu~_hgg";
    subprocess[24] = "uu~_hdd~";
    subprocess[25] = "uu~_huu~";
    subprocess[26] = "uu~_hss~";
    subprocess[27] = "uu~_hcc~";
    subprocess[28] = "us~_hus~";
    subprocess[29] = "uc~_huc~";
    subprocess[30] = "d~d~_hd~d~";
    subprocess[31] = "d~u~_hd~u~";
    subprocess[32] = "d~s~_hd~s~";
    subprocess[33] = "d~c~_hd~c~";
    subprocess[34] = "u~u~_hu~u~";
    subprocess[35] = "u~c~_hu~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
