#include "header.hpp"

#include "pph21loop.phasespace.set.hpp"

pph21loop_phasespace_set::~pph21loop_phasespace_set(){
  static Logger logger("pph21loop_phasespace_set::~pph21loop_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::optimize_minv_born(){
  static Logger logger("pph21loop_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int pph21loop_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("pph21loop_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("pph21loop_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("pph21loop_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_210_gg_h(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("pph21loop_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_210_gg_h(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("pph21loop_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_210_gg_h(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::optimize_minv_real(){
  static Logger logger("pph21loop_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int pph21loop_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("pph21loop_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 9;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 7){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("pph21loop_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -36};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 7){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("pph21loop_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_310_gg_hg(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_310_gd_hd(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_310_gu_hu(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_310_gdx_hdx(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_310_gux_hux(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_310_ddx_hg(x_a);}
    else if (csi->no_process_parton[x_a] == 7){ax_psp_310_uux_hg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("pph21loop_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_310_gg_hg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_310_gd_hd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_310_gu_hu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_310_gdx_hdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_310_gux_hux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_310_ddx_hg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 7){ac_psp_310_uux_hg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("pph21loop_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_310_gg_hg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_310_gd_hd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_310_gu_hu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_310_gdx_hdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_310_gux_hux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_310_ddx_hg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 7){ag_psp_310_uux_hg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("pph21loop_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int pph21loop_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("pph21loop_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 4 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 48;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 81;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 13;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 13;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 13;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 38){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("pph21loop_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 4 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0, -36};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0, -36};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0, -36};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("pph21loop_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 4 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_410_gg_hgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_410_gg_hddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_410_gg_huux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_410_gd_hgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_410_gu_hgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_410_gdx_hgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_410_gux_hgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_410_dd_hdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_410_du_hdu(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_410_ds_hds(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_410_dc_hdc(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_410_ddx_hgg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_410_ddx_hddx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_410_ddx_huux(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_410_ddx_hssx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_410_ddx_hccx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_410_dux_hdux(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_410_dsx_hdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_410_dcx_hdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_410_uu_huu(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_410_uc_huc(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_410_udx_hudx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_410_uux_hgg(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_410_uux_hddx(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_410_uux_huux(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_410_uux_hssx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_410_uux_hccx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_410_usx_husx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_410_ucx_hucx(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_410_dxdx_hdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_410_dxux_hdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_410_dxsx_hdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 38){ax_psp_410_dxcx_hdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_410_uxux_huxux(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_410_uxcx_huxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("pph21loop_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 4 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_410_gg_hgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_410_gg_hddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_410_gg_huux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_410_gd_hgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_410_gu_hgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_410_gdx_hgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_410_gux_hgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_410_dd_hdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_410_du_hdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_410_ds_hds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_410_dc_hdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_410_ddx_hgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_410_ddx_hddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_410_ddx_huux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_410_ddx_hssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_410_ddx_hccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_410_dux_hdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_410_dsx_hdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_410_dcx_hdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_410_uu_huu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_410_uc_huc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_410_udx_hudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_410_uux_hgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_410_uux_hddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_410_uux_huux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_410_uux_hssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_410_uux_hccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_410_usx_husx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_410_ucx_hucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_410_dxdx_hdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_410_dxux_hdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_410_dxsx_hdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 38){ac_psp_410_dxcx_hdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_410_uxux_huxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_410_uxcx_huxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void pph21loop_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("pph21loop_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 4 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_410_gg_hgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_410_gg_hddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_410_gg_huux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_410_gd_hgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_410_gu_hgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_410_gdx_hgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_410_gux_hgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_410_dd_hdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_410_du_hdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_410_ds_hds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_410_dc_hdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_410_ddx_hgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_410_ddx_hddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_410_ddx_huux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_410_ddx_hssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_410_ddx_hccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_410_dux_hdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_410_dsx_hdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_410_dcx_hdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_410_uu_huu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_410_uc_huc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_410_udx_hudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_410_uux_hgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_410_uux_hddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_410_uux_huux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_410_uux_hssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_410_uux_hccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_410_usx_husx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_410_ucx_hucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_410_dxdx_hdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_410_dxux_hdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_410_dxsx_hdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 38){ag_psp_410_dxcx_hdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_410_uxux_huxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_410_uxcx_huxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
