#include "header.hpp"

#include "ppeexa03.summary.hpp"

ppeexa03_summary_generic::ppeexa03_summary_generic(munich * xmunich){
  Logger logger("ppeexa03_summary_generic::ppeexa03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeexa03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeexa03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeexa03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeexa03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeexa03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeexa03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_born(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subprocess[4] = "aa_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subprocess[4] = "aa_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subprocess[4] = "aa_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepa";
    subprocess[2] = "uu~_emepa";
    subprocess[3] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepad";
    subprocess[2] = "gu_emepau";
    subprocess[3] = "gb_emepab";
    subprocess[4] = "gd~_emepad~";
    subprocess[5] = "gu~_emepau~";
    subprocess[6] = "gb~_emepab~";
    subprocess[7] = "dd~_emepag";
    subprocess[8] = "uu~_emepag";
    subprocess[9] = "bb~_emepag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emepag";
    subprocess[2] = "gd_emepad";
    subprocess[3] = "gu_emepau";
    subprocess[4] = "gb_emepab";
    subprocess[5] = "gd~_emepad~";
    subprocess[6] = "gu~_emepau~";
    subprocess[7] = "gb~_emepab~";
    subprocess[8] = "dd~_emepag";
    subprocess[9] = "uu~_emepag";
    subprocess[10] = "bb~_emepag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emepad";
    subprocess[2] = "ua_emepau";
    subprocess[3] = "ba_emepab";
    subprocess[4] = "d~a_emepad~";
    subprocess[5] = "u~a_emepau~";
    subprocess[6] = "b~a_emepab~";
    subprocess[7] = "dd~_emepaa";
    subprocess[8] = "uu~_emepaa";
    subprocess[9] = "bb~_emepaa";
    subprocess[10] = "aa_emepaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepad";
    subprocess[2] = "gu_emepau";
    subprocess[3] = "gb_emepab";
    subprocess[4] = "gd~_emepad~";
    subprocess[5] = "gu~_emepau~";
    subprocess[6] = "gb~_emepab~";
    subprocess[7] = "dd~_emepag";
    subprocess[8] = "uu~_emepag";
    subprocess[9] = "bb~_emepag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepad";
    subprocess[2] = "ua_emepau";
    subprocess[3] = "ba_emepab";
    subprocess[4] = "d~a_emepad~";
    subprocess[5] = "u~a_emepau~";
    subprocess[6] = "b~a_emepab~";
    subprocess[7] = "dd~_emepaa";
    subprocess[8] = "uu~_emepaa";
    subprocess[9] = "bb~_emepaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepad";
    subprocess[2] = "gu_emepau";
    subprocess[3] = "gb_emepab";
    subprocess[4] = "gd~_emepad~";
    subprocess[5] = "gu~_emepau~";
    subprocess[6] = "gb~_emepab~";
    subprocess[7] = "dd~_emepag";
    subprocess[8] = "uu~_emepag";
    subprocess[9] = "bb~_emepag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepad";
    subprocess[2] = "ua_emepau";
    subprocess[3] = "ba_emepab";
    subprocess[4] = "d~a_emepad~";
    subprocess[5] = "u~a_emepau~";
    subprocess[6] = "b~a_emepab~";
    subprocess[7] = "dd~_emepaa";
    subprocess[8] = "uu~_emepaa";
    subprocess[9] = "bb~_emepaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexa03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeexa03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_emepadd~";
    subprocess[2] = "gg_emepauu~";
    subprocess[3] = "gg_emepabb~";
    subprocess[4] = "gd_emepagd";
    subprocess[5] = "gu_emepagu";
    subprocess[6] = "gb_emepagb";
    subprocess[7] = "gd~_emepagd~";
    subprocess[8] = "gu~_emepagu~";
    subprocess[9] = "gb~_emepagb~";
    subprocess[10] = "dd_emepadd";
    subprocess[11] = "du_emepadu";
    subprocess[12] = "ds_emepads";
    subprocess[13] = "dc_emepadc";
    subprocess[14] = "db_emepadb";
    subprocess[15] = "dd~_emepagg";
    subprocess[16] = "dd~_emepadd~";
    subprocess[17] = "dd~_emepauu~";
    subprocess[18] = "dd~_emepass~";
    subprocess[19] = "dd~_emepacc~";
    subprocess[20] = "dd~_emepabb~";
    subprocess[21] = "du~_emepadu~";
    subprocess[22] = "ds~_emepads~";
    subprocess[23] = "dc~_emepadc~";
    subprocess[24] = "db~_emepadb~";
    subprocess[25] = "uu_emepauu";
    subprocess[26] = "uc_emepauc";
    subprocess[27] = "ub_emepaub";
    subprocess[28] = "ud~_emepaud~";
    subprocess[29] = "uu~_emepagg";
    subprocess[30] = "uu~_emepadd~";
    subprocess[31] = "uu~_emepauu~";
    subprocess[32] = "uu~_emepass~";
    subprocess[33] = "uu~_emepacc~";
    subprocess[34] = "uu~_emepabb~";
    subprocess[35] = "us~_emepaus~";
    subprocess[36] = "uc~_emepauc~";
    subprocess[37] = "ub~_emepaub~";
    subprocess[38] = "bb_emepabb";
    subprocess[39] = "bd~_emepabd~";
    subprocess[40] = "bu~_emepabu~";
    subprocess[41] = "bb~_emepagg";
    subprocess[42] = "bb~_emepadd~";
    subprocess[43] = "bb~_emepauu~";
    subprocess[44] = "bb~_emepabb~";
    subprocess[45] = "d~d~_emepad~d~";
    subprocess[46] = "d~u~_emepad~u~";
    subprocess[47] = "d~s~_emepad~s~";
    subprocess[48] = "d~c~_emepad~c~";
    subprocess[49] = "d~b~_emepad~b~";
    subprocess[50] = "u~u~_emepau~u~";
    subprocess[51] = "u~c~_emepau~c~";
    subprocess[52] = "u~b~_emepau~b~";
    subprocess[53] = "b~b~_emepab~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
