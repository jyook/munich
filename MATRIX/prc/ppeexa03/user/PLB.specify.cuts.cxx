/*
if (osi_cut_ps[ia] != -1){
  //  cuts ...
  //  if (){osi_cut_ps[ia] = -1;}
  if ( osi_p_parton[ia][0].x0()<pset.cut_var_b() && pset.cut_var_b()>1.0) {
    osi_cut_ps[ia]=-1;
    //cout << dx_p[0][0].x0() << endl;
  }
 }
*/

// cuts for ATLAS comporison; FIXME: misuse of variables..
if (osi_cut_ps[ia] != -1){
  if ((osi_p_event[ia][0] + osi_p_event[ia][1]).m() < 40.0){osi_cut_ps[ia] = -1;}
  if (R(osi_p_event[ia][0],osi_p_event[ia][2]) < pset.cut_R_leplep()){osi_cut_ps[ia] = -1;}
  if (R(osi_p_event[ia][1],osi_p_event[ia][2]) < pset.cut_R_leplep()){osi_cut_ps[ia] = -1;}
//  if ((osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m() < 95.0){osi_cut_ps[ia] = -1;}
//  if ((osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m() > 130.0){osi_cut_ps[ia] = -1;}
//  if ((osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m() > 95.0 && (osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m() < 130.0){osi_cut_ps[ia] = -1;}
}

// Jet isolation
if (osi_p_event[ia].size()>=4) {
  if (R(osi_p_event[ia][0],osi_p_event[ia][3]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
  if (R(osi_p_event[ia][1],osi_p_event[ia][3]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
  if (R(osi_p_event[ia][2],osi_p_event[ia][3]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
  if (osi_p_event[ia].size()==5) {
    if (R(osi_p_event[ia][0],osi_p_event[ia][4]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
    if (R(osi_p_event[ia][1],osi_p_event[ia][4]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
    if (R(osi_p_event[ia][2],osi_p_event[ia][4]) < pset.cut_R_lepjet()){osi_cut_ps[ia] = -1;}
  }
}

//if (osi_cut_ps[ia] != -1){
//  if ((abs((osi_p_event[ia][0] + osi_p_event[ia][1]).m() - M_Z) / Gamma_Z > pset.cut_resonance_Z()) ||
//      //      (osi_p_event[ia][0].pT() <= pset.cut_pT_lep()) ||
//      //      (osi_p_event[ia][1].pT() <= pset.cut_pT_lep()) ||
//      //      (osi_p_event[ia][2].pT() <= pset.cut_pT_lep()) || // should be photon !!!
//      //      (abs(osi_p_event[ia][0].eta()) >= pset.cut_eta_lep()) ||
//      //      (abs(osi_p_event[ia][1].eta()) >= pset.cut_eta_lep()) ||
//      //      (abs(osi_p_event[ia][2].eta()) >= pset.cut_eta_lep()) || // should be photon !!!
//      //      (R(osi_p_event[ia][0],osi_p_event[ia][1]) < pset.cut_R_leplep()) ||
//      //      (R(osi_p_event[ia][0],osi_p_event[ia][2]) < pset.cut_R_leplep()) || // should be lep-phot (2x) !!!
//      //      (R(osi_p_event[ia][1],osi_p_event[ia][2]) < pset.cut_R_leplep())){osi_cut_ps[ia] = -1;}
//      ((osi_p_event[ia][0] + osi_p_event[ia][1]).m() < pset.cut_var_c()) ||
//      ((osi_p_event[ia][0] + osi_p_event[ia][2]).m() < pset.cut_var_c()) ||
//      ((osi_p_event[ia][1] + osi_p_event[ia][2]).m() < pset.cut_var_c()) ||
//      ((osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m() < pset.cut_Mjetjet())){osi_cut_ps[ia] = -1;} // pset.cut_var_b() -> pset.cut_Mjetjet()
// }
if (osi_cut_ps[ia] != -1){

  if (pset.subcontribution() == "NNLO.RRA" ||
      pset.subcontribution() == "NNLO.RCA" ||
      pset.subcontribution() == "NNLO.RVA" ||
      pset.subcontribution() == "NNLO.RoldCA" ||
      pset.subcontribution() == "NLO.R" ||
      pset.subcontribution() == "NNLO.R"){
    //  if (pset.subcontribution() == "NNLO.RRA"){
    // TODO: implement this properly
    /*
    double r0 = pset.min_qTcut()/250.0;
    double Qpartonic = (osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m();
    if ((osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).pT() / Qpartonic < r0){osi_cut_ps[ia] = -1;}
  */
    double QTpartonic = (osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).pT();
    if (osi_switch_qTcut == 1){
      double Qpartonic = (osi_p_event[ia][0] + osi_p_event[ia][1] + osi_p_event[ia][2]).m();
      if (QTpartonic / Qpartonic < osi_min_qTcut / 100.){osi_cut_ps[ia] = -1;}
      else {
	osi_cut_ps[ia] = GSL_MIN_INT(int(((QTpartonic / Qpartonic - osi_min_qTcut / 100.) * 100) / osi_step_qTcut), osi_n_qTcut);
      }
    }
    else if (osi_switch_qTcut == 2){
      if (QTpartonic < pset.min_qTcut()){osi_cut_ps[ia] = -1;}
      else {
	osi_cut_ps[ia] = GSL_MIN_INT(int((QTpartonic - osi_min_qTcut) / osi_step_qTcut), osi_n_qTcut);
      }
    }
    //    osi_cut_ps[ia] = int((QTpartonic - pset.min_qTcut()) / pset.step_qTcut());
  }

  /*
  else if (pset.subcontribution() == "NLO.G" ||
	   pset.subcontribution() == "NNLO.G" ||
	   pset.subcontribution() == "NNLO.G2"){
    // should be always the case due to lower bound in mapping of qT
    //    if (QTpartonic < pset.min_qTcut()){osi_cut_ps[ia] = -1;}
    //    else {
    osi_cut_ps[ia] = int((sqrt(qt2) - pset.min_qTcut()) / pset.step_qTcut());
    osi_cut_ps[ia] = int((sqrt(qt2) - pset.min_qTcut()) / pset.step_qTcu());
    if (osi_cut_ps[ia] > pset.n_qTcut()){osi_cut_ps[ia] = pset.n_qTcut();}
    // }
  }
  */
/*
  else {
    osi_cut_ps[ia] = pset.n_qTcut();
  }
*/
}
