logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_M_leplep = USERSWITCH("M_leplep");
  static double cut_min_M_leplep = USERCUT("min_M_leplep");
  static double cut_min_M2_leplep = pow(cut_min_M_leplep, 2);
  static double cut_max_M_leplep = USERCUT("max_M_leplep");
  static double cut_max_M2_leplep = pow(cut_max_M_leplep, 2);

  static int switch_cut_M_leplep_p_M_leplepgam = USERSWITCH("M_leplep+M_leplepgam");
  static double cut_min_M_leplep_p_M_leplepgam = USERCUT("min_M_leplep+M_leplepgam");
  static double cut_max_M_leplep_p_M_leplepgam = USERCUT("max_M_leplep+M_leplepgam");

  static int switch_cut_M_lepgam = USERSWITCH("M_lepgam");
  static double cut_min_M_lepgam = USERCUT("min_M_lepgam");
  static double cut_min_M2_lepgam = pow(cut_min_M_lepgam, 2);
  static double cut_max_M_lepgam = USERCUT("max_M_lepgam");
  static double cut_max_M2_lepgam = pow(cut_max_M_lepgam, 2);
  
  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);
  
  static int switch_cut_R_lepgam = USERSWITCH("R_lepgam");
  static double cut_min_R_lepgam = USERCUT("min_R_lepgam");
  static double cut_min_R2_lepgam = pow(cut_min_R_lepgam, 2);
  
  /*
  static int switch_cut_eta_lep = USERSWITCH("eta_lep");
  static double cut_min_eta_lep = USERCUT("min_eta_lep");
  */

  /*
  static int switch_cut_eta_gam = USERSWITCH("eta_gam");
  static double cut_min_eta_gam = USERCUT("min_eta_gam");
  */
  
  static int switch_cut_R_lepjet = USERSWITCH("R_lepjet");
  static double cut_min_R_lepjet = USERCUT("min_R_lepjet");
  static double cut_min_R2_lepjet = pow(cut_min_R_lepjet, 2);
  
  static int switch_cut_R_gamjet = USERSWITCH("R_gamjet");
  static double cut_min_R_gamjet = USERCUT("min_R_gamjet");
  static double cut_min_R2_gamjet = pow(cut_min_R_gamjet, 2);

  static int switch_cut_pT_lep_1st = USERSWITCH("pT_lep_1st");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");

  static int switch_cut_M_leplep_plus_M_leplepgam = USERSWITCH("M_leplep_plus_M_leplepgam");
  static double cut_min_M_leplep_plus_M_leplepgam = USERCUT("min_M_leplep_plus_M_leplepgam");
  static double cut_max_M_leplep_plus_M_leplepgam = USERCUT("max_M_leplep_plus_M_leplepgam");

  static int switch_cut_rel_ET_iso_with_lep = USERSWITCH("rel_ET_iso_with_lep");
  static double cut_max_rel_ET_iso_with_lep = USERCUT("max_rel_ET_iso_with_lep");

  static int switch_cut_rel_ET_iso_no_lep = USERSWITCH("rel_ET_iso_no_lep");
  static double cut_max_rel_ET_iso_no_lep = USERCUT("max_rel_ET_iso_no_lep");

  static int switch_cut_pT_leplepgamma = USERSWITCH("pT_leplepgamma");
  static double cut_min_pT_leplepgamma = USERCUT("min_pT_leplepgamma");
  static double cut_max_pT_leplepgamma = USERCUT("max_pT_leplepgamma");


  // cut on pT of lepton-lepton-photon system
  if (switch_cut_pT_leplepgamma == 1){
    double temp_pT_leplepgamma = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("photon")[0].momentum).pT();
    if (temp_pT_leplepgamma < cut_min_pT_leplepgamma){
      cut_ps[i_a] = -1;
      return;
    }
    if (temp_pT_leplepgamma > cut_max_pT_leplepgamma){
      cut_ps[i_a] = -1;
      return;
    }
  }

  // cut on hardest (highest-pT) lepton
  if (switch_cut_pT_lep_1st == 1){
    double temp_pT_lep_1st = PARTICLE("lep")[0].pT;
    if (temp_pT_lep_1st < cut_min_pT_lep_1st){
      cut_ps[i_a] = -1;
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_1st << " < cut_min_pT_lep_1st = " << cut_min_pT_lep_1st << endl;
      return;
    }
  }



  // lepton--lepton invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep = " << switch_cut_M_leplep << endl;}
  if (switch_cut_M_leplep == 1){
    double M2_leplep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum).m2();

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_leplep << endl;}

    if (M2_leplep < cut_min_M2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep min cut applied" << endl; 
      return;
    }

    if (switch_output_cutinfo){if (cut_max_M2_leplep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " < " << cut_max_M2_leplep << endl;}}

    if (cut_max_M2_leplep != 0 && M2_leplep > cut_max_M2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep max cut applied" << endl; 
      return;
    }
  }



  // lepton--photon invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_lepgam = " << switch_cut_M_lepgam << endl;}
  if (switch_cut_M_lepgam == 1){
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      double M2_lepgam = (PARTICLE("lep")[i_l].momentum + PARTICLE("photon")[0].momentum).m2();
      
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_lepgam = " << M2_lepgam << " > " << cut_min_M2_lepgam << endl;}
      
      if (M2_lepgam < cut_min_M2_lepgam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_lepgam" << endl;
	  logger << LOG_DEBUG << endl << info_cut.str();
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lepgam min cut applied" << endl; 
	return;
      }
      
      if (switch_output_cutinfo){if (cut_max_M2_lepgam != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_lepgam = " << M2_lepgam << " < " << cut_max_M2_lepgam << endl;}}
      
      if (cut_max_M2_lepgam != 0 && M2_lepgam > cut_max_M2_lepgam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_lepgam" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lepgam max cut applied" << endl; 
	return;
      }
    }
  }

  // lepton--lepton + lepton--lepton--photon invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep_p_M_leplepgam = " << switch_cut_M_leplep_p_M_leplepgam << endl;}
  if (switch_cut_M_leplep_p_M_leplepgam == 1){
    double M_leplep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum).m();
    double M_leplepgam = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("photon")[0].momentum).m();
    double M_leplep_p_M_leplepgam = M_leplep + M_leplepgam;

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] =    " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] =    " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M_leplep_p_M_leplepgam = " << M_leplep_p_M_leplepgam << " > " << cut_min_M_leplep_p_M_leplepgam << endl;}

    if (M_leplep_p_M_leplepgam < cut_min_M_leplep_p_M_leplepgam){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep_p_M_leplepgam" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep_p_M_leplepgam min cut applied" << endl; 
      return;
    }

    if (switch_output_cutinfo){if (cut_max_M_leplep_p_M_leplepgam != 0.){info_cut << "[" << setw(2) << i_a << "]   M_leplep_p_M_leplepgam = " << M_leplep_p_M_leplepgam << " < " << cut_max_M_leplep_p_M_leplepgam << endl;}}

    if (cut_max_M_leplep_p_M_leplepgam != 0 && M_leplep_p_M_leplepgam > cut_max_M_leplep_p_M_leplepgam){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep_p_M_leplepgam" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep_p_M_leplepgam max cut applied" << endl; 
      return;
    }
  }



  // lepton--lepton isolation cuts
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_leplep = " << switch_cut_R_leplep << endl; }
  if (switch_cut_R_leplep == 1){
    double R2_eta_leplep = R2_eta(PARTICLE("lep")[0], PARTICLE("lep")[1]);

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_leplep = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}

    if (R2_eta_leplep < cut_min_R2_leplep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_leplep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_R_leplep cut applied" << endl; 
      return;
    }
  }



  /*
  // lower lepton pseudo-rapidity cuts (higher cut implemented generically via lepton identification)
  if (switch_cut_eta_lep == 1){
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      if (PARTICLE("lep")[i_l].eta < cut_min_eta_lep){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_eta_lep cut applied" << endl; return;}
    }
  }
  */



  /*
  // lower photon pseudo-rapidity cuts (higher cut implented generically via photon identification)
  if (switch_cut_eta_gam == 1){
    for (int i_l = 0; i_l < PARTICLE("photon").size(); i_l++){
      if (PARTICLE("photon")[i_l].eta < cut_min_eta_gam){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_eta_gam cut applied" << endl; return;}
    }
  }
  */



  // lepton--photon isolation cuts
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_lepgam = " << switch_cut_R_lepgam << endl;}
  if (switch_cut_R_lepgam){
    int n_photon_max = 0;
    if (switch_cut_R_lepgam == 1){n_photon_max = PARTICLE("lep").size();}
    if (switch_cut_R_lepgam == 2){n_photon_max = NUMBER("lep");}
    // always: NUMBER("lep") < PARTICLE("lep").size()
    // switch_cut_R_lepgam = 1 is in general not IR-safe in case of EW corrections (soft photons close to lepton would be vetoed)
    
    for (int i_l = 0; i_l < n_photon_max; i_l++){
      double R2_eta_lepgam = R2_eta(PARTICLE("lep")[i_l], PARTICLE("photon")[0]);

      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_lepgam = " << R2_eta_lepgam << " > " << cut_min_R2_lepgam << endl;}

      if (R2_eta_lepgam < cut_min_R2_lepgam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_lepgam" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepgam cut applied" << endl; 
	return;
      }
    }
  }



  /*
  static int switch_cut_M_leplep_plus_M_leplepgam = USERSWITCH("M_leplep_plus_M_leplepgam");
  static double cut_min_M_leplep_plus_M_leplepgam = USERCUT("min_M_leplep_plus_M_leplepgam");
  static double cut_max_M_leplep_plus_M_leplepgam = USERCUT("max_M_leplep_plus_M_leplepgam");
  */
  // combined lepton--lepton plus lepton-lepton--photon invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep_plus_M_leplepgam = " << switch_cut_M_leplep_plus_M_leplepgam << endl;}
  if (switch_cut_M_leplep_plus_M_leplepgam){
    if (NUMBER("lep") > 1 && NUMBER("photon") > 0){
      double M_leplep_plus_M_leplepgam = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum).m() + (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("photon")[0].momentum).m();
      
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M_leplep_plus_M_leplepgam = " << M_leplep_plus_M_leplepgam << " > " << cut_min_M_leplep_plus_M_leplepgam << endl;}
      
	if (M_leplep_plus_M_leplepgam < cut_min_M_leplep_plus_M_leplepgam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_min_M_leplep_plus_M_leplepgam" << endl;
	  logger << LOG_DEBUG << endl << info_cut.str();
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep_plus_M_leplepgam min cut applied" << endl; 
	return;
      }
      
      if (switch_output_cutinfo){if (cut_max_M_leplep_plus_M_leplepgam != 0.){info_cut << "[" << setw(2) << i_a << "]   M_leplep_plus_M_leplepgam = " << M_leplep_plus_M_leplepgam << " < " << cut_max_M_leplep_plus_M_leplepgam << endl;}}
      
      if (cut_max_M_leplep_plus_M_leplepgam != 0. && M_leplep_plus_M_leplepgam > cut_max_M_leplep_plus_M_leplepgam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_max_M_leplep_plus_M_leplepgam" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep_plus_M_leplepgam max cut applied" << endl; 
	return;
      }
    }
  }




  

  for (int i_j = 0; i_j < NUMBER("jet"); i_j++){
    // lepton--jet isolation cuts
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_lepjet = " << switch_cut_R_lepjet << endl;}
    if (switch_cut_R_lepjet == 1){
      for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
	double R2_eta_lepjet = R2_eta(PARTICLE("lep")[i_l], PARTICLE("jet")[i_j]);

	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(jet)[" << i_j << "] = " << PARTICLE("jet")[i_j].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_lepjet = " << R2_eta_lepjet << " > " << cut_min_R2_lepjet << endl;}

	if (R2_eta_lepjet < cut_min_R2_lepjet){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_lepjet" << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepjet cut applied" << endl; 
	  return;
	}
      }
    }
    


    // photon--jet isolation cuts
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_gamjet = " << switch_cut_R_gamjet << endl;}
    if (switch_cut_R_gamjet == 1){
      double R2_eta_gamjet = R2_eta(PARTICLE("photon")[0], PARTICLE("jet")[i_j]);
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(jet)[" << i_j << "] = " << PARTICLE("jet")[i_j].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_gamjet = " << R2_eta_gamjet << " > " << cut_min_R2_gamjet << endl;}

      if (R2_eta_gamjet < cut_min_R2_gamjet){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_gamjet" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_R_gamjet cut applied" << endl; 
	return;
      }
    }
  }

  // extra photon isolation (jets and leptons)
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_rel_ET_iso_with_lep = " << switch_cut_rel_ET_iso_with_lep << endl;}
  if (switch_cut_rel_ET_iso_with_lep == 1){
    double ET_sum = 0.;
    double ET_photon = PARTICLE("photon")[0].momentum.pT(); 
    double cone2 = 0.2*0.2;

    logger << LOG_DEBUG_VERBOSE << "N_parton = " << particle_event[0][i_a].size() << endl;
    //    logger << LOG_DEBUG << "N_parton = " << PARTICLE("all").size() << endl;
    for (int i_p=3; i_p<particle_event[0][i_a].size(); i_p++) {
      double R2eta = R2_eta(particle_event[0][i_a][i_p], PARTICLE("photon")[0]);
      if (csi->type_parton[i_a][i_p] != 22) {
	logger << LOG_DEBUG_VERBOSE << "i_p = " << i_p << "; type_p = " << csi->type_parton[i_a][i_p] << "; R2eta = " << R2eta << "; ET = " << particle_event[0][i_a][i_p].momentum.ET() << endl;
	if (R2eta < cone2) {
	  ET_sum = ET_sum + particle_event[0][i_a][i_p].momentum.ET();
	  if (csi->type_parton[i_a][i_p] > 7 && csi->type_parton[i_a][i_p] < -7) {
	    logger << LOG_INFO << "ERROR: did not expect that there is a lepton remaining close to the photon!" << endl;
	    assert(false);
	  }
	}
      }
    }
    // for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
    //   double R2eta = R2_eta(PARTICLE("lep")[i_l], PARTICLE("photon")[0]);
    //   if (R2eta < cone2) {
    // 	ET_sum = ET_sum + PARTICLE("lep")[i_l].momentum.ET();
    // 	logger << LOG_INFO << "ERROR: did not expect that there is a lepton remaining close to the photon!" << endl;
    // 	assert(false);
    //   }
    // }

    double rel_ET_ratio = ET_sum/ET_photon;
    if (rel_ET_ratio > cut_max_rel_ET_iso_with_lep){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_max_rel_ET_iso_with_lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_rel_ET_iso_with_lep cut applied" << endl; 
      return;
    }
  }

  // extra photon isolation (only jets)
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_rel_ET_iso_no_lep = " << switch_cut_rel_ET_iso_no_lep << endl;}
  if (switch_cut_rel_ET_iso_no_lep == 1){
    double ET_sum = 0.;
    double ET_photon = PARTICLE("photon")[0].momentum.pT(); 
    double cone2 = 0.2*0.2;

    logger << LOG_DEBUG_VERBOSE << "N_parton = " << particle_event[0][i_a].size() << endl;
    //    logger << LOG_DEBUG << "N_parton = " << PARTICLE("all").size() << endl;
    for (int i_p=3; i_p<particle_event[0][i_a].size(); i_p++) {
      double R2eta = R2_eta(particle_event[0][i_a][i_p], PARTICLE("photon")[0]);
      if (csi->type_parton[i_a][i_p] < 7 && csi->type_parton[i_a][i_p] > -7) {
	logger << LOG_DEBUG_VERBOSE << "i_p = " << i_p << "; type_p = " << csi->type_parton[i_a][i_p] << "; R2eta = " << R2eta << "; ET = " << particle_event[0][i_a][i_p].momentum.ET() << endl;
	if (R2eta < cone2) {
	  ET_sum = ET_sum + particle_event[0][i_a][i_p].momentum.ET();
	}
      }
    }
    // for (int i_p=0; i_p<PARTICLE("all").size(); i_p++) {
    //   double R2eta = R2_eta(PARTICLE("all")[i_p], PARTICLE("photon")[0]);
    //   logger << LOG_DEBUG << "i_p = " << i_p << "; R2eta = " << R2eta << "; ET = " << PARTICLE("all")[i_p].momentum.ET() << endl;
    //   if (R2eta < cone2) {
    // 	ET_sum = ET_sum + PARTICLE("all")[i_p].momentum.ET();
    //   }
    // }

    double rel_ET_ratio = ET_sum/ET_photon;
    logger << LOG_DEBUG_VERBOSE << "ET_sum = " << ET_sum << endl;
    logger << LOG_DEBUG_VERBOSE << "ET_photon = " << ET_photon << endl;
    logger << LOG_DEBUG_VERBOSE << "rel_ET_ratio = " << rel_ET_ratio << endl;

    if (rel_ET_ratio > cut_max_rel_ET_iso_no_lep){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_max_rel_ET_iso_no_lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_rel_ET_iso_with_lep cut applied" << endl; 
      return;
    }
  }
    
  if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}

}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;
