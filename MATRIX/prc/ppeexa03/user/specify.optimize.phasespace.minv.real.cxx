if (csi->type_correction == "QCD"){
  static int switch_cut_M_leplep = user->switch_value[user->switch_map["M_leplep"]];
  static double cut_min_M_leplep = user->cut_value[user->cut_map["min_M_leplep"]];
  static int switch_cut_M_lepgam = user->switch_value[user->switch_map["M_lepgam"]];
  static double cut_min_M_lepgam = user->cut_value[user->cut_map["min_M_lepgam"]];
  
  //for (int i_a = 0; i_a < 1; i_a++){
  for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
    if (switch_cut_M_leplep){sqrtsmin_opt[i_a][12] = cut_min_M_leplep;}
    if (switch_cut_M_lepgam){sqrtsmin_opt[i_a][20] = cut_min_M_lepgam;}
    if (switch_cut_M_lepgam){sqrtsmin_opt[i_a][24] = cut_min_M_lepgam;}
    //  if (switch_cut_M_leplep){sqrtsmin_opt[i_a][28] = cut_min_M_leplep;}
  }
  
  for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
    logger << LOG_INFO << "sqrtsmin_opt after adding invariant-mass cuts:" << endl;
    for (int i = 4; i < sqrtsmin_opt[i_a].size(); i += 4){
      logger << LOG_INFO << "sqrtsmin_opt[" << i_a << "][" << setw(3) << i << "] = " << sqrtsmin_opt[i_a][i] << endl;
    }
  }
 }
