#include "header.hpp"

#include "pplla03.amplitude.set.hpp"

#include "ppeexa03.contribution.set.hpp"
#include "ppeexa03.event.set.hpp"
#include "ppeexa03.phasespace.set.hpp"
#include "ppeexa03.observable.set.hpp"
#include "ppeexa03.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emepa+X");

  MUC->csi = new ppeexa03_contribution_set();
  MUC->esi = new ppeexa03_event_set();
  MUC->psi = new ppeexa03_phasespace_set();
  MUC->osi = new ppeexa03_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    pplla03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppeexa03_phasespace_set();
      MUC->psi->fake_psi->csi = new ppeexa03_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppeexa03_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
