#include "header.hpp"

#include "ppnenex02.summary.hpp"

ppnenex02_summary_generic::ppnenex02_summary_generic(munich * xmunich){
  Logger logger("ppnenex02_summary_generic::ppnenex02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppnenex02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppnenex02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppnenex02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppnenex02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppnenex02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppnenex02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_born(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_veve~";
    subprocess[2] = "uu~_veve~";
    subprocess[3] = "bb~_veve~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_veve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~d";
    subprocess[2] = "gu_veve~u";
    subprocess[3] = "gb_veve~b";
    subprocess[4] = "gd~_veve~d~";
    subprocess[5] = "gu~_veve~u~";
    subprocess[6] = "gb~_veve~b~";
    subprocess[7] = "dd~_veve~g";
    subprocess[8] = "uu~_veve~g";
    subprocess[9] = "bb~_veve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_veve~g";
    subprocess[2] = "gd_veve~d";
    subprocess[3] = "gu_veve~u";
    subprocess[4] = "gb_veve~b";
    subprocess[5] = "gd~_veve~d~";
    subprocess[6] = "gu~_veve~u~";
    subprocess[7] = "gb~_veve~b~";
    subprocess[8] = "dd~_veve~g";
    subprocess[9] = "uu~_veve~g";
    subprocess[10] = "bb~_veve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~d";
    subprocess[2] = "ua_veve~u";
    subprocess[3] = "ba_veve~b";
    subprocess[4] = "d~a_veve~d~";
    subprocess[5] = "u~a_veve~u~";
    subprocess[6] = "b~a_veve~b~";
    subprocess[7] = "dd~_veve~a";
    subprocess[8] = "uu~_veve~a";
    subprocess[9] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~d";
    subprocess[2] = "gu_veve~u";
    subprocess[3] = "gb_veve~b";
    subprocess[4] = "gd~_veve~d~";
    subprocess[5] = "gu~_veve~u~";
    subprocess[6] = "gb~_veve~b~";
    subprocess[7] = "dd~_veve~g";
    subprocess[8] = "uu~_veve~g";
    subprocess[9] = "bb~_veve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~d";
    subprocess[2] = "ua_veve~u";
    subprocess[3] = "ba_veve~b";
    subprocess[4] = "d~a_veve~d~";
    subprocess[5] = "u~a_veve~u~";
    subprocess[6] = "b~a_veve~b~";
    subprocess[7] = "dd~_veve~a";
    subprocess[8] = "uu~_veve~a";
    subprocess[9] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_veve~d";
    subprocess[2] = "gu_veve~u";
    subprocess[3] = "gb_veve~b";
    subprocess[4] = "gd~_veve~d~";
    subprocess[5] = "gu~_veve~u~";
    subprocess[6] = "gb~_veve~b~";
    subprocess[7] = "dd~_veve~g";
    subprocess[8] = "uu~_veve~g";
    subprocess[9] = "bb~_veve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_veve~d";
    subprocess[2] = "ua_veve~u";
    subprocess[3] = "ba_veve~b";
    subprocess[4] = "d~a_veve~d~";
    subprocess[5] = "u~a_veve~u~";
    subprocess[6] = "b~a_veve~b~";
    subprocess[7] = "dd~_veve~a";
    subprocess[8] = "uu~_veve~a";
    subprocess[9] = "bb~_veve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenex02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppnenex02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_veve~dd~";
    subprocess[2] = "gg_veve~uu~";
    subprocess[3] = "gg_veve~bb~";
    subprocess[4] = "gd_veve~gd";
    subprocess[5] = "gu_veve~gu";
    subprocess[6] = "gb_veve~gb";
    subprocess[7] = "gd~_veve~gd~";
    subprocess[8] = "gu~_veve~gu~";
    subprocess[9] = "gb~_veve~gb~";
    subprocess[10] = "dd_veve~dd";
    subprocess[11] = "du_veve~du";
    subprocess[12] = "ds_veve~ds";
    subprocess[13] = "dc_veve~dc";
    subprocess[14] = "db_veve~db";
    subprocess[15] = "dd~_veve~gg";
    subprocess[16] = "dd~_veve~dd~";
    subprocess[17] = "dd~_veve~uu~";
    subprocess[18] = "dd~_veve~ss~";
    subprocess[19] = "dd~_veve~cc~";
    subprocess[20] = "dd~_veve~bb~";
    subprocess[21] = "du~_veve~du~";
    subprocess[22] = "ds~_veve~ds~";
    subprocess[23] = "dc~_veve~dc~";
    subprocess[24] = "db~_veve~db~";
    subprocess[25] = "uu_veve~uu";
    subprocess[26] = "uc_veve~uc";
    subprocess[27] = "ub_veve~ub";
    subprocess[28] = "ud~_veve~ud~";
    subprocess[29] = "uu~_veve~gg";
    subprocess[30] = "uu~_veve~dd~";
    subprocess[31] = "uu~_veve~uu~";
    subprocess[32] = "uu~_veve~ss~";
    subprocess[33] = "uu~_veve~cc~";
    subprocess[34] = "uu~_veve~bb~";
    subprocess[35] = "us~_veve~us~";
    subprocess[36] = "uc~_veve~uc~";
    subprocess[37] = "ub~_veve~ub~";
    subprocess[38] = "bb_veve~bb";
    subprocess[39] = "bd~_veve~bd~";
    subprocess[40] = "bu~_veve~bu~";
    subprocess[41] = "bb~_veve~gg";
    subprocess[42] = "bb~_veve~dd~";
    subprocess[43] = "bb~_veve~uu~";
    subprocess[44] = "bb~_veve~bb~";
    subprocess[45] = "d~d~_veve~d~d~";
    subprocess[46] = "d~u~_veve~d~u~";
    subprocess[47] = "d~s~_veve~d~s~";
    subprocess[48] = "d~c~_veve~d~c~";
    subprocess[49] = "d~b~_veve~d~b~";
    subprocess[50] = "u~u~_veve~u~u~";
    subprocess[51] = "u~c~_veve~u~c~";
    subprocess[52] = "u~b~_veve~u~b~";
    subprocess[53] = "b~b~_veve~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
