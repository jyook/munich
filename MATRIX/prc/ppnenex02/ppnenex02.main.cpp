#include "header.hpp"

#include "ppll02.amplitude.set.hpp"

#include "ppnenex02.contribution.set.hpp"
#include "ppnenex02.event.set.hpp"
#include "ppnenex02.phasespace.set.hpp"
#include "ppnenex02.observable.set.hpp"
#include "ppnenex02.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-veve~+X");

  MUC->csi = new ppnenex02_contribution_set();
  MUC->esi = new ppnenex02_event_set();
  MUC->psi = new ppnenex02_phasespace_set();
  MUC->osi = new ppnenex02_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppll02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppnenex02_phasespace_set();
      MUC->psi->fake_psi->csi = new ppnenex02_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppnenex02_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
