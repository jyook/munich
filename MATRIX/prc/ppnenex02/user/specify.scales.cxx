{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).m();
    // takes sum of momenta of hardest neutrino PARTICLE("nua")[0].momentum and second-hardest neutrino PARTICLE("nua")[1].momentum 
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).m();
    double pT = (PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).pT();
    temp_mu_central = sqrt(pow(m,2)+pow(pT,2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
