logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_neutrino_cuts = USERSWITCH("neutrino_cuts");
  static double cut_min_pT_1st_nu = USERCUT("min_pT_1st_nu");
  static double cut_min_pT_2nd_nu = USERCUT("min_pT_2nd_nu");
  
  // technical lower (3.1 GeV) cut on m_nunu
  double m_nunu = sqrt((PARTICLE("nua")[0].momentum + PARTICLE("nua")[1].momentum).m2());
  if (csi->type_contribution == "RVA" || 
      csi->type_contribution == "RRA" || 
      csi->type_contribution == "RCA" || 
      csi->type_contribution == "VT2" || 
      csi->type_contribution == "CT2"){
	if (m_nunu < 10){
	  cut_ps[i_a] = -1;
	  return;
	}
  }

  // cuts on hardest and second-hardest neutrino
  if (switch_neutrino_cuts){
    double pT_nu_1st = PARTICLE("nua")[0].pT;
    double pT_nu_2nd = PARTICLE("nua")[1].pT;

    logger << LOG_DEBUG_VERBOSE << "p_nu1 = " << PARTICLE("nua")[0].pT << "pT_nu1 = " << pT_nu_1st << endl;
    logger << LOG_DEBUG_VERBOSE << "p_nu2 = " << PARTICLE("nua")[1].pT << "pT_nu2 = " << pT_nu_2nd << endl;
    logger << LOG_DEBUG_VERBOSE << "pT_cut_nu1 = " << cut_min_pT_1st_nu << endl;
    logger << LOG_DEBUG_VERBOSE << "pT_cut_nu2 = " << cut_min_pT_2nd_nu << endl;
    if (pT_nu_1st < cut_min_pT_1st_nu || pT_nu_2nd < cut_min_pT_2nd_nu){
      cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "event cutted because of nuton pT threshold" << endl;
      return;
    }
    else{
      logger << LOG_DEBUG_VERBOSE << "event passed nuton pT threshold" << endl;
    }
  }
}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;
