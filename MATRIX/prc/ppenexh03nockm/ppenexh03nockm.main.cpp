#include "header.hpp"

#include "ppllh03.amplitude.set.hpp"

#include "ppenexh03nockm.contribution.set.hpp"
#include "ppenexh03nockm.event.set.hpp"
#include "ppenexh03nockm.phasespace.set.hpp"
#include "ppenexh03nockm.observable.set.hpp"
#include "ppenexh03nockm.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emve~h+X");

  MUC->csi = new ppenexh03nockm_contribution_set();
  MUC->esi = new ppenexh03nockm_event_set();
  MUC->psi = new ppenexh03nockm_phasespace_set();
  MUC->osi = new ppenexh03nockm_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllh03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppenexh03nockm_phasespace_set();
      MUC->psi->fake_psi->csi = new ppenexh03nockm_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppenexh03nockm_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
