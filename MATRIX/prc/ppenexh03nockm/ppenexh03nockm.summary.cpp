#include "header.hpp"

#include "ppenexh03nockm.summary.hpp"

ppenexh03nockm_summary_generic::ppenexh03nockm_summary_generic(munich * xmunich){
  Logger logger("ppenexh03nockm_summary_generic::ppenexh03nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppenexh03nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppenexh03nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppenexh03nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppenexh03nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppenexh03nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppenexh03nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~h";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gu~_emve~hd~";
    subprocess[3] = "du~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "u~a_emve~hd~";
    subprocess[3] = "du~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gu~_emve~hd~";
    subprocess[3] = "du~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "u~a_emve~hd~";
    subprocess[3] = "du~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~hu";
    subprocess[2] = "gu~_emve~hd~";
    subprocess[3] = "du~_emve~hg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~hu";
    subprocess[2] = "u~a_emve~hd~";
    subprocess[3] = "du~_emve~ha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenexh03nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppenexh03nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_emve~hud~";
    subprocess[2] = "gd_emve~hgu";
    subprocess[3] = "gu~_emve~hgd~";
    subprocess[4] = "dd_emve~hdu";
    subprocess[5] = "du_emve~huu";
    subprocess[6] = "ds_emve~hdc";
    subprocess[7] = "dc_emve~huc";
    subprocess[8] = "dd~_emve~hud~";
    subprocess[9] = "dd~_emve~hcs~";
    subprocess[10] = "du~_emve~hgg";
    subprocess[11] = "du~_emve~hdd~";
    subprocess[12] = "du~_emve~huu~";
    subprocess[13] = "du~_emve~hss~";
    subprocess[14] = "du~_emve~hcc~";
    subprocess[15] = "ds~_emve~hus~";
    subprocess[16] = "dc~_emve~hds~";
    subprocess[17] = "dc~_emve~huc~";
    subprocess[18] = "uu~_emve~hud~";
    subprocess[19] = "uu~_emve~hcs~";
    subprocess[20] = "uc~_emve~hus~";
    subprocess[21] = "d~u~_emve~hd~d~";
    subprocess[22] = "d~c~_emve~hd~s~";
    subprocess[23] = "u~u~_emve~hd~u~";
    subprocess[24] = "u~c~_emve~hd~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
