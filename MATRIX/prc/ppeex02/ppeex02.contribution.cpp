#include "header.hpp"

#include "ppeex02.contribution.set.hpp"

ppeex02_contribution_set::~ppeex02_contribution_set(){
  static Logger logger("ppeex02_contribution_set::~ppeex02_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppeex02_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 6; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppeex02_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> e   ex   //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppeex02_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_process_parton[i_a] = 24; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   5)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -1)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -3)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  -5)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==   0)){no_process_parton[i_a] = 16; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppeex02_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   ex  d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   ex  s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   ex  d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   ex  s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   ex  u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   ex  c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   ex  u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   ex  c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   ex  b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   ex  b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   ex  dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   ex  sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   ex  dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   ex  sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   ex  ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   ex  cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   ex  ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   ex  cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   ex  bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   ex  bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex  g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> e   ex  d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> e   ex  s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> e   ex  d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> e   ex  s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> e   ex  u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> e   ex  c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> e   ex  u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> e   ex  c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> e   ex  b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> e   ex  b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> e   ex  dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> e   ex  sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> e   ex  dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> e   ex  sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   ex  ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> e   ex  cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> e   ex  ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> e   ex  cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> e   ex  bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> e   ex  bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex  a    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> e   ex  a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  g   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   ex  d   //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   ex  s   //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   ex  d   //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   ex  s   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   ex  u   //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   ex  c   //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   ex  u   //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   ex  c   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   ex  b   //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   ex  b   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   ex  dx  //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   ex  sx  //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   ex  dx  //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   ex  sx  //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   ex  ux  //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   ex  cx  //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   ex  ux  //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   ex  cx  //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   ex  bx  //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   ex  bx  //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  g   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  g   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  g   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  g   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  g   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  g   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  g   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  g   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  g   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex  g   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppeex02_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[out[5]] ==  -5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppeex02_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   ex  s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   ex  c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   ex  b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   ex  g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   ex  g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   ex  g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   ex  g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> e   ex  g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> e   ex  g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> e   ex  g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> e   ex  g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> e   ex  g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> e   ex  g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> e   ex  g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> e   ex  g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> e   ex  g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> e   ex  g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   ex  g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   ex  g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   ex  g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   ex  g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> e   ex  g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> e   ex  g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> e   ex  d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> e   ex  s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   ex  d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> e   ex  s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> e   ex  d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> e   ex  s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   ex  d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> e   ex  d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   ex  d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> e   ex  u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> e   ex  u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> e   ex  d   c    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   ex  d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> e   ex  s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> e   ex  d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> e   ex  s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   ex  b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   ex  b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   ex  b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   ex  b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   ex  d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   ex  s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   ex  d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   ex  s   cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   ex  d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> e   ex  s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> e   ex  s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> e   ex  d   sx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   ex  d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> e   ex  s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> e   ex  s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> e   ex  d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> e   ex  d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> e   ex  s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> e   ex  d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> e   ex  s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> e   ex  u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> e   ex  c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> e   ex  u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> e   ex  u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> e   ex  u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> e   ex  c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> e   ex  u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> e   ex  c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> e   ex  u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> e   ex  c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> e   ex  u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> e   ex  c   sx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   ex  b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   ex  b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   ex  b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   ex  b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> e   ex  u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> e   ex  c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> e   ex  c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> e   ex  u   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   ex  u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> e   ex  c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> e   ex  c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> e   ex  u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> e   ex  u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> e   ex  c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> e   ex  u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> e   ex  c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> e   ex  b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> e   ex  b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> e   ex  b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> e   ex  b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> e   ex  b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   ex  b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> e   ex  b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> e   ex  b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> e   ex  b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex  g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> e   ex  s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> e   ex  d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> e   ex  s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> e   ex  c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> e   ex  u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> e   ex  c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   ex  b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> e   ex  b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> e   ex  dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> e   ex  sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   ex  dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> e   ex  sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> e   ex  dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> e   ex  sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> e   ex  dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> e   ex  dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   ex  dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> e   ex  ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> e   ex  ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> e   ex  dx  cx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> e   ex  dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> e   ex  sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> e   ex  dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> e   ex  sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   ex  ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> e   ex  cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   ex  ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> e   ex  ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   ex  ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> e   ex  cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> e   ex  ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> e   ex  cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> e   ex  bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
