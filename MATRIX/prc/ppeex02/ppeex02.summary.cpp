#include "header.hpp"

#include "ppeex02.summary.hpp"

ppeex02_summary_generic::ppeex02_summary_generic(munich * xmunich){
  Logger logger("ppeex02_summary_generic::ppeex02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeex02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeex02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeex02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeex02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeex02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeex02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_born(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subprocess[4] = "aa_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subprocess[4] = "aa_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subprocess[4] = "aa_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emep";
    subprocess[2] = "uu~_emep";
    subprocess[3] = "bb~_emep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepd";
    subprocess[2] = "gu_emepu";
    subprocess[3] = "gb_emepb";
    subprocess[4] = "gd~_emepd~";
    subprocess[5] = "gu~_emepu~";
    subprocess[6] = "gb~_emepb~";
    subprocess[7] = "dd~_emepg";
    subprocess[8] = "uu~_emepg";
    subprocess[9] = "bb~_emepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emepg";
    subprocess[2] = "gd_emepd";
    subprocess[3] = "gu_emepu";
    subprocess[4] = "gb_emepb";
    subprocess[5] = "gd~_emepd~";
    subprocess[6] = "gu~_emepu~";
    subprocess[7] = "gb~_emepb~";
    subprocess[8] = "dd~_emepg";
    subprocess[9] = "uu~_emepg";
    subprocess[10] = "bb~_emepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emepd";
    subprocess[2] = "ua_emepu";
    subprocess[3] = "ba_emepb";
    subprocess[4] = "d~a_emepd~";
    subprocess[5] = "u~a_emepu~";
    subprocess[6] = "b~a_emepb~";
    subprocess[7] = "dd~_emepa";
    subprocess[8] = "uu~_emepa";
    subprocess[9] = "bb~_emepa";
    subprocess[10] = "aa_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepd";
    subprocess[2] = "gu_emepu";
    subprocess[3] = "gb_emepb";
    subprocess[4] = "gd~_emepd~";
    subprocess[5] = "gu~_emepu~";
    subprocess[6] = "gb~_emepb~";
    subprocess[7] = "dd~_emepg";
    subprocess[8] = "uu~_emepg";
    subprocess[9] = "bb~_emepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepd";
    subprocess[2] = "ua_emepu";
    subprocess[3] = "ba_emepb";
    subprocess[4] = "d~a_emepd~";
    subprocess[5] = "u~a_emepu~";
    subprocess[6] = "b~a_emepb~";
    subprocess[7] = "dd~_emepa";
    subprocess[8] = "uu~_emepa";
    subprocess[9] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepd";
    subprocess[2] = "gu_emepu";
    subprocess[3] = "gb_emepb";
    subprocess[4] = "gd~_emepd~";
    subprocess[5] = "gu~_emepu~";
    subprocess[6] = "gb~_emepb~";
    subprocess[7] = "dd~_emepg";
    subprocess[8] = "uu~_emepg";
    subprocess[9] = "bb~_emepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepd";
    subprocess[2] = "ua_emepu";
    subprocess[3] = "ba_emepb";
    subprocess[4] = "d~a_emepd~";
    subprocess[5] = "u~a_emepu~";
    subprocess[6] = "b~a_emepb~";
    subprocess[7] = "dd~_emepa";
    subprocess[8] = "uu~_emepa";
    subprocess[9] = "bb~_emepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeex02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeex02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_emepdd~";
    subprocess[2] = "gg_emepuu~";
    subprocess[3] = "gg_emepbb~";
    subprocess[4] = "gd_emepgd";
    subprocess[5] = "gu_emepgu";
    subprocess[6] = "gb_emepgb";
    subprocess[7] = "gd~_emepgd~";
    subprocess[8] = "gu~_emepgu~";
    subprocess[9] = "gb~_emepgb~";
    subprocess[10] = "dd_emepdd";
    subprocess[11] = "du_emepdu";
    subprocess[12] = "ds_emepds";
    subprocess[13] = "dc_emepdc";
    subprocess[14] = "db_emepdb";
    subprocess[15] = "dd~_emepgg";
    subprocess[16] = "dd~_emepdd~";
    subprocess[17] = "dd~_emepuu~";
    subprocess[18] = "dd~_emepss~";
    subprocess[19] = "dd~_emepcc~";
    subprocess[20] = "dd~_emepbb~";
    subprocess[21] = "du~_emepdu~";
    subprocess[22] = "ds~_emepds~";
    subprocess[23] = "dc~_emepdc~";
    subprocess[24] = "db~_emepdb~";
    subprocess[25] = "uu_emepuu";
    subprocess[26] = "uc_emepuc";
    subprocess[27] = "ub_emepub";
    subprocess[28] = "ud~_emepud~";
    subprocess[29] = "uu~_emepgg";
    subprocess[30] = "uu~_emepdd~";
    subprocess[31] = "uu~_emepuu~";
    subprocess[32] = "uu~_emepss~";
    subprocess[33] = "uu~_emepcc~";
    subprocess[34] = "uu~_emepbb~";
    subprocess[35] = "us~_emepus~";
    subprocess[36] = "uc~_emepuc~";
    subprocess[37] = "ub~_emepub~";
    subprocess[38] = "bb_emepbb";
    subprocess[39] = "bd~_emepbd~";
    subprocess[40] = "bu~_emepbu~";
    subprocess[41] = "bb~_emepgg";
    subprocess[42] = "bb~_emepdd~";
    subprocess[43] = "bb~_emepuu~";
    subprocess[44] = "bb~_emepbb~";
    subprocess[45] = "d~d~_emepd~d~";
    subprocess[46] = "d~u~_emepd~u~";
    subprocess[47] = "d~s~_emepd~s~";
    subprocess[48] = "d~c~_emepd~c~";
    subprocess[49] = "d~b~_emepd~b~";
    subprocess[50] = "u~u~_emepu~u~";
    subprocess[51] = "u~c~_emepu~c~";
    subprocess[52] = "u~b~_emepu~b~";
    subprocess[53] = "b~b~_emepb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
