logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_M_leplep = USERSWITCH("M_leplep");
  static double cut_min_M_leplep = USERCUT("min_M_leplep");
  static double cut_min_M2_leplep = pow(cut_min_M_leplep, 2);
  static double cut_max_M_leplep = USERCUT("max_M_leplep");
  static double cut_max_M2_leplep = pow(cut_max_M_leplep, 2);

  static int switch_cut_delta_M_leplep_MZ = USERSWITCH("delta_M_leplep_MZ");
  static double cut_max_delta_M_leplep_MZ = USERCUT("max_delta_M_leplep_MZ");

  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);
    
  // pT cut on harder lepton from Z decay
  /*
  static int switch_pT_lep_1st = USERSWITCH("pT_lep_1st");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");
  */
  static int switch_lepton_cuts = USERSWITCH("lepton_cuts");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");
  static double cut_min_pT_lep_2nd = USERCUT("min_pT_lep_2nd");



  // lepton--lepton isolation cuts
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_leplep = " << switch_cut_R_leplep << endl; }
  if (switch_cut_R_leplep == 1){
    double R2_eta_leplep = R2_eta(PARTICLE("lep")[0], PARTICLE("lep")[1]);

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_leplep = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}

    if (R2_eta_leplep < cut_min_R2_leplep){
      cut_ps[i_a] = -1;
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_leplep" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_R_leplep cut applied" << endl; 
      return;
    }
  }
    
  // lepton invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep = " << switch_cut_M_leplep << endl;}
  if (switch_cut_M_leplep || switch_cut_delta_M_leplep_MZ){
    double M2_leplep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum).m2();
    if (switch_cut_M_leplep) {
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " > " << cut_min_M2_leplep << endl;}

      if (M2_leplep < cut_min_M2_leplep){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl;
	  logger << LOG_DEBUG << endl << info_cut.str();
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep min cut applied" << endl; 
	return;
      }
      
      if (switch_output_cutinfo){if (cut_max_M2_leplep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_leplep = " << M2_leplep << " < " << cut_max_M2_leplep << endl;}}

      if (cut_max_M2_leplep != 0. && M2_leplep > cut_max_M2_leplep){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_leplep" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep max cut applied" << endl; 
	return;
      }
    }

    if (switch_cut_delta_M_leplep_MZ){
      if (abs(sqrt(M2_leplep) - msi->M_Z) > cut_max_delta_M_leplep_MZ) {
	cut_ps[i_a] = -1;
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppeexexne04-cut after cut_max_delta_M_leplep_MZ" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_max_delta_M_leplep_MZ cut applied" << endl; 
	return;
      }
    }
  }

  // cuts on hardest and second-hardest electrons and on hardest muon (in particular needed in "SF" case)
  /*
  if (switch_pT_lep_1st){
    if (PARTICLE("lep")[0].pT < cut_min_pT_lep_1st){
      cut_ps[i_a] = -1; 
      return;
    }
  }
  */
    
  // cuts on hardest and second-hardest lepton
  if (switch_lepton_cuts){
    double pT_lep_1st = PARTICLE("lep")[0].pT;
    double pT_lep_2nd = PARTICLE("lep")[1].pT;

    logger << LOG_DEBUG_VERBOSE << "p_lep1 = " << PARTICLE("lep")[0].pT << "pT_lep1 = " << pT_lep_1st << endl;
    logger << LOG_DEBUG_VERBOSE << "p_lep2 = " << PARTICLE("lep")[1].pT << "pT_lep2 = " << pT_lep_2nd << endl;
    logger << LOG_DEBUG_VERBOSE << "pT_cut_lep1 = " << cut_min_pT_lep_1st << endl;
    logger << LOG_DEBUG_VERBOSE << "pT_cut_lep2 = " << cut_min_pT_lep_2nd << endl;
    if (pT_lep_1st < cut_min_pT_lep_1st || pT_lep_2nd < cut_min_pT_lep_2nd){
      cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "event cutted because of lepton pT threshold" << endl;
      return;
    }
    else{
      logger << LOG_DEBUG_VERBOSE << "event passed lepton pT threshold" << endl;
    }
  }


  if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}

}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;

