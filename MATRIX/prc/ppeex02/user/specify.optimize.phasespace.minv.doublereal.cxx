{
  static int switch_cut_M_leplep = user->switch_value[user->switch_map["M_leplep"]];
  static int switch_cut_delta_M_leplep_MZ = user->switch_value[user->switch_map["delta_M_leplep_MZ"]];
  if (switch_cut_M_leplep || switch_cut_delta_M_leplep_MZ){
    static double cut_min_M_leplep = user->cut_value[user->cut_map["min_M_leplep"]];
    static double cut_max_delta_M_leplep_MZ = user->cut_value[user->cut_map["max_delta_M_leplep_MZ"]];
    for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
      if (switch_cut_M_leplep && !switch_cut_delta_M_leplep_MZ){sqrtsmin_opt[i_a][12] = cut_min_M_leplep;}
      if (!switch_cut_M_leplep && switch_cut_delta_M_leplep_MZ){sqrtsmin_opt[i_a][12] = M[23] - cut_max_delta_M_leplep_MZ;}
      if (switch_cut_M_leplep && switch_cut_delta_M_leplep_MZ){sqrtsmin_opt[i_a][12] = max(cut_min_M_leplep, M[23] - cut_max_delta_M_leplep_MZ);}
    }
  }
}
