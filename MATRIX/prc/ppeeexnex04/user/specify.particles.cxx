logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  USERPARTICLE("<particle_name>").push_back(<particle>);

  static int switch_lepton_identification = USERSWITCH("lepton_identification");

  //-----------------------------------------------------------------//
  // apply additional cuts on lepton identified to come from W-boson //
  //-----------------------------------------------------------------//

  // in the equal-flavor case we first have to identify the leptons coming from the Z and the W boson

  vector<particle> lm;
  particle nu;
  particle lp;
  if (csi->process_class == "pp-ememepve~+X"){
    lm = PARTICLE("em"); // list of electrons
    nu = PARTICLE("nea")[0]; // neutrino
    lp = PARTICLE("ep")[0]; // list of positrons
  }
  else if (csi->process_class == "pp-mummummupvm~+X"){
    lm = PARTICLE("mum"); // list of muons
    nu = PARTICLE("nma")[0]; // neutrino
    lp = PARTICLE("mup")[0]; // list of anti-muons
  }
  else {
    logger << LOG_FATAL << "Particles are not defined for " << csi->process_class << "." << endl;
    exit(1);
  }
  
  if (switch_lepton_identification == 1){ // ATLAS prescription using "resonant shape" algorithm
    double m2ll_1 = (lm[0].momentum + lp.momentum).m2();
    double m2lpnu_1 = (lm[1].momentum + nu.momentum).m2();

    double P_estimator_1 = 1. / ((pow(m2ll_1 - msi->M2_Z, 2) + msi->M2_Z * pow(msi->Gamma_Z, 2)) * (pow(m2lpnu_1 - msi->M2_W, 2) + msi->M2_W * pow(msi->Gamma_W, 2)));
    
    double m2ll_2 = (lm[1].momentum + lp.momentum).m2();
    double m2lpnu_2 = (lm[0].momentum + nu.momentum).m2();
    double P_estimator_2 = 1. / ((pow(m2ll_2 - msi->M2_Z, 2) + msi->M2_Z * pow(msi->Gamma_Z, 2)) * (pow(m2lpnu_2 - msi->M2_W, 2) + msi->M2_W * pow(msi->Gamma_W, 2)));
    
    if (P_estimator_1 > P_estimator_2){
      USERPARTICLE("Zrec").push_back(lm[0] + lp);
      USERPARTICLE("Wrec").push_back(lm[1] + nu);
      USERPARTICLE("lepW").push_back(lm[1]);
      USERPARTICLE("lmZ").push_back(lm[0]);
      if(lm[0].momentum.pT() > lp.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lm[0]);
	  USERPARTICLE("lepZ").push_back(lp);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lp);
	  USERPARTICLE("lepZ").push_back(lm[0]);
	}
    }
    else {
      USERPARTICLE("Zrec").push_back(lm[1] + lp);
      USERPARTICLE("Wrec").push_back(lm[0] + nu);
      USERPARTICLE("lepW").push_back(lm[0]);
      USERPARTICLE("lmZ").push_back(lm[1]);
      if(lm[1].momentum.pT() > lp.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lm[1]);
	  USERPARTICLE("lepZ").push_back(lp);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lp);
	  USERPARTICLE("lepZ").push_back(lm[1]);
	}
    }
  }



  else if (switch_lepton_identification == 2){ // CMS prescription reconstructing leptons from Z by invariant mass of two leptons closest to Z mass
    double mll_1 = (lm[0].momentum + lp.momentum).m();
    double P_estimator_1 = abs(mll_1 - msi->M_Z);
    double mll_2 = (lm[1].momentum + lp.momentum).m();
    double P_estimator_2 = abs(mll_2 - msi->M_Z);

    if (P_estimator_1 < P_estimator_2){
      USERPARTICLE("Zrec").push_back(lm[0] + lp);
      USERPARTICLE("Wrec").push_back(lm[1] + nu);
      USERPARTICLE("lepW").push_back(lm[1]);
      USERPARTICLE("lmZ").push_back(lm[0]);
      if(lm[0].momentum.pT() > lp.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lm[0]);
	  USERPARTICLE("lepZ").push_back(lp);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lp);
	  USERPARTICLE("lepZ").push_back(lm[0]);
	}
    }
    else {
      USERPARTICLE("Zrec").push_back(lm[1] + lp);
      USERPARTICLE("Wrec").push_back(lm[0] + nu);
      USERPARTICLE("lepW").push_back(lm[0]);
      USERPARTICLE("lmZ").push_back(lm[1]);
      if(lm[1].momentum.pT() > lp.momentum.pT()) // respect pT ordering
	{
	  USERPARTICLE("lepZ").push_back(lm[1]);
	  USERPARTICLE("lepZ").push_back(lp);
	}
      else
	{
	  USERPARTICLE("lepZ").push_back(lp);
	  USERPARTICLE("lepZ").push_back(lm[1]);
	}
    }
  }
}

logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
