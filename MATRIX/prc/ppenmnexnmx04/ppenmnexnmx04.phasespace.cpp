#include "header.hpp"

#include "ppenmnexnmx04.phasespace.set.hpp"

ppenmnexnmx04_phasespace_set::~ppenmnexnmx04_phasespace_set(){
  static Logger logger("ppenmnexnmx04_phasespace_set::~ppenmnexnmx04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppenmnexnmx04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenmnexnmx04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 6;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_040_dux_emvmvexvmx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_040_dux_emvmvexvmx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppenmnexnmx04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_040_dux_emvmvexvmx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppenmnexnmx04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenmnexnmx04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 22;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 14;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 54;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 30;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_140_gd_emvmvexvmxu(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_140_gux_emvmvexvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_140_dux_emvmvexvmxg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_050_da_emvmvexvmxu(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_050_uxa_emvmvexvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_050_dux_emvmvexvmxa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_140_gd_emvmvexvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_140_gux_emvmvexvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_140_dux_emvmvexvmxg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_050_da_emvmvexvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_050_uxa_emvmvexvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_050_dux_emvmvexvmxa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppenmnexnmx04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_140_gd_emvmvexvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_140_gux_emvmvexvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_140_dux_emvmvexvmxg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_050_da_emvmvexvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_050_uxa_emvmvexvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_050_dux_emvmvexvmxa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppenmnexnmx04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenmnexnmx04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 146;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 36;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 36;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 18;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 18;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 18;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 18;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppenmnexnmx04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_240_gg_emvmvexvmxudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gd_emvmvexvmxgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gux_emvmvexvmxgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_dd_emvmvexvmxdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_du_emvmvexvmxuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_ds_emvmvexvmxdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_dc_emvmvexvmxuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_ddx_emvmvexvmxudx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_ddx_emvmvexvmxcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_240_dux_emvmvexvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_240_dux_emvmvexvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_240_dux_emvmvexvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_240_dux_emvmvexvmxssx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_dux_emvmvexvmxccx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_dsx_emvmvexvmxusx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_dcx_emvmvexvmxdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_dcx_emvmvexvmxucx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_uux_emvmvexvmxudx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_uux_emvmvexvmxcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_240_ucx_emvmvexvmxusx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_dxux_emvmvexvmxdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_dxcx_emvmvexvmxdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_uxux_emvmvexvmxdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_uxcx_emvmvexvmxdxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppenmnexnmx04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_240_gg_emvmvexvmxudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gd_emvmvexvmxgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gux_emvmvexvmxgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_dd_emvmvexvmxdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_du_emvmvexvmxuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_ds_emvmvexvmxdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_dc_emvmvexvmxuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_ddx_emvmvexvmxudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_ddx_emvmvexvmxcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_240_dux_emvmvexvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_240_dux_emvmvexvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_240_dux_emvmvexvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_240_dux_emvmvexvmxssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_dux_emvmvexvmxccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_dsx_emvmvexvmxusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_dcx_emvmvexvmxdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_dcx_emvmvexvmxucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_uux_emvmvexvmxudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_uux_emvmvexvmxcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_240_ucx_emvmvexvmxusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_dxux_emvmvexvmxdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_dxcx_emvmvexvmxdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_uxux_emvmvexvmxdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_uxcx_emvmvexvmxdxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenmnexnmx04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppenmnexnmx04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_240_gg_emvmvexvmxudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gd_emvmvexvmxgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gux_emvmvexvmxgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_dd_emvmvexvmxdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_du_emvmvexvmxuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_ds_emvmvexvmxdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_dc_emvmvexvmxuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_ddx_emvmvexvmxudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_ddx_emvmvexvmxcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_240_dux_emvmvexvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_240_dux_emvmvexvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_240_dux_emvmvexvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_240_dux_emvmvexvmxssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_dux_emvmvexvmxccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_dsx_emvmvexvmxusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_dcx_emvmvexvmxdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_dcx_emvmvexvmxucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_uux_emvmvexvmxudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_uux_emvmvexvmxcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_240_ucx_emvmvexvmxusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_dxux_emvmvexvmxdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_dxcx_emvmvexvmxdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_uxux_emvmvexvmxdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_uxcx_emvmvexvmxdxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
