#include "header.hpp"

#include "ppnenmnexnmx04.summary.hpp"

ppnenmnexnmx04_summary_generic::ppnenmnexnmx04_summary_generic(munich * xmunich){
  Logger logger("ppnenmnexnmx04_summary_generic::ppnenmnexnmx04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppnenmnexnmx04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppnenmnexnmx04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppnenmnexnmx04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppnenmnexnmx04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppnenmnexnmx04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppnenmnexnmx04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_born(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_vevmve~vm~";
    subprocess[2] = "uu~_vevmve~vm~";
    subprocess[3] = "bb~_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_vevmve~vm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_vevmve~vm~d";
    subprocess[2] = "gu_vevmve~vm~u";
    subprocess[3] = "gb_vevmve~vm~b";
    subprocess[4] = "gd~_vevmve~vm~d~";
    subprocess[5] = "gu~_vevmve~vm~u~";
    subprocess[6] = "gb~_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~g";
    subprocess[8] = "uu~_vevmve~vm~g";
    subprocess[9] = "bb~_vevmve~vm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_vevmve~vm~g";
    subprocess[2] = "gd_vevmve~vm~d";
    subprocess[3] = "gu_vevmve~vm~u";
    subprocess[4] = "gb_vevmve~vm~b";
    subprocess[5] = "gd~_vevmve~vm~d~";
    subprocess[6] = "gu~_vevmve~vm~u~";
    subprocess[7] = "gb~_vevmve~vm~b~";
    subprocess[8] = "dd~_vevmve~vm~g";
    subprocess[9] = "uu~_vevmve~vm~g";
    subprocess[10] = "bb~_vevmve~vm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_vevmve~vm~d";
    subprocess[2] = "ua_vevmve~vm~u";
    subprocess[3] = "ba_vevmve~vm~b";
    subprocess[4] = "d~a_vevmve~vm~d~";
    subprocess[5] = "u~a_vevmve~vm~u~";
    subprocess[6] = "b~a_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~a";
    subprocess[8] = "uu~_vevmve~vm~a";
    subprocess[9] = "bb~_vevmve~vm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_vevmve~vm~d";
    subprocess[2] = "gu_vevmve~vm~u";
    subprocess[3] = "gb_vevmve~vm~b";
    subprocess[4] = "gd~_vevmve~vm~d~";
    subprocess[5] = "gu~_vevmve~vm~u~";
    subprocess[6] = "gb~_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~g";
    subprocess[8] = "uu~_vevmve~vm~g";
    subprocess[9] = "bb~_vevmve~vm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_vevmve~vm~d";
    subprocess[2] = "ua_vevmve~vm~u";
    subprocess[3] = "ba_vevmve~vm~b";
    subprocess[4] = "d~a_vevmve~vm~d~";
    subprocess[5] = "u~a_vevmve~vm~u~";
    subprocess[6] = "b~a_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~a";
    subprocess[8] = "uu~_vevmve~vm~a";
    subprocess[9] = "bb~_vevmve~vm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_vevmve~vm~d";
    subprocess[2] = "gu_vevmve~vm~u";
    subprocess[3] = "gb_vevmve~vm~b";
    subprocess[4] = "gd~_vevmve~vm~d~";
    subprocess[5] = "gu~_vevmve~vm~u~";
    subprocess[6] = "gb~_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~g";
    subprocess[8] = "uu~_vevmve~vm~g";
    subprocess[9] = "bb~_vevmve~vm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_vevmve~vm~d";
    subprocess[2] = "ua_vevmve~vm~u";
    subprocess[3] = "ba_vevmve~vm~b";
    subprocess[4] = "d~a_vevmve~vm~d~";
    subprocess[5] = "u~a_vevmve~vm~u~";
    subprocess[6] = "b~a_vevmve~vm~b~";
    subprocess[7] = "dd~_vevmve~vm~a";
    subprocess[8] = "uu~_vevmve~vm~a";
    subprocess[9] = "bb~_vevmve~vm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppnenmnexnmx04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_vevmve~vm~dd~";
    subprocess[2] = "gg_vevmve~vm~uu~";
    subprocess[3] = "gg_vevmve~vm~bb~";
    subprocess[4] = "gd_vevmve~vm~gd";
    subprocess[5] = "gu_vevmve~vm~gu";
    subprocess[6] = "gb_vevmve~vm~gb";
    subprocess[7] = "gd~_vevmve~vm~gd~";
    subprocess[8] = "gu~_vevmve~vm~gu~";
    subprocess[9] = "gb~_vevmve~vm~gb~";
    subprocess[10] = "dd_vevmve~vm~dd";
    subprocess[11] = "du_vevmve~vm~du";
    subprocess[12] = "ds_vevmve~vm~ds";
    subprocess[13] = "dc_vevmve~vm~dc";
    subprocess[14] = "db_vevmve~vm~db";
    subprocess[15] = "dd~_vevmve~vm~gg";
    subprocess[16] = "dd~_vevmve~vm~dd~";
    subprocess[17] = "dd~_vevmve~vm~uu~";
    subprocess[18] = "dd~_vevmve~vm~ss~";
    subprocess[19] = "dd~_vevmve~vm~cc~";
    subprocess[20] = "dd~_vevmve~vm~bb~";
    subprocess[21] = "du~_vevmve~vm~du~";
    subprocess[22] = "ds~_vevmve~vm~ds~";
    subprocess[23] = "dc~_vevmve~vm~dc~";
    subprocess[24] = "db~_vevmve~vm~db~";
    subprocess[25] = "uu_vevmve~vm~uu";
    subprocess[26] = "uc_vevmve~vm~uc";
    subprocess[27] = "ub_vevmve~vm~ub";
    subprocess[28] = "ud~_vevmve~vm~ud~";
    subprocess[29] = "uu~_vevmve~vm~gg";
    subprocess[30] = "uu~_vevmve~vm~dd~";
    subprocess[31] = "uu~_vevmve~vm~uu~";
    subprocess[32] = "uu~_vevmve~vm~ss~";
    subprocess[33] = "uu~_vevmve~vm~cc~";
    subprocess[34] = "uu~_vevmve~vm~bb~";
    subprocess[35] = "us~_vevmve~vm~us~";
    subprocess[36] = "uc~_vevmve~vm~uc~";
    subprocess[37] = "ub~_vevmve~vm~ub~";
    subprocess[38] = "bb_vevmve~vm~bb";
    subprocess[39] = "bd~_vevmve~vm~bd~";
    subprocess[40] = "bu~_vevmve~vm~bu~";
    subprocess[41] = "bb~_vevmve~vm~gg";
    subprocess[42] = "bb~_vevmve~vm~dd~";
    subprocess[43] = "bb~_vevmve~vm~uu~";
    subprocess[44] = "bb~_vevmve~vm~bb~";
    subprocess[45] = "d~d~_vevmve~vm~d~d~";
    subprocess[46] = "d~u~_vevmve~vm~d~u~";
    subprocess[47] = "d~s~_vevmve~vm~d~s~";
    subprocess[48] = "d~c~_vevmve~vm~d~c~";
    subprocess[49] = "d~b~_vevmve~vm~d~b~";
    subprocess[50] = "u~u~_vevmve~vm~u~u~";
    subprocess[51] = "u~c~_vevmve~vm~u~c~";
    subprocess[52] = "u~b~_vevmve~vm~u~b~";
    subprocess[53] = "b~b~_vevmve~vm~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
