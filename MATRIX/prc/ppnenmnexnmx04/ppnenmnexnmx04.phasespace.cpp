#include "header.hpp"

#include "ppnenmnexnmx04.phasespace.set.hpp"

ppnenmnexnmx04_phasespace_set::~ppnenmnexnmx04_phasespace_set(){
  static Logger logger("ppnenmnexnmx04_phasespace_set::~ppnenmnexnmx04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppnenmnexnmx04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenmnexnmx04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 11;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -23};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-23);
      tau_MC_map.push_back(-25);
      tau_MC_map.push_back(-36);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_040_ddx_vevmvexvmx(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_040_uux_vevmvexvmx(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_040_bbx_vevmvexvmx(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_240_gg_vevmvexvmx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_040_ddx_vevmvexvmx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_040_uux_vevmvexvmx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_040_bbx_vevmvexvmx(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_240_gg_vevmvexvmx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_040_ddx_vevmvexvmx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_040_uux_vevmvexvmx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_040_bbx_vevmvexvmx(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_240_gg_vevmvexvmx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppnenmnexnmx04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenmnexnmx04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 22;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 14;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 14;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 22;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 14;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 14;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 90;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 15;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 15;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 13){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 15){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 17){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_140_gd_vevmvexvmxd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_140_gu_vevmvexvmxu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_140_gb_vevmvexvmxb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_140_gdx_vevmvexvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_140_gux_vevmvexvmxux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_140_gbx_vevmvexvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_140_ddx_vevmvexvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_140_uux_vevmvexvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_140_bbx_vevmvexvmxg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_050_da_vevmvexvmxd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_050_ua_vevmvexvmxu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_050_ba_vevmvexvmxb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_050_dxa_vevmvexvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_050_uxa_vevmvexvmxux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_050_bxa_vevmvexvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_050_ddx_vevmvexvmxa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_050_uux_vevmvexvmxa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_050_bbx_vevmvexvmxa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_340_gg_vevmvexvmxg(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_340_gd_vevmvexvmxd(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_340_gu_vevmvexvmxu(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_340_gb_vevmvexvmxb(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_340_gdx_vevmvexvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_340_gux_vevmvexvmxux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_340_gbx_vevmvexvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_340_ddx_vevmvexvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_340_uux_vevmvexvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_340_bbx_vevmvexvmxg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_140_gd_vevmvexvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_140_gu_vevmvexvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_140_gb_vevmvexvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_140_gdx_vevmvexvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_140_gux_vevmvexvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_140_gbx_vevmvexvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_140_ddx_vevmvexvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_140_uux_vevmvexvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_140_bbx_vevmvexvmxg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_050_da_vevmvexvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_050_ua_vevmvexvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_050_ba_vevmvexvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_050_dxa_vevmvexvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_050_uxa_vevmvexvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_050_bxa_vevmvexvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_050_ddx_vevmvexvmxa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_050_uux_vevmvexvmxa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_050_bbx_vevmvexvmxa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_340_gg_vevmvexvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_340_gd_vevmvexvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_340_gu_vevmvexvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_340_gb_vevmvexvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_340_gdx_vevmvexvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_340_gux_vevmvexvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_340_gbx_vevmvexvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_340_ddx_vevmvexvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_340_uux_vevmvexvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_340_bbx_vevmvexvmxg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_140_gd_vevmvexvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_140_gu_vevmvexvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_140_gb_vevmvexvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_140_gdx_vevmvexvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_140_gux_vevmvexvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_140_gbx_vevmvexvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_140_ddx_vevmvexvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_140_uux_vevmvexvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_140_bbx_vevmvexvmxg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_050_da_vevmvexvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_050_ua_vevmvexvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_050_ba_vevmvexvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_050_dxa_vevmvexvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_050_uxa_vevmvexvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_050_bxa_vevmvexvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_050_ddx_vevmvexvmxa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_050_uux_vevmvexvmxa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_050_bbx_vevmvexvmxa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_340_gg_vevmvexvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_340_gd_vevmvexvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_340_gu_vevmvexvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_340_gb_vevmvexvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_340_gdx_vevmvexvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_340_gux_vevmvexvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_340_gbx_vevmvexvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_340_ddx_vevmvexvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_340_uux_vevmvexvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_340_bbx_vevmvexvmxg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppnenmnexnmx04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppnenmnexnmx04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 146;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 62;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 62;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 72;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 72;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gg_vevmvexvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gg_vevmvexvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_240_gg_vevmvexvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_gd_vevmvexvmxgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_gu_vevmvexvmxgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_gb_vevmvexvmxgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_gdx_vevmvexvmxgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_gux_vevmvexvmxgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_gbx_vevmvexvmxgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_dd_vevmvexvmxdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_du_vevmvexvmxdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_ds_vevmvexvmxds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_dc_vevmvexvmxdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_db_vevmvexvmxdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_ddx_vevmvexvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_240_ddx_vevmvexvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_ddx_vevmvexvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_ddx_vevmvexvmxssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_ddx_vevmvexvmxccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_ddx_vevmvexvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_240_dux_vevmvexvmxdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_240_dsx_vevmvexvmxdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_240_dcx_vevmvexvmxdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_240_dbx_vevmvexvmxdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_240_uu_vevmvexvmxuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_240_uc_vevmvexvmxuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_240_ub_vevmvexvmxub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_240_udx_vevmvexvmxudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_240_uux_vevmvexvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_240_uux_vevmvexvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_240_uux_vevmvexvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_240_uux_vevmvexvmxssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_240_uux_vevmvexvmxccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_240_uux_vevmvexvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_240_usx_vevmvexvmxusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_240_ucx_vevmvexvmxucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_240_ubx_vevmvexvmxubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_240_bb_vevmvexvmxbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_240_bdx_vevmvexvmxbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_240_bux_vevmvexvmxbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_240_bbx_vevmvexvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_240_bbx_vevmvexvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_240_bbx_vevmvexvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_240_bbx_vevmvexvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_240_dxdx_vevmvexvmxdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_240_dxux_vevmvexvmxdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_240_dxsx_vevmvexvmxdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_240_dxcx_vevmvexvmxdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_240_dxbx_vevmvexvmxdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_240_uxux_vevmvexvmxuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_240_uxcx_vevmvexvmxuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_240_uxbx_vevmvexvmxuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_240_bxbx_vevmvexvmxbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gg_vevmvexvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gg_vevmvexvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_240_gg_vevmvexvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_gd_vevmvexvmxgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_gu_vevmvexvmxgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_gb_vevmvexvmxgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_gdx_vevmvexvmxgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_gux_vevmvexvmxgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_gbx_vevmvexvmxgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_dd_vevmvexvmxdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_du_vevmvexvmxdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_ds_vevmvexvmxds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_dc_vevmvexvmxdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_db_vevmvexvmxdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_ddx_vevmvexvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_240_ddx_vevmvexvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_ddx_vevmvexvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_ddx_vevmvexvmxssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_ddx_vevmvexvmxccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_ddx_vevmvexvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_240_dux_vevmvexvmxdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_240_dsx_vevmvexvmxdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_240_dcx_vevmvexvmxdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_240_dbx_vevmvexvmxdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_240_uu_vevmvexvmxuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_240_uc_vevmvexvmxuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_240_ub_vevmvexvmxub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_240_udx_vevmvexvmxudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_240_uux_vevmvexvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_240_uux_vevmvexvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_240_uux_vevmvexvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_240_uux_vevmvexvmxssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_240_uux_vevmvexvmxccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_240_uux_vevmvexvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_240_usx_vevmvexvmxusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_240_ucx_vevmvexvmxucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_240_ubx_vevmvexvmxubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_240_bb_vevmvexvmxbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_240_bdx_vevmvexvmxbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_240_bux_vevmvexvmxbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_240_bbx_vevmvexvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_240_bbx_vevmvexvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_240_bbx_vevmvexvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_240_bbx_vevmvexvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_240_dxdx_vevmvexvmxdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_240_dxux_vevmvexvmxdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_240_dxsx_vevmvexvmxdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_240_dxcx_vevmvexvmxdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_240_dxbx_vevmvexvmxdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_240_uxux_vevmvexvmxuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_240_uxcx_vevmvexvmxuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_240_uxbx_vevmvexvmxuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_240_bxbx_vevmvexvmxbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppnenmnexnmx04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppnenmnexnmx04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gg_vevmvexvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gg_vevmvexvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_240_gg_vevmvexvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_gd_vevmvexvmxgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_gu_vevmvexvmxgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_gb_vevmvexvmxgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_gdx_vevmvexvmxgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_gux_vevmvexvmxgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_gbx_vevmvexvmxgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_dd_vevmvexvmxdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_du_vevmvexvmxdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_ds_vevmvexvmxds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_dc_vevmvexvmxdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_db_vevmvexvmxdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_ddx_vevmvexvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_240_ddx_vevmvexvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_ddx_vevmvexvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_ddx_vevmvexvmxssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_ddx_vevmvexvmxccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_ddx_vevmvexvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_240_dux_vevmvexvmxdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_240_dsx_vevmvexvmxdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_240_dcx_vevmvexvmxdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_240_dbx_vevmvexvmxdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_240_uu_vevmvexvmxuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_240_uc_vevmvexvmxuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_240_ub_vevmvexvmxub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_240_udx_vevmvexvmxudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_240_uux_vevmvexvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_240_uux_vevmvexvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_240_uux_vevmvexvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_240_uux_vevmvexvmxssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_240_uux_vevmvexvmxccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_240_uux_vevmvexvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_240_usx_vevmvexvmxusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_240_ucx_vevmvexvmxucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_240_ubx_vevmvexvmxubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_240_bb_vevmvexvmxbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_240_bdx_vevmvexvmxbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_240_bux_vevmvexvmxbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_240_bbx_vevmvexvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_240_bbx_vevmvexvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_240_bbx_vevmvexvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_240_bbx_vevmvexvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_240_dxdx_vevmvexvmxdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_240_dxux_vevmvexvmxdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_240_dxsx_vevmvexvmxdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_240_dxcx_vevmvexvmxdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_240_dxbx_vevmvexvmxdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_240_uxux_vevmvexvmxuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_240_uxcx_vevmvexvmxuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_240_uxbx_vevmvexvmxuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_240_bxbx_vevmvexvmxbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
