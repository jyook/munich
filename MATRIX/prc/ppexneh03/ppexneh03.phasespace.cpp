#include "header.hpp"

#include "ppexneh03.phasespace.set.hpp"

ppexneh03_phasespace_set::~ppexneh03_phasespace_set(){
  static Logger logger("ppexneh03_phasespace_set::~ppexneh03_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::optimize_minv_born(){
  static Logger logger("ppexneh03_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppexneh03_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppexneh03_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_030_udx_epveh(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_030_usx_epveh(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_030_ubx_epveh(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_030_cdx_epveh(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_030_csx_epveh(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_030_cbx_epveh(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppexneh03_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_030_udx_epveh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_030_usx_epveh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_030_ubx_epveh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_030_cdx_epveh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_030_csx_epveh(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_030_cbx_epveh(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppexneh03_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_030_udx_epveh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_030_usx_epveh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_030_ubx_epveh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_030_cdx_epveh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_030_csx_epveh(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_030_cbx_epveh(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::optimize_minv_real(){
  static Logger logger("ppexneh03_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppexneh03_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 12;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 36;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 33){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 5;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 33){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0, -24};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppexneh03_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_130_gu_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_130_gu_epvehs(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_130_gu_epvehb(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_130_gc_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_130_gc_epvehs(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_130_gc_epvehb(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_130_gdx_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_130_gdx_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_130_gsx_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_130_gsx_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_130_gbx_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_130_gbx_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_130_udx_epvehg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_130_usx_epvehg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_130_ubx_epvehg(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_130_cdx_epvehg(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_130_csx_epvehg(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_130_cbx_epvehg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_040_ua_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_040_ua_epvehs(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_040_ua_epvehb(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_040_ca_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_040_ca_epvehs(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_040_ca_epvehb(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_040_dxa_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_040_dxa_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_040_sxa_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_040_sxa_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_040_bxa_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_040_bxa_epvehcx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_040_udx_epveha(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_040_usx_epveha(x_a);}
    else if (csi->no_process_parton[x_a] == 33){ax_psp_040_ubx_epveha(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_040_cdx_epveha(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_040_csx_epveha(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_040_cbx_epveha(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppexneh03_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_130_gu_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_130_gu_epvehs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_130_gu_epvehb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_130_gc_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_130_gc_epvehs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_130_gc_epvehb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_130_gdx_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_130_gdx_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_130_gsx_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_130_gsx_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_130_gbx_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_130_gbx_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_130_udx_epvehg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_130_usx_epvehg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_130_ubx_epvehg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_130_cdx_epvehg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_130_csx_epvehg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_130_cbx_epvehg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_040_ua_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_040_ua_epvehs(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_040_ua_epvehb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_040_ca_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_040_ca_epvehs(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_040_ca_epvehb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_040_dxa_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_040_dxa_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_040_sxa_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_040_sxa_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_040_bxa_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_040_bxa_epvehcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_040_udx_epveha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_040_usx_epveha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 33){ac_psp_040_ubx_epveha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_040_cdx_epveha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_040_csx_epveha(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_040_cbx_epveha(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppexneh03_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_130_gu_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_130_gu_epvehs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_130_gu_epvehb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_130_gc_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_130_gc_epvehs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_130_gc_epvehb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_130_gdx_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_130_gdx_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_130_gsx_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_130_gsx_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_130_gbx_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_130_gbx_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_130_udx_epvehg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_130_usx_epvehg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_130_ubx_epvehg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_130_cdx_epvehg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_130_csx_epvehg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_130_cbx_epvehg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_040_ua_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_040_ua_epvehs(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_040_ua_epvehb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_040_ca_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_040_ca_epvehs(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_040_ca_epvehb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_040_dxa_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_040_dxa_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_040_sxa_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_040_sxa_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_040_bxa_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_040_bxa_epvehcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_040_udx_epveha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_040_usx_epveha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 33){ag_psp_040_ubx_epveha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_040_cdx_epveha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_040_csx_epveha(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_040_cbx_epveha(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppexneh03_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppexneh03_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==   0){n_channel = 60;}
    else if (csi->no_process_parton[x_a] ==   1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  11){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  13){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  14){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  16){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  17){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  18){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  26){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  27){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  32){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  33){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  37){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  38){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  39){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  40){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  41){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  42){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  43){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  44){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  49){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  50){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  55){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  57){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  58){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  62){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  64){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  65){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  67){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  68){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  69){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  70){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  71){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  72){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  75){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  77){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  78){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  79){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  80){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  83){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  84){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  85){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  86){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  89){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  90){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  92){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  93){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  94){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  95){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  96){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  97){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  98){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  99){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 100){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 101){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 103){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 104){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 106){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 109){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 111){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 112){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 114){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 116){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 118){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 119){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 120){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 122){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 123){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 124){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 126){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 127){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 129){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 132){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 137){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 139){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 140){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 144){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 145){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 148){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 149){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 150){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 151){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 152){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 153){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 156){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 157){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 161){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 164){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 165){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 168){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 170){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 171){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 172){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 173){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 176){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 178){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 179){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 181){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 182){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 183){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 185){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 186){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 188){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 190){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 192){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 193){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 194){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 196){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 199){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 201){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 202){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 204){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 205){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 206){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 207){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 208){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 209){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 210){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 211){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 212){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 213){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 215){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 216){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 219){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 220){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 221){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 222){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 225){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 233){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 234){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 239){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 240){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 241){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 242){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 243){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 244){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 245){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 246){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 247){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 248){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 253){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 254){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 256){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 257){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 258){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 260){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 263){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 264){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 265){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 266){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 268){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 270){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 273){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 274){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 276){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 277){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 281){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 283){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 286){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 287){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 290){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 291){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 292){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 293){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 297){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 298){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 302){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 304){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  33){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  49){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  57){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  62){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  64){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  68){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  69){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  70){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  71){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  72){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  75){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  77){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  78){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  79){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  80){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  83){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  84){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  85){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  86){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  89){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  90){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  92){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  93){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  94){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  95){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  96){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  97){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  98){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  99){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 100){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 101){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 103){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 104){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 106){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 109){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 111){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 112){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 114){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 116){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 118){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 119){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 120){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 122){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 123){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 124){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 126){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 127){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 129){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 132){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 137){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 139){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 140){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 144){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 145){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 148){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 149){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 150){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 151){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 152){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 153){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 156){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 157){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 161){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 164){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 165){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 168){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 170){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 171){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 172){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 173){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 176){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 178){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 179){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 181){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 182){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 183){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 185){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 186){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 188){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 190){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 192){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 193){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 194){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 196){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 199){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 201){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 202){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 204){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 205){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 206){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 207){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 208){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 209){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 210){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 211){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 212){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 213){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 215){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 216){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 219){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 220){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 221){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 222){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 225){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 233){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 234){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 239){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 240){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 241){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 242){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 243){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 244){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 245){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 246){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 247){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 248){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 253){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 254){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 256){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 257){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 258){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 260){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 263){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 264){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 265){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 266){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 268){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 270){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 273){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 274){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 276){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 277){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 281){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 283){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 286){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 287){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 290){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 291){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 292){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 293){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 297){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 298){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 302){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 304){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppexneh03_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ax_psp_230_gg_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==   2){ax_psp_230_gg_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==   3){ax_psp_230_gg_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] ==   4){ax_psp_230_gg_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] ==   5){ax_psp_230_gg_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] ==   6){ax_psp_230_gg_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==   7){ax_psp_230_gu_epvehgd(x_a);}
    else if (csi->no_process_parton[x_a] ==   8){ax_psp_230_gu_epvehgs(x_a);}
    else if (csi->no_process_parton[x_a] ==   9){ax_psp_230_gu_epvehgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  10){ax_psp_230_gc_epvehgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  11){ax_psp_230_gc_epvehgs(x_a);}
    else if (csi->no_process_parton[x_a] ==  12){ax_psp_230_gc_epvehgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  13){ax_psp_230_gdx_epvehgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  14){ax_psp_230_gdx_epvehgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  15){ax_psp_230_gsx_epvehgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  16){ax_psp_230_gsx_epvehgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  17){ax_psp_230_gbx_epvehgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  18){ax_psp_230_gbx_epvehgcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  25){ax_psp_230_du_epvehdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  26){ax_psp_230_du_epvehds(x_a);}
    else if (csi->no_process_parton[x_a] ==  27){ax_psp_230_du_epvehdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  31){ax_psp_230_dc_epvehdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  32){ax_psp_230_dc_epvehds(x_a);}
    else if (csi->no_process_parton[x_a] ==  33){ax_psp_230_dc_epvehdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  37){ax_psp_230_ddx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  38){ax_psp_230_ddx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  39){ax_psp_230_ddx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] ==  40){ax_psp_230_ddx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  41){ax_psp_230_ddx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] ==  42){ax_psp_230_ddx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  43){ax_psp_230_dsx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  44){ax_psp_230_dsx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  49){ax_psp_230_dbx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  50){ax_psp_230_dbx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  55){ax_psp_230_uu_epvehdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  57){ax_psp_230_uu_epvehus(x_a);}
    else if (csi->no_process_parton[x_a] ==  58){ax_psp_230_uu_epvehub(x_a);}
    else if (csi->no_process_parton[x_a] ==  62){ax_psp_230_us_epvehds(x_a);}
    else if (csi->no_process_parton[x_a] ==  64){ax_psp_230_us_epvehss(x_a);}
    else if (csi->no_process_parton[x_a] ==  65){ax_psp_230_us_epvehsb(x_a);}
    else if (csi->no_process_parton[x_a] ==  67){ax_psp_230_uc_epvehdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  68){ax_psp_230_uc_epvehdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  69){ax_psp_230_uc_epvehus(x_a);}
    else if (csi->no_process_parton[x_a] ==  70){ax_psp_230_uc_epvehub(x_a);}
    else if (csi->no_process_parton[x_a] ==  71){ax_psp_230_uc_epvehsc(x_a);}
    else if (csi->no_process_parton[x_a] ==  72){ax_psp_230_uc_epvehcb(x_a);}
    else if (csi->no_process_parton[x_a] ==  75){ax_psp_230_ub_epvehdb(x_a);}
    else if (csi->no_process_parton[x_a] ==  77){ax_psp_230_ub_epvehsb(x_a);}
    else if (csi->no_process_parton[x_a] ==  78){ax_psp_230_ub_epvehbb(x_a);}
    else if (csi->no_process_parton[x_a] ==  79){ax_psp_230_udx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  80){ax_psp_230_udx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  83){ax_psp_230_udx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  84){ax_psp_230_udx_epvehucx(x_a);}
    else if (csi->no_process_parton[x_a] ==  85){ax_psp_230_udx_epvehsdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  86){ax_psp_230_udx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  89){ax_psp_230_udx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  90){ax_psp_230_udx_epvehbdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  92){ax_psp_230_udx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  93){ax_psp_230_uux_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  94){ax_psp_230_uux_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  95){ax_psp_230_uux_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] ==  96){ax_psp_230_uux_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  97){ax_psp_230_uux_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] ==  98){ax_psp_230_uux_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] ==  99){ax_psp_230_usx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 100){ax_psp_230_usx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 101){ax_psp_230_usx_epvehdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 103){ax_psp_230_usx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 104){ax_psp_230_usx_epvehucx(x_a);}
    else if (csi->no_process_parton[x_a] == 106){ax_psp_230_usx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 109){ax_psp_230_usx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 111){ax_psp_230_usx_epvehbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 112){ax_psp_230_usx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 114){ax_psp_230_ucx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 116){ax_psp_230_ucx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 118){ax_psp_230_ucx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 119){ax_psp_230_ubx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 120){ax_psp_230_ubx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 122){ax_psp_230_ubx_epvehdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 123){ax_psp_230_ubx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 124){ax_psp_230_ubx_epvehucx(x_a);}
    else if (csi->no_process_parton[x_a] == 126){ax_psp_230_ubx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 127){ax_psp_230_ubx_epvehsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 129){ax_psp_230_ubx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 132){ax_psp_230_ubx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 137){ax_psp_230_sc_epvehds(x_a);}
    else if (csi->no_process_parton[x_a] == 139){ax_psp_230_sc_epvehss(x_a);}
    else if (csi->no_process_parton[x_a] == 140){ax_psp_230_sc_epvehsb(x_a);}
    else if (csi->no_process_parton[x_a] == 144){ax_psp_230_sdx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 145){ax_psp_230_sdx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 148){ax_psp_230_ssx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] == 149){ax_psp_230_ssx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 150){ax_psp_230_ssx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 151){ax_psp_230_ssx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 152){ax_psp_230_ssx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 153){ax_psp_230_ssx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 156){ax_psp_230_sbx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 157){ax_psp_230_sbx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 161){ax_psp_230_cc_epvehdc(x_a);}
    else if (csi->no_process_parton[x_a] == 164){ax_psp_230_cc_epvehsc(x_a);}
    else if (csi->no_process_parton[x_a] == 165){ax_psp_230_cc_epvehcb(x_a);}
    else if (csi->no_process_parton[x_a] == 168){ax_psp_230_cb_epvehdb(x_a);}
    else if (csi->no_process_parton[x_a] == 170){ax_psp_230_cb_epvehsb(x_a);}
    else if (csi->no_process_parton[x_a] == 171){ax_psp_230_cb_epvehbb(x_a);}
    else if (csi->no_process_parton[x_a] == 172){ax_psp_230_cdx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 173){ax_psp_230_cdx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 176){ax_psp_230_cdx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 178){ax_psp_230_cdx_epvehsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 179){ax_psp_230_cdx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 181){ax_psp_230_cdx_epvehcux(x_a);}
    else if (csi->no_process_parton[x_a] == 182){ax_psp_230_cdx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 183){ax_psp_230_cdx_epvehbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 185){ax_psp_230_cdx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 186){ax_psp_230_cux_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] == 188){ax_psp_230_cux_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 190){ax_psp_230_cux_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 192){ax_psp_230_csx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 193){ax_psp_230_csx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 194){ax_psp_230_csx_epvehdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 196){ax_psp_230_csx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 199){ax_psp_230_csx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 201){ax_psp_230_csx_epvehcux(x_a);}
    else if (csi->no_process_parton[x_a] == 202){ax_psp_230_csx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 204){ax_psp_230_csx_epvehbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 205){ax_psp_230_csx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 206){ax_psp_230_ccx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] == 207){ax_psp_230_ccx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 208){ax_psp_230_ccx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 209){ax_psp_230_ccx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 210){ax_psp_230_ccx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 211){ax_psp_230_ccx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 212){ax_psp_230_cbx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 213){ax_psp_230_cbx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 215){ax_psp_230_cbx_epvehdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 216){ax_psp_230_cbx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 219){ax_psp_230_cbx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 220){ax_psp_230_cbx_epvehsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 221){ax_psp_230_cbx_epvehcux(x_a);}
    else if (csi->no_process_parton[x_a] == 222){ax_psp_230_cbx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 225){ax_psp_230_cbx_epvehbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 233){ax_psp_230_bdx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 234){ax_psp_230_bdx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 239){ax_psp_230_bsx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 240){ax_psp_230_bsx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 241){ax_psp_230_bbx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] == 242){ax_psp_230_bbx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 243){ax_psp_230_bbx_epvehsux(x_a);}
    else if (csi->no_process_parton[x_a] == 244){ax_psp_230_bbx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 245){ax_psp_230_bbx_epvehbux(x_a);}
    else if (csi->no_process_parton[x_a] == 246){ax_psp_230_bbx_epvehbcx(x_a);}
    else if (csi->no_process_parton[x_a] == 247){ax_psp_230_dxdx_epvehdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 248){ax_psp_230_dxdx_epvehdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 253){ax_psp_230_dxux_epvehuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 254){ax_psp_230_dxux_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 256){ax_psp_230_dxsx_epvehdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 257){ax_psp_230_dxsx_epvehdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 258){ax_psp_230_dxsx_epvehuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 260){ax_psp_230_dxsx_epvehsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 263){ax_psp_230_dxcx_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 264){ax_psp_230_dxcx_epvehcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 265){ax_psp_230_dxbx_epvehdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 266){ax_psp_230_dxbx_epvehdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 268){ax_psp_230_dxbx_epvehuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 270){ax_psp_230_dxbx_epvehcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 273){ax_psp_230_uxsx_epvehuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 274){ax_psp_230_uxsx_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 276){ax_psp_230_uxbx_epvehuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 277){ax_psp_230_uxbx_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 281){ax_psp_230_sxsx_epvehuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 283){ax_psp_230_sxsx_epvehsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 286){ax_psp_230_sxcx_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 287){ax_psp_230_sxcx_epvehcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 290){ax_psp_230_sxbx_epvehuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 291){ax_psp_230_sxbx_epvehuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 292){ax_psp_230_sxbx_epvehsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 293){ax_psp_230_sxbx_epvehcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 297){ax_psp_230_cxbx_epvehuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 298){ax_psp_230_cxbx_epvehcxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 302){ax_psp_230_bxbx_epvehuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 304){ax_psp_230_bxbx_epvehcxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppexneh03_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ac_psp_230_gg_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   2){ac_psp_230_gg_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   3){ac_psp_230_gg_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   4){ac_psp_230_gg_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   5){ac_psp_230_gg_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   6){ac_psp_230_gg_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   7){ac_psp_230_gu_epvehgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   8){ac_psp_230_gu_epvehgs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   9){ac_psp_230_gu_epvehgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  10){ac_psp_230_gc_epvehgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  11){ac_psp_230_gc_epvehgs(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  12){ac_psp_230_gc_epvehgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  13){ac_psp_230_gdx_epvehgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  14){ac_psp_230_gdx_epvehgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  15){ac_psp_230_gsx_epvehgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  16){ac_psp_230_gsx_epvehgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  17){ac_psp_230_gbx_epvehgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  18){ac_psp_230_gbx_epvehgcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  25){ac_psp_230_du_epvehdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  26){ac_psp_230_du_epvehds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  27){ac_psp_230_du_epvehdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  31){ac_psp_230_dc_epvehdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  32){ac_psp_230_dc_epvehds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  33){ac_psp_230_dc_epvehdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  37){ac_psp_230_ddx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  38){ac_psp_230_ddx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  39){ac_psp_230_ddx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  40){ac_psp_230_ddx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  41){ac_psp_230_ddx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  42){ac_psp_230_ddx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  43){ac_psp_230_dsx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  44){ac_psp_230_dsx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  49){ac_psp_230_dbx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  50){ac_psp_230_dbx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  55){ac_psp_230_uu_epvehdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  57){ac_psp_230_uu_epvehus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  58){ac_psp_230_uu_epvehub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  62){ac_psp_230_us_epvehds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  64){ac_psp_230_us_epvehss(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  65){ac_psp_230_us_epvehsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  67){ac_psp_230_uc_epvehdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  68){ac_psp_230_uc_epvehdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  69){ac_psp_230_uc_epvehus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  70){ac_psp_230_uc_epvehub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  71){ac_psp_230_uc_epvehsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  72){ac_psp_230_uc_epvehcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  75){ac_psp_230_ub_epvehdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  77){ac_psp_230_ub_epvehsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  78){ac_psp_230_ub_epvehbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  79){ac_psp_230_udx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  80){ac_psp_230_udx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  83){ac_psp_230_udx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  84){ac_psp_230_udx_epvehucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  85){ac_psp_230_udx_epvehsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  86){ac_psp_230_udx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  89){ac_psp_230_udx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  90){ac_psp_230_udx_epvehbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  92){ac_psp_230_udx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  93){ac_psp_230_uux_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  94){ac_psp_230_uux_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  95){ac_psp_230_uux_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  96){ac_psp_230_uux_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  97){ac_psp_230_uux_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  98){ac_psp_230_uux_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  99){ac_psp_230_usx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 100){ac_psp_230_usx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 101){ac_psp_230_usx_epvehdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 103){ac_psp_230_usx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 104){ac_psp_230_usx_epvehucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 106){ac_psp_230_usx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 109){ac_psp_230_usx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 111){ac_psp_230_usx_epvehbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 112){ac_psp_230_usx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 114){ac_psp_230_ucx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 116){ac_psp_230_ucx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 118){ac_psp_230_ucx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 119){ac_psp_230_ubx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 120){ac_psp_230_ubx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 122){ac_psp_230_ubx_epvehdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 123){ac_psp_230_ubx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 124){ac_psp_230_ubx_epvehucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 126){ac_psp_230_ubx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 127){ac_psp_230_ubx_epvehsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 129){ac_psp_230_ubx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 132){ac_psp_230_ubx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 137){ac_psp_230_sc_epvehds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 139){ac_psp_230_sc_epvehss(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 140){ac_psp_230_sc_epvehsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 144){ac_psp_230_sdx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 145){ac_psp_230_sdx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 148){ac_psp_230_ssx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 149){ac_psp_230_ssx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 150){ac_psp_230_ssx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 151){ac_psp_230_ssx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 152){ac_psp_230_ssx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 153){ac_psp_230_ssx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 156){ac_psp_230_sbx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 157){ac_psp_230_sbx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 161){ac_psp_230_cc_epvehdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 164){ac_psp_230_cc_epvehsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 165){ac_psp_230_cc_epvehcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 168){ac_psp_230_cb_epvehdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 170){ac_psp_230_cb_epvehsb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 171){ac_psp_230_cb_epvehbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 172){ac_psp_230_cdx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 173){ac_psp_230_cdx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 176){ac_psp_230_cdx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 178){ac_psp_230_cdx_epvehsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 179){ac_psp_230_cdx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 181){ac_psp_230_cdx_epvehcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 182){ac_psp_230_cdx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 183){ac_psp_230_cdx_epvehbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 185){ac_psp_230_cdx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 186){ac_psp_230_cux_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 188){ac_psp_230_cux_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 190){ac_psp_230_cux_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 192){ac_psp_230_csx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 193){ac_psp_230_csx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 194){ac_psp_230_csx_epvehdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 196){ac_psp_230_csx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 199){ac_psp_230_csx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 201){ac_psp_230_csx_epvehcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 202){ac_psp_230_csx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 204){ac_psp_230_csx_epvehbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 205){ac_psp_230_csx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 206){ac_psp_230_ccx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 207){ac_psp_230_ccx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 208){ac_psp_230_ccx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 209){ac_psp_230_ccx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 210){ac_psp_230_ccx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 211){ac_psp_230_ccx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 212){ac_psp_230_cbx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 213){ac_psp_230_cbx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 215){ac_psp_230_cbx_epvehdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 216){ac_psp_230_cbx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 219){ac_psp_230_cbx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 220){ac_psp_230_cbx_epvehsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 221){ac_psp_230_cbx_epvehcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 222){ac_psp_230_cbx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 225){ac_psp_230_cbx_epvehbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 233){ac_psp_230_bdx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 234){ac_psp_230_bdx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 239){ac_psp_230_bsx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 240){ac_psp_230_bsx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 241){ac_psp_230_bbx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 242){ac_psp_230_bbx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 243){ac_psp_230_bbx_epvehsux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 244){ac_psp_230_bbx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 245){ac_psp_230_bbx_epvehbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 246){ac_psp_230_bbx_epvehbcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 247){ac_psp_230_dxdx_epvehdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 248){ac_psp_230_dxdx_epvehdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 253){ac_psp_230_dxux_epvehuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 254){ac_psp_230_dxux_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 256){ac_psp_230_dxsx_epvehdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 257){ac_psp_230_dxsx_epvehdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 258){ac_psp_230_dxsx_epvehuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 260){ac_psp_230_dxsx_epvehsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 263){ac_psp_230_dxcx_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 264){ac_psp_230_dxcx_epvehcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 265){ac_psp_230_dxbx_epvehdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 266){ac_psp_230_dxbx_epvehdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 268){ac_psp_230_dxbx_epvehuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 270){ac_psp_230_dxbx_epvehcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 273){ac_psp_230_uxsx_epvehuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 274){ac_psp_230_uxsx_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 276){ac_psp_230_uxbx_epvehuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 277){ac_psp_230_uxbx_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 281){ac_psp_230_sxsx_epvehuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 283){ac_psp_230_sxsx_epvehsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 286){ac_psp_230_sxcx_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 287){ac_psp_230_sxcx_epvehcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 290){ac_psp_230_sxbx_epvehuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 291){ac_psp_230_sxbx_epvehuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 292){ac_psp_230_sxbx_epvehsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 293){ac_psp_230_sxbx_epvehcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 297){ac_psp_230_cxbx_epvehuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 298){ac_psp_230_cxbx_epvehcxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 302){ac_psp_230_bxbx_epvehuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 304){ac_psp_230_bxbx_epvehcxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppexneh03_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ag_psp_230_gg_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   2){ag_psp_230_gg_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   3){ag_psp_230_gg_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   4){ag_psp_230_gg_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   5){ag_psp_230_gg_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   6){ag_psp_230_gg_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   7){ag_psp_230_gu_epvehgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   8){ag_psp_230_gu_epvehgs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   9){ag_psp_230_gu_epvehgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  10){ag_psp_230_gc_epvehgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  11){ag_psp_230_gc_epvehgs(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  12){ag_psp_230_gc_epvehgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  13){ag_psp_230_gdx_epvehgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  14){ag_psp_230_gdx_epvehgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  15){ag_psp_230_gsx_epvehgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  16){ag_psp_230_gsx_epvehgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  17){ag_psp_230_gbx_epvehgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  18){ag_psp_230_gbx_epvehgcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  25){ag_psp_230_du_epvehdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  26){ag_psp_230_du_epvehds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  27){ag_psp_230_du_epvehdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  31){ag_psp_230_dc_epvehdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  32){ag_psp_230_dc_epvehds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  33){ag_psp_230_dc_epvehdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  37){ag_psp_230_ddx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  38){ag_psp_230_ddx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  39){ag_psp_230_ddx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  40){ag_psp_230_ddx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  41){ag_psp_230_ddx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  42){ag_psp_230_ddx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  43){ag_psp_230_dsx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  44){ag_psp_230_dsx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  49){ag_psp_230_dbx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  50){ag_psp_230_dbx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  55){ag_psp_230_uu_epvehdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  57){ag_psp_230_uu_epvehus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  58){ag_psp_230_uu_epvehub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  62){ag_psp_230_us_epvehds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  64){ag_psp_230_us_epvehss(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  65){ag_psp_230_us_epvehsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  67){ag_psp_230_uc_epvehdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  68){ag_psp_230_uc_epvehdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  69){ag_psp_230_uc_epvehus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  70){ag_psp_230_uc_epvehub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  71){ag_psp_230_uc_epvehsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  72){ag_psp_230_uc_epvehcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  75){ag_psp_230_ub_epvehdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  77){ag_psp_230_ub_epvehsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  78){ag_psp_230_ub_epvehbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  79){ag_psp_230_udx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  80){ag_psp_230_udx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  83){ag_psp_230_udx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  84){ag_psp_230_udx_epvehucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  85){ag_psp_230_udx_epvehsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  86){ag_psp_230_udx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  89){ag_psp_230_udx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  90){ag_psp_230_udx_epvehbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  92){ag_psp_230_udx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  93){ag_psp_230_uux_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  94){ag_psp_230_uux_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  95){ag_psp_230_uux_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  96){ag_psp_230_uux_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  97){ag_psp_230_uux_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  98){ag_psp_230_uux_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  99){ag_psp_230_usx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 100){ag_psp_230_usx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 101){ag_psp_230_usx_epvehdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 103){ag_psp_230_usx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 104){ag_psp_230_usx_epvehucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 106){ag_psp_230_usx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 109){ag_psp_230_usx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 111){ag_psp_230_usx_epvehbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 112){ag_psp_230_usx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 114){ag_psp_230_ucx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 116){ag_psp_230_ucx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 118){ag_psp_230_ucx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 119){ag_psp_230_ubx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 120){ag_psp_230_ubx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 122){ag_psp_230_ubx_epvehdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 123){ag_psp_230_ubx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 124){ag_psp_230_ubx_epvehucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 126){ag_psp_230_ubx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 127){ag_psp_230_ubx_epvehsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 129){ag_psp_230_ubx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 132){ag_psp_230_ubx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 137){ag_psp_230_sc_epvehds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 139){ag_psp_230_sc_epvehss(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 140){ag_psp_230_sc_epvehsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 144){ag_psp_230_sdx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 145){ag_psp_230_sdx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 148){ag_psp_230_ssx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 149){ag_psp_230_ssx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 150){ag_psp_230_ssx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 151){ag_psp_230_ssx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 152){ag_psp_230_ssx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 153){ag_psp_230_ssx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 156){ag_psp_230_sbx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 157){ag_psp_230_sbx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 161){ag_psp_230_cc_epvehdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 164){ag_psp_230_cc_epvehsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 165){ag_psp_230_cc_epvehcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 168){ag_psp_230_cb_epvehdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 170){ag_psp_230_cb_epvehsb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 171){ag_psp_230_cb_epvehbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 172){ag_psp_230_cdx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 173){ag_psp_230_cdx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 176){ag_psp_230_cdx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 178){ag_psp_230_cdx_epvehsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 179){ag_psp_230_cdx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 181){ag_psp_230_cdx_epvehcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 182){ag_psp_230_cdx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 183){ag_psp_230_cdx_epvehbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 185){ag_psp_230_cdx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 186){ag_psp_230_cux_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 188){ag_psp_230_cux_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 190){ag_psp_230_cux_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 192){ag_psp_230_csx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 193){ag_psp_230_csx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 194){ag_psp_230_csx_epvehdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 196){ag_psp_230_csx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 199){ag_psp_230_csx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 201){ag_psp_230_csx_epvehcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 202){ag_psp_230_csx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 204){ag_psp_230_csx_epvehbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 205){ag_psp_230_csx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 206){ag_psp_230_ccx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 207){ag_psp_230_ccx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 208){ag_psp_230_ccx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 209){ag_psp_230_ccx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 210){ag_psp_230_ccx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 211){ag_psp_230_ccx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 212){ag_psp_230_cbx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 213){ag_psp_230_cbx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 215){ag_psp_230_cbx_epvehdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 216){ag_psp_230_cbx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 219){ag_psp_230_cbx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 220){ag_psp_230_cbx_epvehsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 221){ag_psp_230_cbx_epvehcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 222){ag_psp_230_cbx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 225){ag_psp_230_cbx_epvehbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 233){ag_psp_230_bdx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 234){ag_psp_230_bdx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 239){ag_psp_230_bsx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 240){ag_psp_230_bsx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 241){ag_psp_230_bbx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 242){ag_psp_230_bbx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 243){ag_psp_230_bbx_epvehsux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 244){ag_psp_230_bbx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 245){ag_psp_230_bbx_epvehbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 246){ag_psp_230_bbx_epvehbcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 247){ag_psp_230_dxdx_epvehdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 248){ag_psp_230_dxdx_epvehdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 253){ag_psp_230_dxux_epvehuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 254){ag_psp_230_dxux_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 256){ag_psp_230_dxsx_epvehdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 257){ag_psp_230_dxsx_epvehdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 258){ag_psp_230_dxsx_epvehuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 260){ag_psp_230_dxsx_epvehsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 263){ag_psp_230_dxcx_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 264){ag_psp_230_dxcx_epvehcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 265){ag_psp_230_dxbx_epvehdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 266){ag_psp_230_dxbx_epvehdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 268){ag_psp_230_dxbx_epvehuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 270){ag_psp_230_dxbx_epvehcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 273){ag_psp_230_uxsx_epvehuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 274){ag_psp_230_uxsx_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 276){ag_psp_230_uxbx_epvehuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 277){ag_psp_230_uxbx_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 281){ag_psp_230_sxsx_epvehuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 283){ag_psp_230_sxsx_epvehsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 286){ag_psp_230_sxcx_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 287){ag_psp_230_sxcx_epvehcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 290){ag_psp_230_sxbx_epvehuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 291){ag_psp_230_sxbx_epvehuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 292){ag_psp_230_sxbx_epvehsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 293){ag_psp_230_sxbx_epvehcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 297){ag_psp_230_cxbx_epvehuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 298){ag_psp_230_cxbx_epvehcxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 302){ag_psp_230_bxbx_epvehuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 304){ag_psp_230_bxbx_epvehcxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
