#include "header.hpp"

#include "ppexneh03.summary.hpp"

ppexneh03_summary_generic::ppexneh03_summary_generic(munich * xmunich){
  Logger logger("ppexneh03_summary_generic::ppexneh03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexneh03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexneh03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexneh03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexneh03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexneh03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexneh03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_born(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epveh";
    subprocess[2] = "us~_epveh";
    subprocess[3] = "ub~_epveh";
    subprocess[4] = "cd~_epveh";
    subprocess[5] = "cs~_epveh";
    subprocess[6] = "cb~_epveh";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gu_epvehs";
    subprocess[3] = "gu_epvehb";
    subprocess[4] = "gc_epvehd";
    subprocess[5] = "gc_epvehs";
    subprocess[6] = "gc_epvehb";
    subprocess[7] = "gd~_epvehu~";
    subprocess[8] = "gd~_epvehc~";
    subprocess[9] = "gs~_epvehu~";
    subprocess[10] = "gs~_epvehc~";
    subprocess[11] = "gb~_epvehu~";
    subprocess[12] = "gb~_epvehc~";
    subprocess[13] = "ud~_epvehg";
    subprocess[14] = "us~_epvehg";
    subprocess[15] = "ub~_epvehg";
    subprocess[16] = "cd~_epvehg";
    subprocess[17] = "cs~_epvehg";
    subprocess[18] = "cb~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "ua_epvehs";
    subprocess[3] = "ua_epvehb";
    subprocess[4] = "ca_epvehd";
    subprocess[5] = "ca_epvehs";
    subprocess[6] = "ca_epvehb";
    subprocess[7] = "d~a_epvehu~";
    subprocess[8] = "d~a_epvehc~";
    subprocess[9] = "s~a_epvehu~";
    subprocess[10] = "s~a_epvehc~";
    subprocess[11] = "b~a_epvehu~";
    subprocess[12] = "b~a_epvehc~";
    subprocess[13] = "ud~_epveha";
    subprocess[14] = "us~_epveha";
    subprocess[15] = "ub~_epveha";
    subprocess[16] = "cd~_epveha";
    subprocess[17] = "cs~_epveha";
    subprocess[18] = "cb~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gu_epvehs";
    subprocess[3] = "gu_epvehb";
    subprocess[4] = "gc_epvehd";
    subprocess[5] = "gc_epvehs";
    subprocess[6] = "gc_epvehb";
    subprocess[7] = "gd~_epvehu~";
    subprocess[8] = "gd~_epvehc~";
    subprocess[9] = "gs~_epvehu~";
    subprocess[10] = "gs~_epvehc~";
    subprocess[11] = "gb~_epvehu~";
    subprocess[12] = "gb~_epvehc~";
    subprocess[13] = "ud~_epvehg";
    subprocess[14] = "us~_epvehg";
    subprocess[15] = "ub~_epvehg";
    subprocess[16] = "cd~_epvehg";
    subprocess[17] = "cs~_epvehg";
    subprocess[18] = "cb~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "ua_epvehs";
    subprocess[3] = "ua_epvehb";
    subprocess[4] = "ca_epvehd";
    subprocess[5] = "ca_epvehs";
    subprocess[6] = "ca_epvehb";
    subprocess[7] = "d~a_epvehu~";
    subprocess[8] = "d~a_epvehc~";
    subprocess[9] = "s~a_epvehu~";
    subprocess[10] = "s~a_epvehc~";
    subprocess[11] = "b~a_epvehu~";
    subprocess[12] = "b~a_epvehc~";
    subprocess[13] = "ud~_epveha";
    subprocess[14] = "us~_epveha";
    subprocess[15] = "ub~_epveha";
    subprocess[16] = "cd~_epveha";
    subprocess[17] = "cs~_epveha";
    subprocess[18] = "cb~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gu_epvehs";
    subprocess[3] = "gu_epvehb";
    subprocess[4] = "gc_epvehd";
    subprocess[5] = "gc_epvehs";
    subprocess[6] = "gc_epvehb";
    subprocess[7] = "gd~_epvehu~";
    subprocess[8] = "gd~_epvehc~";
    subprocess[9] = "gs~_epvehu~";
    subprocess[10] = "gs~_epvehc~";
    subprocess[11] = "gb~_epvehu~";
    subprocess[12] = "gb~_epvehc~";
    subprocess[13] = "ud~_epvehg";
    subprocess[14] = "us~_epvehg";
    subprocess[15] = "ub~_epvehg";
    subprocess[16] = "cd~_epvehg";
    subprocess[17] = "cs~_epvehg";
    subprocess[18] = "cb~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "ua_epvehs";
    subprocess[3] = "ua_epvehb";
    subprocess[4] = "ca_epvehd";
    subprocess[5] = "ca_epvehs";
    subprocess[6] = "ca_epvehb";
    subprocess[7] = "d~a_epvehu~";
    subprocess[8] = "d~a_epvehc~";
    subprocess[9] = "s~a_epvehu~";
    subprocess[10] = "s~a_epvehc~";
    subprocess[11] = "b~a_epvehu~";
    subprocess[12] = "b~a_epvehc~";
    subprocess[13] = "ud~_epveha";
    subprocess[14] = "us~_epveha";
    subprocess[15] = "ub~_epveha";
    subprocess[16] = "cd~_epveha";
    subprocess[17] = "cs~_epveha";
    subprocess[18] = "cb~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexneh03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_epvehdu~";
    subprocess[2] = "gg_epvehdc~";
    subprocess[3] = "gg_epvehsu~";
    subprocess[4] = "gg_epvehsc~";
    subprocess[5] = "gg_epvehbu~";
    subprocess[6] = "gg_epvehbc~";
    subprocess[7] = "gu_epvehgd";
    subprocess[8] = "gu_epvehgs";
    subprocess[9] = "gu_epvehgb";
    subprocess[10] = "gc_epvehgd";
    subprocess[11] = "gc_epvehgs";
    subprocess[12] = "gc_epvehgb";
    subprocess[13] = "gd~_epvehgu~";
    subprocess[14] = "gd~_epvehgc~";
    subprocess[15] = "gs~_epvehgu~";
    subprocess[16] = "gs~_epvehgc~";
    subprocess[17] = "gb~_epvehgu~";
    subprocess[18] = "gb~_epvehgc~";
    subprocess[19] = "du_epvehdd";
    subprocess[20] = "du_epvehds";
    subprocess[21] = "du_epvehdb";
    subprocess[22] = "dc_epvehdd";
    subprocess[23] = "dc_epvehds";
    subprocess[24] = "dc_epvehdb";
    subprocess[25] = "dd~_epvehdu~";
    subprocess[26] = "dd~_epvehdc~";
    subprocess[27] = "dd~_epvehsu~";
    subprocess[28] = "dd~_epvehsc~";
    subprocess[29] = "dd~_epvehbu~";
    subprocess[30] = "dd~_epvehbc~";
    subprocess[31] = "ds~_epvehdu~";
    subprocess[32] = "ds~_epvehdc~";
    subprocess[33] = "db~_epvehdu~";
    subprocess[34] = "db~_epvehdc~";
    subprocess[35] = "uu_epvehdu";
    subprocess[36] = "uu_epvehus";
    subprocess[37] = "uu_epvehub";
    subprocess[38] = "us_epvehds";
    subprocess[39] = "us_epvehss";
    subprocess[40] = "us_epvehsb";
    subprocess[41] = "uc_epvehdu";
    subprocess[42] = "uc_epvehdc";
    subprocess[43] = "uc_epvehus";
    subprocess[44] = "uc_epvehub";
    subprocess[45] = "uc_epvehsc";
    subprocess[46] = "uc_epvehcb";
    subprocess[47] = "ub_epvehdb";
    subprocess[48] = "ub_epvehsb";
    subprocess[49] = "ub_epvehbb";
    subprocess[50] = "ud~_epvehgg";
    subprocess[51] = "ud~_epvehdd~";
    subprocess[52] = "ud~_epvehuu~";
    subprocess[53] = "ud~_epvehuc~";
    subprocess[54] = "ud~_epvehsd~";
    subprocess[55] = "ud~_epvehss~";
    subprocess[56] = "ud~_epvehcc~";
    subprocess[57] = "ud~_epvehbd~";
    subprocess[58] = "ud~_epvehbb~";
    subprocess[59] = "uu~_epvehdu~";
    subprocess[60] = "uu~_epvehdc~";
    subprocess[61] = "uu~_epvehsu~";
    subprocess[62] = "uu~_epvehsc~";
    subprocess[63] = "uu~_epvehbu~";
    subprocess[64] = "uu~_epvehbc~";
    subprocess[65] = "us~_epvehgg";
    subprocess[66] = "us~_epvehdd~";
    subprocess[67] = "us~_epvehds~";
    subprocess[68] = "us~_epvehuu~";
    subprocess[69] = "us~_epvehuc~";
    subprocess[70] = "us~_epvehss~";
    subprocess[71] = "us~_epvehcc~";
    subprocess[72] = "us~_epvehbs~";
    subprocess[73] = "us~_epvehbb~";
    subprocess[74] = "uc~_epvehdc~";
    subprocess[75] = "uc~_epvehsc~";
    subprocess[76] = "uc~_epvehbc~";
    subprocess[77] = "ub~_epvehgg";
    subprocess[78] = "ub~_epvehdd~";
    subprocess[79] = "ub~_epvehdb~";
    subprocess[80] = "ub~_epvehuu~";
    subprocess[81] = "ub~_epvehuc~";
    subprocess[82] = "ub~_epvehss~";
    subprocess[83] = "ub~_epvehsb~";
    subprocess[84] = "ub~_epvehcc~";
    subprocess[85] = "ub~_epvehbb~";
    subprocess[86] = "sc_epvehds";
    subprocess[87] = "sc_epvehss";
    subprocess[88] = "sc_epvehsb";
    subprocess[89] = "sd~_epvehsu~";
    subprocess[90] = "sd~_epvehsc~";
    subprocess[91] = "ss~_epvehdu~";
    subprocess[92] = "ss~_epvehdc~";
    subprocess[93] = "ss~_epvehsu~";
    subprocess[94] = "ss~_epvehsc~";
    subprocess[95] = "ss~_epvehbu~";
    subprocess[96] = "ss~_epvehbc~";
    subprocess[97] = "sb~_epvehsu~";
    subprocess[98] = "sb~_epvehsc~";
    subprocess[99] = "cc_epvehdc";
    subprocess[100] = "cc_epvehsc";
    subprocess[101] = "cc_epvehcb";
    subprocess[102] = "cb_epvehdb";
    subprocess[103] = "cb_epvehsb";
    subprocess[104] = "cb_epvehbb";
    subprocess[105] = "cd~_epvehgg";
    subprocess[106] = "cd~_epvehdd~";
    subprocess[107] = "cd~_epvehuu~";
    subprocess[108] = "cd~_epvehsd~";
    subprocess[109] = "cd~_epvehss~";
    subprocess[110] = "cd~_epvehcu~";
    subprocess[111] = "cd~_epvehcc~";
    subprocess[112] = "cd~_epvehbd~";
    subprocess[113] = "cd~_epvehbb~";
    subprocess[114] = "cu~_epvehdu~";
    subprocess[115] = "cu~_epvehsu~";
    subprocess[116] = "cu~_epvehbu~";
    subprocess[117] = "cs~_epvehgg";
    subprocess[118] = "cs~_epvehdd~";
    subprocess[119] = "cs~_epvehds~";
    subprocess[120] = "cs~_epvehuu~";
    subprocess[121] = "cs~_epvehss~";
    subprocess[122] = "cs~_epvehcu~";
    subprocess[123] = "cs~_epvehcc~";
    subprocess[124] = "cs~_epvehbs~";
    subprocess[125] = "cs~_epvehbb~";
    subprocess[126] = "cc~_epvehdu~";
    subprocess[127] = "cc~_epvehdc~";
    subprocess[128] = "cc~_epvehsu~";
    subprocess[129] = "cc~_epvehsc~";
    subprocess[130] = "cc~_epvehbu~";
    subprocess[131] = "cc~_epvehbc~";
    subprocess[132] = "cb~_epvehgg";
    subprocess[133] = "cb~_epvehdd~";
    subprocess[134] = "cb~_epvehdb~";
    subprocess[135] = "cb~_epvehuu~";
    subprocess[136] = "cb~_epvehss~";
    subprocess[137] = "cb~_epvehsb~";
    subprocess[138] = "cb~_epvehcu~";
    subprocess[139] = "cb~_epvehcc~";
    subprocess[140] = "cb~_epvehbb~";
    subprocess[141] = "bd~_epvehbu~";
    subprocess[142] = "bd~_epvehbc~";
    subprocess[143] = "bs~_epvehbu~";
    subprocess[144] = "bs~_epvehbc~";
    subprocess[145] = "bb~_epvehdu~";
    subprocess[146] = "bb~_epvehdc~";
    subprocess[147] = "bb~_epvehsu~";
    subprocess[148] = "bb~_epvehsc~";
    subprocess[149] = "bb~_epvehbu~";
    subprocess[150] = "bb~_epvehbc~";
    subprocess[151] = "d~d~_epvehd~u~";
    subprocess[152] = "d~d~_epvehd~c~";
    subprocess[153] = "d~u~_epvehu~u~";
    subprocess[154] = "d~u~_epvehu~c~";
    subprocess[155] = "d~s~_epvehd~u~";
    subprocess[156] = "d~s~_epvehd~c~";
    subprocess[157] = "d~s~_epvehu~s~";
    subprocess[158] = "d~s~_epvehs~c~";
    subprocess[159] = "d~c~_epvehu~c~";
    subprocess[160] = "d~c~_epvehc~c~";
    subprocess[161] = "d~b~_epvehd~u~";
    subprocess[162] = "d~b~_epvehd~c~";
    subprocess[163] = "d~b~_epvehu~b~";
    subprocess[164] = "d~b~_epvehc~b~";
    subprocess[165] = "u~s~_epvehu~u~";
    subprocess[166] = "u~s~_epvehu~c~";
    subprocess[167] = "u~b~_epvehu~u~";
    subprocess[168] = "u~b~_epvehu~c~";
    subprocess[169] = "s~s~_epvehu~s~";
    subprocess[170] = "s~s~_epvehs~c~";
    subprocess[171] = "s~c~_epvehu~c~";
    subprocess[172] = "s~c~_epvehc~c~";
    subprocess[173] = "s~b~_epvehu~s~";
    subprocess[174] = "s~b~_epvehu~b~";
    subprocess[175] = "s~b~_epvehs~c~";
    subprocess[176] = "s~b~_epvehc~b~";
    subprocess[177] = "c~b~_epvehu~c~";
    subprocess[178] = "c~b~_epvehc~c~";
    subprocess[179] = "b~b~_epvehu~b~";
    subprocess[180] = "b~b~_epvehc~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
