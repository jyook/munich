#include "header.hpp"

#include "ppllh03.amplitude.set.hpp"

#include "ppexneh03.contribution.set.hpp"
#include "ppexneh03.event.set.hpp"
#include "ppexneh03.phasespace.set.hpp"
#include "ppexneh03.observable.set.hpp"
#include "ppexneh03.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-epveh+X");

  MUC->csi = new ppexneh03_contribution_set();
  MUC->esi = new ppexneh03_event_set();
  MUC->psi = new ppexneh03_phasespace_set();
  MUC->osi = new ppexneh03_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllh03_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppexneh03_phasespace_set();
      MUC->psi->fake_psi->csi = new ppexneh03_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppexneh03_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
