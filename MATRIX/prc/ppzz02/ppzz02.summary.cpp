#include "header.hpp"

#include "ppzz02.summary.hpp"

ppzz02_summary_generic::ppzz02_summary_generic(munich * xmunich){
  Logger logger("ppzz02_summary_generic::ppzz02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppzz02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppzz02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppzz02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppzz02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppzz02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppzz02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_born(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_zz";
    subprocess[2] = "uu~_zz";
    subprocess[3] = "bb~_zz";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_zz";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zzd";
    subprocess[2] = "gu_zzu";
    subprocess[3] = "gb_zzb";
    subprocess[4] = "gd~_zzd~";
    subprocess[5] = "gu~_zzu~";
    subprocess[6] = "gb~_zzb~";
    subprocess[7] = "dd~_zzg";
    subprocess[8] = "uu~_zzg";
    subprocess[9] = "bb~_zzg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_zzg";
    subprocess[2] = "gd_zzd";
    subprocess[3] = "gu_zzu";
    subprocess[4] = "gb_zzb";
    subprocess[5] = "gd~_zzd~";
    subprocess[6] = "gu~_zzu~";
    subprocess[7] = "gb~_zzb~";
    subprocess[8] = "dd~_zzg";
    subprocess[9] = "uu~_zzg";
    subprocess[10] = "bb~_zzg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zzd";
    subprocess[2] = "ua_zzu";
    subprocess[3] = "ba_zzb";
    subprocess[4] = "d~a_zzd~";
    subprocess[5] = "u~a_zzu~";
    subprocess[6] = "b~a_zzb~";
    subprocess[7] = "dd~_zza";
    subprocess[8] = "uu~_zza";
    subprocess[9] = "bb~_zza";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zzd";
    subprocess[2] = "gu_zzu";
    subprocess[3] = "gb_zzb";
    subprocess[4] = "gd~_zzd~";
    subprocess[5] = "gu~_zzu~";
    subprocess[6] = "gb~_zzb~";
    subprocess[7] = "dd~_zzg";
    subprocess[8] = "uu~_zzg";
    subprocess[9] = "bb~_zzg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zzd";
    subprocess[2] = "ua_zzu";
    subprocess[3] = "ba_zzb";
    subprocess[4] = "d~a_zzd~";
    subprocess[5] = "u~a_zzu~";
    subprocess[6] = "b~a_zzb~";
    subprocess[7] = "dd~_zza";
    subprocess[8] = "uu~_zza";
    subprocess[9] = "bb~_zza";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zzd";
    subprocess[2] = "gu_zzu";
    subprocess[3] = "gb_zzb";
    subprocess[4] = "gd~_zzd~";
    subprocess[5] = "gu~_zzu~";
    subprocess[6] = "gb~_zzb~";
    subprocess[7] = "dd~_zzg";
    subprocess[8] = "uu~_zzg";
    subprocess[9] = "bb~_zzg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zzd";
    subprocess[2] = "ua_zzu";
    subprocess[3] = "ba_zzb";
    subprocess[4] = "d~a_zzd~";
    subprocess[5] = "u~a_zzu~";
    subprocess[6] = "b~a_zzb~";
    subprocess[7] = "dd~_zza";
    subprocess[8] = "uu~_zza";
    subprocess[9] = "bb~_zza";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppzz02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_zzdd~";
    subprocess[2] = "gg_zzuu~";
    subprocess[3] = "gg_zzbb~";
    subprocess[4] = "gd_zzgd";
    subprocess[5] = "gu_zzgu";
    subprocess[6] = "gb_zzgb";
    subprocess[7] = "gd~_zzgd~";
    subprocess[8] = "gu~_zzgu~";
    subprocess[9] = "gb~_zzgb~";
    subprocess[10] = "dd_zzdd";
    subprocess[11] = "du_zzdu";
    subprocess[12] = "ds_zzds";
    subprocess[13] = "dc_zzdc";
    subprocess[14] = "db_zzdb";
    subprocess[15] = "dd~_zzgg";
    subprocess[16] = "dd~_zzdd~";
    subprocess[17] = "dd~_zzuu~";
    subprocess[18] = "dd~_zzss~";
    subprocess[19] = "dd~_zzcc~";
    subprocess[20] = "dd~_zzbb~";
    subprocess[21] = "du~_zzdu~";
    subprocess[22] = "ds~_zzds~";
    subprocess[23] = "dc~_zzdc~";
    subprocess[24] = "db~_zzdb~";
    subprocess[25] = "uu_zzuu";
    subprocess[26] = "uc_zzuc";
    subprocess[27] = "ub_zzub";
    subprocess[28] = "ud~_zzud~";
    subprocess[29] = "uu~_zzgg";
    subprocess[30] = "uu~_zzdd~";
    subprocess[31] = "uu~_zzuu~";
    subprocess[32] = "uu~_zzss~";
    subprocess[33] = "uu~_zzcc~";
    subprocess[34] = "uu~_zzbb~";
    subprocess[35] = "us~_zzus~";
    subprocess[36] = "uc~_zzuc~";
    subprocess[37] = "ub~_zzub~";
    subprocess[38] = "bb_zzbb";
    subprocess[39] = "bd~_zzbd~";
    subprocess[40] = "bu~_zzbu~";
    subprocess[41] = "bb~_zzgg";
    subprocess[42] = "bb~_zzdd~";
    subprocess[43] = "bb~_zzuu~";
    subprocess[44] = "bb~_zzbb~";
    subprocess[45] = "d~d~_zzd~d~";
    subprocess[46] = "d~u~_zzd~u~";
    subprocess[47] = "d~s~_zzd~s~";
    subprocess[48] = "d~c~_zzd~c~";
    subprocess[49] = "d~b~_zzd~b~";
    subprocess[50] = "u~u~_zzu~u~";
    subprocess[51] = "u~c~_zzu~c~";
    subprocess[52] = "u~b~_zzu~b~";
    subprocess[53] = "b~b~_zzb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
