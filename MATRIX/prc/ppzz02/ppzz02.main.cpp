#include "header.hpp"

#include "ppzz02.amplitude.set.hpp"

#include "ppzz02.contribution.set.hpp"
#include "ppzz02.event.set.hpp"
#include "ppzz02.phasespace.set.hpp"
#include "ppzz02.observable.set.hpp"
#include "ppzz02.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-zz+X");

  MUC->csi = new ppzz02_contribution_set();
  MUC->esi = new ppzz02_event_set();
  MUC->psi = new ppzz02_phasespace_set();
  MUC->osi = new ppzz02_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppzz02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppzz02_phasespace_set();
      MUC->psi->fake_psi->csi = new ppzz02_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppzz02_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
