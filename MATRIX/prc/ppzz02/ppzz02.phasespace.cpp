#include "header.hpp"

#include "ppzz02.phasespace.set.hpp"

ppzz02_phasespace_set::~ppzz02_phasespace_set(){
  static Logger logger("ppzz02_phasespace_set::~ppzz02_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::optimize_minv_born(){
  static Logger logger("ppzz02_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppzz02_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppzz02_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 7;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppzz02_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-25);
      tau_MC_map.push_back(-36);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppzz02_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_020_ddx_zz(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_020_uux_zz(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_020_bbx_zz(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_220_gg_zz(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppzz02_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_020_ddx_zz(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_020_uux_zz(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_020_bbx_zz(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_220_gg_zz(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppzz02_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_020_ddx_zz(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_020_uux_zz(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_020_bbx_zz(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_220_gg_zz(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::optimize_minv_real(){
  static Logger logger("ppzz02_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppzz02_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppzz02_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 54;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 11;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 11;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 11;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppzz02_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 13){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 15){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 17){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppzz02_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_120_gd_zzd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_120_gu_zzu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_120_gb_zzb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_120_gdx_zzdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_120_gux_zzux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_120_gbx_zzbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_120_ddx_zzg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_120_uux_zzg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_120_bbx_zzg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_030_da_zzd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_030_ua_zzu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_030_ba_zzb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_030_dxa_zzdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_030_uxa_zzux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_030_bxa_zzbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_030_ddx_zza(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_030_uux_zza(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_030_bbx_zza(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_320_gg_zzg(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_320_gd_zzd(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_320_gu_zzu(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_320_gb_zzb(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_320_gdx_zzdx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_320_gux_zzux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_320_gbx_zzbx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_320_ddx_zzg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_320_uux_zzg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_320_bbx_zzg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppzz02_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_120_gd_zzd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_120_gu_zzu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_120_gb_zzb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_120_gdx_zzdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_120_gux_zzux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_120_gbx_zzbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_120_ddx_zzg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_120_uux_zzg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_120_bbx_zzg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_030_da_zzd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_030_ua_zzu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_030_ba_zzb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_030_dxa_zzdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_030_uxa_zzux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_030_bxa_zzbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_030_ddx_zza(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_030_uux_zza(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_030_bbx_zza(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_320_gg_zzg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_320_gd_zzd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_320_gu_zzu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_320_gb_zzb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_320_gdx_zzdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_320_gux_zzux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_320_gbx_zzbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_320_ddx_zzg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_320_uux_zzg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_320_bbx_zzg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppzz02_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_120_gd_zzd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_120_gu_zzu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_120_gb_zzb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_120_gdx_zzdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_120_gux_zzux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_120_gbx_zzbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_120_ddx_zzg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_120_uux_zzg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_120_bbx_zzg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_030_da_zzd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_030_ua_zzu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_030_ba_zzb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_030_dxa_zzdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_030_uxa_zzux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_030_bxa_zzbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_030_ddx_zza(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_030_uux_zza(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_030_bbx_zza(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_320_gg_zzg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_320_gd_zzd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_320_gu_zzu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_320_gb_zzb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_320_gdx_zzdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_320_gux_zzux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_320_gbx_zzbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_320_ddx_zzg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_320_uux_zzg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_320_bbx_zzg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppzz02_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppzz02_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppzz02_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 86;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 40;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppzz02_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppzz02_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_220_gg_zzddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_220_gg_zzuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_220_gg_zzbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_220_gd_zzgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_220_gu_zzgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_220_gb_zzgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_220_gdx_zzgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_220_gux_zzgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_220_gbx_zzgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_220_dd_zzdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_220_du_zzdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_220_ds_zzds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_220_dc_zzdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_220_db_zzdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_220_ddx_zzgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_220_ddx_zzddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_220_ddx_zzuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_220_ddx_zzssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_220_ddx_zzccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_220_ddx_zzbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_220_dux_zzdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_220_dsx_zzdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_220_dcx_zzdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_220_dbx_zzdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_220_uu_zzuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_220_uc_zzuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_220_ub_zzub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_220_udx_zzudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_220_uux_zzgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_220_uux_zzddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_220_uux_zzuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_220_uux_zzssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_220_uux_zzccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_220_uux_zzbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_220_usx_zzusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_220_ucx_zzucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_220_ubx_zzubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_220_bb_zzbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_220_bdx_zzbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_220_bux_zzbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_220_bbx_zzgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_220_bbx_zzddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_220_bbx_zzuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_220_bbx_zzbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_220_dxdx_zzdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_220_dxux_zzdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_220_dxsx_zzdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_220_dxcx_zzdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_220_dxbx_zzdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_220_uxux_zzuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_220_uxcx_zzuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_220_uxbx_zzuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_220_bxbx_zzbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppzz02_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_220_gg_zzddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_220_gg_zzuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_220_gg_zzbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_220_gd_zzgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_220_gu_zzgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_220_gb_zzgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_220_gdx_zzgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_220_gux_zzgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_220_gbx_zzgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_220_dd_zzdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_220_du_zzdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_220_ds_zzds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_220_dc_zzdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_220_db_zzdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_220_ddx_zzgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_220_ddx_zzddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_220_ddx_zzuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_220_ddx_zzssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_220_ddx_zzccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_220_ddx_zzbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_220_dux_zzdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_220_dsx_zzdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_220_dcx_zzdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_220_dbx_zzdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_220_uu_zzuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_220_uc_zzuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_220_ub_zzub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_220_udx_zzudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_220_uux_zzgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_220_uux_zzddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_220_uux_zzuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_220_uux_zzssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_220_uux_zzccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_220_uux_zzbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_220_usx_zzusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_220_ucx_zzucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_220_ubx_zzubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_220_bb_zzbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_220_bdx_zzbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_220_bux_zzbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_220_bbx_zzgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_220_bbx_zzddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_220_bbx_zzuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_220_bbx_zzbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_220_dxdx_zzdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_220_dxux_zzdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_220_dxsx_zzdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_220_dxcx_zzdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_220_dxbx_zzdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_220_uxux_zzuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_220_uxcx_zzuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_220_uxbx_zzuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_220_bxbx_zzbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppzz02_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppzz02_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_220_gg_zzddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_220_gg_zzuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_220_gg_zzbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_220_gd_zzgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_220_gu_zzgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_220_gb_zzgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_220_gdx_zzgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_220_gux_zzgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_220_gbx_zzgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_220_dd_zzdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_220_du_zzdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_220_ds_zzds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_220_dc_zzdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_220_db_zzdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_220_ddx_zzgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_220_ddx_zzddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_220_ddx_zzuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_220_ddx_zzssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_220_ddx_zzccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_220_ddx_zzbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_220_dux_zzdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_220_dsx_zzdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_220_dcx_zzdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_220_dbx_zzdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_220_uu_zzuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_220_uc_zzuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_220_ub_zzub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_220_udx_zzudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_220_uux_zzgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_220_uux_zzddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_220_uux_zzuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_220_uux_zzssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_220_uux_zzccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_220_uux_zzbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_220_usx_zzusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_220_ucx_zzucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_220_ubx_zzubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_220_bb_zzbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_220_bdx_zzbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_220_bux_zzbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_220_bbx_zzgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_220_bbx_zzddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_220_bbx_zzuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_220_bbx_zzbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_220_dxdx_zzdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_220_dxux_zzdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_220_dxsx_zzdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_220_dxcx_zzdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_220_dxbx_zzdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_220_uxux_zzuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_220_uxcx_zzuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_220_uxbx_zzuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_220_bxbx_zzbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
