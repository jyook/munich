#include "header.hpp"

#include "ppexne02.summary.hpp"

ppexne02_summary_generic::ppexne02_summary_generic(munich * xmunich){
  Logger logger("ppexne02_summary_generic::ppexne02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexne02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexne02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexne02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexne02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexne02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexne02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_born(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "ud~_epve";
    subprocess[2] = "us~_epve";
    subprocess[3] = "ub~_epve";
    subprocess[4] = "cd~_epve";
    subprocess[5] = "cs~_epve";
    subprocess[6] = "cb~_epve";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gu_epves";
    subprocess[3] = "gu_epveb";
    subprocess[4] = "gc_epved";
    subprocess[5] = "gc_epves";
    subprocess[6] = "gc_epveb";
    subprocess[7] = "gd~_epveu~";
    subprocess[8] = "gd~_epvec~";
    subprocess[9] = "gs~_epveu~";
    subprocess[10] = "gs~_epvec~";
    subprocess[11] = "gb~_epveu~";
    subprocess[12] = "gb~_epvec~";
    subprocess[13] = "ud~_epveg";
    subprocess[14] = "us~_epveg";
    subprocess[15] = "ub~_epveg";
    subprocess[16] = "cd~_epveg";
    subprocess[17] = "cs~_epveg";
    subprocess[18] = "cb~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epved";
    subprocess[2] = "ua_epves";
    subprocess[3] = "ua_epveb";
    subprocess[4] = "ca_epved";
    subprocess[5] = "ca_epves";
    subprocess[6] = "ca_epveb";
    subprocess[7] = "d~a_epveu~";
    subprocess[8] = "d~a_epvec~";
    subprocess[9] = "s~a_epveu~";
    subprocess[10] = "s~a_epvec~";
    subprocess[11] = "b~a_epveu~";
    subprocess[12] = "b~a_epvec~";
    subprocess[13] = "ud~_epvea";
    subprocess[14] = "us~_epvea";
    subprocess[15] = "ub~_epvea";
    subprocess[16] = "cd~_epvea";
    subprocess[17] = "cs~_epvea";
    subprocess[18] = "cb~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gu_epves";
    subprocess[3] = "gu_epveb";
    subprocess[4] = "gc_epved";
    subprocess[5] = "gc_epves";
    subprocess[6] = "gc_epveb";
    subprocess[7] = "gd~_epveu~";
    subprocess[8] = "gd~_epvec~";
    subprocess[9] = "gs~_epveu~";
    subprocess[10] = "gs~_epvec~";
    subprocess[11] = "gb~_epveu~";
    subprocess[12] = "gb~_epvec~";
    subprocess[13] = "ud~_epveg";
    subprocess[14] = "us~_epveg";
    subprocess[15] = "ub~_epveg";
    subprocess[16] = "cd~_epveg";
    subprocess[17] = "cs~_epveg";
    subprocess[18] = "cb~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epved";
    subprocess[2] = "ua_epves";
    subprocess[3] = "ua_epveb";
    subprocess[4] = "ca_epved";
    subprocess[5] = "ca_epves";
    subprocess[6] = "ca_epveb";
    subprocess[7] = "d~a_epveu~";
    subprocess[8] = "d~a_epvec~";
    subprocess[9] = "s~a_epveu~";
    subprocess[10] = "s~a_epvec~";
    subprocess[11] = "b~a_epveu~";
    subprocess[12] = "b~a_epvec~";
    subprocess[13] = "ud~_epvea";
    subprocess[14] = "us~_epvea";
    subprocess[15] = "ub~_epvea";
    subprocess[16] = "cd~_epvea";
    subprocess[17] = "cs~_epvea";
    subprocess[18] = "cb~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gu_epved";
    subprocess[2] = "gu_epves";
    subprocess[3] = "gu_epveb";
    subprocess[4] = "gc_epved";
    subprocess[5] = "gc_epves";
    subprocess[6] = "gc_epveb";
    subprocess[7] = "gd~_epveu~";
    subprocess[8] = "gd~_epvec~";
    subprocess[9] = "gs~_epveu~";
    subprocess[10] = "gs~_epvec~";
    subprocess[11] = "gb~_epveu~";
    subprocess[12] = "gb~_epvec~";
    subprocess[13] = "ud~_epveg";
    subprocess[14] = "us~_epveg";
    subprocess[15] = "ub~_epveg";
    subprocess[16] = "cd~_epveg";
    subprocess[17] = "cs~_epveg";
    subprocess[18] = "cb~_epveg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "ua_epved";
    subprocess[2] = "ua_epves";
    subprocess[3] = "ua_epveb";
    subprocess[4] = "ca_epved";
    subprocess[5] = "ca_epves";
    subprocess[6] = "ca_epveb";
    subprocess[7] = "d~a_epveu~";
    subprocess[8] = "d~a_epvec~";
    subprocess[9] = "s~a_epveu~";
    subprocess[10] = "s~a_epvec~";
    subprocess[11] = "b~a_epveu~";
    subprocess[12] = "b~a_epvec~";
    subprocess[13] = "ud~_epvea";
    subprocess[14] = "us~_epvea";
    subprocess[15] = "ub~_epvea";
    subprocess[16] = "cd~_epvea";
    subprocess[17] = "cs~_epvea";
    subprocess[18] = "cb~_epvea";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexne02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_epvedu~";
    subprocess[2] = "gg_epvedc~";
    subprocess[3] = "gg_epvesu~";
    subprocess[4] = "gg_epvesc~";
    subprocess[5] = "gg_epvebu~";
    subprocess[6] = "gg_epvebc~";
    subprocess[7] = "gu_epvegd";
    subprocess[8] = "gu_epvegs";
    subprocess[9] = "gu_epvegb";
    subprocess[10] = "gc_epvegd";
    subprocess[11] = "gc_epvegs";
    subprocess[12] = "gc_epvegb";
    subprocess[13] = "gd~_epvegu~";
    subprocess[14] = "gd~_epvegc~";
    subprocess[15] = "gs~_epvegu~";
    subprocess[16] = "gs~_epvegc~";
    subprocess[17] = "gb~_epvegu~";
    subprocess[18] = "gb~_epvegc~";
    subprocess[19] = "du_epvedd";
    subprocess[20] = "du_epveds";
    subprocess[21] = "du_epvedb";
    subprocess[22] = "dc_epvedd";
    subprocess[23] = "dc_epveds";
    subprocess[24] = "dc_epvedb";
    subprocess[25] = "dd~_epvedu~";
    subprocess[26] = "dd~_epvedc~";
    subprocess[27] = "dd~_epvesu~";
    subprocess[28] = "dd~_epvesc~";
    subprocess[29] = "dd~_epvebu~";
    subprocess[30] = "dd~_epvebc~";
    subprocess[31] = "ds~_epvedu~";
    subprocess[32] = "ds~_epvedc~";
    subprocess[33] = "db~_epvedu~";
    subprocess[34] = "db~_epvedc~";
    subprocess[35] = "uu_epvedu";
    subprocess[36] = "uu_epveus";
    subprocess[37] = "uu_epveub";
    subprocess[38] = "us_epveds";
    subprocess[39] = "us_epvess";
    subprocess[40] = "us_epvesb";
    subprocess[41] = "uc_epvedu";
    subprocess[42] = "uc_epvedc";
    subprocess[43] = "uc_epveus";
    subprocess[44] = "uc_epveub";
    subprocess[45] = "uc_epvesc";
    subprocess[46] = "uc_epvecb";
    subprocess[47] = "ub_epvedb";
    subprocess[48] = "ub_epvesb";
    subprocess[49] = "ub_epvebb";
    subprocess[50] = "ud~_epvegg";
    subprocess[51] = "ud~_epvedd~";
    subprocess[52] = "ud~_epveuu~";
    subprocess[53] = "ud~_epveuc~";
    subprocess[54] = "ud~_epvesd~";
    subprocess[55] = "ud~_epvess~";
    subprocess[56] = "ud~_epvecc~";
    subprocess[57] = "ud~_epvebd~";
    subprocess[58] = "ud~_epvebb~";
    subprocess[59] = "uu~_epvedu~";
    subprocess[60] = "uu~_epvedc~";
    subprocess[61] = "uu~_epvesu~";
    subprocess[62] = "uu~_epvesc~";
    subprocess[63] = "uu~_epvebu~";
    subprocess[64] = "uu~_epvebc~";
    subprocess[65] = "us~_epvegg";
    subprocess[66] = "us~_epvedd~";
    subprocess[67] = "us~_epveds~";
    subprocess[68] = "us~_epveuu~";
    subprocess[69] = "us~_epveuc~";
    subprocess[70] = "us~_epvess~";
    subprocess[71] = "us~_epvecc~";
    subprocess[72] = "us~_epvebs~";
    subprocess[73] = "us~_epvebb~";
    subprocess[74] = "uc~_epvedc~";
    subprocess[75] = "uc~_epvesc~";
    subprocess[76] = "uc~_epvebc~";
    subprocess[77] = "ub~_epvegg";
    subprocess[78] = "ub~_epvedd~";
    subprocess[79] = "ub~_epvedb~";
    subprocess[80] = "ub~_epveuu~";
    subprocess[81] = "ub~_epveuc~";
    subprocess[82] = "ub~_epvess~";
    subprocess[83] = "ub~_epvesb~";
    subprocess[84] = "ub~_epvecc~";
    subprocess[85] = "ub~_epvebb~";
    subprocess[86] = "sc_epveds";
    subprocess[87] = "sc_epvess";
    subprocess[88] = "sc_epvesb";
    subprocess[89] = "sd~_epvesu~";
    subprocess[90] = "sd~_epvesc~";
    subprocess[91] = "ss~_epvedu~";
    subprocess[92] = "ss~_epvedc~";
    subprocess[93] = "ss~_epvesu~";
    subprocess[94] = "ss~_epvesc~";
    subprocess[95] = "ss~_epvebu~";
    subprocess[96] = "ss~_epvebc~";
    subprocess[97] = "sb~_epvesu~";
    subprocess[98] = "sb~_epvesc~";
    subprocess[99] = "cc_epvedc";
    subprocess[100] = "cc_epvesc";
    subprocess[101] = "cc_epvecb";
    subprocess[102] = "cb_epvedb";
    subprocess[103] = "cb_epvesb";
    subprocess[104] = "cb_epvebb";
    subprocess[105] = "cd~_epvegg";
    subprocess[106] = "cd~_epvedd~";
    subprocess[107] = "cd~_epveuu~";
    subprocess[108] = "cd~_epvesd~";
    subprocess[109] = "cd~_epvess~";
    subprocess[110] = "cd~_epvecu~";
    subprocess[111] = "cd~_epvecc~";
    subprocess[112] = "cd~_epvebd~";
    subprocess[113] = "cd~_epvebb~";
    subprocess[114] = "cu~_epvedu~";
    subprocess[115] = "cu~_epvesu~";
    subprocess[116] = "cu~_epvebu~";
    subprocess[117] = "cs~_epvegg";
    subprocess[118] = "cs~_epvedd~";
    subprocess[119] = "cs~_epveds~";
    subprocess[120] = "cs~_epveuu~";
    subprocess[121] = "cs~_epvess~";
    subprocess[122] = "cs~_epvecu~";
    subprocess[123] = "cs~_epvecc~";
    subprocess[124] = "cs~_epvebs~";
    subprocess[125] = "cs~_epvebb~";
    subprocess[126] = "cc~_epvedu~";
    subprocess[127] = "cc~_epvedc~";
    subprocess[128] = "cc~_epvesu~";
    subprocess[129] = "cc~_epvesc~";
    subprocess[130] = "cc~_epvebu~";
    subprocess[131] = "cc~_epvebc~";
    subprocess[132] = "cb~_epvegg";
    subprocess[133] = "cb~_epvedd~";
    subprocess[134] = "cb~_epvedb~";
    subprocess[135] = "cb~_epveuu~";
    subprocess[136] = "cb~_epvess~";
    subprocess[137] = "cb~_epvesb~";
    subprocess[138] = "cb~_epvecu~";
    subprocess[139] = "cb~_epvecc~";
    subprocess[140] = "cb~_epvebb~";
    subprocess[141] = "bd~_epvebu~";
    subprocess[142] = "bd~_epvebc~";
    subprocess[143] = "bs~_epvebu~";
    subprocess[144] = "bs~_epvebc~";
    subprocess[145] = "bb~_epvedu~";
    subprocess[146] = "bb~_epvedc~";
    subprocess[147] = "bb~_epvesu~";
    subprocess[148] = "bb~_epvesc~";
    subprocess[149] = "bb~_epvebu~";
    subprocess[150] = "bb~_epvebc~";
    subprocess[151] = "d~d~_epved~u~";
    subprocess[152] = "d~d~_epved~c~";
    subprocess[153] = "d~u~_epveu~u~";
    subprocess[154] = "d~u~_epveu~c~";
    subprocess[155] = "d~s~_epved~u~";
    subprocess[156] = "d~s~_epved~c~";
    subprocess[157] = "d~s~_epveu~s~";
    subprocess[158] = "d~s~_epves~c~";
    subprocess[159] = "d~c~_epveu~c~";
    subprocess[160] = "d~c~_epvec~c~";
    subprocess[161] = "d~b~_epved~u~";
    subprocess[162] = "d~b~_epved~c~";
    subprocess[163] = "d~b~_epveu~b~";
    subprocess[164] = "d~b~_epvec~b~";
    subprocess[165] = "u~s~_epveu~u~";
    subprocess[166] = "u~s~_epveu~c~";
    subprocess[167] = "u~b~_epveu~u~";
    subprocess[168] = "u~b~_epveu~c~";
    subprocess[169] = "s~s~_epveu~s~";
    subprocess[170] = "s~s~_epves~c~";
    subprocess[171] = "s~c~_epveu~c~";
    subprocess[172] = "s~c~_epvec~c~";
    subprocess[173] = "s~b~_epveu~s~";
    subprocess[174] = "s~b~_epveu~b~";
    subprocess[175] = "s~b~_epves~c~";
    subprocess[176] = "s~b~_epvec~b~";
    subprocess[177] = "c~b~_epveu~c~";
    subprocess[178] = "c~b~_epvec~c~";
    subprocess[179] = "b~b~_epveu~b~";
    subprocess[180] = "b~b~_epvec~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
