#include "header.hpp"

#include "ppexne02.contribution.set.hpp"

ppexne02_contribution_set::~ppexne02_contribution_set(){
  static Logger logger("ppexne02_contribution_set::~ppexne02_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppexne02_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppexne02_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppexne02_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   5)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   0)){no_process_parton[i_a] = 21; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   3)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==   5)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -2)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  -4)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 33; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[5] ==  22)){no_process_parton[i_a] = 36; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 33){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppexne02_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  d    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  d    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  s    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  b    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  b    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  d    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  d    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  s    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  b    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  b    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  ux   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  cx   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> ex  ne  ux   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> ex  ne  cx   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ex  ne  ux   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ex  ne  cx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  g    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  g    //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  g    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  g    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  g    //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ex  ne  d    //
      combination_pdf[1] = {-1,   2,   7};   // a   u    -> ex  ne  d    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ex  ne  s    //
      combination_pdf[1] = {-1,   2,   7};   // a   u    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> ex  ne  b    //
      combination_pdf[1] = {-1,   2,   7};   // a   u    -> ex  ne  b    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   7};   // c   a    -> ex  ne  d    //
      combination_pdf[1] = {-1,   4,   7};   // a   c    -> ex  ne  d    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   7};   // c   a    -> ex  ne  s    //
      combination_pdf[1] = {-1,   4,   7};   // a   c    -> ex  ne  s    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   7};   // c   a    -> ex  ne  b    //
      combination_pdf[1] = {-1,   4,   7};   // a   c    -> ex  ne  b    //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> ex  ne  ux   //
      combination_pdf[1] = {-1,  -1,   7};   // a   dx   -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> ex  ne  cx   //
      combination_pdf[1] = {-1,  -1,   7};   // a   dx   -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,   7};   // sx  a    -> ex  ne  ux   //
      combination_pdf[1] = {-1,  -3,   7};   // a   sx   -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,   7};   // sx  a    -> ex  ne  cx   //
      combination_pdf[1] = {-1,  -3,   7};   // a   sx   -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> ex  ne  ux   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> ex  ne  ux   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> ex  ne  cx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> ex  ne  cx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  a    //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  a    //
    }
    else if (no_process_parton[i_a] == 33){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  a    //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  a    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  a    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  a    //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppexne02_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 33; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 49; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 57; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 64; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 68; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 69; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 70; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 71; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 72; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 75; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 77; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 78; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 79; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 80; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 83; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 84; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 85; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 86; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 89; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 90; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 92; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 93; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 94; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 95; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 96; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 97; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 98; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 99; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 100; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 101; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 103; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 104; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 106; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 109; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 111; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 112; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 114; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 116; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 118; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 119; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 120; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 122; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 123; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 124; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 126; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 127; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 129; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 132; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 137; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 139; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 140; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 144; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 145; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 148; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 149; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 150; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 151; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 152; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 153; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 156; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 157; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 161; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 164; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 165; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 168; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 170; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 171; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 172; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 173; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 176; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 178; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 179; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 181; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 182; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 183; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 185; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 186; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 188; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 190; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 192; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 193; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 194; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 196; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 199; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 201; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 202; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 204; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 205; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 206; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 207; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 208; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 209; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 210; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 211; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 212; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 213; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 215; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 216; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 219; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 220; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 221; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 222; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 225; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 233; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 234; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 239; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 240; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 241; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 242; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 243; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 244; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 245; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 246; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 247; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 248; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 253; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 254; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 256; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 257; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 258; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 260; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 263; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 264; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 265; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 266; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 268; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 270; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 273; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 274; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 276; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 277; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 281; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 283; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 286; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 287; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 290; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 291; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 292; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 293; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 297; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 298; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 302; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] == -11) && (tp[4] ==  12) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 304; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 33){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 49){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 57){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 64){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 68){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 70){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 71){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 72){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 75){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 77){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 78){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 79){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 80){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 83){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 84){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 85){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 86){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 89){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 90){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 92){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 93){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 94){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 95){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 96){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 97){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 98){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 99){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 100){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 101){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 103){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 104){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 106){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 109){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 111){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 112){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 114){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 116){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 118){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 119){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 120){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 122){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 123){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 124){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 126){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 127){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 129){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 132){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 137){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 139){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 140){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 144){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 145){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 148){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 149){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 150){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 151){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 152){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 153){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 156){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 157){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 161){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 164){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 165){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 168){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 170){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 171){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 172){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 173){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 176){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 178){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 179){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 181){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 182){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 183){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 185){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 186){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 188){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 190){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 192){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 193){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 194){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 196){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 199){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 201){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 202){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 204){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 205){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 206){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 207){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 208){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 209){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 210){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 211){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 212){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 213){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 215){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 216){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 219){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 220){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 221){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 222){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 225){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 233){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 234){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 239){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 240){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 241){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 242){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 243){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 244){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 245){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 246){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 247){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 248){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 253){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 254){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 256){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 257){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 258){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 260){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 263){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 264){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 265){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 266){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 268){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 270){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 273){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 274){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 276){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 277){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 281){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 283){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 286){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 287){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 290){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 291){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 292){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 293){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 297){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 298){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 302){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 304){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexne02_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppexne02_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  g   d    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  g   d    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  g   s    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  g   s    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> ex  ne  g   b    //
      combination_pdf[1] = {-1,   0,   2};   // u   g    -> ex  ne  g   b    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  g   d    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  g   d    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  g   s    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  g   s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   4};   // g   c    -> ex  ne  g   b    //
      combination_pdf[1] = {-1,   0,   4};   // c   g    -> ex  ne  g   b    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  g   ux   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> ex  ne  g   ux   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> ex  ne  g   cx   //
      combination_pdf[1] = {-1,   0,  -1};   // dx  g    -> ex  ne  g   cx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> ex  ne  g   ux   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> ex  ne  g   ux   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -3};   // g   sx   -> ex  ne  g   cx   //
      combination_pdf[1] = {-1,   0,  -3};   // sx  g    -> ex  ne  g   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ex  ne  g   ux   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ex  ne  g   ux   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> ex  ne  g   cx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> ex  ne  g   cx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ex  ne  d   d    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> ex  ne  d   d    //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ex  ne  d   s    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> ex  ne  d   s    //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> ex  ne  d   b    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> ex  ne  d   b    //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ex  ne  d   d    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> ex  ne  d   d    //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ex  ne  d   s    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> ex  ne  d   s    //
    }
    else if (no_process_parton[i_a] == 33){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> ex  ne  d   b    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> ex  ne  d   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 49){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ex  ne  d   u    //
    }
    else if (no_process_parton[i_a] == 57){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ex  ne  u   s    //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> ex  ne  u   b    //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> ex  ne  d   s    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> ex  ne  d   s    //
    }
    else if (no_process_parton[i_a] == 64){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> ex  ne  s   s    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> ex  ne  s   s    //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> ex  ne  s   b    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> ex  ne  s   b    //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  d   u    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  d   u    //
    }
    else if (no_process_parton[i_a] == 68){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  d   c    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  d   c    //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  u   s    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  u   s    //
    }
    else if (no_process_parton[i_a] == 70){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  u   b    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  u   b    //
    }
    else if (no_process_parton[i_a] == 71){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  s   c    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  s   c    //
    }
    else if (no_process_parton[i_a] == 72){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> ex  ne  c   b    //
      combination_pdf[1] = {-1,   2,   4};   // c   u    -> ex  ne  c   b    //
    }
    else if (no_process_parton[i_a] == 75){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> ex  ne  d   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> ex  ne  d   b    //
    }
    else if (no_process_parton[i_a] == 77){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> ex  ne  s   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> ex  ne  s   b    //
    }
    else if (no_process_parton[i_a] == 78){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> ex  ne  b   b    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> ex  ne  b   b    //
    }
    else if (no_process_parton[i_a] == 79){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 80){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 83){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 84){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  u   cx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  u   cx   //
    }
    else if (no_process_parton[i_a] == 85){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  s   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  s   dx   //
    }
    else if (no_process_parton[i_a] == 86){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 89){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 90){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  b   dx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  b   dx   //
    }
    else if (no_process_parton[i_a] == 92){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   2,  -1};   // dx  u    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 93){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 94){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 95){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 96){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 97){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 98){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 99){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 100){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 101){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  d   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  d   sx   //
    }
    else if (no_process_parton[i_a] == 103){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 104){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  u   cx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  u   cx   //
    }
    else if (no_process_parton[i_a] == 106){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 109){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 111){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  b   sx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  b   sx   //
    }
    else if (no_process_parton[i_a] == 112){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   2,  -3};   // sx  u    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 114){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 116){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 118){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 119){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 120){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 122){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  d   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  d   bx   //
    }
    else if (no_process_parton[i_a] == 123){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 124){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  u   cx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  u   cx   //
    }
    else if (no_process_parton[i_a] == 126){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 127){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  s   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  s   bx   //
    }
    else if (no_process_parton[i_a] == 129){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 132){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   2,  -5};   // bx  u    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 137){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> ex  ne  d   s    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> ex  ne  d   s    //
    }
    else if (no_process_parton[i_a] == 139){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> ex  ne  s   s    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> ex  ne  s   s    //
    }
    else if (no_process_parton[i_a] == 140){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> ex  ne  s   b    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> ex  ne  s   b    //
    }
    else if (no_process_parton[i_a] == 144){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 145){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 148){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 149){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 150){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 151){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 152){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 153){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 156){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 157){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 161){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> ex  ne  d   c    //
    }
    else if (no_process_parton[i_a] == 164){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> ex  ne  s   c    //
    }
    else if (no_process_parton[i_a] == 165){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   4,   4};   // c   c    -> ex  ne  c   b    //
    }
    else if (no_process_parton[i_a] == 168){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> ex  ne  d   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> ex  ne  d   b    //
    }
    else if (no_process_parton[i_a] == 170){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> ex  ne  s   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> ex  ne  s   b    //
    }
    else if (no_process_parton[i_a] == 171){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> ex  ne  b   b    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> ex  ne  b   b    //
    }
    else if (no_process_parton[i_a] == 172){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 173){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 176){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 178){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  s   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  s   dx   //
    }
    else if (no_process_parton[i_a] == 179){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 181){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  c   ux   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  c   ux   //
    }
    else if (no_process_parton[i_a] == 182){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 183){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  b   dx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  b   dx   //
    }
    else if (no_process_parton[i_a] == 185){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -1};   // c   dx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   4,  -1};   // dx  c    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 186){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 188){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 190){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 192){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 193){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 194){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  d   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  d   sx   //
    }
    else if (no_process_parton[i_a] == 196){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 199){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 201){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  c   ux   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  c   ux   //
    }
    else if (no_process_parton[i_a] == 202){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 204){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  b   sx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  b   sx   //
    }
    else if (no_process_parton[i_a] == 205){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -3};   // c   sx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   4,  -3};   // sx  c    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 206){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 207){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 208){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 209){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 210){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 211){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 212){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  g   g    //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  g   g    //
    }
    else if (no_process_parton[i_a] == 213){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  d   dx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  d   dx   //
    }
    else if (no_process_parton[i_a] == 215){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  d   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  d   bx   //
    }
    else if (no_process_parton[i_a] == 216){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  u   ux   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  u   ux   //
    }
    else if (no_process_parton[i_a] == 219){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  s   sx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  s   sx   //
    }
    else if (no_process_parton[i_a] == 220){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  s   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  s   bx   //
    }
    else if (no_process_parton[i_a] == 221){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  c   ux   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  c   ux   //
    }
    else if (no_process_parton[i_a] == 222){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  c   cx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  c   cx   //
    }
    else if (no_process_parton[i_a] == 225){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -5};   // c   bx   -> ex  ne  b   bx   //
      combination_pdf[1] = {-1,   4,  -5};   // bx  c    -> ex  ne  b   bx   //
    }
    else if (no_process_parton[i_a] == 233){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 234){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 239){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 240){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 241){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  d   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  d   ux   //
    }
    else if (no_process_parton[i_a] == 242){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  d   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  d   cx   //
    }
    else if (no_process_parton[i_a] == 243){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  s   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  s   ux   //
    }
    else if (no_process_parton[i_a] == 244){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  s   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  s   cx   //
    }
    else if (no_process_parton[i_a] == 245){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  b   ux   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  b   ux   //
    }
    else if (no_process_parton[i_a] == 246){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> ex  ne  b   cx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> ex  ne  b   cx   //
    }
    else if (no_process_parton[i_a] == 247){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> ex  ne  dx  ux   //
    }
    else if (no_process_parton[i_a] == 248){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> ex  ne  dx  cx   //
    }
    else if (no_process_parton[i_a] == 253){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> ex  ne  ux  ux   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> ex  ne  ux  ux   //
    }
    else if (no_process_parton[i_a] == 254){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 256){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  dx  ux   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  dx  ux   //
    }
    else if (no_process_parton[i_a] == 257){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  dx  cx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  dx  cx   //
    }
    else if (no_process_parton[i_a] == 258){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  ux  sx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  ux  sx   //
    }
    else if (no_process_parton[i_a] == 260){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> ex  ne  sx  cx   //
      combination_pdf[1] = {-1,  -1,  -3};   // sx  dx   -> ex  ne  sx  cx   //
    }
    else if (no_process_parton[i_a] == 263){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 264){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> ex  ne  cx  cx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> ex  ne  cx  cx   //
    }
    else if (no_process_parton[i_a] == 265){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ex  ne  dx  ux   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> ex  ne  dx  ux   //
    }
    else if (no_process_parton[i_a] == 266){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ex  ne  dx  cx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> ex  ne  dx  cx   //
    }
    else if (no_process_parton[i_a] == 268){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ex  ne  ux  bx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> ex  ne  ux  bx   //
    }
    else if (no_process_parton[i_a] == 270){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> ex  ne  cx  bx   //
      combination_pdf[1] = {-1,  -1,  -5};   // bx  dx   -> ex  ne  cx  bx   //
    }
    else if (no_process_parton[i_a] == 273){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> ex  ne  ux  ux   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> ex  ne  ux  ux   //
    }
    else if (no_process_parton[i_a] == 274){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 276){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> ex  ne  ux  ux   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> ex  ne  ux  ux   //
    }
    else if (no_process_parton[i_a] == 277){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 281){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -3,  -3};   // sx  sx   -> ex  ne  ux  sx   //
    }
    else if (no_process_parton[i_a] == 283){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -3,  -3};   // sx  sx   -> ex  ne  sx  cx   //
    }
    else if (no_process_parton[i_a] == 286){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 287){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> ex  ne  cx  cx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> ex  ne  cx  cx   //
    }
    else if (no_process_parton[i_a] == 290){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> ex  ne  ux  sx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> ex  ne  ux  sx   //
    }
    else if (no_process_parton[i_a] == 291){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> ex  ne  ux  bx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> ex  ne  ux  bx   //
    }
    else if (no_process_parton[i_a] == 292){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> ex  ne  sx  cx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> ex  ne  sx  cx   //
    }
    else if (no_process_parton[i_a] == 293){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -5};   // sx  bx   -> ex  ne  cx  bx   //
      combination_pdf[1] = {-1,  -3,  -5};   // bx  sx   -> ex  ne  cx  bx   //
    }
    else if (no_process_parton[i_a] == 297){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> ex  ne  ux  cx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> ex  ne  ux  cx   //
    }
    else if (no_process_parton[i_a] == 298){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> ex  ne  cx  cx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> ex  ne  cx  cx   //
    }
    else if (no_process_parton[i_a] == 302){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> ex  ne  ux  bx   //
    }
    else if (no_process_parton[i_a] == 304){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> ex  ne  cx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
