#include "header.hpp"

#include "ppeexnmnmx04.summary.hpp"

ppeexnmnmx04_summary_generic::ppeexnmnmx04_summary_generic(munich * xmunich){
  Logger logger("ppeexnmnmx04_summary_generic::ppeexnmnmx04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeexnmnmx04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeexnmnmx04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeexnmnmx04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeexnmnmx04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeexnmnmx04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeexnmnmx04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_born(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subprocess[4] = "aa_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subprocess[4] = "aa_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subprocess[4] = "aa_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emepvmvm~";
    subprocess[2] = "uu~_emepvmvm~";
    subprocess[3] = "bb~_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emepvmvm~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepvmvm~d";
    subprocess[2] = "gu_emepvmvm~u";
    subprocess[3] = "gb_emepvmvm~b";
    subprocess[4] = "gd~_emepvmvm~d~";
    subprocess[5] = "gu~_emepvmvm~u~";
    subprocess[6] = "gb~_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~g";
    subprocess[8] = "uu~_emepvmvm~g";
    subprocess[9] = "bb~_emepvmvm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emepvmvm~g";
    subprocess[2] = "gd_emepvmvm~d";
    subprocess[3] = "gu_emepvmvm~u";
    subprocess[4] = "gb_emepvmvm~b";
    subprocess[5] = "gd~_emepvmvm~d~";
    subprocess[6] = "gu~_emepvmvm~u~";
    subprocess[7] = "gb~_emepvmvm~b~";
    subprocess[8] = "dd~_emepvmvm~g";
    subprocess[9] = "uu~_emepvmvm~g";
    subprocess[10] = "bb~_emepvmvm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emepvmvm~d";
    subprocess[2] = "ua_emepvmvm~u";
    subprocess[3] = "ba_emepvmvm~b";
    subprocess[4] = "d~a_emepvmvm~d~";
    subprocess[5] = "u~a_emepvmvm~u~";
    subprocess[6] = "b~a_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~a";
    subprocess[8] = "uu~_emepvmvm~a";
    subprocess[9] = "bb~_emepvmvm~a";
    subprocess[10] = "aa_emepvmvm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepvmvm~d";
    subprocess[2] = "gu_emepvmvm~u";
    subprocess[3] = "gb_emepvmvm~b";
    subprocess[4] = "gd~_emepvmvm~d~";
    subprocess[5] = "gu~_emepvmvm~u~";
    subprocess[6] = "gb~_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~g";
    subprocess[8] = "uu~_emepvmvm~g";
    subprocess[9] = "bb~_emepvmvm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepvmvm~d";
    subprocess[2] = "ua_emepvmvm~u";
    subprocess[3] = "ba_emepvmvm~b";
    subprocess[4] = "d~a_emepvmvm~d~";
    subprocess[5] = "u~a_emepvmvm~u~";
    subprocess[6] = "b~a_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~a";
    subprocess[8] = "uu~_emepvmvm~a";
    subprocess[9] = "bb~_emepvmvm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emepvmvm~d";
    subprocess[2] = "gu_emepvmvm~u";
    subprocess[3] = "gb_emepvmvm~b";
    subprocess[4] = "gd~_emepvmvm~d~";
    subprocess[5] = "gu~_emepvmvm~u~";
    subprocess[6] = "gb~_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~g";
    subprocess[8] = "uu~_emepvmvm~g";
    subprocess[9] = "bb~_emepvmvm~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emepvmvm~d";
    subprocess[2] = "ua_emepvmvm~u";
    subprocess[3] = "ba_emepvmvm~b";
    subprocess[4] = "d~a_emepvmvm~d~";
    subprocess[5] = "u~a_emepvmvm~u~";
    subprocess[6] = "b~a_emepvmvm~b~";
    subprocess[7] = "dd~_emepvmvm~a";
    subprocess[8] = "uu~_emepvmvm~a";
    subprocess[9] = "bb~_emepvmvm~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeexnmnmx04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_emepvmvm~dd~";
    subprocess[2] = "gg_emepvmvm~uu~";
    subprocess[3] = "gg_emepvmvm~bb~";
    subprocess[4] = "gd_emepvmvm~gd";
    subprocess[5] = "gu_emepvmvm~gu";
    subprocess[6] = "gb_emepvmvm~gb";
    subprocess[7] = "gd~_emepvmvm~gd~";
    subprocess[8] = "gu~_emepvmvm~gu~";
    subprocess[9] = "gb~_emepvmvm~gb~";
    subprocess[10] = "dd_emepvmvm~dd";
    subprocess[11] = "du_emepvmvm~du";
    subprocess[12] = "ds_emepvmvm~ds";
    subprocess[13] = "dc_emepvmvm~dc";
    subprocess[14] = "db_emepvmvm~db";
    subprocess[15] = "dd~_emepvmvm~gg";
    subprocess[16] = "dd~_emepvmvm~dd~";
    subprocess[17] = "dd~_emepvmvm~uu~";
    subprocess[18] = "dd~_emepvmvm~ss~";
    subprocess[19] = "dd~_emepvmvm~cc~";
    subprocess[20] = "dd~_emepvmvm~bb~";
    subprocess[21] = "du~_emepvmvm~du~";
    subprocess[22] = "ds~_emepvmvm~ds~";
    subprocess[23] = "dc~_emepvmvm~dc~";
    subprocess[24] = "db~_emepvmvm~db~";
    subprocess[25] = "uu_emepvmvm~uu";
    subprocess[26] = "uc_emepvmvm~uc";
    subprocess[27] = "ub_emepvmvm~ub";
    subprocess[28] = "ud~_emepvmvm~ud~";
    subprocess[29] = "uu~_emepvmvm~gg";
    subprocess[30] = "uu~_emepvmvm~dd~";
    subprocess[31] = "uu~_emepvmvm~uu~";
    subprocess[32] = "uu~_emepvmvm~ss~";
    subprocess[33] = "uu~_emepvmvm~cc~";
    subprocess[34] = "uu~_emepvmvm~bb~";
    subprocess[35] = "us~_emepvmvm~us~";
    subprocess[36] = "uc~_emepvmvm~uc~";
    subprocess[37] = "ub~_emepvmvm~ub~";
    subprocess[38] = "bb_emepvmvm~bb";
    subprocess[39] = "bd~_emepvmvm~bd~";
    subprocess[40] = "bu~_emepvmvm~bu~";
    subprocess[41] = "bb~_emepvmvm~gg";
    subprocess[42] = "bb~_emepvmvm~dd~";
    subprocess[43] = "bb~_emepvmvm~uu~";
    subprocess[44] = "bb~_emepvmvm~bb~";
    subprocess[45] = "d~d~_emepvmvm~d~d~";
    subprocess[46] = "d~u~_emepvmvm~d~u~";
    subprocess[47] = "d~s~_emepvmvm~d~s~";
    subprocess[48] = "d~c~_emepvmvm~d~c~";
    subprocess[49] = "d~b~_emepvmvm~d~b~";
    subprocess[50] = "u~u~_emepvmvm~u~u~";
    subprocess[51] = "u~c~_emepvmvm~u~c~";
    subprocess[52] = "u~b~_emepvmvm~u~b~";
    subprocess[53] = "b~b~_emepvmvm~b~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
