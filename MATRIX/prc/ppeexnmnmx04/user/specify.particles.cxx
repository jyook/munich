logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  USERPARTICLE("<particle_name>").push_back(<particle>);

  static int switch_lepton_identification = USERSWITCH("lepton_identification");

  // lepton identification not needed in DF case, but might be useful if for whatever reason event selection is build only on reconstructed Z bosons

  if (switch_lepton_identification){
    USERPARTICLE("Znrec").push_back(PARTICLE("nu")[0] + PARTICLE("nux")[0]);
    USERPARTICLE("Zlrec").push_back(PARTICLE("lm")[0] + PARTICLE("lp")[0]);
    USERPARTICLE("Zrec").push_back(USERPARTICLE("Znrec")[0]);
    USERPARTICLE("Zrec").push_back(USERPARTICLE("Zlrec")[0]);

    vector<double> abs_M_leplep_MZ_combination(PARTICLE("Zrec").size());
    for (int i_z = 0; i_z < PARTICLE("Zrec").size(); i_z++){
      abs_M_leplep_MZ_combination[i_z] = abs(PARTICLE("Zrec")[i_z].m - msi->M_Z);
    }

    if (abs_M_leplep_MZ_combination[0] < abs_M_leplep_MZ_combination[1]){
      USERPARTICLE("Z1rec").push_back(PARTICLE("Zrec")[0]);
      USERPARTICLE("Z2rec").push_back(PARTICLE("Zrec")[1]);
    } 
    else {
      USERPARTICLE("Z1rec").push_back(PARTICLE("Zrec")[1]);
      USERPARTICLE("Z2rec").push_back(PARTICLE("Zrec")[0]);
    }
  }

}
logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
