#include "header.hpp"

#include "ppeexnmnmx04.phasespace.set.hpp"

ppeexnmnmx04_phasespace_set::~ppeexnmnmx04_phasespace_set(){
  static Logger logger("ppeexnmnmx04_phasespace_set::~ppeexnmnmx04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppeexnmnmx04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnmnmx04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 21;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 11;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 25;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -23, -25};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-23);
      //  tau_MC_map.push_back(35);
      tau_MC_map.push_back(-36);
      tau_MC_map.push_back(-25);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_040_ddx_emepvmvmx(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_040_uux_emepvmvmx(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_040_bbx_emepvmvmx(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_040_aa_emepvmvmx(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_240_gg_emepvmvmx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_040_ddx_emepvmvmx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_040_uux_emepvmvmx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_040_bbx_emepvmvmx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_040_aa_emepvmvmx(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_240_gg_emepvmvmx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppeexnmnmx04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_040_ddx_emepvmvmx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_040_uux_emepvmvmx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_040_bbx_emepvmvmx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_040_aa_emepvmvmx(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_240_gg_emepvmvmx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppeexnmnmx04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnmnmx04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 79;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 24;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 24;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 26;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 24;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 24;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 26;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 24;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 24;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 26;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 171;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 52;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 52;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 48;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 52;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 24;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 225;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 33;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 33;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 33;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 33;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 33;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 33;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 33;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 33;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 33;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0, -23, -25};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      //  tau_MC_map.push_back(35);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  6){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 14){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 16){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_140_gd_emepvmvmxd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_140_gu_emepvmvmxu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_140_gb_emepvmvmxb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_140_gdx_emepvmvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_140_gux_emepvmvmxux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_140_gbx_emepvmvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_140_ddx_emepvmvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_140_uux_emepvmvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_140_bbx_emepvmvmxg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_050_da_emepvmvmxd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_050_ua_emepvmvmxu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_050_ba_emepvmvmxb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_050_dxa_emepvmvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_050_uxa_emepvmvmxux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_050_bxa_emepvmvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_050_ddx_emepvmvmxa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_050_uux_emepvmvmxa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_050_bbx_emepvmvmxa(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_050_aa_emepvmvmxa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_340_gg_emepvmvmxg(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_340_gd_emepvmvmxd(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_340_gu_emepvmvmxu(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_340_gb_emepvmvmxb(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_340_gdx_emepvmvmxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_340_gux_emepvmvmxux(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_340_gbx_emepvmvmxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_340_ddx_emepvmvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_340_uux_emepvmvmxg(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_340_bbx_emepvmvmxg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_140_gd_emepvmvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_140_gu_emepvmvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_140_gb_emepvmvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_140_gdx_emepvmvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_140_gux_emepvmvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_140_gbx_emepvmvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_140_ddx_emepvmvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_140_uux_emepvmvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_140_bbx_emepvmvmxg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_050_da_emepvmvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_050_ua_emepvmvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_050_ba_emepvmvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_050_dxa_emepvmvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_050_uxa_emepvmvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_050_bxa_emepvmvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_050_ddx_emepvmvmxa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_050_uux_emepvmvmxa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_050_bbx_emepvmvmxa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_050_aa_emepvmvmxa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_340_gg_emepvmvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_340_gd_emepvmvmxd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_340_gu_emepvmvmxu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_340_gb_emepvmvmxb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_340_gdx_emepvmvmxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_340_gux_emepvmvmxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_340_gbx_emepvmvmxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_340_ddx_emepvmvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_340_uux_emepvmvmxg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_340_bbx_emepvmvmxg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppeexnmnmx04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_140_gd_emepvmvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_140_gu_emepvmvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_140_gb_emepvmvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_140_gdx_emepvmvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_140_gux_emepvmvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_140_gbx_emepvmvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_140_ddx_emepvmvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_140_uux_emepvmvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_140_bbx_emepvmvmxg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_050_da_emepvmvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_050_ua_emepvmvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_050_ba_emepvmvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_050_dxa_emepvmvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_050_uxa_emepvmvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_050_bxa_emepvmvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_050_ddx_emepvmvmxa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_050_uux_emepvmvmxa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_050_bbx_emepvmvmxa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_050_aa_emepvmvmxa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_340_gg_emepvmvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_340_gd_emepvmvmxd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_340_gu_emepvmvmxu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_340_gb_emepvmvmxb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_340_gdx_emepvmvmxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_340_gux_emepvmvmxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_340_gbx_emepvmvmxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_340_ddx_emepvmvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_340_uux_emepvmvmxg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_340_bbx_emepvmvmxg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppeexnmnmx04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppeexnmnmx04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 737;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 108;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 108;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 116;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 108;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 108;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 116;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 108;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 108;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 116;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 108;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 108;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 136;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 116;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 136;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 128;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 66;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 136;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0,  -5};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppeexnmnmx04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gg_emepvmvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gg_emepvmvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_240_gg_emepvmvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_gd_emepvmvmxgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_gu_emepvmvmxgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_gb_emepvmvmxgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_gdx_emepvmvmxgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_gux_emepvmvmxgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_gbx_emepvmvmxgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_dd_emepvmvmxdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_du_emepvmvmxdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_ds_emepvmvmxds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_dc_emepvmvmxdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_db_emepvmvmxdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_ddx_emepvmvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_240_ddx_emepvmvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_ddx_emepvmvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_ddx_emepvmvmxssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_ddx_emepvmvmxccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_ddx_emepvmvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_240_dux_emepvmvmxdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_240_dsx_emepvmvmxdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_240_dcx_emepvmvmxdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_240_dbx_emepvmvmxdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_240_uu_emepvmvmxuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_240_uc_emepvmvmxuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_240_ub_emepvmvmxub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_240_udx_emepvmvmxudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_240_uux_emepvmvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_240_uux_emepvmvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_240_uux_emepvmvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_240_uux_emepvmvmxssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_240_uux_emepvmvmxccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_240_uux_emepvmvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_240_usx_emepvmvmxusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_240_ucx_emepvmvmxucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_240_ubx_emepvmvmxubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_240_bb_emepvmvmxbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_240_bdx_emepvmvmxbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_240_bux_emepvmvmxbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_240_bbx_emepvmvmxgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_240_bbx_emepvmvmxddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_240_bbx_emepvmvmxuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_240_bbx_emepvmvmxbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_240_dxdx_emepvmvmxdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_240_dxux_emepvmvmxdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_240_dxsx_emepvmvmxdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_240_dxcx_emepvmvmxdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_240_dxbx_emepvmvmxdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_240_uxux_emepvmvmxuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_240_uxcx_emepvmvmxuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_240_uxbx_emepvmvmxuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_240_bxbx_emepvmvmxbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppeexnmnmx04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gg_emepvmvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gg_emepvmvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_240_gg_emepvmvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_gd_emepvmvmxgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_gu_emepvmvmxgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_gb_emepvmvmxgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_gdx_emepvmvmxgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_gux_emepvmvmxgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_gbx_emepvmvmxgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_dd_emepvmvmxdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_du_emepvmvmxdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_ds_emepvmvmxds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_dc_emepvmvmxdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_db_emepvmvmxdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_ddx_emepvmvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_240_ddx_emepvmvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_ddx_emepvmvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_ddx_emepvmvmxssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_ddx_emepvmvmxccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_ddx_emepvmvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_240_dux_emepvmvmxdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_240_dsx_emepvmvmxdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_240_dcx_emepvmvmxdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_240_dbx_emepvmvmxdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_240_uu_emepvmvmxuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_240_uc_emepvmvmxuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_240_ub_emepvmvmxub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_240_udx_emepvmvmxudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_240_uux_emepvmvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_240_uux_emepvmvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_240_uux_emepvmvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_240_uux_emepvmvmxssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_240_uux_emepvmvmxccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_240_uux_emepvmvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_240_usx_emepvmvmxusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_240_ucx_emepvmvmxucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_240_ubx_emepvmvmxubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_240_bb_emepvmvmxbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_240_bdx_emepvmvmxbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_240_bux_emepvmvmxbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_240_bbx_emepvmvmxgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_240_bbx_emepvmvmxddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_240_bbx_emepvmvmxuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_240_bbx_emepvmvmxbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_240_dxdx_emepvmvmxdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_240_dxux_emepvmvmxdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_240_dxsx_emepvmvmxdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_240_dxcx_emepvmvmxdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_240_dxbx_emepvmvmxdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_240_uxux_emepvmvmxuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_240_uxcx_emepvmvmxuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_240_uxbx_emepvmvmxuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_240_bxbx_emepvmvmxbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeexnmnmx04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppeexnmnmx04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gg_emepvmvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gg_emepvmvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_240_gg_emepvmvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_gd_emepvmvmxgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_gu_emepvmvmxgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_gb_emepvmvmxgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_gdx_emepvmvmxgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_gux_emepvmvmxgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_gbx_emepvmvmxgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_dd_emepvmvmxdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_du_emepvmvmxdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_ds_emepvmvmxds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_dc_emepvmvmxdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_db_emepvmvmxdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_ddx_emepvmvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_240_ddx_emepvmvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_ddx_emepvmvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_ddx_emepvmvmxssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_ddx_emepvmvmxccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_ddx_emepvmvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_240_dux_emepvmvmxdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_240_dsx_emepvmvmxdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_240_dcx_emepvmvmxdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_240_dbx_emepvmvmxdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_240_uu_emepvmvmxuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_240_uc_emepvmvmxuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_240_ub_emepvmvmxub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_240_udx_emepvmvmxudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_240_uux_emepvmvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_240_uux_emepvmvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_240_uux_emepvmvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_240_uux_emepvmvmxssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_240_uux_emepvmvmxccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_240_uux_emepvmvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_240_usx_emepvmvmxusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_240_ucx_emepvmvmxucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_240_ubx_emepvmvmxubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_240_bb_emepvmvmxbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_240_bdx_emepvmvmxbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_240_bux_emepvmvmxbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_240_bbx_emepvmvmxgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_240_bbx_emepvmvmxddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_240_bbx_emepvmvmxuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_240_bbx_emepvmvmxbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_240_dxdx_emepvmvmxdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_240_dxux_emepvmvmxdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_240_dxsx_emepvmvmxdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_240_dxcx_emepvmvmxdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_240_dxbx_emepvmvmxdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_240_uxux_emepvmvmxuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_240_uxcx_emepvmvmxuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_240_uxbx_emepvmvmxuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_240_bxbx_emepvmvmxbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
