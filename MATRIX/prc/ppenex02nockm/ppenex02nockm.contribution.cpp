#include "header.hpp"

#include "ppenex02nockm.contribution.set.hpp"

ppenex02nockm_contribution_set::~ppenex02nockm_contribution_set(){
  static Logger logger("ppenex02nockm_contribution_set::~ppenex02nockm_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex  //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex  //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex  //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex  //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   0)){no_process_parton[i_a] = 3; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   2)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==   4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  22)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[5] ==  22)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex u    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   nex c    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   nex u    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   nex c    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex dx   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   nex sx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   nex dx   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   nex sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex g    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex g    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex g    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> e   nex u    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> e   nex c    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> e   nex u    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> e   nex c    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> e   nex dx   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> e   nex sx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> e   nex dx   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> e   nex sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex a    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex a    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex a    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   3) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -12) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppenex02nockm_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> e   nex u   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> e   nex c   sx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> e   nex g   u    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> e   nex g   c    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> e   nex g   u    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> e   nex g   c    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> e   nex g   dx   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> e   nex g   sx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> e   nex g   dx   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> e   nex g   sx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> e   nex d   u    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> e   nex s   c    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> e   nex u   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> e   nex c   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> e   nex u   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> e   nex c   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> e   nex d   c    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> e   nex u   s    //
      combination_pdf[2] = {-1,   3,   1};   // d   s    -> e   nex u   s    //
      combination_pdf[3] = {-1,   1,   3};   // s   d    -> e   nex d   c    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> e   nex u   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> e   nex u   c    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> e   nex u   c    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> e   nex u   c    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> e   nex u   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> e   nex c   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> e   nex u   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> e   nex c   b    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex u   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   nex c   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   nex u   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   nex c   sx   //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> e   nex c   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> e   nex u   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> e   nex c   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> e   nex u   dx   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex g   g    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex g   g    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex g   g    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex g   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex d   dx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex s   sx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex d   dx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex s   sx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex u   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex c   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex u   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex c   cx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex s   sx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex d   dx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex s   sx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex d   dx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex c   cx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex u   ux   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex c   cx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex u   ux   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> e   nex b   bx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> e   nex b   bx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> e   nex b   bx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> e   nex b   bx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> e   nex u   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> e   nex c   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> e   nex c   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> e   nex u   sx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex d   sx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> e   nex s   dx   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> e   nex s   dx   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> e   nex d   sx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> e   nex u   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> e   nex c   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> e   nex c   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> e   nex u   cx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> e   nex u   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> e   nex c   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> e   nex u   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> e   nex c   bx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex u   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   nex c   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   nex u   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   nex c   sx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> e   nex c   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> e   nex u   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> e   nex c   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> e   nex u   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> e   nex u   sx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> e   nex c   dx   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> e   nex c   dx   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> e   nex u   sx   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> e   nex b   dx   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> e   nex b   sx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> e   nex b   dx   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> e   nex b   sx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> e   nex u   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> e   nex c   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> e   nex u   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> e   nex c   sx   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> e   nex dx  dx   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> e   nex sx  sx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> e   nex dx  dx   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> e   nex sx  sx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> e   nex dx  sx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> e   nex dx  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> e   nex dx  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> e   nex dx  sx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> e   nex dx  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> e   nex sx  cx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> e   nex dx  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> e   nex ux  sx   //
      combination_pdf[2] = {-1,  -4,  -2};   // ux  cx   -> e   nex ux  sx   //
      combination_pdf[3] = {-1,  -2,  -4};   // cx  ux   -> e   nex dx  cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> e   nex dx  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> e   nex sx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> e   nex dx  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> e   nex sx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
