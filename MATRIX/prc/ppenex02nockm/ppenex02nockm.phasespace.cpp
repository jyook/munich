#include "header.hpp"

#include "ppenex02nockm.phasespace.set.hpp"

ppenex02nockm_phasespace_set::~ppenex02nockm_phasespace_set(){
  static Logger logger("ppenex02nockm_phasespace_set::~ppenex02nockm_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::optimize_minv_born(){
  static Logger logger("ppenex02nockm_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02nockm_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02nockm_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_020_dux_emvex(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppenex02nockm_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_020_dux_emvex(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppenex02nockm_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_020_dux_emvex(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::optimize_minv_real(){
  static Logger logger("ppenex02nockm_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02nockm_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 7;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02nockm_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_120_gd_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_120_gux_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_120_dux_emvexg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_030_da_emvexu(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_030_uxa_emvexdx(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_030_dux_emvexa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppenex02nockm_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_120_gd_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_120_gux_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_120_dux_emvexg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_030_da_emvexu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_030_uxa_emvexdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_030_dux_emvexa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppenex02nockm_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_120_gd_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_120_gux_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_120_dux_emvexg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_030_da_emvexu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_030_uxa_emvexdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_030_dux_emvexa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppenex02nockm_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppenex02nockm_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 28){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 30){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppenex02nockm_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppenex02nockm_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_220_gg_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_220_gd_emvexgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_220_gux_emvexgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_220_dd_emvexdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_220_du_emvexuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_220_ds_emvexdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_220_dc_emvexuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_220_db_emvexub(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_220_ddx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_220_ddx_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_220_dux_emvexgg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_220_dux_emvexddx(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_220_dux_emvexuux(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_220_dux_emvexssx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_220_dux_emvexccx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_220_dux_emvexbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_220_dsx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_220_dcx_emvexdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_220_dcx_emvexucx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_220_dbx_emvexubx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_220_uux_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_220_uux_emvexcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_220_ucx_emvexusx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_220_bux_emvexbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_220_bbx_emvexudx(x_a);}
    else if (csi->no_process_parton[x_a] == 28){ax_psp_220_dxux_emvexdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_220_dxcx_emvexdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 30){ax_psp_220_uxux_emvexdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_220_uxcx_emvexdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_220_uxbx_emvexdxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppenex02nockm_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_220_gg_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_220_gd_emvexgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_220_gux_emvexgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_220_dd_emvexdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_220_du_emvexuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_220_ds_emvexdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_220_dc_emvexuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_220_db_emvexub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_220_ddx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_220_ddx_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_220_dux_emvexgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_220_dux_emvexddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_220_dux_emvexuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_220_dux_emvexssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_220_dux_emvexccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_220_dux_emvexbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_220_dsx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_220_dcx_emvexdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_220_dcx_emvexucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_220_dbx_emvexubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_220_uux_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_220_uux_emvexcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_220_ucx_emvexusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_220_bux_emvexbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_220_bbx_emvexudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 28){ac_psp_220_dxux_emvexdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_220_dxcx_emvexdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 30){ac_psp_220_uxux_emvexdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_220_uxcx_emvexdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_220_uxbx_emvexdxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppenex02nockm_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_220_gg_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_220_gd_emvexgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_220_gux_emvexgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_220_dd_emvexdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_220_du_emvexuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_220_ds_emvexdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_220_dc_emvexuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_220_db_emvexub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_220_ddx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_220_ddx_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_220_dux_emvexgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_220_dux_emvexddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_220_dux_emvexuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_220_dux_emvexssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_220_dux_emvexccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_220_dux_emvexbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_220_dsx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_220_dcx_emvexdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_220_dcx_emvexucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_220_dbx_emvexubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_220_uux_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_220_uux_emvexcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_220_ucx_emvexusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_220_bux_emvexbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_220_bbx_emvexudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 28){ag_psp_220_dxux_emvexdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_220_dxcx_emvexdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 30){ag_psp_220_uxux_emvexdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_220_uxcx_emvexdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_220_uxbx_emvexdxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
