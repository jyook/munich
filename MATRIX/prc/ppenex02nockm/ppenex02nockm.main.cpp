#include "header.hpp"

#include "ppll02.amplitude.set.hpp"

#include "ppenex02nockm.contribution.set.hpp"
#include "ppenex02nockm.event.set.hpp"
#include "ppenex02nockm.phasespace.set.hpp"
#include "ppenex02nockm.observable.set.hpp"
#include "ppenex02nockm.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emve~+X");

  MUC->csi = new ppenex02nockm_contribution_set();
  MUC->esi = new ppenex02nockm_event_set();
  MUC->psi = new ppenex02nockm_phasespace_set();
  MUC->osi = new ppenex02nockm_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppll02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppenex02nockm_phasespace_set();
      MUC->psi->fake_psi->csi = new ppenex02nockm_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppenex02nockm_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
