#include "header.hpp"

#include "ppenex02nockm.summary.hpp"

ppenex02nockm_summary_generic::ppenex02nockm_summary_generic(munich * xmunich){
  Logger logger("ppenex02nockm_summary_generic::ppenex02nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppenex02nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppenex02nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppenex02nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppenex02nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppenex02nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppenex02nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_emve~";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gu~_emve~d~";
    subprocess[3] = "du~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "u~a_emve~d~";
    subprocess[3] = "du~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gu~_emve~d~";
    subprocess[3] = "du~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "u~a_emve~d~";
    subprocess[3] = "du~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_emve~u";
    subprocess[2] = "gu~_emve~d~";
    subprocess[3] = "du~_emve~g";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "da_emve~u";
    subprocess[2] = "u~a_emve~d~";
    subprocess[3] = "du~_emve~a";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppenex02nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppenex02nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(31);
    subprocess[1] = "gg_emve~ud~";
    subprocess[2] = "gd_emve~gu";
    subprocess[3] = "gu~_emve~gd~";
    subprocess[4] = "dd_emve~du";
    subprocess[5] = "du_emve~uu";
    subprocess[6] = "ds_emve~dc";
    subprocess[7] = "dc_emve~uc";
    subprocess[8] = "db_emve~ub";
    subprocess[9] = "dd~_emve~ud~";
    subprocess[10] = "dd~_emve~cs~";
    subprocess[11] = "du~_emve~gg";
    subprocess[12] = "du~_emve~dd~";
    subprocess[13] = "du~_emve~uu~";
    subprocess[14] = "du~_emve~ss~";
    subprocess[15] = "du~_emve~cc~";
    subprocess[16] = "du~_emve~bb~";
    subprocess[17] = "ds~_emve~us~";
    subprocess[18] = "dc~_emve~ds~";
    subprocess[19] = "dc~_emve~uc~";
    subprocess[20] = "db~_emve~ub~";
    subprocess[21] = "uu~_emve~ud~";
    subprocess[22] = "uu~_emve~cs~";
    subprocess[23] = "uc~_emve~us~";
    subprocess[24] = "bu~_emve~bd~";
    subprocess[25] = "bb~_emve~ud~";
    subprocess[26] = "d~u~_emve~d~d~";
    subprocess[27] = "d~c~_emve~d~s~";
    subprocess[28] = "u~u~_emve~d~u~";
    subprocess[29] = "u~c~_emve~d~c~";
    subprocess[30] = "u~b~_emve~d~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
