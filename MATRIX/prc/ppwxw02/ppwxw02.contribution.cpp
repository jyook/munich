#include "header.hpp"

#include "ppwxw02.contribution.set.hpp"

ppwxw02_contribution_set::~ppwxw02_contribution_set(){
  static Logger logger("ppwxw02_contribution_set::~ppwxw02_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppwxw02_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 6; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppwxw02_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> wx  w    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  w   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppwxw02_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  22)){no_process_parton[i_a] = 24; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   1)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   3)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   5)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -1)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -3)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -2)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==  -5)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[5] ==   0)){no_process_parton[i_a] = 16; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppwxw02_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> wx  w   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> wx  w   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> wx  w   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> wx  w   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  w   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> wx  w   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> wx  w   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> wx  w   c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> wx  w   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> wx  w   b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  w   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> wx  w   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> wx  w   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> wx  w   sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> wx  w   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> wx  w   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> wx  w   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> wx  w   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  w   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  w   bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w   g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> wx  w   d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> wx  w   s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> wx  w   d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> wx  w   s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> wx  w   u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> wx  w   c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> wx  w   u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> wx  w   c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> wx  w   b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> wx  w   b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> wx  w   dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> wx  w   sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> wx  w   dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> wx  w   sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> wx  w   ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> wx  w   cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> wx  w   ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> wx  w   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> wx  w   bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> wx  w   bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w   a    //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   7,   7};   // a   a    -> wx  w   a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  w   g   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> wx  w   d   //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> wx  w   s   //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> wx  w   d   //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> wx  w   s   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  w   u   //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> wx  w   c   //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> wx  w   u   //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> wx  w   c   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> wx  w   b   //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> wx  w   b   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  w   dx  //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> wx  w   sx  //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> wx  w   dx  //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> wx  w   sx  //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> wx  w   ux  //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> wx  w   cx  //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> wx  w   ux  //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> wx  w   cx  //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  w   bx  //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  w   bx  //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   g   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   g   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   g   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   g   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   g   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   g   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   g   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   g   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   g   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w   g   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppwxw02_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] == -24) && (tp[4] ==  24) && (tp[out[5]] ==  -5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 46){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppwxw02_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  w   d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> wx  w   s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  w   u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> wx  w   c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> wx  w   b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> wx  w   g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> wx  w   g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> wx  w   g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> wx  w   g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> wx  w   g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> wx  w   g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> wx  w   g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> wx  w   g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> wx  w   g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> wx  w   g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> wx  w   g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> wx  w   g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> wx  w   g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> wx  w   g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> wx  w   g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> wx  w   g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> wx  w   g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> wx  w   g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> wx  w   g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> wx  w   g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> wx  w   d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> wx  w   s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> wx  w   d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> wx  w   s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> wx  w   d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> wx  w   s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> wx  w   d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> wx  w   d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  w   d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> wx  w   u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> wx  w   u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> wx  w   d   c    //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> wx  w   u   s    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> wx  w   d   c    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> wx  w   d   c    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> wx  w   u   s    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> wx  w   d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> wx  w   s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> wx  w   d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> wx  w   s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> wx  w   b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> wx  w   b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> wx  w   b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> wx  w   b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> wx  w   d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> wx  w   s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> wx  w   d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> wx  w   s   cx   //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> wx  w   s   cx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> wx  w   d   ux   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> wx  w   s   cx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> wx  w   d   ux   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> wx  w   d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> wx  w   s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> wx  w   s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> wx  w   d   sx   //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> wx  w   u   cx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> wx  w   c   ux   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> wx  w   c   ux   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> wx  w   u   cx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> wx  w   d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> wx  w   s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> wx  w   s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> wx  w   d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> wx  w   d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> wx  w   s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> wx  w   d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> wx  w   s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> wx  w   u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> wx  w   c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> wx  w   u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> wx  w   u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> wx  w   u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> wx  w   c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> wx  w   u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> wx  w   c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  w   u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  w   c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  w   u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  w   c   sx   //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> wx  w   c   sx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> wx  w   u   dx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> wx  w   c   sx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> wx  w   u   dx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> wx  w   b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> wx  w   b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> wx  w   b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> wx  w   b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> wx  w   u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> wx  w   c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> wx  w   c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> wx  w   u   sx   //
    }
    else if (no_process_parton[i_a] == 46){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  w   d   sx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> wx  w   s   dx   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> wx  w   s   dx   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> wx  w   d   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> wx  w   u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> wx  w   c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> wx  w   c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> wx  w   u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> wx  w   u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> wx  w   c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> wx  w   u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> wx  w   c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> wx  w   b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> wx  w   b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> wx  w   b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> wx  w   b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> wx  w   b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> wx  w   b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> wx  w   b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> wx  w   b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> wx  w   b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w   g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> wx  w   s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> wx  w   d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> wx  w   s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> wx  w   c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> wx  w   u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> wx  w   c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> wx  w   b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> wx  w   b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> wx  w   dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> wx  w   sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> wx  w   dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> wx  w   sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> wx  w   dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> wx  w   sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> wx  w   dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> wx  w   dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> wx  w   dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> wx  w   ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> wx  w   ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> wx  w   dx  cx   //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> wx  w   ux  sx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> wx  w   dx  cx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> wx  w   dx  cx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> wx  w   ux  sx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> wx  w   dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> wx  w   sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> wx  w   dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> wx  w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> wx  w   ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> wx  w   cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> wx  w   ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> wx  w   ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> wx  w   ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> wx  w   cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> wx  w   ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> wx  w   cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> wx  w   bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
