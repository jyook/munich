#include "header.hpp"

#include "ppww02.amplitude.set.hpp"

#include "ppwxw02.contribution.set.hpp"
#include "ppwxw02.event.set.hpp"
#include "ppwxw02.phasespace.set.hpp"
#include "ppwxw02.observable.set.hpp"
#include "ppwxw02.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-wpwm+X");

  MUC->csi = new ppwxw02_contribution_set();
  MUC->esi = new ppwxw02_event_set();
  MUC->psi = new ppwxw02_phasespace_set();
  MUC->osi = new ppwxw02_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppww02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppwxw02_phasespace_set();
      MUC->psi->fake_psi->csi = new ppwxw02_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppwxw02_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
