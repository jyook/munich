{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("wp")[0].momentum + PARTICLE("wm")[0].momentum).m();
    // takes sum of momenta of W+ boson PARTICLE("wp")[0].momentum and W- boson PARTICLE("wm")[1].momentum
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("wp")[0].momentum + PARTICLE("wm")[1].momentum).m();
    double pT = (PARTICLE("wp")[0].momentum + PARTICLE("wm")[1].momentum).pT();
    temp_mu_central = sqrt(pow(m,2)+pow(pT,2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
