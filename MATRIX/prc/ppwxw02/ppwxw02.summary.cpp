#include "header.hpp"

#include "ppwxw02.summary.hpp"

ppwxw02_summary_generic::ppwxw02_summary_generic(munich * xmunich){
  Logger logger("ppwxw02_summary_generic::ppwxw02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppwxw02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppwxw02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppwxw02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppwxw02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppwxw02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppwxw02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_born(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subprocess[4] = "aa_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subprocess[4] = "aa_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subprocess[4] = "aa_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_wpwm";
    subprocess[2] = "uu~_wpwm";
    subprocess[3] = "bb~_wpwm";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_wpwm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_wpwmd";
    subprocess[2] = "gu_wpwmu";
    subprocess[3] = "gb_wpwmb";
    subprocess[4] = "gd~_wpwmd~";
    subprocess[5] = "gu~_wpwmu~";
    subprocess[6] = "gb~_wpwmb~";
    subprocess[7] = "dd~_wpwmg";
    subprocess[8] = "uu~_wpwmg";
    subprocess[9] = "bb~_wpwmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_wpwmg";
    subprocess[2] = "gd_wpwmd";
    subprocess[3] = "gu_wpwmu";
    subprocess[4] = "gb_wpwmb";
    subprocess[5] = "gd~_wpwmd~";
    subprocess[6] = "gu~_wpwmu~";
    subprocess[7] = "gb~_wpwmb~";
    subprocess[8] = "dd~_wpwmg";
    subprocess[9] = "uu~_wpwmg";
    subprocess[10] = "bb~_wpwmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_wpwmd";
    subprocess[2] = "ua_wpwmu";
    subprocess[3] = "ba_wpwmb";
    subprocess[4] = "d~a_wpwmd~";
    subprocess[5] = "u~a_wpwmu~";
    subprocess[6] = "b~a_wpwmb~";
    subprocess[7] = "dd~_wpwma";
    subprocess[8] = "uu~_wpwma";
    subprocess[9] = "bb~_wpwma";
    subprocess[10] = "aa_wpwma";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_wpwmd";
    subprocess[2] = "gu_wpwmu";
    subprocess[3] = "gb_wpwmb";
    subprocess[4] = "gd~_wpwmd~";
    subprocess[5] = "gu~_wpwmu~";
    subprocess[6] = "gb~_wpwmb~";
    subprocess[7] = "dd~_wpwmg";
    subprocess[8] = "uu~_wpwmg";
    subprocess[9] = "bb~_wpwmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_wpwmd";
    subprocess[2] = "ua_wpwmu";
    subprocess[3] = "ba_wpwmb";
    subprocess[4] = "d~a_wpwmd~";
    subprocess[5] = "u~a_wpwmu~";
    subprocess[6] = "b~a_wpwmb~";
    subprocess[7] = "dd~_wpwma";
    subprocess[8] = "uu~_wpwma";
    subprocess[9] = "bb~_wpwma";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_wpwmd";
    subprocess[2] = "gu_wpwmu";
    subprocess[3] = "gb_wpwmb";
    subprocess[4] = "gd~_wpwmd~";
    subprocess[5] = "gu~_wpwmu~";
    subprocess[6] = "gb~_wpwmb~";
    subprocess[7] = "dd~_wpwmg";
    subprocess[8] = "uu~_wpwmg";
    subprocess[9] = "bb~_wpwmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_wpwmd";
    subprocess[2] = "ua_wpwmu";
    subprocess[3] = "ba_wpwmb";
    subprocess[4] = "d~a_wpwmd~";
    subprocess[5] = "u~a_wpwmu~";
    subprocess[6] = "b~a_wpwmb~";
    subprocess[7] = "dd~_wpwma";
    subprocess[8] = "uu~_wpwma";
    subprocess[9] = "bb~_wpwma";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppwxw02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppwxw02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(60);
    subprocess[1] = "gg_wpwmdd~";
    subprocess[2] = "gg_wpwmuu~";
    subprocess[3] = "gg_wpwmbb~";
    subprocess[4] = "gd_wpwmgd";
    subprocess[5] = "gu_wpwmgu";
    subprocess[6] = "gb_wpwmgb";
    subprocess[7] = "gd~_wpwmgd~";
    subprocess[8] = "gu~_wpwmgu~";
    subprocess[9] = "gb~_wpwmgb~";
    subprocess[10] = "dd_wpwmdd";
    subprocess[11] = "du_wpwmdu";
    subprocess[12] = "ds_wpwmds";
    subprocess[13] = "dc_wpwmdc";
    subprocess[14] = "dc_wpwmus";
    subprocess[15] = "db_wpwmdb";
    subprocess[16] = "dd~_wpwmgg";
    subprocess[17] = "dd~_wpwmdd~";
    subprocess[18] = "dd~_wpwmuu~";
    subprocess[19] = "dd~_wpwmss~";
    subprocess[20] = "dd~_wpwmcc~";
    subprocess[21] = "dd~_wpwmbb~";
    subprocess[22] = "du~_wpwmdu~";
    subprocess[23] = "du~_wpwmsc~";
    subprocess[24] = "ds~_wpwmds~";
    subprocess[25] = "ds~_wpwmuc~";
    subprocess[26] = "dc~_wpwmdc~";
    subprocess[27] = "db~_wpwmdb~";
    subprocess[28] = "uu_wpwmuu";
    subprocess[29] = "uc_wpwmuc";
    subprocess[30] = "ub_wpwmub";
    subprocess[31] = "ud~_wpwmud~";
    subprocess[32] = "ud~_wpwmcs~";
    subprocess[33] = "uu~_wpwmgg";
    subprocess[34] = "uu~_wpwmdd~";
    subprocess[35] = "uu~_wpwmuu~";
    subprocess[36] = "uu~_wpwmss~";
    subprocess[37] = "uu~_wpwmcc~";
    subprocess[38] = "uu~_wpwmbb~";
    subprocess[39] = "us~_wpwmus~";
    subprocess[40] = "uc~_wpwmds~";
    subprocess[41] = "uc~_wpwmuc~";
    subprocess[42] = "ub~_wpwmub~";
    subprocess[43] = "bb_wpwmbb";
    subprocess[44] = "bd~_wpwmbd~";
    subprocess[45] = "bu~_wpwmbu~";
    subprocess[46] = "bb~_wpwmgg";
    subprocess[47] = "bb~_wpwmdd~";
    subprocess[48] = "bb~_wpwmuu~";
    subprocess[49] = "bb~_wpwmbb~";
    subprocess[50] = "d~d~_wpwmd~d~";
    subprocess[51] = "d~u~_wpwmd~u~";
    subprocess[52] = "d~s~_wpwmd~s~";
    subprocess[53] = "d~c~_wpwmd~c~";
    subprocess[54] = "d~c~_wpwmu~s~";
    subprocess[55] = "d~b~_wpwmd~b~";
    subprocess[56] = "u~u~_wpwmu~u~";
    subprocess[57] = "u~c~_wpwmu~c~";
    subprocess[58] = "u~b~_wpwmu~b~";
    subprocess[59] = "b~b~_wpwmb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
