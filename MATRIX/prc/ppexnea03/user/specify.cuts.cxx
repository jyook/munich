logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_M_lepgam = USERSWITCH("M_lepgam");
  static double cut_min_M_lepgam = USERCUT("min_M_lepgam");
  static double cut_min_M2_lepgam = pow(cut_min_M_lepgam, 2);
  static double cut_max_M_lepgam = USERCUT("max_M_lepgam");
  static double cut_max_M2_lepgam = pow(cut_max_M_lepgam, 2);
  
  static int switch_cut_R_lepgam = USERSWITCH("R_lepgam");
  static double cut_min_R_lepgam = USERCUT("min_R_lepgam");
  static double cut_min_R2_lepgam = pow(cut_min_R_lepgam, 2);

  static int switch_cut_R_lepjet = USERSWITCH("R_lepjet");
  static double cut_min_R_lepjet = USERCUT("min_R_lepjet");
  static double cut_min_R2_lepjet = pow(cut_min_R_lepjet, 2);

  static int switch_cut_R_gamjet = USERSWITCH("R_gamjet");
  static double cut_min_R_gamjet = USERCUT("min_R_gamjet");
  static double cut_min_R2_gamjet = pow(cut_min_R_gamjet, 2);

  static int switch_cut_mT_CMS = USERSWITCH("mT_CMS");
  static double cut_min_mT_CMS = USERCUT("min_mT_CMS");
  static double cut_min_mT2_CMS = pow(cut_min_mT_CMS, 2);

  static int switch_cut_gap_eta_gam = USERSWITCH("gap_eta_gam");
  static double cut_gap_min_eta_gam = USERCUT("gap_min_eta_gam");
  static double cut_gap_max_eta_gam = USERCUT("gap_max_eta_gam");



  // lepton--photon invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_lepgam = " << switch_cut_M_lepgam << endl;}
  if (switch_cut_M_lepgam == 1){
     for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
       double M2_lepgam = (PARTICLE("lep")[i_l].momentum + PARTICLE("photon")[0].momentum).m2();
       
       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[0] = " << PARTICLE("photon")[0].momentum << endl;}
       if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_lepgam = " << M2_lepgam << " > " << cut_min_M2_lepgam << endl;}
       
       if (M2_lepgam < cut_min_M2_lepgam){
	 cut_ps[i_a] = -1; 
	 if (switch_output_cutinfo){
	   info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_lepgam" << endl;
	   logger << LOG_DEBUG << endl << info_cut.str();
	 }
	 logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lepgam min cut applied" << endl; 
	 return;
       }
       
       if (switch_output_cutinfo){if (cut_max_M2_lepgam != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_lepgam = " << M2_lepgam << " < " << cut_max_M2_lepgam << endl;}}
       
       if (cut_max_M2_lepgam != 0 && M2_lepgam > cut_max_M2_lepgam){
	 cut_ps[i_a] = -1; 
	 if (switch_output_cutinfo){
	   info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_M_lepgam" << endl; 
	   logger << LOG_DEBUG << endl << info_cut.str(); 
	 }
	 logger << LOG_DEBUG_VERBOSE << "switch_cut_M_lepgam max cut applied" << endl; 
	 return;
       }
    }
  }



  // lepton--photon isolation cuts
  if (switch_cut_R_lepgam == 1){
    for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
      if (R2_eta(PARTICLE("lep")[i_l], PARTICLE("photon")[0]) < cut_min_R2_lepgam){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepgam cut applied" << endl; return;}
    }
  }



  for (int i_j = 0; i_j < NUMBER("jet"); i_j++){
    // lepton--jet isolation cuts
    if (switch_cut_R_lepjet == 1){
      for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
	if (R2_eta(PARTICLE("lep")[i_l], PARTICLE("jet")[i_j]) < cut_min_R2_lepjet){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepjet cut applied" << endl; return;}
      }
    }


    // photon--jet isolation cuts
    if (switch_cut_R_gamjet == 1){
      if (R2_eta(PARTICLE("photon")[0], PARTICLE("jet")[i_j]) < cut_min_R2_gamjet){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_R_gamjet cut applied" << endl; return;}
    }
  }



  // mT_CMS cut
  if (switch_cut_mT_CMS == 1){
    double temp_dphi = abs(PARTICLE("nua")[0].phi - PARTICLE("lep")[0].phi);
    if (temp_dphi > pi){temp_dphi = 2 * pi - temp_dphi;}
    double temp_mT2_CMS = 2 * PARTICLE("lep")[0].pT * PARTICLE("nua")[0].pT * (1. - cos(temp_dphi));
    if (temp_mT2_CMS < cut_min_mT2_CMS){cut_ps[i_a] = -1; logger << LOG_DEBUG_VERBOSE << "switch_cut_mT_CMS cut applied" << endl; return;}
  }



  // (detector-geometry-motivated) gap in the photon-pseudo-rapidity acceptance (eta_gam < gap_min_eta_gam or eta_gam > gap_max_gam)
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_gap_eta_gam = " << switch_cut_gap_eta_gam << endl;}
  if (switch_cut_gap_eta_gam){
    for (int i_l = 0; i_l < PARTICLE("photon").size(); i_l++){
      double abs_eta_gam = abs(PARTICLE("photon")[i_l].eta);
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(photon)[" << i_l << "] = " << PARTICLE("photon")[i_l].momentum << endl;}
      if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   |eta_gam| = " << abs_eta_gam << " < " << cut_gap_min_eta_gam << "   or   |eta_gam| = " << abs_eta_gam << " > " << cut_gap_max_eta_gam << endl;}

      if (abs_eta_gam > cut_gap_min_eta_gam && abs_eta_gam < cut_gap_max_eta_gam){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   individual cut after switch_cut_gap_eta_gam   i_l = " << i_l << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	
	logger << LOG_DEBUG_VERBOSE << "switch_cut_gap_eta_gam cut applied" << endl; 
	return;
      }
    }
  }


  if (switch_output_cutinfo){info_cut << "process-specific cuts passed" << endl;}
}
logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx ended" << endl;
