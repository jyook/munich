#include "header.hpp"

#include "ppexnea03.summary.hpp"

ppexnea03_summary_generic::ppexnea03_summary_generic(munich * xmunich){
  Logger logger("ppexnea03_summary_generic::ppexnea03_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexnea03_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexnea03_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexnea03_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexnea03_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexnea03_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexnea03_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_born(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epvea";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvead";
    subprocess[2] = "gd~_epveau~";
    subprocess[3] = "ud~_epveag";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvead";
    subprocess[2] = "d~a_epveau~";
    subprocess[3] = "ud~_epveaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvead";
    subprocess[2] = "gd~_epveau~";
    subprocess[3] = "ud~_epveag";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvead";
    subprocess[2] = "d~a_epveau~";
    subprocess[3] = "ud~_epveaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvead";
    subprocess[2] = "gd~_epveau~";
    subprocess[3] = "ud~_epveag";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvead";
    subprocess[2] = "d~a_epveau~";
    subprocess[3] = "ud~_epveaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexnea03_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexnea03_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_epveadu~";
    subprocess[2] = "gu_epveagd";
    subprocess[3] = "gd~_epveagu~";
    subprocess[4] = "du_epveadd";
    subprocess[5] = "dc_epveads";
    subprocess[6] = "dd~_epveadu~";
    subprocess[7] = "dd~_epveasc~";
    subprocess[8] = "ds~_epveadc~";
    subprocess[9] = "uu_epveadu";
    subprocess[10] = "uc_epveadc";
    subprocess[11] = "ud~_epveagg";
    subprocess[12] = "ud~_epveadd~";
    subprocess[13] = "ud~_epveauu~";
    subprocess[14] = "ud~_epveass~";
    subprocess[15] = "ud~_epveacc~";
    subprocess[16] = "uu~_epveadu~";
    subprocess[17] = "uu~_epveasc~";
    subprocess[18] = "us~_epveads~";
    subprocess[19] = "us~_epveauc~";
    subprocess[20] = "uc~_epveadc~";
    subprocess[21] = "d~d~_epvead~u~";
    subprocess[22] = "d~u~_epveau~u~";
    subprocess[23] = "d~s~_epvead~c~";
    subprocess[24] = "d~c~_epveau~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
