{
  static int switch_cut_M_Zrec = user->switch_value[user->switch_map["M_Zrec"]];
  static double cut_min_M_Zrec = user->cut_value[user->cut_map["min_M_Zrec"]];

  static int switch_cut_M_4lep = user->switch_value[user->switch_map["M_4lep"]];
  static double cut_min_M_4lep = user->cut_value[user->cut_map["min_M_4lep"]];

  static int switch_cut_delta_M_4lep = user->switch_value[user->switch_map["delta_M_4lep"]];
  static double cut_max_delta_M_4lep = user->cut_value[user->cut_map["max_delta_M_4lep"]];

  for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
    if (switch_cut_M_Zrec){
      if (sqrtsmin_opt[i_a][60] < 2 * cut_min_M_Zrec){
	sqrtsmin_opt[i_a][60] = 2 * cut_min_M_Zrec;
      }
    }
    
    if (switch_cut_M_4lep){
      if (sqrtsmin_opt[i_a][60] < cut_min_M_4lep){
	sqrtsmin_opt[i_a][60] = cut_min_M_4lep;
      }
    }
    
    if (switch_cut_delta_M_4lep){
      if (sqrtsmin_opt[i_a][60] < M[23] - cut_max_delta_M_4lep){
	sqrtsmin_opt[i_a][60] = M[23] - cut_max_delta_M_4lep;
      }
    }
  }
}
