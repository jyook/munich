logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  USERPARTICLE("<particle_name>").push_back(<particle>);

  static int switch_lepton_identification = USERSWITCH("lepton_identification");

  // in the equal-flavor case we first have to identify the leptons coming from the different Z bosons

  int x_lp_1 = 0;
  int x_lp_2 = 0;
  int x_lm_1 = 0;
  int x_lm_2 = 0;
  double min_abs_M_leplep_MZ_combination = 1.e99;
  vector<vector<double> > abs_M_leplep_MZ_combination(PARTICLE("lp").size(), vector<double> (PARTICLE("lm").size(), 0.));

  if (switch_lepton_identification){ // needed in both ATLAS and CMS prescriptions
    for (int i_lp = 0; i_lp < PARTICLE("lp").size(); i_lp++){
      for (int i_lm = 0; i_lm < PARTICLE("lm").size(); i_lm++){
	abs_M_leplep_MZ_combination[i_lp][i_lm] = abs((PARTICLE("lp")[i_lp].momentum + PARTICLE("lm")[i_lm].momentum).m() - msi->M_Z);
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   abs_M_leplep_MZ_combination[" << i_lp << "][" << i_lm << "] = " << abs_M_leplep_MZ_combination[i_lp][i_lm] << "   M_Z = " << msi->M[23] << endl;}
      }
    }
  }

  if (switch_lepton_identification == 1){ // ATLAS prescription using ... algorithm
    int i_lm = 0;
    for (int i_lp = 0; i_lp < PARTICLE("lp").size(); i_lp++){
      int j_lp = (i_lp + 1) % 2;
      int j_lm = (i_lm + 1) % 2;

      if (abs_M_leplep_MZ_combination[i_lp][i_lm] + abs_M_leplep_MZ_combination[j_lp][j_lm] < min_abs_M_leplep_MZ_combination){
	min_abs_M_leplep_MZ_combination = abs_M_leplep_MZ_combination[i_lp][i_lm] + abs_M_leplep_MZ_combination[j_lp][j_lm];
	if (abs_M_leplep_MZ_combination[i_lp][i_lm] < abs_M_leplep_MZ_combination[j_lp][j_lm]){
	  x_lp_1 = i_lp;
	  x_lm_1 = i_lm;
	  x_lp_2 = j_lp;
	  x_lm_2 = j_lm;
	}
	else {
	  x_lp_1 = j_lp;
	  x_lm_1 = j_lm;
	  x_lp_2 = i_lp;
	  x_lm_2 = i_lm;
	}
      }
    }
  }

  else if (switch_lepton_identification == 2){ // CMS prescription using ... algorithm
    for (int i_lp = 0; i_lp < PARTICLE("lp").size(); i_lp++){
      for (int i_lm = 0; i_lm < PARTICLE("lm").size(); i_lm++){
	if (abs_M_leplep_MZ_combination[i_lp][i_lm] < min_abs_M_leplep_MZ_combination){
	  min_abs_M_leplep_MZ_combination = abs_M_leplep_MZ_combination[i_lp][i_lm];
	  x_lp_1 = i_lp;
	  x_lm_1 = i_lm;
	}
      }
    }
    x_lp_2 = (x_lp_1 + 1) % 2;
    x_lm_2 = (x_lm_1 + 1) % 2;
  }
  /*
  else {
    assert(false && "ERROR: Trying to compute observable M_Z, where leptons from Z bosons must be identified, but switch lepton_identification not set to 1 or 2.");
  }
  */

  if (switch_lepton_identification){ // needed in both ATLAS and CMS prescriptions
    USERPARTICLE("Zrec").push_back(PARTICLE("lm")[x_lm_1] + PARTICLE("lp")[x_lp_1]);
    USERPARTICLE("Zrec").push_back(PARTICLE("lm")[x_lm_2] + PARTICLE("lp")[x_lp_2]);
    if ((PARTICLE("lm")[x_lm_1] + PARTICLE("lp")[x_lp_1]).pT > (PARTICLE("lm")[x_lm_2] + PARTICLE("lp")[x_lp_2]).pT){
      USERPARTICLE("lmZlead").push_back(PARTICLE("lm")[x_lm_1]);
      USERPARTICLE("lpZlead").push_back(PARTICLE("lp")[x_lp_1]);
      USERPARTICLE("lmZsub").push_back(PARTICLE("lm")[x_lm_2]);
      USERPARTICLE("lpZsub").push_back(PARTICLE("lp")[x_lp_2]);
    }
    else {
      USERPARTICLE("lmZsub").push_back(PARTICLE("lm")[x_lm_1]);
      USERPARTICLE("lpZsub").push_back(PARTICLE("lp")[x_lp_1]);
      USERPARTICLE("lmZlead").push_back(PARTICLE("lm")[x_lm_2]);
      USERPARTICLE("lpZlead").push_back(PARTICLE("lp")[x_lp_2]);
    }
    
    // does order into "closer" and "further":
    USERPARTICLE("Z1rec").push_back(PARTICLE("lm")[x_lm_1] + PARTICLE("lp")[x_lp_1]);
    USERPARTICLE("Z2rec").push_back(PARTICLE("lm")[x_lm_2] + PARTICLE("lp")[x_lp_2]);
    USERPARTICLE("lmZ1").push_back(PARTICLE("lm")[x_lm_1]);
    USERPARTICLE("lmZ2").push_back(PARTICLE("lm")[x_lm_2]);
    USERPARTICLE("lpZ1").push_back(PARTICLE("lp")[x_lp_1]);
    USERPARTICLE("lpZ2").push_back(PARTICLE("lp")[x_lp_2]);
  }
}

logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
