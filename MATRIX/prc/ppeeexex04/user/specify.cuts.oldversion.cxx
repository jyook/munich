{
  //  static int switch_info_cut = 1;
  static stringstream info_cut;
  if (osi_switch_output_cutinfo){
    info_cut.clear();
    info_cut.str("");
    for (int i_o = 0; i_o < osi_relevant_object_list.size(); i_o++){
      int j_o = osi_equivalent_no_object[i_o]; 
      // i_o -> number in relevant_object_list
      // j_o -> number in object_list
      info_cut << "[" << setw(2) << i_a << "]   osi_relevant_object_list[" << setw(2) << i_o << "] = " << setw(6) << osi_relevant_object_list[i_o] << "   osi_n_object[" << setw(2) << j_o << "][" << setw(2) << i_a << "] = " << osi_n_object[j_o][i_a] << "   osi_particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[j_o][i_a].size() << endl;
      for (int i_p = 0; i_p < osi_n_object[j_o][i_a]; i_p++){
      info_cut << "       osi_particle_event[" << setw(2) << j_o << "][" << setw(2) << i_a << "][" << setw(2) << i_p << "] = " << osi_particle_event[j_o][i_a][i_p].momentum << endl;
      }
    }
  }
  
  static int switch_cut_M_leplep = osi_user_switch_value[osi_user_switch_map["M_leplep"]];
  static int switch_cut_M_leplep_ATLAS = osi_user_switch_value[osi_user_switch_map["M_leplep_ATLAS"]];
  static int switch_cut_M_leplep_CMS = osi_user_switch_value[osi_user_switch_map["M_leplep_CMS"]];
  static double cut_min_M_leplep = osi_user_cut_value[osi_user_cut_map["min_M_leplep"]];
  static double cut_min_M2_leplep = pow(cut_min_M_leplep, 2);
  static double cut_max_M_leplep = osi_user_cut_value[osi_user_cut_map["max_M_leplep"]];
  static double cut_max_M2_leplep = pow(cut_max_M_leplep, 2);
  static double cut_min_M_leplep_IR = osi_user_cut_value[osi_user_cut_map["min_M_leplep_IR"]];

  static double cut_min_M_12 = osi_user_cut_value[osi_user_cut_map["min_M_12"]];
  static double cut_min_M2_12 = pow(cut_min_M_12, 2);
  static double cut_max_M_12 = osi_user_cut_value[osi_user_cut_map["max_M_12"]];
  static double cut_max_M2_12 = pow(cut_max_M_12, 2);
 
  static int switch_cut_M_4lep = osi_user_switch_value[osi_user_switch_map["M_4lep"]];
  static double cut_min_delta_M_4lep = osi_user_cut_value[osi_user_cut_map["min_delta_M_4lep"]];
  static double cut_max_delta_M_4lep = osi_user_cut_value[osi_user_cut_map["max_delta_M_4lep"]];
  static double cut_min_M_4lep = osi_user_cut_value[osi_user_cut_map["min_M_4lep"]];
  static double cut_min_M2_4lep = pow(cut_min_M_4lep, 2);
  static double cut_max_M_4lep = osi_user_cut_value[osi_user_cut_map["max_M_4lep"]];
  static double cut_max_M2_4lep = pow(cut_max_M_4lep, 2);

  static int switch_cut_R_leplep = osi_user_switch_value[osi_user_switch_map["R_leplep"]];
  static double cut_min_R_leplep = osi_user_cut_value[osi_user_cut_map["min_R_leplep"]];
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);

  static int switch_cut_pT_lep_1st = osi_user_switch_value[osi_user_switch_map["pT_lep_1st"]];
  static double cut_min_pT_lep_1st = osi_user_cut_value[osi_user_cut_map["min_pT_lep_1st"]];

  static int switch_cut_pT_lep_2nd = osi_user_switch_value[osi_user_switch_map["pT_lep_2nd"]];
  static double cut_min_pT_lep_2nd = osi_user_cut_value[osi_user_cut_map["min_pT_lep_2nd"]];

  static int switch_cut_lep_iso = osi_user_switch_value[osi_user_switch_map["lep_iso"]];
  static double cut_R_lep_iso = osi_user_cut_value[osi_user_cut_map["lep_iso_delta_0"]];
  static double cut_R2_lep_iso = pow(cut_R_lep_iso, 2);
  static double cut_threshold_lep_iso = osi_user_cut_value[osi_user_cut_map["lep_iso_epsilon"]];

  // lepton isolation: [pT sum of all particles except neutrinos in cone] / pT < threshold
  if (switch_cut_lep_iso) {
    // collect final-state partons
    vector<particle> particle_protojet;

    for (int i_l = 0; i_l < osi_ps_runtime_jet_algorithm[i_a].size(); i_l++){
      int i_p = osi_ps_runtime_jet_algorithm[i_a][i_l];
      particle_protojet.push_back(osi_particle_event[0][i_a][i_p]);
    }

    for (int i = 0; i < osi_particle_event[osi_access_object["lep"]][i_a].size(); i++) {
      double pT_sum=0;
      // leptons
      for (int j = 0; j < osi_particle_event[osi_access_object["lep"]][i_a].size(); j++) {
        if (i==j)
          continue;
        double R2_eta_leplep = R2_eta(osi_particle_event[osi_access_object["lep"]][i_a][i], osi_particle_event[osi_access_object["lep"]][i_a][j]);
        if (R2_eta_leplep < cut_R2_lep_iso) {
          pT_sum += osi_particle_event[osi_access_object["lep"]][i_a][j].pT;
        }
      }
      // partons
      for (int j = 0; j < particle_protojet.size(); j++) {
        double R2_eta_lepjet = R2_eta(osi_particle_event[osi_access_object["lep"]][i_a][i], particle_protojet[j]);
        if (R2_eta_lepjet < cut_R2_lep_iso){
          pT_sum += particle_protojet[j].pT;
        }
      }

      if (pT_sum/osi_particle_event[osi_access_object["lep"]][i_a][i].pT > cut_threshold_lep_iso) {
        osi_cut_ps[i_a] = -1;
        return;
      }
    }
  }


  if (switch_cut_pT_lep_1st == 1){
    double temp_pT_lep_1st = osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][0].pT;
    if (temp_pT_lep_1st < cut_min_pT_lep_1st){
      osi_cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_1st << " < cut_min_pT_lep_1st = " << cut_min_pT_lep_1st << endl;
      return;
    }
  }
 
  if (switch_cut_pT_lep_2nd == 1){
    double temp_pT_lep_2nd = osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][1].pT;
    if (temp_pT_lep_2nd < cut_min_pT_lep_2nd){
      osi_cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_2nd << " < cut_min_pT_lep_2nd = " << cut_min_pT_lep_2nd << endl;
      return;
    }
  }
 


  // lepton lepton separation cut (to be applied to *any* two leptons)
  if (switch_cut_R_leplep) {
    for (int i_l1 = 0; i_l1 < osi_particle_event[osi_access_object["lep"]][i_a].size(); i_l1++) {
      for (int i_l2 = i_l1 + 1; i_l2 < osi_particle_event[osi_access_object["lep"]][i_a].size(); i_l2++) {
        double R2_eta_leplep = R2_eta(osi_particle_event[osi_access_object["lep"]][i_a][i_l1], osi_particle_event[osi_access_object["lep"]][i_a][i_l2]);
	if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_lep[" << i_l1 << "]lep[" << i_l2 << "] = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}
        if (R2_eta_leplep < cut_min_R2_leplep) {
          osi_cut_ps[i_a] = -1;
	  if (osi_switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_R_leplep" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
          return;
        }
      }
    }
  }

  // lepton invariant-mass cut
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep = " << switch_cut_M_leplep << endl;}
  if (switch_cut_M_leplep == 1){
    double M2_leplep1;
    double M2_leplep2;
    int x_lp_1;
    int x_lp_2;
    int x_lm_1;
    int x_lm_2;
    double min_abs_M_leplep_MZ_combination = 1.e99;
    vector<vector<double> > M_leplep_MZ_combination(osi_particle_event[osi_access_object["lp"]][i_a].size(), vector<double> (osi_particle_event[osi_access_object["lm"]][i_a].size(), 0.));
    for (int i_lp = 0; i_lp < osi_particle_event[osi_access_object["lp"]][i_a].size(); i_lp++){
      for (int i_lm = 0; i_lm < osi_particle_event[osi_access_object["lm"]][i_a].size(); i_lm++){
	M_leplep_MZ_combination[i_lp][i_lm] = (osi_particle_event[osi_access_object["lp"]][i_a][i_lp].momentum + osi_particle_event[osi_access_object["lm"]][i_a][i_lm].momentum).m();
	if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M_leplep_MZ_combination[" << i_lp << "][" << i_lm << "] = " << M_leplep_MZ_combination[i_lp][i_lm] << "   M_Z = " << osi_M[23] << endl;}
      }
    }

    if (switch_cut_M_leplep_ATLAS == 1){
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep_ATLAS = " << switch_cut_M_leplep_ATLAS << endl;}
      int i_lm = 0;
      for (int i_lp = 0; i_lp < osi_particle_event[osi_access_object["lp"]][i_a].size(); i_lp++){
        {	
        //for (int i_lm = 0; i_lm < osi_particle_event[osi_access_object["lm"]][i_a].size(); i_lm++){
          //	  M_leplep_MZ_combination[i_lp][i_lm] = (osi_particle_event[osi_access_object["lp"]][i_a][i_lp].momentum + osi_particle_event[osi_access_object["lm"]][i_a][i_lm].momentum).m();
          int j_lp = (i_lp + 1) % 2;
          int j_lm = (i_lm + 1) % 2;
          //	  M_leplep_MZ_combination[i_lp][i_lm] = (osi_particle_event[osi_access_object["lp"]][i_a][i_lp].momentum + osi_particle_event[osi_access_object["lm"]][i_a][i_lm].momentum).m();
          //	  M_leplep_MZ_combination[j_lp][j_lm] = (osi_particle_event[osi_access_object["lp"]][i_a][j_lp].momentum + osi_particle_event[osi_access_object["lm"]][i_a][j_lm].momentum).m();
          if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   abs_M_leplep_MZ_combination[" << i_lp << "][" << i_lm << "] = " << abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}
          if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   abs_M_leplep_MZ_combination[" << j_lp << "][" << j_lm << "] = " << abs(M_leplep_MZ_combination[j_lp][j_lm] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}

          if (abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]) + abs(M_leplep_MZ_combination[j_lp][j_lm] - osi_M[23]) < min_abs_M_leplep_MZ_combination){
            min_abs_M_leplep_MZ_combination = abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]) + abs(M_leplep_MZ_combination[j_lp][j_lm] - osi_M[23]);
            x_lp_1 = i_lp;
            x_lm_1 = i_lm;
            x_lp_2 = j_lp;
            x_lm_2 = j_lm;
          }
        }
      }

      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   1: M_leplep_MZ_combination[" << x_lp_1 << "][" << x_lm_1 << "] = " << abs(M_leplep_MZ_combination[x_lp_1][x_lm_1] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   2: M_leplep_MZ_combination[" << x_lp_2 << "][" << x_lm_2 << "] = " << abs(M_leplep_MZ_combination[x_lp_2][x_lm_2] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}

      M2_leplep1 = pow(M_leplep_MZ_combination[x_lp_1][x_lm_1], 2);
      M2_leplep2 = pow(M_leplep_MZ_combination[x_lp_2][x_lm_2], 2);

    }


  
    if (switch_cut_M_leplep_CMS == 1){
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_leplep_ATLAS = " << switch_cut_M_leplep_ATLAS << endl;}
      for (int i_lp = 0; i_lp < osi_particle_event[osi_access_object["lp"]][i_a].size(); i_lp++){
	for (int i_lm = 0; i_lm < osi_particle_event[osi_access_object["lm"]][i_a].size(); i_lm++){
	  //	  M_leplep_MZ_combination[i_lp][i_lm] = (osi_particle_event[osi_access_object["lp"]][i_a][i_lp].momentum + osi_particle_event[osi_access_object["lm"]][i_a][i_lm].momentum).m();
	  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   abs_M_leplep_MZ_combination[" << i_lp << "][" << i_lm << "] = " << abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}

	  if (abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]) < min_abs_M_leplep_MZ_combination){
	    min_abs_M_leplep_MZ_combination = abs(M_leplep_MZ_combination[i_lp][i_lm] - osi_M[23]);
	    x_lp_1 = i_lp;
	    x_lm_1 = i_lm;
	  }
	}
      }
      x_lp_2 = (x_lp_1 + 1) % 2;
      x_lm_2 = (x_lm_1 + 1) % 2;

      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   1: M_leplep_MZ_combination[" << x_lp_1 << "][" << x_lm_1 << "] = " << abs(M_leplep_MZ_combination[x_lp_1][x_lm_1] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   2: M_leplep_MZ_combination[" << x_lp_2 << "][" << x_lm_2 << "] = " << abs(M_leplep_MZ_combination[x_lp_2][x_lm_2] - osi_M[23]) << "   M_Z = " << osi_M[23] << endl;}

      M2_leplep1 = pow(M_leplep_MZ_combination[x_lp_1][x_lm_1], 2);
      M2_leplep2 = pow(M_leplep_MZ_combination[x_lp_2][x_lm_2], 2);
    }


    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep1(llx) = " << M2_leplep1 << " > " << cut_min_M2_leplep << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_leplep2(llx) = " << M2_leplep2 << " > " << cut_min_M2_leplep << endl;}

    if (M2_leplep1 < cut_min_M2_leplep || M2_leplep2 < cut_min_M2_leplep){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_leplep" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep min cut applied" << endl; 
      return;
    }

    if (osi_switch_output_cutinfo){if (cut_max_M2_leplep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_leplep1 = " << M2_leplep1 << " < " << cut_max_M2_leplep << endl;}}
    if (osi_switch_output_cutinfo){if (cut_max_M2_leplep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_leplep2 = " << M2_leplep2 << " < " << cut_max_M2_leplep << endl;}}

    if (cut_max_M2_leplep != 0 && (M2_leplep1 > cut_max_M2_leplep || M2_leplep2 > cut_max_M2_leplep)){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_leplep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_leplep max cut applied" << endl; 
      return;
    }
    // note: assignment is not necessarily correct at this point
    double M2_12 = M2_leplep1;
    if (abs(sqrt(M2_leplep2)-osi_M[23]) < abs(sqrt(M2_leplep1)-osi_M[23])) {
      M2_12 = M2_leplep2;
    }

    if (cut_max_M2_12 != 0 && (M2_12 > cut_max_M2_12 || M2_12 < cut_min_M2_12)){
      osi_cut_ps[i_a] = -1;
      return;
    }

    if (cut_min_M_leplep_IR != 0.){
      for (int i_lp = 0; i_lp < osi_particle_event[osi_access_object["lp"]][i_a].size(); i_lp++){
        for (int i_lm = 0; i_lm < osi_particle_event[osi_access_object["lm"]][i_a].size(); i_lm++){
          if (M_leplep_MZ_combination[i_lp][i_lm] < cut_min_M_leplep_IR){
            osi_cut_ps[i_a] = -1; 
            if (osi_switch_output_cutinfo){
              info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_leplep_min_IR" << endl; 
              logger << LOG_DEBUG << endl << info_cut.str(); 
            }
            logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
            return;
          }
        }
      }
    }



  }

  if (switch_cut_M_4lep == 1){
    double M2_4lep = (osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][0].momentum + osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][1].momentum + osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][2].momentum + osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][3].momentum).m2();

    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << osi_equivalent_no_object[osi_no_relevant_object["lep"]] << "][" << i_a << "][0] = " << osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][0].momentum << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << osi_equivalent_no_object[osi_no_relevant_object["lep"]] << "][" << i_a << "][1] = " << osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["lep"]]][i_a][1].momentum << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << osi_equivalent_no_object[osi_no_relevant_object["nua"]] << "][" << i_a << "][0] = " << osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["nua"]]][i_a][0].momentum << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << osi_equivalent_no_object[osi_no_relevant_object["nua"]] << "][" << i_a << "][1] = " << osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object["nua"]]][i_a][1].momentum << endl;}
    if (osi_switch_output_cutinfo){if (cut_min_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_4lep = " << M2_4lep << " > " << cut_min_M2_4lep << endl;}}
    if (osi_switch_output_cutinfo){if (cut_max_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_4lep = " << M2_4lep << " < " << cut_max_M2_4lep << endl;}}
    if (osi_switch_output_cutinfo){if (cut_min_delta_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   |M_4lep - M_Z| = " << abs(sqrt(M2_4lep) - osi_msi.M_Z) << " > " << cut_min_delta_M_4lep << endl;}}
    if (osi_switch_output_cutinfo){if (cut_max_delta_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   |M_4lep - M_Z| = " << abs(sqrt(M2_4lep) - osi_msi.M_Z) << " < " << cut_max_delta_M_4lep << endl;}}


    if (cut_min_delta_M_4lep != 0.){
      if (abs(sqrt(M2_4lep) - osi_msi.M_Z) < cut_min_delta_M_4lep){
	osi_cut_ps[i_a] = -1; 
	if (osi_switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_min_delta_M_4lep" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_M_leplep delta cut applied" << endl; 
	return;
      }
    }

    if (cut_max_delta_M_4lep != 0.){
      if (abs(sqrt(M2_4lep) - osi_msi.M_Z) > cut_max_delta_M_4lep){
	osi_cut_ps[i_a] = -1; 
	if (osi_switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_max_delta_M_4lep" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_M_leplep delta cut applied" << endl; 
	return;
      }
    }

    if (cut_min_M2_4lep != 0 && M2_4lep < cut_min_M2_4lep){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_min_M_4lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "cut_M_leplep min cut applied" << endl; 
      return;
    }

    if (cut_max_M2_4lep != 0 && M2_4lep > cut_max_M2_4lep){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_max_M_4lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "cut_M_leplep max cut applied" << endl; 
      return;
    }
  }

  if (osi_switch_output_cutinfo){info_cut << "ppemxnmnex04_cuts passed" << endl;}
}
