{
  static int switch_cut_M_Zrec = USERSWITCH("M_Zrec");
  static double cut_min_M_Zrec = USERCUT("min_M_Zrec");
  static double cut_min_M2_Zrec = pow(cut_min_M_Zrec, 2);
  static double cut_max_M_Zrec = USERCUT("max_M_Zrec");
  static double cut_max_M2_Zrec = pow(cut_max_M_Zrec, 2);
  static double cut_min_M_Z1 = USERCUT("min_M_Z1");
  static double cut_min_M2_Z1 = pow(cut_min_M_Z1, 2);
  static double cut_max_M_Z1 = USERCUT("max_M_Z1");
  static double cut_max_M2_Z1 = pow(cut_max_M_Z1, 2);

  static int switch_M_leplep_IR = USERSWITCH("M_leplep_OSSF");
  static double cut_min_M_leplep_IR = USERCUT("min_M_leplep_OSSF");
 
  static int switch_cut_M_4lep = USERSWITCH("M_4lep");
  static double cut_min_delta_M_4lep = USERCUT("min_delta_M_4lep");
  static double cut_max_delta_M_4lep = USERCUT("max_delta_M_4lep");
  static double cut_min_M_4lep = USERCUT("min_M_4lep");
  static double cut_min_M2_4lep = pow(cut_min_M_4lep, 2);
  static double cut_max_M_4lep = USERCUT("max_M_4lep");
  static double cut_max_M2_4lep = pow(cut_max_M_4lep, 2);

  static int switch_cut_R_leplep = USERSWITCH("R_leplep");
  static double cut_min_R_leplep = USERCUT("min_R_leplep");
  static double cut_min_R2_leplep = pow(cut_min_R_leplep, 2);

  static int switch_cut_R_rapidity_leplep = USERSWITCH("R_rapidity_leplep");
  static double cut_min_R_rapidity_leplep = USERCUT("min_R_rapidity_leplep");
  static double cut_min_R2_rapidity_leplep = pow(cut_min_R_rapidity_leplep, 2);

  static int switch_cut_R_eta_leplep = USERSWITCH("R_eta_leplep");
  static double cut_min_R_eta_leplep = USERCUT("min_R_eta_leplep");
  static double cut_min_R2_eta_leplep = pow(cut_min_R_eta_leplep, 2);

  static int switch_jetveto_R_lepjet = USERSWITCH("jetveto_R_lepjet");
  static double min_jetveto_R_lepjet = USERCUT("min_jetveto_R_lepjet");
  static double min_jetveto_R2_lepjet = pow(min_jetveto_R_lepjet, 2);

  // can be removed once switch_leptons_cuts introduced in parameter.dat >>>
  static int switch_cut_pT_lep_1st = USERSWITCH("pT_lep_1st");
  // static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st"); // commented because exists now below

  static int switch_cut_pT_lep_2nd = USERSWITCH("pT_lep_2nd");
  // static double cut_min_pT_lep_2nd = USERCUT("min_pT_lep_2nd"); // commented because exists now below

  static int switch_lepton_cuts = USERSWITCH("lepton_cuts");
  static double cut_min_pT_lep_1st = USERCUT("min_pT_lep_1st");
  static double cut_max_eta_lep_1st = USERCUT("max_eta_lep_1st");
  static double cut_min_pT_lep_2nd = USERCUT("min_pT_lep_2nd");
  static double cut_max_eta_lep_2nd = USERCUT("max_eta_lep_2nd");
  static double cut_min_pT_lep_3rd = USERCUT("min_pT_lep_3rd");
  static double cut_max_eta_lep_3rd = USERCUT("max_eta_lep_3rd");
  static double cut_min_pT_lep_4th = USERCUT("min_pT_lep_4th");
  static double cut_max_eta_lep_4th = USERCUT("max_eta_lep_4th");
  static double cut_extra_eta_lep = USERCUT("extra_eta_lep");
  static int n_observed_min_lep_extra = USERINT("n_observed_min_lep_extra");
  static int n_observed_max_lep_extra = USERINT("n_observed_max_lep_extra");

  static int switch_cut_lep_iso = USERSWITCH("lep_iso");
  static double cut_R_lep_iso = USERCUT("lep_iso_delta_0");
  static double cut_R2_lep_iso = pow(cut_R_lep_iso, 2);
  static double cut_threshold_lep_iso = USERCUT("lep_iso_epsilon");

  // lepton isolation: [pT sum of all particles except neutrinos in cone] / pT < threshold
  if (switch_cut_lep_iso) {
    // collect final-state partons
    vector<particle> particle_protojet;

    for (int i_l = 0; i_l < ps_runtime_jet_algorithm[i_a].size(); i_l++){
      int i_p = ps_runtime_jet_algorithm[i_a][i_l];
      particle_protojet.push_back(particle_event[0][i_a][i_p]);
    }

    for (int i = 0; i < PARTICLE("lep").size(); i++) {
      double pT_sum=0;
      // leptons
      for (int j = 0; j < PARTICLE("lep").size(); j++) {
        if (i==j)
          continue;
        double R2_eta_leplep = R2_eta(PARTICLE("lep")[i], PARTICLE("lep")[j]);
        if (R2_eta_leplep < cut_R2_lep_iso) {
          pT_sum += PARTICLE("lep")[j].pT;
        }
      }
      // partons
      for (int j = 0; j < particle_protojet.size(); j++) {
        double R2_eta_lepjet = R2_eta(PARTICLE("lep")[i], particle_protojet[j]);
        if (R2_eta_lepjet < cut_R2_lep_iso){
          pT_sum += particle_protojet[j].pT;
        }
      }

      if (pT_sum/PARTICLE("lep")[i].pT > cut_threshold_lep_iso) {
        cut_ps[i_a] = -1;
        return;
      }
    }
  }

  // can be removed once switch_leptons_cuts introduced in parameter.dat >>>
  if (switch_cut_pT_lep_1st == 1){
    double temp_pT_lep_1st = PARTICLE("lep")[0].pT;
    if (temp_pT_lep_1st < cut_min_pT_lep_1st){
      cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_1st << " < cut_min_pT_lep_1st = " << cut_min_pT_lep_1st << endl;
      return;
    }
  }
 
  if (switch_cut_pT_lep_2nd == 1){
    double temp_pT_lep_2nd = PARTICLE("lep")[1].pT;
    if (temp_pT_lep_2nd < cut_min_pT_lep_2nd){
      cut_ps[i_a] = -1; 
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_2nd << " < cut_min_pT_lep_2nd = " << cut_min_pT_lep_2nd << endl;
      return;
    }
  }
  // <<<

  // pT and eta cuts on leptons
  if (switch_lepton_cuts == 1){
    double temp_pT_lep_1st  = PARTICLE("lep")[0].pT;
    double temp_eta_lep_1st = abs(PARTICLE("lep")[0].eta);
    double temp_pT_lep_2nd  = PARTICLE("lep")[1].pT;
    double temp_eta_lep_2nd = abs(PARTICLE("lep")[1].eta);
    double temp_pT_lep_3rd  = PARTICLE("lep")[2].pT;
    double temp_eta_lep_3rd = abs(PARTICLE("lep")[2].eta);
    double temp_pT_lep_4th  = PARTICLE("lep")[3].pT;
    double temp_eta_lep_4th = abs(PARTICLE("lep")[3].eta);
    if (temp_pT_lep_1st < cut_min_pT_lep_1st){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_1st << " < cut_min_pT_lep_1st = " << cut_min_pT_lep_1st << endl;
      return;
    }
    if (temp_eta_lep_1st > cut_max_eta_lep_1st){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_eta_lep_1st << " > cut_max_eta_lep_1st = " << cut_max_eta_lep_1st << endl;
      return;
    }
    if (temp_pT_lep_2nd < cut_min_pT_lep_2nd){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_2nd << " < cut_min_pT_lep_2nd = " << cut_min_pT_lep_2nd << endl;
      return;
    }
    if (temp_eta_lep_2nd > cut_max_eta_lep_2nd){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_eta_lep_2nd << " > cut_max_eta_lep_2nd = " << cut_max_eta_lep_2nd << endl;
      return;
    }
    if (temp_pT_lep_3rd < cut_min_pT_lep_3rd){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_3rd << " < cut_min_pT_lep_3rd = " << cut_min_pT_lep_3rd << endl;
      return;
    }
    if (temp_eta_lep_3rd > cut_max_eta_lep_3rd){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_eta_lep_3rd << " > cut_max_eta_lep_3rd = " << cut_max_eta_lep_3rd << endl;
      return;
    }
    if (temp_pT_lep_4th < cut_min_pT_lep_4th){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_pT_lep_4th << " < cut_min_pT_lep_4th = " << cut_min_pT_lep_4th << endl;
      return;
    }
    if (temp_eta_lep_4th > cut_max_eta_lep_4th){
      cut_ps[i_a] = -1; // discards the events
      logger << LOG_DEBUG_VERBOSE << "Event at ps = " << i_a << " discarded due to " << temp_eta_lep_4th << " > cut_max_eta_lep_4th = " << cut_max_eta_lep_4th << endl;
      return;
    }
    int number_of_lep = PARTICLE("lep").size();
    for (int i = 0; i < PARTICLE("lep").size(); i++) {
      //      cout << PARTICLE("lep")[i].eta << endl; 
      if(abs(PARTICLE("lep")[i].eta) > cut_extra_eta_lep) {
	number_of_lep--;
      }
    }
    if (number_of_lep < n_observed_min_lep_extra || number_of_lep > n_observed_max_lep_extra) {
      cut_ps[i_a] = -1; // discards the events
      return;
    }
  }

  // lepton lepton separation cut (to be applied to *any* two leptons)
  if (switch_cut_R_leplep) {
    for (int i_l1 = 0; i_l1 < PARTICLE("lep").size(); i_l1++) {
      for (int i_l2 = i_l1 + 1; i_l2 < PARTICLE("lep").size(); i_l2++) {
        double R2_eta_leplep = R2_eta(PARTICLE("lep")[i_l1], PARTICLE("lep")[i_l2]);
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_lep[" << i_l1 << "]lep[" << i_l2 << "] = " << R2_eta_leplep << " > " << cut_min_R2_leplep << endl;}
        if (R2_eta_leplep < cut_min_R2_leplep) {
          cut_ps[i_a] = -1;
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_R_leplep" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
          return;
        }
      }
    }
  }

  if (switch_cut_R_rapidity_leplep) {
    for (int i_l1 = 0; i_l1 < PARTICLE("lep").size(); i_l1++) {
      for (int i_l2 = i_l1 + 1; i_l2 < PARTICLE("lep").size(); i_l2++) {
        double R2_rapidity_leplep = R2_rapidity(PARTICLE("lep")[i_l1], PARTICLE("lep")[i_l2]);
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_lep[" << i_l1 << "]lep[" << i_l2 << "] = " << R2_rapidity_leplep << " > " << cut_min_R2_rapidity_leplep << endl;}
        if (R2_rapidity_leplep < cut_min_R2_rapidity_leplep) {
          cut_ps[i_a] = -1;
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   process-specific cut after cut_R_rapidity_leplep" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
          return;
        }
      }
    }
  }


  // lepton lepton R-separation (pseudo-rapidity-based) cut (to be applied to *any* two leptons)
  if (switch_cut_R_eta_leplep) {
    for (int i_l1 = 0; i_l1 < PARTICLE("lep").size(); i_l1++) {
      for (int i_l2 = i_l1 + 1; i_l2 < PARTICLE("lep").size(); i_l2++) {
        double R2_eta_leplep = R2_eta(PARTICLE("lep")[i_l1], PARTICLE("lep")[i_l2]);
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_lep[" << i_l1 << "]lep[" << i_l2 << "] = " << R2_eta_leplep << " > " << cut_min_R2_eta_leplep << endl;}
        if (R2_eta_leplep < cut_min_R2_eta_leplep) {
          cut_ps[i_a] = -1;
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   process-specific cut after cut_R_eta_leplep" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
          return;
        }
      }
    }
  }

  // lepton invariant-mass cut
  if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_M_Zrec = " << switch_cut_M_Zrec << endl;}
  if (switch_cut_M_Zrec == 1){
    // selection of leptons pairing to Z bosons should be separated from applying invariant-mass cuts:

    double M2_Z1rec = PARTICLE("Z1rec")[0].m2;
    double M2_Z2rec = PARTICLE("Z2rec")[0].m2;

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_Z1rec(llx) = " << M2_Z1rec << " > " << cut_min_M2_Zrec << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   M2_Z2rec(llx) = " << M2_Z2rec << " > " << cut_min_M2_Zrec << endl;}

    if (M2_Z1rec < cut_min_M2_Zrec || M2_Z2rec < cut_min_M2_Zrec){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_Zrec" << endl;
	logger << LOG_DEBUG << endl << info_cut.str();
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Zrec min cut applied" << endl; 
      return;
    }

    if (switch_output_cutinfo){if (cut_max_M2_Zrec != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_Z1rec = " << M2_Z1rec << " < " << cut_max_M2_Zrec << endl;}}
    if (switch_output_cutinfo){if (cut_max_M2_Zrec != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_Z2rec = " << M2_Z2rec << " < " << cut_max_M2_Zrec << endl;}}

    if (cut_max_M2_Zrec != 0 && (M2_Z1rec > cut_max_M2_Zrec || M2_Z2rec > cut_max_M2_Zrec)){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_Zrec" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "switch_cut_M_Zrec max cut applied" << endl; 
      return;
    }

    if (cut_max_M2_Z1 != 0 && (M2_Z1rec > cut_max_M2_Z1 || M2_Z1rec < cut_min_M2_Z1)){
      cut_ps[i_a] = -1;
      return;
    }
  }

  if (switch_M_leplep_IR == 1){
    // IR safety cut: invariant mass of any em-ep combination must be larger than cut_min_M_leplep_IR
    if (cut_min_M_leplep_IR != 0.){
      for (int i_lp = 0; i_lp < PARTICLE("ep").size(); i_lp++){
        for (int i_lm = 0; i_lm < PARTICLE("em").size(); i_lm++){
          if ((PARTICLE("ep")[i_lp].momentum + PARTICLE("em")[i_lm].momentum).m() < cut_min_M_leplep_IR){
            cut_ps[i_a] = -1; 
            if (switch_output_cutinfo){
              info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_M_leplep_min_IR" << endl; 
              logger << LOG_DEBUG << endl << info_cut.str(); 
            }
            logger << LOG_DEBUG_VERBOSE << "cut_M_leplep_min_IR cut applied" << endl; 
            return;
          }
        }
      }
    }
  }

  if (switch_cut_M_4lep == 1){
    double M2_4lep = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("lep")[3].momentum).m2();

    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[0] = " << PARTICLE("lep")[0].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[1] = " << PARTICLE("lep")[1].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[2] = " << PARTICLE("lep")[2].momentum << endl;}
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[3] = " << PARTICLE("lep")[3].momentum << endl;}

    if (switch_output_cutinfo){if (cut_min_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_4lep = " << M2_4lep << " > " << cut_min_M2_4lep << endl;}}
    if (switch_output_cutinfo){if (cut_max_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   M2_4lep = " << M2_4lep << " < " << cut_max_M2_4lep << endl;}}
    if (switch_output_cutinfo){if (cut_min_delta_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   |M_4lep - M_Z| = " << abs(sqrt(M2_4lep) - msi->M_Z) << " > " << cut_min_delta_M_4lep << endl;}}
    if (switch_output_cutinfo){if (cut_max_delta_M_4lep != 0.){info_cut << "[" << setw(2) << i_a << "]   |M_4lep - M_Z| = " << abs(sqrt(M2_4lep) - msi->M_Z) << " < " << cut_max_delta_M_4lep << endl;}}


    if (cut_min_delta_M_4lep != 0.){
      if (abs(sqrt(M2_4lep) - msi->M_Z) < cut_min_delta_M_4lep){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_min_delta_M_4lep" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_M_leplep delta cut applied" << endl; 
	return;
      }
    }

    if (cut_max_delta_M_4lep != 0.){
      if (abs(sqrt(M2_4lep) - msi->M_Z) > cut_max_delta_M_4lep){
	cut_ps[i_a] = -1; 
	if (switch_output_cutinfo){
	  info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_max_delta_M_4lep" << endl; 
	  logger << LOG_DEBUG << endl << info_cut.str(); 
	}
	logger << LOG_DEBUG_VERBOSE << "cut_M_leplep delta cut applied" << endl; 
	return;
      }
    }

    if (cut_min_M2_4lep != 0 && M2_4lep < cut_min_M2_4lep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_min_M_4lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "cut_M_leplep min cut applied" << endl; 
      return;
    }

    if (cut_max_M2_4lep != 0 && M2_4lep > cut_max_M2_4lep){
      cut_ps[i_a] = -1; 
      if (switch_output_cutinfo){
	info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-cut after cut_max_M_4lep" << endl; 
	logger << LOG_DEBUG << endl << info_cut.str(); 
      }
      logger << LOG_DEBUG_VERBOSE << "cut_M_leplep max cut applied" << endl; 
      return;
    }
  }

  // lepton-jet separation -> jet veto
  if (switch_jetveto_R_lepjet) {
    for (int i_l = 0; i_l < NUMBER("lep"); i_l++) {
      for (int i_j = 0; i_j < NUMBER("jet"); i_j++) {
        double R2_eta_lepjet = R2_eta(PARTICLE("jet")[i_j], PARTICLE("lep")[i_l]);
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_lep[" << i_j << "]lep[" << i_l << "] = " << R2_eta_lepjet << " > " << min_jetveto_R2_lepjet << endl;}
        if (R2_eta_lepjet < min_jetveto_R2_lepjet) {
	  NUMBER("jet")--;
	  PARTICLE("jet").erase(PARTICLE("jet").begin() + i_j);
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppemxnmnex04-jet removed after jetveto_R_lepjet" << endl;
	    logger << LOG_DEBUG << endl << info_cut.str();
	  }
	  break;
        }
      }
    }
  }



  if (switch_output_cutinfo){info_cut << "ppemxnmnex04_cuts passed" << endl;}
}
