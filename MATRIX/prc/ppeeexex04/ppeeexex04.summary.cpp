#include "header.hpp"

#include "ppeeexex04.summary.hpp"

ppeeexex04_summary_generic::ppeeexex04_summary_generic(munich * xmunich){
  Logger logger("ppeeexex04_summary_generic::ppeeexex04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppeeexex04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppeeexex04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppeeexex04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppeeexex04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppeeexex04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppeeexex04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_born(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subprocess[4] = "aa_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subprocess[4] = "aa_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subprocess[4] = "aa_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_ememepep";
    subprocess[2] = "uu~_ememepep";
    subprocess[3] = "bb~_ememepep";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_ememepep";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_ememepepd";
    subprocess[2] = "gu_ememepepu";
    subprocess[3] = "gb_ememepepb";
    subprocess[4] = "gd~_ememepepd~";
    subprocess[5] = "gu~_ememepepu~";
    subprocess[6] = "gb~_ememepepb~";
    subprocess[7] = "dd~_ememepepg";
    subprocess[8] = "uu~_ememepepg";
    subprocess[9] = "bb~_ememepepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_ememepepg";
    subprocess[2] = "gd_ememepepd";
    subprocess[3] = "gu_ememepepu";
    subprocess[4] = "gb_ememepepb";
    subprocess[5] = "gd~_ememepepd~";
    subprocess[6] = "gu~_ememepepu~";
    subprocess[7] = "gb~_ememepepb~";
    subprocess[8] = "dd~_ememepepg";
    subprocess[9] = "uu~_ememepepg";
    subprocess[10] = "bb~_ememepepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_ememepepd";
    subprocess[2] = "ua_ememepepu";
    subprocess[3] = "ba_ememepepb";
    subprocess[4] = "d~a_ememepepd~";
    subprocess[5] = "u~a_ememepepu~";
    subprocess[6] = "b~a_ememepepb~";
    subprocess[7] = "dd~_ememepepa";
    subprocess[8] = "uu~_ememepepa";
    subprocess[9] = "bb~_ememepepa";
    subprocess[10] = "aa_ememepepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_ememepepd";
    subprocess[2] = "gu_ememepepu";
    subprocess[3] = "gb_ememepepb";
    subprocess[4] = "gd~_ememepepd~";
    subprocess[5] = "gu~_ememepepu~";
    subprocess[6] = "gb~_ememepepb~";
    subprocess[7] = "dd~_ememepepg";
    subprocess[8] = "uu~_ememepepg";
    subprocess[9] = "bb~_ememepepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_ememepepd";
    subprocess[2] = "ua_ememepepu";
    subprocess[3] = "ba_ememepepb";
    subprocess[4] = "d~a_ememepepd~";
    subprocess[5] = "u~a_ememepepu~";
    subprocess[6] = "b~a_ememepepb~";
    subprocess[7] = "dd~_ememepepa";
    subprocess[8] = "uu~_ememepepa";
    subprocess[9] = "bb~_ememepepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_ememepepd";
    subprocess[2] = "gu_ememepepu";
    subprocess[3] = "gb_ememepepb";
    subprocess[4] = "gd~_ememepepd~";
    subprocess[5] = "gu~_ememepepu~";
    subprocess[6] = "gb~_ememepepb~";
    subprocess[7] = "dd~_ememepepg";
    subprocess[8] = "uu~_ememepepg";
    subprocess[9] = "bb~_ememepepg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_ememepepd";
    subprocess[2] = "ua_ememepepu";
    subprocess[3] = "ba_ememepepb";
    subprocess[4] = "d~a_ememepepd~";
    subprocess[5] = "u~a_ememepepu~";
    subprocess[6] = "b~a_ememepepb~";
    subprocess[7] = "dd~_ememepepa";
    subprocess[8] = "uu~_ememepepa";
    subprocess[9] = "bb~_ememepepa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppeeexex04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppeeexex04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_ememepepdd~";
    subprocess[2] = "gg_ememepepuu~";
    subprocess[3] = "gg_ememepepbb~";
    subprocess[4] = "gd_ememepepgd";
    subprocess[5] = "gu_ememepepgu";
    subprocess[6] = "gb_ememepepgb";
    subprocess[7] = "gd~_ememepepgd~";
    subprocess[8] = "gu~_ememepepgu~";
    subprocess[9] = "gb~_ememepepgb~";
    subprocess[10] = "dd_ememepepdd";
    subprocess[11] = "du_ememepepdu";
    subprocess[12] = "ds_ememepepds";
    subprocess[13] = "dc_ememepepdc";
    subprocess[14] = "db_ememepepdb";
    subprocess[15] = "dd~_ememepepgg";
    subprocess[16] = "dd~_ememepepdd~";
    subprocess[17] = "dd~_ememepepuu~";
    subprocess[18] = "dd~_ememepepss~";
    subprocess[19] = "dd~_ememepepcc~";
    subprocess[20] = "dd~_ememepepbb~";
    subprocess[21] = "du~_ememepepdu~";
    subprocess[22] = "ds~_ememepepds~";
    subprocess[23] = "dc~_ememepepdc~";
    subprocess[24] = "db~_ememepepdb~";
    subprocess[25] = "uu_ememepepuu";
    subprocess[26] = "uc_ememepepuc";
    subprocess[27] = "ub_ememepepub";
    subprocess[28] = "ud~_ememepepud~";
    subprocess[29] = "uu~_ememepepgg";
    subprocess[30] = "uu~_ememepepdd~";
    subprocess[31] = "uu~_ememepepuu~";
    subprocess[32] = "uu~_ememepepss~";
    subprocess[33] = "uu~_ememepepcc~";
    subprocess[34] = "uu~_ememepepbb~";
    subprocess[35] = "us~_ememepepus~";
    subprocess[36] = "uc~_ememepepuc~";
    subprocess[37] = "ub~_ememepepub~";
    subprocess[38] = "bb_ememepepbb";
    subprocess[39] = "bd~_ememepepbd~";
    subprocess[40] = "bu~_ememepepbu~";
    subprocess[41] = "bb~_ememepepgg";
    subprocess[42] = "bb~_ememepepdd~";
    subprocess[43] = "bb~_ememepepuu~";
    subprocess[44] = "bb~_ememepepbb~";
    subprocess[45] = "d~d~_ememepepd~d~";
    subprocess[46] = "d~u~_ememepepd~u~";
    subprocess[47] = "d~s~_ememepepd~s~";
    subprocess[48] = "d~c~_ememepepd~c~";
    subprocess[49] = "d~b~_ememepepd~b~";
    subprocess[50] = "u~u~_ememepepu~u~";
    subprocess[51] = "u~c~_ememepepu~c~";
    subprocess[52] = "u~b~_ememepepu~b~";
    subprocess[53] = "b~b~_ememepepb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
