#include "header.hpp"

#include "ppz01.summary.hpp"

ppz01_summary_generic::ppz01_summary_generic(munich * xmunich){
  Logger logger("ppz01_summary_generic::ppz01_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppz01_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppz01_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppz01_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppz01_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppz01_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppz01_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_born(){
  Logger logger("ppz01_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_z";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppz01_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppz01_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_z";
    subprocess[2] = "uu~_z";
    subprocess[3] = "bb~_z";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zd";
    subprocess[2] = "gu_zu";
    subprocess[3] = "gb_zb";
    subprocess[4] = "gd~_zd~";
    subprocess[5] = "gu~_zu~";
    subprocess[6] = "gb~_zb~";
    subprocess[7] = "dd~_zg";
    subprocess[8] = "uu~_zg";
    subprocess[9] = "bb~_zg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppz01_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zd";
    subprocess[2] = "ua_zu";
    subprocess[3] = "ba_zb";
    subprocess[4] = "d~a_zd~";
    subprocess[5] = "u~a_zu~";
    subprocess[6] = "b~a_zb~";
    subprocess[7] = "dd~_za";
    subprocess[8] = "uu~_za";
    subprocess[9] = "bb~_za";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zd";
    subprocess[2] = "gu_zu";
    subprocess[3] = "gb_zb";
    subprocess[4] = "gd~_zd~";
    subprocess[5] = "gu~_zu~";
    subprocess[6] = "gb~_zb~";
    subprocess[7] = "dd~_zg";
    subprocess[8] = "uu~_zg";
    subprocess[9] = "bb~_zg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zd";
    subprocess[2] = "ua_zu";
    subprocess[3] = "ba_zb";
    subprocess[4] = "d~a_zd~";
    subprocess[5] = "u~a_zu~";
    subprocess[6] = "b~a_zb~";
    subprocess[7] = "dd~_za";
    subprocess[8] = "uu~_za";
    subprocess[9] = "bb~_za";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_zd";
    subprocess[2] = "gu_zu";
    subprocess[3] = "gb_zb";
    subprocess[4] = "gd~_zd~";
    subprocess[5] = "gu~_zu~";
    subprocess[6] = "gb~_zb~";
    subprocess[7] = "dd~_zg";
    subprocess[8] = "uu~_zg";
    subprocess[9] = "bb~_zg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_zd";
    subprocess[2] = "ua_zu";
    subprocess[3] = "ba_zb";
    subprocess[4] = "d~a_zd~";
    subprocess[5] = "u~a_zu~";
    subprocess[6] = "b~a_zb~";
    subprocess[7] = "dd~_za";
    subprocess[8] = "uu~_za";
    subprocess[9] = "bb~_za";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppz01_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_zdd~";
    subprocess[2] = "gg_zuu~";
    subprocess[3] = "gg_zbb~";
    subprocess[4] = "gd_zgd";
    subprocess[5] = "gu_zgu";
    subprocess[6] = "gb_zgb";
    subprocess[7] = "gd~_zgd~";
    subprocess[8] = "gu~_zgu~";
    subprocess[9] = "gb~_zgb~";
    subprocess[10] = "dd_zdd";
    subprocess[11] = "du_zdu";
    subprocess[12] = "ds_zds";
    subprocess[13] = "dc_zdc";
    subprocess[14] = "db_zdb";
    subprocess[15] = "dd~_zgg";
    subprocess[16] = "dd~_zdd~";
    subprocess[17] = "dd~_zuu~";
    subprocess[18] = "dd~_zss~";
    subprocess[19] = "dd~_zcc~";
    subprocess[20] = "dd~_zbb~";
    subprocess[21] = "du~_zdu~";
    subprocess[22] = "ds~_zds~";
    subprocess[23] = "dc~_zdc~";
    subprocess[24] = "db~_zdb~";
    subprocess[25] = "uu_zuu";
    subprocess[26] = "uc_zuc";
    subprocess[27] = "ub_zub";
    subprocess[28] = "ud~_zud~";
    subprocess[29] = "uu~_zgg";
    subprocess[30] = "uu~_zdd~";
    subprocess[31] = "uu~_zuu~";
    subprocess[32] = "uu~_zss~";
    subprocess[33] = "uu~_zcc~";
    subprocess[34] = "uu~_zbb~";
    subprocess[35] = "us~_zus~";
    subprocess[36] = "uc~_zuc~";
    subprocess[37] = "ub~_zub~";
    subprocess[38] = "bb_zbb";
    subprocess[39] = "bd~_zbd~";
    subprocess[40] = "bu~_zbu~";
    subprocess[41] = "bb~_zgg";
    subprocess[42] = "bb~_zdd~";
    subprocess[43] = "bb~_zuu~";
    subprocess[44] = "bb~_zbb~";
    subprocess[45] = "d~d~_zd~d~";
    subprocess[46] = "d~u~_zd~u~";
    subprocess[47] = "d~s~_zd~s~";
    subprocess[48] = "d~c~_zd~c~";
    subprocess[49] = "d~b~_zd~b~";
    subprocess[50] = "u~u~_zu~u~";
    subprocess[51] = "u~c~_zu~c~";
    subprocess[52] = "u~b~_zu~b~";
    subprocess[53] = "b~b~_zb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
