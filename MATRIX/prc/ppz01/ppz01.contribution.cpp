#include "header.hpp"

#include "ppz01.contribution.set.hpp"

ppz01_contribution_set::~ppz01_contribution_set(){
  static Logger logger("ppz01_contribution_set::~ppz01_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppz01_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23)){no_process_parton[i_a] = 5; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppz01_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> z    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> z   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppz01_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  23) && (tp[4] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  23) && (tp[4] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[4] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[4] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[4] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[4] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[4] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[4] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[4] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[4] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[4] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[4] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[4] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[4] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[4] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  23) && (tp[4] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[4] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[4] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[4] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[4] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[4] ==  22)){no_process_parton[i_a] = 23; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppz01_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> z   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> z   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> z   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> z   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> z   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> z   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> z   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> z   c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> z   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> z   b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> z   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> z   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> z   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> z   sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> z   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> z   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> z   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> z   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> z   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> z   bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> z   g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> z   d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> z   s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> z   d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> z   s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> z   u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> z   c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> z   u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> z   c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> z   b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> z   b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> z   dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> z   sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> z   dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> z   sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> z   ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> z   cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> z   ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> z   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> z   bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> z   bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> z   a    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppz01_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  23) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  23) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  23) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  23) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  23) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppz01_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> z   d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> z   s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> z   u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> z   c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> z   b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> z   g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> z   g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> z   g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> z   g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> z   g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> z   g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> z   g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> z   g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> z   g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> z   g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> z   g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> z   g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> z   g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> z   g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> z   g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> z   g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> z   g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> z   g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> z   g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> z   g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> z   d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> z   s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> z   d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> z   s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> z   d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> z   s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> z   d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> z   d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> z   d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> z   u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> z   u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> z   d   c    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> z   d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> z   s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> z   d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> z   s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> z   b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> z   b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> z   b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> z   b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> z   d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> z   s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> z   d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> z   s   cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> z   d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> z   s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> z   s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> z   d   sx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> z   d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> z   s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> z   s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> z   d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> z   d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> z   s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> z   d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> z   s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> z   u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> z   c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> z   u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> z   u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> z   u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> z   c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> z   u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> z   c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> z   u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> z   c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> z   u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> z   c   sx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> z   b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> z   b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> z   b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> z   b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> z   u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> z   c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> z   c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> z   u   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> z   u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> z   c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> z   c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> z   u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> z   u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> z   c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> z   u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> z   c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> z   b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> z   b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> z   b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> z   b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> z   b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> z   b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> z   b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> z   b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> z   b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> z   g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> z   s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> z   d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> z   s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> z   c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> z   u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> z   c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> z   b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> z   b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> z   dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> z   sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> z   dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> z   sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> z   dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> z   sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> z   dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> z   dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> z   dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> z   ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> z   ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> z   dx  cx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> z   dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> z   sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> z   dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> z   sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> z   ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> z   cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> z   ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> z   ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> z   ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> z   cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> z   ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> z   cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> z   bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
