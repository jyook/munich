#include "header.hpp"

#include "ppv01.amplitude.set.hpp"

#include "ppz01.contribution.set.hpp"
#include "ppz01.event.set.hpp"
#include "ppz01.phasespace.set.hpp"
#include "ppz01.observable.set.hpp"
#include "ppz01.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-z+X");

  MUC->csi = new ppz01_contribution_set();
  MUC->esi = new ppz01_event_set();
  MUC->psi = new ppz01_phasespace_set();
  MUC->osi = new ppz01_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppv01_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppz01_phasespace_set();
      MUC->psi->fake_psi->csi = new ppz01_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppz01_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
