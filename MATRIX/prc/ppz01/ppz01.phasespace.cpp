#include "header.hpp"

#include "ppz01.phasespace.set.hpp"

ppz01_phasespace_set::~ppz01_phasespace_set(){
  static Logger logger("ppz01_phasespace_set::~ppz01_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::optimize_minv_born(){
  static Logger logger("ppz01_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppz01_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppz01_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppz01_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppz01_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_010_ddx_z(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_010_uux_z(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_010_bbx_z(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_210_gg_z(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppz01_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_010_ddx_z(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_010_uux_z(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_010_bbx_z(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_210_gg_z(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppz01_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_010_ddx_z(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_010_uux_z(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_010_bbx_z(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_210_gg_z(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::optimize_minv_real(){
  static Logger logger("ppz01_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppz01_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppz01_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppz01_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppz01_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_110_gd_zd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_110_gu_zu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_110_gb_zb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_110_gdx_zdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_110_gux_zux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_110_gbx_zbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_110_ddx_zg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_110_uux_zg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_110_bbx_zg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_020_da_zd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_020_ua_zu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_020_ba_zb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_020_dxa_zdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_020_uxa_zux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_020_bxa_zbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_020_ddx_za(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_020_uux_za(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_020_bbx_za(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppz01_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_110_gd_zd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_110_gu_zu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_110_gb_zb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_110_gdx_zdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_110_gux_zux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_110_gbx_zbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_110_ddx_zg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_110_uux_zg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_110_bbx_zg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_020_da_zd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_020_ua_zu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_020_ba_zb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_020_dxa_zdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_020_uxa_zux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_020_bxa_zbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_020_ddx_za(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_020_uux_za(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_020_bbx_za(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppz01_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_110_gd_zd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_110_gu_zu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_110_gb_zb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_110_gdx_zdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_110_gux_zux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_110_gbx_zbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_110_ddx_zg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_110_uux_zg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_110_bbx_zg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_020_da_zd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_020_ua_zu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_020_ba_zb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_020_dxa_zdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_020_uxa_zux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_020_bxa_zbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_020_ddx_za(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_020_uux_za(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_020_bbx_za(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppz01_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppz01_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppz01_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 8;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppz01_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppz01_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_210_gg_zddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_210_gg_zuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_210_gg_zbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_210_gd_zgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_210_gu_zgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_210_gb_zgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_210_gdx_zgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_210_gux_zgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_210_gbx_zgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_210_dd_zdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_210_du_zdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_210_ds_zds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_210_dc_zdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_210_db_zdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_210_ddx_zgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_210_ddx_zddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_210_ddx_zuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_210_ddx_zssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_210_ddx_zccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_210_ddx_zbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_210_dux_zdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_210_dsx_zdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_210_dcx_zdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_210_dbx_zdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_210_uu_zuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_210_uc_zuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_210_ub_zub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_210_udx_zudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_210_uux_zgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_210_uux_zddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_210_uux_zuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_210_uux_zssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_210_uux_zccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_210_uux_zbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_210_usx_zusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_210_ucx_zucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_210_ubx_zubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_210_bb_zbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_210_bdx_zbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_210_bux_zbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_210_bbx_zgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_210_bbx_zddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_210_bbx_zuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_210_bbx_zbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_210_dxdx_zdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_210_dxux_zdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_210_dxsx_zdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_210_dxcx_zdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_210_dxbx_zdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_210_uxux_zuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_210_uxcx_zuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_210_uxbx_zuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_210_bxbx_zbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppz01_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_210_gg_zddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_210_gg_zuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_210_gg_zbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_210_gd_zgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_210_gu_zgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_210_gb_zgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_210_gdx_zgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_210_gux_zgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_210_gbx_zgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_210_dd_zdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_210_du_zdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_210_ds_zds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_210_dc_zdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_210_db_zdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_210_ddx_zgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_210_ddx_zddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_210_ddx_zuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_210_ddx_zssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_210_ddx_zccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_210_ddx_zbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_210_dux_zdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_210_dsx_zdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_210_dcx_zdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_210_dbx_zdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_210_uu_zuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_210_uc_zuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_210_ub_zub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_210_udx_zudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_210_uux_zgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_210_uux_zddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_210_uux_zuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_210_uux_zssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_210_uux_zccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_210_uux_zbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_210_usx_zusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_210_ucx_zucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_210_ubx_zubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_210_bb_zbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_210_bdx_zbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_210_bux_zbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_210_bbx_zgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_210_bbx_zddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_210_bbx_zuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_210_bbx_zbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_210_dxdx_zdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_210_dxux_zdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_210_dxsx_zdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_210_dxcx_zdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_210_dxbx_zdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_210_uxux_zuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_210_uxcx_zuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_210_uxbx_zuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_210_bxbx_zbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppz01_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppz01_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_210_gg_zddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_210_gg_zuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_210_gg_zbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_210_gd_zgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_210_gu_zgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_210_gb_zgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_210_gdx_zgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_210_gux_zgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_210_gbx_zgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_210_dd_zdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_210_du_zdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_210_ds_zds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_210_dc_zdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_210_db_zdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_210_ddx_zgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_210_ddx_zddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_210_ddx_zuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_210_ddx_zssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_210_ddx_zccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_210_ddx_zbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_210_dux_zdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_210_dsx_zdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_210_dcx_zdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_210_dbx_zdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_210_uu_zuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_210_uc_zuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_210_ub_zub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_210_udx_zudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_210_uux_zgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_210_uux_zddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_210_uux_zuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_210_uux_zssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_210_uux_zccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_210_uux_zbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_210_usx_zusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_210_ucx_zucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_210_ubx_zubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_210_bb_zbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_210_bdx_zbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_210_bux_zbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_210_bbx_zgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_210_bbx_zddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_210_bbx_zuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_210_bbx_zbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_210_dxdx_zdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_210_dxux_zdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_210_dxsx_zdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_210_dxcx_zdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_210_dxbx_zdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_210_uxux_zuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_210_uxcx_zuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_210_uxbx_zuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_210_bxbx_zbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
