#include "header.hpp"

#include "ppw01.contribution.set.hpp"

ppw01_contribution_set::~ppw01_contribution_set(){
  static Logger logger("ppw01_contribution_set::~ppw01_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppw01_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24)){no_process_parton[i_a] = 6; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppw01_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppw01_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 18; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppw01_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   u    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> w   u    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   c    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> w   c    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> w   u    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> w   u    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> w   c    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> w   c    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> w   u    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> w   u    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> w   c    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> w   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   dx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   dx   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   sx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   bx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   bx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   dx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   dx   //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   sx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   sx   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   bx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   bx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   g    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   g    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   g    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   g    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   g    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   g    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   g    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   g    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   g    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   g    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   g    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   g    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppw01_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 28; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 30; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 38; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 46; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 49; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 62; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 64; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 69; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 70; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 71; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 72; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 73; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 74; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 76; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 79; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 82; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 85; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 88; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 89; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 90; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 92; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 93; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 95; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 96; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 97; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 98; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 99; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 100; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 101; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 102; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 103; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 109; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 111; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 114; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 115; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   3)){no_process_parton[i_a] = 118; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 119; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 120; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 121; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 122; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 125; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 128; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 129; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 132; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 134; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 135; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 136; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 137; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 138; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 141; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 142; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 143; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 144; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 145; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 146; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 147; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 148; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 149; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 152; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 153; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 154; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 155; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 156; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 158; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 161; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 164; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 167; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 169; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 170; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 174; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 175; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 176; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 177; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 178; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 179; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 180; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 181; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 182; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 186; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 188; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 189; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 192; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 195; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 196; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 199; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 202; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 204; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 205; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 206; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 207; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 208; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 210; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 213; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 215; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 216; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 219; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 220; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 222; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 225; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 226; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 227; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 228; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 229; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 230; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 231; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 232; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 233; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 234; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 235; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 236; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 237; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 241; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 242; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 243; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 247; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 249; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 250; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 254; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 256; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 257; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 259; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 260; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 261; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 262; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 263; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 264; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 267; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 269; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 270; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 272; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 274; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 275; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 278; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 281; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 282; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 285; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 287; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==  -5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 288; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 28){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 30){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 38){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 46){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 49){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 62){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 64){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 70){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 71){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 72){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 73){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 74){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 76){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 79){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 82){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 85){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 88){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 89){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 90){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 92){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 93){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 95){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 96){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 97){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 98){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 99){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 100){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 101){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 102){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 103){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 109){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 111){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 114){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 115){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 118){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 119){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 120){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 121){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 122){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 125){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 128){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 129){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 132){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 134){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 135){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 136){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 137){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 138){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 141){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 142){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 143){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 144){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 145){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 146){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 147){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 148){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 149){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 152){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 153){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 154){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 155){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 156){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 158){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 161){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 164){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 167){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 169){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 170){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 174){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 175){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 176){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 177){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 178){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 179){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 180){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 181){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 182){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 186){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 188){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 189){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 192){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 195){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 196){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 199){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 202){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 204){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 205){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 206){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 207){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 208){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 210){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 213){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 215){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 216){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 219){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 220){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 222){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 225){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 226){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 227){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 228){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 229){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 230){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 231){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 232){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 233){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 234){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 235){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 236){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 237){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 241){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 242){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 243){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 247){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 249){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 250){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 254){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 256){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 257){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 259){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 260){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 261){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 262){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 263){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 264){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 267){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 269){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 270){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 272){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 274){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 275){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 278){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 281){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 282){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 285){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 287){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 288){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppw01_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   g   u    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> w   g   u    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   g   c    //
      combination_pdf[1] = {-1,   0,   1};   // d   g    -> w   g   c    //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> w   g   u    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> w   g   u    //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   3};   // g   s    -> w   g   c    //
      combination_pdf[1] = {-1,   0,   3};   // s   g    -> w   g   c    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> w   g   u    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> w   g   u    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> w   g   c    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> w   g   c    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   g   dx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   g   dx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   g   sx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   g   sx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   g   bx   //
      combination_pdf[1] = {-1,   0,  -2};   // ux  g    -> w   g   bx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   g   dx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   g   dx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   g   sx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   g   sx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -4};   // g   cx   -> w   g   bx   //
      combination_pdf[1] = {-1,   0,  -4};   // cx  g    -> w   g   bx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> w   d   u    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> w   d   c    //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> w   u   u    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> w   u   u    //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> w   u   c    //
      combination_pdf[1] = {-1,   1,   2};   // u   d    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 28){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> w   d   u    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> w   d   u    //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> w   d   c    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> w   d   c    //
    }
    else if (no_process_parton[i_a] == 30){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> w   u   s    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> w   u   s    //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> w   s   c    //
      combination_pdf[1] = {-1,   1,   3};   // s   d    -> w   s   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> w   u   c    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> w   c   c    //
      combination_pdf[1] = {-1,   1,   4};   // c   d    -> w   c   c    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> w   d   u    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> w   d   u    //
    }
    else if (no_process_parton[i_a] == 38){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> w   d   c    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> w   d   c    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> w   u   b    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> w   u   b    //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> w   c   b    //
      combination_pdf[1] = {-1,   1,   5};   // b   d    -> w   c   b    //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   u   dx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   u   sx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   u   bx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 46){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   c   dx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   c   sx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   c   bx   //
      combination_pdf[1] = {-1,   1,  -1};   // dx  d    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 49){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   g   g    //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   d   dx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   d   sx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   d   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   d   bx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   d   bx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   u   ux   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   s   sx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   c   ux   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   c   ux   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   c   cx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 62){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   b   bx   //
      combination_pdf[1] = {-1,   1,  -2};   // ux  d    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 64){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> w   u   sx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> w   c   sx   //
      combination_pdf[1] = {-1,   1,  -3};   // sx  d    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   g   g    //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 70){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   d   dx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 71){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   d   sx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   d   sx   //
    }
    else if (no_process_parton[i_a] == 72){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   d   bx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   d   bx   //
    }
    else if (no_process_parton[i_a] == 73){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   u   ux   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 74){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   u   cx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   u   cx   //
    }
    else if (no_process_parton[i_a] == 76){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   s   sx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 79){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   c   cx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 82){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   b   bx   //
      combination_pdf[1] = {-1,   1,  -4};   // cx  d    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 85){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> w   u   bx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 88){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> w   c   bx   //
      combination_pdf[1] = {-1,   1,  -5};   // bx  d    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 89){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> w   u   u    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> w   u   u    //
    }
    else if (no_process_parton[i_a] == 90){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   3};   // u   s    -> w   u   c    //
      combination_pdf[1] = {-1,   2,   3};   // s   u    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 92){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> w   u   u    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> w   u   u    //
    }
    else if (no_process_parton[i_a] == 93){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> w   u   c    //
      combination_pdf[1] = {-1,   2,   5};   // b   u    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 95){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   u   dx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 96){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   u   sx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 97){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   u   bx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 98){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   c   dx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 99){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   c   sx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 100){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   c   bx   //
      combination_pdf[1] = {-1,   2,  -2};   // ux  u    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 101){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> w   u   dx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 102){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> w   u   sx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 103){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> w   u   bx   //
      combination_pdf[1] = {-1,   2,  -4};   // cx  u    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 109){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   3,   3};   // s   s    -> w   u   s    //
    }
    else if (no_process_parton[i_a] == 111){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   3,   3};   // s   s    -> w   s   c    //
    }
    else if (no_process_parton[i_a] == 114){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> w   u   c    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 115){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   4};   // s   c    -> w   c   c    //
      combination_pdf[1] = {-1,   3,   4};   // c   s    -> w   c   c    //
    }
    else if (no_process_parton[i_a] == 118){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> w   u   s    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> w   u   s    //
    }
    else if (no_process_parton[i_a] == 119){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> w   u   b    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> w   u   b    //
    }
    else if (no_process_parton[i_a] == 120){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> w   s   c    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> w   s   c    //
    }
    else if (no_process_parton[i_a] == 121){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,   5};   // s   b    -> w   c   b    //
      combination_pdf[1] = {-1,   3,   5};   // b   s    -> w   c   b    //
    }
    else if (no_process_parton[i_a] == 122){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> w   u   dx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 125){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -1};   // s   dx   -> w   c   dx   //
      combination_pdf[1] = {-1,   3,  -1};   // dx  s    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 128){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   g   g    //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 129){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   d   dx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 132){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   u   ux   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 134){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   s   dx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   s   dx   //
    }
    else if (no_process_parton[i_a] == 135){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   s   sx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 136){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   s   bx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   s   bx   //
    }
    else if (no_process_parton[i_a] == 137){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   c   ux   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   c   ux   //
    }
    else if (no_process_parton[i_a] == 138){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   c   cx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 141){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -2};   // s   ux   -> w   b   bx   //
      combination_pdf[1] = {-1,   3,  -2};   // ux  s    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 142){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   u   dx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 143){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   u   sx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 144){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   u   bx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 145){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   c   dx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 146){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   c   sx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 147){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -3};   // s   sx   -> w   c   bx   //
      combination_pdf[1] = {-1,   3,  -3};   // sx  s    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 148){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   g   g    //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 149){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   d   dx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 152){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   u   ux   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 153){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   u   cx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   u   cx   //
    }
    else if (no_process_parton[i_a] == 154){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   s   dx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   s   dx   //
    }
    else if (no_process_parton[i_a] == 155){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   s   sx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 156){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   s   bx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   s   bx   //
    }
    else if (no_process_parton[i_a] == 158){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   c   cx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 161){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -4};   // s   cx   -> w   b   bx   //
      combination_pdf[1] = {-1,   3,  -4};   // cx  s    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 164){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> w   u   bx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 167){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   3,  -5};   // s   bx   -> w   c   bx   //
      combination_pdf[1] = {-1,   3,  -5};   // bx  s    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 169){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> w   u   c    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 170){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,   5};   // c   b    -> w   c   c    //
      combination_pdf[1] = {-1,   4,   5};   // b   c    -> w   c   c    //
    }
    else if (no_process_parton[i_a] == 174){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> w   c   dx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 175){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> w   c   sx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 176){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -2};   // c   ux   -> w   c   bx   //
      combination_pdf[1] = {-1,   4,  -2};   // ux  c    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 177){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   u   dx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 178){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   u   sx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 179){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   u   bx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 180){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   c   dx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 181){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   c   sx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 182){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   4,  -4};   // c   cx   -> w   c   bx   //
      combination_pdf[1] = {-1,   4,  -4};   // cx  c    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 186){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> w   u   b    //
    }
    else if (no_process_parton[i_a] == 188){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> w   c   b    //
    }
    else if (no_process_parton[i_a] == 189){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> w   u   dx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 192){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> w   c   dx   //
      combination_pdf[1] = {-1,   5,  -1};   // dx  b    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 195){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   g   g    //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 196){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   d   dx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 199){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   u   ux   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 202){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   s   sx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 204){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   c   ux   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   c   ux   //
    }
    else if (no_process_parton[i_a] == 205){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   c   cx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 206){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   b   dx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   b   dx   //
    }
    else if (no_process_parton[i_a] == 207){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   b   sx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   b   sx   //
    }
    else if (no_process_parton[i_a] == 208){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> w   b   bx   //
      combination_pdf[1] = {-1,   5,  -2};   // ux  b    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 210){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> w   u   sx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 213){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -3};   // b   sx   -> w   c   sx   //
      combination_pdf[1] = {-1,   5,  -3};   // sx  b    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 215){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   g   g    //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 216){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   d   dx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   d   dx   //
    }
    else if (no_process_parton[i_a] == 219){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   u   ux   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 220){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   u   cx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   u   cx   //
    }
    else if (no_process_parton[i_a] == 222){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   s   sx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 225){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   c   cx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 226){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   b   dx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   b   dx   //
    }
    else if (no_process_parton[i_a] == 227){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   b   sx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   b   sx   //
    }
    else if (no_process_parton[i_a] == 228){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -4};   // b   cx   -> w   b   bx   //
      combination_pdf[1] = {-1,   5,  -4};   // cx  b    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 229){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   u   dx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 230){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   u   sx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 231){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   u   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   u   bx   //
    }
    else if (no_process_parton[i_a] == 232){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   c   dx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   c   dx   //
    }
    else if (no_process_parton[i_a] == 233){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   c   sx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 234){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> w   c   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 235){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> w   dx  dx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> w   dx  dx   //
    }
    else if (no_process_parton[i_a] == 236){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> w   dx  sx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> w   dx  sx   //
    }
    else if (no_process_parton[i_a] == 237){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> w   dx  bx   //
      combination_pdf[1] = {-1,  -1,  -2};   // ux  dx   -> w   dx  bx   //
    }
    else if (no_process_parton[i_a] == 241){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> w   dx  dx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> w   dx  dx   //
    }
    else if (no_process_parton[i_a] == 242){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> w   dx  sx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> w   dx  sx   //
    }
    else if (no_process_parton[i_a] == 243){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> w   dx  bx   //
      combination_pdf[1] = {-1,  -1,  -4};   // cx  dx   -> w   dx  bx   //
    }
    else if (no_process_parton[i_a] == 247){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> w   dx  ux   //
    }
    else if (no_process_parton[i_a] == 249){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> w   ux  sx   //
    }
    else if (no_process_parton[i_a] == 250){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> w   ux  bx   //
    }
    else if (no_process_parton[i_a] == 254){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> w   dx  sx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> w   dx  sx   //
    }
    else if (no_process_parton[i_a] == 256){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> w   sx  sx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> w   sx  sx   //
    }
    else if (no_process_parton[i_a] == 257){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -3};   // ux  sx   -> w   sx  bx   //
      combination_pdf[1] = {-1,  -2,  -3};   // sx  ux   -> w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 259){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   dx  ux   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   dx  ux   //
    }
    else if (no_process_parton[i_a] == 260){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   dx  cx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   dx  cx   //
    }
    else if (no_process_parton[i_a] == 261){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   ux  sx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   ux  sx   //
    }
    else if (no_process_parton[i_a] == 262){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   ux  bx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   ux  bx   //
    }
    else if (no_process_parton[i_a] == 263){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   sx  cx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   sx  cx   //
    }
    else if (no_process_parton[i_a] == 264){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   cx  bx   //
      combination_pdf[1] = {-1,  -2,  -4};   // cx  ux   -> w   cx  bx   //
    }
    else if (no_process_parton[i_a] == 267){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> w   dx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> w   dx  bx   //
    }
    else if (no_process_parton[i_a] == 269){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> w   sx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 270){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> w   bx  bx   //
      combination_pdf[1] = {-1,  -2,  -5};   // bx  ux   -> w   bx  bx   //
    }
    else if (no_process_parton[i_a] == 272){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> w   dx  sx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> w   dx  sx   //
    }
    else if (no_process_parton[i_a] == 274){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> w   sx  sx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> w   sx  sx   //
    }
    else if (no_process_parton[i_a] == 275){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -3,  -4};   // sx  cx   -> w   sx  bx   //
      combination_pdf[1] = {-1,  -3,  -4};   // cx  sx   -> w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 278){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> w   dx  cx   //
    }
    else if (no_process_parton[i_a] == 281){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> w   sx  cx   //
    }
    else if (no_process_parton[i_a] == 282){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -4,  -4};   // cx  cx   -> w   cx  bx   //
    }
    else if (no_process_parton[i_a] == 285){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> w   dx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> w   dx  bx   //
    }
    else if (no_process_parton[i_a] == 287){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> w   sx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 288){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -4,  -5};   // cx  bx   -> w   bx  bx   //
      combination_pdf[1] = {-1,  -4,  -5};   // bx  cx   -> w   bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
