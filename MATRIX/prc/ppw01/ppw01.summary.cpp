#include "header.hpp"

#include "ppw01.summary.hpp"

ppw01_summary_generic::ppw01_summary_generic(munich * xmunich){
  Logger logger("ppw01_summary_generic::ppw01_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppw01_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppw01_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppw01_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppw01_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppw01_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppw01_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_born(){
  Logger logger("ppw01_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_wm";
    subprocess[2] = "dc~_wm";
    subprocess[3] = "su~_wm";
    subprocess[4] = "sc~_wm";
    subprocess[5] = "bu~_wm";
    subprocess[6] = "bc~_wm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_wm";
    subprocess[2] = "dc~_wm";
    subprocess[3] = "su~_wm";
    subprocess[4] = "sc~_wm";
    subprocess[5] = "bu~_wm";
    subprocess[6] = "bc~_wm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_wm";
    subprocess[2] = "dc~_wm";
    subprocess[3] = "su~_wm";
    subprocess[4] = "sc~_wm";
    subprocess[5] = "bu~_wm";
    subprocess[6] = "bc~_wm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_wm";
    subprocess[2] = "dc~_wm";
    subprocess[3] = "su~_wm";
    subprocess[4] = "sc~_wm";
    subprocess[5] = "bu~_wm";
    subprocess[6] = "bc~_wm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(7);
    subprocess[1] = "du~_wm";
    subprocess[2] = "dc~_wm";
    subprocess[3] = "su~_wm";
    subprocess[4] = "sc~_wm";
    subprocess[5] = "bu~_wm";
    subprocess[6] = "bc~_wm";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gd_wmc";
    subprocess[3] = "gs_wmu";
    subprocess[4] = "gs_wmc";
    subprocess[5] = "gb_wmu";
    subprocess[6] = "gb_wmc";
    subprocess[7] = "gu~_wmd~";
    subprocess[8] = "gu~_wms~";
    subprocess[9] = "gu~_wmb~";
    subprocess[10] = "gc~_wmd~";
    subprocess[11] = "gc~_wms~";
    subprocess[12] = "gc~_wmb~";
    subprocess[13] = "du~_wmg";
    subprocess[14] = "dc~_wmg";
    subprocess[15] = "su~_wmg";
    subprocess[16] = "sc~_wmg";
    subprocess[17] = "bu~_wmg";
    subprocess[18] = "bc~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gd_wmc";
    subprocess[3] = "gs_wmu";
    subprocess[4] = "gs_wmc";
    subprocess[5] = "gb_wmu";
    subprocess[6] = "gb_wmc";
    subprocess[7] = "gu~_wmd~";
    subprocess[8] = "gu~_wms~";
    subprocess[9] = "gu~_wmb~";
    subprocess[10] = "gc~_wmd~";
    subprocess[11] = "gc~_wms~";
    subprocess[12] = "gc~_wmb~";
    subprocess[13] = "du~_wmg";
    subprocess[14] = "dc~_wmg";
    subprocess[15] = "su~_wmg";
    subprocess[16] = "sc~_wmg";
    subprocess[17] = "bu~_wmg";
    subprocess[18] = "bc~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(19);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gd_wmc";
    subprocess[3] = "gs_wmu";
    subprocess[4] = "gs_wmc";
    subprocess[5] = "gb_wmu";
    subprocess[6] = "gb_wmc";
    subprocess[7] = "gu~_wmd~";
    subprocess[8] = "gu~_wms~";
    subprocess[9] = "gu~_wmb~";
    subprocess[10] = "gc~_wmd~";
    subprocess[11] = "gc~_wms~";
    subprocess[12] = "gc~_wmb~";
    subprocess[13] = "du~_wmg";
    subprocess[14] = "dc~_wmg";
    subprocess[15] = "su~_wmg";
    subprocess[16] = "sc~_wmg";
    subprocess[17] = "bu~_wmg";
    subprocess[18] = "bc~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppw01_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(181);
    subprocess[1] = "gg_wmud~";
    subprocess[2] = "gg_wmus~";
    subprocess[3] = "gg_wmub~";
    subprocess[4] = "gg_wmcd~";
    subprocess[5] = "gg_wmcs~";
    subprocess[6] = "gg_wmcb~";
    subprocess[7] = "gd_wmgu";
    subprocess[8] = "gd_wmgc";
    subprocess[9] = "gs_wmgu";
    subprocess[10] = "gs_wmgc";
    subprocess[11] = "gb_wmgu";
    subprocess[12] = "gb_wmgc";
    subprocess[13] = "gu~_wmgd~";
    subprocess[14] = "gu~_wmgs~";
    subprocess[15] = "gu~_wmgb~";
    subprocess[16] = "gc~_wmgd~";
    subprocess[17] = "gc~_wmgs~";
    subprocess[18] = "gc~_wmgb~";
    subprocess[19] = "dd_wmdu";
    subprocess[20] = "dd_wmdc";
    subprocess[21] = "du_wmuu";
    subprocess[22] = "du_wmuc";
    subprocess[23] = "ds_wmdu";
    subprocess[24] = "ds_wmdc";
    subprocess[25] = "ds_wmus";
    subprocess[26] = "ds_wmsc";
    subprocess[27] = "dc_wmuc";
    subprocess[28] = "dc_wmcc";
    subprocess[29] = "db_wmdu";
    subprocess[30] = "db_wmdc";
    subprocess[31] = "db_wmub";
    subprocess[32] = "db_wmcb";
    subprocess[33] = "dd~_wmud~";
    subprocess[34] = "dd~_wmus~";
    subprocess[35] = "dd~_wmub~";
    subprocess[36] = "dd~_wmcd~";
    subprocess[37] = "dd~_wmcs~";
    subprocess[38] = "dd~_wmcb~";
    subprocess[39] = "du~_wmgg";
    subprocess[40] = "du~_wmdd~";
    subprocess[41] = "du~_wmds~";
    subprocess[42] = "du~_wmdb~";
    subprocess[43] = "du~_wmuu~";
    subprocess[44] = "du~_wmss~";
    subprocess[45] = "du~_wmcu~";
    subprocess[46] = "du~_wmcc~";
    subprocess[47] = "du~_wmbb~";
    subprocess[48] = "ds~_wmus~";
    subprocess[49] = "ds~_wmcs~";
    subprocess[50] = "dc~_wmgg";
    subprocess[51] = "dc~_wmdd~";
    subprocess[52] = "dc~_wmds~";
    subprocess[53] = "dc~_wmdb~";
    subprocess[54] = "dc~_wmuu~";
    subprocess[55] = "dc~_wmuc~";
    subprocess[56] = "dc~_wmss~";
    subprocess[57] = "dc~_wmcc~";
    subprocess[58] = "dc~_wmbb~";
    subprocess[59] = "db~_wmub~";
    subprocess[60] = "db~_wmcb~";
    subprocess[61] = "us_wmuu";
    subprocess[62] = "us_wmuc";
    subprocess[63] = "ub_wmuu";
    subprocess[64] = "ub_wmuc";
    subprocess[65] = "uu~_wmud~";
    subprocess[66] = "uu~_wmus~";
    subprocess[67] = "uu~_wmub~";
    subprocess[68] = "uu~_wmcd~";
    subprocess[69] = "uu~_wmcs~";
    subprocess[70] = "uu~_wmcb~";
    subprocess[71] = "uc~_wmud~";
    subprocess[72] = "uc~_wmus~";
    subprocess[73] = "uc~_wmub~";
    subprocess[74] = "ss_wmus";
    subprocess[75] = "ss_wmsc";
    subprocess[76] = "sc_wmuc";
    subprocess[77] = "sc_wmcc";
    subprocess[78] = "sb_wmus";
    subprocess[79] = "sb_wmub";
    subprocess[80] = "sb_wmsc";
    subprocess[81] = "sb_wmcb";
    subprocess[82] = "sd~_wmud~";
    subprocess[83] = "sd~_wmcd~";
    subprocess[84] = "su~_wmgg";
    subprocess[85] = "su~_wmdd~";
    subprocess[86] = "su~_wmuu~";
    subprocess[87] = "su~_wmsd~";
    subprocess[88] = "su~_wmss~";
    subprocess[89] = "su~_wmsb~";
    subprocess[90] = "su~_wmcu~";
    subprocess[91] = "su~_wmcc~";
    subprocess[92] = "su~_wmbb~";
    subprocess[93] = "ss~_wmud~";
    subprocess[94] = "ss~_wmus~";
    subprocess[95] = "ss~_wmub~";
    subprocess[96] = "ss~_wmcd~";
    subprocess[97] = "ss~_wmcs~";
    subprocess[98] = "ss~_wmcb~";
    subprocess[99] = "sc~_wmgg";
    subprocess[100] = "sc~_wmdd~";
    subprocess[101] = "sc~_wmuu~";
    subprocess[102] = "sc~_wmuc~";
    subprocess[103] = "sc~_wmsd~";
    subprocess[104] = "sc~_wmss~";
    subprocess[105] = "sc~_wmsb~";
    subprocess[106] = "sc~_wmcc~";
    subprocess[107] = "sc~_wmbb~";
    subprocess[108] = "sb~_wmub~";
    subprocess[109] = "sb~_wmcb~";
    subprocess[110] = "cb_wmuc";
    subprocess[111] = "cb_wmcc";
    subprocess[112] = "cu~_wmcd~";
    subprocess[113] = "cu~_wmcs~";
    subprocess[114] = "cu~_wmcb~";
    subprocess[115] = "cc~_wmud~";
    subprocess[116] = "cc~_wmus~";
    subprocess[117] = "cc~_wmub~";
    subprocess[118] = "cc~_wmcd~";
    subprocess[119] = "cc~_wmcs~";
    subprocess[120] = "cc~_wmcb~";
    subprocess[121] = "bb_wmub";
    subprocess[122] = "bb_wmcb";
    subprocess[123] = "bd~_wmud~";
    subprocess[124] = "bd~_wmcd~";
    subprocess[125] = "bu~_wmgg";
    subprocess[126] = "bu~_wmdd~";
    subprocess[127] = "bu~_wmuu~";
    subprocess[128] = "bu~_wmss~";
    subprocess[129] = "bu~_wmcu~";
    subprocess[130] = "bu~_wmcc~";
    subprocess[131] = "bu~_wmbd~";
    subprocess[132] = "bu~_wmbs~";
    subprocess[133] = "bu~_wmbb~";
    subprocess[134] = "bs~_wmus~";
    subprocess[135] = "bs~_wmcs~";
    subprocess[136] = "bc~_wmgg";
    subprocess[137] = "bc~_wmdd~";
    subprocess[138] = "bc~_wmuu~";
    subprocess[139] = "bc~_wmuc~";
    subprocess[140] = "bc~_wmss~";
    subprocess[141] = "bc~_wmcc~";
    subprocess[142] = "bc~_wmbd~";
    subprocess[143] = "bc~_wmbs~";
    subprocess[144] = "bc~_wmbb~";
    subprocess[145] = "bb~_wmud~";
    subprocess[146] = "bb~_wmus~";
    subprocess[147] = "bb~_wmub~";
    subprocess[148] = "bb~_wmcd~";
    subprocess[149] = "bb~_wmcs~";
    subprocess[150] = "bb~_wmcb~";
    subprocess[151] = "d~u~_wmd~d~";
    subprocess[152] = "d~u~_wmd~s~";
    subprocess[153] = "d~u~_wmd~b~";
    subprocess[154] = "d~c~_wmd~d~";
    subprocess[155] = "d~c~_wmd~s~";
    subprocess[156] = "d~c~_wmd~b~";
    subprocess[157] = "u~u~_wmd~u~";
    subprocess[158] = "u~u~_wmu~s~";
    subprocess[159] = "u~u~_wmu~b~";
    subprocess[160] = "u~s~_wmd~s~";
    subprocess[161] = "u~s~_wms~s~";
    subprocess[162] = "u~s~_wms~b~";
    subprocess[163] = "u~c~_wmd~u~";
    subprocess[164] = "u~c~_wmd~c~";
    subprocess[165] = "u~c~_wmu~s~";
    subprocess[166] = "u~c~_wmu~b~";
    subprocess[167] = "u~c~_wms~c~";
    subprocess[168] = "u~c~_wmc~b~";
    subprocess[169] = "u~b~_wmd~b~";
    subprocess[170] = "u~b~_wms~b~";
    subprocess[171] = "u~b~_wmb~b~";
    subprocess[172] = "s~c~_wmd~s~";
    subprocess[173] = "s~c~_wms~s~";
    subprocess[174] = "s~c~_wms~b~";
    subprocess[175] = "c~c~_wmd~c~";
    subprocess[176] = "c~c~_wms~c~";
    subprocess[177] = "c~c~_wmc~b~";
    subprocess[178] = "c~b~_wmd~b~";
    subprocess[179] = "c~b~_wms~b~";
    subprocess[180] = "c~b~_wmb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
