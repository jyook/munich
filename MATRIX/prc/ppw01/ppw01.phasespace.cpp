#include "header.hpp"

#include "ppw01.phasespace.set.hpp"

ppw01_phasespace_set::~ppw01_phasespace_set(){
  static Logger logger("ppw01_phasespace_set::~ppw01_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::optimize_minv_born(){
  static Logger logger("ppw01_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppw01_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppw01_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_010_dux_wm(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_010_dcx_wm(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_010_sux_wm(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_010_scx_wm(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_010_bux_wm(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_010_bcx_wm(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppw01_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_010_dux_wm(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_010_dcx_wm(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_010_sux_wm(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_010_scx_wm(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_010_bux_wm(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_010_bcx_wm(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppw01_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_010_dux_wm(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_010_dcx_wm(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_010_sux_wm(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_010_scx_wm(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_010_bux_wm(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_010_bcx_wm(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::optimize_minv_real(){
  static Logger logger("ppw01_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppw01_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 12;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppw01_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_110_gd_wmu(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_110_gd_wmc(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_110_gs_wmu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_110_gs_wmc(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_110_gb_wmu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_110_gb_wmc(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_110_gux_wmdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_110_gux_wmsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_110_gux_wmbx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_110_gcx_wmdx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_110_gcx_wmsx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_110_gcx_wmbx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_110_dux_wmg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_110_dcx_wmg(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_110_sux_wmg(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_110_scx_wmg(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_110_bux_wmg(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_110_bcx_wmg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppw01_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_110_gd_wmu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_110_gd_wmc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_110_gs_wmu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_110_gs_wmc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_110_gb_wmu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_110_gb_wmc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_110_gux_wmdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_110_gux_wmsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_110_gux_wmbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_110_gcx_wmdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_110_gcx_wmsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_110_gcx_wmbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_110_dux_wmg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_110_dcx_wmg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_110_sux_wmg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_110_scx_wmg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_110_bux_wmg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_110_bcx_wmg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppw01_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_110_gd_wmu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_110_gd_wmc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_110_gs_wmu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_110_gs_wmc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_110_gb_wmu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_110_gb_wmc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_110_gux_wmdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_110_gux_wmsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_110_gux_wmbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_110_gcx_wmdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_110_gcx_wmsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_110_gcx_wmbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_110_dux_wmg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_110_dcx_wmg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_110_sux_wmg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_110_scx_wmg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_110_bux_wmg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_110_bcx_wmg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppw01_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppw01_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==   0){n_channel = 60;}
    else if (csi->no_process_parton[x_a] ==   1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   4){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   5){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   6){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   7){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   8){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==   9){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  11){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  13){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  14){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  15){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  16){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  17){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  18){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  19){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  20){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  25){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  26){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  28){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  29){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  30){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  32){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  35){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  36){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  37){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  38){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  40){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  42){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  43){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  44){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  45){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  46){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  47){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  48){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  49){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  50){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  51){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  52){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  53){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  56){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  58){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  59){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  62){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  64){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  67){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  69){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  70){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  71){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  72){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  73){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  74){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  76){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  79){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  82){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  85){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  88){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  89){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  90){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  92){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  93){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  95){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  96){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  97){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  98){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  99){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 100){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 101){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 102){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 103){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 109){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 111){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 114){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 115){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 118){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 119){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 120){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 121){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 122){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 125){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 128){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 129){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 132){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 134){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 135){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 136){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 137){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 138){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 141){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 142){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 143){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 144){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 145){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 146){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 147){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 148){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 149){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 152){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 153){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 154){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 155){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 156){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 158){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 161){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 164){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 167){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 169){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 170){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 174){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 175){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 176){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 177){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 178){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 179){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 180){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 181){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 182){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 186){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 188){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 189){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 192){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 195){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 196){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 199){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 202){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 204){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 205){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 206){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 207){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 208){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 210){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 213){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 215){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 216){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 219){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 220){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 222){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 225){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 226){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 227){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 228){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 229){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 230){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 231){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 232){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 233){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 234){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 235){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 236){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 237){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 241){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 242){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 243){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 247){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 249){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 250){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 254){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 256){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 257){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 259){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 260){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 261){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 262){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 263){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 264){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 267){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 269){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 270){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 272){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 274){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 275){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 278){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 281){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 282){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 285){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 287){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 288){n_channel = 4;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==   9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  28){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  30){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  38){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  46){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  49){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  62){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  64){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  69){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  70){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  71){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  72){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  73){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  74){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  76){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  79){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  82){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  85){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  88){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  89){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  90){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  92){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  93){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  95){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  96){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  97){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  98){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  99){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 100){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 101){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 102){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 103){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 109){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 111){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 114){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 115){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 118){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 119){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 120){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 121){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 122){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 125){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 128){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 129){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 132){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 134){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 135){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 136){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 137){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 138){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 141){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 142){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 143){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 144){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 145){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 146){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 147){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 148){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 149){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 152){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 153){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 154){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 155){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 156){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 158){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 161){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 164){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 167){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 169){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 170){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 174){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 175){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 176){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 177){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 178){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 179){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 180){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 181){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 182){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 186){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 188){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 189){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 192){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 195){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 196){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 199){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 202){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 204){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 205){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 206){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 207){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 208){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 210){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 213){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 215){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 216){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 219){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 220){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 222){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 225){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 226){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 227){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 228){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 229){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 230){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 231){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 232){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 233){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 234){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 235){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 236){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 237){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 241){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 242){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 243){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 247){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 249){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 250){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 254){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 256){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 257){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 259){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 260){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 261){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 262){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 263){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 264){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 267){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 269){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 270){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 272){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 274){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 275){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 278){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 281){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 282){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 285){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 287){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 288){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppw01_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ax_psp_210_gg_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] ==   2){ax_psp_210_gg_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] ==   3){ax_psp_210_gg_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] ==   4){ax_psp_210_gg_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] ==   5){ax_psp_210_gg_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==   6){ax_psp_210_gg_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==   7){ax_psp_210_gd_wmgu(x_a);}
    else if (csi->no_process_parton[x_a] ==   8){ax_psp_210_gd_wmgc(x_a);}
    else if (csi->no_process_parton[x_a] ==   9){ax_psp_210_gs_wmgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  10){ax_psp_210_gs_wmgc(x_a);}
    else if (csi->no_process_parton[x_a] ==  11){ax_psp_210_gb_wmgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  12){ax_psp_210_gb_wmgc(x_a);}
    else if (csi->no_process_parton[x_a] ==  13){ax_psp_210_gux_wmgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  14){ax_psp_210_gux_wmgsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  15){ax_psp_210_gux_wmgbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  16){ax_psp_210_gcx_wmgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  17){ax_psp_210_gcx_wmgsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  18){ax_psp_210_gcx_wmgbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  19){ax_psp_210_dd_wmdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  20){ax_psp_210_dd_wmdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  25){ax_psp_210_du_wmuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  26){ax_psp_210_du_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  28){ax_psp_210_ds_wmdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  29){ax_psp_210_ds_wmdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  30){ax_psp_210_ds_wmus(x_a);}
    else if (csi->no_process_parton[x_a] ==  32){ax_psp_210_ds_wmsc(x_a);}
    else if (csi->no_process_parton[x_a] ==  35){ax_psp_210_dc_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  36){ax_psp_210_dc_wmcc(x_a);}
    else if (csi->no_process_parton[x_a] ==  37){ax_psp_210_db_wmdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  38){ax_psp_210_db_wmdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  40){ax_psp_210_db_wmub(x_a);}
    else if (csi->no_process_parton[x_a] ==  42){ax_psp_210_db_wmcb(x_a);}
    else if (csi->no_process_parton[x_a] ==  43){ax_psp_210_ddx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  44){ax_psp_210_ddx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] ==  45){ax_psp_210_ddx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] ==  46){ax_psp_210_ddx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  47){ax_psp_210_ddx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  48){ax_psp_210_ddx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  49){ax_psp_210_dux_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  50){ax_psp_210_dux_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  51){ax_psp_210_dux_wmdsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  52){ax_psp_210_dux_wmdbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  53){ax_psp_210_dux_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  56){ax_psp_210_dux_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  58){ax_psp_210_dux_wmcux(x_a);}
    else if (csi->no_process_parton[x_a] ==  59){ax_psp_210_dux_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  62){ax_psp_210_dux_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  64){ax_psp_210_dsx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] ==  67){ax_psp_210_dsx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  69){ax_psp_210_dcx_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] ==  70){ax_psp_210_dcx_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  71){ax_psp_210_dcx_wmdsx(x_a);}
    else if (csi->no_process_parton[x_a] ==  72){ax_psp_210_dcx_wmdbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  73){ax_psp_210_dcx_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  74){ax_psp_210_dcx_wmucx(x_a);}
    else if (csi->no_process_parton[x_a] ==  76){ax_psp_210_dcx_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] ==  79){ax_psp_210_dcx_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] ==  82){ax_psp_210_dcx_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  85){ax_psp_210_dbx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] ==  88){ax_psp_210_dbx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  89){ax_psp_210_us_wmuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  90){ax_psp_210_us_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  92){ax_psp_210_ub_wmuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  93){ax_psp_210_ub_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  95){ax_psp_210_uux_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  96){ax_psp_210_uux_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] ==  97){ax_psp_210_uux_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] ==  98){ax_psp_210_uux_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  99){ax_psp_210_uux_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 100){ax_psp_210_uux_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 101){ax_psp_210_ucx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 102){ax_psp_210_ucx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 103){ax_psp_210_ucx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] == 109){ax_psp_210_ss_wmus(x_a);}
    else if (csi->no_process_parton[x_a] == 111){ax_psp_210_ss_wmsc(x_a);}
    else if (csi->no_process_parton[x_a] == 114){ax_psp_210_sc_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] == 115){ax_psp_210_sc_wmcc(x_a);}
    else if (csi->no_process_parton[x_a] == 118){ax_psp_210_sb_wmus(x_a);}
    else if (csi->no_process_parton[x_a] == 119){ax_psp_210_sb_wmub(x_a);}
    else if (csi->no_process_parton[x_a] == 120){ax_psp_210_sb_wmsc(x_a);}
    else if (csi->no_process_parton[x_a] == 121){ax_psp_210_sb_wmcb(x_a);}
    else if (csi->no_process_parton[x_a] == 122){ax_psp_210_sdx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 125){ax_psp_210_sdx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 128){ax_psp_210_sux_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] == 129){ax_psp_210_sux_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] == 132){ax_psp_210_sux_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] == 134){ax_psp_210_sux_wmsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 135){ax_psp_210_sux_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] == 136){ax_psp_210_sux_wmsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 137){ax_psp_210_sux_wmcux(x_a);}
    else if (csi->no_process_parton[x_a] == 138){ax_psp_210_sux_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] == 141){ax_psp_210_sux_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 142){ax_psp_210_ssx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 143){ax_psp_210_ssx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 144){ax_psp_210_ssx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] == 145){ax_psp_210_ssx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 146){ax_psp_210_ssx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 147){ax_psp_210_ssx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 148){ax_psp_210_scx_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] == 149){ax_psp_210_scx_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] == 152){ax_psp_210_scx_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] == 153){ax_psp_210_scx_wmucx(x_a);}
    else if (csi->no_process_parton[x_a] == 154){ax_psp_210_scx_wmsdx(x_a);}
    else if (csi->no_process_parton[x_a] == 155){ax_psp_210_scx_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] == 156){ax_psp_210_scx_wmsbx(x_a);}
    else if (csi->no_process_parton[x_a] == 158){ax_psp_210_scx_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] == 161){ax_psp_210_scx_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 164){ax_psp_210_sbx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] == 167){ax_psp_210_sbx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 169){ax_psp_210_cb_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] == 170){ax_psp_210_cb_wmcc(x_a);}
    else if (csi->no_process_parton[x_a] == 174){ax_psp_210_cux_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 175){ax_psp_210_cux_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 176){ax_psp_210_cux_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 177){ax_psp_210_ccx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 178){ax_psp_210_ccx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 179){ax_psp_210_ccx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] == 180){ax_psp_210_ccx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 181){ax_psp_210_ccx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 182){ax_psp_210_ccx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 186){ax_psp_210_bb_wmub(x_a);}
    else if (csi->no_process_parton[x_a] == 188){ax_psp_210_bb_wmcb(x_a);}
    else if (csi->no_process_parton[x_a] == 189){ax_psp_210_bdx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 192){ax_psp_210_bdx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 195){ax_psp_210_bux_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] == 196){ax_psp_210_bux_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] == 199){ax_psp_210_bux_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] == 202){ax_psp_210_bux_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] == 204){ax_psp_210_bux_wmcux(x_a);}
    else if (csi->no_process_parton[x_a] == 205){ax_psp_210_bux_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] == 206){ax_psp_210_bux_wmbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 207){ax_psp_210_bux_wmbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 208){ax_psp_210_bux_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 210){ax_psp_210_bsx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 213){ax_psp_210_bsx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 215){ax_psp_210_bcx_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] == 216){ax_psp_210_bcx_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] == 219){ax_psp_210_bcx_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] == 220){ax_psp_210_bcx_wmucx(x_a);}
    else if (csi->no_process_parton[x_a] == 222){ax_psp_210_bcx_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] == 225){ax_psp_210_bcx_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] == 226){ax_psp_210_bcx_wmbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 227){ax_psp_210_bcx_wmbsx(x_a);}
    else if (csi->no_process_parton[x_a] == 228){ax_psp_210_bcx_wmbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 229){ax_psp_210_bbx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 230){ax_psp_210_bbx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 231){ax_psp_210_bbx_wmubx(x_a);}
    else if (csi->no_process_parton[x_a] == 232){ax_psp_210_bbx_wmcdx(x_a);}
    else if (csi->no_process_parton[x_a] == 233){ax_psp_210_bbx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 234){ax_psp_210_bbx_wmcbx(x_a);}
    else if (csi->no_process_parton[x_a] == 235){ax_psp_210_dxux_wmdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 236){ax_psp_210_dxux_wmdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 237){ax_psp_210_dxux_wmdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 241){ax_psp_210_dxcx_wmdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 242){ax_psp_210_dxcx_wmdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 243){ax_psp_210_dxcx_wmdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 247){ax_psp_210_uxux_wmdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 249){ax_psp_210_uxux_wmuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 250){ax_psp_210_uxux_wmuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 254){ax_psp_210_uxsx_wmdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 256){ax_psp_210_uxsx_wmsxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 257){ax_psp_210_uxsx_wmsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 259){ax_psp_210_uxcx_wmdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 260){ax_psp_210_uxcx_wmdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 261){ax_psp_210_uxcx_wmuxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 262){ax_psp_210_uxcx_wmuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 263){ax_psp_210_uxcx_wmsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 264){ax_psp_210_uxcx_wmcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 267){ax_psp_210_uxbx_wmdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 269){ax_psp_210_uxbx_wmsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 270){ax_psp_210_uxbx_wmbxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 272){ax_psp_210_sxcx_wmdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 274){ax_psp_210_sxcx_wmsxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 275){ax_psp_210_sxcx_wmsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 278){ax_psp_210_cxcx_wmdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 281){ax_psp_210_cxcx_wmsxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 282){ax_psp_210_cxcx_wmcxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 285){ax_psp_210_cxbx_wmdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 287){ax_psp_210_cxbx_wmsxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 288){ax_psp_210_cxbx_wmbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppw01_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ac_psp_210_gg_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   2){ac_psp_210_gg_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   3){ac_psp_210_gg_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   4){ac_psp_210_gg_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   5){ac_psp_210_gg_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   6){ac_psp_210_gg_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   7){ac_psp_210_gd_wmgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   8){ac_psp_210_gd_wmgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==   9){ac_psp_210_gs_wmgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  10){ac_psp_210_gs_wmgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  11){ac_psp_210_gb_wmgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  12){ac_psp_210_gb_wmgc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  13){ac_psp_210_gux_wmgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  14){ac_psp_210_gux_wmgsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  15){ac_psp_210_gux_wmgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  16){ac_psp_210_gcx_wmgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  17){ac_psp_210_gcx_wmgsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  18){ac_psp_210_gcx_wmgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  19){ac_psp_210_dd_wmdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  20){ac_psp_210_dd_wmdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  25){ac_psp_210_du_wmuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  26){ac_psp_210_du_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  28){ac_psp_210_ds_wmdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  29){ac_psp_210_ds_wmdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  30){ac_psp_210_ds_wmus(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  32){ac_psp_210_ds_wmsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  35){ac_psp_210_dc_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  36){ac_psp_210_dc_wmcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  37){ac_psp_210_db_wmdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  38){ac_psp_210_db_wmdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  40){ac_psp_210_db_wmub(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  42){ac_psp_210_db_wmcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  43){ac_psp_210_ddx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  44){ac_psp_210_ddx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  45){ac_psp_210_ddx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  46){ac_psp_210_ddx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  47){ac_psp_210_ddx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  48){ac_psp_210_ddx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  49){ac_psp_210_dux_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  50){ac_psp_210_dux_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  51){ac_psp_210_dux_wmdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  52){ac_psp_210_dux_wmdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  53){ac_psp_210_dux_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  56){ac_psp_210_dux_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  58){ac_psp_210_dux_wmcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  59){ac_psp_210_dux_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  62){ac_psp_210_dux_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  64){ac_psp_210_dsx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  67){ac_psp_210_dsx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  69){ac_psp_210_dcx_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  70){ac_psp_210_dcx_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  71){ac_psp_210_dcx_wmdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  72){ac_psp_210_dcx_wmdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  73){ac_psp_210_dcx_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  74){ac_psp_210_dcx_wmucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  76){ac_psp_210_dcx_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  79){ac_psp_210_dcx_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  82){ac_psp_210_dcx_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  85){ac_psp_210_dbx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  88){ac_psp_210_dbx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  89){ac_psp_210_us_wmuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  90){ac_psp_210_us_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  92){ac_psp_210_ub_wmuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  93){ac_psp_210_ub_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  95){ac_psp_210_uux_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  96){ac_psp_210_uux_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  97){ac_psp_210_uux_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  98){ac_psp_210_uux_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  99){ac_psp_210_uux_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 100){ac_psp_210_uux_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 101){ac_psp_210_ucx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 102){ac_psp_210_ucx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 103){ac_psp_210_ucx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 109){ac_psp_210_ss_wmus(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 111){ac_psp_210_ss_wmsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 114){ac_psp_210_sc_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 115){ac_psp_210_sc_wmcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 118){ac_psp_210_sb_wmus(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 119){ac_psp_210_sb_wmub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 120){ac_psp_210_sb_wmsc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 121){ac_psp_210_sb_wmcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 122){ac_psp_210_sdx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 125){ac_psp_210_sdx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 128){ac_psp_210_sux_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 129){ac_psp_210_sux_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 132){ac_psp_210_sux_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 134){ac_psp_210_sux_wmsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 135){ac_psp_210_sux_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 136){ac_psp_210_sux_wmsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 137){ac_psp_210_sux_wmcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 138){ac_psp_210_sux_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 141){ac_psp_210_sux_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 142){ac_psp_210_ssx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 143){ac_psp_210_ssx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 144){ac_psp_210_ssx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 145){ac_psp_210_ssx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 146){ac_psp_210_ssx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 147){ac_psp_210_ssx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 148){ac_psp_210_scx_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 149){ac_psp_210_scx_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 152){ac_psp_210_scx_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 153){ac_psp_210_scx_wmucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 154){ac_psp_210_scx_wmsdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 155){ac_psp_210_scx_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 156){ac_psp_210_scx_wmsbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 158){ac_psp_210_scx_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 161){ac_psp_210_scx_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 164){ac_psp_210_sbx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 167){ac_psp_210_sbx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 169){ac_psp_210_cb_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 170){ac_psp_210_cb_wmcc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 174){ac_psp_210_cux_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 175){ac_psp_210_cux_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 176){ac_psp_210_cux_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 177){ac_psp_210_ccx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 178){ac_psp_210_ccx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 179){ac_psp_210_ccx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 180){ac_psp_210_ccx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 181){ac_psp_210_ccx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 182){ac_psp_210_ccx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 186){ac_psp_210_bb_wmub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 188){ac_psp_210_bb_wmcb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 189){ac_psp_210_bdx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 192){ac_psp_210_bdx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 195){ac_psp_210_bux_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 196){ac_psp_210_bux_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 199){ac_psp_210_bux_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 202){ac_psp_210_bux_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 204){ac_psp_210_bux_wmcux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 205){ac_psp_210_bux_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 206){ac_psp_210_bux_wmbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 207){ac_psp_210_bux_wmbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 208){ac_psp_210_bux_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 210){ac_psp_210_bsx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 213){ac_psp_210_bsx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 215){ac_psp_210_bcx_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 216){ac_psp_210_bcx_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 219){ac_psp_210_bcx_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 220){ac_psp_210_bcx_wmucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 222){ac_psp_210_bcx_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 225){ac_psp_210_bcx_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 226){ac_psp_210_bcx_wmbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 227){ac_psp_210_bcx_wmbsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 228){ac_psp_210_bcx_wmbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 229){ac_psp_210_bbx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 230){ac_psp_210_bbx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 231){ac_psp_210_bbx_wmubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 232){ac_psp_210_bbx_wmcdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 233){ac_psp_210_bbx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 234){ac_psp_210_bbx_wmcbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 235){ac_psp_210_dxux_wmdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 236){ac_psp_210_dxux_wmdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 237){ac_psp_210_dxux_wmdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 241){ac_psp_210_dxcx_wmdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 242){ac_psp_210_dxcx_wmdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 243){ac_psp_210_dxcx_wmdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 247){ac_psp_210_uxux_wmdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 249){ac_psp_210_uxux_wmuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 250){ac_psp_210_uxux_wmuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 254){ac_psp_210_uxsx_wmdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 256){ac_psp_210_uxsx_wmsxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 257){ac_psp_210_uxsx_wmsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 259){ac_psp_210_uxcx_wmdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 260){ac_psp_210_uxcx_wmdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 261){ac_psp_210_uxcx_wmuxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 262){ac_psp_210_uxcx_wmuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 263){ac_psp_210_uxcx_wmsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 264){ac_psp_210_uxcx_wmcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 267){ac_psp_210_uxbx_wmdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 269){ac_psp_210_uxbx_wmsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 270){ac_psp_210_uxbx_wmbxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 272){ac_psp_210_sxcx_wmdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 274){ac_psp_210_sxcx_wmsxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 275){ac_psp_210_sxcx_wmsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 278){ac_psp_210_cxcx_wmdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 281){ac_psp_210_cxcx_wmsxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 282){ac_psp_210_cxcx_wmcxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 285){ac_psp_210_cxbx_wmdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 287){ac_psp_210_cxbx_wmsxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 288){ac_psp_210_cxbx_wmbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppw01_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==   1){ag_psp_210_gg_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   2){ag_psp_210_gg_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   3){ag_psp_210_gg_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   4){ag_psp_210_gg_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   5){ag_psp_210_gg_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   6){ag_psp_210_gg_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   7){ag_psp_210_gd_wmgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   8){ag_psp_210_gd_wmgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==   9){ag_psp_210_gs_wmgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  10){ag_psp_210_gs_wmgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  11){ag_psp_210_gb_wmgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  12){ag_psp_210_gb_wmgc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  13){ag_psp_210_gux_wmgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  14){ag_psp_210_gux_wmgsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  15){ag_psp_210_gux_wmgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  16){ag_psp_210_gcx_wmgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  17){ag_psp_210_gcx_wmgsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  18){ag_psp_210_gcx_wmgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  19){ag_psp_210_dd_wmdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  20){ag_psp_210_dd_wmdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  25){ag_psp_210_du_wmuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  26){ag_psp_210_du_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  28){ag_psp_210_ds_wmdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  29){ag_psp_210_ds_wmdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  30){ag_psp_210_ds_wmus(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  32){ag_psp_210_ds_wmsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  35){ag_psp_210_dc_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  36){ag_psp_210_dc_wmcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  37){ag_psp_210_db_wmdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  38){ag_psp_210_db_wmdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  40){ag_psp_210_db_wmub(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  42){ag_psp_210_db_wmcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  43){ag_psp_210_ddx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  44){ag_psp_210_ddx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  45){ag_psp_210_ddx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  46){ag_psp_210_ddx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  47){ag_psp_210_ddx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  48){ag_psp_210_ddx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  49){ag_psp_210_dux_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  50){ag_psp_210_dux_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  51){ag_psp_210_dux_wmdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  52){ag_psp_210_dux_wmdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  53){ag_psp_210_dux_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  56){ag_psp_210_dux_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  58){ag_psp_210_dux_wmcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  59){ag_psp_210_dux_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  62){ag_psp_210_dux_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  64){ag_psp_210_dsx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  67){ag_psp_210_dsx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  69){ag_psp_210_dcx_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  70){ag_psp_210_dcx_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  71){ag_psp_210_dcx_wmdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  72){ag_psp_210_dcx_wmdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  73){ag_psp_210_dcx_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  74){ag_psp_210_dcx_wmucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  76){ag_psp_210_dcx_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  79){ag_psp_210_dcx_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  82){ag_psp_210_dcx_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  85){ag_psp_210_dbx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  88){ag_psp_210_dbx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  89){ag_psp_210_us_wmuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  90){ag_psp_210_us_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  92){ag_psp_210_ub_wmuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  93){ag_psp_210_ub_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  95){ag_psp_210_uux_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  96){ag_psp_210_uux_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  97){ag_psp_210_uux_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  98){ag_psp_210_uux_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  99){ag_psp_210_uux_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 100){ag_psp_210_uux_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 101){ag_psp_210_ucx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 102){ag_psp_210_ucx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 103){ag_psp_210_ucx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 109){ag_psp_210_ss_wmus(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 111){ag_psp_210_ss_wmsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 114){ag_psp_210_sc_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 115){ag_psp_210_sc_wmcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 118){ag_psp_210_sb_wmus(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 119){ag_psp_210_sb_wmub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 120){ag_psp_210_sb_wmsc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 121){ag_psp_210_sb_wmcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 122){ag_psp_210_sdx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 125){ag_psp_210_sdx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 128){ag_psp_210_sux_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 129){ag_psp_210_sux_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 132){ag_psp_210_sux_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 134){ag_psp_210_sux_wmsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 135){ag_psp_210_sux_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 136){ag_psp_210_sux_wmsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 137){ag_psp_210_sux_wmcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 138){ag_psp_210_sux_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 141){ag_psp_210_sux_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 142){ag_psp_210_ssx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 143){ag_psp_210_ssx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 144){ag_psp_210_ssx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 145){ag_psp_210_ssx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 146){ag_psp_210_ssx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 147){ag_psp_210_ssx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 148){ag_psp_210_scx_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 149){ag_psp_210_scx_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 152){ag_psp_210_scx_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 153){ag_psp_210_scx_wmucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 154){ag_psp_210_scx_wmsdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 155){ag_psp_210_scx_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 156){ag_psp_210_scx_wmsbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 158){ag_psp_210_scx_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 161){ag_psp_210_scx_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 164){ag_psp_210_sbx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 167){ag_psp_210_sbx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 169){ag_psp_210_cb_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 170){ag_psp_210_cb_wmcc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 174){ag_psp_210_cux_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 175){ag_psp_210_cux_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 176){ag_psp_210_cux_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 177){ag_psp_210_ccx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 178){ag_psp_210_ccx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 179){ag_psp_210_ccx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 180){ag_psp_210_ccx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 181){ag_psp_210_ccx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 182){ag_psp_210_ccx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 186){ag_psp_210_bb_wmub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 188){ag_psp_210_bb_wmcb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 189){ag_psp_210_bdx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 192){ag_psp_210_bdx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 195){ag_psp_210_bux_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 196){ag_psp_210_bux_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 199){ag_psp_210_bux_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 202){ag_psp_210_bux_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 204){ag_psp_210_bux_wmcux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 205){ag_psp_210_bux_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 206){ag_psp_210_bux_wmbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 207){ag_psp_210_bux_wmbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 208){ag_psp_210_bux_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 210){ag_psp_210_bsx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 213){ag_psp_210_bsx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 215){ag_psp_210_bcx_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 216){ag_psp_210_bcx_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 219){ag_psp_210_bcx_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 220){ag_psp_210_bcx_wmucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 222){ag_psp_210_bcx_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 225){ag_psp_210_bcx_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 226){ag_psp_210_bcx_wmbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 227){ag_psp_210_bcx_wmbsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 228){ag_psp_210_bcx_wmbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 229){ag_psp_210_bbx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 230){ag_psp_210_bbx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 231){ag_psp_210_bbx_wmubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 232){ag_psp_210_bbx_wmcdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 233){ag_psp_210_bbx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 234){ag_psp_210_bbx_wmcbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 235){ag_psp_210_dxux_wmdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 236){ag_psp_210_dxux_wmdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 237){ag_psp_210_dxux_wmdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 241){ag_psp_210_dxcx_wmdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 242){ag_psp_210_dxcx_wmdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 243){ag_psp_210_dxcx_wmdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 247){ag_psp_210_uxux_wmdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 249){ag_psp_210_uxux_wmuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 250){ag_psp_210_uxux_wmuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 254){ag_psp_210_uxsx_wmdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 256){ag_psp_210_uxsx_wmsxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 257){ag_psp_210_uxsx_wmsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 259){ag_psp_210_uxcx_wmdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 260){ag_psp_210_uxcx_wmdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 261){ag_psp_210_uxcx_wmuxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 262){ag_psp_210_uxcx_wmuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 263){ag_psp_210_uxcx_wmsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 264){ag_psp_210_uxcx_wmcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 267){ag_psp_210_uxbx_wmdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 269){ag_psp_210_uxbx_wmsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 270){ag_psp_210_uxbx_wmbxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 272){ag_psp_210_sxcx_wmdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 274){ag_psp_210_sxcx_wmsxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 275){ag_psp_210_sxcx_wmsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 278){ag_psp_210_cxcx_wmdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 281){ag_psp_210_cxcx_wmsxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 282){ag_psp_210_cxcx_wmcxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 285){ag_psp_210_cxbx_wmdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 287){ag_psp_210_cxbx_wmsxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 288){ag_psp_210_cxbx_wmbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
