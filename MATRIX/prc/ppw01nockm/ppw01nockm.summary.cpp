#include "header.hpp"

#include "ppw01nockm.summary.hpp"

ppw01nockm_summary_generic::ppw01nockm_summary_generic(munich * xmunich){
  Logger logger("ppw01nockm_summary_generic::ppw01nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppw01nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppw01nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppw01nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppw01nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppw01nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppw01nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_wm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_wm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_wm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_wm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "du~_wm";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gu~_wmd~";
    subprocess[3] = "du~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gu~_wmd~";
    subprocess[3] = "du~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gd_wmu";
    subprocess[2] = "gu~_wmd~";
    subprocess[3] = "du~_wmg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppw01nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 1 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_wmud~";
    subprocess[2] = "gd_wmgu";
    subprocess[3] = "gu~_wmgd~";
    subprocess[4] = "dd_wmdu";
    subprocess[5] = "du_wmuu";
    subprocess[6] = "ds_wmdc";
    subprocess[7] = "dc_wmuc";
    subprocess[8] = "dd~_wmud~";
    subprocess[9] = "dd~_wmcs~";
    subprocess[10] = "du~_wmgg";
    subprocess[11] = "du~_wmdd~";
    subprocess[12] = "du~_wmuu~";
    subprocess[13] = "du~_wmss~";
    subprocess[14] = "du~_wmcc~";
    subprocess[15] = "ds~_wmus~";
    subprocess[16] = "dc~_wmds~";
    subprocess[17] = "dc~_wmuc~";
    subprocess[18] = "uu~_wmud~";
    subprocess[19] = "uu~_wmcs~";
    subprocess[20] = "uc~_wmus~";
    subprocess[21] = "d~u~_wmd~d~";
    subprocess[22] = "d~c~_wmd~s~";
    subprocess[23] = "u~u~_wmd~u~";
    subprocess[24] = "u~c~_wmd~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
