#include "header.hpp"

#include "ppw01nockm.phasespace.set.hpp"

ppw01nockm_phasespace_set::~ppw01nockm_phasespace_set(){
  static Logger logger("ppw01nockm_phasespace_set::~ppw01nockm_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::optimize_minv_born(){
  static Logger logger("ppw01nockm_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01nockm_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01nockm_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_010_dux_wm(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppw01nockm_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_010_dux_wm(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppw01nockm_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_010_dux_wm(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::optimize_minv_real(){
  static Logger logger("ppw01nockm_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01nockm_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01nockm_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_110_gd_wmu(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_110_gux_wmdx(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_110_dux_wmg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppw01nockm_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_110_gd_wmu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_110_gux_wmdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_110_dux_wmg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppw01nockm_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_110_gd_wmu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_110_gux_wmdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_110_dux_wmg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppw01nockm_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppw01nockm_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppw01nockm_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppw01nockm_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_210_gg_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_210_gd_wmgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_210_gux_wmgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_210_dd_wmdu(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_210_du_wmuu(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_210_ds_wmdc(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_210_dc_wmuc(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_210_ddx_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_210_ddx_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_210_dux_wmgg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_210_dux_wmddx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_210_dux_wmuux(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_210_dux_wmssx(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_210_dux_wmccx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_210_dsx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_210_dcx_wmdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_210_dcx_wmucx(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_210_uux_wmudx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_210_uux_wmcsx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_210_ucx_wmusx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_210_dxux_wmdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_210_dxcx_wmdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_210_uxux_wmdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_210_uxcx_wmdxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppw01nockm_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_210_gg_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_210_gd_wmgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_210_gux_wmgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_210_dd_wmdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_210_du_wmuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_210_ds_wmdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_210_dc_wmuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_210_ddx_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_210_ddx_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_210_dux_wmgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_210_dux_wmddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_210_dux_wmuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_210_dux_wmssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_210_dux_wmccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_210_dsx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_210_dcx_wmdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_210_dcx_wmucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_210_uux_wmudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_210_uux_wmcsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_210_ucx_wmusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_210_dxux_wmdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_210_dxcx_wmdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_210_uxux_wmdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_210_uxcx_wmdxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppw01nockm_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 1 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_210_gg_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_210_gd_wmgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_210_gux_wmgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_210_dd_wmdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_210_du_wmuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_210_ds_wmdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_210_dc_wmuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_210_ddx_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_210_ddx_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_210_dux_wmgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_210_dux_wmddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_210_dux_wmuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_210_dux_wmssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_210_dux_wmccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_210_dsx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_210_dcx_wmdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_210_dcx_wmucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_210_uux_wmudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_210_uux_wmcsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_210_ucx_wmusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_210_dxux_wmdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_210_dxcx_wmdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_210_uxux_wmdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_210_uxcx_wmdxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
