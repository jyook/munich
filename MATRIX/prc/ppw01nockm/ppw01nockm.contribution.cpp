#include "header.hpp"

#include "ppw01nockm.contribution.set.hpp"

ppw01nockm_contribution_set::~ppw01nockm_contribution_set(){
  static Logger logger("ppw01nockm_contribution_set::~ppw01nockm_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppw01nockm_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(4);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppw01nockm_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppw01nockm_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[4] = 4;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   2)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   4)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0)){no_process_parton[i_a] = 3; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppw01nockm_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   u    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> w   c    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> w   u    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> w   c    //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   dx   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> w   sx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> w   dx   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> w   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   g    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   g    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w   g    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w   g    //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppw01nockm_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[4] = 4; out[5] = 5;}
      if (o == 1){out[4] = 5; out[5] = 4;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   5)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==   4)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==   2)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   0) && (tp[out[5]] ==   0)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   5) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 19; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==   2) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==   4) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -5)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  24) && (tp[out[4]] ==  -1) && (tp[out[5]] ==  -4)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  24) && (tp[out[4]] ==  -3) && (tp[out[5]] ==  -2)){no_process_parton[i_a] = 24; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 19){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 1;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 1;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppw01nockm_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppw01nockm_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 1 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> w   u   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> w   g   u    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> w   g   c    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> w   g   u    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> w   g   c    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> w   g   dx   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> w   g   sx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> w   g   dx   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> w   g   sx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> w   d   u    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> w   s   c    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> w   u   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> w   c   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> w   u   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> w   c   c    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> w   d   c    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> w   u   s    //
      combination_pdf[2] = { 1,   5,   1};   // b   d    -> w   u   b    //
      combination_pdf[3] = { 1,   5,   3};   // b   s    -> w   c   b    //
      combination_pdf[4] = {-1,   3,   1};   // d   s    -> w   u   s    //
      combination_pdf[5] = {-1,   1,   3};   // s   d    -> w   d   c    //
      combination_pdf[6] = {-1,   5,   1};   // d   b    -> w   u   b    //
      combination_pdf[7] = {-1,   5,   3};   // s   b    -> w   c   b    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> w   u   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> w   u   c    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> w   u   c    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> w   u   c    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   u   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> w   c   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> w   u   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> w   c   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> w   u   dx   //
      combination_pdf[2] = { 1,   5,  -5};   // b   bx   -> w   u   dx   //
      combination_pdf[3] = { 1,   5,  -5};   // b   bx   -> w   c   sx   //
      combination_pdf[4] = {-1,   1,  -1};   // dx  d    -> w   c   sx   //
      combination_pdf[5] = {-1,   3,  -3};   // sx  s    -> w   u   dx   //
      combination_pdf[6] = {-1,   5,  -5};   // bx  b    -> w   u   dx   //
      combination_pdf[7] = {-1,   5,  -5};   // bx  b    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   g   g    //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   g   g    //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w   g   g    //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w   g   g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   d   dx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   s   sx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w   d   dx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w   s   sx   //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   u   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   c   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w   u   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w   c   cx   //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   s   sx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   d   dx   //
      combination_pdf[2] = { 1,   1,  -2};   // d   ux   -> w   b   bx   //
      combination_pdf[3] = { 1,   3,  -4};   // s   cx   -> w   b   bx   //
      combination_pdf[4] = {-1,   1,  -2};   // ux  d    -> w   s   sx   //
      combination_pdf[5] = {-1,   3,  -4};   // cx  s    -> w   d   dx   //
      combination_pdf[6] = {-1,   1,  -2};   // ux  d    -> w   b   bx   //
      combination_pdf[7] = {-1,   3,  -4};   // cx  s    -> w   b   bx   //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> w   c   cx   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> w   u   ux   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> w   c   cx   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> w   u   ux   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> w   u   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> w   c   dx   //
      combination_pdf[2] = { 1,   1,  -5};   // d   bx   -> w   u   bx   //
      combination_pdf[3] = { 1,   3,  -5};   // s   bx   -> w   c   bx   //
      combination_pdf[4] = {-1,   3,  -1};   // dx  s    -> w   c   dx   //
      combination_pdf[5] = {-1,   1,  -3};   // sx  d    -> w   u   sx   //
      combination_pdf[6] = {-1,   1,  -5};   // bx  d    -> w   u   bx   //
      combination_pdf[7] = {-1,   3,  -5};   // bx  s    -> w   c   bx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   d   sx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> w   s   dx   //
      combination_pdf[2] = { 1,   5,  -2};   // b   ux   -> w   b   dx   //
      combination_pdf[3] = { 1,   5,  -4};   // b   cx   -> w   b   sx   //
      combination_pdf[4] = {-1,   3,  -2};   // ux  s    -> w   s   dx   //
      combination_pdf[5] = {-1,   1,  -4};   // cx  d    -> w   d   sx   //
      combination_pdf[6] = {-1,   5,  -2};   // ux  b    -> w   b   dx   //
      combination_pdf[7] = {-1,   5,  -4};   // cx  b    -> w   b   sx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> w   u   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> w   c   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> w   c   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> w   u   cx   //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   u   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> w   c   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> w   u   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> w   c   sx   //
    }
    else if (no_process_parton[i_a] == 19){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> w   c   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> w   u   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> w   c   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> w   u   dx   //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> w   u   sx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> w   c   dx   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> w   c   dx   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> w   u   sx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> w   dx  dx   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> w   sx  sx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> w   dx  dx   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> w   sx  sx   //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(8);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> w   dx  sx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> w   dx  sx   //
      combination_pdf[2] = { 1,  -5,  -2};   // bx  ux   -> w   dx  bx   //
      combination_pdf[3] = { 1,  -5,  -4};   // bx  cx   -> w   sx  bx   //
      combination_pdf[4] = {-1,  -3,  -2};   // ux  sx   -> w   dx  sx   //
      combination_pdf[5] = {-1,  -1,  -4};   // cx  dx   -> w   dx  sx   //
      combination_pdf[6] = {-1,  -5,  -2};   // ux  bx   -> w   dx  bx   //
      combination_pdf[7] = {-1,  -5,  -4};   // cx  bx   -> w   sx  bx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> w   dx  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> w   sx  cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> w   dx  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> w   ux  sx   //
      combination_pdf[2] = {-1,  -4,  -2};   // ux  cx   -> w   ux  sx   //
      combination_pdf[3] = {-1,  -2,  -4};   // cx  ux   -> w   dx  cx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
