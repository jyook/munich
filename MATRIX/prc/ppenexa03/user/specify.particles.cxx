logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  user_particle[oset.user.particle_map["<particle_name>"]].push_back(<particle>);

  USERPARTICLE("Wrec").push_back(PARTICLE("lep")[0] + PARTICLE("nua")[0]);

}
logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
