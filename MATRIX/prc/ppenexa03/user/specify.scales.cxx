{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("lep")[0].momentum + PARTICLE("nua")[0].momentum + PARTICLE("photon")[0].momentum).m();
    // takes sum of momenta of hardest lepton PARTICLE("lep")[0].momentum, hardest neutrino PARTICLE("nua")[0].momentum, etc. 
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("lep")[0].momentum + PARTICLE("nua")[0].momentum + PARTICLE("photon")[0].momentum).m();
    double pT = (PARTICLE("lep")[0].momentum + PARTICLE("nua")[0].momentum + PARTICLE("photon")[0].momentum).pT();
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else if (sd == 3){
    // transverse mass (mT=ET=pT) of photon
    // transverse mass (mT=ET=pT) of photon
    temp_mu_central = PARTICLE("photon")[0].momentum.pT();
  }
  else if (sd == 4){
    // transverse mass of lepton+neutrino (from W decay)
    temp_mu_central = (PARTICLE("lep")[0].momentum + PARTICLE("nua")[0].momentum).ET();
  }
  else if (sd == 5){
    // geometric avarage of transverse masses of photon and lepton+neutrino (from W decay)
    temp_mu_central = sqrt(PARTICLE("photon")[0].pT * (PARTICLE("lep")[0].momentum + PARTICLE("nua")[0].momentum).ET());
  }
  else if (sd == 6){
    // quadratic sum of W mass and transverse mass of the photon
    temp_mu_central = sqrt(msi->M2_W + PARTICLE("photon")[0].momentum.pT2());
  }
  else if (sd == 7){
    double temp_pT = 0.;
    for (int i_l = 3; i_l < esi->p_parton[i_a].size(); i_l++){temp_pT = temp_pT + esi->p_parton[i_a][i_l].ET();}
    temp_mu_central = temp_pT;
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
