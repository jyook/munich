#include "header.hpp"

#include "ppemexmx04.summary.hpp"

ppemexmx04_summary_generic::ppemexmx04_summary_generic(munich * xmunich){
  Logger logger("ppemexmx04_summary_generic::ppemexmx04_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppemexmx04_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppemexmx04_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppemexmx04_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppemexmx04_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppemexmx04_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppemexmx04_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_born(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subprocess[4] = "aa_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subprocess[4] = "aa_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(5);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subprocess[4] = "aa_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_emmumepmup";
    subprocess[2] = "uu~_emmumepmup";
    subprocess[3] = "bb~_emmumepmup";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_emmumepmup";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmumepmupd";
    subprocess[2] = "gu_emmumepmupu";
    subprocess[3] = "gb_emmumepmupb";
    subprocess[4] = "gd~_emmumepmupd~";
    subprocess[5] = "gu~_emmumepmupu~";
    subprocess[6] = "gb~_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupg";
    subprocess[8] = "uu~_emmumepmupg";
    subprocess[9] = "bb~_emmumepmupg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_emmumepmupg";
    subprocess[2] = "gd_emmumepmupd";
    subprocess[3] = "gu_emmumepmupu";
    subprocess[4] = "gb_emmumepmupb";
    subprocess[5] = "gd~_emmumepmupd~";
    subprocess[6] = "gu~_emmumepmupu~";
    subprocess[7] = "gb~_emmumepmupb~";
    subprocess[8] = "dd~_emmumepmupg";
    subprocess[9] = "uu~_emmumepmupg";
    subprocess[10] = "bb~_emmumepmupg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "da_emmumepmupd";
    subprocess[2] = "ua_emmumepmupu";
    subprocess[3] = "ba_emmumepmupb";
    subprocess[4] = "d~a_emmumepmupd~";
    subprocess[5] = "u~a_emmumepmupu~";
    subprocess[6] = "b~a_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupa";
    subprocess[8] = "uu~_emmumepmupa";
    subprocess[9] = "bb~_emmumepmupa";
    subprocess[10] = "aa_emmumepmupa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmumepmupd";
    subprocess[2] = "gu_emmumepmupu";
    subprocess[3] = "gb_emmumepmupb";
    subprocess[4] = "gd~_emmumepmupd~";
    subprocess[5] = "gu~_emmumepmupu~";
    subprocess[6] = "gb~_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupg";
    subprocess[8] = "uu~_emmumepmupg";
    subprocess[9] = "bb~_emmumepmupg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emmumepmupd";
    subprocess[2] = "ua_emmumepmupu";
    subprocess[3] = "ba_emmumepmupb";
    subprocess[4] = "d~a_emmumepmupd~";
    subprocess[5] = "u~a_emmumepmupu~";
    subprocess[6] = "b~a_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupa";
    subprocess[8] = "uu~_emmumepmupa";
    subprocess[9] = "bb~_emmumepmupa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_emmumepmupd";
    subprocess[2] = "gu_emmumepmupu";
    subprocess[3] = "gb_emmumepmupb";
    subprocess[4] = "gd~_emmumepmupd~";
    subprocess[5] = "gu~_emmumepmupu~";
    subprocess[6] = "gb~_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupg";
    subprocess[8] = "uu~_emmumepmupg";
    subprocess[9] = "bb~_emmumepmupg";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 5 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_emmumepmupd";
    subprocess[2] = "ua_emmumepmupu";
    subprocess[3] = "ba_emmumepmupb";
    subprocess[4] = "d~a_emmumepmupd~";
    subprocess[5] = "u~a_emmumepmupu~";
    subprocess[6] = "b~a_emmumepmupb~";
    subprocess[7] = "dd~_emmumepmupa";
    subprocess[8] = "uu~_emmumepmupa";
    subprocess[9] = "bb~_emmumepmupa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppemexmx04_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_emmumepmupdd~";
    subprocess[2] = "gg_emmumepmupuu~";
    subprocess[3] = "gg_emmumepmupbb~";
    subprocess[4] = "gd_emmumepmupgd";
    subprocess[5] = "gu_emmumepmupgu";
    subprocess[6] = "gb_emmumepmupgb";
    subprocess[7] = "gd~_emmumepmupgd~";
    subprocess[8] = "gu~_emmumepmupgu~";
    subprocess[9] = "gb~_emmumepmupgb~";
    subprocess[10] = "dd_emmumepmupdd";
    subprocess[11] = "du_emmumepmupdu";
    subprocess[12] = "ds_emmumepmupds";
    subprocess[13] = "dc_emmumepmupdc";
    subprocess[14] = "db_emmumepmupdb";
    subprocess[15] = "dd~_emmumepmupgg";
    subprocess[16] = "dd~_emmumepmupdd~";
    subprocess[17] = "dd~_emmumepmupuu~";
    subprocess[18] = "dd~_emmumepmupss~";
    subprocess[19] = "dd~_emmumepmupcc~";
    subprocess[20] = "dd~_emmumepmupbb~";
    subprocess[21] = "du~_emmumepmupdu~";
    subprocess[22] = "ds~_emmumepmupds~";
    subprocess[23] = "dc~_emmumepmupdc~";
    subprocess[24] = "db~_emmumepmupdb~";
    subprocess[25] = "uu_emmumepmupuu";
    subprocess[26] = "uc_emmumepmupuc";
    subprocess[27] = "ub_emmumepmupub";
    subprocess[28] = "ud~_emmumepmupud~";
    subprocess[29] = "uu~_emmumepmupgg";
    subprocess[30] = "uu~_emmumepmupdd~";
    subprocess[31] = "uu~_emmumepmupuu~";
    subprocess[32] = "uu~_emmumepmupss~";
    subprocess[33] = "uu~_emmumepmupcc~";
    subprocess[34] = "uu~_emmumepmupbb~";
    subprocess[35] = "us~_emmumepmupus~";
    subprocess[36] = "uc~_emmumepmupuc~";
    subprocess[37] = "ub~_emmumepmupub~";
    subprocess[38] = "bb_emmumepmupbb";
    subprocess[39] = "bd~_emmumepmupbd~";
    subprocess[40] = "bu~_emmumepmupbu~";
    subprocess[41] = "bb~_emmumepmupgg";
    subprocess[42] = "bb~_emmumepmupdd~";
    subprocess[43] = "bb~_emmumepmupuu~";
    subprocess[44] = "bb~_emmumepmupbb~";
    subprocess[45] = "d~d~_emmumepmupd~d~";
    subprocess[46] = "d~u~_emmumepmupd~u~";
    subprocess[47] = "d~s~_emmumepmupd~s~";
    subprocess[48] = "d~c~_emmumepmupd~c~";
    subprocess[49] = "d~b~_emmumepmupd~b~";
    subprocess[50] = "u~u~_emmumepmupu~u~";
    subprocess[51] = "u~c~_emmumepmupu~c~";
    subprocess[52] = "u~b~_emmumepmupu~b~";
    subprocess[53] = "b~b~_emmumepmupb~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
