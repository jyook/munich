#include "header.hpp"

#include "ppllll04.amplitude.set.hpp"

#include "ppemexmx04.contribution.set.hpp"
#include "ppemexmx04.event.set.hpp"
#include "ppemexmx04.phasespace.set.hpp"
#include "ppemexmx04.observable.set.hpp"
#include "ppemexmx04.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-emmumepmup+X");

  MUC->csi = new ppemexmx04_contribution_set();
  MUC->esi = new ppemexmx04_event_set();
  MUC->psi = new ppemexmx04_phasespace_set();
  MUC->osi = new ppemexmx04_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppllll04_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppemexmx04_phasespace_set();
      MUC->psi->fake_psi->csi = new ppemexmx04_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppemexmx04_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
