#include "header.hpp"

#include "ppemexmx04.phasespace.set.hpp"

ppemexmx04_phasespace_set::~ppemexmx04_phasespace_set(){
  static Logger logger("ppemexmx04_phasespace_set::~ppemexmx04_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::optimize_minv_born(){
  static Logger logger("ppemexmx04_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppemexmx04_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 64;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 24;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 24;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 24;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 40;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 41;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppemexmx04_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-23);
      tau_MC_map.push_back(-36);
      tau_MC_map.push_back(-25);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_040_ddx_emmumepmup(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_040_uux_emmumepmup(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_040_bbx_emmumepmup(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_040_aa_emmumepmup(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_240_gg_emmumepmup(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppemexmx04_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_040_ddx_emmumepmup(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_040_uux_emmumepmup(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_040_bbx_emmumepmup(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_040_aa_emmumepmup(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_240_gg_emmumepmup(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppemexmx04_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_040_ddx_emmumepmup(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_040_uux_emmumepmup(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_040_bbx_emmumepmup(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_040_aa_emmumepmup(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_240_gg_emmumepmup(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::optimize_minv_real(){
  static Logger logger("ppemexmx04_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppemexmx04_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 88;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 56;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 56;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 56;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 56;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 552;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 168;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 240;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 342;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 57;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 57;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 57;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 57;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 57;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 57;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 57;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 57;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 57;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppemexmx04_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0, -23};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  6){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 14){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 16){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_140_gd_emmumepmupd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_140_gu_emmumepmupu(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_140_gb_emmumepmupb(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_140_gdx_emmumepmupdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_140_gux_emmumepmupux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_140_gbx_emmumepmupbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_140_ddx_emmumepmupg(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_140_uux_emmumepmupg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_140_bbx_emmumepmupg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_050_da_emmumepmupd(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_050_ua_emmumepmupu(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_050_ba_emmumepmupb(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_050_dxa_emmumepmupdx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_050_uxa_emmumepmupux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_050_bxa_emmumepmupbx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_050_ddx_emmumepmupa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_050_uux_emmumepmupa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_050_bbx_emmumepmupa(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_050_aa_emmumepmupa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_340_gg_emmumepmupg(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_340_gd_emmumepmupd(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_340_gu_emmumepmupu(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_340_gb_emmumepmupb(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_340_gdx_emmumepmupdx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_340_gux_emmumepmupux(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_340_gbx_emmumepmupbx(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_340_ddx_emmumepmupg(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_340_uux_emmumepmupg(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_340_bbx_emmumepmupg(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppemexmx04_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_140_gd_emmumepmupd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_140_gu_emmumepmupu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_140_gb_emmumepmupb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_140_gdx_emmumepmupdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_140_gux_emmumepmupux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_140_gbx_emmumepmupbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_140_ddx_emmumepmupg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_140_uux_emmumepmupg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_140_bbx_emmumepmupg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_050_da_emmumepmupd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_050_ua_emmumepmupu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_050_ba_emmumepmupb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_050_dxa_emmumepmupdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_050_uxa_emmumepmupux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_050_bxa_emmumepmupbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_050_ddx_emmumepmupa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_050_uux_emmumepmupa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_050_bbx_emmumepmupa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_050_aa_emmumepmupa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_340_gg_emmumepmupg(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_340_gd_emmumepmupd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_340_gu_emmumepmupu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_340_gb_emmumepmupb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_340_gdx_emmumepmupdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_340_gux_emmumepmupux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_340_gbx_emmumepmupbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_340_ddx_emmumepmupg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_340_uux_emmumepmupg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_340_bbx_emmumepmupg(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppemexmx04_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_140_gd_emmumepmupd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_140_gu_emmumepmupu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_140_gb_emmumepmupb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_140_gdx_emmumepmupdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_140_gux_emmumepmupux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_140_gbx_emmumepmupbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_140_ddx_emmumepmupg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_140_uux_emmumepmupg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_140_bbx_emmumepmupg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 5 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_050_da_emmumepmupd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_050_ua_emmumepmupu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_050_ba_emmumepmupb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_050_dxa_emmumepmupdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_050_uxa_emmumepmupux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_050_bxa_emmumepmupbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_050_ddx_emmumepmupa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_050_uux_emmumepmupa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_050_bbx_emmumepmupa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_050_aa_emmumepmupa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_340_gg_emmumepmupg(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_340_gd_emmumepmupd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_340_gu_emmumepmupu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_340_gb_emmumepmupb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_340_gdx_emmumepmupdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_340_gux_emmumepmupux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_340_gbx_emmumepmupbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_340_ddx_emmumepmupg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_340_uux_emmumepmupg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_340_bbx_emmumepmupg(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppemexmx04_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppemexmx04_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 584;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 248;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 248;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 248;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 248;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 248;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 248;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 288;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 144;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 288;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppemexmx04_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppemexmx04_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_240_gg_emmumepmupddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_240_gg_emmumepmupuux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_240_gg_emmumepmupbbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_240_gd_emmumepmupgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_240_gu_emmumepmupgu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_240_gb_emmumepmupgb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_240_gdx_emmumepmupgdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_240_gux_emmumepmupgux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_240_gbx_emmumepmupgbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_240_dd_emmumepmupdd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_240_du_emmumepmupdu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_240_ds_emmumepmupds(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_240_dc_emmumepmupdc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_240_db_emmumepmupdb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_240_ddx_emmumepmupgg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_240_ddx_emmumepmupddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_240_ddx_emmumepmupuux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_240_ddx_emmumepmupssx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_240_ddx_emmumepmupccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_240_ddx_emmumepmupbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_240_dux_emmumepmupdux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_240_dsx_emmumepmupdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_240_dcx_emmumepmupdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_240_dbx_emmumepmupdbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_240_uu_emmumepmupuu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_240_uc_emmumepmupuc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_240_ub_emmumepmupub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_240_udx_emmumepmupudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_240_uux_emmumepmupgg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_240_uux_emmumepmupddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_240_uux_emmumepmupuux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_240_uux_emmumepmupssx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_240_uux_emmumepmupccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_240_uux_emmumepmupbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_240_usx_emmumepmupusx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_240_ucx_emmumepmupucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_240_ubx_emmumepmupubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_240_bb_emmumepmupbb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_240_bdx_emmumepmupbdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_240_bux_emmumepmupbux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_240_bbx_emmumepmupgg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_240_bbx_emmumepmupddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_240_bbx_emmumepmupuux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_240_bbx_emmumepmupbbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_240_dxdx_emmumepmupdxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_240_dxux_emmumepmupdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_240_dxsx_emmumepmupdxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_240_dxcx_emmumepmupdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_240_dxbx_emmumepmupdxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_240_uxux_emmumepmupuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_240_uxcx_emmumepmupuxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_240_uxbx_emmumepmupuxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_240_bxbx_emmumepmupbxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppemexmx04_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_240_gg_emmumepmupddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_240_gg_emmumepmupuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_240_gg_emmumepmupbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_240_gd_emmumepmupgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_240_gu_emmumepmupgu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_240_gb_emmumepmupgb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_240_gdx_emmumepmupgdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_240_gux_emmumepmupgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_240_gbx_emmumepmupgbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_240_dd_emmumepmupdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_240_du_emmumepmupdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_240_ds_emmumepmupds(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_240_dc_emmumepmupdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_240_db_emmumepmupdb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_240_ddx_emmumepmupgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_240_ddx_emmumepmupddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_240_ddx_emmumepmupuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_240_ddx_emmumepmupssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_240_ddx_emmumepmupccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_240_ddx_emmumepmupbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_240_dux_emmumepmupdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_240_dsx_emmumepmupdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_240_dcx_emmumepmupdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_240_dbx_emmumepmupdbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_240_uu_emmumepmupuu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_240_uc_emmumepmupuc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_240_ub_emmumepmupub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_240_udx_emmumepmupudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_240_uux_emmumepmupgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_240_uux_emmumepmupddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_240_uux_emmumepmupuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_240_uux_emmumepmupssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_240_uux_emmumepmupccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_240_uux_emmumepmupbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_240_usx_emmumepmupusx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_240_ucx_emmumepmupucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_240_ubx_emmumepmupubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_240_bb_emmumepmupbb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_240_bdx_emmumepmupbdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_240_bux_emmumepmupbux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_240_bbx_emmumepmupgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_240_bbx_emmumepmupddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_240_bbx_emmumepmupuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_240_bbx_emmumepmupbbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_240_dxdx_emmumepmupdxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_240_dxux_emmumepmupdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_240_dxsx_emmumepmupdxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_240_dxcx_emmumepmupdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_240_dxbx_emmumepmupdxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_240_uxux_emmumepmupuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_240_uxcx_emmumepmupuxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_240_uxbx_emmumepmupuxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_240_bxbx_emmumepmupbxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppemexmx04_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppemexmx04_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_240_gg_emmumepmupddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_240_gg_emmumepmupuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_240_gg_emmumepmupbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_240_gd_emmumepmupgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_240_gu_emmumepmupgu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_240_gb_emmumepmupgb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_240_gdx_emmumepmupgdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_240_gux_emmumepmupgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_240_gbx_emmumepmupgbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_240_dd_emmumepmupdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_240_du_emmumepmupdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_240_ds_emmumepmupds(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_240_dc_emmumepmupdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_240_db_emmumepmupdb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_240_ddx_emmumepmupgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_240_ddx_emmumepmupddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_240_ddx_emmumepmupuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_240_ddx_emmumepmupssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_240_ddx_emmumepmupccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_240_ddx_emmumepmupbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_240_dux_emmumepmupdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_240_dsx_emmumepmupdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_240_dcx_emmumepmupdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_240_dbx_emmumepmupdbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_240_uu_emmumepmupuu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_240_uc_emmumepmupuc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_240_ub_emmumepmupub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_240_udx_emmumepmupudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_240_uux_emmumepmupgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_240_uux_emmumepmupddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_240_uux_emmumepmupuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_240_uux_emmumepmupssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_240_uux_emmumepmupccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_240_uux_emmumepmupbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_240_usx_emmumepmupusx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_240_ucx_emmumepmupucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_240_ubx_emmumepmupubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_240_bb_emmumepmupbb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_240_bdx_emmumepmupbdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_240_bux_emmumepmupbux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_240_bbx_emmumepmupgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_240_bbx_emmumepmupddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_240_bbx_emmumepmupuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_240_bbx_emmumepmupbbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_240_dxdx_emmumepmupdxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_240_dxux_emmumepmupdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_240_dxsx_emmumepmupdxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_240_dxcx_emmumepmupdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_240_dxbx_emmumepmupdxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_240_uxux_emmumepmupuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_240_uxcx_emmumepmupuxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_240_uxbx_emmumepmupuxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_240_bxbx_emmumepmupbxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
