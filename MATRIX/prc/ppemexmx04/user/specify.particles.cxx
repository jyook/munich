logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx started" << endl;
{
  //  particles ...
  //  vector<vector<particle> > 'user_particle' with all user-defined particle candidates is filled:
  //  USERPARTICLE("<particle_name>").push_back(<particle>);

  USERPARTICLE("Zrec").push_back(PARTICLE("em")[0] + PARTICLE("ep")[0]);
  USERPARTICLE("Zrec").push_back(PARTICLE("mum")[0] + PARTICLE("mup")[0]);

  vector<double> abs_M_leplep_MZ_combination(USERPARTICLE("Zrec").size());
  for (int i_z = 0; i_z < USERPARTICLE("Zrec").size(); i_z++){
    abs_M_leplep_MZ_combination[i_z] = abs(USERPARTICLE("Zrec")[i_z].m - msi->M_Z);
  }

  if (abs_M_leplep_MZ_combination[0] < abs_M_leplep_MZ_combination[1]){
    USERPARTICLE("Z1rec").push_back(USERPARTICLE("Zrec")[0]);
    USERPARTICLE("lmZ1").push_back(PARTICLE("em")[0]);
    USERPARTICLE("lpZ1").push_back(PARTICLE("ep")[0]);
    USERPARTICLE("Z2rec").push_back(USERPARTICLE("Zrec")[1]);
    USERPARTICLE("lmZ2").push_back(PARTICLE("mum")[0]);
    USERPARTICLE("lpZ2").push_back(PARTICLE("mup")[0]);
  } 
  else {
    USERPARTICLE("Z1rec").push_back(USERPARTICLE("Zrec")[1]);
    USERPARTICLE("lmZ1").push_back(PARTICLE("mum")[0]);
    USERPARTICLE("lpZ1").push_back(PARTICLE("mup")[0]);
    USERPARTICLE("Z2rec").push_back(USERPARTICLE("Zrec")[0]);
    USERPARTICLE("lmZ2").push_back(PARTICLE("em")[0]);
    USERPARTICLE("lpZ2").push_back(PARTICLE("ep")[0]);
  }

  if ((PARTICLE("em")[0] + PARTICLE("ep")[0]).pT > (PARTICLE("mum")[0] + PARTICLE("mup")[0]).pT){
    USERPARTICLE("lmZlead").push_back(PARTICLE("em")[0]);
    USERPARTICLE("lpZlead").push_back(PARTICLE("ep")[0]);
    USERPARTICLE("lmZsub").push_back(PARTICLE("mum")[0]);
    USERPARTICLE("lpZsub").push_back(PARTICLE("mup")[0]);
  }
  else {
    USERPARTICLE("lmZsub").push_back(PARTICLE("em")[0]);
    USERPARTICLE("lpZsub").push_back(PARTICLE("ep")[0]);
    USERPARTICLE("lmZlead").push_back(PARTICLE("mum")[0]);
    USERPARTICLE("lpZlead").push_back(PARTICLE("mup")[0]);
  }
}
logger << LOG_DEBUG_VERBOSE << "user/specify.particles.cxx ended" << endl;
