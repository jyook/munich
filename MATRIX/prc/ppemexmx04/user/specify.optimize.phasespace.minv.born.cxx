{
  static int switch_cut_M_leplep_OSSF = user->switch_value[user->switch_map["M_leplep_OSSF"]];
  static double cut_min_M_leplep_OSSF = user->cut_value[user->cut_map["min_M_leplep_OSSF"]];

  static int switch_cut_M_4lep = user->switch_value[user->switch_map["M_4lep"]];
  static double cut_min_M_4lep = user->cut_value[user->cut_map["min_M_4lep"]];

  static int switch_cut_delta_M_4lep = user->switch_value[user->switch_map["delta_M_4lep"]];
  static double cut_max_delta_M_4lep = user->cut_value[user->cut_map["max_delta_M_4lep"]];

  static int switch_cut_M_Zrec = user->switch_value[user->switch_map["M_Zrec"]];
  static double cut_min_M_Zrec = user->cut_value[user->cut_map["min_M_Zrec"]];

  logger << LOG_INFO << "switch_cut_M_Zrec = " << switch_cut_M_Zrec << "   cut_min_M_Zrec = " << cut_min_M_Zrec << endl;
  
  if (switch_cut_M_leplep_OSSF){sqrtsmin_opt[0][20] = cut_min_M_leplep_OSSF;}
  if (switch_cut_M_leplep_OSSF){sqrtsmin_opt[0][40] = cut_min_M_leplep_OSSF;}

  if (switch_cut_M_4lep){
    if (sqrtsmin_opt[0][60] < cut_min_M_4lep){
      sqrtsmin_opt[0][60] = cut_min_M_4lep;
    }
  }

  if (switch_cut_delta_M_4lep){
    if (sqrtsmin_opt[0][60] < M[23] - cut_max_delta_M_4lep){
      sqrtsmin_opt[0][60] = M[23] - cut_max_delta_M_4lep;
    }
  }

  if (switch_cut_M_Zrec){
    if (sqrtsmin_opt[0][20] < cut_min_M_Zrec){
      sqrtsmin_opt[0][20] = cut_min_M_Zrec;
    }
  }
  if (switch_cut_M_Zrec){
    if (sqrtsmin_opt[0][40] < cut_min_M_Zrec){
      sqrtsmin_opt[0][40] = cut_min_M_Zrec;
    }
  }

  logger << LOG_INFO << "sqrtsmin_opt[0][20] = " << sqrtsmin_opt[0][20] << endl;
  logger << LOG_INFO << "sqrtsmin_opt[0][40] = " << sqrtsmin_opt[0][40] << endl;
  
}
