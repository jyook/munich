{
  if (sd == 1){
    // invariant mass m of the colourless system
    temp_mu_central = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("lep")[3].momentum).m();
    // takes sum of momenta of hardest lepton PARTICLE("lep")[0].momentum, second-hardest lepton PARTICLE("lep")[1].momentum, etc. 
    // and computes their invariant mass (***).m()
  }
  else if (sd == 2){
    // transverse mass of the colourless system
    double m  = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("lep")[3].momentum).m();
    double pT = (PARTICLE("lep")[0].momentum + PARTICLE("lep")[1].momentum + PARTICLE("lep")[2].momentum + PARTICLE("lep")[3].momentum).pT();
    temp_mu_central = sqrt(pow(m, 2) + pow(pT, 2));
    // quadratic sum of invariant mass (***).m() and transverse momentum (***).pT()
  }
  else if (sd == 3){
    // geometric average of transverse masses of Z bosons
    double ET_Z1 = PARTICLE("Z1rec")[0].ET;
    double ET_Z2 = PARTICLE("Z2rec")[0].ET;
    temp_mu_central = sqrt(ET_Z1 * ET_Z2);
  }
  else if (sd == 4){
    // sum of transverse masses of Z bosons with the invariant masses replaced by their pole mass
    double ET_Z1 = sqrt(msi->M2_Z + PARTICLE("Z1rec")[0].pT2);
    double ET_Z2 = sqrt(msi->M2_Z + PARTICLE("Z2rec")[0].pT2);
    temp_mu_central = (ET_Z1 + ET_Z2);
  }
  else if (sd == 5){
    // sum of transverse masses of Z bosons
    double ET_Z1 = PARTICLE("Z1rec")[0].ET;
    double ET_Z2 = PARTICLE("Z2rec")[0].ET;
    temp_mu_central =(ET_Z1 + ET_Z2);
  }
  else{
    assert(false && "Scale setting not defined. Reset dynamical scale to different value.");
  }
}
