#include "header.hpp"

#include "ppexneh03nockm.summary.hpp"

ppexneh03nockm_summary_generic::ppexneh03nockm_summary_generic(munich * xmunich){
  Logger logger("ppexneh03nockm_summary_generic::ppexneh03nockm_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppexneh03nockm_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppexneh03nockm_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppexneh03nockm_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppexneh03nockm_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppexneh03nockm_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppexneh03nockm_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_born(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "ud~_epveh";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gd~_epvehu~";
    subprocess[3] = "ud~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "d~a_epvehu~";
    subprocess[3] = "ud~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gd~_epvehu~";
    subprocess[3] = "ud~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "d~a_epvehu~";
    subprocess[3] = "ud~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "gu_epvehd";
    subprocess[2] = "gd~_epvehu~";
    subprocess[3] = "ud~_epvehg";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 4 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "ua_epvehd";
    subprocess[2] = "d~a_epvehu~";
    subprocess[3] = "ud~_epveha";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppexneh03nockm_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(25);
    subprocess[1] = "gg_epvehdu~";
    subprocess[2] = "gu_epvehgd";
    subprocess[3] = "gd~_epvehgu~";
    subprocess[4] = "du_epvehdd";
    subprocess[5] = "dc_epvehds";
    subprocess[6] = "dd~_epvehdu~";
    subprocess[7] = "dd~_epvehsc~";
    subprocess[8] = "ds~_epvehdc~";
    subprocess[9] = "uu_epvehdu";
    subprocess[10] = "uc_epvehdc";
    subprocess[11] = "ud~_epvehgg";
    subprocess[12] = "ud~_epvehdd~";
    subprocess[13] = "ud~_epvehuu~";
    subprocess[14] = "ud~_epvehss~";
    subprocess[15] = "ud~_epvehcc~";
    subprocess[16] = "uu~_epvehdu~";
    subprocess[17] = "uu~_epvehsc~";
    subprocess[18] = "us~_epvehds~";
    subprocess[19] = "us~_epvehuc~";
    subprocess[20] = "uc~_epvehdc~";
    subprocess[21] = "d~d~_epvehd~u~";
    subprocess[22] = "d~u~_epvehu~u~";
    subprocess[23] = "d~s~_epvehd~c~";
    subprocess[24] = "d~c~_epvehu~c~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
