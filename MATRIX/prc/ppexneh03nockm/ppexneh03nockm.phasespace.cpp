#include "header.hpp"

#include "ppexneh03nockm.phasespace.set.hpp"

ppexneh03nockm_phasespace_set::~ppexneh03nockm_phasespace_set(){
  static Logger logger("ppexneh03nockm_phasespace_set::~ppexneh03nockm_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::optimize_minv_born(){
  static Logger logger("ppexneh03nockm_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03nockm_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 1;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 1;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_030_udx_epveh(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_030_udx_epveh(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppexneh03nockm_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_030_udx_epveh(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::optimize_minv_real(){
  static Logger logger("ppexneh03nockm_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03nockm_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 3;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 2){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 9;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 5;}
    else if (csi->no_process_parton[x_a] == 6){n_channel = 5;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 6){tau_MC_map = vector<int> {  0, -24};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_130_gu_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] == 2){ax_psp_130_gdx_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_130_udx_epvehg(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_040_ua_epvehd(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_040_dxa_epvehux(x_a);}
    else if (csi->no_process_parton[x_a] == 6){ax_psp_040_udx_epveha(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_130_gu_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 2){ac_psp_130_gdx_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_130_udx_epvehg(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_040_ua_epvehd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_040_dxa_epvehux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 6){ac_psp_040_udx_epveha(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppexneh03nockm_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_130_gu_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 2){ag_psp_130_gdx_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_130_udx_epvehg(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 4 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_040_ua_epvehd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_040_dxa_epvehux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 6){ag_psp_040_udx_epveha(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppexneh03nockm_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppexneh03nockm_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 15;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 8;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 4;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 2;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 8;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 19){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 4;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 2;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 19){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppexneh03nockm_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_230_gg_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_230_gu_epvehgd(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_230_gdx_epvehgux(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_230_du_epvehdd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_230_dc_epvehds(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_230_ddx_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_230_ddx_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_230_dsx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_230_uu_epvehdu(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_230_uc_epvehdc(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_230_udx_epvehgg(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_230_udx_epvehddx(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_230_udx_epvehuux(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_230_udx_epvehssx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_230_udx_epvehccx(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_230_uux_epvehdux(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_230_uux_epvehscx(x_a);}
    else if (csi->no_process_parton[x_a] == 19){ax_psp_230_usx_epvehdsx(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_230_usx_epvehucx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_230_ucx_epvehdcx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_230_dxdx_epvehdxux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_230_dxux_epvehuxux(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_230_dxsx_epvehdxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_230_dxcx_epvehuxcx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppexneh03nockm_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_230_gg_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_230_gu_epvehgd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_230_gdx_epvehgux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_230_du_epvehdd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_230_dc_epvehds(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_230_ddx_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_230_ddx_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_230_dsx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_230_uu_epvehdu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_230_uc_epvehdc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_230_udx_epvehgg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_230_udx_epvehddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_230_udx_epvehuux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_230_udx_epvehssx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_230_udx_epvehccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_230_uux_epvehdux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_230_uux_epvehscx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 19){ac_psp_230_usx_epvehdsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_230_usx_epvehucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_230_ucx_epvehdcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_230_dxdx_epvehdxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_230_dxux_epvehuxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_230_dxsx_epvehdxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_230_dxcx_epvehuxcx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppexneh03nockm_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppexneh03nockm_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_230_gg_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_230_gu_epvehgd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_230_gdx_epvehgux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_230_du_epvehdd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_230_dc_epvehds(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_230_ddx_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_230_ddx_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_230_dsx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_230_uu_epvehdu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_230_uc_epvehdc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_230_udx_epvehgg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_230_udx_epvehddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_230_udx_epvehuux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_230_udx_epvehssx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_230_udx_epvehccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_230_uux_epvehdux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_230_uux_epvehscx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 19){ag_psp_230_usx_epvehdsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_230_usx_epvehucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_230_ucx_epvehdcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_230_dxdx_epvehdxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_230_dxux_epvehuxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_230_dxsx_epvehdxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_230_dxcx_epvehuxcx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
