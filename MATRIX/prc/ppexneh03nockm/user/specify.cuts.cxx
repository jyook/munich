logger << LOG_DEBUG_VERBOSE << "user/specify.cuts.cxx started" << endl;
{
  static int switch_cut_R_lepjet = USERSWITCH("R_lepjet");
  static double cut_min_R_lepjet = USERCUT("min_R_lepjet");
  static double cut_min_R2_lepjet = pow(cut_min_R_lepjet, 2);
  


  for (int i_j = 0; i_j < NUMBER("jet"); i_j++){
    // lepton--jet isolation cuts
    if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   switch_cut_R_lepjet = " << switch_cut_R_lepjet << endl;}
    if (switch_cut_R_lepjet == 1){
      for (int i_l = 0; i_l < PARTICLE("lep").size(); i_l++){
	double R2_eta_lepjet = R2_eta(PARTICLE("lep")[i_l], PARTICLE("jet")[i_j]);

	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(lep)[" << i_l << "] = " << PARTICLE("lep")[i_l].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   PARTICLE(jet)[" << i_j << "] = " << PARTICLE("jet")[i_j].momentum << endl;}
	if (switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   R2_eta_lepjet = " << R2_eta_lepjet << " > " << cut_min_R2_lepjet << endl;}

	if (R2_eta_lepjet < cut_min_R2_lepjet){
	  cut_ps[i_a] = -1; 
	  if (switch_output_cutinfo){
	    info_cut << "[" << setw(2) << i_a << "]" << "   ppeexa03-cut after cut_R_lepjet" << endl; 
	    logger << LOG_DEBUG << endl << info_cut.str(); 
	  }
	  logger << LOG_DEBUG_VERBOSE << "switch_cut_R_lepjet cut applied" << endl; 
	  return;
	}
      }
    }
  }
    
  if (switch_output_cutinfo){info_cut << "individual cuts passed" << endl;}

}
