#include "header.hpp"

#include "ppaa02.contribution.set.hpp"

ppaa02_contribution_set::~ppaa02_contribution_set(){
  static Logger logger("ppaa02_contribution_set::~ppaa02_contribution_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::determination_subprocess_born(int i_a){
  static Logger logger("ppaa02_contribution_set::determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(5);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 5; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22)){no_process_parton[i_a] = 1; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::combination_subprocess_born(int i_a){
  static Logger logger("ppaa02_contribution_set::combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> a   a   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::determination_subprocess_real(int i_a){
  static Logger logger("ppaa02_contribution_set::determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[5] = 5;}
      if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   2)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   4)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   5)){no_process_parton[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -5)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  22)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  22)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  22)){no_process_parton[i_a] = 23; break;}
      }
      else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   1)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   3)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   2)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   4)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   5)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -1)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -3)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -2)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -4)){no_process_parton[i_a] = 11; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==  -5)){no_process_parton[i_a] = 12; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 13; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[5] ==   0)){no_process_parton[i_a] = 17; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 14){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 6;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 6;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 6;}
    symmetry_factor = symmetry_id_factor[i_a];
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 11){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 12){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 13){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 2;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::combination_subprocess_real(int i_a){
  static Logger logger("ppaa02_contribution_set::combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 1 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> a   a   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> a   a   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> a   a   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> a   a   s    //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> a   a   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> a   a   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> a   a   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> a   a   c    //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> a   a   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> a   a   b    //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> a   a   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> a   a   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> a   a   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> a   a   sx   //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> a   a   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> a   a   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> a   a   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> a   a   cx   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> a   a   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> a   a   bx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   g    //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   g    //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a   g    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 0 && phasespace_order_alpha_e[i_a] == 3 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   7};   // d   a    -> a   a   d    //
      combination_pdf[1] = { 1,   3,   7};   // s   a    -> a   a   s    //
      combination_pdf[2] = {-1,   1,   7};   // a   d    -> a   a   d    //
      combination_pdf[3] = {-1,   3,   7};   // a   s    -> a   a   s    //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   7};   // u   a    -> a   a   u    //
      combination_pdf[1] = { 1,   4,   7};   // c   a    -> a   a   c    //
      combination_pdf[2] = {-1,   2,   7};   // a   u    -> a   a   u    //
      combination_pdf[3] = {-1,   4,   7};   // a   c    -> a   a   c    //
    }
    else if (no_process_parton[i_a] == 14){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,   7};   // b   a    -> a   a   b    //
      combination_pdf[1] = {-1,   5,   7};   // a   b    -> a   a   b    //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,   7};   // dx  a    -> a   a   dx   //
      combination_pdf[1] = { 1,  -3,   7};   // sx  a    -> a   a   sx   //
      combination_pdf[2] = {-1,  -1,   7};   // a   dx   -> a   a   dx   //
      combination_pdf[3] = {-1,  -3,   7};   // a   sx   -> a   a   sx   //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,   7};   // ux  a    -> a   a   ux   //
      combination_pdf[1] = { 1,  -4,   7};   // cx  a    -> a   a   cx   //
      combination_pdf[2] = {-1,  -2,   7};   // a   ux   -> a   a   ux   //
      combination_pdf[3] = {-1,  -4,   7};   // a   cx   -> a   a   cx   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -5,   7};   // bx  a    -> a   a   bx   //
      combination_pdf[1] = {-1,  -5,   7};   // a   bx   -> a   a   bx   //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   a    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   a    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   a    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   a    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   a    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   a    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   a    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   a    //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   a    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a   a    //
    }
  }
  else if (phasespace_order_alpha_s[i_a] == 3 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 1){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> a   a   g   //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> a   a   d   //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> a   a   s   //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> a   a   d   //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> a   a   s   //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> a   a   u   //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> a   a   c   //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> a   a   u   //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> a   a   c   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> a   a   b   //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> a   a   b   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> a   a   dx  //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> a   a   sx  //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> a   a   dx  //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> a   a   sx  //
    }
    else if (no_process_parton[i_a] == 11){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> a   a   ux  //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> a   a   cx  //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> a   a   ux  //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> a   a   cx  //
    }
    else if (no_process_parton[i_a] == 12){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> a   a   bx  //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> a   a   bx  //
    }
    else if (no_process_parton[i_a] == 13){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   g   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   g   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   g   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   g   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   g   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   g   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   g   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   g   //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   g   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a   g   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::determination_subprocess_doublereal(int i_a){
  static Logger logger("ppaa02_contribution_set::determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  const vector<int> tp = basic_type_parton[i_a];

  no_process_parton[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[5] = 5; out[6] = 6;}
      if (o == 1){out[5] = 6; out[6] = 5;}
      if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 10; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 15; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 16; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==   3)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==   1)){no_process_parton[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 18; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 20; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 21; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 22; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 23; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 24; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 25; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 26; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 27; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 29; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 31; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 32; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 34; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==   4)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==   2)){no_process_parton[i_a] = 35; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 36; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 37; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 39; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 40; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 41; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 42; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 43; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 44; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 45; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 47; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 48; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==   5)){no_process_parton[i_a] = 50; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 51; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 52; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   0) && (tp[out[6]] ==   0)){no_process_parton[i_a] = 53; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 54; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 55; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==   5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 56; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 58; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 59; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -3)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -1)){no_process_parton[i_a] = 60; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 61; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -1) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -3) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 63; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 65; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -4)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -2)){no_process_parton[i_a] = 66; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -2) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -4) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 67; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  22) && (tp[4] ==  22) && (tp[out[5]] ==  -5) && (tp[out[6]] ==  -5)){no_process_parton[i_a] = 69; break;}
      }
    }
    if (no_process_parton[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){swap_parton[i_a][o] = o;}
    else {swap_parton[i_a][o] = out[o];}
  }
  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 3){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 4){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 5){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 6){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 7){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 8){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 9){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 10){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 15){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 16){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 17){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 18){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 20){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 21){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 22){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 23){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 24){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 25){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 26){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 27){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 29){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 31){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 32){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 34){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 35){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 36){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 37){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 39){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 40){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 41){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 42){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 43){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 44){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 45){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 47){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 48){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 50){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 51){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 52){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 53){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 54){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 55){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 56){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 58){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 59){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 60){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 61){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 63){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 65){symmetry_id_factor[i_a] = 4;}
    else if (no_process_parton[i_a] == 66){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 67){symmetry_id_factor[i_a] = 2;}
    else if (no_process_parton[i_a] == 69){symmetry_id_factor[i_a] = 4;}
    symmetry_factor = symmetry_id_factor[i_a];
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_contribution_set::combination_subprocess_doublereal(int i_a){
  static Logger logger("ppaa02_contribution_set::combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (phasespace_order_alpha_s[i_a] == 2 && phasespace_order_alpha_e[i_a] == 2 && phasespace_order_interference[i_a] == 0){
    if (no_process_parton[i_a] == 0){}
    else if (no_process_parton[i_a] == 2){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> a   a   d   dx   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> a   a   s   sx   //
    }
    else if (no_process_parton[i_a] == 3){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> a   a   u   ux   //
      combination_pdf[1] = { 1,   0,   0};   // g   g    -> a   a   c   cx   //
    }
    else if (no_process_parton[i_a] == 4){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   0,   0};   // g   g    -> a   a   b   bx   //
    }
    else if (no_process_parton[i_a] == 5){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   1};   // g   d    -> a   a   g   d    //
      combination_pdf[1] = { 1,   0,   3};   // g   s    -> a   a   g   s    //
      combination_pdf[2] = {-1,   0,   1};   // d   g    -> a   a   g   d    //
      combination_pdf[3] = {-1,   0,   3};   // s   g    -> a   a   g   s    //
    }
    else if (no_process_parton[i_a] == 6){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,   2};   // g   u    -> a   a   g   u    //
      combination_pdf[1] = { 1,   0,   4};   // g   c    -> a   a   g   c    //
      combination_pdf[2] = {-1,   0,   2};   // u   g    -> a   a   g   u    //
      combination_pdf[3] = {-1,   0,   4};   // c   g    -> a   a   g   c    //
    }
    else if (no_process_parton[i_a] == 7){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,   5};   // g   b    -> a   a   g   b    //
      combination_pdf[1] = {-1,   0,   5};   // b   g    -> a   a   g   b    //
    }
    else if (no_process_parton[i_a] == 8){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -1};   // g   dx   -> a   a   g   dx   //
      combination_pdf[1] = { 1,   0,  -3};   // g   sx   -> a   a   g   sx   //
      combination_pdf[2] = {-1,   0,  -1};   // dx  g    -> a   a   g   dx   //
      combination_pdf[3] = {-1,   0,  -3};   // sx  g    -> a   a   g   sx   //
    }
    else if (no_process_parton[i_a] == 9){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   0,  -2};   // g   ux   -> a   a   g   ux   //
      combination_pdf[1] = { 1,   0,  -4};   // g   cx   -> a   a   g   cx   //
      combination_pdf[2] = {-1,   0,  -2};   // ux  g    -> a   a   g   ux   //
      combination_pdf[3] = {-1,   0,  -4};   // cx  g    -> a   a   g   cx   //
    }
    else if (no_process_parton[i_a] == 10){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   0,  -5};   // g   bx   -> a   a   g   bx   //
      combination_pdf[1] = {-1,   0,  -5};   // bx  g    -> a   a   g   bx   //
    }
    else if (no_process_parton[i_a] == 15){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   1};   // d   d    -> a   a   d   d    //
      combination_pdf[1] = { 1,   3,   3};   // s   s    -> a   a   s   s    //
    }
    else if (no_process_parton[i_a] == 16){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   2};   // d   u    -> a   a   d   u    //
      combination_pdf[1] = { 1,   3,   4};   // s   c    -> a   a   s   c    //
      combination_pdf[2] = {-1,   1,   2};   // u   d    -> a   a   d   u    //
      combination_pdf[3] = {-1,   3,   4};   // c   s    -> a   a   s   c    //
    }
    else if (no_process_parton[i_a] == 17){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   1,   3};   // d   s    -> a   a   d   s    //
      combination_pdf[1] = { 1,   3,   1};   // s   d    -> a   a   d   s    //
    }
    else if (no_process_parton[i_a] == 18){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   4};   // d   c    -> a   a   d   c    //
      combination_pdf[1] = { 1,   3,   2};   // s   u    -> a   a   u   s    //
      combination_pdf[2] = {-1,   3,   2};   // u   s    -> a   a   u   s    //
      combination_pdf[3] = {-1,   1,   4};   // c   d    -> a   a   d   c    //
    }
    else if (no_process_parton[i_a] == 20){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,   5};   // d   b    -> a   a   d   b    //
      combination_pdf[1] = { 1,   3,   5};   // s   b    -> a   a   s   b    //
      combination_pdf[2] = {-1,   1,   5};   // b   d    -> a   a   d   b    //
      combination_pdf[3] = {-1,   3,   5};   // b   s    -> a   a   s   b    //
    }
    else if (no_process_parton[i_a] == 21){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   g   g    //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   g   g    //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   g   g    //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   g   g    //
    }
    else if (no_process_parton[i_a] == 22){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   d   dx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   s   sx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   d   dx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   s   sx   //
    }
    else if (no_process_parton[i_a] == 23){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   u   ux   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   c   cx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   u   ux   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   c   cx   //
    }
    else if (no_process_parton[i_a] == 24){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   s   sx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   d   dx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   s   sx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   d   dx   //
    }
    else if (no_process_parton[i_a] == 25){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   c   cx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   u   ux   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   c   cx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   u   ux   //
    }
    else if (no_process_parton[i_a] == 26){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -1};   // d   dx   -> a   a   b   bx   //
      combination_pdf[1] = { 1,   3,  -3};   // s   sx   -> a   a   b   bx   //
      combination_pdf[2] = {-1,   1,  -1};   // dx  d    -> a   a   b   bx   //
      combination_pdf[3] = {-1,   3,  -3};   // sx  s    -> a   a   b   bx   //
    }
    else if (no_process_parton[i_a] == 27){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -2};   // d   ux   -> a   a   d   ux   //
      combination_pdf[1] = { 1,   3,  -4};   // s   cx   -> a   a   s   cx   //
      combination_pdf[2] = {-1,   1,  -2};   // ux  d    -> a   a   d   ux   //
      combination_pdf[3] = {-1,   3,  -4};   // cx  s    -> a   a   s   cx   //
    }
    else if (no_process_parton[i_a] == 29){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -3};   // d   sx   -> a   a   d   sx   //
      combination_pdf[1] = { 1,   3,  -1};   // s   dx   -> a   a   s   dx   //
      combination_pdf[2] = {-1,   3,  -1};   // dx  s    -> a   a   s   dx   //
      combination_pdf[3] = {-1,   1,  -3};   // sx  d    -> a   a   d   sx   //
    }
    else if (no_process_parton[i_a] == 31){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -4};   // d   cx   -> a   a   d   cx   //
      combination_pdf[1] = { 1,   3,  -2};   // s   ux   -> a   a   s   ux   //
      combination_pdf[2] = {-1,   3,  -2};   // ux  s    -> a   a   s   ux   //
      combination_pdf[3] = {-1,   1,  -4};   // cx  d    -> a   a   d   cx   //
    }
    else if (no_process_parton[i_a] == 32){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   1,  -5};   // d   bx   -> a   a   d   bx   //
      combination_pdf[1] = { 1,   3,  -5};   // s   bx   -> a   a   s   bx   //
      combination_pdf[2] = {-1,   1,  -5};   // bx  d    -> a   a   d   bx   //
      combination_pdf[3] = {-1,   3,  -5};   // bx  s    -> a   a   s   bx   //
    }
    else if (no_process_parton[i_a] == 34){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   2};   // u   u    -> a   a   u   u    //
      combination_pdf[1] = { 1,   4,   4};   // c   c    -> a   a   c   c    //
    }
    else if (no_process_parton[i_a] == 35){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   2,   4};   // u   c    -> a   a   u   c    //
      combination_pdf[1] = { 1,   4,   2};   // c   u    -> a   a   u   c    //
    }
    else if (no_process_parton[i_a] == 36){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,   5};   // u   b    -> a   a   u   b    //
      combination_pdf[1] = { 1,   4,   5};   // c   b    -> a   a   c   b    //
      combination_pdf[2] = {-1,   2,   5};   // b   u    -> a   a   u   b    //
      combination_pdf[3] = {-1,   4,   5};   // b   c    -> a   a   c   b    //
    }
    else if (no_process_parton[i_a] == 37){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -1};   // u   dx   -> a   a   u   dx   //
      combination_pdf[1] = { 1,   4,  -3};   // c   sx   -> a   a   c   sx   //
      combination_pdf[2] = {-1,   2,  -1};   // dx  u    -> a   a   u   dx   //
      combination_pdf[3] = {-1,   4,  -3};   // sx  c    -> a   a   c   sx   //
    }
    else if (no_process_parton[i_a] == 39){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   g   g    //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   g   g    //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   g   g    //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   g   g    //
    }
    else if (no_process_parton[i_a] == 40){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   d   dx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   s   sx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   d   dx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   s   sx   //
    }
    else if (no_process_parton[i_a] == 41){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   u   ux   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   c   cx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   u   ux   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   c   cx   //
    }
    else if (no_process_parton[i_a] == 42){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   s   sx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   d   dx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   s   sx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   d   dx   //
    }
    else if (no_process_parton[i_a] == 43){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   c   cx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   u   ux   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   c   cx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   u   ux   //
    }
    else if (no_process_parton[i_a] == 44){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -2};   // u   ux   -> a   a   b   bx   //
      combination_pdf[1] = { 1,   4,  -4};   // c   cx   -> a   a   b   bx   //
      combination_pdf[2] = {-1,   2,  -2};   // ux  u    -> a   a   b   bx   //
      combination_pdf[3] = {-1,   4,  -4};   // cx  c    -> a   a   b   bx   //
    }
    else if (no_process_parton[i_a] == 45){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -3};   // u   sx   -> a   a   u   sx   //
      combination_pdf[1] = { 1,   4,  -1};   // c   dx   -> a   a   c   dx   //
      combination_pdf[2] = {-1,   4,  -1};   // dx  c    -> a   a   c   dx   //
      combination_pdf[3] = {-1,   2,  -3};   // sx  u    -> a   a   u   sx   //
    }
    else if (no_process_parton[i_a] == 47){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -4};   // u   cx   -> a   a   u   cx   //
      combination_pdf[1] = { 1,   4,  -2};   // c   ux   -> a   a   c   ux   //
      combination_pdf[2] = {-1,   4,  -2};   // ux  c    -> a   a   c   ux   //
      combination_pdf[3] = {-1,   2,  -4};   // cx  u    -> a   a   u   cx   //
    }
    else if (no_process_parton[i_a] == 48){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   2,  -5};   // u   bx   -> a   a   u   bx   //
      combination_pdf[1] = { 1,   4,  -5};   // c   bx   -> a   a   c   bx   //
      combination_pdf[2] = {-1,   2,  -5};   // bx  u    -> a   a   u   bx   //
      combination_pdf[3] = {-1,   4,  -5};   // bx  c    -> a   a   c   bx   //
    }
    else if (no_process_parton[i_a] == 50){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,   5,   5};   // b   b    -> a   a   b   b    //
    }
    else if (no_process_parton[i_a] == 51){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -1};   // b   dx   -> a   a   b   dx   //
      combination_pdf[1] = { 1,   5,  -3};   // b   sx   -> a   a   b   sx   //
      combination_pdf[2] = {-1,   5,  -1};   // dx  b    -> a   a   b   dx   //
      combination_pdf[3] = {-1,   5,  -3};   // sx  b    -> a   a   b   sx   //
    }
    else if (no_process_parton[i_a] == 52){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -2};   // b   ux   -> a   a   b   ux   //
      combination_pdf[1] = { 1,   5,  -4};   // b   cx   -> a   a   b   cx   //
      combination_pdf[2] = {-1,   5,  -2};   // ux  b    -> a   a   b   ux   //
      combination_pdf[3] = {-1,   5,  -4};   // cx  b    -> a   a   b   cx   //
    }
    else if (no_process_parton[i_a] == 53){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   g   g    //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a   g   g    //
    }
    else if (no_process_parton[i_a] == 54){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   d   dx   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> a   a   s   sx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> a   a   d   dx   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> a   a   s   sx   //
    }
    else if (no_process_parton[i_a] == 55){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   u   ux   //
      combination_pdf[1] = { 1,   5,  -5};   // b   bx   -> a   a   c   cx   //
      combination_pdf[2] = {-1,   5,  -5};   // bx  b    -> a   a   u   ux   //
      combination_pdf[3] = {-1,   5,  -5};   // bx  b    -> a   a   c   cx   //
    }
    else if (no_process_parton[i_a] == 56){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,   5,  -5};   // b   bx   -> a   a   b   bx   //
      combination_pdf[1] = {-1,   5,  -5};   // bx  b    -> a   a   b   bx   //
    }
    else if (no_process_parton[i_a] == 58){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -1};   // dx  dx   -> a   a   dx  dx   //
      combination_pdf[1] = { 1,  -3,  -3};   // sx  sx   -> a   a   sx  sx   //
    }
    else if (no_process_parton[i_a] == 59){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -2};   // dx  ux   -> a   a   dx  ux   //
      combination_pdf[1] = { 1,  -3,  -4};   // sx  cx   -> a   a   sx  cx   //
      combination_pdf[2] = {-1,  -1,  -2};   // ux  dx   -> a   a   dx  ux   //
      combination_pdf[3] = {-1,  -3,  -4};   // cx  sx   -> a   a   sx  cx   //
    }
    else if (no_process_parton[i_a] == 60){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -1,  -3};   // dx  sx   -> a   a   dx  sx   //
      combination_pdf[1] = { 1,  -3,  -1};   // sx  dx   -> a   a   dx  sx   //
    }
    else if (no_process_parton[i_a] == 61){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -4};   // dx  cx   -> a   a   dx  cx   //
      combination_pdf[1] = { 1,  -3,  -2};   // sx  ux   -> a   a   ux  sx   //
      combination_pdf[2] = {-1,  -3,  -2};   // ux  sx   -> a   a   ux  sx   //
      combination_pdf[3] = {-1,  -1,  -4};   // cx  dx   -> a   a   dx  cx   //
    }
    else if (no_process_parton[i_a] == 63){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -1,  -5};   // dx  bx   -> a   a   dx  bx   //
      combination_pdf[1] = { 1,  -3,  -5};   // sx  bx   -> a   a   sx  bx   //
      combination_pdf[2] = {-1,  -1,  -5};   // bx  dx   -> a   a   dx  bx   //
      combination_pdf[3] = {-1,  -3,  -5};   // bx  sx   -> a   a   sx  bx   //
    }
    else if (no_process_parton[i_a] == 65){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -2};   // ux  ux   -> a   a   ux  ux   //
      combination_pdf[1] = { 1,  -4,  -4};   // cx  cx   -> a   a   cx  cx   //
    }
    else if (no_process_parton[i_a] == 66){
      combination_pdf.resize(2);
      combination_pdf[0] = { 1,  -2,  -4};   // ux  cx   -> a   a   ux  cx   //
      combination_pdf[1] = { 1,  -4,  -2};   // cx  ux   -> a   a   ux  cx   //
    }
    else if (no_process_parton[i_a] == 67){
      combination_pdf.resize(4);
      combination_pdf[0] = { 1,  -2,  -5};   // ux  bx   -> a   a   ux  bx   //
      combination_pdf[1] = { 1,  -4,  -5};   // cx  bx   -> a   a   cx  bx   //
      combination_pdf[2] = {-1,  -2,  -5};   // bx  ux   -> a   a   ux  bx   //
      combination_pdf[3] = {-1,  -4,  -5};   // bx  cx   -> a   a   cx  bx   //
    }
    else if (no_process_parton[i_a] == 69){
      combination_pdf.resize(1);
      combination_pdf[0] = { 1,  -5,  -5};   // bx  bx   -> a   a   bx  bx   //
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
