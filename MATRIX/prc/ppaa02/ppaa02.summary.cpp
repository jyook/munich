#include "header.hpp"

#include "ppaa02.summary.hpp"

ppaa02_summary_generic::ppaa02_summary_generic(munich * xmunich){
  Logger logger("ppaa02_summary_generic::ppaa02_summary_generic");
  logger << LOG_DEBUG << "called" << endl;

  logger << LOG_DEBUG << "xmunich: order = " << xmunich->order << endl;
  logger << LOG_DEBUG << "xmunich: infilename = " << xmunich->infilename << endl;
  logger << LOG_DEBUG << "xmunich: infilename_scaleband = " << xmunich->infilename_scaleband << endl;

  csi = xmunich->csi;
  isi = xmunich->isi;
  osi = xmunich->osi;

  order = xmunich->order;
  infilename = xmunich->infilename;
  infilename_scaleband = xmunich->infilename_scaleband;

  logger << LOG_DEBUG << "order = " << order << endl;
  logger << LOG_DEBUG << "infilename = " << infilename << endl;
  logger << LOG_DEBUG << "infilename_scaleband = " << infilename_scaleband << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_generic::initialization_summary_order(size_t n_order){
  Logger logger("ppaa02_summary_generic::initialization_summary_order");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  yorder.resize(n_order);
  for (size_t i_o = 0; i_o < n_order; i_o++){yorder[i_o] = new ppaa02_summary_order();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_generic::initialization_summary_list(size_t n_list){
  Logger logger("ppaa02_summary_generic::initialization_summary_list");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  xlist.resize(n_list);
  for (size_t i_l = 0; i_l < n_list; i_l++){xlist[i_l] = new ppaa02_summary_list();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_list::initialization_summary_contribution(size_t n_contribution){
  Logger logger("ppaa02_summary_list::initialization_summary_contribution");
  logger << LOG_DEBUG << "called" << endl;

  xcontribution.resize(n_contribution);
  for (size_t i_l = 0; i_l < n_contribution; i_l++){xcontribution[i_l] = new ppaa02_summary_contribution();}

  logger << LOG_DEBUG << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_born(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_C_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_C_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_C_QEW(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_C_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_V_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_V_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_V_QEW(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_V_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_C2_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_C2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_V2_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_V2_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(4);
    subprocess[1] = "dd~_aa";
    subprocess[2] = "uu~_aa";
    subprocess[3] = "bb~_aa";
    subgroup_no_member[0] = vector<int> {1, 2, 3};
  }
  else if (in_contribution_order_alpha_s == 4 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(2);
    subprocess[1] = "gg_aa";
    subgroup_no_member[0] = vector<int> {1};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_R_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_R_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_aad";
    subprocess[2] = "gu_aau";
    subprocess[3] = "gb_aab";
    subprocess[4] = "gd~_aad~";
    subprocess[5] = "gu~_aau~";
    subprocess[6] = "gb~_aab~";
    subprocess[7] = "dd~_aag";
    subprocess[8] = "uu~_aag";
    subprocess[9] = "bb~_aag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 3 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(11);
    subprocess[1] = "gg_aag";
    subprocess[2] = "gd_aad";
    subprocess[3] = "gu_aau";
    subprocess[4] = "gb_aab";
    subprocess[5] = "gd~_aad~";
    subprocess[6] = "gu~_aau~";
    subprocess[7] = "gb~_aab~";
    subprocess[8] = "dd~_aag";
    subprocess[9] = "uu~_aag";
    subprocess[10] = "bb~_aag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_R_QEW(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_R_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 0 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_aad";
    subprocess[2] = "ua_aau";
    subprocess[3] = "ba_aab";
    subprocess[4] = "d~a_aad~";
    subprocess[5] = "u~a_aau~";
    subprocess[6] = "b~a_aab~";
    subprocess[7] = "dd~_aaa";
    subprocess[8] = "uu~_aaa";
    subprocess[9] = "bb~_aaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_RC_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_RC_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_aad";
    subprocess[2] = "gu_aau";
    subprocess[3] = "gb_aab";
    subprocess[4] = "gd~_aad~";
    subprocess[5] = "gu~_aau~";
    subprocess[6] = "gb~_aab~";
    subprocess[7] = "dd~_aag";
    subprocess[8] = "uu~_aag";
    subprocess[9] = "bb~_aag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_aad";
    subprocess[2] = "ua_aau";
    subprocess[3] = "ba_aab";
    subprocess[4] = "d~a_aad~";
    subprocess[5] = "u~a_aau~";
    subprocess[6] = "b~a_aab~";
    subprocess[7] = "dd~_aaa";
    subprocess[8] = "uu~_aaa";
    subprocess[9] = "bb~_aaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_RV_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_RV_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "gd_aad";
    subprocess[2] = "gu_aau";
    subprocess[3] = "gb_aab";
    subprocess[4] = "gd~_aad~";
    subprocess[5] = "gu~_aau~";
    subprocess[6] = "gb~_aab~";
    subprocess[7] = "dd~_aag";
    subprocess[8] = "uu~_aag";
    subprocess[9] = "bb~_aag";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }
  else if (in_contribution_order_alpha_s == 1 && in_contribution_order_alpha_e == 3 && interference == 0){
    subprocess.resize(10);
    subprocess[1] = "da_aad";
    subprocess[2] = "ua_aau";
    subprocess[3] = "ba_aab";
    subprocess[4] = "d~a_aad~";
    subprocess[5] = "u~a_aau~";
    subprocess[6] = "b~a_aab~";
    subprocess[7] = "dd~_aaa";
    subprocess[8] = "uu~_aaa";
    subprocess[9] = "bb~_aaa";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_summary_contribution::list_subprocess_RR_QCD(){
  Logger logger("ppaa02_summary_contribution::list_subprocess_RR_QCD");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (in_contribution_order_alpha_s == 2 && in_contribution_order_alpha_e == 2 && interference == 0){
    subprocess.resize(54);
    subprocess[1] = "gg_aadd~";
    subprocess[2] = "gg_aauu~";
    subprocess[3] = "gg_aabb~";
    subprocess[4] = "gd_aagd";
    subprocess[5] = "gu_aagu";
    subprocess[6] = "gb_aagb";
    subprocess[7] = "gd~_aagd~";
    subprocess[8] = "gu~_aagu~";
    subprocess[9] = "gb~_aagb~";
    subprocess[10] = "dd_aadd";
    subprocess[11] = "du_aadu";
    subprocess[12] = "ds_aads";
    subprocess[13] = "dc_aadc";
    subprocess[14] = "db_aadb";
    subprocess[15] = "dd~_aagg";
    subprocess[16] = "dd~_aadd~";
    subprocess[17] = "dd~_aauu~";
    subprocess[18] = "dd~_aass~";
    subprocess[19] = "dd~_aacc~";
    subprocess[20] = "dd~_aabb~";
    subprocess[21] = "du~_aadu~";
    subprocess[22] = "ds~_aads~";
    subprocess[23] = "dc~_aadc~";
    subprocess[24] = "db~_aadb~";
    subprocess[25] = "uu_aauu";
    subprocess[26] = "uc_aauc";
    subprocess[27] = "ub_aaub";
    subprocess[28] = "ud~_aaud~";
    subprocess[29] = "uu~_aagg";
    subprocess[30] = "uu~_aadd~";
    subprocess[31] = "uu~_aauu~";
    subprocess[32] = "uu~_aass~";
    subprocess[33] = "uu~_aacc~";
    subprocess[34] = "uu~_aabb~";
    subprocess[35] = "us~_aaus~";
    subprocess[36] = "uc~_aauc~";
    subprocess[37] = "ub~_aaub~";
    subprocess[38] = "bb_aabb";
    subprocess[39] = "bd~_aabd~";
    subprocess[40] = "bu~_aabu~";
    subprocess[41] = "bb~_aagg";
    subprocess[42] = "bb~_aadd~";
    subprocess[43] = "bb~_aauu~";
    subprocess[44] = "bb~_aabb~";
    subprocess[45] = "d~d~_aad~d~";
    subprocess[46] = "d~u~_aad~u~";
    subprocess[47] = "d~s~_aad~s~";
    subprocess[48] = "d~c~_aad~c~";
    subprocess[49] = "d~b~_aad~b~";
    subprocess[50] = "u~u~_aau~u~";
    subprocess[51] = "u~c~_aau~c~";
    subprocess[52] = "u~b~_aau~b~";
    subprocess[53] = "b~b~_aab~b~";
    subgroup_no_member[0] = vector<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
