static int switch_cut_M_gamgam = user->switch_value[user->switch_map["M_gamgam"]];
static double cut_min_M_gamgam = user->cut_value[user->cut_map["min_M_gamgam"]];

//for (int i_a = 0; i_a < 1; i_a++){
for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
  if (switch_cut_M_gamgam){sqrtsmin_opt[i_a][12] = cut_min_M_gamgam;}
 }

for (int i_a = 0; i_a < sqrtsmin_opt.size(); i_a++){
  logger << LOG_INFO << "sqrtsmin_opt after adding invariant-mass cuts:" << endl;
  for (int i = 4; i < sqrtsmin_opt[i_a].size(); i += 4){
    logger << LOG_INFO << "sqrtsmin_opt[" << i_a << "][" << setw(3) << i << "] = " << sqrtsmin_opt[i_a][i] << endl;
  }
 }
