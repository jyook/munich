{
  //  cuts ...
  //  if (cuts ){cut_ps[i_a] = -1; return;}
  
  static int switch_cut_M_gamgam = USERSWITCH("M_gamgam");
  static double cut_min_M_gamgam = USERCUT("min_M_gamgam");
  static double cut_min_M2_gamgam = pow(cut_min_M_gamgam, 2);
  static double cut_max_M_gamgam = USERCUT("max_M_gamgam");
  static double cut_max_M2_gamgam = pow(cut_max_M_gamgam, 2);
  
  static int switch_cut_pT_gam_1st = USERSWITCH("pT_gam_1st");
  static double cut_min_pT_gam_1st = USERCUT("min_pT_gam_1st");
  
  static int switch_cut_gap_eta_gam = USERSWITCH("gap_eta_gam");
  static double cut_gap_min_eta_gam = USERCUT("gap_min_eta_gam");
  static double cut_gap_max_eta_gam = USERCUT("gap_max_eta_gam");
  
  static int switch_cut_R_gamgam = USERSWITCH("R_gamgam");
  static double cut_min_R_gamgam = USERCUT("min_R_gamgam");
  static double cut_min_R2_gamgam = pow(cut_min_R_gamgam, 2);

  if (switch_cut_pT_gam_1st == 1){
    double temp_pT_gam_1st = PARTICLE("photon")[0].pT;
    if (temp_pT_gam_1st < cut_min_pT_gam_1st){
      cut_ps[i_a] = -1; 
      return;
    }
  }

  if (switch_cut_R_gamgam) {
    double R2_eta_gamgam = R2_eta(PARTICLE("photon")[0], PARTICLE("photon")[1]);
    if (R2_eta_gamgam < cut_min_R2_gamgam) {
      cut_ps[i_a] = -1;
      return;
    }
  }

  
  if (switch_cut_M_gamgam == 1){
    double M2_gamgam = (PARTICLE("photon")[0].momentum + PARTICLE("photon")[1].momentum).m2();

    if (M2_gamgam < cut_min_M2_gamgam){
      cut_ps[i_a] = -1; 
      return;
    }

    if (cut_max_M2_gamgam != 0 && M2_gamgam > cut_max_M2_gamgam){
      cut_ps[i_a] = -1; 
      return;
    }
  }
  

  if (switch_cut_gap_eta_gam){
    for (int i_l = 0; i_l < PARTICLE("photon").size(); i_l++){
      double abs_eta_gam = abs(PARTICLE("photon")[i_l].eta);
      if (abs_eta_gam > cut_gap_min_eta_gam && abs_eta_gam < cut_gap_max_eta_gam){
	cut_ps[i_a] = -1; 
	return;
      }
    }
  }
  
}
