#include "header.hpp"

#include "ppaa02.phasespace.set.hpp"

ppaa02_phasespace_set::~ppaa02_phasespace_set(){
  static Logger logger("ppaa02_phasespace_set::~ppaa02_phasespace_set");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::optimize_minv_born(){
  static Logger logger("ppaa02_phasespace_set::optimize_minv_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.born.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppaa02_phasespace_set::determination_MCchannels_born(int x_a){
  static Logger logger("ppaa02_phasespace_set::determination_MCchannels_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 3){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 4){n_channel = 2;}
    else if (csi->no_process_parton[x_a] == 5){n_channel = 2;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] == 1){n_channel = 6;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_tau_psp_born(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppaa02_phasespace_set::ac_tau_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 5){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ax_psp_born(int x_a){
  static Logger logger("ppaa02_phasespace_set::ax_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ax_psp_020_ddx_aa(x_a);}
    else if (csi->no_process_parton[x_a] == 4){ax_psp_020_uux_aa(x_a);}
    else if (csi->no_process_parton[x_a] == 5){ax_psp_020_bbx_aa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ax_psp_220_gg_aa(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_psp_born(int x_a, int channel){
  static Logger logger("ppaa02_phasespace_set::ac_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ac_psp_020_ddx_aa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 4){ac_psp_020_uux_aa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 5){ac_psp_020_bbx_aa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ac_psp_220_gg_aa(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ag_psp_born(int x_a, int zero){
  static Logger logger("ppaa02_phasespace_set::ag_psp_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 3){ag_psp_020_ddx_aa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 4){ag_psp_020_uux_aa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 5){ag_psp_020_bbx_aa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 1){ag_psp_220_gg_aa(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::optimize_minv_real(){
  static Logger logger("ppaa02_phasespace_set::optimize_minv_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.real.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppaa02_phasespace_set::determination_MCchannels_real(int x_a){
  static Logger logger("ppaa02_phasespace_set::determination_MCchannels_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 6;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 14){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 6;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 6;}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 0;}
    else if (csi->no_process_parton[x_a] ==  1){n_channel = 48;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 10;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 11){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 12){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 13){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 10;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 10;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_tau_psp_real(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppaa02_phasespace_set::ac_tau_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 11){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 13){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 12){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 14){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){
      tau_MC_map.push_back(0);
      tau_MC_map.push_back(-36);
    }
    else if (csi->no_process_parton[x_a] ==  7){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  8){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] ==  9){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 10){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 11){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 12){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 13){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 15){
      tau_MC_map.push_back(0);
    }
    else if (csi->no_process_parton[x_a] == 17){
      tau_MC_map.push_back(0);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ax_psp_real(int x_a){
  static Logger logger("ppaa02_phasespace_set::ax_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_120_gd_aad(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_120_gu_aau(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_120_gb_aab(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_120_gdx_aadx(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_120_gux_aaux(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_120_gbx_aabx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_120_ddx_aag(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_120_uux_aag(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_120_bbx_aag(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_030_da_aad(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_030_ua_aau(x_a);}
    else if (csi->no_process_parton[x_a] == 14){ax_psp_030_ba_aab(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_030_dxa_aadx(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_030_uxa_aaux(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_030_bxa_aabx(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_030_ddx_aaa(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_030_uux_aaa(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_030_bbx_aaa(x_a);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ax_psp_320_gg_aag(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_320_gd_aad(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_320_gu_aau(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_320_gb_aab(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_320_gdx_aadx(x_a);}
    else if (csi->no_process_parton[x_a] == 11){ax_psp_320_gux_aaux(x_a);}
    else if (csi->no_process_parton[x_a] == 12){ax_psp_320_gbx_aabx(x_a);}
    else if (csi->no_process_parton[x_a] == 13){ax_psp_320_ddx_aag(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_320_uux_aag(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_320_bbx_aag(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_psp_real(int x_a, int channel){
  static Logger logger("ppaa02_phasespace_set::ac_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_120_gd_aad(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_120_gu_aau(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_120_gb_aab(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_120_gdx_aadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_120_gux_aaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_120_gbx_aabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_120_ddx_aag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_120_uux_aag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_120_bbx_aag(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_030_da_aad(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_030_ua_aau(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 14){ac_psp_030_ba_aab(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_030_dxa_aadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_030_uxa_aaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_030_bxa_aabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_030_ddx_aaa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_030_uux_aaa(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_030_bbx_aaa(x_a, channel);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ac_psp_320_gg_aag(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_320_gd_aad(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_320_gu_aau(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_320_gb_aab(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_320_gdx_aadx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 11){ac_psp_320_gux_aaux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 12){ac_psp_320_gbx_aabx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 13){ac_psp_320_ddx_aag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_320_uux_aag(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_320_bbx_aag(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ag_psp_real(int x_a, int zero){
  static Logger logger("ppaa02_phasespace_set::ag_psp_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 1 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_120_gd_aad(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_120_gu_aau(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_120_gb_aab(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_120_gdx_aadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_120_gux_aaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_120_gbx_aabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_120_ddx_aag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_120_uux_aag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_120_bbx_aag(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 0 && csi->phasespace_order_alpha_e[x_a] == 3 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_030_da_aad(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_030_ua_aau(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 14){ag_psp_030_ba_aab(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_030_dxa_aadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_030_uxa_aaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_030_bxa_aabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_030_ddx_aaa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_030_uux_aaa(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_030_bbx_aaa(x_a, zero);}
  }
  else if (csi->phasespace_order_alpha_s[x_a] == 3 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  1){ag_psp_320_gg_aag(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_320_gd_aad(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_320_gu_aau(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_320_gb_aab(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_320_gdx_aadx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 11){ag_psp_320_gux_aaux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 12){ag_psp_320_gbx_aabx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 13){ag_psp_320_ddx_aag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_320_uux_aag(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_320_bbx_aag(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::optimize_minv_doublereal(){
  static Logger logger("ppaa02_phasespace_set::optimize_minv_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

#include "specify.optimize.phasespace.minv.doublereal.cxx"

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

int ppaa02_phasespace_set::determination_MCchannels_doublereal(int x_a){
  static Logger logger("ppaa02_phasespace_set::determination_MCchannels_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  int n_channel = 0;
  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] ==  0){n_channel = 86;}
    else if (csi->no_process_parton[x_a] ==  2){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  3){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  4){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  5){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  6){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  7){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  8){n_channel = 30;}
    else if (csi->no_process_parton[x_a] ==  9){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 10){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 15){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 16){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 17){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 18){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 20){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 21){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 22){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 23){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 24){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 25){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 26){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 27){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 29){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 31){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 32){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 34){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 35){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 36){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 37){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 39){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 40){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 41){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 42){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 43){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 44){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 45){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 47){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 48){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 50){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 51){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 52){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 53){n_channel = 30;}
    else if (csi->no_process_parton[x_a] == 54){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 55){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 56){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 58){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 59){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 60){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 61){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 63){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 65){n_channel = 40;}
    else if (csi->no_process_parton[x_a] == 66){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 67){n_channel = 20;}
    else if (csi->no_process_parton[x_a] == 69){n_channel = 40;}
  }
  return n_channel;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_tau_psp_doublereal(int x_a, vector<int> & tau_MC_map){
  static Logger logger("ppaa02_phasespace_set::ac_tau_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if      (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  3){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  4){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  5){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  6){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  7){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  8){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] ==  9){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 10){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 15){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 16){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 17){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 18){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 20){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 21){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 22){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 23){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 24){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 25){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 26){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 27){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 29){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 31){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 32){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 34){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 35){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 36){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 37){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 39){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 40){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 41){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 42){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 43){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 44){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 45){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 47){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 48){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 50){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 51){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 52){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 53){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 54){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 55){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 56){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 58){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 59){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 60){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 61){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 63){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 65){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 66){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 67){tau_MC_map = vector<int> {  0};}
    else if (csi->no_process_parton[x_a] == 69){tau_MC_map = vector<int> {  0};}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ax_psp_doublereal(int x_a){
  static Logger logger("ppaa02_phasespace_set::ax_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ax_psp_220_gg_aaddx(x_a);}
    else if (csi->no_process_parton[x_a] ==  3){ax_psp_220_gg_aauux(x_a);}
    else if (csi->no_process_parton[x_a] ==  4){ax_psp_220_gg_aabbx(x_a);}
    else if (csi->no_process_parton[x_a] ==  5){ax_psp_220_gd_aagd(x_a);}
    else if (csi->no_process_parton[x_a] ==  6){ax_psp_220_gu_aagu(x_a);}
    else if (csi->no_process_parton[x_a] ==  7){ax_psp_220_gb_aagb(x_a);}
    else if (csi->no_process_parton[x_a] ==  8){ax_psp_220_gdx_aagdx(x_a);}
    else if (csi->no_process_parton[x_a] ==  9){ax_psp_220_gux_aagux(x_a);}
    else if (csi->no_process_parton[x_a] == 10){ax_psp_220_gbx_aagbx(x_a);}
    else if (csi->no_process_parton[x_a] == 15){ax_psp_220_dd_aadd(x_a);}
    else if (csi->no_process_parton[x_a] == 16){ax_psp_220_du_aadu(x_a);}
    else if (csi->no_process_parton[x_a] == 17){ax_psp_220_ds_aads(x_a);}
    else if (csi->no_process_parton[x_a] == 18){ax_psp_220_dc_aadc(x_a);}
    else if (csi->no_process_parton[x_a] == 20){ax_psp_220_db_aadb(x_a);}
    else if (csi->no_process_parton[x_a] == 21){ax_psp_220_ddx_aagg(x_a);}
    else if (csi->no_process_parton[x_a] == 22){ax_psp_220_ddx_aaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 23){ax_psp_220_ddx_aauux(x_a);}
    else if (csi->no_process_parton[x_a] == 24){ax_psp_220_ddx_aassx(x_a);}
    else if (csi->no_process_parton[x_a] == 25){ax_psp_220_ddx_aaccx(x_a);}
    else if (csi->no_process_parton[x_a] == 26){ax_psp_220_ddx_aabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 27){ax_psp_220_dux_aadux(x_a);}
    else if (csi->no_process_parton[x_a] == 29){ax_psp_220_dsx_aadsx(x_a);}
    else if (csi->no_process_parton[x_a] == 31){ax_psp_220_dcx_aadcx(x_a);}
    else if (csi->no_process_parton[x_a] == 32){ax_psp_220_dbx_aadbx(x_a);}
    else if (csi->no_process_parton[x_a] == 34){ax_psp_220_uu_aauu(x_a);}
    else if (csi->no_process_parton[x_a] == 35){ax_psp_220_uc_aauc(x_a);}
    else if (csi->no_process_parton[x_a] == 36){ax_psp_220_ub_aaub(x_a);}
    else if (csi->no_process_parton[x_a] == 37){ax_psp_220_udx_aaudx(x_a);}
    else if (csi->no_process_parton[x_a] == 39){ax_psp_220_uux_aagg(x_a);}
    else if (csi->no_process_parton[x_a] == 40){ax_psp_220_uux_aaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 41){ax_psp_220_uux_aauux(x_a);}
    else if (csi->no_process_parton[x_a] == 42){ax_psp_220_uux_aassx(x_a);}
    else if (csi->no_process_parton[x_a] == 43){ax_psp_220_uux_aaccx(x_a);}
    else if (csi->no_process_parton[x_a] == 44){ax_psp_220_uux_aabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 45){ax_psp_220_usx_aausx(x_a);}
    else if (csi->no_process_parton[x_a] == 47){ax_psp_220_ucx_aaucx(x_a);}
    else if (csi->no_process_parton[x_a] == 48){ax_psp_220_ubx_aaubx(x_a);}
    else if (csi->no_process_parton[x_a] == 50){ax_psp_220_bb_aabb(x_a);}
    else if (csi->no_process_parton[x_a] == 51){ax_psp_220_bdx_aabdx(x_a);}
    else if (csi->no_process_parton[x_a] == 52){ax_psp_220_bux_aabux(x_a);}
    else if (csi->no_process_parton[x_a] == 53){ax_psp_220_bbx_aagg(x_a);}
    else if (csi->no_process_parton[x_a] == 54){ax_psp_220_bbx_aaddx(x_a);}
    else if (csi->no_process_parton[x_a] == 55){ax_psp_220_bbx_aauux(x_a);}
    else if (csi->no_process_parton[x_a] == 56){ax_psp_220_bbx_aabbx(x_a);}
    else if (csi->no_process_parton[x_a] == 58){ax_psp_220_dxdx_aadxdx(x_a);}
    else if (csi->no_process_parton[x_a] == 59){ax_psp_220_dxux_aadxux(x_a);}
    else if (csi->no_process_parton[x_a] == 60){ax_psp_220_dxsx_aadxsx(x_a);}
    else if (csi->no_process_parton[x_a] == 61){ax_psp_220_dxcx_aadxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 63){ax_psp_220_dxbx_aadxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 65){ax_psp_220_uxux_aauxux(x_a);}
    else if (csi->no_process_parton[x_a] == 66){ax_psp_220_uxcx_aauxcx(x_a);}
    else if (csi->no_process_parton[x_a] == 67){ax_psp_220_uxbx_aauxbx(x_a);}
    else if (csi->no_process_parton[x_a] == 69){ax_psp_220_bxbx_aabxbx(x_a);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ac_psp_doublereal(int x_a, int channel){
  static Logger logger("ppaa02_phasespace_set::ac_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ac_psp_220_gg_aaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  3){ac_psp_220_gg_aauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  4){ac_psp_220_gg_aabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  5){ac_psp_220_gd_aagd(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  6){ac_psp_220_gu_aagu(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  7){ac_psp_220_gb_aagb(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  8){ac_psp_220_gdx_aagdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] ==  9){ac_psp_220_gux_aagux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 10){ac_psp_220_gbx_aagbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 15){ac_psp_220_dd_aadd(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 16){ac_psp_220_du_aadu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 17){ac_psp_220_ds_aads(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 18){ac_psp_220_dc_aadc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 20){ac_psp_220_db_aadb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 21){ac_psp_220_ddx_aagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 22){ac_psp_220_ddx_aaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 23){ac_psp_220_ddx_aauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 24){ac_psp_220_ddx_aassx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 25){ac_psp_220_ddx_aaccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 26){ac_psp_220_ddx_aabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 27){ac_psp_220_dux_aadux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 29){ac_psp_220_dsx_aadsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 31){ac_psp_220_dcx_aadcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 32){ac_psp_220_dbx_aadbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 34){ac_psp_220_uu_aauu(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 35){ac_psp_220_uc_aauc(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 36){ac_psp_220_ub_aaub(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 37){ac_psp_220_udx_aaudx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 39){ac_psp_220_uux_aagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 40){ac_psp_220_uux_aaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 41){ac_psp_220_uux_aauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 42){ac_psp_220_uux_aassx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 43){ac_psp_220_uux_aaccx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 44){ac_psp_220_uux_aabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 45){ac_psp_220_usx_aausx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 47){ac_psp_220_ucx_aaucx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 48){ac_psp_220_ubx_aaubx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 50){ac_psp_220_bb_aabb(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 51){ac_psp_220_bdx_aabdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 52){ac_psp_220_bux_aabux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 53){ac_psp_220_bbx_aagg(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 54){ac_psp_220_bbx_aaddx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 55){ac_psp_220_bbx_aauux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 56){ac_psp_220_bbx_aabbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 58){ac_psp_220_dxdx_aadxdx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 59){ac_psp_220_dxux_aadxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 60){ac_psp_220_dxsx_aadxsx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 61){ac_psp_220_dxcx_aadxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 63){ac_psp_220_dxbx_aadxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 65){ac_psp_220_uxux_aauxux(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 66){ac_psp_220_uxcx_aauxcx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 67){ac_psp_220_uxbx_aauxbx(x_a, channel);}
    else if (csi->no_process_parton[x_a] == 69){ac_psp_220_bxbx_aabxbx(x_a, channel);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void ppaa02_phasespace_set::ag_psp_doublereal(int x_a, int zero){
  static Logger logger("ppaa02_phasespace_set::ag_psp_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (csi->phasespace_order_alpha_s[x_a] == 2 && csi->phasespace_order_alpha_e[x_a] == 2 && csi->phasespace_order_interference[x_a] == 0){
    if (csi->no_process_parton[x_a] == 0){}
    else if (csi->no_process_parton[x_a] ==  2){ag_psp_220_gg_aaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  3){ag_psp_220_gg_aauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  4){ag_psp_220_gg_aabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  5){ag_psp_220_gd_aagd(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  6){ag_psp_220_gu_aagu(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  7){ag_psp_220_gb_aagb(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  8){ag_psp_220_gdx_aagdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] ==  9){ag_psp_220_gux_aagux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 10){ag_psp_220_gbx_aagbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 15){ag_psp_220_dd_aadd(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 16){ag_psp_220_du_aadu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 17){ag_psp_220_ds_aads(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 18){ag_psp_220_dc_aadc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 20){ag_psp_220_db_aadb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 21){ag_psp_220_ddx_aagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 22){ag_psp_220_ddx_aaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 23){ag_psp_220_ddx_aauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 24){ag_psp_220_ddx_aassx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 25){ag_psp_220_ddx_aaccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 26){ag_psp_220_ddx_aabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 27){ag_psp_220_dux_aadux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 29){ag_psp_220_dsx_aadsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 31){ag_psp_220_dcx_aadcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 32){ag_psp_220_dbx_aadbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 34){ag_psp_220_uu_aauu(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 35){ag_psp_220_uc_aauc(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 36){ag_psp_220_ub_aaub(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 37){ag_psp_220_udx_aaudx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 39){ag_psp_220_uux_aagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 40){ag_psp_220_uux_aaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 41){ag_psp_220_uux_aauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 42){ag_psp_220_uux_aassx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 43){ag_psp_220_uux_aaccx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 44){ag_psp_220_uux_aabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 45){ag_psp_220_usx_aausx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 47){ag_psp_220_ucx_aaucx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 48){ag_psp_220_ubx_aaubx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 50){ag_psp_220_bb_aabb(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 51){ag_psp_220_bdx_aabdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 52){ag_psp_220_bux_aabux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 53){ag_psp_220_bbx_aagg(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 54){ag_psp_220_bbx_aaddx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 55){ag_psp_220_bbx_aauux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 56){ag_psp_220_bbx_aabbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 58){ag_psp_220_dxdx_aadxdx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 59){ag_psp_220_dxux_aadxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 60){ag_psp_220_dxsx_aadxsx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 61){ag_psp_220_dxcx_aadxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 63){ag_psp_220_dxbx_aadxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 65){ag_psp_220_uxux_aauxux(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 66){ag_psp_220_uxcx_aauxcx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 67){ag_psp_220_uxbx_aauxbx(x_a, zero);}
    else if (csi->no_process_parton[x_a] == 69){ag_psp_220_bxbx_aabxbx(x_a, zero);}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
