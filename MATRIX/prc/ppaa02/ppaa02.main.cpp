#include "header.hpp"

#include "ppaa02.amplitude.set.hpp"

#include "ppaa02.contribution.set.hpp"
#include "ppaa02.event.set.hpp"
#include "ppaa02.phasespace.set.hpp"
#include "ppaa02.observable.set.hpp"
#include "ppaa02.summary.hpp"

int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich * MUC;
  MUC = new munich(argc, argv, "pp-aa+X");

  MUC->csi = new ppaa02_contribution_set();
  MUC->esi = new ppaa02_event_set();
  MUC->psi = new ppaa02_phasespace_set();
  MUC->osi = new ppaa02_observable_set();

  MUC->initialization();

  if (MUC->subprocess != ""){
    ppaa02_amplitude_initialization(MUC);

    if (MUC->csi->type_contribution == "RT" || MUC->csi->type_contribution == "RRA"){
      MUC->psi->fake_psi = new ppaa02_phasespace_set();
      MUC->psi->fake_psi->csi = new ppaa02_contribution_set();
    }

    MUC->run_initialization();
    MUC->run_integration();

    cout << "END " << MUC->csi->type_contribution << " " << MUC->csi->type_correction << endl;
  }
  else {
    MUC->ysi = new ppaa02_summary_generic(MUC);
    MUC->get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  MUC->walltime_end();
  return 0;
}
