#!/usr/bin/env python

import os
from shutil import copyfile


processes = ["pph21","ppz01","ppw01","ppwx01","ppw01ckm","ppwx01ckm","ppeex02","ppnenex02","ppenex02","ppexne02","ppenex02ckm","ppexne02ckm","pphh22","ppaa02","ppzz02","ppwxw02","ppeexa03","ppnenexa03","ppenexa03","ppexnea03","ppeeexex04","ppemexmx04","ppeexnmnmx04","ppemxnmnex04","ppeexnenex04","ppemexnmx04","ppeeexnex04","ppeexmxnm04","ppeexexne04"]

for process in processes:
    src = os.path.join("/mnt/runs2/wiesemann/runs_for_release/all_total_rates",process+"_MATRIX_test","run_test_"+process,"result/runtime.dat") 
    dst = process+"/default.input.MATRIX/runtime.dat"    
    copyfile(src, dst)
