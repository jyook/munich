##########################
# MATRIX input parameter #
##########################

#----------------------\
# general run settings |
#----------------------/
process_class   =  pp-aa+X     #  process id
E               =  6500.       #  energy per beam
coll_choice     =  1           #  (1) PP collider; (2) PPbar collider


#----------------\
# scale settings |
#----------------/
scale_ren       =  91.1876     #  renormalization (muR) scale
scale_fact      =  91.1876     #  factorization (muF) scale
dynamic_scale   =  1           #  dynamic ren./fac. scale
                               #  0: fixed scale above
                               #  1: invariant mass (Q) of system (of the colourless final states)
                               #  2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)
factor_central_scale = 1       #  relative factor for central scale (important for dynamic scales)
scale_variation  = 1           #  switch for muR/muF uncertainties (0) off; (1) 7-point (default); (2) 9-point variation
variation_factor = 2           #  symmetric variation factor; usually a factor of 2 up and down (default)


#------------------------------\
# order-dependent run settings |
#------------------------------/
# LO
run_LO          =  1           #  switch for LO cross section (1) on; (0) off
LHAPDF_LO       =  NNPDF30_lo_as_0118 #  LO LHAPDF set
PDFsubset_LO    =  0           #  member of LO PDF set
precision_LO    =  1.e-2       #  precision of LO cross section

# NLO
run_NLO         =  0           #  switch for NLO cross section (1) on; (0) off
LHAPDF_NLO      =  NNPDF30_nlo_as_0118 #  NLO LHAPDF set
PDFsubset_NLO   =  0           #  member of NLO PDF set
precision_NLO   =  1.e-2       #  precision of NLO cross section
NLO_subtraction_method = 1     #  switch to use (2) qT subtraction (1) Catani-Seymour at NLO

# NNLO
run_NNLO        =  0           #  switch for NNLO cross section (1) on; (0) off
LHAPDF_NNLO     =  NNPDF30_nnlo_as_0118 #  NNLO LHAPDF set
PDFsubset_NNLO  =  0           #  member of NNLO PDF set
precision_NNLO  =  1.e-2       #  precision of NNLO cross section
loop_induced    =  1           #  switch to turn on (1) and off (0) loop-induced gg channel

switch_qT_accuracy = 0         #  switch to improve qT-subtraction accuracy (slower numerical convergence) 


#----------------------------\
# settings for fiducial cuts |
#----------------------------/
# Jet algorithm
jet_algorithm = 3              #  (1) Cambridge-Aachen (2) kT (3) anti-kT
jet_R_definition = 0           #  (0) pseudo-rapidity (1) rapidity
jet_R = 0.4                    #  DeltaR

# Frixione isolation
frixione_isolation = 1         #  switch for Frixione isolation (0) off;
                               #  (1) with frixione_epsilon, used by ATLAS;
                               #  (2) with frixione_fixed_ET_max, used by CMS
frixione_n = 1                 #  exponent of delta-term
frixione_delta_0 = 0.4         #  maximal cone size
frixione_epsilon = 0.5         #  photon momentum fraction
#frixione_fixed_ET_max = 5      #  fixed maximal pT inside cone

# Jet cuts
define_pT jet = 25.            #  requirement on jet transverse momentum (lower cut)
define_eta jet = 4.5           #  requirement on jet pseudo-rapidity (upper cut)
define_y jet = 1.e99           #  requirement on jet rapidity (upper cut)
n_observed_min jet = 0         #  minimal number of observed jets (with cuts above)
n_observed_max jet = 99        #  maximal number of observed jets (with cuts above)

# Photon cuts
define_pT photon = 25.         #  requirement on photon transverse momentum (lower cut)
define_eta photon = 2.5        #  requirement on photon pseudo-rapidity (upper cut)
define_y photon = 1.e99        #  requirement on photon rapidity (upper cut)
n_observed_min photon = 2      #  minimal number of observed photons (with cuts above)
n_observed_max photon = 99     #  maximal number of observed photons (with cuts above)

# Blocks with user-defined cuts (only used if defined in 'MATRIX/prc/$process/user/specify.cuts.cxx')
# Mandatory cuts for this process:
# -- min_M_gamgam OR min_R_gamgam
#
user_switch M_gamgam = 1                #  switch to turn on (1) and off (0) cuts on photon-photon invariant mass
user_cut min_M_gamgam = 20.             #  requirement on photon-photon invariant mass (lower cut)
user_cut max_M_gamgam = 250.            #  requirement on photon-photon invariant mass (upper cut)

user_switch pT_gam_1st = 1              #  switch to turn on (1) and off (0) cuts on pT of hardest photon
user_cut min_pT_gam_1st = 40.           #  requirement on pT of hardest photon (lower cut)

user_switch gap_eta_gam = 0             #  switch to turn on (1) and off (0) detector gap for eta of photons
user_cut gap_min_eta_gam = 0.           #  lower bound of detector gap in eta
user_cut gap_max_eta_gam = 0.           #  upper bound of detector gap in eta

user_switch R_gamgam = 0                #  switch to turn on (1) and off (0) cuts on photon-photon separation
user_cut min_R_gamgam = 0.3             #  requirement on photon-photon separation in y-phi-plane (lower cut)


#-----------------\
# MATRIX behavior |
#-----------------/
max_time_per_job = 12          #  very rough time(hours) one main run job shall take (default: 24h)
                               #  unreliable when < 1h, use as tuning parameter for degree of parallelization
                               #  note: becomes ineffective when job number > max_nr_parallel_jobs
                               #        which is set in MATRIX_configuration file
switch_distribution = 1        #  switch to turn on (1) and off (0) distributions
save_previous_result = 1       #  switch to save previous result of this run (in result/"run"/saved_result_$i)
save_previous_log = 0          #  switch to save previous log of this run (in log/"run"/saved_result_$i)
#include_pre_in_results = 0     #  switch to (0) only include main run in results; (1) also all extrapolation (pre) runs;
                               #  crucial to set to 0 if re-running main with different inputs (apart from precision)
                               #  note: if missing (default) pre runs used if important for precision
                               #        (separately for each contribution);
reduce_workload = 0            #  switch to keep full job output (0), reduce (1) or minimize (2) workload on slow clusters
random_seed = 0                #  specify integer value (grid-/pre-run reproducible)
