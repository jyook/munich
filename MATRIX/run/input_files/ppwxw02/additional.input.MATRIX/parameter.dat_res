##########################
# MATRIX input parameter #
##########################

#----------------------\
# general run settings |
#----------------------/
process_class   =  pp-wpwm+X        #  process id
flavor_scheme   =  1                #  (1) four-flavor scheme; (2) five-flavor scheme; note: use consistent PDFs
E               =  4000.            #  energy per beam
coll_choice     =  1                #  (1) PP collider; (2) PPbar collider
loop_induced    =  1                #  switch to turn on (1) and off (0) loop-induced gg channel
switch_distribution = 1             #  switch to turn on (1) and off (0) distributions
max_time_per_job = 12               #  very rough time(hours) one main run job shall take (default: 24h)
                                    #  unreliable when < 1h, use as tuning parameter for degree of parallelization
                                    #  note: becomes ineffective when job number > max_nr_parallel_jobs
                                    #        which is set in MATRIX_configuration file


#----------------\
# scale settings |
#----------------/
scale_fact      =  160.798     #  factorization scale
scale_ren       =  160.798     #  renormalization scale
dynamic_scale   =  1           #  dynamic ren./fac. scale
                               #  0: fixed scale above
                               #  1: invariant mass (Q) of system (of the colourless final states)
                               #  2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)
factor_central_scale = 1       #  relative factor for central scale (important for dynamic scales)
scale_variation  = 1           #  switch for muR/muF uncertainties (0) off; (1) 7-point (default); (2) 9-point variation
variation_factor = 2           #  symmetric variation factor; usually a factor of 2 up and down (default)
                               #  note: automatic scale variation not working in resummation runs

# pT resummation
scale_res         =  91.1876   #  resummation scale
dynamic_scale_res =  0         #  dynamic resummation scale
                               #  0: fixed scale above
                               #  1: invariant mass of colorless Born system
factor_scale_res = 1           #  relative factor for central resummation scale (important for dynamic scale)


#------------------------------\
# order-dependent run settings |
#------------------------------/
# LO
run_LO          =  1           #  switch for LO cross section (1) on; (0) off 
LHAPDF_LO       =  NNPDF30_lo_as_0118_nf_4 #  LO LHAPDF set
PDFsubset_LO    =  0           #  member of LO PDF set
accuracy_LO     =  1.e-2       #  accuracy of LO cross section

# NLO(+NLL)
run_NLO         =  0           #  switch for NLO cross section (1) on; (0) off 
add_NLL         =  0           #  switch to add NLL pT resummation to NLO (1) on; (0) off 
LHAPDF_NLO      =  NNPDF30_nlo_as_0118_nf_4 #  NLO LHAPDF set
PDFsubset_NLO   =  0           #  member of NLO PDF set
accuracy_NLO    =  1.e-2       #  accuracy of NLO cross section

# NNLO(+NNLL)
run_NNLO        =  0           #  switch for NNLO cross section (1) on; (0) off
add_NNLL        =  0           #  switch to add NNLL pT resummation to NNLO (1) on; (0) off 
LHAPDF_NNLO     =  NNPDF30_nnlo_as_0118_nf_4 #  NNLO LHAPDF set
PDFsubset_NNLO  =  0           #  member of NNLO PDF set
accuracy_NNLO   =  1.e-2       #  accuracy of NNLO cross section


#----------------------------\
# settings for fiducial cuts |
#----------------------------/
# Jet algorithm
jet_algorithm = 3              #  (1) Cambridge-Aachen (2) kT (3) anti-kT
jet_R_definition = 0           #  (0) pseudo-rapidity (1) rapidity
jet_R = 0.4                    #  DeltaR

# Frixione isolation
frixione_isolation = 1         #  switch for Frixione isolation (0) off;
                               #  (1) with frixione_epsilon, used by ATLAS;
                               #  (2) with frixione_fixed_ET_max, used by CMS
frixione_n = 1                 #  exponent of delta-term
frixione_epsilon = 0.5         #  photon momentum fraction
frixione_delta_0 = 0.4         #  maximal cone size
#frixione_fixed_ET_max = 5      #  fixed maximal pT inside cone

# Jet cuts
define_pT jet = 25.            #  requirement on jet transverse momentum (lower cut)
define_eta jet = 4.5           #  requirement on jet pseudo-rapidity (upper cut)
define_y jet = 1.e99           #  requirement on jet rapidity (upper cut)
n_observed_min jet = 0         #  minimal number of observed jets (with cuts above)
n_observed_max jet = 99        #  maximal number of observed jets (with cuts above)

# Light-jet cuts
define_pT ljet = 30.           #  requirement on light-jet transverse momentum (lower cut)
define_eta ljet = 4.4          #  requirement on light-jet pseudo-rapidity (upper cut)
define_y ljet = 1.e99          #  requirement on light-jet rapidity (upper cut)
n_observed_min ljet = 0        #  minimal number of observed light jets (with cuts above)
n_observed_max ljet = 99       #  maximal number of observed light jets (with cuts above)

# Lepton cuts
define_pT lep = 0.             #  requirement on lepton transverse momentum (lower cut)
define_eta lep = 1.e99         #  requirement on lepton pseudo-rapidity (upper cut)
define_y lep = 1.e99           #  requirement on lepton rapidity (upper cut)
n_observed_min lep = 0         #  minimal number of observed leptons (with cuts above)
n_observed_max lep = 99        #  maximal number of observed leptons (with cuts above)

# Muon cuts
define_pT mu = 0.              #  requirement on muon transverse momentum (lower cut)
define_eta mu = 2.4            #  requirement on muon pseudo-rapidity (upper cut)
define_y mu = 1.e99            #  requirement on muon rapidity (upper cut)
n_observed_min mu = 0          #  minimal number of observed muons (with cuts above)
n_observed_max mu = 99         #  maximal number of observed muons (with cuts above)

# Neutrino cuts
define_pT missing = 20.        #  requirement on transverse momentum of sum of all neutrinos (lower cut)


# Blocks with user-defined cuts (only used if defined in 'MATRIX/prc/$process/user/specify.cuts.cxx')
# Mandatory cuts for this process:
# -- none

#-----------------\
# MATRIX behavior |
#-----------------/
save_previous_result  =  1     #  switch to save previous result of this run (in result/"run"/saved_result_$i)
save_previous_log  =  0        #  switch to save previous log of this run (in log/"run"/saved_result_$i)
#include_pre_in_results = 0    #  switch to (0) only include main run in results; (1) also all extrapolation (pre) runs;
                               # crucial to set to 0 if re-running main with different inputs (apart from accuracy)
                               # note: if missing (default) pre runs used if important for accuracy
                               #       (separately for each contribution);
NLO_subtraction_method = 1     # switch to use (2) qT subtraction (1) Catani-Seymour at NLO
