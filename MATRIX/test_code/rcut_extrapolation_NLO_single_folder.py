#!/usr/bin/env python

take_normalization_from_run = True
use_new_extrapolation = True
ext_015 = False
ext_005 = True

processes = ["ppaaa03"]
process_output_from_id = {}
process_output_from_id["pph21"] = "$pp \\to H$"
process_output_from_id["ppz01"] = "$pp \\to Z$"
process_output_from_id["ppw01"] = "$pp \\to W^-$"
process_output_from_id["ppwx01"] = "$pp \\to W^+$"
process_output_from_id["ppw01nockm"] = "$pp \\to W^-$ no CKM"
process_output_from_id["ppwx01nockm"] = "$pp \\to W^+$ no CKM"
process_output_from_id["ppeex02"] = "$pp \\to e^- e^+$"
process_output_from_id["ppnenex02"] = "$pp \\to \\nu_e \\bar\\nu_e$"
process_output_from_id["ppenex02"] = "$pp \\to e^- \\bar\\nu_e$"
process_output_from_id["ppexne02"] = "$pp \\to e^+ \\nu_e$"
process_output_from_id["ppenex02nockm"] = "$pp \\to e^- \\bar\\nu_e$"
process_output_from_id["ppexne02nockm"] = "$pp \\to e^+ \\nu_e$"
process_output_from_id["pphh22"] = "$pp \\to H H$"
process_output_from_id["ppaa02"] = "$pp \\to \\gamma\\gamma$"
process_output_from_id["ppaaa03"] = "$pp \\to \\gamma\\gamma\\gamma$"
process_output_from_id["ppzz02"] = "$pp \\to Z Z$"
process_output_from_id["ppwxw02"] = "$pp \\to W^+ W^-$"
process_output_from_id["ppeexa03"] = "$pp \\to e^- e^+ \\gamma$"
process_output_from_id["ppnenexa03"] = "$pp \\to \\nu_e \\bar\\nu_e \\gamma$"
process_output_from_id["ppenexa03"] = "$pp \\to e^- \\bar\\nu_e \\gamma$"
process_output_from_id["ppexnea03"] = "$pp \\to e^+ \\nu_e \\gamma$"
process_output_from_id["ppeeexex04"] = "$pp \\to e^- e^- e^+ e^+$"
process_output_from_id["ppemexmx04"] = "$pp \\to e^- \\mu^- e^+ \\mu^+$"
process_output_from_id["ppeexnmnmx04"] = "$pp \\to e^- e^+ \\nu_\\mu \\bar\\nu_\\mu$"
process_output_from_id["ppemxnmnex04"] = "$pp \\to e^- \\mu^+ \\nu_\\mu \\bar\\nu_e $"
process_output_from_id["ppeexnenex04"] = "$pp \\to e^- e^+ \\nu_e \\bar\\nu_e$"
process_output_from_id["ppemexnmx04"] = "$pp \\to e^- \\mu^- e^+ \\bar\\nu_\\mu $"
process_output_from_id["ppeeexnex04"] = "$pp \\to e^- e^- e^+ \\bar\\nu_e $"
process_output_from_id["ppeexmxnm04"] = "$pp \\to e^- e^+ \\mu^+ \\nu_\\mu$"
process_output_from_id["ppeexexne04"] = "$pp \\to e^- e^+ e^+ \\nu_e$"

from math import log10, floor
import sys
import stat
import copy
import glob
import math
import os
import shutil
import re
import subprocess
from os.path import join as pjoin 

#{{{ def: readin_file(self,file_path)
def readin_file(file_path):
    # this reads a file and writes it into a table
    with open(file_path,'r') as f:
        table = [row.strip().split() for row in f if not row.split()[0].startswith("#") and not row.split()[0].startswith("%")]
    return table
#}}}    
#{{{ def: create_totalXS_table(totalXS_dict)
def create_totalXS_table(totalXS_dict,out_file):
    with open(out_file,'w') as f:
        header = """
\\begin{table}[t]
\\begin{center}
\\resizebox{\\columnwidth}{!}{%
\\begin{tabular}{c c c c c c}
\\toprule

{\\bf process (\\matrixparam{\\$\\{process\\_id\\}})}
& $\\sigma_{\\textrm{LO}}$
& $\\sigma_{\\textrm{NLO}}$
& $\\sigma_{\\textrm{loop-induced}}$
& $\\sigma_{\\textrm{NLO}}$\\\\

\\bottomrule
"""
        f.write(header)
        for process in process_order:
            if not process in totalXS_dict:
                continue
            LO_path = totalXS_dict[process]["LO"]
            NLO_path = totalXS_dict[process]["NLO"]
            NLO_path = totalXS_dict[process]["NLO"]
            loop_induced = totalXS_dict[process].get("loop_induced","---")
            line = process_output_from_id[process]+" (\\ttt{"+process+"}) & "+get_table_entry_from_XS_file(LO_path)+" & "+get_table_entry_from_XS_file(NLO_path)+" & "+get_table_entry_from_XS_file(loop_induced)+" & "+get_table_entry_from_XS_file(NLO_path)+" \\Bstrut\\\\\n"
            print line
            f.write(line)
        body = """\\bottomrule

\\end{tabular}}
\\end{center}
\\renewcommand{\\baselinestretch}{1.0}
\\caption{\\label{tab:} Fiducial cross sections for ...}
\\end{table}
"""
        f.write(body)
#}}}
#{{{ def: get_table_entry_from_XS_file(in_file)
def get_table_entry_from_XS_file(in_file):
    # this routine returns a cross section with error and uncertainties from a file path
    if in_file == "---": return "---"
    new_data = readin_file(in_file) # reads space-separated matrix from file
    
    unit = "fb"
    central        = float(new_data[3][2])
    central_err    = float(new_data[3][3])
    min_value      = min([float(row[2]) for row in new_data])
    max_value      = max([float(row[2]) for row in new_data])

    if central > 10000000:
        unit = "nb"
        central     /= 1000000
        central_err /= 1000000
        min_value   /= 1000000
        max_value   /= 1000000
    elif central > 10000:
        unit = "pb"
        central     /= 1000
        central_err /= 1000
        min_value   /= 1000
        max_value   /= 1000

    central_string = ("%#.4g" % central).rstrip('.') 
    central_base   = 10**(4-math.ceil(math.log10(central)))
    err_string     = "("+str(int(round(central_err * central_base)))+")"
    min_ratio      = ("%#.2g" % ((min_value/central-1) * 100)).rstrip('.') 
    max_ratio      = ("%#.2g" % ((max_value/central-1) * 100)).rstrip('.') 
    min_string     = "_{%s\\%%}" % min_ratio
    max_string     = "^{+%s\\%%}" % max_ratio

    string = "$"+central_string+err_string+min_string+max_string+"$\,%s" % unit
    return string 
#}}}
#{{{ def: write_rcut_variation_file(input_list,out_file_name)
def write_rcut_variation_NLO_file(input_list,out_file_name):
    bash_file_content = """#!/bin/bash
gnuplot << _plotend
set term pslatex color
set output "v$$.plot"
set title ""
set lmargin 21
set format x "{&small \\$%%g\\$}"
set format y "{&small \\$%%g&qquad\\$}"

set style line 2 lt 1 lw 4 lc rgb "blue" 
set style line 12 lt 2 lw 4 lc rgb "blue" 
set style line 1 lt 1 lw 4 lc rgb "red"
set style line 11 lt 2 lw 4 lc rgb "red"
set style line 3 lt 1 lw 4 lc rgb "dark-green" 
set style line 13 lt 2 lw 4 lc rgb "dark-green" 
set style line 4 lt 1 lw 4 lc rgb "dark-cyan" 
set style line 14 lt 2 lw 4 lc rgb "dark-cyan" 
set style line 5 lt 1 lw 4 lc rgb "black" 
set style line 15 lt 2 lw 4 lc rgb "black" 

fitnnlomin = 0.01
fitnnlomax = %(fitnnlomax)s

set multiplot

set origin 0.,0.
set size 1.4,1.0
set lmargin 21
set xlabel "&Large\\$r=&mathrm\\{cut\\}_{q_&mathrm{T}/q\\}[&%%]$"
set xlabel "&Large\\$r_{&mathrm\\{cut\\}\\}[&%%]$"
set xlabel offset 0,-0.7
unset label
set label "&Large\\$\\{%(process_string)s\\}\\$ \\@ 13 TeV" at graph 1.0, graph 1.04 right
set label "&Large\\$&sigma/{&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{&scriptstyle &phantom{CS}}}-1}\\{[&%%]\\}\\$" at graph 0., graph 1.06 left

set xrange [0.:1.];
set yrange [%(y_min)s:%(y_max)s];

set xtics offset 0, graph -0.02
set xtics mirror ("&Large 0" 0, "" 0.01 1, "" 0.02 1, "" 0.03 1, "" 0.04 1, "" 0.05, "" 0.06 1, "" 0.07 1, "" 0.08 1, "" 0.09 1, "&Large 0.1" 0.1, "" 0.11 1, "" 0.12 1, "" 0.13 1, "" 0.14 1, "" 0.15, "" 0.16 1, "" 0.17 1, "" 0.18 1, "" 0.19 1, "&Large 0.2" 0.2, "" 0.21 1, "" 0.22 1, "" 0.23 1, "" 0.24 1, "" 0.25, "" 0.26 1, "" 0.27 1, "" 0.28 1, "" 0.29 1, "&Large 0.3" 0.3, "" 0.31 1, "" 0.32 1, "" 0.33 1, "" 0.34 1, "" 0.35, "" 0.36 1, "" 0.37 1, "" 0.38 1, "" 0.39 1, "&Large 0.4" 0.4, "" 0.41 1, "" 0.42 1, "" 0.43 1, "" 0.44 1, "" 0.45, "" 0.46 1, "" 0.47 1, "" 0.48 1, "" 0.49 1, "&Large 0.5" 0.5, "" 0.51 1, "" 0.52 1, "" 0.53 1, "" 0.54 1, "" 0.55, "" 0.56 1, "" 0.57 1, "" 0.58 1, "" 0.59 1, "&Large 0.6" 0.6, "" 0.61 1, "" 0.62 1, "" 0.63 1, "" 0.64 1, "" 0.65, "" 0.66 1, "" 0.67 1, "" 0.68 1, "" 0.69 1, "&Large 0.7" 0.7, "" 0.71 1, "" 0.72 1, "" 0.73 1, "" 0.74 1, "" 0.75, "" 0.76 1, "" 0.77 1, "" 0.78 1, "" 0.79 1, "&Large 0.8" 0.8, "" 0.81 1, "" 0.82 1, "" 0.83 1, "" 0.84 1, "" 0.85, "" 0.86 1, "" 0.87 1, "" 0.88 1, "" 0.89 1, "&Large 0.9" 0.9, "" 0.91 1, "" 0.92 1, "" 0.93 1, "" 0.94 1, "" 0.95, "" 0.96 1, "" 0.97 1, "" 0.98 1, "" 0.99 1, "&Large 1.0" 1.0, "" 1.1 1, "&Large 1.2" 1.2, "" 1.3 1, "&Large 1.4" 1.4, "" 1.5 1, "&Large 1.6" 1.6, "" 1.7 1, "&Large 1.8" 1.8, "" 1.9 1, "&Large 2.0" 2.0) 

set ytics %(ytics)s

set style fill solid .2
""" % input_list
    if use_new_extrapolation:
        bash_file_content = bash_file_content + """
set key at graph 1.71,0.23 Left reverse spacing 2
"""
        if True: #"test_runs" in input_list["rcut_NLO_file"]:
            bash_file_content = bash_file_content + """
nnloresult = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$2}'") 
error = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$3}'")
nnloamin    = nnloresult - error
nnloamax    = nnloresult + error
""" % input_list
            if "result.NLO.QT-CS.NLO.CS.LO_0.15" in input_list["rcut_NLO_file"]:
                bash_file_content = bash_file_content + """
nnloresult005 = system("head -4 " . "%(NLO_ext_005_path)s" . " | tail -1 | awk '{print \\$2}'") 
error005 = system("head -4 " . "%(NLO_ext_005_path)s" . " | tail -1 | awk '{print \\$3}'")
nnloamin005 = nnloresult005 - error005
nnloamax005 = nnloresult005 + error005
""" % input_lists
            if ext_015:
                bash_file_content = bash_file_content + """
set arrow from 0.15,graph(0,0) to 0.15,graph(1,1) lw 2.5 lc rgb "blue" lt 3 nohead""" % input_list
                input_list["ls"] = 2
                input_list["rcut_value"] = 0.15
            elif ext_005:
                bash_file_content = bash_file_content + """
set arrow from 0.05,graph(0,0) to 0.05,graph(1,1) lw 2.5 lc rgb "red" lt 3 nohead""" % input_list
                input_list["ls"] = 1
                input_list["rcut_value"] = 0.05
            else:
                out.print_error("not correct lowest extrapolation number selected")
                
#             if "result.NLO.QT-CS.NLO.CS.LO_0.15" in input_list["rcut_NLO_file"]:
#                 bash_file_content = bash_file_content + """
# set arrow from 0.05,graph(0,0) to 0.05,graph(1,1) lw 2.5 lc rgb "red" lt 3 nohead
# """ % input_list
            bash_file_content = bash_file_content + """

plot "%(rcut_NLO_file)s" us (\\$1):((nnloamin/nnloresult-1)):((nnloamax/nnloresult-1)) t '' w filledcurves ls %(ls)s,\\""" % input_list
     #        if "result.NLO.QT-CS.NLO.CS.LO_0.15" in input_list["rcut_NLO_file"]:
     #            bash_file_content = bash_file_content + """
     # "%(rcut_NLO_file)s" us (\\$1):((nnloamin005/nnloresult-1)):((nnloamax005/nnloresult-1)) t '' w filledcurves ls 11,\\""" % input_list
            bash_file_content = bash_file_content + """
     "%(rcut_NLO_file)s" us (\\$1):((nnloamin/nnloresult-1)) t '' smooth unique ls 1%(ls)s,\\
     "%(rcut_NLO_file)s" us (\\$1):(nnloresult/nnloresult-1) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}(r_{\\rm cut}\ge %(rcut_value)s)\\$}' smooth unique ls %(ls)s,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloamax/nnloresult-1)) t '' smooth unique ls 1%(ls)s,\\""" % input_list
     #        if "result.NLO.QT-CS.NLO.CS.LO_0.15" in input_list["rcut_NLO_file"]:
     #            bash_file_content = bash_file_content + """
     # "%(rcut_NLO_file)s" us (\\$1):((nnloamin005/nnloresult-1)) t '' smooth unique ls 11,\\
     # "%(rcut_NLO_file)s" us (\\$1):(nnloresult005/nnloresult-1) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}(r_{\\rm cut}\ge 0.05)\\$}' smooth unique ls 1,\\
     # "%(rcut_NLO_file)s" us (\\$1):((nnloamax005/nnloresult-1)) t '' smooth unique ls 11,\\""" % input_list
            bash_file_content = bash_file_content + """
     "%(rcut_NLO_file)s" us (\\$1):(\\$8/nnloresult-1):(\\$9/nnloresult) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green"
     
set origin 0.,0.
plot "%(rcut_NLO_file)s" us (\\$1):(-8) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}(r_{\\rm cut}\ge %(rcut_value)s)\\$}' smooth unique ls %(ls)s,\\""" % input_list
     #        if "result.NLO.QT-CS.NLO.CS.LO_0.15" in input_list["rcut_NLO_file"]:
     #            bash_file_content = bash_file_content + """
     # "%(rcut_NLO_file)s" us (\\$1):(-8) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}(r_{\\rm cut}\ge 0.05)\\$}' smooth unique ls 1,\\""" % input_list
            bash_file_content = bash_file_content + """
     "%(rcut_NLO_file)s" us (\\$1):(-8):(0) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green"

_plotend
""" % input_list
    elif "NLO_normalization" in input_list:
        if "test_runs" in input_list["rcut_NLO_file"]:
            bash_file_content = bash_file_content + """
nnloresult = system("head -15 " . "%(rcut_NLO_file)s" . " | tail -1 | awk '{print \\$8}'")
error      = system("head -15 " . "%(rcut_NLO_file)s" . " | tail -1 | awk '{print \\$9}'")
nnloamin    = nnloresult - (error+%(rel_qTerror)s/100*nnloresult)
nnloamax    = nnloresult + (error+%(rel_qTerror)s/100*nnloresult)

""" % input_list
        else:
            bash_file_content = bash_file_content + """
nnloresult = system("head -2 " . "%(rcut_NLO_file)s" . " | tail -1 | awk '{print \\$8}'")
error      = system("head -2 " . "%(rcut_NLO_file)s" . " | tail -1 | awk '{print \\$9}'")
nnloamin    = nnloresult - (error+%(rel_qTerror)s/100*nnloresult)
nnloamax    = nnloresult + (error+%(rel_qTerror)s/100*nnloresult)

""" % input_list            
        if "all_total_rates" in input_list["NLO_orig_path"]:
            bash_file_content = bash_file_content + """
nnlo015orig = system("head -4 " . "%(NLO_orig_path)s" . " | tail -1 | awk '{print \\$2}'")
nnloextorig = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$2}'") 
nnloextorigerr = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$3}'")
nnloext     = nnloresult * nnloextorig/nnlo015orig
nnloextamin = nnloresult * (nnloextorig-nnloextorigerr)/nnlo015orig
nnloextamax = nnloresult * (nnloextorig+nnloextorigerr)/nnlo015orig
""" % input_list
        else:
            bash_file_content = bash_file_content + """
nnlo015orig = system("head -15 " . "%(rcut_NLO_file)s" . " | tail -1 | awk '{print \\$8}'")
nnloextorig = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$2}'") 
nnloextorigerr = system("head -4 " . "%(NLO_ext_orig_path)s" . " | tail -1 | awk '{print \\$3}'")
nnloext     = nnloresult * nnloextorig/nnlo015orig
nnloextamin = nnloresult * (nnloextorig-nnloextorigerr)/nnlo015orig
nnloextamax = nnloresult * (nnloextorig+nnloextorigerr)/nnlo015orig
""" % input_list        
        bash_file_content = bash_file_content + """
plot "%(rcut_NLO_file)s" us (\\$1):((nnloextamin/nnloresult-1)):((nnloextamax/nnloresult-1)) t '' w filledcurves ls 15,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloamin/nnloresult-1)):((nnloamax/nnloresult-1)) t '' w filledcurves ls %(ls)s,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloamin/nnloresult-1)) t '' smooth unique ls 12,\\
     "%(rcut_NLO_file)s" us (\\$1):(nnloresult/nnloresult-1) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{&phantom{CS}}}(%(rcut_value)s)\\$}' smooth unique ls 2,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloamax/nnloresult-1)) t '' smooth unique ls 12,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloextamin/nnloresult-1)) t '' smooth unique ls 15,\\
     "%(rcut_NLO_file)s" us (\\$1):(nnloext/nnloresult-1) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}\\$}' smooth unique ls 5,\\
     "%(rcut_NLO_file)s" us (\\$1):((nnloextamax/nnloresult-1)) t '' smooth unique ls 15,\\
     "%(rcut_NLO_file)s" us (\\$1):(\\$8/nnloresult-1):(\\$9/nnloresult) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green"
     
set origin 0.,0.
plot "%(rcut_NLO_file)s" us (\\$1):(-8) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{&phantom{CS}}}(%(rcut_value)s)\\$}' smooth unique ls 2,\\
     "%(rcut_NLO_file)s" us (\\$1):(-8) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{extrapolated}}\\$}' smooth unique ls 5,\\
     "%(rcut_NLO_file)s" us (\\$1):(-8):(0) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green"

_plotend
""" % input_list
    else:
        bash_file_content = bash_file_content + """
nnlofit(t) = nnloa + nnlob * t + t*t * nnlol0
fit [fitnnlomin:fitnnlomax] nnlofit(x) "%(rcut_NLO_file)s" using (\\$1):(\\$8):(\\$9) via nnloa, nnlob, nnlol0

nnlofitmax(t) = nnloamax + nnlobmax * t + t*t * nnlol0max
fit [fitnnlomin:fitnnlomax] nnlofitmax(x) "%(rcut_NLO_file)s" using (\\$1):(\\$8+\\$9):(\\$9) via nnloamax, nnlobmax, nnlol0max

nnlofitmin(t) = nnloamin + nnlobmin * t + t*t * nnlol0min
fit [fitnnlomin:fitnnlomax] nnlofitmin(x) "%(rcut_NLO_file)s" using (\\$1):(\\$8-\\$9):(\\$9) via nnloamin, nnlobmin, nnlol0min

nnloresult = nnloa

plot "%(rcut_NLO_file)s" us (\\$1):(2*(nnloamin/nnloresult-1)):(2*(nnloamax/nnloresult-1)) t '' w filledcurves ls %(ls)s,\\
     "%(rcut_NLO_file)s" us (\\$1):(2*(nnloamin/nnloresult-1)) t '' smooth unique ls 12,\\
     "%(rcut_NLO_file)s" us (\\$1):(nnloa/nnloresult-1) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{&phantom{CS}}}\\$}' smooth unique ls 2,\\
     "%(rcut_NLO_file)s" us (\\$1):(2*(nnloamax/nnloresult-1)) t '' smooth unique ls 12,\\
     "%(rcut_NLO_file)s" us (\\$1):(\\$8/nnloresult-1):(\\$9/nnloresult) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green",\\
     nnlofit(x)/nnloresult-1 t '' ls 4,\\
     nnlofitmax(x)/nnloresult-1 t '' ls 14,\\
     nnlofitmin(x)/nnloresult-1 t '' ls 14
     
set origin 0.,0.
plot "%(rcut_NLO_file)s" us (\\$1):(-8) t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{&mathrm{&phantom{CS}}}\\$}' smooth unique ls 2,\\
     "%(rcut_NLO_file)s" us (\\$1):(-8):(0) w errorbars t '&Large{\\$&sigma_{&mathrm{&scriptstyle NLO}}^{q_{&mathrm{T}}}(r)\\$}' pt 1 ps 1 lt 1 lw 4 lc rgb "dark-green"

_plotend
""" % input_list
    bash_file_content = bash_file_content + """
cat << _texstuff > v$$.tex
\\\\documentclass[11pt]{article}
\\\\usepackage{color}
\\\\newcommand{\\\\Black}{\\\\textcolor{black}}
\\\\newcommand{\\\\Red}{\\\\textcolor{red}}
\\\\newcommand{\\\\Green}{\\\\textcolor{green}}
\\\\newcommand{\\\\Blue}{\\\\textcolor{blue}}
\\\\newcommand{\\\\Cyan}{\\\\textcolor{cyan}}
\\\\newcommand{\\\\Magenta}{\\\\textcolor{magenta}}
\\\\newcommand{\\\\Yellow}{\\\\textcolor{yellow}}
\\\\newcommand{\\\\Pe}{\\\\mathrm{e}}
\\\\newcommand{\\\\GeV}{\\\\unskip\\,\\\\mathrm{GeV}}
\\\\oddsidemargin=0pt
\\\\evensidemargin=0pt
\\\\parindent=0pt
\\\\pagestyle{empty}
\\\\def\\\\dfrac#1#2{{\\\\displaystyle{#1\\\\over #2}}} 
\\\\def\\\\d{\\\\mathrm{d}}
\\\\begin{document}
_texstuff
sed \\
  -e 's/&/\\\\/g' \\
  -e "s/\\\\[4 dl 3 dl 1 dl 3 dl\\\\] 1 1 0/\\\\[5 dl 2 dl 1 dl 2 dl 1 dl 2 dl\\\\] 0 1 1/g" \\
  -e "s/e+0/e+/g" -e "s/e-0/e-/g" \\
  -e "s/\\endinput//g" \\
  -e "s/1.0e+/10^{/g" -e "s/1.0e-/10^{-/g" \\
  -e "s/e+/\\\\\\times 10^{/g" -e "s/e-/\\\\\\times 10^{-/g" \\
  -e "s/10^{[-0-9]*/&}/g" v$$.plot >> v$$.tex

cat << _texstuff2 >> v$$.tex
\\\\end{document}
_texstuff2

epsfile="`basename $0 .sh`.eps"
latex v$$.tex
dvips -E -o $epsfile v$$.dvi
rm -f v$$.plot v$$.tex v$$.aux v$$.log v$$.dvi
epstopdf $epsfile
rm $epsfile
""" % input_list

    with open(out_file_name,"w") as bash_file:
        bash_file.write(bash_file_content)
#}}}
#{{{ def: determine_y_range(in_file)
def determine_y_range(in_file, extrapolated_path = ""):
    # this routine returns a cross section with error and uncertainties from a file path
    new_data = readin_file(in_file) # reads space-separated matrix from file

    central_values = []
    for row in new_data:
        for index,column in enumerate(row):
            if index == 7: central_values.append(float(column))
    if extrapolated_path:
        extrapolated_data = readin_file(extrapolated_path)
        central_values.append(float(extrapolated_data[3][1]))
    y_range = abs((min(central_values)-max(central_values))/min(central_values))
    return y_range
#}}}
#{{{ def: get_central_with_error_from_files(normalization_NLO_path,normalization_summary_path)
def get_central_with_error_from_files(NLO_file,summary_file):
    # this routine returns a cross section with error and uncertainties from a file path
    NLO_data = readin_file(NLO_file) # reads space-separated matrix from file
    
    central        = float(NLO_data[3][2])
    central_err    = float(NLO_data[3][3])
    qTerror = 0

    with open(summary_file,"r") as f:
        for line in f:
            line = line.strip()
            search_for = "<MATRIX-RESULT> r_cut-->0 indicates qT-subtraction uncertainty of"
            if search_for in line:
                try:
                    qTerror = float(line.split(search_for)[1].split("%")[0])
                except:
                    qTerror = 0
    return central, central_err, qTerror
#}}}

script_dir = os.path.dirname(os.path.abspath(__file__))
script_dir_not_link = os.path.dirname(os.path.realpath(__file__))
sys.path.append(pjoin(os.path.dirname(script_dir_not_link),"bin/modules"))
from handle_output import *
out = print_output() # class for handling the on-screen output

# give one path of runs as input
if len(sys.argv) != 2:
    out.print_error("You have to give one argument where the run is that you want to create the extrapolation plot for")

run_folder = sys.argv[1]
rcut_folder = sys.argv[1]+"_rcut_plot"

all_pdf_files = []
for process in processes:
    process_name = process
    try:
        os.makedirs(rcut_folder)
    except:
        pass
    os.chdir(rcut_folder)
    rcut_NLO_path  = pjoin(run_folder,"result","result..MATRIX.NLO.result","CV","7-point","plot.qTcut.NLO.QCD__NLORUN.dat")
    rcut_NLO_path  = pjoin(run_folder,"result","result..MATRIX.NLO.result","CV","7-point","plot.qTcut.NLO.QCD__NLORUN.dat")
    NLO_orig_path = rcut_NLO_path.replace("plot.qTcut.NLO.QCD__NLORUN.dat","plot.CV.NLO.QCD__NLORUN.dat")
    NLO_ext_orig_path = rcut_NLO_path.replace("plot.qTcut.NLO.QCD__NLORUN.dat","plot.CV.extrapolated.NLO.QCD__NLORUN.dat")
    input_list = {}
    if os.path.exists(rcut_NLO_path) and os.path.exists(rcut_NLO_path):
        # if take_normalization_from_run:
        #     normalization_NLO_path    = pjoin(process.replace(processes_path,normalization_path),"result","run_test_"+process_name,"NLO-run","rate_NLO_QCD.dat")
        #     normalization_summary_path = pjoin(process.replace(processes_path,normalization_path),"result","run_test_"+process_name,"summary","result_summary.dat")
        #     if os.path.exists(normalization_NLO_path) and os.path.exists(normalization_summary_path):
        #         central, error, qTerror = get_central_with_error_from_files(normalization_NLO_path,normalization_summary_path)
        #         input_list["NLO_normalization"] = central
        #         input_list["error"]   = error
        #         input_list["rel_qTerror"] = qTerror
        #     else:
        #         out.print_warning("Path to normalization files not found (not ready yet) for process path %s. Skipping folder..." % pjoin(process.replace(processes_path,normalization_path),"result","run_test_"+process_name))
        #         continue
        if "a" in process_name:
            input_list["fitnnlomax"] = 0.05
        else:
            input_list["fitnnlomax"] = 1
        input_list["rcut_NLO_file"]  = rcut_NLO_path
        input_list["rcut_NLO_file"] = rcut_NLO_path
        input_list["NLO_orig_path"] = NLO_orig_path
        input_list["NLO_ext_orig_path"] = NLO_ext_orig_path
        input_list["NLO_ext_005_path"] = NLO_ext_orig_path
        input_list["process_string"] = process_output_from_id[process_name].replace("\\","&").replace("$","")
        input_list["y_max"] =  0.003
        input_list["y_min"] = -0.003
        input_list["ytics"] = """
("&Large\\$ -1.00\\$" -0.010, "" -0.0098 1, "" -0.0096 1, "" -0.0094 1, "" -0.0092 1,
 "&Large\\$ -0.90\\$" -0.009, "" -0.0088 1, "" -0.0086 1, "" -0.0084 1, "" -0.0082 1,
 "&Large\\$ -0.80\\$" -0.008, "" -0.0078 1, "" -0.0076 1, "" -0.0074 1, "" -0.0072 1,
 "&Large\\$ -0.70\\$" -0.007, "" -0.0068 1, "" -0.0066 1, "" -0.0064 1, "" -0.0062 1,
 "&Large\\$ -0.60\\$" -0.006, "" -0.0058 1, "" -0.0056 1, "" -0.0054 1, "" -0.0052 1,
 "&Large\\$ -0.50\\$" -0.005, "" -0.0048 1, "" -0.0046 1, "" -0.0044 1, "" -0.0042 1,
 "&Large\\$ -0.40\\$" -0.004, "" -0.0038 1, "" -0.0036 1, "" -0.0034 1, "" -0.0032 1,
 "&Large\\$ -0.30\\$" -0.003, "" -0.0028 1, "" -0.0026 1, "" -0.0024 1, "" -0.0022 1,
 "&Large\\$ -0.20\\$" -0.002, "" -0.0018 1, "" -0.0016 1, "" -0.0014 1, "" -0.0012 1,
 "&Large\\$ -0.10\\$" -0.001, "" -0.0008 1, "" -0.0006 1, "" -0.0004 1, "" -0.0002 1,
 "&Large\\$ 0\\$" 0., "" 0.0002 1, "" 0.0004 1, "" 0.0006 1, "" 0.0008 1,
 "&Large\\$ +0.10\\$" 0.001, "" 0.0012 1, "" 0.0014 1, "" 0.0016 1, "" 0.0018 1,
 "&Large\\$ +0.20\\$" 0.002, "" 0.0022 1, "" 0.0024 1, "" 0.0026 1, "" 0.0028 1,
 "&Large\\$ +0.30\\$" 0.003, "" 0.0032 1, "" 0.0034 1, "" 0.0036 1, "" 0.0038 1,
 "&Large\\$ +0.40\\$" 0.004, "" 0.0042 1, "" 0.0044 1, "" 0.0046 1, "" 0.0048 1,
 "&Large\\$ +0.50\\$" 0.005, "" 0.0052 1, "" 0.0054 1, "" 0.0056 1, "" 0.0058 1,
 "&Large\\$ +0.60\\$" 0.006, "" 0.0062 1, "" 0.0064 1, "" 0.0066 1, "" 0.0068 1,
 "&Large\\$ +0.70\\$" 0.007, "" 0.0072 1, "" 0.0074 1, "" 0.0076 1, "" 0.0078 1,
 "&Large\\$ +0.80\\$" 0.008, "" 0.0082 1, "" 0.0084 1, "" 0.0086 1, "" 0.0088 1,
 "&Large\\$ +0.90\\$" 0.009, "" 0.0092 1, "" 0.0094 1, "" 0.0096 1, "" 0.0098 1,
 "&Large\\$ +1.00\\$" 0.010)""".replace("\n","")
        y_range = determine_y_range(rcut_NLO_path,NLO_ext_orig_path)
        if y_range/2 > 0.002:
            rounded_y_range = round(y_range, -int(floor(log10(abs(y_range)))))
            input_list["y_max"] = rounded_y_range
            input_list["y_min"] = -rounded_y_range
            if rounded_y_range >= 0.03:
                 input_list["ytics"] = """
(
 "&Large\\$ -7.00\\$" -0.070, "" -0.068 1, "" -0.066 1, "" -0.064 1, "" -0.062 1,
 "&Large\\$ -6.00\\$" -0.060, "" -0.058 1, "" -0.056 1, "" -0.054 1, "" -0.052 1,
 "&Large\\$ -5.00\\$" -0.050, "" -0.048 1, "" -0.046 1, "" -0.044 1, "" -0.042 1,
 "&Large\\$ -4.00\\$" -0.040, "" -0.038 1, "" -0.036 1, "" -0.034 1, "" -0.032 1,
 "&Large\\$ -3.00\\$" -0.030, "" -0.028 1, "" -0.026 1, "" -0.024 1, "" -0.022 1,
 "&Large\\$ -2.00\\$" -0.020, "" -0.018 1, "" -0.016 1, "" -0.014 1, "" -0.012 1,
 "&Large\\$ -1.00\\$" -0.010, "" -0.008 1, "" -0.006 1, "" -0.004 1, "" -0.002 1,
 "&Large\\$ 0\\$" 0., "" 0.002 1, "" 0.004 1, "" 0.006 1, "" 0.008 1,
 "&Large\\$ +1.00\\$" 0.010, "" 0.012 1, "" 0.014 1, "" 0.016 1, "" 0.018 1,
 "&Large\\$ +2.00\\$" 0.020, "" 0.022 1, "" 0.024 1, "" 0.026 1, "" 0.028 1,
 "&Large\\$ +3.00\\$" 0.030, "" 0.032 1, "" 0.034 1, "" 0.036 1, "" 0.038 1,
 "&Large\\$ +4.00\\$" 0.040, "" 0.042 1, "" 0.044 1, "" 0.046 1, "" 0.048 1,
 "&Large\\$ +5.00\\$" 0.050, "" 0.052 1, "" 0.054 1, "" 0.056 1, "" 0.058 1,
 "&Large\\$ +6.00\\$" 0.060, "" 0.062 1, "" 0.064 1, "" 0.066 1, "" 0.068 1,
 "&Large\\$ +7.00\\$" 0.070, "" 0.072 1, "" 0.074 1, "" 0.076 1, "" 0.078 1
)""".replace("\n","")
            elif rounded_y_range >= 0.02:
                 input_list["ytics"] = """
(
 "&Large\\$ -3.00\\$" -0.030, "" -0.029 1, "" -0.028 1, "" -0.027 1, "" -0.026 1,
 "&Large\\$ -2.50\\$" -0.025, "" -0.024 1, "" -0.023 1, "" -0.022 1, "" -0.021 1,
 "&Large\\$ -2.00\\$" -0.020, "" -0.019 1, "" -0.018 1, "" -0.017 1, "" -0.016 1,
 "&Large\\$ -1.50\\$" -0.015, "" -0.014 1, "" -0.013 1, "" -0.012 1, "" -0.011 1,
 "&Large\\$ -1.00\\$" -0.010, "" -0.009 1, "" -0.008 1, "" -0.007 1, "" -0.006 1,
 "&Large\\$ -0.50\\$" -0.005, "" -0.004 1, "" -0.003 1, "" -0.002 1, "" -0.001 1,
 "&Large\\$ 0\\$" 0., "" 0.001 1, "" 0.002 1, "" 0.003 1, "" 0.004 1,
 "&Large\\$ +0.50\\$" 0.005, "" 0.006 1, "" 0.007 1, "" 0.008 1, "" 0.009 1,
 "&Large\\$ +1.00\\$" 0.010, "" 0.011 1, "" 0.012 1, "" 0.013 1, "" 0.014 1,
 "&Large\\$ +1.50\\$" 0.015, "" 0.016 1, "" 0.017 1, "" 0.018 1, "" 0.019 1,
 "&Large\\$ +2.00\\$" 0.020, "" 0.021 1, "" 0.022 1, "" 0.023 1, "" 0.024 1,
 "&Large\\$ +2.50\\$" 0.025, "" 0.026 1, "" 0.027 1, "" 0.028 1, "" 0.029 1,
 "&Large\\$ +3.00\\$" 0.030, "" 0.031 1, "" 0.032 1, "" 0.033 1, "" 0.034 1
)""".replace("\n","")
            elif rounded_y_range >= 0.01:
                 input_list["ytics"] = """
("&Large\\$ -1.00\\$" -0.010, "" -0.0098 1, "" -0.0096 1, "" -0.0094 1, "" -0.0092 1,
 "&Large\\$ -0.80\\$" -0.008, "" -0.0078 1, "" -0.0076 1, "" -0.0074 1, "" -0.0072 1,
 "&Large\\$ -0.60\\$" -0.006, "" -0.0058 1, "" -0.0056 1, "" -0.0054 1, "" -0.0052 1,
 "&Large\\$ -0.40\\$" -0.004, "" -0.0038 1, "" -0.0036 1, "" -0.0034 1, "" -0.0032 1,
 "&Large\\$ -0.20\\$" -0.002, "" -0.0018 1, "" -0.0016 1, "" -0.0014 1, "" -0.0012 1,
 "&Large\\$ 0\\$" 0., "" 0.0002 1, "" 0.0004 1, "" 0.0006 1, "" 0.0008 1,
 "&Large\\$ +0.20\\$" 0.002, "" 0.0022 1, "" 0.0024 1, "" 0.0026 1, "" 0.0028 1,
 "&Large\\$ +0.40\\$" 0.004, "" 0.0042 1, "" 0.0044 1, "" 0.0046 1, "" 0.0048 1,
 "&Large\\$ +0.60\\$" 0.006, "" 0.0062 1, "" 0.0064 1, "" 0.0066 1, "" 0.0068 1,
 "&Large\\$ +0.80\\$" 0.008, "" 0.0082 1, "" 0.0084 1, "" 0.0086 1, "" 0.0088 1,
 "&Large\\$ +1.00\\$" 0.010)""".replace("\n","")
        bash_script_file_name_NLO = pjoin(rcut_folder,"rcut_NLO_"+process_name+".sh")
        write_rcut_variation_NLO_file(input_list,bash_script_file_name_NLO)
        # make bash script executable
        st = os.stat(bash_script_file_name_NLO)
        os.chmod(bash_script_file_name_NLO, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
        # execute bashs cript
        subprocess.call(bash_script_file_name_NLO)
        all_pdf_files.append(bash_script_file_name_NLO.replace(".sh",".pdf"))
    else:
        out.print_warning("Path to rcut plots not found (not ready yet) for process path %s. Skipping folder..." % os.path.dirname(rcut_NLO_path))
        continue

command = "pdfunite"
# Appending all pdfs
for pdf_file in all_pdf_files:
     command += " \"%s\"" % pdf_file
     # Writing all the collected pages to a file
combined_pdf_file = pjoin(rcut_folder,"all_rcut_plots.pdf")
command += " \"%s\"" % combined_pdf_file
out.print_info("Combining all pdf files into single file \"all_rcut_plots.pdf\"...")
subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
