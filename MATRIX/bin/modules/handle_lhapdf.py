#{{{ imports
import os
import subprocess
import glob
import tarfile
import urllib
from os.path import join as pjoin
# own modules
from initialize_classes import out
#}}}

#{{{ class: lhapdf()
class lhapdf(): # class that takes care of PDF sets
#{{{ def: init(self,path,lo_set_in,nlo_set_in,nnlo_set_in,,order_in)
    def __init__(self,config_list,lo_set_in,nlo_set_in,nnlo_set_in,order_in):
        try: # first use path from MATRIX_configuration
            self.lhapdf_config = config_list["path_to_lhapdf"]
        except:
            try: # otherwise try using which to identify path to lhapdf-config
                self.lhapdf_config = self.get_lhapdf_config_path()
            except:
                try: # sometime which does not work, test wether simply using lhapdf-config directly works
                    self.lhapdf_config = "lhapdf-config"
                    subprocess.Popen([self.lhapdf_config,"--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
                except:
                    out.print_error("Path to \"lhapdf-config\" is not set in MATRIX_configuration and does not appear to be configured as executable. Set \"path_to_lhapdf\" in MATRIX_configuration file or add path to \"lhapdf-config\" to your environmental $PATH variable.")
        self.lhapdf_version = self.get_lhapdf_version(self.lhapdf_config) # this is also a check wether lhapdf-config works fine
        self.print_lhapdf_version()
        self.lhapdf_set = {}
        self.lhapdf_set["LO"]   = lo_set_in
        self.lhapdf_set["NLO"]  = nlo_set_in
        self.lhapdf_set["NNLO"] = nnlo_set_in
        self.order = order_in
        self.init_pdfpath_and_installed_sets()
#}}}
#{{{ def: init_pdfpath_and_installed_sets(self)
    def init_pdfpath_and_installed_sets(self):
        self.pdf_sets_path    = self.get_pdf_sets_path(self.lhapdf_config)
        self.read_access_pdf_sets_path = True
        self.write_access_pdf_sets_path = True
        if not os.access(self.pdf_sets_path, os.R_OK):
            out.print_warning("No read access for PDFsets folder %s. Make sure you have at least read access to lhapdf-config --datadir, or \"export LHAPDF_DATA_PATH=/path/\" to a path where you have read/write access. Continuing without checking if PDF sets are available..." % self.pdf_sets_path)
            self.read_access_pdf_sets_path = False
        if not os.access(self.pdf_sets_path, os.W_OK):
            self.write_access_pdf_sets_path = False

        if self.lhapdf_version.startswith("6."):
            self.installed_sets = [os.path.basename(x) for x in glob.iglob(pjoin(self.pdf_sets_path,"*")) if os.path.isdir(x)]
        elif self.lhapdf_version.startswith("5."):
            self.installed_sets = [os.path.splitext(os.path.basename(x))[0] for x in glob.iglob(pjoin(self.pdf_sets_path,"*.LHgrid"))]
        else:
            out.print_error("LHAPDF version gives %s, but must be either 5 or 6. Check wether your \"lhapdf-config\" is the right one." % self.lhapdf_version)

#}}}
#{{{ def: get_lhapdf_config_path(self)
    def get_lhapdf_config_path(self):
        lhapdf_config_path = subprocess.Popen(["which","lhapdf-config"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
        if lhapdf_config_path == "": # if empty string cause exception
            lhapdf_config_path = 1/lhapdf_config_path
        return lhapdf_config_path
#}}}    
#{{{ def: get_lhapdf_version(self,lhapdf_config_path)
    def get_lhapdf_version(self,lhapdf_config_path):
        try:
            version = subprocess.Popen([lhapdf_config_path,"--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
        except:
            out.print_error("Cannot extract version from path to \"lhapdf-config\": %s" % lhapdf_config_path)
        return version
#}}}    
#{{{ def: print_lhapdf_version(self,lhapdf_config_path)
    def print_lhapdf_version(self):
        out.print_info("Using LHAPDF version %s..." % self.lhapdf_version )
#}}}    
#{{{ def: get_pdf_sets_path(self,lhapdf_config_path)
    def get_pdf_sets_path(self,lhapdf_config_path):
        if self.lhapdf_version.startswith("6."):
            return self.get_pdf_sets_path_6(lhapdf_config_path)
        elif self.lhapdf_version.startswith("5."):
            return self.get_pdf_sets_path_5(lhapdf_config_path)
        else:
            out.print_error("LHAPDF version gives %s, but must be either 5 or 6. Check wether your \"lhapdf-config\" is the right one." % self.lhapdf_version)
#}}}    
#{{{ def: get_pdf_sets_path_5(self,lhapdf_config_path)
    def get_pdf_sets_path_5(self,lhapdf_config_path):
        pdf_sets_path = subprocess.Popen([lhapdf_config_path,"--pdfsets-path"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
        return pdf_sets_path
#}}}    
#{{{ def: get_pdf_sets_path_6(self,lhapdf_config_path)
    def get_pdf_sets_path_6(self,lhapdf_config_path):
        if os.environ.get('LHAPDF_DATA_PATH',''):
            out.print_info("Found exported variable $LHAPDF_DATA_PATH. Usinig folder %s for download." % os.environ['LHAPDF_DATA_PATH'])
            pdf_sets_path = os.environ['LHAPDF_DATA_PATH']
        else:
            pdf_sets_path = subprocess.Popen([lhapdf_config_path,"--datadir"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
        return pdf_sets_path
#}}}    
#{{{ def: get_missing_sets(self)
    def get_missing_sets(self):
        missing_sets = []
        for this_order in self.order:
            if not self.lhapdf_set[this_order] in self.installed_sets:
                missing_sets.append(self.lhapdf_set[this_order])
        if not missing_sets:
            out.print_info("All PDF sets already installed. Continuning without download...")
        return missing_sets
#}}}
#{{{ def: download_pdf_sets(self)
    def download_pdf_sets(self,missing_sets):
        out.print_info("Downloading missing LHAPDF sets...")
        if not self.write_access_pdf_sets_path:
            out.print_warning("No write access for PDFsets folder %s. Make sure you have write access to lhapdf-config --datadir, or \"export LHAPDF_DATA_PATH=/path/\" to a path where you have write access. Continuing without downloading PDF sets..." % self.pdf_sets_path)
            return
        if self.lhapdf_version.startswith("6."):
            return self.download_pdf_sets_6(missing_sets)
        elif self.lhapdf_version.startswith("5."):
            return self.download_pdf_sets_5(missing_sets)
        else:
            out.print_error("LHAPDF version gives %s, but must be either 5 or 6. Check wether your \"lhapdf-config\" is the right one." % self.lhapdf_version)
#}}}
#{{{ def: download_pdf_sets_5(self)
    def download_pdf_sets_5(self,missing_sets):
        for missing_set in missing_sets:
            if "NNPDF30" in missing_set:
#                try:
                    self.download_set_by_hand("http://pcteserver.mi.infn.it/~nnpdf/nnpdf30/",missing_set+".LHgrid.tgz")
                    out.print_info("LHAPDF set \"%s.LHgrid\" successfully downloaded." % missing_set)
#                except:
#                    out.print_error("PDF set \"%s\" is no standard set in LHAPDF 5. Downloading it directly from the developer's website failed. Try to download it by hand or change PDF set or upgrade to LHAPDF 6, and restart code!" % missing_set)
            elif "MMHT2014" in missing_set:
                try:
                    self.download_set_by_hand("https://www.hep.ucl.ac.uk/mmht/LHAPDF5/",missing_set+".LHgrid")
                    out.print_info("LHAPDF set \"%s.LHgrid\" successfully downloaded." % missing_set)
                except:
                    out.print_error("PDF set \"%s\" is no standard set in LHAPDF 5. Downloading it directly from the developer's website failed. Try to download it by hand or change PDF set or upgrade to LHAPDF 6, and restart code!" % missing_set)
            else:
                download_set = subprocess.Popen(["lhapdf-getdata","--dest="+self.pdf_sets_path,missing_set+".LHgrid"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1].strip().rsplit("\n",1)[1]
                if "Getting PDF set from" in download_set:
                    out.print_info("LHAPDF set \"%s.LHgrid\" successfully downloaded." % missing_set)
                elif "No sets match the arguments given" in download_set:
                    out.print_error("Failed to download LHAPDF set \"%s\" with \"lhapdf-getdata\" script. Try installing it manually to folder %s and restart the code." % (missing_set+".LHgrid",self.pdf_sets_path))
                else:
                    out.print_warning("LHAPDF printout \"%s\" not recognized. Stop the code manually if set \"%s\" was not correctly installed in folder %s." % (download_set,missing_set+".LHgrid",self.pdf_sets_path))
#}}}
#{{{ def: download_set_by_hand(self,url_link,PDF_set)
    def download_set_by_hand(self,url_link,pdf_set):
        cwd = os.getcwd()
        os.chdir(self.pdf_sets_path)
        pdf_file_url = url_link+pdf_set
        try:
            urllib.urlretrieve(pdf_file_url,pdf_set)
        except:
            try:
                urllib.request.urlretrieve(pdf_file_url,pdf_set)
            except:
                pass
        if pdf_set.endswith(".tgz") or pdf_set.endswith(".tar")or pdf_set.endswith(".tar.gz"):
            tar = tarfile.open(pdf_set)
            tar.extractall()
            tar.close()
            os.remove(pdf_set)
        os.chdir(cwd)
#}}}
#{{{ def: download_pdf_sets_6(self)


    def download_pdf_sets_6(self,missing_sets):
        firsttime = True
        for missing_set in missing_sets:
            if missing_set in ["NNPDF31_nlo_as_0118_luxqed_nf_4","NNPDF31_nnlo_as_0118_luxqed_nf_4"]:
                try:
                    self.download_set_by_hand("http://nnpdf.mi.infn.it/wp-content/uploads/2019/01/",missing_set+".tar.gz")
                    out.print_info("LHAPDF set \"%s\" successfully downloaded." % missing_set)
                except:
                    out.print_warning("Could not download PDF set %s. Please download it yourself from the NNPDF website (http://nnpdf.mi.infn.it/nnpdf3-1luxqed/)." % missing_set)
                    pass
            else:
                download_set = filter(None, subprocess.Popen(["lhapdf","get",missing_set], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate())
                try:
                    download_set = list(download_set)
                except:
                    pass
                try:
                    download_set = (download_set+["noreturn"])[0].strip()
                except:
                    pass
                installed_sets = [os.path.basename(x) for x in glob.iglob(pjoin(self.pdf_sets_path,"*")) if os.path.isdir(x)]
                if download_set.startswith("%s.tar.gz" % missing_set) or missing_set in installed_sets:
                    out.print_info("LHAPDF set \"%s\" successfully downloaded." % missing_set)
                else:
                    if firsttime:
                        out.print_warning("Cannot download any generic PDF set, as \"lhapdf get\" script constantly failing. Trying to download them manually...")
                        firsttime = False
                    try:
                        self.download_set_by_hand("https://lhapdf.hepforge.org/downloads?f=pdfsets/v6.backup/current/",missing_set+".tar.gz")
                        out.print_info("LHAPDF set \"%s\" successfully downloaded." % missing_set)
                    except:
                        out.print_warning("Could not download PDF set %s. Please download it yourself from the LHAPDF website (https://lhapdf.hepforge.org/pdfsets)" % missing_set)
#}}}
#}}}
