#!/usr/bin/env python

do_muR_muF_creation = True
do_totalXS          = True
do_distributions    = True

import copy
import glob
import math
import os
import shutil
from os.path import join as pjoin 

#{{{ def: readin_file(self,file_path)
def readin_file(file_path):
    # this reads a file and writes it into a table
    with open(file_path,'r') as f:
        table = [row.strip().split() for row in f if not row.split()[0].startswith("#") and not row.split()[0].startswith("%")]
    return table
#}}}    
#{{{ def: create_muRmuF_result_file(file_list,out_file)
def create_muRmuF_result_file(file_list,out_file,norm):
    # this routine take several files with identical first column; it keeps the first column and adds the other columns to the right
    firsttime = True
    filenumber = -1
    for file_path in file_list:
        filenumber += 1
        new_data = readin_file(file_path) # reads space-separated matrix from file
        if firsttime: # initialize data array; lengt must be 1 first column + the number of other columns times the number of files
            data = [[0 for col in range(1+(len(new_data[0])-1)*len(file_list))] for row in range(len(new_data))]
            firsttime = False
        for row in range(len(new_data)):
            for column in range(len(new_data[row])):
                if column == 0:
                    if filenumber == 0:
                        data[row][column] = float(new_data[row][column])
                else:
                    data[row][column+filenumber*(len(new_data[0])-1)] = float(new_data[row][column])/norm
    with open(out_file,'w') as f:
        f.write("#    observable  mR=0.5 mR=0.5        num_err  mR=0.5 mR=1.0        num_err  mR=2.0 mR=1.0        num_err  mR=1.0 mR=1.0        num_err  mR=1.0 mR=2.0        num_err  mR=1.0 mR=0.5        num_err  mR=2.0 mR=2.0        num_err\n")
        for row in range(len(data)):
            line = ""
            for column in range(len(data[row])):
                length = 15
                line = line+" "*(length-len("%.8g" % data[row][column]))+"%.8g" % data[row][column]
            f.write(line+"\n")
        f.close()
#}}}
#{{{ def: add_totalXS_files(file_list,out_file)
def add_totalXS_files(file_list,out_file,norm):
    # this routine adds a number of files (that MUST have the same structure), keeping the first twoc olumns fixed and adding all other columns linear
    firsttime = True
    for file_path in file_list:
        new_data = readin_file(file_path) # reads space-separated matrix from file
        if firsttime: # initialize data array
            data = [[0 for col in range(len(new_data[0]))] for row in range(len(new_data))]
            firsttime = False
        for row in range(len(new_data)):
            for column in range(len(new_data[row])):
                if column == 0 or column == 1:
                    data[row][column] = float(new_data[row][column])
                else:
                    data[row][column] += float(new_data[row][column])/norm
    with open(out_file,'w') as f:
        for row in range(len(data)):
            line = ""
            for column in range(len(data[row])):
                length = 15
                line = line+" "*(length-len("%.8g" % data[row][column]))+"%.8g" % data[row][column]
            f.write(line+"\n")
        f.close()
#}}}
#{{{ def: add_distribution_files(file_list,out_file)
def add_distribution_files(file_list,out_file,norm):
    # this routine adds a number of files (that MUST have the same structure), keeping the first column fixed and adding all other columns linear
    firsttime = True
    for file_path in file_list:
        new_data = readin_file(file_path) # reads space-separated matrix from file
        if firsttime: # initialize data array
            data = [[0 for col in range(len(new_data[0]))] for row in range(len(new_data))]
            firsttime = False
        for row in range(len(new_data)):
            for column in range(len(new_data[row])):
                if column == 0:
                    data[row][column] = float(new_data[row][column])
                else:
                    data[row][column] += float(new_data[row][column])/norm
    with open(out_file,'w') as f:
        for row in range(len(data)):
            line = ""
            for column in range(len(data[row])):
                length = 15
                line = line+" "*(length-len("%.8g" % data[row][column]))+"%.8g" % data[row][column]
            f.write(line+"\n")
        f.close()
#}}}
#{{{ def: get_central_min_max_from_muR_muF(in_file,out_file,norm)
def get_central_min_max_from_muR_muF(in_file,out_file,norm):
    # this routine adds a number of files (that MUST have the same structure), keeping the first column fixed and adding all other columns linear
    new_data = readin_file(in_file) # reads space-separated matrix from file
    data = [[0 for col in range(7)] for row in range(len(new_data))]
    for row in range(len(new_data)):
        data[row][1] = float(new_data[row][7])/norm
        data[row][2] = float(new_data[row][8])/norm
        for column in range(len(new_data[row])):
            if column == 0:
                data[row][column] = float(new_data[row][column])
            elif column == 1:
                data[row][3] = float(new_data[row][column])/norm
                data[row][4] = float(new_data[row][column+1])/norm
                data[row][5] = float(new_data[row][column])/norm
                data[row][6] = float(new_data[row][column+1])/norm
            elif column % 2 == 1:
                if data[row][3] > float(new_data[row][column])/norm:
                    data[row][3] = float(new_data[row][column])/norm
                    data[row][4] = float(new_data[row][column+1])/norm
                if data[row][5] < float(new_data[row][column])/norm:
                    data[row][5] = float(new_data[row][column])/norm
                    data[row][6] = float(new_data[row][column+1])/norm
    with open(out_file,'w') as f:
        for row in range(len(data)):
            line = ""
            for column in range(len(data[row])):
                length = 15
                line = line+" "*(length-len("%.8g" % data[row][column]))+"%.8g" % data[row][column]
            f.write(line+"\n")
        f.close()
#}}}
#{{{ def: create_totalXS_table(totalXS_dict)
def create_totalXS_table(totalXS_dict,out_file,mode):
    # ***VERY SPECIFIC*** this routine takes a dictionary with paths to total cross section files and creates a table
    Wm_production = ["ppeeexnex04","ppemexnmx04"]
    Wp_production = ["ppeexexne04","ppeexmxnm04"]
    Wm_dict = {}
    Wp_dict = {}
    mupee   = "$\\mu^+ e^+e^-$"
    epmumu  = "$e^+ \\mu^+\\mu^-$"
    epee    = "$e^+ e^+e^-$"
    mupmumu = "$\\mu^+ \\mu^+\\mu^-$"
    mumee   = "$\\mu^- e^+e^-$"
    emmumu  = "$e^- \\mu^+\\mu^-$"
    emee    = "$e^- e^+e^-$"
    mummumu = "$\\mu^- \\mu^+\\mu^-$"
    lprimepll = "$\\elle^{'+} \\elle^+\\elle^-$"
    lprimemll = "$\\elle^{'-} \\elle^+\\elle^-$"
    lporlprimepll = "$\\elle^{(')+} \\elle^+\\elle^-$"
    lmorlprimemll = "$\\elle^{(')-} \\elle^+\\elle^-$"
    lpll    = "$\\elle^{+} \\elle^+\\elle^-$"
    lmll    = "$\\elle^{-} \\elle^+\\elle^-$"
    combined= "combined"
    channel_dict = {"eem+unu" : mupee, "mumue+unu" : epmumu, "eee+nu" : epee, "mumumu+nu" : mupmumu, "eem-unu" : mumee, "mumue-nu" : emmumu, "eee-nu" : emee, "mumumu-nu" : mummumu, "combined" : combined, "lprimepll" : lprimepll, "lprimemll" : lprimemll, "lpll" : lpll, "lmll" : lmll, "lporlprimepll" : lporlprimepll, "lmorlprimemll" : lmorlprimemll}
    with open(out_file,'w') as f:
        header = """
\\begin{table}[t]
\\begin{center}
\\resizebox{\\columnwidth}{!}{%
\\begin{tabular}{c c c c c c}
\\toprule

channel
& $\\sigma_{\\textrm{LO}}$ [fb]
& $\\sigma_{\\textrm{NLO}}$ [fb]
& $\\sigma_{\\textrm{NNLO}}$ [fb]
& $\\sigma_{\\textrm{NLO}}/\\sigma_{\\textrm{LO}}$
& $\\sigma_{\\textrm{NNLO}}/\\sigma_{\\textrm{NLO}}$ [fb]\\\\

\\bottomrule
"""
        f.write(header)
        for key in ["LO","NLO","NNLO"]:
            for path in totalXS_dict[key]:
                if "3lnu_NP" in path and "W+-Z" in path:
                    channel = "combined"
                elif "3lnu_NP" in path and "W+Z_DF" in path:
                    channel = "lprimepll"
                elif "3lnu_NP" in path and "W-Z_DF" in path:
                    channel = "lprimemll"
                elif "3lnu_NP" in path and "W+Z_SF" in path:
                    channel = "lpll"
                elif "3lnu_NP" in path and "W-Z_SF" in path:
                    channel = "lmll"
                elif "3lnu_NP" in path and "W+Z" in path:
                    channel = "lporlprimepll"
                elif "3lnu_NP" in path and "W-Z" in path:
                    channel = "lmorlprimemll"
                else:
                    channel = path.split("_NNLO")[0].split("run_dynScale_NewPhysicsCMS13_")[1]
#                print path,channel,get_table_entry_from_XS_file(path)
                channel_dict[channel] = channel_dict[channel]+" & "+get_table_entry_from_XS_file(path)
        for ratio in [["NLO","LO"],["NNLO","NLO"]]:
            for path in totalXS_dict[ratio[0]]:
                if "3lnu_NP" in path and "W+-Z" in path:
                    channel = "combined"
                elif "3lnu_NP" in path and "W+Z_DF" in path:
                    channel = "lprimepll"
                elif "3lnu_NP" in path and "W-Z_DF" in path:
                    channel = "lprimemll"
                elif "3lnu_NP" in path and "W+Z_SF" in path:
                    channel = "lpll"
                elif "3lnu_NP" in path and "W-Z_SF" in path:
                    channel = "lmll"
                elif "3lnu_NP" in path and "W+Z" in path:
                    channel = "lporlprimepll"
                elif "3lnu_NP" in path and "W-Z" in path:
                    channel = "lmorlprimemll"
                else:
                    channel = path.split("_NNLO")[0].split("run_dynScale_NewPhysicsCMS13_")[1]
                path_denominator = path.replace(ratio[0]+"-run",ratio[1]+"-run").replace("_NLO_QCD.","_LO.").replace("_NNLO_QCD.","_NLO_QCD.")
                channel_dict[channel] = channel_dict[channel]+" & "+get_ratio_from_XS_files(path,path_denominator)
        if mode == 1: 
            channel_list = ["eem+unu","mumue+unu","eee+nu","mumumu+nu","eem-unu","mumue-nu","eee-nu","mumumu-nu","combined"]
        if mode == 2:
            channel_list = ["lprimepll","lpll","lprimemll","lmll","combined"]
        for channel in channel_list:
            if channel == "combined":
                f.write("\\midrule\n")
                f.write(channel_dict[channel]+" \\Bstrut\\\\\n")
            else:
                f.write(channel_dict[channel]+" \\Bstrut\\\\\n")
        body = """\\bottomrule

\\end{tabular}}
\\end{center}
\\renewcommand{\\baselinestretch}{1.0}
\\caption{\\label{tab:} Fiducial cross sections for ...}
\\end{table}
"""
        f.write(body)
#}}}
#{{{ def: get_table_entry_from_XS_file(in_file)
def get_table_entry_from_XS_file(in_file):
    # this routine returns a cross section with error and uncertainties from a file path
    new_data = readin_file(in_file) # reads space-separated matrix from file
    
    central        = float(new_data[3][2])
    central_err    = float(new_data[3][3])
    min_value      = min([float(row[2]) for row in new_data])
    max_value      = max([float(row[2]) for row in new_data])
    central_string = ("%#.4g" % central).rstrip('.') 
    central_base   = 10**(4-math.ceil(math.log10(central)))
    err_string     = "("+str(int(round(central_err * central_base)))+")"
    min_ratio      = ("%#.2g" % ((min_value/central-1) * 100)).rstrip('.') 
    max_ratio      = ("%#.2g" % ((max_value/central-1) * 100)).rstrip('.') 
    min_string     = "_{%s\\%%}" % min_ratio
    max_string     = "^{+%s\\%%}" % max_ratio

    string = "$"+central_string+err_string+min_string+max_string+"$"
#    print string
    return string 
#}}}
#{{{ def: get_ratio_from_XS_files(nominator_path,denominator_path)
def get_ratio_from_XS_files(nominator_path,denominator_path):
    # this routine returns a cross section with error and uncertainties from a file path
    nominator   = readin_file(nominator_path) # reads space-separated matrix from file
    denominator = readin_file(denominator_path)   # reads space-separated matrix from file
    
    ratio = ("%#.3g" % ((float(nominator[3][2])/float(denominator[3][2])-1) * 100)).rstrip('.')+"\\%"
#    print ratio
    return ratio
#}}}
script_dir = os.path.dirname(os.path.abspath(__file__)) # needed to determine process from name
runs_dir = "/mnt/runs2/wiesemann"

#runs =["","_ETmissCUT","_ATLAS_identification","_MTcut","_MTcut_ATLAS_identification","_mllCUT"]#,"_ETmissCUT_ATLAS_identification"]
runs = ["","_ATLAS_identification"]
#cases =["W+-Z","W+-Z_SF","W+-Z_DF","W+-Z_SF_e","W+-Z_SF_mu","W+-Z_DF_e","W+-Z_DF_mu","W+Z_DF","W-Z_DF","W+Z_SF","W-Z_SF","W+Z","W-Z"]
cases =["W+-Z","W+-Z_DF","W+-Z_DF_e","W+-Z_DF_mu","W+Z_DF","W-Z_DF","W+Z","W-Z"]
order_runs = ["LO-run","NLO-run","NNLO-run"]

# create distributions with full muR-muF information in all subprocesses
if do_muR_muF_creation:
  for process in ["ppeeexnex04","ppeexexne04","ppeexmxnm04","ppemexnmx04"]:
    for order_run in order_runs:
        order = order_run.replace("-run","")
        wp_wm_folders = glob.glob(pjoin(runs_dir,process+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*_NNLO*",order_run,"distributions"))
        for folder in wp_wm_folders:
            muR_muF_folder = pjoin(folder,"muR_muF_information")
            try:
                shutil.rmtree(muR_muF_folder)
            except:
                pass
            print "Creating full muR-muF information in folder: "+muR_muF_folder
            os.makedirs(muR_muF_folder)
            runfolder = os.path.basename(os.path.normpath(os.path.dirname(os.path.dirname(folder))))
            input_folders = [pjoin(pjoin(runs_dir,process+"_MATRIX",runfolder,"result","result.NNLO.QT-CS.NLO.CS.LO_py","CV"),"scale."+str(x)+"."+str(x)) for x in range(7)]
            input_files = glob.glob(pjoin(input_folders[0],"plot.*.."+order+"*.dat"))
            for input_file in input_files:
                filename = os.path.basename(input_file)
                muR_muF_file_list = [pjoin(x,filename) for x in input_folders]
                output_file = pjoin(muR_muF_folder,filename).replace("plot.","").replace("..","__").replace(".QCD","_QCD")
                create_muRmuF_result_file(muR_muF_file_list,output_file,1)

channels = {}
channels["ppeeexnex04"] = ["eee*nu","mumumu*nu"]
channels["ppeexexne04"] = ["eee*nu","mumumu*nu"]
channels["ppeexmxnm04"] = ["eem*unu","mumue*nu"]
channels["ppemexnmx04"] = ["eem*unu","mumue*nu"]
for run in runs:
  print "run: "+run
  print "------------------------------"
  for case in cases:
    if "_ATLAS_identification" in run and "DF" in case:
        continue
    print "    case: "+case
    lepton_restriction = ""
    if "SF_e" in case:
        processes = ["ppeeexnex04","ppeexexne04"]
        lepton_restriction = "eee*nu"
        norm = 1
    elif "SF_mu" in case:
        processes = ["ppeeexnex04","ppeexexne04"]
        lepton_restriction = "mumumu*nu"
        norm = 1
    elif "DF_e" in case:
        processes = ["ppeexmxnm04","ppemexnmx04"]
        lepton_restriction = "eem*unu"
        norm = 1
    elif "DF_mu" in case:
        processes = ["ppeexmxnm04","ppemexnmx04"]
        lepton_restriction = "mumue*nu"
        norm = 1
    elif "W+Z_SF" in case:
        processes = ["ppeexexne04"]
        norm = 1#2
    elif "W-Z_SF" in case:
        processes = ["ppeeexnex04"]
        norm = 1#2
    elif "W+Z_DF" in case:
        processes = ["ppeexmxnm04"]
        norm = 1#2
    elif "W-Z_DF" in case:
        processes = ["ppemexnmx04"]
        norm = 1#2
    elif "W+Z" in case:
        processes = ["ppeexexne04","ppeexmxnm04"]
        norm = 1#4
    elif "W-Z" in case:
        processes = ["ppeeexnex04","ppemexnmx04"]
        norm = 1#4
    elif "SF" in case:
        processes = ["ppeeexnex04","ppeexexne04"]
        norm = 1#2
    elif "DF" in case:
        processes = ["ppeexmxnm04","ppemexnmx04"]
        norm = 1#2
    elif "W+-Z" in case:
        processes = ["ppeeexnex04","ppeexexne04","ppeexmxnm04","ppemexnmx04"]
        norm = 1#4
    totalXS_dict = {}
    for order_run in order_runs:
        print "        order: "+order_run
        case_dir = pjoin(script_dir,"combined_"+case+run)
        try:
            os.makedirs(case_dir)
        except:
            pass
        os.chdir(case_dir)
        try:
            os.makedirs(order_run)
        except:
            pass
        os.chdir(order_run)
        if lepton_restriction:
            channel = lepton_restriction
        else:
            channel = channels[processes[0]][0]
        totalXS_files_glob = glob.glob(pjoin(runs_dir,processes[0]+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*"+channel+"_NNLO"+run,order_run,"rate_*"))
        # for ATLAS identification case change DF channel to the normal one
        result_files_final = []
        if do_totalXS:
          for path in totalXS_files_glob:
            filename = os.path.basename(path)
            result_files = []
            for process in processes:
                for channel in channels[process]:
                    if lepton_restriction and not lepton_restriction in channel: continue
                    if "_ATLAS_identification" in run and process in ["ppeexmxnm04","ppemexnmx04"]:
                        run_now = run.replace("_ATLAS_identification","")
                    else:
                        run_now = run
                    print pjoin(runs_dir,process+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*"+channel+"_NNLO"+run_now,order_run,filename)
                    totalXS_file = glob.glob(pjoin(runs_dir,process+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*"+channel+"_NNLO"+run_now,order_run,filename))[0]
                    result_files.append(totalXS_file)
            result_files_final = copy.copy(result_files)
            result_files_final.append(pjoin(script_dir,case_dir,order_run,filename))
            print "totalXS - number of files added: "+str(len(result_files))
            add_totalXS_files(result_files,filename,norm)
        order = order_run.replace("-run","")
        totalXS_dict[order] = result_files_final
        if not do_distributions: continue
        try:
            shutil.rmtree("distributions")
        except:
            pass
        os.makedirs("distributions")
        os.chdir("distributions")
        try:
            shutil.rmtree("muR_muF_information")
        except:
            pass
        os.makedirs("muR_muF_information")
        os.chdir("muR_muF_information")
        if lepton_restriction:
            channel = lepton_restriction
        else:
            channel = channels[processes[0]][0]
        distribution_files_glob = glob.glob(pjoin(runs_dir,processes[0]+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*"+channel+"_NNLO"+run,order_run,"distributions","muR_muF_information","*.dat"))
        for path in distribution_files_glob:
            filename = os.path.basename(path)
            result_files = []
            for process in processes:
                for channel in channels[process]:
                    if lepton_restriction and not lepton_restriction in channel: continue
                    if "_ATLAS_identification" in run and process in ["ppeexmxnm04","ppemexnmx04"]:
                        run_now = run.replace("_ATLAS_identification","")
                    else:
                        run_now = run
                    distribution_file = glob.glob(pjoin(runs_dir,process+"_MATRIX","result","run_dynScale_NewPhysicsCMS13_*"+channel+"_NNLO"+run_now,order_run,"distributions","muR_muF_information",filename))[0]
                    result_files.append(distribution_file)
            print "distribution ("+filename+") - number of files added: "+str(len(result_files))
            add_distribution_files(result_files,filename,norm)
            os.chdir("..")
            get_central_min_max_from_muR_muF(pjoin("muR_muF_information",filename),filename,1)
            os.chdir("muR_muF_information")
    table_out_file = pjoin(case_dir,"XS_"+case+run+".tex")
    create_totalXS_table(totalXS_dict,table_out_file,1)
  table_out_file = pjoin(script_dir,"XS"+run+".tex")
  totalXS_dict = {}
  totalXS_dict["LO"]   = [pjoin(script_dir,"combined_W+Z_SF"+run,"LO-run","rate_LO.dat"),pjoin(script_dir,"combined_W-Z_SF"+run,"LO-run","rate_LO.dat"),pjoin(script_dir,"combined_W+Z_DF"+run,"LO-run","rate_LO.dat"),pjoin(script_dir,"combined_W-Z_DF"+run,"LO-run","rate_LO.dat"),pjoin(script_dir,"combined_W+-Z"+run,"LO-run","rate_LO.dat")]
  totalXS_dict["NLO"]  = [pjoin(script_dir,"combined_W+Z_SF"+run,"NLO-run","rate_NLO_QCD.dat"),pjoin(script_dir,"combined_W-Z_SF"+run,"NLO-run","rate_NLO_QCD.dat"),pjoin(script_dir,"combined_W+Z_DF"+run,"NLO-run","rate_NLO_QCD.dat"),pjoin(script_dir,"combined_W-Z_DF"+run,"NLO-run","rate_NLO_QCD.dat"),pjoin(script_dir,"combined_W+-Z"+run,"NLO-run","rate_NLO_QCD.dat")]
  totalXS_dict["NNLO"] = [pjoin(script_dir,"combined_W+Z_SF"+run,"NNLO-run","rate_NNLO_QCD.dat"),pjoin(script_dir,"combined_W-Z_SF"+run,"NNLO-run","rate_NNLO_QCD.dat"),pjoin(script_dir,"combined_W+Z_DF"+run,"NNLO-run","rate_NNLO_QCD.dat"),pjoin(script_dir,"combined_W-Z_DF"+run,"NNLO-run","rate_NNLO_QCD.dat"),pjoin(script_dir,"combined_W+-Z"+run,"NNLO-run","rate_NNLO_QCD.dat")]
  if not "_ATLAS_identification" in run:
      create_totalXS_table(totalXS_dict,table_out_file,2)
