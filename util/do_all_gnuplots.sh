#/bin/sh

SEARCH_FOLDER="combined_*"

for folder in $SEARCH_FOLDER
do
    if [ -d "$folder" ]
    then
	cd $folder;
	create_files="custom_*.py"
	for file in $create_files
	do
#	    echo $file
	    if [ -f $file ]
	    then
#	    	echo $file
		./$file
	    fi
	done
	cd ..;
    fi
done

#for i in "combined_*/create_*"; do echo $i; DIR=$(dirname "$i"); cd $DIR; cd ..; done
