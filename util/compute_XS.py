import numpy as np
import math
import sys

def find_maxmin(Xsections):
	central = Xsections[3,:]
	minXS=central
	maxXS=central
	for i in range(len(Xsections[:,0])):
		if Xsections[i,1]<minXS[1]:
			minXS=Xsections[i]
		if Xsections[i,1]>maxXS[1]:
                        maxXS=Xsections[i]
	return central,maxXS,minXS


def nsf(num, n=1):
	"""n-Significant Figures"""
	if math.ceil(math.log10(num))>n:
		numstr = round(num)
	else:
		numstr = ("{0:.%ie}" % (n)).format(num)
	return float(numstr)

def print_sign_digits(XS):
	print(nsf(XS[1],int(math.ceil(math.log10(XS[1]/XS[2])))))

def print_result(central,maxXS,minXS):
	print(str(central[1])+' +/- '+str(central[2]))
	print(str(maxXS[1])+' +/- '+str(maxXS[2]))
	print(str(minXS[1])+' +/- '+str(minXS[2]))
	print_sign_digits(central)

	print('scale variations:')
	print('up: ' + str((maxXS[1]/central[1]-1)*100)+'% (+'+str(maxXS[1]-central[1])+')')
	print('down: ' + str((1-minXS[1]/central[1])*100)+'% (-'+str(central[1]-minXS[1])+')')
	return

if __name__=='__main__':
	XS = np.loadtxt(sys.argv[1])

	XScentral,XSmax,XSmin = find_maxmin(XS)

	print_result(XScentral,XSmax,XSmin)

#print(nnlocentral)
#print(nnlomax)
#print(nnlomin)

